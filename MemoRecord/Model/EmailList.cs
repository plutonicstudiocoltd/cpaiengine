﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoRecord.Model
{
    [Serializable]
    public class EmailList
    {
        public List<EMAIL_LIST> EMAIL_LIST { get; set; }
    }

    public class EMAIL_LIST
    {
        public string EMAIL { get; set; }
    }
}
