﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemoRecord.Model
{
    public class FailedRecord
    {
        public string PoNo { get; set; }
        public decimal PoItem { get; set; }
        public string MemoNo { get; set; }
        public string Reason { get; set; }
        public string Action { get; set; }
    }
}
