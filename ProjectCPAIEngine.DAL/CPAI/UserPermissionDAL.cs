﻿using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.Model;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;



namespace ProjectCPAIEngine.DAL
{
    public class UserPermissionDAL : IDisposable
    {
        public bool CheckUserWindowsAuthen(string username)
        {
            using (var context = new EntityCPAIEngine())
            {                
                var user = from u in context.USERS where u.USR_LOGIN.ToUpper() == username.ToUpper() select u;
                if (user.ToList().Count() > 0) return true;
                else return false;
            }
        }
        public List<MenuPermission> GetUserRoleMenu(string userLogin)
        {
            List<MenuPermission> lstMainMenu = new List<MenuPermission>();
            try
            {
                #region GetMenu
                using (var context = new EntityCPAIEngine())
                {
                    //List<MENU> _lstMenu = context.MENU.ToList();
                    //Model.UserRoleMenu uMenu = new Model.UserRoleMenu();

                    var MenuObj = from m in context.MENU
                                join rm in context.ROLE_MENU on m.MEU_ROW_ID equals rm.RMN_FK_MENU
                                join r in context.ROLE on rm.RMN_FK_ROLE equals r.ROL_ROW_ID
                                join ur in context.USER_ROLE on r.ROL_ROW_ID equals ur.URO_FK_ROLE
                                join u in context.USERS on ur.URO_FK_USER equals u.USR_ROW_ID
                                where u.USR_LOGIN.ToUpper() == userLogin.ToUpper() && m.MEU_ACTIVE=="ACTIVE"
                                select new MENUModel
                                {
                                   MEU_ROW_ID= m.MEU_ROW_ID,
                                   MEU_GROUP_MENU= m.MEU_GROUP_MENU,
                                   MEU_PARENT_ID= m.MEU_PARENT_ID,
                                   LNG_DESCRIPTION= m.LNG_DESCRIPTION,
                                   MEU_URL= m.MEU_URL,
                                   MEU_IMG= m.MEU_IMG,
                                   MEU_LEVEL= m.MEU_LEVEL,
                                   MEU_LIST_NO= m.MEU_LIST_NO,
                                   MEU_CONTROL_TYPE= m.MEU_CONTROL_TYPE,
                                   MEU_URL_DIRECT = m.MEU_URL_DIRECT

                                };
                    if (MenuObj != null)
                    {
                        
                        List<MENUModel> lstMain = MenuObj.Where(x => x.MEU_LEVEL == "1" && x.MEU_CONTROL_TYPE == "MENU").ToList();
                        foreach(var _itemMain in lstMain)
                        {
                            MenuPermission mainmenu = new MenuPermission();
                            mainmenu.MEU_ROWID = _itemMain.MEU_ROW_ID;
                            mainmenu.LNG_DESCRIPTION = _itemMain.LNG_DESCRIPTION;
                            mainmenu.MEU_CONTROL_TYPE = _itemMain.MEU_CONTROL_TYPE;
                            mainmenu.MEU_IMG = _itemMain.MEU_IMG;
                            mainmenu.MEU_URL =string.Format("{0}{1}", _itemMain.MEU_URL,(_itemMain.MEU_URL.ToUpper().IndexOf("DIRECTPAGE")>=0) ? ("?MEUROWID=" + _itemMain.MEU_ROW_ID) : "");
                            mainmenu.MEU_URL_DIRECT = _itemMain.MEU_URL_DIRECT;
                            mainmenu.MEU_LIST_NO = _itemMain.MEU_LIST_NO;
                            mainmenu.MEU_LEVEL = _itemMain.MEU_LEVEL;
                            List<MenuPermission> lstChild = new List<MenuPermission>();
                            List<MenuPermission> lstEvent = new List<MenuPermission>();
                            lstChild = GetChildMenu(_itemMain, (List<MENUModel>)MenuObj.ToList(), 2);
                            lstEvent = GetChildMenu(_itemMain, (List<MENUModel>)MenuObj.ToList(), 1);
                            mainmenu.MENU_CHILD = lstChild;
                            mainmenu.MENU_Event = lstEvent;
                            lstMainMenu.Add(mainmenu);
                        }

                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstMainMenu;
        }

        private List<MenuPermission> GetChildMenu(MENUModel itemMain, List<MENUModel> MenuObj, int MenuLevel)
        {
            List<MenuPermission> _lstPermission = new List<MenuPermission>();
            List<MENUModel> lstMain = MenuObj.Where(x => x.MEU_LEVEL == MenuLevel.ToString() && x.MEU_PARENT_ID == itemMain.MEU_ROW_ID ).ToList();
            foreach (var _itemMain in lstMain)
            {
                MenuPermission mainmenu = new MenuPermission();
                mainmenu.MEU_ROWID = _itemMain.MEU_ROW_ID;
                mainmenu.LNG_DESCRIPTION = _itemMain.LNG_DESCRIPTION;
                mainmenu.MEU_CONTROL_TYPE = _itemMain.MEU_CONTROL_TYPE;
                mainmenu.MEU_IMG = _itemMain.MEU_IMG;
                mainmenu.MEU_URL  = string.Format("{0}{1}", _itemMain.MEU_URL, (_itemMain.MEU_URL.ToUpper().IndexOf("DIRECTPAGE") >= 0) ? ("?MEUROWID="+_itemMain.MEU_ROW_ID) : "");
                mainmenu.MEU_URL_DIRECT = _itemMain.MEU_URL_DIRECT;
                mainmenu.MEU_LEVEL = _itemMain.MEU_LEVEL;
                mainmenu.MEU_LIST_NO = _itemMain.MEU_LIST_NO;
                List<MenuPermission> lstChild = new List<MenuPermission>();
                List<MenuPermission> lstEvent = new List<MenuPermission>();
                lstChild = GetChildMenu(_itemMain, MenuObj, MenuLevel+1);
                lstEvent = GetChildMenu(_itemMain, (List<MENUModel>)MenuObj.ToList(), MenuLevel);
                mainmenu.MENU_CHILD = lstChild;
                mainmenu.MENU_Event = lstEvent;
                _lstPermission.Add(mainmenu);
            }

            return _lstPermission;
        }

        #region ApproveFromEmail
        public List<userApprove> CheckPermissionToken(string sToken)
        {
            List<userApprove> uApprove = new List<userApprove>();

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var MenuObj = from ap in context.CPAI_APPROVE_TOKEN
                                  join u in context.USERS on ap.TOK_FK_USER equals u.USR_ROW_ID
                                  where u.USR_STATUS == "ACTIVE" && ap.TOK_TOKEN == sToken
                                  select new userApprove
                                  {
                                      UserName = u.USR_LOGIN,
                                      UserType = ap.TOK_USED_TYPE,
                                      TransactionID = ap.TOK_TRASACTION_ID
                                  };
                    if (MenuObj != null)
                    {
                        uApprove = MenuObj.ToList();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return uApprove;
        }
        #endregion

        public string GetSectionHead(string userName)
        {
            string UserSectionHead = "";
            using (var context = new EntityCPAIEngine())
            {
                var objUser = context.USERS.ToList();
                var objApprove = objUser.Where(x => x.USR_LOGIN.ToUpper() == userName.ToUpper()).ToList();
                if (objApprove != null && objApprove.Count > 0)
                {
                    var UserHead = objUser.Where(x => x.USR_ROW_ID.ToUpper() == objApprove[0].USR_SECTION_HEAD).ToList();
                    if (UserHead != null && UserHead.Count > 0)
                    {
                        UserSectionHead = UserHead[0].USR_LOGIN;
                    }
                }

            }
            return UserSectionHead;
        }

        public List<USERS> GetUserInfomation(string userName)
        {

            List<USERS> lstUser = new List<Entity.USERS>();
            using (var context = new EntityCPAIEngine())
            {
                lstUser = context.USERS.Where(x=>x.USR_LOGIN.ToUpper() == userName.ToUpper()).ToList();                

            }
            return lstUser;
        }

        public List<string> GetUserGroupSortBySystem(string userName,string system)
        {
            List<string> GROUP = new List<string>();
            using (var context = new EntityCPAIEngine())
            {
                var lstUser = context.USERS.Where(x => x.USR_LOGIN.ToUpper() == userName.ToUpper()).ToList().FirstOrDefault();
                GROUP = lstUser.CPAI_USER_GROUP.Where(x => x.USG_USER_SYSTEM == system).Select(x => x.USG_USER_GROUP).ToList();
            }
            return GROUP;
        }

        public string GetUserInfomationName(string userName)
        {
            string NameUser = "";
            var _lstUser = GetUserInfomation(userName);
            if (_lstUser!= null && _lstUser.Count > 0) { NameUser = _lstUser[0].USR_FIRST_NAME_EN; }
            return NameUser;
        }

        public string GetUserRoleType(string userName)
        {
            string ROL_TYPE = "";
            using (var context = new EntityCPAIEngine())
            {
                var lstUser = from r in context.ROLE
                              join ur in context.USER_ROLE on r.ROL_ROW_ID equals ur.URO_FK_ROLE
                              join u in context.USERS on ur.URO_FK_USER equals u.USR_ROW_ID
                              where u.USR_LOGIN.ToUpper() == userName.ToUpper() && u.USR_STATUS == "ACTIVE"
                              select r;
                if (lstUser.ToList().Count > 0) ROL_TYPE = lstUser.ToList()[0].ROL_TYPE;
            }
            return ROL_TYPE;
        }

        public string GetUserRoleName(string userName)
        {
            string ROL_NAME = "";
            using (var context = new EntityCPAIEngine())
            {
                var lstUser = from r in context.ROLE
                              join ur in context.USER_ROLE on r.ROL_ROW_ID equals ur.URO_FK_ROLE
                              join u in context.USERS on ur.URO_FK_USER equals u.USR_ROW_ID
                              where u.USR_LOGIN.ToUpper() == userName.ToUpper() && u.USR_STATUS == "ACTIVE"
                              select r;
                if (lstUser.ToList().Count > 0) ROL_NAME = lstUser.ToList()[0].ROL_NAME;
            }
            return ROL_NAME;
        }

        #region IDisposable Members

        //Implement IDisposable.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        //IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Free other state (managed objects).
            }
            // Free your own state (unmanaged objects).
            // Set large fields to null.
            disposed = true;
        }

        #endregion

        #region ConnectDB
        public void executeNoneQuery(OracleConnection conn, OracleTransaction trans, string cmd, OracleParameter[] param)
        {
            OracleCommand oracmd = new OracleCommand(cmd, conn);
            for (int i = 0; i <= param.Length - 1; i++)
            {
                oracmd.Parameters.Add(param[i]);
            }
            oracmd.ExecuteNonQuery();
        }

        #endregion
    }
}