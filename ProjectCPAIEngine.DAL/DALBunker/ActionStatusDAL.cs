﻿using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Service;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.DAL.Bunker
{
    public class ActionStatusDAL
    {
        public CPAI_ACTION_STATUS findActionStatus(string function, string status, string action, string next_status, string current_action)
        {
            FunctionService s = new FunctionService();
            FUNCTIONS f = s.GetFUNCTIONByFuncID(function);

            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                if(f != null)
                {
                    List<CPAI_ACTION_STATUS> results = entity.CPAI_ACTION_STATUS.Where(x => x.AST_CURRENT_STATUS == status
                                                                                       && x.AST_CURRENT_ACTION == action
                                                                                       && x.AST_VERIFY_ACTION == current_action
                                                                                       && x.AST_VERIFY_STATUS == next_status
                                                                                       && x.AST_FK_FUNCTION == f.FNC_ROW_ID 
                                                                                       && x.AST_STATUS == "ACTIVE"
                                                                                       ).ToList();
                    if (results != null && (results.Count() == 1))
                    {
                        return results[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
        }

    }
}