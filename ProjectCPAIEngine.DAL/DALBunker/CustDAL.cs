﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class CustDAL
    {
        public static List<MT_CUST_DATA> GetCustPaymentTerms(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_CUST_DATA
                             where v.MCS_FK_CUS == id 
                             select v).Distinct();
                return query.ToList();
            }
        }

        public static string getBrokerByCharterer(string id, string type = "BROKER")
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from c in context.MT_CUST_DETAIL
                             join p in context.MT_CUST_BROKER on c.MCD_FK_CUS equals p.MCB_FK_MT_CUST
                             join v in context.MT_VENDOR on p.MCB_FK_MT_VENDOR_BROKER equals v.VND_ACC_NUM_VENDOR
                             join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                             where vc.MVC_SYSTEM.ToUpper() == "CPAI"
                             && vc.MVC_TYPE.ToUpper() == type
                             && vc.MVC_STATUS.ToUpper() == "ACTIVE"
                             && c.MCD_ROW_ID == id
                             && p.MCB_STATUS == "ACTIVE"
                             orderby p.MCB_ORDER ascending
                             select new { v.VND_ACC_NUM_VENDOR, p.MCB_DEFAULT }).ToList();
                var vendor = query.Where(x => x.MCB_DEFAULT == "Y").FirstOrDefault();
                string vendor_name = vendor != null ? vendor.VND_ACC_NUM_VENDOR : query.FirstOrDefault() == null ? "" : query.FirstOrDefault().VND_ACC_NUM_VENDOR;
                return vendor_name;
            }
        }

        public static string GetPaymentTerm(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from p in context.MT_CUST_PAYMENT_TERM
                             where p.MCP_FK_MT_CUST == id
                             select p).FirstOrDefault();
                return query == null ? "" : query.MCP_PAYMENT_TERM;
            }
        }
    }
}
