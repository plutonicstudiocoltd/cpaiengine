﻿using com.pttict.engine.dal.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class ExtendTransactionDAL
    {
        public List<EXTEND_TRANSACTION> findByFtxRowId(string ftxRowId)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<EXTEND_TRANSACTION> results = entity.EXTEND_TRANSACTION.Where(x => x.ETX_FK_FUNC_TRX.ToUpper() == ftxRowId.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<EXTEND_TRANSACTION>();
                }
            }
        }

        public List<EXTEND_TRANSACTION> findByFtxRowIdAndKey(string ftxRowId, string key)
        {
            using (EntitiesEngine entity = new EntitiesEngine())
            {
                List<EXTEND_TRANSACTION> results = entity.EXTEND_TRANSACTION.Where(x => x.ETX_FK_FUNC_TRX.ToUpper() == ftxRowId.ToUpper() && x.ETX_KEY.ToUpper() == key.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<EXTEND_TRANSACTION>();
                }
            }
        }
    }
}
