﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class PortDAL
    {
        public static List<MT_PORT> GetPort(string system, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_PORT
                            join vc in context.MT_PORT_CONTROL on v.MLP_LOADING_PORT_ID equals vc.MMP_FK_PORT
                            where vc.MMP_SYSTEM.ToUpper() == system && vc.MMP_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_JETTY> GetPortJettyById(string row_id)
        {
            List<MT_JETTY> result = new List<MT_JETTY>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_JETTY where v.MTJ_ROW_ID == row_id
                             select new {
                                MTJ_PORT_TYPE = v.MTJ_PORT_TYPE,
                                MTJ_MT_COUNTRY = v.MTJ_MT_COUNTRY,
                                MTJ_LOCATION = v.MTJ_LOCATION,
                                MTJ_JETTY_NAME = v.MTJ_JETTY_NAME
                             });
                foreach (var value in query)
                {
                    MT_JETTY temp = new MT_JETTY();
                    temp.MTJ_PORT_TYPE = value.MTJ_PORT_TYPE;
                    temp.MTJ_MT_COUNTRY = value.MTJ_MT_COUNTRY;
                    temp.MTJ_LOCATION = value.MTJ_LOCATION;
                    temp.MTJ_JETTY_NAME = value.MTJ_JETTY_NAME;
                    result.Add(temp);
                }
                return result;
            }
        }

        public static List<MT_JETTY> GetPortData()
        {
            List<MT_JETTY> result = new List<MT_JETTY>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_JETTY
                            join vc in (from c in context.MT_COUNTRY where c.MCT_CLIENT == "900" select new { c.MCT_LAND1, c.MCT_LANDX }).Distinct() on v.MTJ_MT_COUNTRY equals vc.MCT_LAND1 into view
                            from vc in view.DefaultIfEmpty()
                            where v.MTJ_STATUS.ToUpper() == "ACTIVE"
                            select new 
                            {
                                MTJ_CREATED_BY  = v.MTJ_CREATED_BY
                                ,
                                MTJ_CREATED_DATE = v.MTJ_CREATED_DATE
                                ,
                                MTJ_CREATE_TYPE = v.MTJ_CREATE_TYPE
                                ,
                                MTJ_MT_COUNTRY = vc.MCT_LANDX
                                ,
                                MTJ_JETTY_NAME = v.MTJ_JETTY_NAME
                                ,
                                MTJ_LOCATION = v.MTJ_LOCATION
                                ,
                                MTJ_PORT_TYPE = v.MTJ_PORT_TYPE
                                ,
                                MTJ_ROW_ID = v.MTJ_ROW_ID
                                ,
                                MTJ_STATUS = v.MTJ_STATUS
                                ,
                                MTJ_UPDATED_BY = v.MTJ_UPDATED_BY
                                ,
                                MTJ_UPDATED_DATE = v.MTJ_UPDATED_DATE
                                ,
                                MT_JETTY_CHARGE = v.MT_JETTY_CHARGE

                            });

                foreach(var i in query)
                {
                    MT_JETTY temp = new MT_JETTY();
                    temp.MTJ_CREATED_BY = i.MTJ_CREATED_BY;
                    temp.MTJ_CREATED_DATE = i.MTJ_CREATED_DATE;
                    temp.MTJ_CREATE_TYPE = i.MTJ_CREATE_TYPE;
                    temp.MTJ_MT_COUNTRY = i.MTJ_MT_COUNTRY;
                    temp.MTJ_JETTY_NAME = i.MTJ_JETTY_NAME;
                    temp.MTJ_LOCATION = i.MTJ_LOCATION;
                    temp.MTJ_PORT_TYPE = i.MTJ_PORT_TYPE;
                    temp.MTJ_ROW_ID = i.MTJ_ROW_ID;
                    temp.MTJ_STATUS = i.MTJ_STATUS;
                    temp.MTJ_UPDATED_BY = i.MTJ_UPDATED_BY;
                    temp.MTJ_UPDATED_DATE = i.MTJ_UPDATED_DATE;
                    temp.MT_JETTY_CHARGE = i.MT_JETTY_CHARGE;
                    result.Add(temp);
                }    
                return result;
            }
        }
    }
}
