﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Model;

namespace ProjectCPAIEngine.DAL.DALBunker
{
    public class VendorDAL
    {
        public static List<MT_VENDOR> GetVendor(string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == "CPAI"
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_VENDOR> GetVendorCP(string type, string status)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            join cp in context.HEDG_MT_CP on v.VND_ACC_NUM_VENDOR equals cp.HMCP_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == "CPAI"
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static List<MT_VENDOR> GetVendor(string system,string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == system
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }
        public static MT_VENDOR GetVendorById(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_VENDOR> results = context.MT_VENDOR.Where(x => x.VND_ACC_NUM_VENDOR.ToUpper() == id.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_VENDOR();
                }
            }
        }
        public static List<MT_VENDOR> GetVendorPIT(string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == "PIT"
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        // Get Data Vendor Master and Color code
        public static List<VendorColor> GetVendorColor(string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where 
                            vc.MVC_SYSTEM.ToUpper() == "CPAI"
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select new VendorColor {
                                VENDOR = v,
                                 VENDOR_COLOR_CODE = vc.MVC_COLOR_CODE
                            };
                return query.ToList();
            }
        }

        // Get Data Vendor Master and Color code
        public static List<VendorColor> GetVendorColor(string system,string type, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == system
                            && vc.MVC_TYPE.ToUpper() == type
                            && vc.MVC_STATUS.ToUpper() == status
                            select new VendorColor
                            {
                                VENDOR = v,
                                VENDOR_COLOR_CODE = vc.MVC_COLOR_CODE
                            };
                return query.ToList();
            }
        }

        public static string GetVendorCommission(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_VENDOR
                            where v.VND_ACC_NUM_VENDOR == id
                            select v).FirstOrDefault();
                return query == null ? "" : query.VND_BROKER_COMMISSION;
            }
        }
    }
}
