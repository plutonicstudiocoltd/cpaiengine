﻿using com.pttict.engine.dal.Entity;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDO
{
    public class CDO_DATA_DAL
    {
        EntityCPAIEngine db = new EntityCPAIEngine();
        EntitiesEngine entity = new EntitiesEngine();

        public CDO_DATA GetWrap(string transaction_id)
        {
            try
            {
                EntityCPAIEngine context = new EntityCPAIEngine();
                context.Configuration.ProxyCreationEnabled = false;
                CDO_DATA result = context.CDO_DATA.Where(m => m.CDO_ROW_ID == transaction_id).FirstOrDefault();
                result.UpdateChild();

                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public CDO_DATA Get(string transaction_id)
        {
            CDO_DATA data = db.CDO_DATA.Where(m => m.CDO_ROW_ID == transaction_id).FirstOrDefault();
            return data;
        }

        public string GetStatus(string transaction_id)
        {
            CDO_DATA data = this.Get(transaction_id);
            return data.CDS_DATA.CDA_STATUS;
        }

        public CPAI_DATA_HISTORY GetLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY> data_histories = dthMan.findByDthTxnRef(id);
            return data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).FirstOrDefault();
        }

        public string GetLastStatus(string id)
        {
            CPAI_DATA_HISTORY dth = this.GetLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                CDO_DATA pd = JsonConvert.DeserializeObject<CDO_DATA>(json);
                return pd.CDS_DATA.CDA_STATUS;
            }
            catch (Exception e)
            {
                return "DRAFT";
            }
        }

        public List<CPAI_ACTION_BUTTON> GetButtonByUserID(string user_row_id)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            using (EntityCPAIEngine db = new EntityCPAIEngine())
            {
                result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID WHERE UPPER(CPAI_USER_GROUP.USG_FK_USERS) = :USER_ID AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID AND CPAI_USER_GROUP.USG_USER_SYSTEM = 'CDO'", new[] { new OracleParameter("USER_ID", user_row_id.ToUpper()), new OracleParameter("FUNCTION_ID", "87") }).ToList();
            }
            return result;
        }

        public List<CPAI_ACTION_BUTTON> GetButtons(string username)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            EntityCPAIEngine db = new EntityCPAIEngine();
            result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID JOIN USERS ON CPAI_USER_GROUP.USG_FK_USERS = USERS.USR_ROW_ID WHERE UPPER(USERS.USR_LOGIN) = :USR_LOGIN AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID AND CPAI_USER_GROUP.USG_USER_SYSTEM = 'CDO'", new[] { new OracleParameter("USR_LOGIN", username.ToUpper()), new OracleParameter("FUNCTION_ID", "87") }).ToList();
            return result;
        }

        public string ToDateFromToFormat(DateTime? from, DateTime? to)
        {
            string result = "-";
            if (from != null && to != null)
            {
                result = PAFHelper.ToOnlyDateString(from) + " - " + PAFHelper.ToOnlyDateString(to);
            }
            return result;
        }

        public string ShowTextTimeAndHrs(string text, decimal? hrs)
        {
            string hrs_str = string.Format("{0:n4} Hrs", hrs);
            string result = text;
            if (!String.IsNullOrEmpty(hrs_str))
            {
                result += " (" + hrs_str + ")";
            }
            return result;
        }

        public string ShowTextHrs(decimal? hrs)
        {
            string hrs_str = string.Format("{0:n4}", hrs);
            string result = string.Empty;
            if (!String.IsNullOrEmpty(hrs_str))
            {
                result = hrs_str;
            }
            return result;
        }

        public string ShowTextHrs(double hrs)
        {
            string hrs_str = string.Format("{0:n4}", hrs);
            string result = string.Empty;
            if (!String.IsNullOrEmpty(hrs_str))
            {
                result = hrs_str;
            }
            return result;
        }

        public string GetCompanyFromID(string com_code)
        {
            com_code = com_code ?? "";
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_COMPANY.Where(m => m.MCO_COMPANY_CODE == com_code).Select(m => m.MCO_SHORT_NAME).FirstOrDefault();
            result = result ?? "";
            return result;
        }

        public string GetVendorName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = "";
            MT_VENDOR vnd = db.MT_VENDOR.Where(m => m.VND_ACC_NUM_VENDOR == id).FirstOrDefault();
            if (vnd != null)
            {
                result += (vnd.VND_NAME1 ?? "") + " " + (vnd.VND_NAME2 ?? "");
                result = result.Trim();
            }
            return result;
        }

        public string GetCustomerName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = "";
            MT_CUST_DETAIL customer = db.MT_CUST_DETAIL.Where(m => m.MCD_FK_CUS == id).FirstOrDefault();
            if (customer != null)
            {
                result += (customer.MCD_NAME_1 ?? "") + " " + (customer.MCD_NAME_2 ?? "");
                result = result.Trim();
            }
            return result;
        }

        public string GetCounterpartyByTypeAndId(string type, string DDA_SHIP_OWNER, string DDA_BROKER, string DDA_SUPPLIER, string DDA_CUSTOMER, string DDA_CUSTOMER_BROKER)
        {
            string result = "";
            type = type ?? "";
            type = type.ToLower();
            switch (type)
            {
                case "ship owner":
                    DDA_SHIP_OWNER = DDA_SHIP_OWNER ?? "";
                    if (!String.IsNullOrEmpty(DDA_SHIP_OWNER.Trim()))
                    {
                        result += DDA_SHIP_OWNER + " (Ship Owner)";
                    }
                    string broker = this.GetVendorName(DDA_BROKER);
                    if (!String.IsNullOrEmpty(broker.Trim()))
                    {
                        result += " via " + broker + " (Broker)";
                    }
                    break;
                case "supplier as charterer":
                    string vendor = this.GetVendorName(DDA_SUPPLIER);
                    if (!String.IsNullOrEmpty(vendor.Trim()))
                    {
                        result = vendor + " (Supplier as charterer)";
                    }
                    break;
                case "customer":
                    string customer = this.GetCustomerName(DDA_CUSTOMER);
                    if (!String.IsNullOrEmpty(customer.Trim()))
                    {
                        result = customer + " (Customer)";
                    }
                    string customer_broker = this.GetVendorName(DDA_CUSTOMER_BROKER);
                    if (!String.IsNullOrEmpty(customer_broker.Trim()))
                    {
                        result += " via " + customer_broker + " (Broker)";
                    }
                    break;
                default:
                    break;
            }

            return result;
            //EntityCPAIEngine db = new EntityCPAIEngine();
            //string result = db.MT_COMPANY.Where(m => m.MCO_COMPANY_CODE == com_code).Select(m => m.MCO_SHORT_NAME).FirstOrDefault();
            //result = result ?? "";
            //return result;
        }

        public string ShowDemurrageType(string type, string for_company)
        {
            string result = string.Empty;
            if (type == "Paid")
            {
                result = "Paid to Counterparty";
            }
            else
            {
                result = "Received from Counterparty";
            }

            return result;
        }

        public string GetVesselName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_VEHICLE.Where(m => m.VEH_ID == id).Select(m => m.VEH_VEH_TEXT).FirstOrDefault();
            result = result ?? "";
            return result;
        }

        public string GetMaterialName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_MATERIALS.Where(m => m.MET_NUM == id).Select(m => m.MET_MAT_DES_ENGLISH).FirstOrDefault();
            return result;
        }

        public string GetCountry(string land1)
        {
            //MT_COUNTRY landx = context.MT_COUNTRY.Where(m => (m.MCT_LAND1 ?? "").ToLower() == (land1 ?? "").ToLower()).FirstOrDefault();
            EntityCPAIEngine context = new EntityCPAIEngine();

            string landx = context.Database.SqlQuery<string>("SELECT DISTINCT MCT_LANDX FROM MT_COUNTRY WHERE MCT_LAND1 = '" + land1 + "'").FirstOrDefault();
            return landx ?? "";
        }

        //public string GetPortName(CDO_PORT e)
        //{
        //    //return e.DPT_LOAD_PORT_NAME;
        //    if (e.MT_PORT != null && e.MT_PORT.MLP_LOADING_PORT_NAME != null)
        //    {
        //        return e.MT_PORT.MLP_LOADING_PORT_NAME;
        //    }
        //    else if (e.MT_JETTY != null)
        //    {
        //        string jetty_name = (e.MT_JETTY.MTJ_JETTY_NAME ?? "").Trim();
        //        if (jetty_name.ToLower() == "other")
        //        {
        //            jetty_name = "";
        //        }

        //        return (jetty_name ?? "") + " " + (e.MT_JETTY.MTJ_LOCATION ?? "") + " " + (this.GetCountry(e.MT_JETTY.MTJ_MT_COUNTRY) ?? "");
        //    }
        //    return "";
        //}

        //public string GetProductOrCrude(List<CDO_MATERIAL> mats)
        //{
        //    List<string> products = new List<string>();
        //    foreach (CDO_MATERIAL mat in mats)
        //    {
        //        string mat_name = this.GetMaterialName(mat.DMT_MET_NUM);
        //        if (mat_name != null)
        //        {
        //            products.Add(mat_name);
        //        }
        //    }
        //    return String.Join(", ", products);
        //}

        public void InsertOrUpdate(string status, CDO_DATA data, string id, string username)
        {
            //DateTime now = DateTime.Now;
            //EntityCPAIEngine context = new EntityCPAIEngine();
            //EntityCPAIEngine db = new EntityCPAIEngine();
            //CDO_DATA old = db.CDO_DATA.Where(m => m.CDO_ROW_ID == id).FirstOrDefault();

            //try
            //{
            //    context.Database.ExecuteSqlCommand("DELETE FROM CDO_DATA WHERE CDO_ROW_ID = :DDA_ROW_ID", new[] { new OracleParameter("CDO_ROW_ID", id) });
            //}
            //catch (Exception e)
            //{

            //}

            //CDO_DATA pd = data;
            //pd.CDO_DOC_DATE = ((DateTime)pd.CDO_DOC_DATE);
            //if (old != null)
            //{
            //    pd.CDO_UPDATED = now;
            //    pd.CDO_UPDATED_BY = username;
            //    pd.CDO_CREATED = old.CDO_CREATED ?? pd.CDO_CREATED;
            //    pd.CDO_CREATED_BY = old.CDO_CREATED_BY ?? pd.CDO_CREATED_BY;
            //}
            //else
            //{
            //    pd.CDO_CREATED = now;
            //    pd.CDO_CREATED_BY = username;
            //    pd.CDO_UPDATED = now;
            //    pd.CDO_UPDATED_BY = username;
            //}

            //pd.CDO_ROW_ID = id;
            //pd.CDO_STATUS = status;

            //foreach (CDO_BIDDING_ITEMS e in pd.CDO_BIDDING_ITEMS)
            //{
            //    e.CBI_ROW_ID = PAFHelper.GetSeqId("CDO_BIDDING_ITEMS");
            //    e.CBI_FK_CDO_DATA = pd.CDO_ROW_ID;

            //    e.CBI_CREATED = e.CBI_CREATED ?? now;
            //    e.CBI_CREATED_BY = e.CBI_CREATED_BY ?? username;
            //    e.CBI_UPDATED = now;
            //    e.CBI_UPDATED_BY = username;

            //    int c = 1;
            //    foreach (CDO_ROUND r in e.CDO_ROUND)
            //    {
            //        r.CRN_ROW_ID = PAFHelper.GetSeqId("CDO_ROUND");
            //        r.CRN_COUNT = c++;
            //        r.CRN_FK_CDO_BIDDING_ITEMS = e.CBI_ROW_ID;

            //        r.CRN_CREATED = r.CRN_CREATED ?? now;
            //        r.CRN_CREATED_BY = r.CRN_CREATED_BY ?? username;
            //        r.CRN_UPDATED = now;
            //        r.CRN_UPDATED_BY = username;
            //    }
            //}

            //foreach (CDO_CRUDE_PURCHASE_DETAIL e in pd.CDO_CRUDE_PURCHASE_DETAIL)
            //{
            //    e.CPD_ROW_ID = PAFHelper.GetSeqId("CDO_CRUDE_PURCHASE_DETAIL");
            //    e.CPD_FK_CDO_DATA = pd.CDO_ROW_ID;

            //    e.CPD_CREATED = e.CPD_CREATED ?? now;
            //    e.CPD_CREATED_BY = e.CPD_CREATED_BY ?? username;
            //    e.CPD_UPDATED = now;
            //    e.CPD_UPDATED_BY = username;
            //}


            //foreach (CDO_PROPOSAL_ITEMS e in pd.CDO_PROPOSAL_ITEMS)
            //{
            //    e.CSI_ROW_ID = PAFHelper.GetSeqId("CDO_PROPOSAL_ITEMS");
            //    e.CSI_FK_CDO_DATA = pd.CDO_ROW_ID;

            //    e.CSI_CREATED = e.CSI_CREATED ?? now;
            //    e.CSI_CREATED_BY = e.CSI_CREATED_BY ?? username;
            //    e.CSI_UPDATED = now;
            //    e.CSI_UPDATED_BY = username;
            //}

            //foreach (CDO_ATTACH_FILE e in pd.CDO_ATTACH_FILE)
            //{
            //    e.CAT_ROW_ID = PAFHelper.GetSeqId("CDO_ATTACH_FILE");
            //    e.CAT_FK_CDO_DATA = pd.CDO_ROW_ID;

            //    e.CAT_CREATED = e.CAT_CREATED ?? now;
            //    e.CAT_CREATED_BY = e.CAT_CREATED_BY ?? username;
            //    e.CAT_UPDATED = now;
            //    e.CAT_UPDATED_BY = username;
            //}

            //if ((pd.CDO_STATUS ?? "").StartsWith("WAITING"))
            //{
            //    pd.CDO_REASON = "-";
            //}
            //context.CDO_DATA.Add(pd);
            //try
            //{
            //    context.SaveChanges();
            //}
            //catch (DbUpdateException ex)
            //{
            //    try
            //    {
            //        PAFHelper.log(ex);
            //    }
            //    catch
            //    {

            //    }
            //    context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
            //    try
            //    {
            //        context.SaveChanges();
            //    }
            //    catch (DbUpdateException fex)
            //    {
            //        PAFHelper.log(fex);
            //    }
            //}
            //catch (Exception e)
            //{
            //    PAFHelper.log(e);
            //}
        }

        #region setDataHistory
        public void CreateDataHistory(CDO_DATA data, string current_action, string user, string note, string ftx_row_id, bool is_reject = false)
        {
            string transaction_id = data.TRAN_ID ?? data.CDO_ROW_ID;
            CDO_DATA data2 = this.GetWrap(transaction_id);
            string json = "";
            if (data2 != null)
            {
                json = JsonConvert.SerializeObject(data2);
            }
            else
            {
                json = JsonConvert.SerializeObject(data);
            }
            //add data history
            DataHistoryDAL dthMan = new DataHistoryDAL();
            DateTime now = DateTime.Now;
            CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
            dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
            dataHistory.DTH_ACTION = current_action;
            dataHistory.DTH_ACTION_DATE = now;
            dataHistory.DTH_ACTION_BY = user;
            dataHistory.DTH_NOTE = note;
            dataHistory.DTH_JSON_DATA = json;
            dataHistory.DTH_TXN_REF = ftx_row_id;
            dataHistory.DTH_REJECT_FLAG = is_reject ? "Y" : "N";
            dataHistory.DTH_CREATED_BY = user;
            dataHistory.DTH_CREATED_DATE = now;
            dataHistory.DTH_UPDATED_BY = user;
            dataHistory.DTH_UPDATED_DATE = now;
            dthMan.Save(dataHistory);
        }
        #endregion
        public CPAI_DATA_HISTORY GetBeforeLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY> data_histories = dthMan.findByDthTxnRef(id);
            data_histories = data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).ToList();
            if (data_histories.Count > 1)
            {
                return data_histories[1];
            }
            return null;
        }

        public string GetLastAction(string id)
        {
            CPAI_DATA_HISTORY dth = this.GetBeforeLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                PAF_DATA pd = JsonConvert.DeserializeObject<PAF_DATA>(json);
                return pd.PDA_STATUS;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<string> GetRelatedGroupsFromTransactionId(string transaction_id)
        {
            List<string> result = new List<string>();
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT cug.USG_USER_GROUP FROM PAF_DATA PD LEFT JOIN USERS U ON UPPER(PD.PDA_CREATED_BY) = UPPER(U.USR_LOGIN) JOIN CPAI_USER_GROUP cug ON cug.USG_FK_USERS = U.USR_ROW_ID WHERE PD.PDA_ROW_ID = :PDA_ROW_ID";
            string group = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("PDA_ROW_ID", transaction_id.Trim()) }).FirstOrDefault();
            if (group.StartsWith("CMPS_INTER"))
            {
                result.Add("CMPS_INTER");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_DOM"))
            {
                result.Add("CMPS_DOM");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_SH"))
            {
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMLA"))
            {
                result.Add("CMLA");
                result.Add("CMLA_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            return result;
        }

    }
}
