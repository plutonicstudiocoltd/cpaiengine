﻿using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDO
{
    public class CDO_MAIL_CONTENT
    {
        public UsersDAL userMan = new UsersDAL();
        CDO_DATA_DAL dafMan = new CDO_DATA_DAL();
        public USERS user;
        public CDO_DATA model;
        public string transaction_id = "";

        public CDO_MAIL_CONTENT()
        {

        }

        public CDO_MAIL_CONTENT(USERS user, string transaction_id)
        {
            CDO_DATA_DAL dafMan = new CDO_DATA_DAL();
            this.user = user;
            this.transaction_id = transaction_id;
            this.model = dafMan.Get(transaction_id);
        }

        public string GetNameFromUserLogin(string userlogin)
        {
            List<USERS> users = userMan.findByLogin(userlogin);
            USERS user = users.FirstOrDefault();
            if (user == null)
            {
                return "";
            }
            string action_user = (user.USR_FIRST_NAME_EN ?? "") + " " + (user.USR_LAST_NAME_EN ?? "");
            return action_user;
        }

        public string GetSubject(string subject)
        {
            string doc_no = this.model.CDO_FORM_ID;
            string action_user = (this.user.USR_FIRST_NAME_EN ?? "") + " " + (this.user.USR_LAST_NAME_EN ?? "");
            subject = subject
                        .Replace("Product sale Approval Form", "")
                        .Replace("#doc_no", doc_no)
                        .Replace("#action_user", action_user);

            return subject;
        }

        public string GetBody(string body)
        {
            string result = body;
            string doc_no = this.model.CDO_FORM_ID;
            string action_user = this.GetNameFromUserLogin(this.user.USR_LOGIN);
            //string reason = this.model.CDO_REASON ?? "";
            string create_by = this.GetNameFromUserLogin(PAF_DATA_DAL.GetSubmittedBy(this.transaction_id));
            //string incoterm = this.model. ?? "-";
            //string counterparty = dafMan.GetCounterpartyByTypeAndId(this.model.DDA_COUNTERPARTY_TYPE, this.model.DDA_SHIP_OWNER, this.model.DDA_BROKER, this.model.DDA_SUPPLIER, this.model.DDA_CUSTOMER, this.model.DDA_CUSTOMER_BROKER);
            //string vesselname = dafMan.GetVesselName(this.model.DDA_VESSEL);

            //#region --- demurrage
            //string user_group = this.model.DDA_USER_GROUP;
            //string from_id = this.model.DDA_FORM_ID;
            //string type_of_transaction = this.model.DDA_TYPE_OF_TRANSECTION;
            //string demurrage_type = dafMan.ShowDemurrageType(this.model.DDA_DEMURAGE_TYPE, for_company); // this.model.DDA_DEMURAGE_TYPE;
            //string vessel_name = dafMan.GetVesselName(this.model.DDA_VESSEL);
            //string product_crude = dafMan.GetProductOrCrude(this.model.CDO_MATERIAL.ToList());
            ////string cp_date_contract_date = PAFHelper.ToDateString(this.model.DDA_CONTRACT_DATE);
            //string cp_date_contract_date = this.model.SHOW_DDA_CONTRACT_DATE;
            //string agreed_loading_laycan = dafMan.ToDateFromToFormat(this.model.DDA_AGREED_LOADING_LAYCAN_FROM, this.model.DDA_AGREED_LOADING_LAYCAN_TO);
            //string agreed_discharging_laycan = dafMan.ToDateFromToFormat(this.model.DDA_AGREED_DC_LAYCAN_FROM, this.model.DDA_AGREED_DC_LAYCAN_TO);
            //string agreed_laycan = dafMan.ToDateFromToFormat(this.model.DDA_AGREED_LAYCAN_FROM, this.model.DDA_AGREED_LAYCAN_TO);
            //string time_on_demurrage = dafMan.ShowTextTimeAndHrs(this.model.DDA_TIME_ON_DEM_TEXT, this.model.DDA_TIME_ON_DEM_HRS);
            //string contract_laytime = dafMan.ShowTextHrs(this.model.DDA_LAYTIME_CONTRACT_HRS);

            //double nts = (double)this.model.DDA_NTS_SETTLED_HRS;
            //nts = Math.Floor(nts * 10000) / 10000;
            //string actual_net_laytime = dafMan.ShowTextHrs(nts);
            //string dem_settled = string.Format("{0:n2}", this.model.DDA_NET_PAYMENT_DEM_VALUE);
            //string actual_dem_settled = string.Format("{0:n2}", this.model.DDA_NET_PAYMENT_DEM_VALUE_OV);

            //string price_unit = this.model.DDA_CURRENCY_UNIT;
            //string address_commission = "";
            //string demurrage_sharing_to_top = "";
            //string broker_commission = "";
            //string net_payment_demurrage_value = "";
            //string net_receiving_demurrage_value = "";
            //if (this.model.DDA_IS_ADDRESS_COMMISSION == "Y")
            //{
            //    address_commission = string.Format("{0:n2}", this.model.DDA_AC_SETTLED_VALUE);
            //}
            //if (this.model.DDA_IS_SHARING_TO_TOP == "Y")
            //{
            //    demurrage_sharing_to_top = string.Format("{0:n2}", this.model.DDA_DST_SETTLED_VALUE);
            //}
            //if (this.model.DDA_IS_BROKER_COMMISSION == "Y")
            //{
            //    broker_commission = string.Format("{0:n2}", this.model.DDA_BROKER_COMMISSION_VALUE);
            //}
            //if (this.model.DDA_DEMURAGE_TYPE == "Paid")
            //{
            //    net_payment_demurrage_value = string.Format("{0:n2}", this.model.DDA_NET_PAYMENT_DEM_VALUE);
            //}
            //else
            //{
            //    net_receiving_demurrage_value = string.Format("{0:n2}", this.model.DDA_NET_PAYMENT_DEM_VALUE);
            //}
            //#endregion

            //#region --- remove for PAID some label
            //if (this.model.DDA_DEMURAGE_TYPE == "Paid")
            //{
            //    body = body
            //        .Replace("<br>Agreed Laycan : #laycan", "");
            //}
            //#endregion


            //#region --- remove for PAID some label
            //if (this.model.DDA_DEMURAGE_TYPE == "Received")
            //{
            //    body = body
            //        .Replace("<br>Agreed Loading Laycan : #loading_laycan", "")
            //        .Replace("<br>Agreed Discharging Laycan : #discharging_laycan", "");
            //}
            //#endregion

            body = body.Replace("#doc_no", doc_no)
                    .Replace("#action_user", action_user)
                    //.Replace("#for_company", for_company)
                    //.Replace("#create_by", create_by)
                    //.Replace("#type_of_trans", type_of_trans)
                    //.Replace("#incoterm", incoterm)
                    //.Replace("#dem_type", demurrage_type)
                    //.Replace("#counterparty", counterparty)
                    //.Replace("#vessel_name", vesselname)
                    //.Replace("#cp_contract_date", cp_date_contract_date)
                    //.Replace("#loading_laycan", agreed_loading_laycan)
                    //.Replace("#discharging_laycan", agreed_discharging_laycan)
                    //.Replace("#laycan", agreed_laycan)
                    //.Replace("#crude_product", product_crude)
                    //.Replace("#time_on_dem", time_on_demurrage)
                    //.Replace("#address_com", address_commission)
                    //.Replace("#dem_sharing", demurrage_sharing_to_top)
                    //.Replace("#net_payment", net_payment_demurrage_value)
                    //.Replace("#net_receiving", net_receiving_demurrage_value)
                    //.Replace("#broker_com", broker_commission)
                    //.Replace("#contract_laytime", contract_laytime)
                    //.Replace("#actual_net_laytime", actual_net_laytime)
                    //.Replace("#dem_settled", dem_settled)
                    //.Replace("#price_unit", price_unit)
                    //.Replace("#actual_dem_settled", actual_dem_settled)

                    //.Replace("#reason", reason)
                    .ToString();
            #region --- received case
            //if (this.model.DDA_DEMURAGE_TYPE == "Received" && this.model.DDA_COUNTERPARTY_TYPE == "Supplier as charterer")
            //{
            //    body = body.Replace("settled", "sharing settled");
            //}
            //else
            //if (this.model.DDA_DEMURAGE_TYPE == "Received")
            //{
            //    body = body.Replace("settled", "received settled");
            //}
            //else
            //{
            //    body = body.Replace("settled", "paid settled");
            //}
            #endregion

            return body;
        }
    }
}
