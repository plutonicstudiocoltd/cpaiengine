﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_ATTACH_FILE_DAL
    {
        public void Save(CDP_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_ATTACH_FILE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CDP_ATTACH_FILE data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_ATTACH_FILE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CDP_ATTACH_FILE> GetFileByDataID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiFile = context.CDP_ATTACH_FILE.Where(x => x.CAF_FK_CDP_DATA.ToUpper() == ID.ToUpper()).ToList();
                    return _chiFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CDP_ATTACH_FILE> GetFileByDataID(string ID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiFile = context.CDP_ATTACH_FILE.Where(x => x.CAF_FK_CDP_DATA.ToUpper() == ID.ToUpper()).ToList();
                    if(string.IsNullOrEmpty(TYPE) != true)
                    {
                        _chiFile = _chiFile.Where(x => x.CAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _chiFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateAttachFile(CDP_ATTACH_FILE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _AttachFile = context.CDP_ATTACH_FILE.Where(x => x.CAF_ROW_ID.ToUpper().Equals(data.CAF_ROW_ID.ToUpper())
                                                                      && x.CAF_FK_CDP_DATA.ToUpper().Equals(data.CAF_FK_CDP_DATA.ToUpper())
                                                                      && x.CAF_TYPE.ToUpper().Equals(data.CAF_TYPE.ToUpper())
                                                                        ).ToList().FirstOrDefault();
                    if (_AttachFile != null)
                    {
                        _AttachFile.CAF_UPDATED = data.CAF_UPDATED;
                        _AttachFile.CAF_UPDATED_BY = data.CAF_UPDATED_BY;
                        _AttachFile.CAF_PATH = data.CAF_PATH;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
