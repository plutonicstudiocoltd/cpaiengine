﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_ROUND_COMPET_DAL
    {
        public void Save(CDP_ROUND_COMPET data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_ROUND_COMPET.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CDP_ROUND_COMPET data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_ROUND_COMPET.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CDP_ROUND_COMPET> GetByID(string CIC_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Round = context.CDP_ROUND_COMPET.Where(x => x.CRC_FK_ROUND_COMPET_ITEMS.ToUpper() == CIC_ROW_ID.ToUpper()).ToList();
                    return _Round;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
