﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDPH
{
    public class CDP_ROUND_COMPET_ITEMS_DAL
    {
        public void Save(CDP_ROUND_COMPET_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CDP_ROUND_COMPET_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(CDP_ROUND_COMPET_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CDP_ROUND_COMPET_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CDP_ROUND_COMPET_ITEMS> GetByID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Items = context.CDP_ROUND_COMPET_ITEMS.Where(x => x.CIC_FK_OFFER_COMPET_ITEMS.ToUpper() == ITEMID.ToUpper()).OrderBy(x => x.CIC_ROUND_NO).ToList();
                    return _Items;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
