﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDS
{
    public class CDS_CRUDE_REF_CRITERIA
    {
        public string FEEDSTOCK { get; set; }
        public string NAME { get; set; }
        public Nullable<DateTime> PURCHASE_DATE_FROM { get; set; }
        public Nullable<DateTime> PURCHASE_DATE_TO { get; set; }
        public string PURCHASE_NO { get; set; }
        public string SUPPLIER { get; set; }

    }
}
