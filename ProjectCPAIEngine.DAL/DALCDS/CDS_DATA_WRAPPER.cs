﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCDS
{
    //public class CDSButtonAction
    //{
    //    public string name { get; set; }
    //    public string page_url { get; set; }
    //    public string call_xml { get; set; }

    //}

    //public class CDS_DATA_WRAPPER
    //{
    //    #region attribute
    //    public string CDA_ROW_ID { get; set; }
    //    public string CDA_FORM_ID { get; set; }
    //    public Nullable<System.DateTime> CDA_DOC_DATE { get; set; }
    //    public Nullable<System.DateTime> CDA_TPC_PLAN { get; set; }
    //    public string CDA_SALE_TYPE { get; set; }
    //    public string CDA_SALE_METHOD { get; set; }
    //    public Nullable<decimal> CDA_OPTIMIZATION_SALE { get; set; }
    //    public Nullable<decimal> CDA_OPTIMIZATION_PURCHASE { get; set; }
    //    public string CDA_BRIEF { get; set; }
    //    public string CDA_REASON { get; set; }
    //    public string CDA_NOTE { get; set; }
    //    public string CDA_STATUS { get; set; }
    //    public Nullable<System.DateTime> CDA_CREATED { get; set; }
    //    public string CDA_CREATED_BY { get; set; }
    //    public Nullable<System.DateTime> CDA_UPDATED { get; set; }
    //    public string CDA_UPDATED_BY { get; set; }
    //    public string CDA_REQUEST_BY { get; set; }
    //    public virtual List<CDS_ATTACH_FILE> CDS_ATTACH_FILE { get; set; }
    //    public virtual List<CDS_BIDDING_ITEMS> CDS_BIDDING_ITEMS { get; set; }
    //    public virtual List<CDS_CRUDE_PURCHASE_DETAIL> CDS_CRUDE_PURCHASE_DETAIL { get; set; }
    //    //public virtual List<CDS_OPTIMIZATION_DETAIL> CDS_OPTIMIZATION_DETAIL { get; set; }
    //    public virtual List<CDS_PROPOSAL_ITEMS> CDS_PROPOSAL_ITEMS { get; set; }
    //    #endregion

    //    #region extra attrivute
    //    //Extra
    //    public bool IS_DISABLED { get; set; }
    //    public bool IS_DISABLED_NOTE { get; set; }
    //    public List<CDSButtonAction> Buttons { get; set; }
    //    #endregion


    //    public CDS_DATA_WRAPPER()
    //    {

    //    }

    //    public CDS_DATA_WRAPPER(CDS_DATA source)
    //    {
    //        #region set attribute
    //        this.CDA_ROW_ID = source.CDA_ROW_ID;
    //        this.CDA_FORM_ID = source.CDA_FORM_ID;
    //        this.CDA_DOC_DATE = source.CDA_DOC_DATE;
    //        this.CDA_TPC_PLAN = source.CDA_TPC_PLAN;
    //        this.CDA_SALE_TYPE = source.CDA_SALE_TYPE;
    //        this.CDA_SALE_METHOD = source.CDA_SALE_METHOD;
    //        //this.CDA_OPTIMIZATION_SALE = source.CDA_OPTIMIZATION_SALE;
    //        this.CDA_OPTIMIZATION_PURCHASE = source.CDA_OPTIMIZATION_PURCHASE;
    //        this.CDA_BRIEF = source.CDA_BRIEF;
    //        this.CDA_REASON = source.CDA_REASON;
    //        this.CDA_NOTE = source.CDA_NOTE;
    //        this.CDA_STATUS = source.CDA_STATUS;
    //        this.CDA_CREATED = source.CDA_CREATED;
    //        this.CDA_CREATED_BY = source.CDA_CREATED_BY;
    //        this.CDA_UPDATED = source.CDA_UPDATED;
    //        this.CDA_UPDATED_BY = source.CDA_UPDATED_BY;


    //        this.CDA_REQUEST_BY = source.CDA_REQUEST_BY;
    //        #endregion

    //        this.IS_DISABLED = source.CDA_STATUS != "DRAFT";
    //        this.IS_DISABLED_NOTE = (source.CDA_STATUS == "APPROVED" || source.CDA_STATUS == "CANCEL");

    //        EntityCPAIEngine db = new EntityCPAIEngine();
    //        db.Configuration.ProxyCreationEnabled = false;

    //        this.CDS_ATTACH_FILE = db.CDS_ATTACH_FILE.Where(x => x.CAT_FK_CDS_DATA == source.CDA_ROW_ID).OrderBy(x => x.CAT_ROW_ID).ToList();
    //        this.CDS_BIDDING_ITEMS = db.CDS_BIDDING_ITEMS.Where(x => x.CBI_FK_CDS_DATA == source.CDA_ROW_ID).OrderBy(x => x.CBI_ROW_ID).ToList();
    //        this.CDS_CRUDE_PURCHASE_DETAIL = db.CDS_CRUDE_PURCHASE_DETAIL.Where(x => x.CPD_FK_CDP_DATA == source.CDA_ROW_ID).OrderBy(x => x.CPD_ROW_ID).ToList();
    //        //this.CDS_OPTIMIZATION_DETAIL = db.CDS_OPTIMIZATION_DETAIL.Where(x => x.COD_FK_CDP_DATA == source.CDA_ROW_ID).OrderBy(x => x.COD_ROW_ID).ToList();
    //        this.CDS_PROPOSAL_ITEMS = db.CDS_PROPOSAL_ITEMS.Where(x => x.CSI_FK_CDS_DATA == source.CDA_ROW_ID).OrderBy(x => x.CSI_ROW_ID).ToList();
    //        //this.CDS_MATERIAL = db.CDS_MATERIAL.Where(x => x.DMT_FK_CDS_DATA == source.DDA_ROW_ID).OrderBy(x => x.DMT_ROW_ID).ToList();
    //        //this.CDS_PORT = db.CDS_PORT.Where(x => x.DPT_FK_CDS_DATA == source.DDA_ROW_ID).OrderBy(x => x.DPT_ROW_ID).ToList();

    //        //foreach (CDS_PORT e in this.CDS_PORT)
    //        //{
    //        //    EntityCPAIEngine db2 = new EntityCPAIEngine();
    //        //    db2.Configuration.ProxyCreationEnabled = false;
    //        //    e.CDS_DEDUCTION_ACTIVITY = db2.CDS_DEDUCTION_ACTIVITY.Where(x => x.DDD_FK_CDS_PORT == e.DPT_ROW_ID).OrderBy(x => x.DDD_ROW_ID).ToList();
    //        //}
    //    }
    //}
}
