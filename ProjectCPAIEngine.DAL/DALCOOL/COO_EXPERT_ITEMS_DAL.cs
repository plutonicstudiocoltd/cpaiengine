﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_EXPERT_ITEMS_DAL
    {
        public void Save(COO_EXPERT_ITEMS cooExpertItems, EntityCPAIEngine context)
        {
            try
            {
                context.COO_EXPERT_ITEMS.Add(cooExpertItems);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_EXPERT_ITEMS data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_EXPERT_ITEMS.Find(data.COEI_ROW_ID);
                #region Set Value
                _search.COEI_CAL_RESULT = data.COEI_CAL_RESULT;
                _search.COEI_CAL_SCORE = data.COEI_CAL_SCORE;
                //_search.COEI_WEIGHT_SCORE = data.COEI_CAL_SCORE;
                _search.COEI_UPDATED_BY = data.COEI_UPDATED_BY;
                _search.COEI_UPDATED = data.COEI_UPDATED;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_EXPERT_ITEMS WHERE COEI_FK_COO_EXPERT = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public COO_EXPERT_ITEMS GetByID(string COEI_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_EXPERT_ITEMS.Where(x => x.COEI_ROW_ID.ToUpper() == COEI_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<COO_EXPERT_ITEMS> GetAllByID(string COEX_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.COO_EXPERT_ITEMS.Where(x => x.COEI_FK_COO_EXPERT.ToUpper() == COEX_ROW_ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
