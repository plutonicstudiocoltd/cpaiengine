﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCOOL
{
    public class COO_PROCESSING_UNIT_DAL
    {
        public void Save(COO_PROCESSING_UNIT cooPro, EntityCPAIEngine context)
        {
            try
            {
                context.COO_PROCESSING_UNIT.Add(cooPro);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    throw ex.InnerException;
                }
                else
                {
                    throw ex;
                }

            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM COO_PROCESSING_UNIT WHERE CPU_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(COO_PROCESSING_UNIT data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.COO_PROCESSING_UNIT.Find(data.CPU_ROW_ID);
                #region Set Value
                _search.CPU_DATE = data.CPU_DATE;
                _search.CPU_CRUDE_ABBRV = data.CPU_CRUDE_ABBRV;
                _search.CPU_UNIT = data.CPU_UNIT;
                _search.CPU_UPDATED = data.CPU_UPDATED;
                _search.CPU_UPDATED_BY = data.CPU_UPDATED_BY;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
