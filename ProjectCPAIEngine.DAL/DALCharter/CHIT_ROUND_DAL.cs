﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_ROUND_DAL
    {
        public void Save(CHIT_ROUND data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_ROUND.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHIT_ROUND data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_ROUND.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHIT_ROUND> GetRoundByRoundItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiRound = context.CHIT_ROUND.Where(x => x.ITIR_FK_ROUND_ITEMS.ToUpper() == ITEMID.ToUpper()).OrderBy(x => x.ITIR_ORDER).ToList();
                    return _chiRound;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
