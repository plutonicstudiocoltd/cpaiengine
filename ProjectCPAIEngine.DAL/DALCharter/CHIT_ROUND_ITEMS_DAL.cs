﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHIT_ROUND_ITEMS_DAL
    {
        public void Save(CHIT_ROUND_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHIT_ROUND_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(CHIT_ROUND_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHIT_ROUND_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CHIT_ROUND_ITEMS> GetRoundByOfferItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiItems = context.CHIT_ROUND_ITEMS.Where(x => x.ITRI_FK_OFFER_ITEMS.ToUpper() == ITEMID.ToUpper()).OrderBy(x => x.ITRI_ROUND_NO).ToList();
                    return _chiItems;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
