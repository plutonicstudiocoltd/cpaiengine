﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
   public class CHI_OFFER_ITEMS_DAL
    {
        public void Save(CHI_OFFER_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_OFFER_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHI_OFFER_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_OFFER_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHI_OFFER_ITEMS> GetOfferItemByDataID(string CHIID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiOffer = context.CHI_OFFER_ITEMS.Where(x => x.IOI_FK_CHI_DATA.ToUpper() == CHIID.ToUpper()).ToList();
                    return _chiOffer;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
