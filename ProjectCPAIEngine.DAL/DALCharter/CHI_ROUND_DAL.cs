﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHI_ROUND_DAL
    {
        public void Save(CHI_ROUND data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_ROUND.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHI_ROUND data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_ROUND.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHI_ROUND> GetRoundByRoundItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiRound = context.CHI_ROUND.Where(x => x.IIR_FK_ROUND_ITEMS.ToUpper() == ITEMID.ToUpper()).OrderBy(x => x.IRR_ORDER).ToList();
                    return _chiRound;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
