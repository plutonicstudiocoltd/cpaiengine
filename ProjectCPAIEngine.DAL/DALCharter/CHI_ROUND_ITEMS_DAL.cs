﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHI_ROUND_ITEMS_DAL
    {
        public void Save(CHI_ROUND_ITEMS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHI_ROUND_ITEMS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Save(CHI_ROUND_ITEMS data, EntityCPAIEngine context)
        {
            try
            {
                context.CHI_ROUND_ITEMS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CHI_ROUND_ITEMS> GetRoundByOfferItemID(string ITEMID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chiItems = context.CHI_ROUND_ITEMS.Where(x => x.IRI_FK_OFFER_ITEMS.ToUpper() == ITEMID.ToUpper()).OrderBy(x => x.IRI_ROUND_NO).ToList();
                    return _chiItems;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
