﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALCharter
{
    public class CHOT_PORT_DAL
    {
        public void Save(CHOT_PORT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CHOT_PORT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CHOT_PORT data, EntityCPAIEngine context)
        {
            try
            {
                context.CHOT_PORT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CHOT_PORT> GetPortByDataID(string CHOTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _choPort = context.CHOT_PORT.Where(x => x.OTDP_FK_CHOT_DATA.ToUpper() == CHOTID.ToUpper()).ToList();
                    return _choPort;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
