﻿using ProjectCPAIEngine.DAL.DALPAF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class CHO_CARGOS
    {
        public string TRIP_NO { get; set; }
        public string MAT_NAME { get; set; }
        public string MAT_NUM { get; set; }
    }

    public class CHO_PORTS
    {
        public string TRIP_NO { get; set; }
        public string BROKER { get; set; }
        public string BROKER_NAME { get; set; }
        public string LAYTIME_HRS { get; set; }
        public string UNIT { get; set; }
        public string FDA_DEM { get; set; }
        public Nullable<DateTime> LAYCAN_FROM { get; set; }
        public Nullable<DateTime> LAYCAN_TO { get; set; }
        public string PORT_TYPE { get; set; }
        public string PORT_ID { get; set; }
        public string LOADING_PORT { get; set; }
    }

    public class CHO_DATA
    {
        public string TRIP_NO { get; set; }
        public string SHIP_OWNER { get; set; }

        public Nullable<DateTime> DOC_DATE { get; set; }
        public string SHOW_DOC_DATE {
            get
            {
                return PAFHelper.ToOnlyDateString(this.DOC_DATE);
            }
        }
        public string BROKER_CODE
        {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0 && this.CHO_PORTS[0] != null)
                {
                    return this.CHO_PORTS[0].BROKER;
                }

                return "-";
            }
        }
        public string BROKER_NAME { get; set; }

        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }

        public string CUSTOMER_CODE { get; set; }
        public string CUSTOMER_NAME { get; set; }


        public string VESSEL_CODE { get; set; }
        public string VESSEL_NAME { get; set; }


        public string SHOW_LAYCAN_DATE
        {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0 && this.CHO_PORTS[0] != null)
                {
                    return PAFHelper.ToOnlyDateString(this.CHO_PORTS[0].LAYCAN_FROM) + " - " + PAFHelper.ToOnlyDateString(this.CHO_PORTS[0].LAYCAN_TO);
                }

                return "-";
            }
        }

        public Nullable<DateTime> LAYCAN_FROM {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0 && this.CHO_PORTS[0] != null)
                {
                    return this.CHO_PORTS[0].LAYCAN_FROM;
                }

                return null;
            }
        }

        public Nullable<DateTime> LAYCAN_TO {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0 && this.CHO_PORTS[0] != null)
                {
                    return this.CHO_PORTS[0].LAYCAN_TO;
                }

                return null;
            }
        }

        public Nullable<DateTime> CP_DATE { get; set; }

        public string CRUDE_NAMES { get; set; }

        public string CREATED_BY { get; set; }


        public string LAYTIME
        {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0)
                {
                    return this.CHO_PORTS[0].LAYTIME_HRS;
                }
                return null;
            }
        }
        public string DEMURRAGE
        {
            get
            {
                if (this.CHO_PORTS != null && this.CHO_PORTS.Count() > 0)
                {
                    return this.CHO_PORTS[0].FDA_DEM;
                }
                return null;
            }
        }

        public List<CHO_CARGOS> CHO_CARGOS { get; set; }
        public List<CHO_PORTS> CHO_PORTS { get; set; }
        public List<CHO_PORTS> CHO_PORTS_D { get; set; }
        public List<CHO_PORTS> CHO_PORTS_L { get; set; }
    }
}
