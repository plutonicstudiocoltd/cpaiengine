﻿using com.pttict.engine.dal.Entity;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_DATA_DAL
    {
        EntityCPAIEngine db = new EntityCPAIEngine();
        EntitiesEngine entity = new EntitiesEngine();

        public DAF_DATA_WRAPPER GetWrap(string transaction_id)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                DAF_DATA data = db.DAF_DATA.Where(m => m.DDA_ROW_ID == transaction_id).FirstOrDefault();
                DAF_DATA_WRAPPER result = new DAF_DATA_WRAPPER(data);
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public DAF_DATA Get(string transaction_id)
        {
            DAF_DATA data = db.DAF_DATA.Where(m => m.DDA_ROW_ID == transaction_id).FirstOrDefault();
            return data;
        }

        public string GetStatus(string transaction_id)
        {
            DAF_DATA data = this.Get(transaction_id);
            return data.DDA_STATUS;
        }

        public CPAI_DATA_HISTORY GetLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY> data_histories = dthMan.findByDthTxnRef(id);
            return data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).FirstOrDefault();
        }

        public string GetLastStatus(string id)
        {
            CPAI_DATA_HISTORY dth = this.GetLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                DAF_DATA pd = JsonConvert.DeserializeObject<DAF_DATA>(json);
                return pd.DDA_STATUS;
            }
            catch (Exception e)
            {
                return "DRAFT";
            }
        }

        public List<CPAI_ACTION_BUTTON> GetButtonByUserID(string user_row_id)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            using (EntityCPAIEngine db = new EntityCPAIEngine())
            {
                result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID WHERE UPPER(CPAI_USER_GROUP.USG_FK_USERS) = :USER_ID AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID AND CPAI_USER_GROUP.USG_USER_SYSTEM = 'DAF'", new[] { new OracleParameter("USER_ID", user_row_id.ToUpper()), new OracleParameter("FUNCTION_ID", "80") }).ToList();
            }
            return result;
        }

        public List<CPAI_ACTION_BUTTON> GetButtons(string username)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            EntityCPAIEngine db = new EntityCPAIEngine();
            result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID JOIN USERS ON CPAI_USER_GROUP.USG_FK_USERS = USERS.USR_ROW_ID WHERE UPPER(USERS.USR_LOGIN) = :USR_LOGIN AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID AND CPAI_USER_GROUP.USG_USER_SYSTEM = 'DAF'", new[] { new OracleParameter("USR_LOGIN", username.ToUpper()), new OracleParameter("FUNCTION_ID", "80") }).ToList();
            return result;
        }

        public string ToDateFromToFormat(DateTime? from, DateTime? to)
        {
            string result = "-";
            if (from != null && to != null)
            {
               result = PAFHelper.ToOnlyDateString(from) + " - " + PAFHelper.ToOnlyDateString(to);
            }
            return result;
        }

        public string ShowTextTimeAndHrs(string text, decimal? hrs)
        {
            string hrs_str = string.Format("{0:n4} Hrs", hrs);
            string result = text;
            if (!String.IsNullOrEmpty(hrs_str)) {
                result += " (" + hrs_str + ")";
            }
            return result;
        }

        public string ShowTextHrs(decimal? hrs)
        {
            string hrs_str = string.Format("{0:n4}", hrs);
            string result = string.Empty;
            if (!String.IsNullOrEmpty(hrs_str))
            {
                result = hrs_str;
            }
            return result;
        }

        public string ShowTextHrs(double hrs)
        {
            string hrs_str = string.Format("{0:n4}", hrs);
            string result = string.Empty;
            if (!String.IsNullOrEmpty(hrs_str))
            {
                result = hrs_str;
            }
            return result;
        }

        public string GetCompanyFromID(string com_code)
        {
            com_code = com_code ?? "";
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_COMPANY.Where(m => m.MCO_COMPANY_CODE == com_code).Select(m => m.MCO_SHORT_NAME).FirstOrDefault();
            result = result ?? "";
            return result;
        }

        public string GetVendorName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = "";
            MT_VENDOR vnd = db.MT_VENDOR.Where(m => m.VND_ACC_NUM_VENDOR == id).FirstOrDefault();
            if(vnd != null)
            {
                result += (vnd.VND_NAME1 ?? "") + " " + (vnd.VND_NAME2 ?? "");
                result = result.Trim();
            }
            return result;
        }

        public string GetCustomerName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = "";
            MT_CUST_DETAIL customer = db.MT_CUST_DETAIL.Where(m => m.MCD_FK_CUS == id).FirstOrDefault();
            if (customer != null)
            {
                result += (customer.MCD_NAME_1 ?? "") + " " + (customer.MCD_NAME_2 ?? "");
                result = result.Trim();
            }
            return result;
        }

        public string GetCounterpartyByTypeAndId(string type, string DDA_SHIP_OWNER, string DDA_BROKER, string DDA_SUPPLIER, string DDA_CUSTOMER, string DDA_CUSTOMER_BROKER)
        {
            string result = "";
            type = type ?? "";
            type = type.ToLower();
            switch (type)
            {
                case "ship owner":
                    DDA_SHIP_OWNER = DDA_SHIP_OWNER ?? "";
                    if (!String.IsNullOrEmpty(DDA_SHIP_OWNER.Trim()))
                    {
                        result += DDA_SHIP_OWNER + " (Ship Owner)";
                    }
                    string broker = this.GetVendorName(DDA_BROKER);
                    if (!String.IsNullOrEmpty(broker.Trim()))
                    {
                        result += " via " + broker + " (Broker)";
                    }
                    break;
                case "supplier as charterer":
                    string vendor = this.GetVendorName(DDA_SUPPLIER);
                    if (!String.IsNullOrEmpty(vendor.Trim()))
                    {
                        result = vendor + " (Supplier as charterer)";
                    }
                    break;
                case "customer":
                    string customer = this.GetCustomerName(DDA_CUSTOMER);
                    if (!String.IsNullOrEmpty(customer.Trim()))
                    {
                        result = customer + " (Customer)";
                    }
                    string customer_broker = this.GetVendorName(DDA_CUSTOMER_BROKER);
                    if (!String.IsNullOrEmpty(customer_broker.Trim()))
                    {
                        result += " via " + customer_broker + " (Broker)";
                    }
                    break;
                default:
                    break;
            }

            return result;
            //EntityCPAIEngine db = new EntityCPAIEngine();
            //string result = db.MT_COMPANY.Where(m => m.MCO_COMPANY_CODE == com_code).Select(m => m.MCO_SHORT_NAME).FirstOrDefault();
            //result = result ?? "";
            //return result;
        }

        public string ShowDemurrageType(string type, string for_company)
        {
            string result = string.Empty;
            if (type == "Paid")
            {
                result = "Paid to Counterparty";
            }
            else
            {
                result = "Received from Counterparty";
            }

            return result;
        }

        public string GetVesselName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_VEHICLE.Where(m => m.VEH_ID == id).Select(m => m.VEH_VEH_TEXT).FirstOrDefault();
            result = result ?? "";
            return result;
        }

        public string GetMaterialName(string id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            string result = db.MT_MATERIALS.Where(m => m.MET_NUM == id).Select(m => m.MET_MAT_DES_ENGLISH).FirstOrDefault();
            return result;
        }

        public string GetCountry(string land1)
        {
            //MT_COUNTRY landx = context.MT_COUNTRY.Where(m => (m.MCT_LAND1 ?? "").ToLower() == (land1 ?? "").ToLower()).FirstOrDefault();
            EntityCPAIEngine context = new EntityCPAIEngine();

            string landx = context.Database.SqlQuery<string>("SELECT DISTINCT MCT_LANDX FROM MT_COUNTRY WHERE MCT_LAND1 = '" + land1 + "'").FirstOrDefault();
            return landx ?? "";
        }

        public string GetPortName(DAF_PORT e)
        {
            //return e.DPT_LOAD_PORT_NAME;
            if (e.MT_PORT != null && e.MT_PORT.MLP_LOADING_PORT_NAME != null)
            {
                return e.MT_PORT.MLP_LOADING_PORT_NAME;
            }
            else if (e.MT_JETTY != null)
            {
                string jetty_name = (e.MT_JETTY.MTJ_JETTY_NAME ?? "").Trim();
                if (jetty_name.ToLower() == "other")
                {
                    jetty_name = "";
                }

                return (jetty_name ?? "") + " " + (e.MT_JETTY.MTJ_LOCATION ?? "") + " " + (this.GetCountry(e.MT_JETTY.MTJ_MT_COUNTRY) ?? "");
            }
            return "";
        }

        public string GetProductOrCrude(List<DAF_MATERIAL> mats)
        {
            List<string> products = new List<string>();
            foreach(DAF_MATERIAL mat in mats)
            {
                string mat_name = this.GetMaterialName(mat.DMT_MET_NUM);
                if(mat_name != null)
                {
                    products.Add(mat_name);
                }
            }
            return String.Join(", ", products);
        }

        public void InsertOrUpdate(string status, DAF_DATA data, string id, string username)
        {
            DateTime now = DateTime.Now;
            EntityCPAIEngine context = new EntityCPAIEngine();
            EntityCPAIEngine db = new EntityCPAIEngine();
            DAF_DATA old = db.DAF_DATA.Where(m => m.DDA_ROW_ID == id).FirstOrDefault();

            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM DAF_DATA WHERE DDA_ROW_ID = :DDA_ROW_ID", new[] { new OracleParameter("DDA_ROW_ID", id) });
            }
            catch (Exception e)
            {

            }

            DAF_DATA pd = data;
            pd.DDA_DOC_DATE = ((DateTime)pd.DDA_DOC_DATE);
            //pd.DDA_DOC_DATE = ((DateTime)pd.DDA_DOC_DATE).AddHours(7);
            if (old != null)
            {
                pd.DDA_UPDATED = now;
                pd.DDA_UPDATED_BY = username;
                pd.DDA_CREATED = old.DDA_CREATED ?? pd.DDA_CREATED;
                pd.DDA_CREATED_BY = old.DDA_CREATED_BY ?? pd.DDA_CREATED_BY;
            }
            else
            {
                pd.DDA_CREATED = now;
                pd.DDA_CREATED_BY = username;
                pd.DDA_UPDATED = now;
                pd.DDA_UPDATED_BY = username;
            }

            pd.DDA_ROW_ID = id;
            pd.DDA_STATUS = status;

            foreach(DAF_PORT e in pd.DAF_PORT)
            {
                e.DPT_ROW_ID = PAFHelper.GetSeqId("DAF_PORT");
                e.DPT_FK_DAF_DATA = pd.DDA_ROW_ID;

                e.DPT_CREATED = e.DPT_CREATED ?? now;
                e.DPT_CREATED_BY = e.DPT_CREATED_BY ?? username;
                e.DPT_UPDATED = now;
                e.DPT_UPDATED_BY = username;

                foreach (DAF_DEDUCTION_ACTIVITY f in e.DAF_DEDUCTION_ACTIVITY)
                {
                    f.DDD_ROW_ID = PAFHelper.GetSeqId("DAF_DEDUCTION_ACTIVITY");
                    f.DDD_FK_DAF_PORT = e.DPT_ROW_ID;
                      
                    f.DDD_CREATED = f.DDD_CREATED ?? now;
                    f.DDD_CREATED_BY = f.DDD_CREATED_BY ?? username;
                    f.DDD_UPDATED = now;
                    f.DDD_UPDATED_BY = username;
                }
            }
            
            foreach(DAF_MATERIAL e in pd.DAF_MATERIAL)
            {
                e.DMT_ROW_ID = PAFHelper.GetSeqId("DAF_MATERIAL");
                e.DMT_FK_DAF_DATA = pd.DDA_ROW_ID;

                e.DMT_CREATED = e.DMT_CREATED ?? now;
                e.DMT_CREATED_BY = e.DMT_CREATED_BY ?? username;
                e.DMT_UPDATED = now;
                e.DMT_UPDATED_BY = username;
            }

            foreach (DAF_ATTACH_FILE e in pd.DAF_ATTACH_FILE)
            {
                e.DAT_ROW_ID = PAFHelper.GetSeqId("DAF_ATTACH_FILE");
                e.DAT_FK_DAF_DATA = pd.DDA_ROW_ID;

                e.DAT_CREATED = e.DAT_CREATED ?? now;
                e.DAT_CREATED_BY = e.DAT_CREATED_BY ?? username;
                e.DAT_UPDATED = now;
                e.DAT_UPDATED_BY = username;
            }

            if ((pd.DDA_STATUS ?? "").StartsWith("WAITING"))
            {
                pd.DDA_REASON = "-";
            }
            context.DAF_DATA.Add(pd);
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                try
                {
                    PAFHelper.log(ex);
                }
                catch
                {

                }
                context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateException fex)
                {
                    PAFHelper.log(fex);
                }
            }
            catch (Exception e)
            {
                PAFHelper.log(e);
            }
            ////context.Database.ExecuteSqlCommand("DELETE FROM PAF_REVIEW_ITEMS WHERE PRI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            ////foreach (PAF_REVIEW_ITEMS a in data.PAF_REVIEW_ITEMS)
            ////{
            ////    //a.PRI_ROW_ID = PAFHelper.GetId();
            ////    a.PRI_ROW_ID = Guid.NewGuid().ToString("N");
            ////    a.PRI_FK_PAF_DATA = data.PDA_ROW_ID;
            ////    context.PAF_REVIEW_ITEMS.Add(a);
            ////}

            ////context.Database.ExecuteSqlCommand("DELETE FROM PAF_BIDDING_ITEMS WHERE PBI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            ////foreach (PAF_BIDDING_ITEMS a in data.PAF_BIDDING_ITEMS)
            ////{
            ////    a.PBI_ROW_ID = PAFHelper.GetId();
            ////    a.PBI_FK_PAF_DATA = data.PDA_ROW_ID;
            ////    context.PAF_BIDDING_ITEMS.Add(a);
            ////}

            ////context.Database.ExecuteSqlCommand("DELETE FROM PAF_PROPOSAL_ITEMS WHERE PSI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            ////foreach (PAF_PROPOSAL_ITEMS a in data.PAF_PROPOSAL_ITEMS)
            ////{
            ////    a.PSI_ROW_ID = PAFHelper.GetId();
            ////    a.PSI_FK_PAF_DATA = data.PDA_ROW_ID;
            ////    context.PAF_PROPOSAL_ITEMS.Add(a);
            ////}

            ////context.Database.ExecuteSqlCommand("DELETE FROM PAF_ATTACH_FILE WHERE PAT_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            ////foreach (PAF_ATTACH_FILE a in data.PAF_ATTACH_FILE)
            ////{
            ////    a.PAT_ROW_ID = PAFHelper.GetId();
            ////    a.PAT_FK_PAF_DATA = data.PDA_ROW_ID;
            ////    context.PAF_ATTACH_FILE.Add(a);
            ////}

            //try
            //{
            //    context.SaveChanges();
            //}
            //catch (DbUpdateException ex)
            //{
            //    PAFHelper.log(ex);
            //}
            //catch (DbEntityValidationException ex)
            //{
            //    PAFHelper.log(ex);
            //}
            //catch (Exception ex)
            //{
            //    PAFHelper.log(ex);
            //}
        }

        #region setDataHistory
        public void CreateDataHistory(DAF_DATA data, string current_action, string user, string note, string ftx_row_id, bool is_reject = false)
        {
            string transaction_id = data.TRAN_ID ?? data.DDA_ROW_ID;
            DAF_DATA_WRAPPER data2 = this.GetWrap(transaction_id);
            string json = "";
            if (data2 != null)
            {
                json = JsonConvert.SerializeObject(data2);
            } else
            {
                json = JsonConvert.SerializeObject(data);
            }
            //add data history
            DataHistoryDAL dthMan = new DataHistoryDAL();
            DateTime now = DateTime.Now;
            CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
            dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
            dataHistory.DTH_ACTION = current_action;
            dataHistory.DTH_ACTION_DATE = now;
            dataHistory.DTH_ACTION_BY = user;
            dataHistory.DTH_NOTE = note;
            dataHistory.DTH_JSON_DATA = json;
            dataHistory.DTH_TXN_REF = ftx_row_id;
            dataHistory.DTH_REJECT_FLAG = is_reject ? "Y" : "N";
            dataHistory.DTH_CREATED_BY = user;
            dataHistory.DTH_CREATED_DATE = now;
            dataHistory.DTH_UPDATED_BY = user;
            dataHistory.DTH_UPDATED_DATE = now;
            dthMan.Save(dataHistory);
        }
        #endregion

        public CPAI_DATA_HISTORY GetBeforeLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY> data_histories = dthMan.findByDthTxnRef(id);
            data_histories = data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).ToList();
            if (data_histories.Count > 1)
            {
                return data_histories[1];
            }
            return null;
        }

        public string GetLastAction(string id)
        {
            CPAI_DATA_HISTORY dth = this.GetBeforeLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                PAF_DATA pd = JsonConvert.DeserializeObject<PAF_DATA>(json);
                return pd.PDA_STATUS;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<string> GetRelatedGroupsFromTransactionId(string transaction_id)
        {
            List<string> result = new List<string>();
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT cug.USG_USER_GROUP FROM PAF_DATA PD LEFT JOIN USERS U ON UPPER(PD.PDA_CREATED_BY) = UPPER(U.USR_LOGIN) JOIN CPAI_USER_GROUP cug ON cug.USG_FK_USERS = U.USR_ROW_ID WHERE PD.PDA_ROW_ID = :PDA_ROW_ID";
            string group = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("PDA_ROW_ID", transaction_id.Trim()) }).FirstOrDefault();
            if (group.StartsWith("CMPS_INTER"))
            {
                result.Add("CMPS_INTER");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_DOM"))
            {
                result.Add("CMPS_DOM");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_SH"))
            {
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMLA"))
            {
                result.Add("CMLA");
                result.Add("CMLA_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            return result;
        }

    }
}
