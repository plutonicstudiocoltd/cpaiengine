﻿using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_MASTER_DAL
    {
        EntityCPAIEngine context = new EntityCPAIEngine();
        USER_GROUP_DAL usergroup_service = new USER_GROUP_DAL();
        public List<SELECT_CRITERIA> GetDataFromGlobalConfigArray(string key)
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            string[] ret_from_gc = GetArrayFromGlobalConfig(key);
            foreach (string elem in ret_from_gc)
            {
                SELECT_CRITERIA cur = new SELECT_CRITERIA();
                cur.ID = elem;
                cur.VALUE = elem;
                results.Add(cur);
            }
            return results;
        }

        public string[] GetArrayFromGlobalConfig(string key)
        {
            GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == key).FirstOrDefault();
            if (gc != null)
            {
                string raw_currency = gc.GCG_VALUE;
                string[] result = JsonConvert.DeserializeObject<string[]>(raw_currency);
                return result;
            }
            return new string[0];
        }

        public List<SELECT_CRITERIA> GetCurrency()
        {
            return this.GetDataFromGlobalConfigArray("DAF_CURRENCY_UNIT");
        }

        public List<DAF_CUSTOMER_CRITERIA> GetCustomerList()
        {
            List<DAF_CUSTOMER_CRITERIA> customers = new List<DAF_CUSTOMER_CRITERIA>();
            List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m => m.MCD_NATION == "I" && m.MCD_STATUS == "ACTIVE").ToList();
            foreach (MT_CUST_DETAIL cust in customer_details)
            {
                DAF_CUSTOMER_CRITERIA criteria = new DAF_CUSTOMER_CRITERIA();
                criteria.ID = cust.MCD_FK_CUS;
                criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                criteria.PAYMENT_TERM_DETAIL = PAF_MASTER_DAL.GetPaymentTermForCustomer(cust.MCD_FK_CUS, cust.MCD_FK_COMPANY);
                customers.Add(criteria);
            }
            customers = customers.OrderBy(m => m.VALUE).ToList();
            return customers;
        }

        public List<SELECT_CRITERIA> GetBrokerList()
        {
            //List<SELECT_CRITERIA> results = context.Database.SqlQuery<SELECT_CRITERIA>("SELECT MT_VENDOR.VND_ACC_NUM_VENDOR as ID, MT_VENDOR.VND_NAME1 || ' ' || MT_VENDOR.VND_NAME2 as VALUE, MT_VENDOR_DATA.MVD_FK_COMPANY as COMPANY_CODE, MT_PAYMENT.MPM_DETAIL as PAYMENT_TERM_DETAIL FROM MT_VENDOR LEFT JOIN MT_VENDOR_DATA ON MT_VENDOR.VND_ACC_NUM_VENDOR = MT_VENDOR_DATA.MVD_FK_VENDOR LEFT JOIN MT_VENDOR_TERMS_PAY ON MT_VENDOR_DATA.MVD_FK_TERMS_PAY = MT_VENDOR_TERMS_PAY.MVT_ROW_ID LEFT JOIN MT_PAYMENT ON MT_VENDOR_TERMS_PAY.MVT_TERMS_PAY_KEY = MT_PAYMENT.MPM_ROW_ID WHERE MT_VENDOR.VND_STATUS = 'ACTIVE'").ToList();
            List<SELECT_CRITERIA> results = context.Database.SqlQuery<SELECT_CRITERIA>("SELECT MT_VENDOR.VND_ACC_NUM_VENDOR AS ID, MT_VENDOR.VND_NAME1 || ' ' || MT_VENDOR.VND_NAME2 AS VALUE FROM MT_VENDOR WHERE MT_VENDOR.VND_STATUS = 'ACTIVE' AND MT_VENDOR.VND_DELETION_FLAG IS NULL AND MT_VENDOR.VND_CENTRAL_DELETION_FLAG IS NULL").ToList();
            return results.OrderBy(m => m.VALUE).ToList();
        }

        public List<SELECT_CRITERIA> GetVesselList()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            List <MT_VEHICLE> vehicles = context.MT_VEHICLE.Where(m => ((m.VEH_TYPE ?? "").ToUpper() == "SHIP") && (m.VEH_STATUS ?? "").ToUpper() == "ACTIVE").OrderBy(m => m.VEH_VEH_TEXT).ToList();
            foreach(MT_VEHICLE elem in vehicles)
            {
                SELECT_CRITERIA e = new SELECT_CRITERIA();
                e.ID = elem.VEH_ID;
                e.VALUE = elem.VEH_VEH_TEXT;
                results.Add(e);
            }

            return results;
        }

        public List<SELECT_CRITERIA> GetPortList(string username, string system)
        {
            CPAI_USER_GROUP user_group = usergroup_service.findByUserAndSystem(username, system);
            string usergroup_str = user_group != null ? user_group.USG_USER_GROUP : "";
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            // List<MT_PORT_CONTROL> ports = context.MT_PORT_CONTROL.Where(m => m.MT_PORT !=null && m.MMP_SYSTEM != null && m.MMP_SYSTEM.Contains(usergroup_str)).OrderBy(m => m.MT_PORT.MLP_LOADING_PORT_NAME).ToList();
            List<MT_PORT_CONTROL> ports = context.MT_PORT_CONTROL.Where(m => m.MT_PORT !=null && m.MMP_SYSTEM != null && (m.MMP_SYSTEM.Contains("PCF") || m.MMP_SYSTEM.Contains("DAF")) && m.MMP_STATUS == "ACTIVE").OrderBy(m => m.MT_PORT.MLP_LOADING_PORT_NAME).ToList();
            foreach (MT_PORT_CONTROL elem in ports)
            {
                SELECT_CRITERIA e = new SELECT_CRITERIA();
                if (elem != null)
                {
                    e.ID = elem.MT_PORT.MLP_LOADING_PORT_ID.ToString();
                    e.VALUE = elem.MT_PORT.MLP_LOADING_PORT_NAME;
                }
                //e.USER_GROUP = elem.MT_PORT_CONTROL.
                results.Add(e);
            }

            return results;
        }
        
        public List<DAF_JETTY_CRITERIA> GetJettyLoadList()
        {
            return this.GetJettyList("L");
        }
        public List<DAF_JETTY_CRITERIA> GetJettyDishargeList()
        {
            return this.GetJettyList("D");
        }

        public string GetCountry(string land1)
        {
            //MT_COUNTRY landx = context.MT_COUNTRY.Where(m => (m.MCT_LAND1 ?? "").ToLower() == (land1 ?? "").ToLower()).FirstOrDefault();

            string landx = context.Database.SqlQuery<string>("SELECT DISTINCT MCT_LANDX FROM MT_COUNTRY WHERE MCT_LAND1 = '" + land1 + "'").FirstOrDefault();
            return landx ?? "";
        }

        public List<DAF_JETTY_CRITERIA> GetJettyList(string type)
        {
            List<DAF_JETTY_CRITERIA> results = new List<DAF_JETTY_CRITERIA>();
            List<MT_JETTY> ports = context.MT_JETTY.Where(m =>
                //(m.MTJ_PORT_TYPE ?? "").ToLower() == "domestic" &&
                (m.MTJ_STATUS ?? "").ToUpper() == "ACTIVE"
            ).OrderBy(m => m.MTJ_JETTY_NAME).ToList();
            ports = ports.Where(m => m.MTJ_CREATE_TYPE == type).ToList();
            foreach (MT_JETTY elem in ports)
            {
                DAF_JETTY_CRITERIA e = new DAF_JETTY_CRITERIA();
                if (elem != null)
                {
                    string jetty_name = (elem.MTJ_JETTY_NAME ?? "");
                    if (jetty_name.ToLower() == "other")
                    {
                        jetty_name = "";
                    }
                    else
                    {
                        jetty_name = jetty_name + " ";
                    }

                    e.ID = elem.MTJ_ROW_ID.ToString();
                    e.VALUE = jetty_name + (elem.MTJ_LOCATION ?? "") + " "  + (this.GetCountry(elem.MTJ_MT_COUNTRY) ?? "");
                    if (elem.MTJ_PORT_TYPE != null)
                    {
                        e.VALUE += " (" + elem.MTJ_PORT_TYPE + ")";
                    }
                    e.CREATE_TYPE = elem.MTJ_CREATE_TYPE;
                }
                //e.USER_GROUP = elem.MT_PORT_CONTROL.
                results.Add(e);
            }

            return results;
        }
       
        public List<SELECT_CRITERIA> GetCompany()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            List<MT_COMPANY> list_mt_company = context.MT_COMPANY.Where(m => m.MCO_STATUS == "ACTIVE").ToList();
            foreach(MT_COMPANY elem in list_mt_company)
            {
                SELECT_CRITERIA e = new SELECT_CRITERIA();
                e.ID = elem.MCO_COMPANY_CODE;
                e.VALUE = elem.MCO_SHORT_NAME;
                results.Add(e);
            }
            return results;
        }


        public List<DAF_INCOTERM_CRITERIA> GetIncoterms()
        {
            List<DAF_INCOTERM_CRITERIA> results = new List<DAF_INCOTERM_CRITERIA>();
            List<MT_INCOTERMS> incoterms = context.MT_INCOTERMS.Where(m => m.MIN_STATUS == "ACTIVE").OrderBy(m => m.MIN_INCOTERMS).ToList();
            foreach (MT_INCOTERMS elem in incoterms)
            {
                DAF_INCOTERM_CRITERIA cur = new DAF_INCOTERM_CRITERIA();
                cur.ID = elem.MIN_INCOTERMS;
                cur.VALUE = elem.MIN_INCOTERMS;
                cur.INCLUDED_FREIGHT = elem.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                results.Add(cur);
            }
            return results;
        }

        
        public List<SELECT_CRITERIA> GetMatrials()
        {
            List<SELECT_CRITERIA> results = new List<SELECT_CRITERIA>();
            //List<MT_MATERIALS> dat = context.MT_MATERIALS.OrderBy(m => m.MET_MAT_DES_ENGLISH).ToList();
            //foreach (MT_MATERIALS elem in dat)
            //{
            //    SELECT_CRITERIA cur = new SELECT_CRITERIA();
            //    cur.ID = elem.MET_NUM;
            //    cur.VALUE = elem.MET_MAT_DES_ENGLISH;
            //    results.Add(cur);
            //}
            //List<MT_MATERIALS_CONTROL> dat = context.MT_MATERIALS_CONTROL.OrderBy(m => m.MMC_SYSTEM == "MAT_CMCS" && m.MMC_STATUS == "ACTIVE").ToList();
            List<MT_MATERIALS> dat = context.MT_MATERIALS.Where(m => (m.MET_FLA_MAT_DELETION ?? "").ToUpper() != "X").ToList();
            foreach (MT_MATERIALS elem in dat)
            {
                SELECT_CRITERIA cur = new SELECT_CRITERIA();
                if (elem != null)
                {
                    cur.ID = (elem.MET_NUM ?? "");
                    cur.VALUE = (elem.MET_MAT_DES_ENGLISH ?? "") + " (" + elem.MET_NUM + ")";
                    results.Add(cur);
                }
            }
            return results;
        }
    }
}
