﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALDAF
{
    public class DAF_SEARCH_DATA_CRITERIA
    {
        public string DOC_NO { get; set; }
        public string VESSEL_NAME { get; set; }
        public string CHARTERER { get; set; }

        public Nullable<DateTime> LAYCAN_FROM { get; set; }
        public Nullable<DateTime> LAYCAN_TO { get; set; }
        public Nullable<DateTime> DISCHARGE_FROM { get; set; }
        public Nullable<DateTime> DISCHARGE_TO { get; set; }
        public Nullable<DateTime> LOADING_FROM { get; set; }
        public Nullable<DateTime> LOADING_TO { get; set; }
        public string CREATED_BY { get; set; }


    }
}
