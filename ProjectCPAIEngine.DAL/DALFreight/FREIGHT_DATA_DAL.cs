﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALFreight
{
    public class FREIGHT_DATA_DAL
    {
        public void Save(CPAI_FREIGHT_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_FREIGHT_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_FREIGHT_DATA data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_FREIGHT_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_FREIGHT_DATA data, EntityCPAIEngine context)
        {
            try
            {
               
                    var _search = context.CPAI_FREIGHT_DATA.Find(data.FDA_ROW_ID);
                    #region Set Value
                    _search.FDA_STATUS = data.FDA_STATUS;
                    _search.FDA_UPDATED_BY = data.FDA_UPDATED_BY;
                    _search.FDA_UPDATED = data.FDA_UPDATED;
                    #endregion
                    context.SaveChanges();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_FREIGHT_DATA WHERE FDA_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_FREIGHT_DATA WHERE FDA_ROW_ID = '" + TransID + "'");
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CPAI_FREIGHT_DATA GetFCLDATAByID(string FCLID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _fclData = context.CPAI_FREIGHT_DATA.Where(x => x.FDA_ROW_ID.ToUpper() == FCLID.ToUpper()).FirstOrDefault();
                    return _fclData;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data CPAI_FREIGHT_DATA by Status
        /// </summary>
        /// <param name="Status">status ("NEW","SUBMIT") set string emply or nul get all</param>
        /// <returns></returns>
        public static List<CPAI_FREIGHT_DATA> GetFreightData(string Status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                if (String.IsNullOrEmpty(Status) == false)
                {
                    var query = (from f in context.CPAI_FREIGHT_DATA
                                 where f.FDA_STATUS.ToUpper() == Status.ToUpper()
                                 select f).Distinct();
                    return query.ToList();

                }
                else
                {
                    var query = (from f in context.CPAI_FREIGHT_DATA
                                 select f).Distinct();
                    return query.ToList();
                }

            }
        }

        public CPAI_FREIGHT_DATA GetFreightDataByDocNo(string DocNo)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var data = context.CPAI_FREIGHT_DATA.Where(v => v.FDA_DOC_NO.ToUpper() == DocNo.ToUpper()).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
