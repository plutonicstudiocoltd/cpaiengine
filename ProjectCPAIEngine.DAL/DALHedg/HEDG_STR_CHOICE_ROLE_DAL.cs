﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CHOICE_ROLE_DAL
    {
        public void Save(HEDG_STR_CHOICE_ROLE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CHOICE_ROLE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CHOICE_ROLE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CHOICE_ROLE.SingleOrDefault(p => p.HSCR_ROW_ID == data.HSCR_ROW_ID);
                #region Set Value
                _search.HSCR_FK_HEDG_STR_CHOICE_DETAIL = data.HSCR_FK_HEDG_STR_CHOICE_DETAIL;
                _search.HSCR_ROLE = data.HSCR_ROLE;
                _search.HSCR_AVG_AF_PRICE = data.HSCR_AVG_AF_PRICE;

                _search.HSCR_UPDATED_BY = data.HSCR_UPDATED_BY;
                _search.HSCR_UPDATED_DATE = data.HSCR_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CHOICE_ROLE WHERE HSCR_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CHOICE_ROLE GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_ROLE.Where(x => x.HSCR_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_CHOICE_ROLE> GetBySTRCDID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CHOICE_ROLE.Where(x => x.HSCR_FK_HEDG_STR_CHOICE_DETAIL.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
