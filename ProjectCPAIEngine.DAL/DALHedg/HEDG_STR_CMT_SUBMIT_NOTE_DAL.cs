﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_CMT_SUBMIT_NOTE_DAL
    {
        public void Save(HEDG_STR_CMT_SUBMIT_NOTE data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_CMT_SUBMIT_NOTE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_CMT_SUBMIT_NOTE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_CMT_SUBMIT_NOTE.SingleOrDefault(p => p.HSSN_ROW_ID == data.HSSN_ROW_ID);
                #region Set Value
                _search.HSSN_SUBJECT = data.HSSN_SUBJECT;
                _search.HSSN_APPROVED_DATE = data.HSSN_APPROVED_DATE;
                _search.HSSN_REF_NO = data.HSSN_REF_NO;
                _search.HSSN_NOTE = data.HSSN_NOTE;

                _search.HSSN_UPDATED_BY = data.HSSN_UPDATED_BY;
                _search.HSSN_UPDATED_DATE = data.HSSN_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_CMT_SUBMIT_NOTE WHERE HSSN_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_CMT_SUBMIT_NOTE GetBySTRCMTID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_CMT_SUBMIT_NOTE.Where(x => x.HSSN_FK_HEDG_STR_CMT_FW.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
