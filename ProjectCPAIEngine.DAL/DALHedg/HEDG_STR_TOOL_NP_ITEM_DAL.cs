﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALHedg
{
    public class HEDG_STR_TOOL_NP_ITEM_DAL
    {
        public void Save(HEDG_STR_TOOL_NP_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_STR_TOOL_NP_ITEM.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(HEDG_STR_TOOL_NP_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_STR_TOOL_NP_ITEM.SingleOrDefault(p => p.HSNI_ROW_ID == data.HSNI_ROW_ID);
                #region Set Value
                _search.HSNI_FK_HEDG_STR_TOOL_NP = data.HSNI_FK_HEDG_STR_TOOL_NP;
                _search.HSNI_ORDER = data.HSNI_ORDER;
                _search.HSNI_NET_TARGET = data.HSNI_NET_TARGET;
                _search.HSNI_RETURN = data.HSNI_RETURN;
                _search.HSNI_PREMIUM = data.HSNI_PREMIUM;

                _search.HSNI_UPDATED_BY = data.HSNI_UPDATED_BY;
                _search.HSNI_UPDATED_DATE = data.HSNI_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM HEDG_STR_TOOL_NP_ITEM WHERE HSNI_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public HEDG_STR_TOOL_NP_ITEM GetByID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_NP_ITEM.Where(x => x.HSNI_ROW_ID.Trim().ToUpper() == id.Trim().ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_STR_TOOL_NP_ITEM> GetBySTRNPID(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_STR_TOOL_NP_ITEM.Where(x => x.HSNI_FK_HEDG_STR_TOOL_NP.Trim().ToUpper() == id.Trim().ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
