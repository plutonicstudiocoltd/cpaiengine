﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_FW_ANNUAL_DAL
    {
        public void Save(HEDG_FW_ANNUAL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.HEDG_FW_ANNUAL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(HEDG_FW_ANNUAL data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_FW_ANNUAL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_ANNUAL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_FW_ANNUAL.Find(data.HFT_ROW_ID);
                    #region Set Value                    
                    _search.HFT_ACTIVEDATE_START = data.HFT_ACTIVEDATE_START;
                    _search.HFT_ACTIVEDATE_END = data.HFT_ACTIVEDATE_END;
                    _search.HFT_FK_UNDERLYING = data.HFT_FK_UNDERLYING;
                    _search.HFT_DESC = data.HFT_DESC;
                    _search.HFT_REMARK = data.HFT_REMARK;
                    _search.HFT_STATUS = data.HFT_STATUS;
                    _search.HFT_UPDATED_BY = data.HFT_UPDATED_BY;
                    _search.HFT_UPDATED_DATE = data.HFT_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_FW_ANNUAL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_ANNUAL.Find(data.HFT_ROW_ID);
                #region Set Value                
                _search.HFT_ACTIVEDATE_START = data.HFT_ACTIVEDATE_START;
                _search.HFT_ACTIVEDATE_END = data.HFT_ACTIVEDATE_END;
                _search.HFT_FK_UNDERLYING = data.HFT_FK_UNDERLYING;
                _search.HFT_DESC = data.HFT_DESC;
                _search.HFT_REMARK = data.HFT_REMARK;
                _search.HFT_STATUS = data.HFT_STATUS;
                _search.HFT_UPDATED_BY = data.HFT_UPDATED_BY;
                _search.HFT_UPDATED_DATE = data.HFT_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Cancel(HEDG_FW_ANNUAL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_FW_ANNUAL.Find(data.HFT_ROW_ID);
                #region Set Value                
                _search.HFT_STATUS = data.HFT_STATUS;
                _search.HFT_REASON = data.HFT_REASON;
                _search.HFT_UPDATED_BY = data.HFT_UPDATED_BY;
                _search.HFT_UPDATED_DATE = data.HFT_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
