﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_COMPANY_AUTH_TRADE_DAL
    {
        public void Save(MT_COMPANY_AUTH_TRADE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_COMPANY_AUTH_TRADE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<MT_COMPANY_AUTH_TRADE> getComAuthByCompany(string comCode)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_COMPANY_AUTH_TRADE.Where(c => c.MCAT_FK_MT_COMPANY == comCode);
                return data.ToList();
            }
        }



        public void Delete(string comCode)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_COMPANY_AUTH_TRADE WHERE MCAT_FK_MT_COMPANY = '" + comCode + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }          
        }





        public void Update(MT_COMPANY_AUTH_TRADE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_COMPANY_AUTH_TRADE.Find(data.MCAT_ROW_ID);
                    #region Set Value
                    _search.MCAT_ROW_ID = data.MCAT_ROW_ID != null ? data.MCAT_ROW_ID : _search.MCAT_ROW_ID;
                    _search.MCAT_ORDER = data.MCAT_ORDER != null ? data.MCAT_ORDER : _search.MCAT_ORDER;
                    _search.MCAT_TRADING_BOOK = data.MCAT_TRADING_BOOK != null ? data.MCAT_TRADING_BOOK : _search.MCAT_TRADING_BOOK;
                    _search.MCAT_TRADE_FOR_FLAG = data.MCAT_TRADE_FOR_FLAG != null ? data.MCAT_TRADE_FOR_FLAG : _search.MCAT_TRADE_FOR_FLAG;
                    _search.MCAT_FEE_BBL = data.MCAT_FEE_BBL != null ? data.MCAT_FEE_BBL : _search.MCAT_FEE_BBL;
                    _search.MCAT_FEE_MT = data.MCAT_FEE_MT != null ? data.MCAT_FEE_MT : _search.MCAT_FEE_MT;
                    _search.MCAT_UPDATED_DATE = data.MCAT_UPDATED_DATE;
                    _search.MCAT_UPDATED_BY = data.MCAT_UPDATED_BY;
                    _search.MCAT_FK_MT_COMPANY = data.MCAT_FK_MT_COMPANY != null ? data.MCAT_FK_MT_COMPANY : null;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
