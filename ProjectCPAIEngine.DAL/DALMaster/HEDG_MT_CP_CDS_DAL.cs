﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Linq;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_CDS_DAL
    {
        public void Save(HEDG_MT_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_CDS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_CDS data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_CDS.Find(data.HMCD_ROW_ID);
                #region Set Value
                _search.HMCD_FK_HEDG_MT_CP = data.HMCD_FK_HEDG_MT_CP;
                _search.HMCD_FK_HEDG_MT_CDS_FILE = data.HMCD_FK_HEDG_MT_CDS_FILE;
                _search.HMCD_CDS_DATE = data.HMCD_CDS_DATE;
                _search.HMCD_CDS_VALUE = data.HMCD_CDS_VALUE;
                _search.HMCD_STATUS = data.HMCD_STATUS;
                _search.HMCD_UPDATED_DATE = data.HMCD_UPDATED_DATE;
                _search.HMCD_UPDATED_BY = data.HMCD_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP_CDS WHERE HMCD_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP_CDS where r.HMCD_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP_CDS.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Boolean CheckCPStatus(string HEDG_CP, string TYPE, int CDS_DAY, Decimal CDS_VALUE)
        {
            Boolean resp = false;
            int count = 0;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP_CDS
                                 where v.HMCD_FK_HEDG_MT_CP.ToUpper() == HEDG_CP.ToUpper()
                                 && v.HMCD_STATUS == "ACTIVE"
                                 orderby v.HMCD_CDS_DATE descending
                                 select v).Take(CDS_DAY);
                    var result = query.ToList();
                    if (TYPE == "CDS OUT")
                    {
                        count = (from a in result
                                 where Convert.ToDecimal(a.HMCD_CDS_VALUE) > CDS_VALUE
                                 select a).Count();
                    }
                    else if (TYPE == "CDS IN")
                    {
                        count = (from a in result
                                 where Convert.ToDecimal(a.HMCD_CDS_VALUE) <= CDS_VALUE
                                 select a).Count();
                    }

                    if (count == CDS_DAY)
                    {
                        resp = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resp;
        }

    }
}
