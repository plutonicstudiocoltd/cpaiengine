﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_HISTORY_DAL
    {
        public void Save(HEDG_MT_CP_HISTORY data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_HISTORY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_HISTORY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_HISTORY.Find(data.HMCH_ROW_ID);
                #region Set Value
                _search.HMCH_FK_HEDG_MT_CP = data.HMCH_FK_HEDG_MT_CP;
                _search.HMCH_REASON = data.HMCH_REASON;
                _search.HMCH_UPDATED_DATE = data.HMCH_UPDATED_DATE;
                _search.HMCH_UPDATED_BY = data.HMCH_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP_HISTORY WHERE HMCH_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP_HISTORY where r.HMCH_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP_HISTORY.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static HEDG_MT_CP_HISTORY GetMtCp(string vendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CP_HISTORY
                             where v.HMCH_FK_HEDG_MT_CP.ToUpper() == vendor.ToUpper()
                             select v).FirstOrDefault();
                return query;
            }
        }
    }
}
