﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_CP_HISTORY_LOG_DAL
    {
        public void Save(HEDG_MT_CP_HISTORY_LOG data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_CP_HISTORY_LOG.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_CP_HISTORY_LOG data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_CP_HISTORY_LOG.Find(data.HMHL_ROW_ID);
                #region Set Value
                _search.HMHL_FK_CP_HISTORY = data.HMHL_FK_CP_HISTORY;
                _search.HMHL_TITLE = data.HMHL_TITLE;
                _search.HMHL_ACTION = data.HMHL_ACTION;
                _search.HMHL_NOTE = data.HMHL_NOTE;
                _search.HMHL_UPDATED_DATE = data.HMHL_UPDATED_DATE;
                _search.HMHL_UPDATED_BY = data.HMHL_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_CP_HISTORY_LOG WHERE HMHL_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.HEDG_MT_CP_HISTORY_LOG where r.HMHL_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.HEDG_MT_CP_HISTORY_LOG.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static HEDG_MT_CP_HISTORY_LOG GetMtCp(string vendor)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.HEDG_MT_CP_HISTORY_LOG
                             where v.HMHL_FK_CP_HISTORY.ToUpper() == vendor.ToUpper()
                             select v).FirstOrDefault();
                return query;
            }
        }
    }
}
