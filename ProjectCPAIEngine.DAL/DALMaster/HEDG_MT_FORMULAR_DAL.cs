﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_FORMULAR_DAL
    {
        public void Save(HEDG_MT_FORMULA data)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    context.HEDG_MT_FORMULA.Add(data);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void Delete(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_FORMULA WHERE HMFM_ROW_ID = '" + id + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_FORMULA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.HEDG_MT_FORMULA.Find(data.HMFM_ROW_ID);

                    _search.HMFM_NAME = data.HMFM_NAME != null ? data.HMFM_NAME : _search.HMFM_NAME;
                    _search.HMFM_DESC = data.HMFM_DESC != null ? data.HMFM_DESC : _search.HMFM_DESC;
                    _search.HMFM_TEXT = data.HMFM_TEXT != null ? data.HMFM_TEXT : _search.HMFM_TEXT;
                    _search.HMFM_STATUS = data.HMFM_STATUS != null ? data.HMFM_STATUS : _search.HMFM_STATUS;
                    _search.HMFM_UPDATED_BY = data.HMFM_UPDATED_BY;
                    _search.HMFM_UPDATED_DATE = data.HMFM_UPDATED_DATE;
                    context.SaveChanges();
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<HEDG_MT_FORMULA> GetAllFormula()
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.HEDG_MT_FORMULA.Where(x => x.HMFM_STATUS == "ACTIVE").OrderBy(x => x.HMFM_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
