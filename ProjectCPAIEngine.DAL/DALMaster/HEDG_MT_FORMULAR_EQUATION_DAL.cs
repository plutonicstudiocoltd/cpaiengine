﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_FORMULAR_EQUATION_DAL
    {
       public void Save(HEDG_MT_FORMULA_EQUATION data)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    context.HEDG_MT_FORMULA_EQUATION.Add(data);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void Delete(string formularId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_FORMULA_EQUATION WHERE HMFE_FK_MT_FORMULA = '" + formularId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
