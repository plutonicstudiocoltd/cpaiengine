﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class HEDG_MT_TOOL_SWITCH_DAL
    {
        public void Save(HEDG_MT_TOOL_SWITCH data, EntityCPAIEngine context)
        {
            try
            {
                context.HEDG_MT_TOOL_SWITCH.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(HEDG_MT_TOOL_SWITCH data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.HEDG_MT_TOOL_SWITCH.Find(data.HMTS_ROW_ID);
                #region Set Value
                _search.HMTS_ORDER = data.HMTS_ORDER;
                _search.HMTS_OPTION = data.HMTS_OPTION;
                _search.HMTS_UPDATED_DATE = data.HMTS_UPDATED_DATE;
                _search.HMTS_UPDATED_BY = data.HMTS_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_SWITCH WHERE HMTS_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAll(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM HEDG_MT_TOOL_SWITCH WHERE HMTS_FK_HEDG_MT_TOOL = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HEDG_MT_TOOL_SWITCH> GetDataByFkID(string sID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var result = context.HEDG_MT_TOOL_SWITCH.Where(x => x.HMTS_FK_HEDG_MT_TOOL.ToUpper() == sID.ToUpper()).OrderBy(x => x.HMTS_ORDER).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
