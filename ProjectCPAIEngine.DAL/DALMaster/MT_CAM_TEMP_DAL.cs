﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CAM_TEMP_DAL
    {
        public static MT_CAM_TEMP GetCamTemplate()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_CAM_TEMP
                             orderby v.COCT_CREATED descending
                             select v).FirstOrDefault();
                return query;
            }
        }

        public static int GetMaxVersion()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_CAM_TEMP
                             select v).ToList()
                             .Select(i => int.Parse(i.COCT_VERSION)).OrderByDescending(i => i).FirstOrDefault();
                return query != 0 ? query : 0;
            }
        }


        public void Save(MT_CAM_TEMP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CAM_TEMP.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void Update(MT_CAM_TEMP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CAM_TEMP.Find(data.COCT_ROW_ID);

                    _search.COCT_JSON = data.COCT_JSON != null ? data.COCT_JSON : _search.COCT_JSON;
                    _search.COCT_VERSION = data.COCT_VERSION != null ? data.COCT_VERSION : _search.COCT_VERSION;
                    _search.COCT_REASON = data.COCT_REASON != null ? data.COCT_REASON : _search.COCT_REASON;
                    _search.COCT_UPDATED = data.COCT_UPDATED;
                    _search.COCT_UPDATED_BY = data.COCT_UPDATED_BY;

                    context.SaveChanges();
                };

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(MT_CAM_TEMP data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    // CHECK TRANSACTION DOES NOT USED BEFORE // 
                    if (!context.COO_CAM.Any(i => i.COCA_FK_MT_CAM_TEMP == data.COCT_ROW_ID))
                    {
                        var _search = context.MT_CAM_TEMP.Find(data.COCT_ROW_ID);
                        if (_search != null)
                        {
                            context.MT_CAM_TEMP.Remove(_search);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        throw new ArgumentException("System cannot delete, this transaction has been used.");
                    }                    
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
