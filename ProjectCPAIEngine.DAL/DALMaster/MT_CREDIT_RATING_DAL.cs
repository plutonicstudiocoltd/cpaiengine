﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CREDIT_RATING_DAL
    {
        public void Save(MT_CREDIT_RATING data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CREDIT_RATING.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CREDIT_RATING data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CREDIT_RATING.Find(data.MCR_ROW_ID);
                #region Set Value
                _search.MCR_ORDER = data.MCR_ORDER;
                _search.MCR_RATING = data.MCR_RATING;
                _search.MCR_AMOUNT = data.MCR_AMOUNT;
                _search.MCR_DESC = data.MCR_DESC;
                _search.MCR_STATUS = data.MCR_STATUS;
                _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    if (String.IsNullOrEmpty(RowID))
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_CREDIT_RATING");
                    else
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_CREDIT_RATING WHERE MCR_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByID(string ID, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    var result = from r in context.MT_CREDIT_RATING where r.MCR_ROW_ID == ID select r;

                    if (result.Count() > 0)
                    {
                        foreach (var p in result)
                        {
                            context.MT_CREDIT_RATING.Remove(p);
                        }
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChkExist(string ID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var result = context.MT_CREDIT_RATING.Where(c => c.MCR_ROW_ID == ID).Any();
                if (result)
                    return true;
                else
                    return false;
            }
        }

        public List<string> ChkInActive(List<string> ID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var result = context.MT_CREDIT_RATING.Where(c => !ID.Contains(c.MCR_ROW_ID)).Select(x => x.MCR_ROW_ID).ToList();
                return result;
            }
        }

        public void InActive(MT_CREDIT_RATING data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CREDIT_RATING.Find(data.MCR_ROW_ID);
                #region Set Value
                _search.MCR_STATUS = data.MCR_STATUS;
                _search.MCR_UPDATED_DATE = data.MCR_UPDATED_DATE;
                _search.MCR_UPDATED_BY = data.MCR_UPDATED_BY;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MT_CREDIT_RATING> GetAllCreditRating()
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.MT_CREDIT_RATING.Where(x => x.MCR_STATUS == "ACTIVE").OrderBy(x => x.MCR_ORDER).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
