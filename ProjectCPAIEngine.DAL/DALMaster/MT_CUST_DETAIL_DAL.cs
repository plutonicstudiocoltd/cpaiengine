﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_DETAIL_DAL
    {
        public void Save(MT_CUST_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST_DETAIL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST_DETAIL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_DETAIL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST_DETAIL.Find(data.MCD_ROW_ID);
                    #region Set Value
                    _search.MCD_FK_CUS = data.MCD_FK_CUS;
                    _search.MCD_NAME_1 = data.MCD_NAME_1;
                    _search.MCD_FK_COMPANY = data.MCD_FK_COMPANY;
                    _search.MCD_NATION = data.MCD_NATION;
                    _search.MCD_COUNT_KEY = data.MCD_COUNT_KEY;
                    _search.MCD_STATUS = data.MCD_STATUS;
                    _search.MCD_UPDATED_BY = data.MCD_UPDATED_BY;
                    _search.MCD_UPDATED_DATE = data.MCD_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_DETAIL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST_DETAIL.Find(data.MCD_ROW_ID);
                #region Set Value
                _search.MCD_FK_CUS = data.MCD_FK_CUS;
                _search.MCD_NAME_1 = data.MCD_NAME_1;
                _search.MCD_FK_COMPANY = data.MCD_FK_COMPANY;
                _search.MCD_NATION = data.MCD_NATION;
                _search.MCD_COUNT_KEY = data.MCD_COUNT_KEY;
                _search.MCD_STATUS = data.MCD_STATUS;
                _search.MCD_UPDATED_BY = data.MCD_UPDATED_BY;
                _search.MCD_UPDATED_DATE = data.MCD_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_DETAIL WHERE MCD_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_DETAIL WHERE MCD_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustomerCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_CUST_DETAIL where r.MCD_FK_CUS == CustomerCode select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_CUST_DETAIL.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustomerCode, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_CUST_DETAIL where r.MCD_FK_CUS == CustomerCode select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_CUST_DETAIL.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetCUSTDetailName(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_CUST_DETAIL
                            where v.MCD_ROW_ID == id && v.MCD_NATION == "I"
                            select v.MCD_NAME_1;
                var result = query.FirstOrDefault() ?? "";
                return result;
            }
        }

        public static string GetCUSTDetailShortName(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_CUST_DETAIL
                            where v.MCD_ROW_ID == id && v.MCD_NATION == "I"
                            select v.MCD_SORT_FIELD;
                var result = query.FirstOrDefault() ?? "";
                return result;
            }
        }
    }
}
