﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_CUST_PAYMENT_TERM_DAL
    {
        public void Save(MT_CUST_PAYMENT_TERM data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_CUST_PAYMENT_TERM.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_CUST_PAYMENT_TERM data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_CUST_PAYMENT_TERM.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_PAYMENT_TERM data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_CUST_PAYMENT_TERM.Find(data.MCP_ROW_ID);
                    if (_search == null) {
                        context.MT_CUST_PAYMENT_TERM.Add(data);
                    }
                    else
                    {
                        #region Set Value
                        _search.MCP_FK_MT_CUST = data.MCP_FK_MT_CUST;
                        _search.MCP_PAYMENT_TERM = data.MCP_PAYMENT_TERM;
                        _search.MCP_BROKER_NAME = data.MCP_BROKER_NAME;
                        _search.MCP_BROKER_COMMISSION = data.MCP_BROKER_COMMISSION;
                        _search.MCP_UPDATED_BY = data.MCP_UPDATED_BY;
                        _search.MCP_UPDATED_DATE = data.MCP_UPDATED_DATE;
                        #endregion
                    }
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_CUST_PAYMENT_TERM data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_CUST_PAYMENT_TERM.Find(data.MCP_ROW_ID);
                if (_search == null ) {
                    context.MT_CUST_PAYMENT_TERM.Add(data);
                }
                else
                {
                    #region Set Value
                    _search.MCP_FK_MT_CUST = data.MCP_FK_MT_CUST;
                    _search.MCP_PAYMENT_TERM = data.MCP_PAYMENT_TERM;
                    _search.MCP_BROKER_NAME = data.MCP_BROKER_NAME;
                    _search.MCP_BROKER_COMMISSION = data.MCP_BROKER_COMMISSION;
                    _search.MCP_WITHOLDING_TAX = data.MCP_WITHOLDING_TAX;
                    _search.MCP_UPDATED_BY = data.MCP_UPDATED_BY;
                    _search.MCP_UPDATED_DATE = data.MCP_UPDATED_DATE;
                    #endregion
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_PAYMENT_TERM WHERE MCP_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_CUST_PAYMENT_TERM WHERE MCP_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustomerCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_CUST_PAYMENT_TERM where r.MCP_FK_MT_CUST == CustomerCode select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_CUST_PAYMENT_TERM.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByCode(string CustomerCode, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_CUST_PAYMENT_TERM where r.MCP_FK_MT_CUST == CustomerCode select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_CUST_PAYMENT_TERM.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
