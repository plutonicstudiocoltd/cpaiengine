﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_DISTRIBUTION_CHANNEL_DAL
    {
        public void Save(MT_DISTRIBUTION_CHANNEL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_DISTRIBUTION_CHANNEL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_DISTRIBUTION_CHANNEL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_DISTRIBUTION_CHANNEL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_DISTRIBUTION_CHANNEL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_DISTRIBUTION_CHANNEL.Find(data.MDC_DC);
                    #region Set Value
                    _search.MDC_ZONE = data.MDC_ZONE;
                    _search.MDC_DESCRIPTION = data.MDC_DESCRIPTION;
                    _search.MDC_STATUS = data.MDC_STATUS;
                    _search.MDC_UPDATED_BY = data.MDC_UPDATED_BY;
                    _search.MDC_UPDATED_DATE = data.MDC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_DISTRIBUTION_CHANNEL data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_DISTRIBUTION_CHANNEL.Find(data.MDC_DC);
                #region Set Value
                _search.MDC_ZONE = data.MDC_ZONE;
                _search.MDC_DESCRIPTION = data.MDC_DESCRIPTION;
                _search.MDC_STATUS = data.MDC_STATUS;
                _search.MDC_UPDATED_BY = data.MDC_UPDATED_BY;
                _search.MDC_UPDATED_DATE = data.MDC_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MDC_DC)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_DISTRIBUTION_CHANNEL WHERE MDC_DC = '" + MDC_DC + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MDC_DC, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_DISTRIBUTION_CHANNEL WHERE MDC_DC = '" + MDC_DC + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
