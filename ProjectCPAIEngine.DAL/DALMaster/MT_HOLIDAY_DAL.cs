﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_HOLIDAY_DAL
    {
        public void Save(MT_HOLIDAY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_HOLIDAY.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_HOLIDAY data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_HOLIDAY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_HOLIDAY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_HOLIDAY.Find(data.MH_HOL_ID);
                    #region Set Value
                    _search.MH_HOL_DESC = data.MH_HOL_DESC;
                    //_search.MH_HOL_TYPE = data.MH_HOL_TYPE;
                    //_search.MH_HOL_DATE = data.MH_HOL_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_HOLIDAY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_HOLIDAY.Find(data.MH_HOL_ID);
                #region Set Value
                _search.MH_HOL_DESC = data.MH_HOL_DESC;
                //_search.MH_HOL_TYPE = data.MH_HOL_TYPE;
                //_search.MH_HOL_DATE = data.MH_HOL_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MH_HOL_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_HOLIDAY WHERE MH_HOL_ID = '" + MH_HOL_ID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MH_HOL_ID, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_HOLIDAY WHERE MH_HOL_ID = '" + MH_HOL_ID + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
