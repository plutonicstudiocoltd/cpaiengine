﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_INCOTERMS_DAL
    {
        public static List<MT_INCOTERMS> GetIncoterms()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_INCOTERMS
                            where v.MIN_STATUS == "ACTIVE" 
                            select v;
                return query.ToList();
            }
        }
    }
}
