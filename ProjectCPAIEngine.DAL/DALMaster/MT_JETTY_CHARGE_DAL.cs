﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_JETTY_CHARGE_DAL
    {
        public void Save(MT_JETTY_CHARGE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_JETTY_CHARGE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_JETTY_CHARGE data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_JETTY_CHARGE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_JETTY_CHARGE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_JETTY_CHARGE.Find(data.MJC_ROW_ID);
                    #region Set Value
                    _search.MJC_FK_MT_JETTY = data.MJC_FK_MT_JETTY;
                    _search.MJC_FK_MT_VEHICLE = data.MJC_FK_MT_VEHICLE;
                    _search.MJC_PORT_CHARGE = data.MJC_PORT_CHARGE;
                    _search.MJC_STATUS = data.MJC_STATUS;
                    _search.MJC_CREATE_TYPE = data.MJC_CREATE_TYPE;
                    _search.MJC_UPDATED_DATE = data.MJC_UPDATED_DATE;
                    _search.MJC_UPDATED_BY = data.MJC_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_JETTY_CHARGE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_JETTY_CHARGE.Find(data.MJC_ROW_ID);
                #region Set Value
                _search.MJC_FK_MT_JETTY = data.MJC_FK_MT_JETTY;
                _search.MJC_FK_MT_VEHICLE = data.MJC_FK_MT_VEHICLE;
                _search.MJC_PORT_CHARGE = data.MJC_PORT_CHARGE;
                _search.MJC_STATUS = data.MJC_STATUS;
                _search.MJC_CREATE_TYPE = data.MJC_CREATE_TYPE;
                _search.MJC_UPDATED_DATE = data.MJC_UPDATED_DATE;
                _search.MJC_UPDATED_BY = data.MJC_UPDATED_BY;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_JETTY_CHARGE WHERE MJC_ROW_ID = '" + Num + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Num, EntityCPAIEngine context)
        {
            try
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_JETTY_CHARGE WHERE MJC_ROW_ID = '" + Num + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
