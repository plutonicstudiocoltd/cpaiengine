﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_MATERIALS_CONTROL_DAL
    {
        public void Save(MT_MATERIALS_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_MATERIALS_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_MATERIALS_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_MATERIALS_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_MATERIALS_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_MATERIALS_CONTROL.Find(data.MMC_ROW_ID);
                    #region Set Value
                  
                    _search.MMC_SYSTEM = data.MMC_SYSTEM;
                    _search.MMC_NAME = data.MMC_NAME;
                    _search.MMC_UNIT_PRICE = data.MMC_UNIT_PRICE;
                    //_search.MMC_UNIT_VOLUME = data.MMC_UNIT_VOLUME;
                    _search.MMC_STATUS = data.MMC_STATUS;
                    _search.MMC_UPDATED_BY = data.MMC_UPDATED_BY;
                    _search.MMC_UPDATED_DATE = data.MMC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_MATERIALS_CONTROL data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.MT_MATERIALS_CONTROL.Find(data.MMC_ROW_ID);
                #region Set Value
                _search.MMC_SYSTEM = data.MMC_SYSTEM;
                _search.MMC_NAME = data.MMC_NAME;
                _search.MMC_UNIT_PRICE = data.MMC_UNIT_PRICE;
                //_search.MMC_UNIT_VOLUME = data.MMC_UNIT_VOLUME;
                _search.MMC_STATUS = data.MMC_STATUS;
                _search.MMC_UPDATED_BY = data.MMC_UPDATED_BY;
                _search.MMC_UPDATED_DATE = data.MMC_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_MATERIALS_CONTROL WHERE MMC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_MATERIALS_CONTROL WHERE MMC_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
