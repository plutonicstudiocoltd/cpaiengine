﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_MATERIALS_DAL
    {
        public void Save(MT_MATERIALS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_MATERIALS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_MATERIALS data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_MATERIALS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_MATERIALS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_MATERIALS.Find(data.MET_NUM);
                    #region Set Value

                    _search.MET_DIVISION = data.MET_DIVISION;
                    _search.MET_CRE_DATE = data.MET_CRE_DATE;
                    _search.MET_NAME_PER_CRE = data.MET_NAME_PER_CRE;
                    _search.MET_DATE_LAST = data.MET_DATE_LAST;
                    _search.MET_MAT_TYPE = data.MET_MAT_TYPE;
                    _search.MET_MAT_GROUP = data.MET_MAT_GROUP;
                    _search.MET_OLD_MAT_NUM = data.MET_OLD_MAT_NUM;
                    _search.MET_BASE_MEA = data.MET_BASE_MEA;
                    _search.MET_LAB_DESIGN_OFFICE = data.MET_LAB_DESIGN_OFFICE;
                    _search.MET_FLA_MAT_DELETION = data.MET_FLA_MAT_DELETION;
                    _search.MET_MAT_DES_THAI = data.MET_MAT_DES_THAI;
                    _search.MET_MAT_DES_ENGLISH = data.MET_MAT_DES_ENGLISH;
                    _search.MET_CRUDE_TYPE = data.MET_CRUDE_TYPE;
                    _search.MET_CRUDE_SRC = data.MET_CRUDE_SRC;
                    _search.MET_LOAD_PORT = data.MET_LOAD_PORT;
                    _search.MET_ORIG_CRTY = data.MET_ORIG_CRTY;
                    _search.MET_GRP_CODE = data.MET_GRP_CODE;
                    _search.MET_PRD_TYPE = data.MET_PRD_TYPE;
                    _search.MET_BL_SRC = data.MET_BL_SRC;
                    _search.MET_FK_PRD = data.MET_FK_PRD;
                    _search.MET_DEN_CONV = data.MET_DEN_CONV;
                    _search.MET_VFC_CONV = data.MET_VFC_CONV;
                    _search.MET_IO = data.MET_IO;
                    _search.MET_MAT_ADD = data.MET_MAT_ADD;
                    _search.MET_LORRY = data.MET_LORRY;
                    _search.MET_SHIP = data.MET_SHIP;
                    _search.MET_THAPP = data.MET_THAPP;
                    _search.MET_PIPE = data.MET_PIPE;
                    _search.MET_MAT_GROP_TYPE = data.MET_MAT_GROP_TYPE;
                    _search.MET_ZONE = data.MET_ZONE;
                    _search.MET_PORT = data.MET_PORT;
                    _search.MET_CREATED_DATE = data.MET_CREATED_DATE;
                    _search.MET_CREATED_BY = data.MET_CREATED_BY;
                    _search.MET_UPDATED_DATE = data.MET_UPDATED_DATE;
                    _search.MET_UPDATED_BY = data.MET_UPDATED_BY;
                    _search.MET_IO_TLB = data.MET_IO_TLB;
                    _search.MET_IO_TPX = data.MET_IO_TPX;
                    _search.MET_DOEB_CODE = data.MET_DOEB_CODE;
                    _search.MET_IO_LABIX = data.MET_IO_LABIX;
                    _search.MET_EXCISE_CODE = data.MET_EXCISE_CODE;
                    _search.MET_EXCISE_UOM = data.MET_EXCISE_UOM;
                    _search.MET_PRODUCT_DENSITY = data.MET_PRODUCT_DENSITY;
                    _search.MET_GL_ACCOUNT = data.MET_GL_ACCOUNT;
                    _search.MET_CREATE_TYPE = data.MET_CREATE_TYPE;

                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_MATERIALS data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.MT_MATERIALS.Find(data.MET_NUM);
                #region Set Value
                _search.MET_DIVISION = data.MET_DIVISION;
                _search.MET_CRE_DATE = data.MET_CRE_DATE;
                _search.MET_NAME_PER_CRE = data.MET_NAME_PER_CRE;
                _search.MET_DATE_LAST = data.MET_DATE_LAST;
                _search.MET_MAT_TYPE = data.MET_MAT_TYPE;
                _search.MET_MAT_GROUP = data.MET_MAT_GROUP;
                _search.MET_OLD_MAT_NUM = data.MET_OLD_MAT_NUM;
                _search.MET_BASE_MEA = data.MET_BASE_MEA;
                _search.MET_LAB_DESIGN_OFFICE = data.MET_LAB_DESIGN_OFFICE;
                _search.MET_FLA_MAT_DELETION = data.MET_FLA_MAT_DELETION;
                _search.MET_MAT_DES_THAI = data.MET_MAT_DES_THAI;
                _search.MET_MAT_DES_ENGLISH = data.MET_MAT_DES_ENGLISH;
                _search.MET_CRUDE_TYPE = data.MET_CRUDE_TYPE;
                _search.MET_CRUDE_SRC = data.MET_CRUDE_SRC;
                _search.MET_LOAD_PORT = data.MET_LOAD_PORT;
                _search.MET_ORIG_CRTY = data.MET_ORIG_CRTY;
                _search.MET_GRP_CODE = data.MET_GRP_CODE;
                _search.MET_PRD_TYPE = data.MET_PRD_TYPE;
                _search.MET_BL_SRC = data.MET_BL_SRC;
                _search.MET_FK_PRD = data.MET_FK_PRD;
                _search.MET_DEN_CONV = data.MET_DEN_CONV;
                _search.MET_VFC_CONV = data.MET_VFC_CONV;
                _search.MET_IO = data.MET_IO;
                _search.MET_MAT_ADD = data.MET_MAT_ADD;
                _search.MET_LORRY = data.MET_LORRY;
                _search.MET_SHIP = data.MET_SHIP;
                _search.MET_THAPP = data.MET_THAPP;
                _search.MET_PIPE = data.MET_PIPE;
                _search.MET_MAT_GROP_TYPE = data.MET_MAT_GROP_TYPE;
                _search.MET_ZONE = data.MET_ZONE;
                _search.MET_PORT = data.MET_PORT;
                _search.MET_CREATED_DATE = data.MET_CREATED_DATE;
                _search.MET_CREATED_BY = data.MET_CREATED_BY;
                _search.MET_UPDATED_DATE = data.MET_UPDATED_DATE;
                _search.MET_UPDATED_BY = data.MET_UPDATED_BY;
                _search.MET_IO_TLB = data.MET_IO_TLB;
                _search.MET_IO_TPX = data.MET_IO_TPX;
                _search.MET_DOEB_CODE = data.MET_DOEB_CODE;
                _search.MET_IO_LABIX = data.MET_IO_LABIX;
                _search.MET_EXCISE_CODE = data.MET_EXCISE_CODE;
                _search.MET_EXCISE_UOM = data.MET_EXCISE_UOM;
                _search.MET_PRODUCT_DENSITY = data.MET_PRODUCT_DENSITY;
                _search.MET_GL_ACCOUNT = data.MET_GL_ACCOUNT;
                _search.MET_CREATE_TYPE = data.MET_CREATE_TYPE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MET_NUM)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_MATERIALS WHERE MET_NUM = '" + MET_NUM + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string MET_NUM, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_MATERIALS WHERE MET_NUM = '" + MET_NUM + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
