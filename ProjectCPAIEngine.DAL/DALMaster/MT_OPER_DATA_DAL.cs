﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_OPER_DATA_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(MT_OPER_DATA data)
        {
            try
            {
                context.MT_OPER_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw innerEx;
            }
        }

        public void SaveAll(List<MT_OPER_DATA> dataAll)
        {
            try
            {
                context.MT_OPER_DATA.AddRange(dataAll);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw innerEx;
            }
        }

        public List<MT_OPER_PARAM> getDataByParam(string unit)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_OPER_PARAM.Where(c => c.MOP_FK_MT_OPER_UNIT == unit);
                return data.ToList();
            }
        }


        public void Delete(string paramRowId)
        {
            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_OPER_DATA WHERE MOD_FK_MT_OPER_PARAM  = '" + paramRowId + "'");
                    context.SaveChanges();
                    dbContextTransaction.Commit();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                    dbContextTransaction.Rollback();
                }
            };
        }

    }
}
