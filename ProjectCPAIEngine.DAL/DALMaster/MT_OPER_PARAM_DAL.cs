﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_OPER_PARAM_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(MT_OPER_PARAM data)
        {
            try
            {
                context.MT_OPER_PARAM.Add(data);
                try
                {
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                    throw innerException;
                }         
            }
            catch (Exception ex)
            {
                Exception innerEx = ex.InnerException != null ? ex.InnerException : ex;
                throw innerEx;
            }
        }




        public List<MT_OPER_PARAM> getParamDataByUnit(string unit)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_OPER_PARAM.Where(c => c.MOP_FK_MT_OPER_UNIT == unit);
                return data.ToList();
            }
        }




        public void Delete(string unitRowId)
        {
            try
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_OPER_PARAM WHERE MOP_FK_MT_OPER_UNIT = '" + unitRowId + "'");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;
                        dbContextTransaction.Rollback();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void Update(MT_OPER_PARAM data)
        {
            try
            {
                var _search = context.MT_OPER_PARAM.Find(data.MOP_ROW_ID);
                if (_search != null)
                {
                    _search.MOP_ORDER = data.MOP_ORDER != null ? data.MOP_ORDER : _search.MOP_ORDER;
                    _search.MOP_FK_MT_OPER_UNIT = data.MOP_FK_MT_OPER_UNIT != null ? data.MOP_FK_MT_OPER_UNIT : _search.MOP_FK_MT_OPER_UNIT;
                    _search.MOP_PARAMETER = data.MOP_PARAMETER != null ? data.MOP_PARAMETER : _search.MOP_PARAMETER;
                    _search.MOP_WEIGHT_FACTOR = data.MOP_WEIGHT_FACTOR != null ? data.MOP_WEIGHT_FACTOR : _search.MOP_WEIGHT_FACTOR;
                   
                    _search.MOP_UPDATED_BY = data.MOP_UPDATED_BY;
                    _search.MOP_UPDATED_DATE = data.MOP_UPDATED_DATE;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }                                
    }
}
