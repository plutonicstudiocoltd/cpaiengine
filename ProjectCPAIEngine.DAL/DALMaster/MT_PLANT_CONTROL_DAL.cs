﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_PLANT_CONTROL_DAL
    {
        public void Save(MT_PLANT_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_PLANT_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_PLANT_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_PLANT_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PLANT_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_PLANT_CONTROL.Find(data.MPC_ROW_ID);
                    #region Set Value
                    _search.MPC_FK_PLANT = data.MPC_FK_PLANT;
                    _search.MPC_SYSTEM = data.MPC_SYSTEM;
                    _search.MPC_STATUS = data.MPC_STATUS;
                    _search.MPC_UPDATED_BY = data.MPC_UPDATED_BY;
                    _search.MPC_UPDATED_DATE = data.MPC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_PLANT_CONTROL data, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.MT_PLANT_CONTROL.Find(data.MPC_ROW_ID);
                #region Set Value
                _search.MPC_FK_PLANT = data.MPC_FK_PLANT;
                _search.MPC_SYSTEM = data.MPC_SYSTEM;
                _search.MPC_STATUS = data.MPC_STATUS;
                _search.MPC_UPDATED_BY = data.MPC_UPDATED_BY;
                _search.MPC_UPDATED_DATE = data.MPC_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_PLANT_CONTROL WHERE MPC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_PLANT_CONTROL WHERE MPC_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByPortID(string PlantID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_PLANT_CONTROL where r.MPC_FK_PLANT == PlantID select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_PLANT_CONTROL.Remove(p);
                                }
                                context.SaveChanges();
                            }

                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByPortID(string PlantID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    var result = from r in context.MT_PLANT_CONTROL where r.MPC_FK_PLANT == PlantID select r;
                    //check if there is a record with this id

                    if (result.Count() > 0)
                    {

                        foreach (var p in result)
                        {
                            context.MT_PLANT_CONTROL.Remove(p);
                        }
                        context.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
