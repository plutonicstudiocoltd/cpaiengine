﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_SPEC_DATA_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(MT_SPEC_DATA data)
        {
            try
            {
                context.MT_SPEC_DATA.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }

        public void Update(MT_SPEC_DATA data)
        {
            try
            {
                var _search = context.MT_SPEC_DATA.Find(data.MSPD_ROW_ID);
                if (_search != null)
                {
                    _search.MSPD_FK_MT_SPEC = data.MSPD_FK_MT_SPEC;
                    _search.MSPD_HEADER = data.MSPD_HEADER;
                    _search.MSPD_KEY = data.MSPD_KEY;
                    _search.MSPD_KEY_CHILD = data.MSPD_KEY_CHILD;
                    _search.MSPD_TEST_METHOD = data.MSPD_TEST_METHOD;
                    _search.MSPD_SOURCE = data.MSPD_SOURCE;
                    _search.MSPD_CONSTRAINT = data.MSPD_CONSTRAINT;
                    _search.MSPD_REMARKS = data.MSPD_REMARKS;
                    _search.MSPD_ORDER = data.MSPD_ORDER;

                    _search.MSPD_MIN_1 = data.MSPD_MIN_1;
                    _search.MSPD_MIN_2 = data.MSPD_MIN_2;
                    _search.MSPD_MIN_3 = data.MSPD_MIN_3;
                    _search.MSPD_MIN_4 = data.MSPD_MIN_4;
                    _search.MSPD_MIN_5 = data.MSPD_MIN_5;
                    _search.MSPD_MIN_6 = data.MSPD_MIN_6;
                    _search.MSPD_MIN_7 = data.MSPD_MIN_7;
                    _search.MSPD_MIN_8 = data.MSPD_MIN_8;
                    _search.MSPD_MIN_9 = data.MSPD_MIN_9;
                    _search.MSPD_MIN_10 = data.MSPD_MIN_10;
                    _search.MSPD_MAX_1 = data.MSPD_MAX_1;
                    _search.MSPD_MAX_2 = data.MSPD_MAX_2;
                    _search.MSPD_MAX_3 = data.MSPD_MAX_3;
                    _search.MSPD_MAX_4 = data.MSPD_MAX_4;
                    _search.MSPD_MAX_5 = data.MSPD_MAX_5;
                    _search.MSPD_MAX_6 = data.MSPD_MAX_6;
                    _search.MSPD_MAX_7 = data.MSPD_MAX_7;
                    _search.MSPD_MAX_8 = data.MSPD_MAX_8;
                    _search.MSPD_MAX_9 = data.MSPD_MAX_9;
                    _search.MSPD_MAX_10 = data.MSPD_MAX_10;
                    _search.MSPD_UPDATED_BY = data.MSPD_UPDATED_BY;
                    _search.MSPD_UPDATED_DATE = data.MSPD_UPDATED_DATE;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }

        public void UpdateSpecDetail(string specRowId)
        {
            try
            {
                var _searchSpec = context.MT_SPEC_DATA.Where(c => c.MSPD_FK_MT_SPEC == specRowId).ToList();
                    foreach (var spec in _searchSpec)
                    {
                        spec.MSPD_FK_MT_SPEC = null;
                        context.SaveChanges();
                    }
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }

        }


        public void Delete(string RowId)
        {
            try
            {             
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_SPEC_DATA WHERE MSPD_FK_MT_SPEC = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }


        public List<MT_SPEC_DATA> getSpecDataBySpec(string spec)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_SPEC_DATA.Where(c => c.MSPD_FK_MT_SPEC == spec);
                return data.ToList();
            }
        }
    }
}
