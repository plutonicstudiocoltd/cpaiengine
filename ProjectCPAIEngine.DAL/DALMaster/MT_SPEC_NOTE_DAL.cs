﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_SPEC_NOTE_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(MT_SPEC_NOTE data)
        {
            try
            {
                context.MT_SPEC_NOTE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }

        public void Update(MT_SPEC_NOTE data)
        {
            try
            {
                var _search = context.MT_SPEC_NOTE.Find(data.MSPN_ROW_ID);
                if (_search != null)
                {
                    _search.MSPN_FK_MT_SPEC = data.MSPN_FK_MT_SPEC != null?data.MSPN_FK_MT_SPEC:_search.MSPN_FK_MT_SPEC;
                    _search.MSPN_NOTE = data.MSPN_NOTE != null ? data.MSPN_NOTE : _search.MSPN_NOTE;
                    _search.MSPN_EXPLANATION = data.MSPN_EXPLANATION != null ? data.MSPN_EXPLANATION : _search.MSPN_EXPLANATION;
                    _search.MSPN_IMAGE_PATH = data.MSPN_IMAGE_PATH != null ? data.MSPN_IMAGE_PATH : _search.MSPN_IMAGE_PATH;
                    _search.MSPN_ORDER = data.MSPN_ORDER != null ? data.MSPN_ORDER : _search.MSPN_ORDER;
                    _search.MSPN_UPDATED_BY = data.MSPN_UPDATED_BY;
                    _search.MSPN_UPDATED_DATE = data.MSPN_UPDATED_DATE;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }


        public void UpdateSpecNote(string specRowId)
        {
            try
            {
                var _searchSpec = context.MT_SPEC_NOTE.Where(c => c.MSPN_FK_MT_SPEC == specRowId).ToList();
                foreach (var spec in _searchSpec)
                {
                    spec.MSPN_FK_MT_SPEC = null;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }

        }

        public List<MT_SPEC_NOTE> getSpecNoteBySpec(string spec)
        {          
            var data = context.MT_SPEC_NOTE.Where(c => c.MSPN_FK_MT_SPEC == spec);
            return data.ToList();
        }




        public void Delete(string RowId)
        {
            try
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand("DELETE FROM MT_SPEC_NOTE WHERE MSPN_FK_MT_SPEC = '" + RowId + "'");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;
                        dbContextTransaction.Rollback();
                    }
                };
            }
            catch (Exception ex)
            {
                Exception innerException = ex.InnerException != null ? ex.InnerException : ex;
                throw innerException;
            }
        }
    }
}
