﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;


namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_UNIT_DAL
    {
        public void Save(MT_UNIT data)
        {
            try
            {
                using(var context = new EntityCPAIEngine())
                {
                    context.MT_UNIT.Add(data);
                    context.SaveChanges();
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_UNIT data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_UNIT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_UNIT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_UNIT.Find(data.MUN_ROW_ID);
                    #region Set Value
                    _search.MUN_NAME = data.MUN_NAME != null ? data.MUN_NAME : _search.MUN_NAME;
                    _search.MUN_ORDER = data.MUN_ORDER != null ? data.MUN_ORDER : _search.MUN_ORDER;
                    _search.MUN_USER_GROUP = data.MUN_USER_GROUP != null ? data.MUN_USER_GROUP : _search.MUN_USER_GROUP;
                    _search.MUN_STATUS = data.MUN_STATUS != null ? data.MUN_STATUS : _search.MUN_STATUS;
                    _search.MUN_QUESTION_FLAG = data.MUN_QUESTION_FLAG != null ? data.MUN_QUESTION_FLAG : _search.MUN_QUESTION_FLAG;
                    _search.MUN_UPDATED_BY = data.MUN_UPDATED_BY;
                    _search.MUN_UPDATED_DATE = data.MUN_UPDATED_DATE;
                    _search.MUN_FK_MT_AREA = data.MUN_FK_MT_AREA != null ? data.MUN_FK_MT_AREA : null;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateArea(string areaCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _searchArea = context.MT_UNIT.Where(c=>c.MUN_FK_MT_AREA == areaCode).ToList();
                    foreach(var area in _searchArea)
                    {
                        area.MUN_FK_MT_AREA = null;
                        context.SaveChanges();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public void Update(MT_UNIT data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.MT_UNIT.Find(data.MUN_ROW_ID);
                #region Set Value
                _search.MUN_NAME = data.MUN_NAME;
                _search.MUN_ORDER = data.MUN_ORDER;
                _search.MUN_USER_GROUP = data.MUN_USER_GROUP;
                _search.MUN_STATUS = data.MUN_STATUS;
                _search.MUN_UPDATED_BY = data.MUN_UPDATED_BY;
                _search.MUN_UPDATED_DATE = data.MUN_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_UNIT WHERE MUN_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_UNIT WHERE MUN_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getIDByName(string name)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                // Select All
                MT_UNIT query = (from v in context.MT_UNIT
                            where v.MUN_NAME == name
                            select v).FirstOrDefault();

                if (query != null)
                    return query.MUN_ROW_ID;
                else
                    return "";
            }
        }

        public static MT_UNIT getUnitByID(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_UNIT.Where(c => c.MUN_ROW_ID == id).FirstOrDefault();
                return data ?? new MT_UNIT();
            }
        }

        public MT_UNIT getUnit(string key)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_UNIT.Where(c => c.MUN_ROW_ID == key).FirstOrDefault();
                return data;
            }
        }

        public List<MT_UNIT> getUnitByArea(string area)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.MT_UNIT.Where(c => c.MUN_FK_MT_AREA == area);
                return data.ToList();
            }
        }

        public List<MT_UNIT> getUnitByAreaName(string area)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = (from v in context.MT_UNIT
                            join a in context.MT_AREA
                            on v.MUN_FK_MT_AREA equals a.MAR_ROW_ID
                            where a.MAR_NAME.ToUpper() == area.ToUpper()
                            select v).ToList();
                return data;
            }
        }


        public List<MT_UNIT> getAllUnits()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = (from v in context.MT_UNIT
                            where v.MUN_STATUS == "ACTIVE"
                            select v).ToList();
                return data;
            }
        }


        public bool ChkUnitByName(string name)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var unitName = context.MT_UNIT.Where(c => c.MUN_NAME == name).Any();
                if (!unitName)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool ChkUnitName(string unitCode, string newName)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var oldName = context.MT_UNIT.Where(c => c.MUN_ROW_ID == unitCode).SingleOrDefault().MUN_NAME;

                var name = context.MT_UNIT.Where(u => u.MUN_NAME == newName && u.MUN_NAME != oldName).Any();

                if (!name)
                {
                    return true;
                }
                else
                {
                    return false;
                }               
            }



            // Select All
            //MT_UNIT query = (from v in context.MT_UNIT
            //                 where v.MUN_NAME == name
            //                 select v).FirstOrDefault();

            //if (query != null)
            //    return query.MUN_ROW_ID;
            //else
            //    return "";
        }

    }
}
