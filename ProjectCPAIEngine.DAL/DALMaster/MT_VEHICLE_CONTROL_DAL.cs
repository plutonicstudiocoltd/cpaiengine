﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;
using System.Data.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VEHICLE_CONTROL_DAL
    {
        public void Save(MT_VEHICLE_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VEHICLE_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VEHICLE_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VEHICLE_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VEHICLE_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VEHICLE_CONTROL.Single(c => c.MMC_ROW_ID == data.MMC_ROW_ID);
                    #region Set Value
                    _search.MCC_SYSTEM = data.MCC_SYSTEM;
                    _search.MCC_TYPE = data.MCC_TYPE;
                    _search.MCC_STATUS = data.MCC_STATUS;
                    _search.MCC_UPDATED_BY = data.MCC_UPDATED_BY;
                    _search.MCC_UPDATED_DATE = data.MCC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VEHICLE_CONTROL data, EntityCPAIEngine context)
        {
            try
            {


                var _search = context.MT_VEHICLE_CONTROL.Single(c => c.MMC_ROW_ID == data.MMC_ROW_ID);
                #region Set Value
                _search.MCC_SYSTEM = data.MCC_SYSTEM;
                _search.MCC_TYPE = data.MCC_TYPE;
                _search.MCC_STATUS = data.MCC_STATUS;
                _search.MCC_UPDATED_BY = data.MCC_UPDATED_BY;
                _search.MCC_UPDATED_DATE = data.MCC_UPDATED_DATE;
                #endregion
                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VEHICLE_CONTROL WHERE MMC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {


                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM MT_VEHICLE_CONTROL WHERE MMC_ROW_ID = '" + RowID + "'");
                    context.SaveChanges();

                }
                catch (Exception ex)
                {
                    string res = ex.Message;

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
