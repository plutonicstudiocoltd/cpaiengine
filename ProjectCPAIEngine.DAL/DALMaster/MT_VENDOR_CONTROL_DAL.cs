﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VENDOR_CONTROL_DAL
    {
        public void Save(MT_VENDOR_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VENDOR_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VENDOR_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VENDOR_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VENDOR_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VENDOR_CONTROL.Find(data.MVC_ROW_ID);
                    #region Set Value
                    _search.MVC_FK_VENDOR = data.MVC_FK_VENDOR;
                    _search.MVC_SYSTEM = data.MVC_SYSTEM;
                    _search.MVC_COLOR_CODE = data.MVC_COLOR_CODE;
                    _search.MVC_TYPE = data.MVC_TYPE;
                    _search.MVC_NAME = data.MVC_NAME;
                    _search.MVC_STATUS = data.MVC_STATUS;
                    _search.MVC_UPDATED_BY = data.MVC_UPDATED_BY;
                    _search.MVC_UPDATED_DATE = data.MVC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VENDOR_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                
                    var _search = context.MT_VENDOR_CONTROL.Find(data.MVC_ROW_ID);
                    #region Set Value
                    _search.MVC_FK_VENDOR = data.MVC_FK_VENDOR;
                    _search.MVC_SYSTEM = data.MVC_SYSTEM;
                    _search.MVC_COLOR_CODE = data.MVC_COLOR_CODE;
                    _search.MVC_TYPE = data.MVC_TYPE;
                    _search.MVC_NAME = data.MVC_NAME;
                    _search.MVC_STATUS = data.MVC_STATUS;
                    _search.MVC_UPDATED_BY = data.MVC_UPDATED_BY;
                    _search.MVC_UPDATED_DATE = data.MVC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VENDOR_CONTROL WHERE MVC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
              
                  
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VENDOR_CONTROL WHERE MVC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                           
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            
                        }
                   
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByVendorCode(string VendorCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_VENDOR_CONTROL where r.MVC_FK_VENDOR == VendorCode select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_VENDOR_CONTROL.Remove(p);
                                }
                                context.SaveChanges();
                            }
                        
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByVendorCode(string VendorCode, EntityCPAIEngine context)
        {
            try
            {

                
                    try
                    {
                        var result = from r in context.MT_VENDOR_CONTROL where r.MVC_FK_VENDOR == VendorCode select r;
                        //check if there is a record with this id

                        if (result.Count() > 0)
                        {

                            foreach (var p in result)
                            {
                                context.MT_VENDOR_CONTROL.Remove(p);
                            }
                            context.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;

                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
