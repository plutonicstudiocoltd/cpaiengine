﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MT_VESSEL_ACTIVITY_CONTROL_DAL
    {
        public void Save(MT_VESSEL_ACTIVITY_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.MT_VESSEL_ACTIVITY_CONTROL.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(MT_VESSEL_ACTIVITY_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                context.MT_VESSEL_ACTIVITY_CONTROL.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VESSEL_ACTIVITY_CONTROL data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.MT_VESSEL_ACTIVITY_CONTROL.Find(data.MAC_ROW_ID);
                    #region Set Value
                    _search.MAC_FK_VESSEL_ACTIVITY = data.MAC_FK_VESSEL_ACTIVITY;
                    _search.MAC_SYSTEM = data.MAC_SYSTEM;
                    _search.MAC_COLOR_CODE = data.MAC_COLOR_CODE;
                    _search.MAC_TYPE = data.MAC_TYPE;
                    _search.MAC_STATUS = data.MAC_STATUS;
                    _search.MAC_UPDATED_BY = data.MAC_UPDATED_BY;
                    _search.MAC_UPDATED_DATE = data.MAC_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(MT_VESSEL_ACTIVITY_CONTROL data, EntityCPAIEngine context)
        {
            try
            {
                
                var _search = context.MT_VESSEL_ACTIVITY_CONTROL.Find(data.MAC_ROW_ID);
                #region Set Value
                _search.MAC_FK_VESSEL_ACTIVITY = data.MAC_FK_VESSEL_ACTIVITY;
                _search.MAC_SYSTEM = data.MAC_SYSTEM;
                _search.MAC_COLOR_CODE = data.MAC_COLOR_CODE;
                _search.MAC_TYPE = data.MAC_TYPE;
                _search.MAC_STATUS = data.MAC_STATUS;
                _search.MAC_UPDATED_BY = data.MAC_UPDATED_BY;
                _search.MAC_UPDATED_DATE = data.MAC_UPDATED_DATE;
                #endregion
                context.SaveChanges();
              
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VESSEL_ACTIVITY_CONTROL WHERE MAC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowID, EntityCPAIEngine context)
        {
            try
            {
              
                  
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM MT_VESSEL_ACTIVITY_CONTROL WHERE MAC_ROW_ID = '" + RowID + "'");
                            context.SaveChanges();
                           
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            
                        }
                   
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByVesselActivityCode(string VesselActivityCode)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            var result = from r in context.MT_VESSEL_ACTIVITY_CONTROL where r.MAC_FK_VESSEL_ACTIVITY == VesselActivityCode select r;
                            //check if there is a record with this id

                            if (result.Count() > 0)
                            {

                                foreach (var p in result)
                                {
                                    context.MT_VESSEL_ACTIVITY_CONTROL.Remove(p);
                                }
                                context.SaveChanges();
                            }
                        
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByVesselActivityCode(string VesselActivityCode, EntityCPAIEngine context)
        {
            try
            {

                
                    try
                    {
                        var result = from r in context.MT_VESSEL_ACTIVITY_CONTROL where r.MAC_FK_VESSEL_ACTIVITY == VesselActivityCode select r;
                        //check if there is a record with this id

                        if (result.Count() > 0)
                        {

                            foreach (var p in result)
                            {
                                context.MT_VESSEL_ACTIVITY_CONTROL.Remove(p);
                            }
                            context.SaveChanges();
                        }

                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;

                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
