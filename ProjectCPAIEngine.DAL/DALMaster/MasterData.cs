﻿using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class MasterData
    {
        public static List<MT_VEHICLE> GetVehicle()
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                return context.MT_VEHICLE.ToList();
            }
        }

        public static string GetJsonMasterSetting(string KeyName)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {

                var _Gconfig = context.GLOBAL_CONFIG.Where(x => x.GCG_KEY == KeyName).ToList();
                if (_Gconfig != null && _Gconfig.Count > 0)
                {
                    return _Gconfig[0].GCG_VALUE;
                }
                else
                {
                    return "";
                }
            }
        }
         
        public static string GetJsonGlobalConfig(string KeyName)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var _Gconfig = context.GLOBAL_CONFIG.Where(x => x.GCG_KEY == KeyName).ToList();
                if (_Gconfig != null && _Gconfig.Count > 0)
                {
                    return _Gconfig[0].GCG_VALUE;
                }
                else
                {
                    return "";
                }
            }
        }


        public static List<GLOBAL_CONFIG> GetConfigData(List<CPAI_USER_GROUP> counterKey, string functionKey)
        {
            List<GLOBAL_CONFIG> tempData = new List<GLOBAL_CONFIG>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                foreach (CPAI_USER_GROUP i in counterKey)
                {
                    string groupKey = "COUNTER_" + i.USG_USER_GROUP;
                    var _Gconfig = context.GLOBAL_CONFIG.Where(x => x.GCG_KEY == groupKey || x.GCG_KEY == functionKey).ToList();
                    if (_Gconfig != null && _Gconfig.Count > 0)
                    {
                        foreach (GLOBAL_CONFIG j in _Gconfig)
                        {
                            tempData.Add(j);
                        }

                    }
                }
            }
            return tempData;

        }

        public static List<GLOBAL_CONFIG> GetConfigData( string key)
        {
            List<GLOBAL_CONFIG> tempData = new List<GLOBAL_CONFIG>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
     
                    var _Gconfig = context.GLOBAL_CONFIG.Where(x => x.GCG_KEY == key).ToList();
                    if (_Gconfig != null && _Gconfig.Count > 0)
                    {
                        foreach (GLOBAL_CONFIG j in _Gconfig)
                        {
                            tempData.Add(j);
                        }

                    }
                
            }
            return tempData;

        }



        //select VEH_VEHICLE, VEH_VEH_TEXT from MT_VEHICLE
        //where VEH_ID in(
        //select MCC_FK_VEHICLE from MT_VEHICLE_CONTROL
        //where MCC_SYSTEM = 'CPAI'
        //and MCC_STATUS = 'ACTIVE'
        //and MCC_TYPE = 'PRODUCT'
        //
        //Crude >> system = CPAI , status = ACTIVE , type = CRUDE
        //Product >> system = CPAI , status = ACTIVE , type = PRODUCT
        public static List<MT_VEHICLE> GetVehicle(string system, string status, string type)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VEHICLE
                            join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE
                            where vc.MCC_SYSTEM.ToUpper() == system && vc.MCC_STATUS.ToUpper() == status && vc.MCC_TYPE.ToUpper() == type
                            select v;
                return query.ToList();
            }
        }

        //-- VENDOR
        //select VND_ACC_NUM_VENDOR ,VND_ACC_NUM_VENDOR from MT_VENDOR
        //where VND_ACC_NUM_VENDOR in
        //(select MVC_FK_VENDOR from MT_VENDOR_CONTROL
        //where MVC_SYSTEM = 'CPAI'
        //and MVC_STATUS = 'ACTIVE')
        //--show VND_ACC_NUM_VENDOR
        //system = CPAI , status = ACTIVE
        public static List<MT_VENDOR> GetVendor(string system, string status)
        {

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_VENDOR
                            join vc in context.MT_VENDOR_CONTROL on v.VND_ACC_NUM_VENDOR equals vc.MVC_FK_VENDOR
                            where vc.MVC_SYSTEM.ToUpper() == system && vc.MVC_STATUS.ToUpper() == status
                            select v;
                return query.ToList();
            }
        }

        public static string GetSeqBunker()
        {
            string _data = "0000";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int64>("select BUNKER_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqCharterIn()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select CHAR_IN_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqDAF()
        {
            string _data = "0000";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int64>("select DAF_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqCDS()
        {
            string _data = "0000";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int64>("select CDS_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqCDO()
        {
            string _data = "0000";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int64>("select CDO_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }



        public static string GetSeqCharterOut()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select CHAR_OUT_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqFreight()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select FREIGHT_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static List<string> GetAllUer(string UserName, string System)
        {
            //UserGroupDAL objUserGroup = new UserGroupDAL();
            //CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(UserName, System);
            //List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            //return findUserByGroup.Select(x => x.USR_LOGIN.ToUpper()).Distinct().ToList();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            List<USERS> findUserBySystem = objUserGroup.findUserBySystem(System);
            return findUserBySystem.Select(x => x.USR_LOGIN.ToUpper()).Distinct().ToList();
        }

        public static List<USERS> GetAllUserForDelegate(string UserName, string System)
        {
            UserGroupDAL objUserGroup = new UserGroupDAL();
            List<USERS> lstUsers = new List<USERS>();
            var usrs = objUserGroup.findUserBySystem(null)
                .GroupBy(d => new { d.USR_ROW_ID, d.USR_LOGIN })
                .Select(m => new { m.Key.USR_ROW_ID, m.Key.USR_LOGIN })
                .OrderBy(x => x.USR_LOGIN);
            if (usrs != null)
            {
                foreach (var usr in usrs)
                {
                    USERS u = new USERS();
                    u.USR_ROW_ID = usr.USR_ROW_ID;
                    u.USR_LOGIN = usr.USR_LOGIN;
                    lstUsers.Add(u);
                }
            }
            return lstUsers;
        }

        public static string GetSeqSchedule()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select SCH_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static List<MENU> GetLinkDirect(string MEUROWID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MENU
                            where v.MEU_ROW_ID.ToUpper() == MEUROWID.ToUpper() && v.MEU_ACTIVE == "ACTIVE"
                            select v;
                return query.ToList();
            }
        }

        public static string GetSeqTce()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select TCE_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqTceMk()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select TCE_MK_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSequences(string _seq, string _default = "0000")
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select " + _seq + ".NEXTVAL from dual" ).ToList();
                if (data.Count <= 0) 
                {
                    throw new Exception("Unable to generate Trans ID Sequence");
                }
                else
                {
                    _data = data[0].ToString(_default);
                }
                return _data;
            }
        }

        public List<CPAI_USER_GROUP> getUserList(string usrLogin, string usrSystem)
        {
            DateTime date = DateTime.Now.Date;
            List<CPAI_USER_GROUP> data = new List<CPAI_USER_GROUP>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from u in context.USERS
                            join ug in context.CPAI_USER_GROUP on u.USR_ROW_ID equals ug.USG_FK_USERS
                            where u.USR_LOGIN.ToUpper() == usrLogin.ToUpper() && ug.USG_STATUS.ToUpper() == "ACTIVE"
                            && ug.USG_USER_SYSTEM.ToUpper() == usrSystem.ToUpper()
                            && ((ug.USG_AFFECT_DATE <= date && ug.USG_END_DATE >= date) || ug.USG_DELEGATE_FLAG == null)  //07082017 add by job for delegate 
                            group ug by ug.USG_USER_GROUP into g
                            select new
                            {
                                USG_USER_GROUP = g.Key
                            };

                for (int i = 0; i < query.ToList().Count; i++)
                {
                    CPAI_USER_GROUP s = new CPAI_USER_GROUP();
                    s.USG_USER_GROUP = query.ToList()[i].USG_USER_GROUP;
                    data.Add(s);
                }

                return data;
            }
        }

        public static string GetSeqCDPH()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select CRUDE_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqCOOL()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select COOL_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqVCOOL()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select VCOOL_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqPAF()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select PAF_NEW_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }

        public static string GetSeqHGTOOL()
        {
            string _data = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select HG_TOOL_SEQ.NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("0000");
                return _data;
            }
        }
    }
}
