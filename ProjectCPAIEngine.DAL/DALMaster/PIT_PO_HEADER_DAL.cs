﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PIT_PO_HEADER_DAL
    {
        public void Save(PIT_PO_HEADER data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PIT_PO_HEADER.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PIT_PO_HEADER data, EntityCPAIEngine context)
        {
            try
            {
                context.PIT_PO_HEADER.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PIT_PO_HEADER data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.PIT_PO_HEADER.Find(data.PO_NO);

                #region Set Value

                //_search.PO_NO = data.PO_NO;
                //_search.COMPANY_CODE = data.COMPANY_CODE;
                //_search.ACC_NUM_VENDOR = data.ACC_NUM_VENDOR;
                //_search.PURCHASING_GROUP = data.PURCHASING_GROUP;
                //_search.CURRENCY = data.CURRENCY;
                //_search.FX_BOT = data.FX_BOT;
                //_search.PO_DATE = data.PO_DATE;
                //_search.INCOTERMS = data.INCOTERMS;
                //_search.ORIGIN_COUNTRY = data.ORIGIN_COUNTRY;
                //_search.CARGO_NO = data.CARGO_NO;
                //_search.SO_NO = data.SO_NO;
                //_search.CREATED_DATE = data.CREATED_DATE;
                //_search.CREATED_BY = data.CREATED_BY;\
                _search.IS_DELETE = data.IS_DELETE;
                _search.UPDATED_DATE = data.UPDATED_DATE;
                _search.UPDATED_BY = data.UPDATED_BY;

                #endregion

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
