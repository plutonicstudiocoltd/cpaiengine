﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PIT_PO_ITEM_DAL
    {
        public void Save(PIT_PO_ITEM data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PIT_PO_ITEM.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PIT_PO_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                context.PIT_PO_ITEM.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PIT_PO_ITEM data, EntityCPAIEngine context)
        {
            try
            {
                object[] key = { data.PO_ITEM, data.PO_NO };
                var _search = context.PIT_PO_ITEM.Find(key);

                #region Set Value

                //_search.SHORT_TEXT = data.SHORT_TEXT;
                //_search.MET_NUM = data.MET_NUM;
                //_search.PLANT = data.PLANT;
                //_search.STORAGE_LOCATION = data.STORAGE_LOCATION;
                //_search.TRIP_NO = data.TRIP_NO;
                _search.VOLUME = data.VOLUME;
                _search.VOLUME_UNIT = data.VOLUME_UNIT;
                _search.UNIT_PRICE = data.UNIT_PRICE;
                _search.CURRENCY = data.CURRENCY;
                _search.UPDATED_DATE = data.UPDATED_DATE;
                _search.UPDATED_BY = data.UPDATED_BY;
                //_search.ACC_NUM_VENDOR = data.ACC_NUM_VENDOR;
                //_search.TRAN_ID = data.TRAN_ID;
                //_search.CARGO_NO = data.CARGO_NO;
                //_search.SO_USERNAME = data.SO_USERNAME;

                #endregion

                context.SaveChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
