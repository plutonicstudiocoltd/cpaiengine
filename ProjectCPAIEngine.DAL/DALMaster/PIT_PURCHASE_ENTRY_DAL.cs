﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class PIT_PURCHASE_ENTRY_DAL
    {
        public void Save(PIT_PURCHASE_ENTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PIT_PURCHASE_ENTRY.Find(data.PO_NO, data.PO_ITEM, data.ITEM_NO);
                    if (search == null) //Add new record
                    {
                        data.UPDATED_DATE = null;
                        data.UPDATED_BY = null;

                        context.PIT_PURCHASE_ENTRY.Add(data);
                        context.SaveChanges();
                    }
                    else
                    {
                        search.BL_VOLUME_BBL = data.BL_VOLUME_BBL;
                        search.BL_VOLUME_MT = data.BL_VOLUME_MT;
                        search.BL_VOLUME_L30 = data.BL_VOLUME_L30;
                        search.GR_VOLUME_BBL = data.GR_VOLUME_BBL;
                        search.GR_VOLUME_MT = data.GR_VOLUME_MT;
                        search.GR_VOLUME_L30 = data.GR_VOLUME_L30;
                        search.AMOUNT_THB = data.AMOUNT_THB;
                        search.AMOUNT_USD = data.AMOUNT_USD;
                        search.TOTAL_THB = data.TOTAL_THB;
                        search.TOTAL_USD = data.TOTAL_USD;
                        search.VAT = data.VAT;
                        search.CAL_INVOICE_FIGURE = data.CAL_INVOICE_FIGURE;
                        search.CAL_PRICE_UNIT = data.CAL_PRICE_UNIT;
                        search.CAL_VOLUME_UNIT = data.CAL_VOLUME_UNIT;
                        search.PRICE = data.PRICE;
                        search.ROE = data.ROE;
                        search.UPDATED_DATE = DateTime.Now;
                        search.UPDATED_BY = data.CREATED_BY;
                        search.BL_DATE = data.BL_DATE;
                        search.GR_DATE = data.GR_DATE;
                        search.POSTING_DATE = data.POSTING_DATE;

                        context.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PIT_PURCHASE_ENTRY data, EntityCPAIEngine context)
        {
            try
            {
                context.PIT_PURCHASE_ENTRY.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PIT_PURCHASE_ENTRY data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.PIT_PURCHASE_ENTRY.Find(data.PO_NO, data.PO_ITEM, data.ITEM_NO);
                _search.SAP_FI_DOC_NO = null;
                _search.SAP_FI_DOC_STATUS = "Reversed";
                _search.UPDATED_DATE = DateTime.Now;
                _search.UPDATED_BY = data.UPDATED_BY;

                context.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        public void UpdateSapFiDocNo(string poNo, string poItem, string itemNo, string sapFiDocNo, string userName, string type)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PIT_PURCHASE_ENTRY.Find(poNo, Convert.ToDecimal(poItem), Convert.ToDecimal(itemNo));
                    if (search != null) //Add new record
                    {
                        if (type == "C")
                        {
                            sapFiDocNo = sapFiDocNo.Substring(0, sapFiDocNo.Length - 8);
                        }
                        search.SAP_FI_DOC_NO = sapFiDocNo;
                        search.SAP_FI_DOC_STATUS = "Created";
                        search.UPDATED_DATE = DateTime.Now;
                        search.UPDATED_BY = userName;
                        context.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Cannot find PO_NO = " + poNo + ", PO_ITEM = " + poItem + ", ITEM-NO = " + itemNo);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Remove(PIT_PURCHASE_ENTRY data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var search = context.PIT_PURCHASE_ENTRY.Find(data.PO_NO, data.PO_ITEM, data.ITEM_NO);
                    if(search != null)
                    {
                        context.PIT_PURCHASE_ENTRY.Remove(search);
                        context.SaveChanges();
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}
