﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALMaster
{
    public class ROLE_DAL
    {
        public void Save(ROLE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.ROLE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(ROLE data, EntityCPAIEngine context)
        {
            try
            {
                context.ROLE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ROLE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.ROLE.Find(data.ROL_ROW_ID);
                    #region Set Value
                    _search.ROL_NAME = data.ROL_NAME;
                    _search.ROL_DESCRIPTION = data.ROL_DESCRIPTION;
                    _search.ROL_STATUS = data.ROL_STATUS;
                    _search.ROL_UPDATED_BY = data.ROL_UPDATED_BY;
                    _search.ROL_UPDATED_DATE = data.ROL_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ROLE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.ROLE.Find(data.ROL_ROW_ID);
                #region Set Value
                _search.ROL_NAME = data.ROL_NAME;
                _search.ROL_DESCRIPTION = data.ROL_DESCRIPTION;
                _search.ROL_STATUS = data.ROL_STATUS;
                _search.ROL_UPDATED_BY = data.ROL_UPDATED_BY;
                _search.ROL_UPDATED_DATE = data.ROL_UPDATED_DATE;
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM ROLE WHERE ROL_ROW_ID = '" + RowId + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string RowId, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM ROLE WHERE ROL_ROW_ID = '" + RowId + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
