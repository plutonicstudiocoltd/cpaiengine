﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALMaster
{
   public class USERS_ROLE_DAL
    {
        public void Save(USER_ROLE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.USER_ROLE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(USER_ROLE data, EntityCPAIEngine context)
        {
            try
            {
                context.USER_ROLE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(USER_ROLE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.USER_ROLE.Find(data.URO_ROW_ID);
                    #region Set Value
                    _search.URO_FK_ROLE = data.URO_FK_ROLE;
                    _search.URO_FK_USER = data.URO_FK_USER;
                    _search.URO_UPDATED_BY = data.URO_UPDATED_BY;
                    _search.URO_UPDATED_DATE = data.URO_UPDATED_DATE;

                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(USER_ROLE data, EntityCPAIEngine context)
        {
            try
            {
                var _search = context.USER_ROLE.Find(data.URO_ROW_ID);
                #region Set Value
                _search.URO_FK_ROLE = data.URO_FK_ROLE;
                _search.URO_FK_USER = data.URO_FK_USER;
                _search.URO_UPDATED_BY = data.URO_UPDATED_BY;
                _search.URO_UPDATED_DATE = data.URO_UPDATED_DATE;

                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string id)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM USER_ROLE WHERE URO_FK_USER = '" + id + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string id, EntityCPAIEngine context)
        {
            try
            {

                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM USER_ROLE WHERE URO_FK_USER = '" + id + "'");
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string res = ex.Message;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
