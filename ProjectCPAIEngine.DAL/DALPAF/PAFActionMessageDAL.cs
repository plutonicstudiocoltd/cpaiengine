﻿using com.pttict.engine.dal.Service;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAFActionMessageDAL : ActionMessageDAL
    {
        public List<CPAI_ACTION_MESSAGE> findUserGroupMail(string action, string system, string type, string funciton, string current_status)
        {
            FunctionService s = new FunctionService();
            com.pttict.engine.dal.Entity.FUNCTIONS f = s.GetFUNCTIONByFuncID(funciton);
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_ACTION_MESSAGE> results = entity.CPAI_ACTION_MESSAGE.Where(x =>
                x.AMS_ACTION.ToUpper() == action.ToUpper() &&
                x.AMS_SYSTEM.ToUpper() == system.ToUpper() &&
                x.AMS_TYPE.ToUpper() == type.ToUpper() &&
                x.AMS_FK_FUNCTION.ToUpper() == f.FNC_ROW_ID.ToUpper() &&
                (
                    x.AMS_CURRENT_STATUS != null &&
                    (
                        x.AMS_CURRENT_STATUS.Contains(current_status) ||
                        x.AMS_CURRENT_STATUS == "ALL"
                    )
                ) &&
                x.AMS_MAIL_FLAG.ToUpper() == "Y".ToUpper() &&
                x.AMS_STATUS == "ACTIVE").OrderBy(o => o.AMS_USR_GROUP).ToList();

                if (results != null && results.Count() > 0)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_ACTION_MESSAGE>();
                }
            }
        }

    }
}
