﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALCDS;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAFHelper
    {
        public static String ShowTolerance(String text)
        {
            if (text == null)
            {
                return "";
            }
            if (text.Contains("+/-"))
            {
                return text.Replace("+/-", "±") + "%";
            }
            return text;

        }
        public static String GetMinMaxFormat(Nullable<Decimal> min, Nullable<Decimal> max)
        {
            if (max == null)
            {
                return string.Format("{0:n0}", min);
            }
            if (min != max)
            {
                return string.Format("{0:n0}", min) + " - " + string.Format("{0:n0}", max);
            }
            else
            {
                return string.Format("{0:n0}", min);
            }
        }
        public static String ShowPrice(String floatPrice, Nullable<decimal> fixedPrice)
        {
            if (floatPrice != null)
            {
                return floatPrice.Replace("|", " ").Replace("+ 0", "flat");
            }
            if (fixedPrice != null)
            {
                return String.Format("{0:n}", fixedPrice);
            }
            return "";
        }

        public static string ToDateString(string date)
        {
            try
            {
                DateTime d = DateTime.Parse(date, System.Globalization.CultureInfo.InvariantCulture);

                return d.ToString("dd-MMM-yyyy HH:mm");
            }
            catch
            {
                return "";
            }
        }

        public static string ToDateString(DateTime? date)
        {
            try
            {
                //DateTime d = DateTime.Parse(date, System.Globalization.CultureInfo.InvariantCulture);

                return ((DateTime)date).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                return "";
            }
        }

        public static string ToOnlyDateString(DateTime? date)
        {
            try
            {
                //DateTime d = DateTime.Parse(date, System.Globalization.CultureInfo.InvariantCulture);

                return ((DateTime)date).ToString("dd/MMM/yyyy");
            }
            catch
            {
                return "";
            }
        }

        public static string GetUserGroup(string username, string system)
        {
            username = username ?? "";
            username = username.Trim();
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT cug.USG_USER_GROUP FROM CPAI_USER_GROUP cug INNER JOIN USERS u ON UPPER(cug.USG_FK_USERS) = UPPER(u .USR_ROW_ID) WHERE UPPER(u .USR_LOGIN) = UPPER(:username) AND UPPER(cug.USG_USER_SYSTEM) = UPPER(:system) AND UPPER(cug.USG_STATUS) = 'ACTIVE'";
            string usergroup = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("username", username), new OracleParameter("system", system) }).FirstOrDefault();
            return usergroup ?? "";
        }

        public static string GetSeqId(string table)
        {
            string _data = "";
            
            if(table == "PAF_BIDDING_ITEMS_BENCHMARK")
            {
                table = "PAF_BIDDING_ITEMS_BENCHMARK_S";
            }
            else
            {
                table += "_SEQ";
            }
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.Database.SqlQuery<Int32>("select " + table + ".NEXTVAL from dual").ToList();
                if (data.Count <= 0) throw new Exception("Unable to generate Trans ID Sequence");
                else _data = data[0].ToString("00000000000000");
                return _data;
            }
        }

        public static string GetId()
        {
            return Guid.NewGuid().ToString("N");
            //return DateTime.Now.ToString("yyyyMMddHHmmssffff");
        }
        public static T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        public static object CloneX<T>(T source)
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(ms, source);
            ms.Position = 0;
            object obj = bf.Deserialize(ms);
            ms.Close();
            return obj;
        }

        public static void log(string message)
        {
            string filePath = @"~\Log\PAF_ERROR.txt";
            filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\" + DateTime.Now.ToString("yyyy-MM-dd-") + "PAF_ERROR.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("<br/>" + Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                writer.WriteLine("Message :" + message + "<br/>" + Environment.NewLine);
            }
        }


        public static void log(params string[] messages)
        {
            string filePath = @"~\Log\PAF_ERROR.txt";
            filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\" + DateTime.Now.ToString("yyyy-MM-dd-") + "PAF_ERROR.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                string message = String.Join("<br/>" + Environment.NewLine, messages);
                writer.WriteLine("<br/>" + Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                writer.WriteLine("Message :" + message + "<br/>" + Environment.NewLine);
            }
        }
        public static void LogUserInput(PAF_DATA data)
        {
            string out_put_dir = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\PRODUCT\\" + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
            Directory.CreateDirectory(out_put_dir);
            string filePath = out_put_dir + "USER_INPUT.txt";
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                string json = JsonConvert.SerializeObject(data, microsoftDateFormatSettings);
                writer.WriteLine(json);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

        }

        public static void LogUserInput(DAF_DATA data)
        {
            string out_put_dir = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\DEMURRAGE\\" + data.USER_GROUP + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
            Directory.CreateDirectory(out_put_dir);
            string filePath = out_put_dir + "USER_INPUT.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                string json = JsonConvert.SerializeObject(data, Formatting.Indented);
                writer.WriteLine(json);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

        }

        public static void LogUserInput(CDS_DATA data)
        {
            string out_put_dir = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\CRUDE_SALE\\" + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
            Directory.CreateDirectory(out_put_dir);
            string filePath = out_put_dir + "USER_INPUT.txt";
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                string json = JsonConvert.SerializeObject(data, microsoftDateFormatSettings);
                writer.WriteLine(json);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

        }

        public static void LogUserInput(CDO_DATA data)
        {
            string out_put_dir = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\CRUDE_REOPTIMIZE\\" + DateTime.Now.ToString("yyyy-MM-dd") + "\\";
            Directory.CreateDirectory(out_put_dir);
            string filePath = out_put_dir + "USER_INPUT.txt";
            var microsoftDateFormatSettings = new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                Formatting = Formatting.Indented,
                //DateTimeZoneHandling = DateTimeZoneHandling.Local,
                //DateParseHandling = DateParseHandling.DateTimeOffset
            };
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                string json = JsonConvert.SerializeObject(data, microsoftDateFormatSettings);
                writer.WriteLine(json);
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
            }

        }

        public static void log(dynamic ex)
        {
            string filePath = @"~\Log\PAF_ERROR.txt";
            filePath = System.AppDomain.CurrentDomain.BaseDirectory + "\\Log\\" + DateTime.Now.ToString("yyyy-MM-dd-") + "PAF_ERROR.txt";
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString());
                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                try
                {
                    List<string> errorMessages = new List<string>();
                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            writer.WriteLine(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                        }
                    }
                }catch 
                {
                    
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                    writer.WriteLine(ex.ToString());
                    writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                }

            }
        }

        public static PAF_DATA_WRAPPER CreateWarper(PAF_DATA source)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            db.Configuration.ProxyCreationEnabled = false;

            PAF_DATA_WRAPPER target = new PAF_DATA_WRAPPER();
            target.PDA_ROW_ID = source.PDA_ROW_ID;
            target.PDA_FORM_ID = source.PDA_FORM_ID;
            target.PDA_TEMPLATE = source.PDA_TEMPLATE;
            target.PDA_FOR_COMPANY = source.PDA_FOR_COMPANY;
            target.PDA_TYPE = source.PDA_TYPE;
            target.PDA_SUB_TYPE = source.PDA_SUB_TYPE;
            target.PDA_TYPE_NOTE = source.PDA_TYPE_NOTE;
            target.PDA_BRIEF = source.PDA_BRIEF;
            target.PDA_PRICE_UNIT = source.PDA_PRICE_UNIT;
            target.PDA_PRICING_PERIOD = source.PDA_PRICING_PERIOD;
            target.PDA_SPECIAL_TERMS_OR_CONDITION = source.PDA_SPECIAL_TERMS_OR_CONDITION;
            target.PDA_NOTE = source.PDA_NOTE;
            target.PDA_STATUS = source.PDA_STATUS;
            target.PDA_CREATED_BY = source.PDA_CREATED_BY;
            target.PDA_UPDATED_BY = source.PDA_UPDATED_BY;
            target.PDA_INCLUDED_REVIEW_TABLE = source.PDA_INCLUDED_REVIEW_TABLE;
            target.PDA_INCLUDED_BIDDING_TABLE = source.PDA_INCLUDED_BIDDING_TABLE;
            target.PDA_CONTRACT_DATE_FROM = source.PDA_CONTRACT_DATE_FROM;
            target.PDA_CONTRACT_DATE_TO = source.PDA_CONTRACT_DATE_TO;
            target.PDA_LOADING_DATE_FROM = source.PDA_LOADING_DATE_FROM;
            target.PDA_LOADING_DATE_TO = source.PDA_LOADING_DATE_TO;
            target.PDA_DISCHARGING_DATE_FROM = source.PDA_DISCHARGING_DATE_FROM;
            target.PDA_DISCHARGING_DATE_TO = source.PDA_DISCHARGING_DATE_TO;
            target.PDA_CREATED = source.PDA_CREATED;
            target.PDA_UPDATED = source.PDA_UPDATED;
            target.PDA_REASON = source.PDA_REASON;
            target.PDA_TOPIC = source.PDA_TOPIC;
            target.IS_DISABLED = source.PDA_STATUS != "DRAFT";
            target.IS_DISABLED_NOTE = (source.PDA_STATUS == "APPROVED" || source.PDA_STATUS == "CANCEL");

            target.PAF_ATTACH_FILE = db.PAF_ATTACH_FILE.Where(x => x.PAT_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PAT_ROW_ID).ToList();
            target.PAF_BIDDING_ITEMS = db.PAF_BIDDING_ITEMS.Where(x => x.PBI_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PBI_ROW_ID).ToList();
            foreach (PAF_BIDDING_ITEMS elem in target.PAF_BIDDING_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                elem.PAF_ROUND = db2.PAF_ROUND.Where(x => x.PRN_FK_PAF_BIDDING_ITEMS == elem.PBI_ROW_ID).OrderBy(m => m.PRN_ROW_ID).ToList();
                elem.PAF_BIDDING_ITEMS_BENCHMARK = db2.PAF_BIDDING_ITEMS_BENCHMARK.Where(x => x.PBB_FK_BIDDING_ITEMS == elem.PBI_ROW_ID).OrderBy(m => m.PBB_ROW_ID).ToList();
            }
            target.PAF_PAYMENT_ITEMS = db.PAF_PAYMENT_ITEMS.Where(x => x.PPI_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PPI_ROW_ID).ToList();
            foreach (PAF_PAYMENT_ITEMS elem in target.PAF_PAYMENT_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                elem.PAF_PAYMENT_ITEMS_DETAIL = db2.PAF_PAYMENT_ITEMS_DETAIL.Where(x => x.PPD_FK_PAF_PAYMENT_ITEMS == elem.PPI_ROW_ID).OrderBy(m => m.PPD_ROW_ID).ToList();
            }
            target.PAF_PROPOSAL_ITEMS = db.PAF_PROPOSAL_ITEMS.Where(x => x.PSI_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PSI_ROW_ID).ToList();
            foreach (PAF_PROPOSAL_ITEMS elem in target.PAF_PROPOSAL_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                elem.PAF_PROPOSAL_OTC_ITEMS = db2.PAF_PROPOSAL_OTC_ITEMS.Where(x => x.PSO_FK_PAF_PROPOSAL_ITEMS == elem.PSI_ROW_ID).OrderBy(m => m.PSO_ROW_ID).ToList();
                elem.PAF_PROPOSAL_REASON_ITEMS = db2.PAF_PROPOSAL_REASON_ITEMS.Where(x => x.PSR_FK_PAF_PROPOSAL_ITEMS == elem.PSI_ROW_ID).OrderBy(m => m.PSR_ROW_ID).ToList();
            }
            target.PAF_REVIEW_ITEMS = db.PAF_REVIEW_ITEMS.Where(x => x.PRI_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PRI_ROW_ID).ToList();
            foreach (PAF_REVIEW_ITEMS elem in target.PAF_REVIEW_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                elem.PAF_REVIEW_ITEMS_DETAIL = db2.PAF_REVIEW_ITEMS_DETAIL.Where(x => x.PRD_FK_PAF_REVIEW_ITEMS == elem.PRI_ROW_ID).OrderBy(m => m.PRD_ROW_ID).ToList();
            }

            target.PAF_BTM_DETAIL = db.PAF_BTM_DETAIL.Where(x => x.PBD_ROW_ID == target.PDA_ROW_ID).FirstOrDefault();
            target.PAF_BTM_GRADE_ITEMS = db.PAF_BTM_GRADE_ITEMS.Where(x => x.PBG_FK_PAF_DATA == target.PDA_ROW_ID).OrderBy(m => m.PBG_ROW_ID).ToList();
            foreach (PAF_BTM_GRADE_ITEMS elem in target.PAF_BTM_GRADE_ITEMS)
            {
                EntityCPAIEngine db2 = new EntityCPAIEngine();
                db2.Configuration.ProxyCreationEnabled = false;
                elem.PAF_BTM_ALLOCATE_ITEMS = db2.PAF_BTM_ALLOCATE_ITEMS.Where(x => x.PBA_FK_PAF_BTM_GRADE_ITEMS == elem.PBG_ROW_ID).OrderBy(m => m.PBA_ROW_ID).ToList();
                foreach (PAF_BTM_ALLOCATE_ITEMS a in elem.PAF_BTM_ALLOCATE_ITEMS)
                {
                    EntityCPAIEngine db3 = new EntityCPAIEngine();
                    db3.Configuration.ProxyCreationEnabled = false;
                    a.PAF_BTM_ALLOCATE_ITEMS_DETAIL = db3.PAF_BTM_ALLOCATE_ITEMS_DETAIL.Where(x => x.PAD_FK_BTM_ALLOCATE_ITEMS == a.PBA_ROW_ID).ToList();
                }
                elem.PAF_BTM_GRADE_ASSUME_ITEMS = db2.PAF_BTM_GRADE_ASSUME_ITEMS.Where(x => x.PGA_FK_PAF_BTM_GRADE_ITEMS == elem.PBG_ROW_ID).OrderBy(m => m.PGA_ROW_ID).ToList();
            }
            //foreach (PAF_ATTACH_FILE elem in target.PAF_ATTACH_FILE)
            //{
            //    elem.PAF_DATA = null;
            //}
            //foreach (PAF_BIDDING_ITEMS elem in target.PAF_BIDDING_ITEMS)
            //{
            //    elem.PAF_DATA = null;
            //    elem.PAF_ROUND = null;
            //    elem.MT_CUST = null;
            //    elem.MT_MATERIALS = null;
            //}
            //foreach (PAF_PAYMENT_ITEMS elem in target.PAF_PAYMENT_ITEMS)
            //{
            //    elem.PAF_DATA = null;
            //}
            //foreach (PAF_PROPOSAL_ITEMS elem in target.PAF_PROPOSAL_ITEMS)
            //{
            //    elem.PAF_DATA = null;
            //}
            //foreach (PAF_REVIEW_ITEMS elem in target.PAF_REVIEW_ITEMS)
            //{
            //    elem.PAF_DATA = null;
            //}
            return target;
        }
    }
}
