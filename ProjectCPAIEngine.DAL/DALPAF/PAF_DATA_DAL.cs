using com.pttict.engine.dal.Entity;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public static class ObjectExtensions
    {
        public static T ToObject<T>(this IDictionary<string, object> source)
            where T : class, new()
        {
            T someObject = new T();
            Type someObjectType = someObject.GetType();

            foreach (KeyValuePair<string, object> item in source)
            {
                someObjectType.GetProperty(item.Key).SetValue(someObject, item.Value, null);
            }

            return someObject;
        }

        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );

        }
    }

    public class PAF_DATA_DAL
    {
        public static string GetAwardedSummary(PAF_DATA data)
        {
            string result = "";
            try
            {
                if (data.PDA_TEMPLATE == "CMPS_IMPORT" || data.PDA_TEMPLATE == "CMLA_IMPORT")
                {
                    HashSet<string> supliers = new HashSet<string>();
                    List<string> suplier_ids = data.PAF_PROPOSAL_ITEMS.Select(c => c.PSI_FK_VENDOR).ToList();
                    foreach (string e in suplier_ids)
                    {
                        if (e != null)
                        {
                            MT_VENDOR v = PAF_MASTER_DAL.GetSupplierById(e);
                            if (v != null && v.VND_NAME1 != null)
                            {
                                supliers.Add(v.VND_NAME1);
                            }
                        }
                    }
                    result = string.Join(", ", supliers.ToArray());
                    //m.awarded = data.PAF_PROPOSAL_ITEMS.Select(c => c.MT_VENDOR.VND_NAME1).Aggregate((current, next) => current ?? "" + ", " + next);
                }
                else
                {
                    HashSet<string> customers = new HashSet<string>();
                    List<string> cust_ids = data.PAF_PAYMENT_ITEMS.Select(c => c.PPI_FK_CUSTOMER).ToList();
                    foreach (string cust_id in cust_ids)
                    {
                        if (cust_id != null)
                        {
                            MT_CUST_DETAIL mcd = PAF_MASTER_DAL.GetCustomerById(cust_id);
                            if (mcd != null && mcd.MCD_NAME_1 != null)
                            {
                                customers.Add(mcd.MCD_NAME_1);
                            }
                        }
                    }
                    result = string.Join(", ", customers.ToArray());
                }
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public static List<string> GetRelatedGroupsFromTransactionId(string transaction_id)
        {
            List<string> result = new List<string>();
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT cug.USG_USER_GROUP FROM PAF_DATA PD LEFT JOIN USERS U ON UPPER(PD.PDA_CREATED_BY) = UPPER(U.USR_LOGIN) JOIN CPAI_USER_GROUP cug ON cug.USG_FK_USERS = U.USR_ROW_ID WHERE cug.USG_USER_SYSTEM = 'PAF'  AND PD.PDA_ROW_ID = :PDA_ROW_ID";
            string group = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("PDA_ROW_ID", transaction_id.Trim()) }).FirstOrDefault();
            if (group.StartsWith("CMPS_INTER"))
            {
                result.Add("CMPS_INTER");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_DOM"))
            {
                result.Add("CMPS_DOM");
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMPS_SH"))
            {
                result.Add("CMPS_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            else if (group.StartsWith("CMLA"))
            {
                result.Add("CMLA");
                result.Add("CMLA_SH");
                result.Add("CMVP");
                result.Add("EVPC");
            }
            return result;
        }
        public static List<PAF_DATA_WRAPPER> GetList()
        {
            List<PAF_DATA_WRAPPER> results = new List<PAF_DATA_WRAPPER>();
            List<PAF_DATA> rows = new List<PAF_DATA>();
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    rows = context.PAF_DATA.ToList();
                    for (int i = 0; i < rows.Count; i++)
                    {
                        PAF_DATA row = rows[i];
                        row.MT_COMPANY = null;
                        row.PAF_ATTACH_FILE = null;
                        row.PAF_BIDDING_ITEMS = null;
                        row.PAF_PAYMENT_ITEMS = null;
                        row.PAF_PROPOSAL_ITEMS = null;
                        row.PAF_REVIEW_ITEMS = null;
                        rows[i] = row;
                        PAF_DATA_WRAPPER s = new PAF_DATA_WRAPPER();
                        s.PDA_STATUS = row.PDA_STATUS;
                        s.PDA_ROW_ID = row.PDA_ROW_ID;
                        s.PDA_CREATED = row.PDA_CREATED;
                        results.Add(s);
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return results;
        }

        public static PAF_DATA GetNoWrap(string transaction_id)
        {
            PAF_DATA data = new PAF_DATA();
            EntityCPAIEngine db = new EntityCPAIEngine();

            data = db.PAF_DATA.Where(m => m.PDA_ROW_ID == transaction_id).FirstOrDefault();

            return data;
        }

        public static PAF_DATA_WRAPPER GetByReqTranId(string req_transaction_id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            db.Configuration.ProxyCreationEnabled = false;

            EntitiesEngine entity = new EntitiesEngine();
            FUNCTION_TRANSACTION tran = entity.FUNCTION_TRANSACTION.Where(x => x.FTX_REQ_TRANS.Contains(req_transaction_id)).FirstOrDefault();
            PAF_DATA data = db.PAF_DATA.Where(m => m.PDA_ROW_ID == tran.FTX_TRANS_ID).FirstOrDefault();
            PAF_DATA_WRAPPER result = PAFHelper.CreateWarper(data);
            return result;
        }

        public static string GetSubmittedBy(string transaction_id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            CPAI_DATA_HISTORY result = db.CPAI_DATA_HISTORY.Where(m => m.DTH_TXN_REF == transaction_id && m.DTH_ACTION == "SUBMIT").OrderByDescending(m => m.DTH_CREATED_DATE).FirstOrDefault();
            if (result != null)
            {
                return result.DTH_ACTION_BY;
            }
            return null;
        }


        public static string GetPurchaseNO(string transaction_id)
        {
            PAF_DATA pd = GetNoWrap(transaction_id);
            return pd != null ? pd.PDA_FORM_ID : "";
        }

        public static string GetTemplateName(string transaction_id)
        {
            PAF_DATA pd = GetNoWrap(transaction_id);
            return CreateTemplateName(pd.PDA_TEMPLATE);
        }

        public static string CreateTemplateName(string PDA_TEMPLATE)
        {
            //string result = "Product Approval Form ";
            string result = "";
            try
            {
                switch (PDA_TEMPLATE)
                {
                    case "CMPS_DOM_SALE":
                        result += "(CMPS) Domestic Sale";
                        break;
                    case "CMPS_INTER_SALE":
                        result += "(CMPS) International Export";
                        break;
                    case "CMPS_IMPORT":
                        result += "(CMPS) International Import";
                        break;
                    case "CMPS_OTHER":
                        result += "(CMPS) Other Form";
                        break;
                    case "CMLA_FORM_1":
                        result += "(CMLA) Base Oil & Aromatic";
                        break;
                    case "CMLA_FORM_2":
                        result += "(CMLA) Slack Wax";
                        break;
                    case "CMLA_IMPORT":
                        result += "(CMLA) Import";
                        break;
                    case "CMLA_BITUMEN":
                        result += "(CMLA) Bitumen";
                        break;
                    default:
                        break;
                }
            }
            catch
            {

            }
            return result;
        }

        public static PAF_DATA_WRAPPER Get(string transaction_id)
        {
            try
            {
                EntityCPAIEngine db = new EntityCPAIEngine();
                db.Configuration.ProxyCreationEnabled = false;
                EntitiesEngine entity = new EntitiesEngine();
                PAF_DATA data = db.PAF_DATA.Where(m => m.PDA_ROW_ID == transaction_id).FirstOrDefault();
                PAF_DATA_WRAPPER result = PAFHelper.CreateWarper(data);
                return result;
            } catch (Exception e)
            {
                return null;
            }
        }

        public static string GetStatus(string tran_id)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            PAF_DATA data = db.PAF_DATA.Where(m => m.PDA_ROW_ID == tran_id).FirstOrDefault();
            try
            {
                return data.PDA_STATUS;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public void Save(PAF_DATA data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.PAF_DATA.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateNote(string PDA_REASON, string id)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            PAF_DATA pd = context.PAF_DATA.Where(m => m.PDA_ROW_ID == id).FirstOrDefault();
            pd.PDA_NOTE = PDA_REASON;
            context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public static CPAI_DATA_HISTORY GetLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY>  data_histories = dthMan.findByDthTxnRef(id);
            return data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).FirstOrDefault();
        }

        public static CPAI_DATA_HISTORY GetBeforeLastHistory(string id)
        {
            DataHistoryDAL dthMan = new DataHistoryDAL();
            List<CPAI_DATA_HISTORY> data_histories = dthMan.findByDthTxnRef(id);
            data_histories = data_histories.OrderByDescending(m => m.DTH_CREATED_DATE).ToList();
            if (data_histories.Count > 1) {
                return data_histories[1];
            }
            return null;
        }
        
        public static string GetLastStatus(string id)
        {
            CPAI_DATA_HISTORY dth = GetLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                PAF_DATA pd = JsonConvert.DeserializeObject<PAF_DATA>(json);
                return pd.PDA_STATUS;
            }
            catch (Exception e)
            {
                return "DRAFT";
            }
        }

        public static string GetLastAction(string id)
        {
            CPAI_DATA_HISTORY dth = GetBeforeLastHistory(id);
            try
            {
                string json = dth.DTH_JSON_DATA;
                PAF_DATA pd = JsonConvert.DeserializeObject<PAF_DATA>(json);
                return pd.PDA_STATUS;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        #region setDataHistory
        public static void CreateDataHistory(PAF_DATA data, string current_action, string user, string note, string ftx_row_id, bool is_reject = false)
        {
            PAF_DATA_WRAPPER data2 = PAF_DATA_DAL.Get(data.TRAN_ID);
            string json = "";
            if(data2 != null)
            {
                json = JsonConvert.SerializeObject(data2);
            }
            else
            {
                json = JsonConvert.SerializeObject(data);
            }
            //add data history
            DataHistoryDAL dthMan = new DataHistoryDAL();
            DateTime now = DateTime.Now;
            CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
            dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
            dataHistory.DTH_ACTION = current_action;
            dataHistory.DTH_ACTION_DATE = now;
            dataHistory.DTH_ACTION_BY = user;
            dataHistory.DTH_NOTE = note;
            dataHistory.DTH_JSON_DATA = json;
            dataHistory.DTH_TXN_REF = ftx_row_id;
            dataHistory.DTH_REJECT_FLAG = is_reject ? "Y" : "N";
            dataHistory.DTH_CREATED_BY = user;
            dataHistory.DTH_CREATED_DATE = now;
            dataHistory.DTH_UPDATED_BY = user;
            dataHistory.DTH_UPDATED_DATE = now;
            dthMan.Save(dataHistory);
        }
        #endregion

        public static void UpdateBrief(string PDA_REASON, string id)
        {

            EntityCPAIEngine context = new EntityCPAIEngine();
            PAF_DATA pd = context.PAF_DATA.Where(m => m.PDA_ROW_ID == id).FirstOrDefault();
            pd.PDA_BRIEF = PDA_REASON;
            context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public static void InsertOrUpdate(string status, PAF_DATA data, string id)
        {
            bool is_new = false;
            EntityCPAIEngine context = new EntityCPAIEngine();
            PAF_DATA pd = context.PAF_DATA.Where(m => m.PDA_ROW_ID == id).FirstOrDefault();
            if(pd == null)
            {
                pd = new PAF_DATA();
                pd.PDA_ROW_ID = id;
                is_new = true;
            }
            pd.PDA_STATUS = status;
            pd.PDA_FORM_ID = pd.PDA_FORM_ID ?? data.PDA_FORM_ID;
            pd.PDA_TEMPLATE = pd.PDA_TEMPLATE  ?? data.PDA_TEMPLATE;
            pd.PDA_CREATED = pd.PDA_CREATED ?? DateTime.Now;
            pd.PDA_CREATED_BY = pd.PDA_CREATED_BY ?? data.PDA_CREATED_BY;
            //pd.PDA_REASON = pd.PDA_REASON == null ? "-" : (pd.PDA_REASON == "-" ? data.PDA_REASON);
            pd.PDA_REASON = data.PDA_REASON;
            if (pd.PDA_STATUS == "WAITING VERIFY")
            {
                pd.PDA_REASON = "-";
            }
            //if (pd.PDA_STATUS == "SUBMIT")
            //{
            //    pd.PDA_REASON = "-";
            //}
            string username = pd.PDA_UPDATED_BY;

            if (is_new)
            {
                context.PAF_DATA.Add(pd);
            }
            else
            {
                pd.PDA_UPDATED = pd.PDA_UPDATED ?? data.PDA_UPDATED;
                pd.PDA_UPDATED_BY = pd.PDA_UPDATED_BY ?? data.PDA_UPDATED_BY;
                context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
            }
            context.SaveChanges();
            //data.PDA_ROW_ID = String.IsNullOrEmpty(data.PDA_ROW_ID)? id : data.PDA_ROW_ID;
            //data.PDA_STATUS = status;
            //InsertOrUpdate(data);
        }

        public static void InsertOrUpdate(PAF_DATA data)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            EntityCPAIEngine db = new EntityCPAIEngine();
            PAF_DATA old = db.PAF_DATA.Where(m => m.PDA_ROW_ID == data.PDA_ROW_ID).FirstOrDefault();

            try
            {
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_REVIEW_ITEMS");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_REVIEW_ITEMS WHERE PRI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_BIDDING_ITEMS");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_BIDDING_ITEMS WHERE PBI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_PROPOSAL_ITEMS");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_PROPOSAL_ITEMS WHERE PSI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_ATTACH_FILE");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_ATTACH_FILE WHERE PAT_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_PAYMENT_ITEMS");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_PAYMENT_ITEMS WHERE PPI_FK_PAF_DATA = :PPI_ROW_ID", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_BTM_DETAIL");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_BTM_DETAIL WHERE PBD_ROW_ID = :PBD_ROW_ID", new[] { new OracleParameter("PBD_ROW_ID", data.PDA_ROW_ID) });
                 PAFHelper.log(old.PDA_FORM_ID + " DELETE FROM PAF_BTM_GRADE_ITEMS");
                context.Database.ExecuteSqlCommand("DELETE FROM PAF_BTM_GRADE_ITEMS WHERE PBG_FK_PAF_DATA = :PBG_FK_PAF_DATA", new[] { new OracleParameter("PBG_FK_PAF_DATA", data.PDA_ROW_ID) });
            }
            catch (Exception e)
            {

            }
            //data.PDA_ROW_ID = PAFHelper.GetId();
            //PAF_DATA pd = PAFHelper.Clone(data);
            PAF_DATA pd = data;
            if(old != null)
            {
                pd.PDA_UPDATED = pd.PDA_CREATED;
                pd.PDA_UPDATED_BY = pd.PDA_CREATED_BY;
                pd.PDA_CREATED = old.PDA_CREATED ?? pd.PDA_CREATED;
                pd.PDA_CREATED_BY = old.PDA_CREATED_BY ?? pd.PDA_CREATED_BY;
            }

            DateTime dt = DateTime.Now;
            string username = pd.PDA_UPDATED_BY;
            foreach (PAF_REVIEW_ITEMS a in pd.PAF_REVIEW_ITEMS)
            {
                a.PRI_ROW_ID = PAFHelper.GetSeqId("PAF_REVIEW_ITEMS");
                a.PRI_FK_PAF_DATA = data.PDA_ROW_ID;
                //try
                //{
                //    context.Database.ExecuteSqlCommand("DELETE FROM PAF_REVIEW_ITEMS WHERE PRI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
                //}
                //catch (Exception e)
                //{

                //}
                foreach (PAF_REVIEW_ITEMS_DETAIL b in a.PAF_REVIEW_ITEMS_DETAIL)
                {
                    b.PRD_ROW_ID = PAFHelper.GetSeqId("PAF_REVIEW_ITEMS_DETAIL");
                    //b.PRD_ROW_ID = PAFHelper.GetId();
                    b.PRD_FK_PAF_REVIEW_ITEMS = a.PRI_ROW_ID;
                }
            }


            foreach (PAF_BIDDING_ITEMS a in pd.PAF_BIDDING_ITEMS)
            {
                a.PBI_ROW_ID = PAFHelper.GetSeqId("PAF_BIDDING_ITEMS");
                a.PBI_FK_PAF_DATA = data.PDA_ROW_ID;
                a.PBI_CREATED = a.PBI_CREATED ?? dt;
                a.PBI_CREATED_BY = username;
                a.PBI_UPDATED = dt;
                a.PBI_UPDATED_BY = username;

                int count = 1;
                foreach (PAF_ROUND b in a.PAF_ROUND)
                {
                    b.PRN_COUNT = count;
                    b.PRN_ROW_ID = PAFHelper.GetSeqId("PAF_ROUND");
                    b.PRN_FK_PAF_BIDDING_ITEMS = a.PBI_ROW_ID;
                    b.PRN_CREATED = b.PRN_CREATED ?? dt;
                    b.PRN_CREATED_BY = username;
                    b.PRN_UPDATED = dt;
                    b.PRN_UPDATED_BY = username;
                    count++;
                }

                foreach (PAF_BIDDING_ITEMS_BENCHMARK b in a.PAF_BIDDING_ITEMS_BENCHMARK)
                {
                    b.PBB_ROW_ID = PAFHelper.GetSeqId("PAF_BIDDING_ITEMS_BENCHMARK");
                    //b.PRD_ROW_ID = PAFHelper.GetId();
                    b.PBB_FK_BIDDING_ITEMS = a.PBI_ROW_ID;

                    b.PBB_CREATED = b.PBB_CREATED ?? dt;
                    b.PBB_CREATED_BY = username;
                    b.PBB_UPDATED = dt;
                    b.PBB_UPDATED_BY = username;

                }
            }

            foreach (PAF_PROPOSAL_ITEMS a in pd.PAF_PROPOSAL_ITEMS)
            {
                a.PSI_ROW_ID = PAFHelper.GetSeqId("PAF_PROPOSAL_ITEMS");
                a.PSI_FK_PAF_DATA = data.PDA_ROW_ID;

                a.PSI_CREATED = a.PSI_CREATED ?? dt;
                a.PSI_CREATED_BY = username;
                a.PSI_UPDATED = dt;
                a.PSI_UPDATED_BY = username;

                foreach (PAF_PROPOSAL_REASON_ITEMS b in a.PAF_PROPOSAL_REASON_ITEMS)
                {
                    b.PSR_ROW_ID = PAFHelper.GetSeqId("PAF_PROPOSAL_REASON_ITEMS");
                    b.PSR_FK_PAF_PROPOSAL_ITEMS = a.PSI_ROW_ID;


                    b.PSR_CREATED = b.PSR_CREATED ?? dt;
                    b.PSR_CREATED_BY = username;
                    b.PSR_UPDATED = dt;
                    b.PSR_UPDATED_BY = username;
                }

                foreach (PAF_PROPOSAL_OTC_ITEMS b in a.PAF_PROPOSAL_OTC_ITEMS)
                {
                    b.PSO_ROW_ID = PAFHelper.GetSeqId("PAF_PROPOSAL_OTC_ITEMS");
                    b.PSO_FK_PAF_PROPOSAL_ITEMS = a.PSI_ROW_ID;


                    b.PSO_CREATED = b.PSO_CREATED ?? dt;
                    b.PSO_CREATED_BY = username;
                    b.PSO_UPDATED = dt;
                    b.PSO_UPDATED_BY = username;

                }
            }

            foreach (PAF_ATTACH_FILE a in pd.PAF_ATTACH_FILE)
            {
                a.PAT_ROW_ID = PAFHelper.GetSeqId("PAF_ATTACH_FILE");
                //a.PAT_ROW_ID = PAFHelper.GetId();
                a.PAT_FK_PAF_DATA = data.PDA_ROW_ID;
                a.PAT_CREATED = a.PAT_CREATED ?? dt;
                a.PAT_CREATED_BY = username;
                a.PAT_UPDATED = dt;
                a.PAT_UPDATED_BY = username;
            }

            foreach (PAF_PAYMENT_ITEMS elem in pd.PAF_PAYMENT_ITEMS)
            {
                elem.PPI_ROW_ID = PAFHelper.GetSeqId("PAF_PAYMENT_ITEMS");
                elem.PPI_FK_PAF_DATA = data.PDA_ROW_ID;

                elem.PPI_CREATED = elem.PPI_CREATED ?? dt;
                elem.PPI_CREATED_BY = username;
                elem.PPI_UPDATED = dt;
                elem.PPI_UPDATED_BY = username;

                //foreach (PAF_PAYMENT_ITEMS_DETAIL elem2 in elem.PAF_PAYMENT_ITEMS_DETAIL)
                //{
                //    elem2.PPD_ROW_ID = PAFHelper.GetId();
                //    elem2.PPD_FK_PAF_PAYMENT_ITEMS = elem.PPI_ROW_ID;
                //}
                foreach (PAF_PAYMENT_ITEMS_DETAIL b in elem.PAF_PAYMENT_ITEMS_DETAIL)
                {
                    b.PPD_ROW_ID = PAFHelper.GetSeqId("PAF_PAYMENT_ITEMS_DETAIL");
                    b.PPD_FK_PAF_PAYMENT_ITEMS = elem.PPI_ROW_ID;


                    b.PPD_CREATED = b.PPD_CREATED ?? dt;
                    b.PPD_CREATED_BY = username;
                    b.PPD_UPDATED = dt;
                    b.PPD_UPDATED_BY = username;
                }
            }

            if (pd.PAF_BTM_DETAIL != null)
            {
                pd.PAF_BTM_DETAIL.PBD_ROW_ID = pd.PDA_ROW_ID;
                pd.PAF_BTM_DETAIL.PBD_CREATED = pd.PAF_BTM_DETAIL.PBD_CREATED ?? dt;
                pd.PAF_BTM_DETAIL.PBD_CREATED_BY = username;
                pd.PAF_BTM_DETAIL.PBD_UPDATED = dt;
                pd.PAF_BTM_DETAIL.PBD_UPDATED_BY = username;
            }
            foreach (PAF_BTM_GRADE_ITEMS b in pd.PAF_BTM_GRADE_ITEMS)
            {
                b.PBG_ROW_ID = PAFHelper.GetSeqId("PAF_BTM_GRADE_ITEMS");
                b.PBG_FK_PAF_DATA = pd.PDA_ROW_ID;


                b.PBG_CREATED = b.PBG_CREATED ?? dt;
                b.PBG_CREATED_BY = username;
                b.PBG_UPDATED = dt;
                b.PBG_UPDATED_BY = username;
                foreach (PAF_BTM_ALLOCATE_ITEMS c in b.PAF_BTM_ALLOCATE_ITEMS)
                {
                    c.PBA_ROW_ID = PAFHelper.GetSeqId("PAF_BTM_ALLOCATE_ITEMS");
                    c.PBA_FK_PAF_BTM_GRADE_ITEMS = b.PBG_ROW_ID;

                    c.PBA_CREATED = c.PBA_CREATED ?? dt;
                    c.PBA_CREATED_BY = username;
                    c.PBA_UPDATED = dt;
                    c.PBA_UPDATED_BY = username;
                    foreach (PAF_BTM_ALLOCATE_ITEMS_DETAIL d in c.PAF_BTM_ALLOCATE_ITEMS_DETAIL)
                    {
                        d.PAD_ROW_ID = PAFHelper.GetSeqId("PAF_BTM_ALLOCATE_ITEMS_D");
                        d.PAD_FK_BTM_ALLOCATE_ITEMS = c.PBA_ROW_ID;


                        d.PAD_CREATED = d.PAD_CREATED ?? dt;
                        d.PAD_CREATED_BY = username;
                        d.PAD_UPDATED = dt;
                        d.PAD_UPDATED_BY = username;
                    }
                }

                foreach(PAF_BTM_GRADE_ASSUME_ITEMS c in b.PAF_BTM_GRADE_ASSUME_ITEMS)
                {
                    c.PGA_ROW_ID = PAFHelper.GetSeqId("PAF_BTM_GRADE_ASSUME_ITEMS");
                    c.PGA_FK_PAF_BTM_GRADE_ITEMS = b.PBG_ROW_ID;

                    c.PGA_CREATED = c.PGA_CREATED ?? dt;
                    c.PGA_CREATED_BY = username;
                    c.PGA_UPDATED = dt;
                    c.PGA_UPDATED_BY = username;
                }
            }


            pd.PDA_CREATED_BY = !String.IsNullOrEmpty(pd.PDA_CREATED_BY) ? pd.PDA_CREATED_BY : data.PDA_CREATED_BY;
            if (pd.PDA_STATUS == "WAITING VERIFY")
            {
                pd.PDA_REASON = "-";
            }
            //pd.PAF_BIDDING_ITEMS = null;
            //pd.PAF_REVIEW_ITEMS = null;
            //pd.PAF_PROPOSAL_ITEMS = null;
            //pd.PAF_ATTACH_FILE = null;
            context.PAF_DATA.Add(pd);
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                try
                {
                    PAFHelper.log(ex);
                } catch
                {

                }
                context.Entry(pd).State = System.Data.Entity.EntityState.Modified;
                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateException fex)
                {
                    PAFHelper.log(fex);
                }
            } catch(Exception e)
            {
                PAFHelper.log(e);
            }
            //context.Database.ExecuteSqlCommand("DELETE FROM PAF_REVIEW_ITEMS WHERE PRI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            //foreach (PAF_REVIEW_ITEMS a in data.PAF_REVIEW_ITEMS)
            //{
            //    //a.PRI_ROW_ID = PAFHelper.GetId();
            //    a.PRI_ROW_ID = Guid.NewGuid().ToString("N");
            //    a.PRI_FK_PAF_DATA = data.PDA_ROW_ID;
            //    context.PAF_REVIEW_ITEMS.Add(a);
            //}

            //context.Database.ExecuteSqlCommand("DELETE FROM PAF_BIDDING_ITEMS WHERE PBI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            //foreach (PAF_BIDDING_ITEMS a in data.PAF_BIDDING_ITEMS)
            //{
            //    a.PBI_ROW_ID = PAFHelper.GetId();
            //    a.PBI_FK_PAF_DATA = data.PDA_ROW_ID;
            //    context.PAF_BIDDING_ITEMS.Add(a);
            //}

            //context.Database.ExecuteSqlCommand("DELETE FROM PAF_PROPOSAL_ITEMS WHERE PSI_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            //foreach (PAF_PROPOSAL_ITEMS a in data.PAF_PROPOSAL_ITEMS)
            //{
            //    a.PSI_ROW_ID = PAFHelper.GetId();
            //    a.PSI_FK_PAF_DATA = data.PDA_ROW_ID;
            //    context.PAF_PROPOSAL_ITEMS.Add(a);
            //}

            //context.Database.ExecuteSqlCommand("DELETE FROM PAF_ATTACH_FILE WHERE PAT_FK_PAF_DATA = :PRI_FK_PAF_DATA", new[] { new OracleParameter("PRI_FK_PAF_DATA", data.PDA_ROW_ID) });
            //foreach (PAF_ATTACH_FILE a in data.PAF_ATTACH_FILE)
            //{
            //    a.PAT_ROW_ID = PAFHelper.GetId();
            //    a.PAT_FK_PAF_DATA = data.PDA_ROW_ID;
            //    context.PAF_ATTACH_FILE.Add(a);
            //}

            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                PAFHelper.log(ex);
            }
            catch (DbEntityValidationException ex)
            {
                PAFHelper.log(ex);
            }
            catch (Exception ex)
            {
                PAFHelper.log(ex);
            }
        }


        public static List<CPAI_ACTION_BUTTON> GetButtonByUserID(string user_row_id)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            using (EntityCPAIEngine db = new EntityCPAIEngine())
            {
                result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID WHERE UPPER(CPAI_USER_GROUP.USG_FK_USERS) = :USER_ID AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID", new[] { new OracleParameter("USER_ID", user_row_id.ToUpper()), new OracleParameter("FUNCTION_ID", "44") }).ToList();
            }
            return result;
        }

        public static List<CPAI_ACTION_BUTTON> GetButtons(string username)
        {
            List<CPAI_ACTION_BUTTON> result = new List<CPAI_ACTION_BUTTON>();
            EntityCPAIEngine db = new EntityCPAIEngine();
            result = db.Database.SqlQuery<CPAI_ACTION_BUTTON>("SELECT * FROM CPAI_ACTION_FUNCTION JOIN CPAI_USER_GROUP ON CPAI_ACTION_FUNCTION.ACF_USR_GROUP = CPAI_USER_GROUP.USG_USER_GROUP JOIN CPAI_ACTION_CONTROL ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_FUNCTION = CPAI_ACTION_FUNCTION.ACF_ROW_ID JOIN CPAI_ACTION_BUTTON ON CPAI_ACTION_CONTROL.ACT_FK_ACTION_BUTTON = CPAI_ACTION_BUTTON.ABT_ROW_ID JOIN USERS ON CPAI_USER_GROUP.USG_FK_USERS = USERS.USR_ROW_ID WHERE UPPER(USERS.USR_LOGIN) = :USR_LOGIN AND CPAI_ACTION_FUNCTION.ACF_FK_FUNCTION = :FUNCTION_ID", new[] { new OracleParameter("USR_LOGIN", username.ToUpper()), new OracleParameter("FUNCTION_ID", "44") }).ToList();
            return result;
        }

        public static PAF_DATA_WRAPPER Copy(COPPY_CRITERIA criteria)
        {
            PAF_DATA_WRAPPER result = Get(criteria.TRAN_ID);
            result.PDA_ROW_ID = null;
            result.PDA_FORM_ID = null;
            result.PDA_STATUS = null;
            result.PDA_CREATED = null;
            result.PDA_CREATED_BY = null;
            result.PDA_UPDATED = null;
            result.PDA_UPDATED_BY = null;

            foreach (PAF_ATTACH_FILE elem in result.PAF_ATTACH_FILE)
            {
                elem.PAT_ROW_ID = null;
                elem.PAT_CREATED = null;
                elem.PAT_CREATED_BY = null;
                elem.PAT_UPDATED = null;
                elem.PAT_UPDATED_BY = null;
            }

            foreach (PAF_BIDDING_ITEMS elem in result.PAF_BIDDING_ITEMS)
            {
                elem.PBI_ROW_ID = null;
                elem.PBI_CREATED = null;
                elem.PBI_CREATED_BY = null;
                elem.PBI_UPDATED = null;
                elem.PBI_UPDATED_BY = null;
                foreach (PAF_ROUND e2 in elem.PAF_ROUND)
                {
                    e2.PRN_ROW_ID = null;
                }
                foreach (PAF_BIDDING_ITEMS_BENCHMARK e2 in elem.PAF_BIDDING_ITEMS_BENCHMARK)
                {
                    e2.PBB_ROW_ID = null;
                }
            }

            foreach (PAF_PAYMENT_ITEMS elem in result.PAF_PAYMENT_ITEMS)
            {
                elem.PPI_ROW_ID = null;
                foreach (PAF_PAYMENT_ITEMS_DETAIL e2 in elem.PAF_PAYMENT_ITEMS_DETAIL)
                {
                    e2.PPD_ROW_ID = null;
                }
            }

            foreach (PAF_PROPOSAL_ITEMS elem in result.PAF_PROPOSAL_ITEMS)
            {
                elem.PSI_ROW_ID = null;
                foreach (PAF_PROPOSAL_OTC_ITEMS e2 in elem.PAF_PROPOSAL_OTC_ITEMS)
                {
                    e2.PSO_ROW_ID = null;
                }
                foreach (PAF_PROPOSAL_REASON_ITEMS e2 in elem.PAF_PROPOSAL_REASON_ITEMS)
                {
                    e2.PSR_ROW_ID = null;
                }
            }

            foreach (PAF_REVIEW_ITEMS elem in result.PAF_REVIEW_ITEMS)
            {
                elem.PRI_ROW_ID = null;
                foreach (PAF_REVIEW_ITEMS_DETAIL e2 in elem.PAF_REVIEW_ITEMS_DETAIL)
                {
                    e2.PRD_ROW_ID = null;
                }
            }

            return result;
        }
        }
}
