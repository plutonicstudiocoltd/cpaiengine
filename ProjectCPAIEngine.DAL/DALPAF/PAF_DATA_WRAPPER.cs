﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAFButtonAction
    {
        public string name { get; set; }
        public string page_url { get; set; }
        public string call_xml { get; set; }

    }
    public class PAF_DATA_WRAPPER
    {
        public string PDA_ROW_ID { get; set; }
        public string PDA_FORM_ID { get; set; }
        public string PDA_TEMPLATE { get; set; }
        public string PDA_FOR_COMPANY { get; set; }
        public string PDA_TYPE { get; set; }
        public string PDA_SUB_TYPE { get; set; }
        public string PDA_TYPE_NOTE { get; set; }
        public string PDA_BRIEF { get; set; }
        public string PDA_PRICE_UNIT { get; set; }
        public Nullable<System.DateTime> PDA_CONTRACT_DATE_FROM { get; set; }
        public Nullable<System.DateTime> PDA_CONTRACT_DATE_TO { get; set; }
        public Nullable<System.DateTime> PDA_LOADING_DATE_FROM { get; set; }
        public Nullable<System.DateTime> PDA_LOADING_DATE_TO { get; set; }
        public string PDA_PRICING_PERIOD { get; set; }
        public Nullable<System.DateTime> PDA_DISCHARGING_DATE_FROM { get; set; }
        public Nullable<System.DateTime> PDA_DISCHARGING_DATE_TO { get; set; }
        public string PDA_NOTE { get; set; }
        public string PDA_STATUS { get; set; }
        public Nullable<System.DateTime> PDA_CREATED { get; set; }
        public string PDA_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PDA_UPDATED { get; set; }
        public string PDA_UPDATED_BY { get; set; }
        public string PDA_INCLUDED_REVIEW_TABLE { get; set; }
        public string PDA_INCLUDED_BIDDING_TABLE { get; set; }
        public string PDA_SPECIAL_TERMS_OR_CONDITION { get; set; }
        public string PDA_REASON { get; set; }
        public string PDA_TOPIC { get; set; }
        


        public bool IS_DISABLED { get; set; }
        public bool IS_DISABLED_NOTE { get; set; }
        public List<PAFButtonAction> Buttons { get; set; }

        public List<PAF_ATTACH_FILE> PAF_ATTACH_FILE { get; set; }
        public List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS { get; set; }
        public List<PAF_PAYMENT_ITEMS> PAF_PAYMENT_ITEMS { get; set; }
        public List<PAF_PROPOSAL_ITEMS> PAF_PROPOSAL_ITEMS { get; set; }
        public List<PAF_REVIEW_ITEMS> PAF_REVIEW_ITEMS { get; set; }
        public PAF_BTM_DETAIL PAF_BTM_DETAIL { get; set; }
        public List<PAF_BTM_GRADE_ITEMS> PAF_BTM_GRADE_ITEMS { get; set; }

    }

    public partial class COPPY_CRITERIA
    {
        public string REQ_TRAN_ID { get; set; }
        public string TRAN_ID { get; set; }
    }

    public partial class PAF_SEARCH_CRITERIA
    {
        public string DATE_FROM { get; set; }
        public string DATE_TO { get; set; }
        public string FROM_TYPE { get; set; }
        public string FROM_ID { get; set; }
        public string VENDOR_ID { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string CREATED_BY { get; set; }
    }

}
