﻿using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAF_MAIL_CONTENT
    {
        public static USERS USER { get; set; }
        public static string TransactionId { get; set; }
        public static PAF_DATA data { get; set; }
        public static UsersDAL userMan = new UsersDAL();

        public static void init()
        {
            //if(data != null && data.PDA_ROW_ID == TransactionId)
            //{
            //    return;
            //}

            data = PAF_DATA_DAL.GetNoWrap(TransactionId);
        }

        public static string GetSubject(string subject)
        {
            init();
            string doc_no = data.PDA_FORM_ID;
            string action_user = (USER.USR_FIRST_NAME_EN ?? "") + " " + (USER.USR_LAST_NAME_EN ?? "");
            string form_name = GetFormName();
            subject = subject
                        .Replace("Product sale Approval Form", form_name)
                        .Replace("#doc_no", doc_no)
                        .Replace("#action_user", action_user);
            return subject;
        }

        public static string GetBody(string body)
        {
            init();
            string doc_no = data.PDA_FORM_ID;
            string action_user = (USER.USR_FIRST_NAME_EN ?? "") + " " + (USER.USR_LAST_NAME_EN ?? "");
            string for_company = "";
            try
            {
                for_company = data.MT_COMPANY.MCO_SHORT_NAME ?? "";
            }
            catch (Exception e)
            {

            }
            string reason = data.PDA_REASON ?? "";
            string create_by = GetNameFromUserLogin(PAF_DATA_DAL.GetSubmittedBy(TransactionId));
            string contract_prd = data.PDA_CONTRACT_DATE();
            string loading_prd = data.PDA_LOADING_DATE();
            string pricing_prd = String.IsNullOrEmpty(data.PDA_PRICING_PERIOD) ? "-" : data.PDA_PRICING_PERIOD;
            string discharging_prd = data.PDA_DISCHARGING_DATE();
            string contract_rows = GetContractRowsContent();
            string awarded_rows = GetAwardedRowContent();
            if (data.PDA_TEMPLATE == "CMLA_BITUMEN")
            {
                body = body.Replace("<br> Company: #for_company<br>", "")
                    .Replace(" Contract Period: #contract_prd<br> Loading Period: #loading_prd<br> Discharing Period: #discharging_prd<br><br>", "")
                    .ToString();
            }
            else if(data.PDA_TEMPLATE == "CMPS_OTHER")
            {
                body = body.Replace("<br> Company: #for_company<br>", "")
                    .Replace(" Contract Period: #contract_prd<br> Loading Period: #loading_prd<br> Pricing Period: #pricing_prd<br> Discharing Period: #discharging_prd<br><br>", "")
                    .Replace(" #awarded_rows<br><br>", "")
                    .Replace("#contract_rows", "Topic: " + data.PDA_TOPIC + "<br/>Brief Market Situation:<br/>" + data.PDA_BRIEF)
                    .ToString();
            }
            body = body.Replace("#doc_no", doc_no)
                    .Replace("#action_user", action_user)
                    .Replace("#for_company", for_company)
                    .Replace("#create_by", create_by)
                    .Replace("#contract_prd", contract_prd)
                    .Replace("#loading_prd", loading_prd)
                    .Replace("#pricing_prd", pricing_prd)
                    .Replace("#discharging_prd", discharging_prd)
                    .Replace("#contract_rows", contract_rows)
                    .Replace("#awarded_rows", awarded_rows)
                    .Replace("#reason", reason)
                    .ToString();
            return body;
        }

        public static string GetNameFromUserLogin(string userlogin)
        {
            List<USERS> users = userMan.findByLogin(userlogin);
            USERS user = users.FirstOrDefault();
            if (user == null)
            {
                return "";
            }
            string action_user = (user.USR_FIRST_NAME_EN ?? "") + " " + (user.USR_LAST_NAME_EN ?? "");
            return action_user;
        }

        public static string GetFormName()
        {
            string result = "Product sale Approval Form ";
            switch (data.PDA_TEMPLATE)
            {
                case "CMPS_DOM_SALE":
                    result += " (Domestic Sale)";
                    break;
                case "CMPS_INTER_SALE":
                    result += " (International Export)";
                    break;
                case "CMPS_IMPORT":
                    result += " (International Import)";
                    break;
                case "CMLA_FORM_1":
                    result += " (Base Oil & Aromatic)";
                    break;
                case "CMLA_FORM_2":
                    result += " (Slack Wax)";
                    break;
                case "CMLA_IMPORT":
                    result += " (Import)";
                    break;
                case "CMLA_BITUMEN":
                    result += " (Bitumen)";
                    break;
                case "CMPS_OTHER":
                    result += " (Other Form)";
                    break;
                default:
                    break;
            }

            return result;
        }

        public static string GetContractRowsContent()
        {
            switch (data.PDA_TEMPLATE)
            {
                case "CMLA_FORM_1":
                    return GetReviewRowsBaseOil();
                    break;
            }
            string result = "";
            List<string> rows = new List<string>();
            List<PAF_REVIEW_ITEMS> PAF_REVIEW_ITEMS = data.PAF_REVIEW_ITEMS.OrderBy(m => m.PRI_ROW_ID).ToList();
            if (data.PDA_INCLUDED_REVIEW_TABLE != "Y" || data.PDA_TEMPLATE == "CMLA_IMPORT")
            {
                return result;
            }
            string table_header = "Review existing price";
            bool need_table_header = true;
            foreach (PAF_REVIEW_ITEMS elem in PAF_REVIEW_ITEMS)
            {

                MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(elem.PRI_FK_CUSTOMER);
                string product = "";
                if (elem.MT_MATERIALS != null)
                {
                    product = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                }
                string note = "";
                List<PAF_REVIEW_ITEMS_DETAIL> PAF_REVIEW_ITEMS_DETAIL = elem.PAF_REVIEW_ITEMS_DETAIL.OrderBy(m => m.PRD_ROW_ID).ToList();
                if (PAF_REVIEW_ITEMS_DETAIL.Count > 0)
                {
                    note = PAF_REVIEW_ITEMS_DETAIL[0].PRD_NOTE;
                }
                string row = "";
                int count = 1;
                bool need_customer_content = true;
                foreach (PAF_REVIEW_ITEMS_DETAIL ee in PAF_REVIEW_ITEMS_DETAIL)
                {
                    string quantity = "";
                    if (data.PDA_TEMPLATE == "CMLA_FORM_2")
                    {
                        try
                        {
                            quantity = elem.PAF_REVIEW_ITEMS_DETAIL.First().PRD_OUR_PROPOSAL_QTY_FLOAT;
                        }
                        catch (Exception e)
                        {

                        }
                        quantity = PAFHelper.ShowTolerance(quantity);
                    }
                    else
                    {
                        quantity = PAFHelper.GetMinMaxFormat(ee.PRD_OUR_PROPOSAL_QUANTITY_MIN, ee.PRD_OUR_PROPOSAL_QUANTITY_MAX);
                    }
                    if (String.IsNullOrEmpty(quantity) || (String.IsNullOrEmpty(ee.PRD_OUR_PROPOSAL_PRICE_FLOAT) && ee.PRD_OUR_PROPOSAL_PRICE_FIXED == null))
                    {
                        count++;
                        continue;
                    }
                    if (need_customer_content)
                    {
                        if (need_table_header)
                        {
                            row += table_header;
                            row += "<br/>";
                            need_table_header = false;
                        }
                        row += "Customer: " + customer.MCD_NAME_1 + " " + customer.MCD_NAME_2;
                        row += "<br/>";
                        row += "Product: " + product;
                        row += "<br/>";
                        need_customer_content = false;
                    }
                    if (data.PDA_TEMPLATE != "CMLA_FORM_1" && data.PDA_TEMPLATE != "CMLA_FORM_2")
                    {
                        row += "Tier: " + count.ToString();
                        row += "<br/>";
                    }
                    row += "Quantity (Our Proposal): " + quantity + " " + (elem.PRI_QUANTITY_UNIT ?? "");
                    row += "<br/>";
                    if (data.PDA_TEMPLATE != "CMLA_FORM_1" && data.PDA_TEMPLATE != "CMLA_FORM_2")
                    {
                        row += "Price or Formula (Our Proposal): " + ee.PRD_OUR_PROPOSAL_PRICE_FLOAT + "";
                        row += "<br/>";
                    }
                    count++;
                }
                if (data.PDA_TEMPLATE != "CMLA_FORM_2")
                {
                    //row += "Note: " + (String.IsNullOrEmpty(note) ? " - " : note);
                }
                rows.Add(row);
            }
            result = String.Join("<br/><br/>", rows);
            return result;
        }

        public static string GetAwardedRowContent()
        {
            switch (data.PDA_TEMPLATE)
            {
                case "CMPS_IMPORT":
                case "CMPS_INTER_SALE":
                    return GetAwardedRowCMPSInterContent();
                    break;
                case "CMLA_BITUMEN":
                    return GetSpecialSummaryForBitumen();
                    break;
                case "CMLA_FORM_1":
                    return GetAwardedRowBaseOilContent();
                    break;
            }
            EntityCPAIEngine context = new EntityCPAIEngine();
            string result = "";
            List<string> rows = new List<string>();
            List<string> bidding_items = data.PAF_PAYMENT_ITEMS.Select(m => m.PPI_FK_CUSTOMER).ToList();
            List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS = data.PAF_BIDDING_ITEMS.Where(m => bidding_items.Contains(m.PBI_FK_CUSTOMER)).OrderBy(m => m.PBI_ROW_ID).ToList();
            if (data.PDA_INCLUDED_BIDDING_TABLE != "Y")
            {
                return result;
            }
            foreach (PAF_BIDDING_ITEMS elem in PAF_BIDDING_ITEMS)
            {
                MT_VENDOR vendor = PAF_MASTER_DAL.GetVendor(elem.PBI_FK_VENDOR);
                string quantity = PAFHelper.GetMinMaxFormat(elem.PBI_QUANTITY_MIN, elem.PBI_QUANTITY_MIN);
                if (elem.PBI_AWARDED_BIDDER != "Y")
                {
                    continue;
                }
                MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(elem.PBI_FK_CUSTOMER);
                string product = "";
                if (elem.MT_MATERIALS != null)
                {
                    product = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                }
                string note = "";
                //List<PAF_REVIEW_ITEMS_DETAIL> PAF_REVIEW_ITEMS_DETAIL = elem.PAF_REVIEW_ITEMS_DETAIL.OrderBy(m => m.PRD_ROW_ID).ToList();
                //if (PAF_REVIEW_ITEMS_DETAIL.Count > 0)
                //{
                //    note = PAF_REVIEW_ITEMS_DETAIL[0].PRD_NOTE;
                //}
                string row = "";
                if (data.PDA_TEMPLATE == "CMLA_IMPORT")
                {
                    row += "Supplier: " + vendor.VND_NAME1 + " " + vendor.VND_NAME2;
                    row += "<br/>";
                }
                else
                {
                    row += "Customer: " + customer.MCD_NAME_1 + " " + customer.MCD_NAME_2;
                    row += "<br/>";
                }
                row += "Product: " + product;
                row += "<br/>";
                //row += "Unit: " + elem.PBI_QUANTITY_UNIT ?? "";
                //row += "<br/>";
                if (data.PDA_TEMPLATE == "CMLA_IMPORT" || data.PDA_TEMPLATE == "CMLA_FORM_1" || data.PDA_TEMPLATE == "CMLA_FORM_2")
                {
                    quantity = elem.PBI_QUANTITY_FLOAT;
                    row += "Quantity: " + (PAFHelper.ShowTolerance(quantity) ?? "") + " " + (elem.PBI_QUANTITY_UNIT ?? "");
                    row += "<br/>";
                }
                else
                {
                    row += "Quantity: " + (quantity ?? "") + " " + (elem.PBI_QUANTITY_UNIT ?? "");
                    row += "<br/>";
                }

                if (data.PDA_TEMPLATE == "CMLA_IMPORT")
                {
                    row += "Packaging: " + elem.PBI_FK_PACKAGING ?? "";
                    row += "<br/>";
                }
                else if (data.PDA_TEMPLATE == "CMLA_FORM_2")
                {
                    row += "Destination: " + getNameFromCountry(elem.PBI_COUNTRY) ?? "";
                    row += "<br/>";
                    row += "Packaging: " + elem.PBI_FK_PACKAGING ?? "";
                    row += "<br/>";
                }
                if (data.PDA_TEMPLATE != "CMLA_FORM_1" && data.PDA_TEMPLATE != "CMLA_FORM_2" && data.PDA_TEMPLATE != "CMLA_IMPORT")
                {
                    row += "Price: " + String.Format("{0:n}", elem.PBI_OFFER_PRICE) + " " + (data.PDA_PRICE_UNIT ?? "");
                    row += "<br/>";
                }
                int count = 1;
                //foreach (PAF_REVIEW_ITEMS_DETAIL ee in PAF_REVIEW_ITEMS_DETAIL)
                //{
                //    string quantity = PAFHelper.GetMinMaxFormat(ee.PRD_CUS_PROPOSAL_QUANTITY_MIN, ee.PRD_CUS_PROPOSAL_QUANTITY_MAX);
                //    if (String.IsNullOrEmpty(quantity) || String.IsNullOrEmpty(ee.PRD_CUS_PROPOSAL_PRICE_FLOAT))
                //    {
                //        continue;
                //    }
                //    row += "Tier: " + count.ToString();
                //    row += "<br/>";
                //    row += "Quantity (Our Proposal): " + quantity;
                //    row += "<br/>";
                //    row += "Price or Formula (Our Proposal): " + ee.PRD_CUS_PROPOSAL_PRICE_FLOAT + "";
                //    row += "<br/>";
                //    count++;
                //}
                //row += "Note: " + note ?? "";
                rows.Add(row);
            }
            if (rows.Count > 0)
            {
                result = "Product Price Bidding";
                result += "<br/>";
            }
            result += String.Join("<br/><br/>", rows);
            return result;
        }

        public static string GetReviewRowsBaseOil()
        {
            string result = "";
            List<string> rows = new List<string>();
            List<string> customers = data.PAF_REVIEW_ITEMS.OrderBy(m => m.PRI_ROW_ID).Select(m => m.PRI_FK_CUSTOMER).ToList();
            if (data.PDA_INCLUDED_REVIEW_TABLE != "Y" || data.PDA_TEMPLATE == "CMLA_IMPORT")
            {
                return result;
            }

            if (customers.Count() > 0)
            {
                customers = customers.Distinct().ToList();
            }
            foreach (string fk_customer in customers)
            {
                MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(fk_customer);
                List<PAF_REVIEW_ITEMS> PAF_REVIEW_ITEMS = data.PAF_REVIEW_ITEMS.Where(m => m.PRI_FK_CUSTOMER == fk_customer).OrderBy(m => m.PRI_ROW_ID).ToList();
                string row = "";
                bool is_first_row = true;
                foreach (PAF_REVIEW_ITEMS elem in PAF_REVIEW_ITEMS)
                {
                    string product = "";
                    if (elem.MT_MATERIALS != null)
                    {
                        product = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    }
                    List<PAF_REVIEW_ITEMS_DETAIL> PAF_REVIEW_ITEMS_DETAIL = elem.PAF_REVIEW_ITEMS_DETAIL.OrderBy(m => m.PRD_ROW_ID).ToList();
                    string quantity = "";
                    try
                    {
                        quantity = elem.PAF_REVIEW_ITEMS_DETAIL.First().PRD_OUR_PROPOSAL_QTY_FLOAT;
                    }
                    catch (Exception e)
                    {

                    }

                    if ((quantity ?? "").Trim() == "")
                    {
                        continue;
                    }

                    if (is_first_row)
                    {
                        row += "Customer: " + customer.MCD_NAME_1 + " " + customer.MCD_NAME_2;
                        row += "<br/>";
                        row += "Product: ";
                        row += "<br/>";
                        is_first_row = false;
                    }
                    row += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + product + " = " + PAFHelper.ShowTolerance(quantity) + " " + (elem.PRI_QUANTITY_UNIT ?? "");
                    row += "<br/>";
                }
                rows.Add(row);
            }
            if (rows.Count > 0)
            {
                result = "Review existing price";
                result += "<br/>";
            }
            result += String.Join("<br/><br/>", rows);
            return result;
        }

        public static string GetAwardedRowCMPSInterContent()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            string result = "";
            List<string> rows = new List<string>();
            List<string> bidding_items = data.PAF_PAYMENT_ITEMS.Select(m => m.PPI_FK_CUSTOMER).ToList();
            List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS = data.PAF_BIDDING_ITEMS.Where(m => bidding_items.Contains(m.PBI_FK_CUSTOMER)).OrderBy(m => m.PBI_ROW_ID).ToList();
            foreach (PAF_BIDDING_ITEMS elem in PAF_BIDDING_ITEMS)
            {
                if (elem.PBI_AWARDED_BIDDER != "Y")
                {
                    continue;
                }
                MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(elem.PBI_FK_CUSTOMER);
                MT_VENDOR vendor = PAF_MASTER_DAL.GetVendor(elem.PBI_FK_VENDOR);
                string product = "";
                string quantity = PAFHelper.GetMinMaxFormat(elem.PBI_AWARDED_QUANTITY_MIN, elem.PBI_AWARDED_QUANTITY_MAX);
                if (elem.MT_MATERIALS != null)
                {
                    product = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                }
                decimal lastround = 0;
                PAF_ROUND round = elem.PAF_ROUND.OrderByDescending(m => m.PRN_ROW_ID).FirstOrDefault();
                if (round != null)
                {
                    lastround = (decimal)round.PRN_FIXED;
                }
                bool need_freight = false;
                decimal freight = 0;
                decimal convert_to_fob = 0;
                string average = GetAverage(elem);
                string benchmark = "";

                try
                {
                    benchmark = elem.MKT_MST_MOPS_PRODUCT.M_MPR_PRDNAME;
                }
                catch (Exception e)
                {

                }
                try
                {
                    need_freight = elem.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                    if (!need_freight && data.PDA_TEMPLATE == "CMPS_IMPORT")
                    {
                        freight = (decimal)elem.PBI_FREIGHT_PRICE;
                        convert_to_fob = lastround + freight;
                    }
                    else if (need_freight && data.PDA_TEMPLATE == "CMPS_INTER_SALE")
                    {
                        freight = (decimal)elem.PBI_FREIGHT_PRICE;
                        convert_to_fob = lastround - freight;
                    }
                }
                catch (Exception e)
                {

                }
                string note = "";
                string row = "";
                if (data.PDA_TEMPLATE == "CMPS_IMPORT")
                {
                    row += "Supplier: " + vendor.VND_NAME1 + " " + vendor.VND_NAME2;
                }
                else
                {
                    row += "Customer: " + customer.MCD_NAME_1 + " " + customer.MCD_NAME_2;
                }
                row += "<br/>";
                row += "Product: " + product;
                row += "<br/>";
                //row += "Unit: " + elem.PBI_QUANTITY_UNIT ?? "";
                //row += "<br/>";
                row += "Quantity: " + (quantity ?? "") + " " + (elem.PBI_QUANTITY_UNIT ?? "");
                row += "<br/>";
                row += "Benchmark: " + benchmark ?? "";
                row += "<br/>";
                row += "Incoterms: " + elem.PBI_INCOTERM ?? "";
                row += "<br/>";
                row += "Premium / Discount: " + (String.Format("{0:n}", lastround) ?? "") + " " + (data.PDA_PRICE_UNIT ?? "");
                row += "<br/>";
                if (!need_freight && data.PDA_TEMPLATE == "CMPS_IMPORT")
                {
                    row += "Freight: " + (String.Format("{0:n}", freight) ?? "") + " " + (data.PDA_PRICE_UNIT ?? "");
                    row += "<br/>";
                    row += "Converted to CFR: " + (String.Format("{0:n}", convert_to_fob) ?? "") + " " + (data.PDA_PRICE_UNIT ?? "");
                    row += "<br/>";
                }
                else if (need_freight && data.PDA_TEMPLATE == "CMPS_INTER_SALE")
                {
                    row += "Freight: " + (String.Format("{0:n}", freight) ?? "") + " " + (data.PDA_PRICE_UNIT ?? "");
                    row += "<br/>";
                    row += "Converted to FOB: " + (String.Format("{0:n}", convert_to_fob) ?? "") + " " + (data.PDA_PRICE_UNIT ?? "");
                    row += "<br/>";
                }

                //row += "<br/>";
                //row += "Average: " + average ?? "";
                //row += "<br/>";

                int count = 1;
                //foreach (PAF_REVIEW_ITEMS_DETAIL ee in PAF_REVIEW_ITEMS_DETAIL)
                //{
                //    string quantity = PAFHelper.GetMinMaxFormat(ee.PRD_CUS_PROPOSAL_QUANTITY_MIN, ee.PRD_CUS_PROPOSAL_QUANTITY_MAX);
                //    if (String.IsNullOrEmpty(quantity) || String.IsNullOrEmpty(ee.PRD_CUS_PROPOSAL_PRICE_FLOAT))
                //    {
                //        continue;
                //    }
                //    row += "Tier: " + count.ToString();
                //    row += "<br/>";
                //    row += "Quantity (Our Proposal): " + quantity;
                //    row += "<br/>";
                //    row += "Price or Formula (Our Proposal): " + ee.PRD_CUS_PROPOSAL_PRICE_FLOAT + "";
                //    row += "<br/>";
                //    count++;
                //}
                rows.Add(row);
            }
            if (rows.Count > 0)
            {
                result = "Product Price Bidding";
                result += "<br/>";
            }
            result = String.Join("<br/><br/>", rows);
            return result;
        }



        public static string GetAwardedRowBaseOilContent()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            string result = "";
            List<string> rows = new List<string>();
            List<string> bidding_items = data.PAF_PAYMENT_ITEMS.Select(m => m.PPI_FK_CUSTOMER).ToList();
            //List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS = data.PAF_BIDDING_ITEMS.Where(m => bidding_items.Contains(m.PBI_FK_CUSTOMER)).OrderBy(m => m.PBI_ROW_ID).ToList();
            if (data.PDA_INCLUDED_BIDDING_TABLE != "Y")
            {
                return result;
            }
            foreach (string fk_customer in bidding_items)
            {
                MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(fk_customer);
                List<PAF_BIDDING_ITEMS> PAF_BIDDING_ITEMS = data.PAF_BIDDING_ITEMS.Where(m => m.PBI_FK_CUSTOMER == fk_customer).OrderBy(m => m.PBI_ROW_ID).ToList();

                string row = "";
                row += "Customer: " + customer.MCD_NAME_1 + " " + customer.MCD_NAME_2;
                row += "<br/>";
                bool is_first_row = true;
                foreach (PAF_BIDDING_ITEMS elem in PAF_BIDDING_ITEMS)
                {
                    string product = "";
                    if (elem.MT_MATERIALS != null)
                    {
                        product = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    }
                    if (is_first_row)
                    {
                        row += "Product: ";
                        row += "<br/>";
                        is_first_row = false;
                    }
                    string quantity = elem.PBI_QUANTITY_FLOAT;
                    row += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; " + product + " = " + (PAFHelper.ShowTolerance(quantity) ?? "") + " " + (elem.PBI_QUANTITY_UNIT ?? "");
                    row += "<br/>";
                }
                rows.Add(row);
            }
            if (rows.Count > 0)
            {
                result = "Product Price Bidding";
                result += "<br/>";
            }
            result += String.Join("<br/><br/>", rows);
            return result;
        }

        public static string GetAverage(PAF_BIDDING_ITEMS source)
        {

            //let sum_quantity_max = _.reduce(products, (sum, e) => {
            //    return sum += +(e.PBI_QUANTITY_MAX || 0);
            //}, 0);
            //let convert_to_fob_max = _.reduce(products, (sum, e) => {
            //    return sum += (+(e.SHOW_CONVERT_TO_FOB || 0) * +(e.PBI_QUANTITY_MAX || 0));
            //}, 0);

            List<PAF_BIDDING_ITEMS> cal_src = data.PAF_BIDDING_ITEMS.Where(m => m.PBI_FK_CUSTOMER == source.PBI_FK_CUSTOMER).OrderBy(m => m.PBI_ROW_ID).ToList();
            decimal sum_quantity_max = 0;
            decimal convert_to_fob_max = 0;
            foreach (PAF_BIDDING_ITEMS src in cal_src)
            {
                decimal lastround = 0;
                PAF_ROUND round = src.PAF_ROUND.OrderByDescending(m => m.PRN_ROW_ID).FirstOrDefault();
                if (round != null)
                {
                    lastround = (decimal)(round.PRN_FIXED ?? 0);
                }
                bool need_freight = false;
                decimal freight = 0;
                decimal convert_to_fob = 0;
                try
                {
                    need_freight = src.MT_INCOTERMS.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                    freight = (decimal)(src.PBI_FREIGHT_PRICE ?? 0);
                    convert_to_fob = lastround - freight;
                }
                catch (Exception e)
                {

                }

                sum_quantity_max += (decimal)src.PBI_QUANTITY_MAX;
                convert_to_fob_max += convert_to_fob;
            }

            if (sum_quantity_max == 0)
            {
                return "";
            }
            decimal result_max = convert_to_fob_max / sum_quantity_max;
            return String.Format("{0:n}", result_max);
        }

        public static string GetSpecialSummaryForBitumen()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            string result = "";
            foreach (PAF_BTM_GRADE_ITEMS a in data.PAF_BTM_GRADE_ITEMS)
            {
                List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> pbais = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
                result += "Bitumen Grade: " + a.MT_MATERIALS.MET_MAT_DES_ENGLISH + "<br/><br/>";
                foreach (PAF_BTM_ALLOCATE_ITEMS b in a.PAF_BTM_ALLOCATE_ITEMS.OrderBy(m => m.PBA_ROW_ID))
                {
                    string customer_name = "";
                    try
                    {
                        MT_CUST_DETAIL customer = PAF_MASTER_DAL.GetCustomerById(b.PBA_FK_CUSTOMER);
                        customer_name = customer.MCD_NAME_1 + (customer.MCD_NAME_2 ?? "");
                    }
                    catch (Exception e)
                    {

                    }

                    List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> cs = GetGroupItemsList(b.PAF_BTM_ALLOCATE_ITEMS_DETAIL.OrderBy(m => m.PAD_ROW_ID).ToList());
                    pbais.AddRange(cs);
                    foreach (PAF_BTM_ALLOCATE_ITEMS_DETAIL c in cs)
                    {
                        string tier_name = "";
                        decimal fx_rate = 0.00000001M;
                        try
                        {
                            tier_name = c.PAF_MT_BTM_TIER.PMT_DETAIL;
                            fx_rate = (decimal)data.PAF_BTM_DETAIL.PBD_ESTIMATED_FX;
                        }
                        catch (Exception e)
                        {

                        }

                        decimal selling_argus = Math.Round((decimal)(c.PAD_USD_PER_UNIT), 0) - (decimal)data.PAF_BTM_DETAIL.PBD_ARGUS_PRICE;
                        decimal selling_hsfo =  Math.Round((decimal)(c.PAD_USD_PER_UNIT), 0) - Math.Round((decimal)data.PAF_BTM_DETAIL.PBD_AVG_HSFO, 2);
                        selling_argus = Math.Round(selling_argus, 0);
                        selling_hsfo = Math.Round(selling_hsfo, 0);
                        result += "Customer: " + customer_name + "<br/>";
                        result += "Tier: " + tier_name + "<br/>";
                        result += "Quantity: " + (String.Format("{0:n0}", c.PAD_TIER_VOLUME) ?? "") + " MT <br/>";
                        result += "Price: " + (String.Format("{0:n0}", c.PAD_USD_PER_UNIT) ?? "") + " USD/MT<br/>";
                        result += "Selling Price - Argus: " + String.Format("{0:n0}", selling_argus) + " USD/MT<br/>";
                        result += "Selling Price - HSFO: " + String.Format("{0:n0}", selling_hsfo) + " USD/MT<br/>";
                        result += "<br/>";
                    }
                }

                decimal sum_volume = (decimal)pbais.Sum(m => m.PAD_TIER_VOLUME);
                decimal fx_rate_s = (decimal)data.PAF_BTM_DETAIL.PBD_ESTIMATED_FX;
                decimal weigted_price = Math.Round(Math.Round((decimal)(pbais.Sum(m => m.PAD_USD_PER_UNIT * m.PAD_TIER_VOLUME) / sum_volume)));

                string compare_plan = "";
                if (weigted_price < data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " < Simplan, Corp. Plan";
                }
                else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " > Simplan, Corp. Plan";
                }
                else if (weigted_price < data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " < Simplan, > Corp. Plan";
                }
                else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " > Simplan, < Corp. Plan";
                }
                else if (weigted_price > data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price == data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " > Simplan, = Corp. Plan";
                }
                else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price > data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " = Simplan, > Corp. Plan";
                }
                else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price < data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " = Simplan, < Corp. Plan";
                }
                else if (weigted_price == data.PAF_BTM_DETAIL.PBD_SIM_PLAN && weigted_price == data.PAF_BTM_DETAIL.PBD_CROP_PLAN)
                {
                    compare_plan = " = Simplan, Corp. Plan";
                }

                result += "Weighted Price: " + (String.Format("{0:n0}", weigted_price) ?? "") + " USD/MT " + compare_plan + "<br/>";
                result += "Weighted Price - Argus: " + (String.Format("{0:n0}", weigted_price - data.PAF_BTM_DETAIL.PBD_ARGUS_PRICE) ?? "") + " USD/MT" + "<br/>";
                result += "Weighted Price - HSFO: " + (String.Format("{0:n0}", weigted_price - data.PAF_BTM_DETAIL.PBD_AVG_HSFO) ?? "") + " USD/MT" + "<br/>";
                result += "<br/>";
            }
            return result;

        }

        public static List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> GetGroupItemsList(List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> source)
        {
            List<PAF_BTM_ALLOCATE_ITEMS_DETAIL> result = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
            Dictionary<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>> group_result = new Dictionary<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>>();

            foreach (PAF_BTM_ALLOCATE_ITEMS_DETAIL b in source)
            {
                try
                {
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP].Add(b);
                }
                catch (Exception e)
                {
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP] = new List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>();
                    group_result[b.PAF_MT_BTM_TIER.PMT_GROUP].Add(b);
                }
            }

            foreach (KeyValuePair<string, List<PAF_BTM_ALLOCATE_ITEMS_DETAIL>> entry in group_result)
            {
                PAF_BTM_ALLOCATE_ITEMS_DETAIL a = new PAF_BTM_ALLOCATE_ITEMS_DETAIL();
                a.PAF_MT_BTM_TIER = entry.Value.FirstOrDefault().PAF_MT_BTM_TIER;
                a.PAD_TIER_VOLUME = entry.Value.Sum(m => m.PAD_TIER_VOLUME);
                if (a.PAD_TIER_VOLUME == 0)
                {
                    a.PAD_USD_PER_UNIT = 0;
                }
                else
                {
                    a.PAD_USD_PER_UNIT = (entry.Value.Sum(m => (m.PAD_USD_PER_UNIT * m.PAD_TIER_VOLUME))) / a.PAD_TIER_VOLUME;
                }

                a.PAD_USD_PER_UNIT = Math.Round((decimal)a.PAD_USD_PER_UNIT);
                result.Add(a);
            }

            return result;
        }


        public static String getNameFromCountry(String country) // done
        {

            EntityCPAIEngine db = new EntityCPAIEngine();
            if (db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault() != null)
            {
                return db.MT_COUNTRY.Where(c => c.MCT_ROW_ID == country).FirstOrDefault().MCT_LANDX + "";
            }
            return "";
        }
    }
}
