﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALPAF
{
    public class PAF_MASTER_CRITERIA
    {
        public string ID { get; set; }
        public string VALUE { get; set; }
        public string MET_PRODUCT_DENSITY { get; set; }
        public string COMPANY_CODE { get; set; }
        public string NAME { get; set; }
        public string GROUP { get; set; }
        public string DETAIL { get; set; }
        public string FK_MT_CUST { get; set; }
        public string PAYMENT_TERM_DETAIL { get; set; }
        public bool INCLUDED_FREIGHT { get; set; }
    }

    public class PAF_MASTER_DAL
    {
        public static string[] GetArrayFromGlobalConfig(string key)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == key).FirstOrDefault();
            if (gc != null)
            {
                string raw_currency = gc.GCG_VALUE;
                string[] result = JsonConvert.DeserializeObject<string[]>(raw_currency);
                return result;
            }
            return new string[0];
        }

        public static List<PAF_MASTER_CRITERIA> GetDataFromGlobalConfigArray(string key)
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            string[] ret_from_gc = GetArrayFromGlobalConfig(key);
            foreach (string elem in ret_from_gc)
            {
                PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                cur.ID = elem;
                cur.VALUE = elem;
                results.Add(cur);
            }
            return results;
        }

        public static MT_CUST_DETAIL GetCustomerById(string id)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            MT_CUST_DETAIL customer_detail = context.MT_CUST_DETAIL.Where(m => m.MCD_NATION == "I" && m.MCD_FK_CUS == id).FirstOrDefault();
            return customer_detail;
        }

        public static string GetCustomerName(string id)
        {
            string customername = null;
            MT_CUST_DETAIL customer = GetCustomerById(id);
            if (customer != null) {
                customername = (customer.MCD_NAME_1 ?? "") + " " + (customer.MCD_NAME_2 ?? "");
            }
            return customername;
        }

        public static string GetMaterailName(PAF_REVIEW_ITEMS elem)
        {
            string product_name = "";
            try
            {
                product_name = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
            }
            catch (Exception e)
            {

            }

            return product_name;
        }

        public static string GetMaterailName(PAF_BIDDING_ITEMS elem)
        {
            string product_name = "";
            try
            {
                product_name = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
            }
            catch (Exception e)
            {

            }

            return product_name;
        }


        public static MT_VENDOR GetVendor(string vendor)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            return db.MT_VENDOR.Where(c => c.VND_ACC_NUM_VENDOR == vendor).FirstOrDefault();
        }

        public static string GetVendorName(string vendor_id)
        {
            string vendor_name = null;
            MT_VENDOR vendor = GetVendor(vendor_id);
            if(vendor != null)
            {
                vendor_name = (vendor.VND_NAME1 ?? "") + " " + (vendor.VND_NAME2??"");
            }

            return vendor_name;
        }


        public static MT_VENDOR GetSupplierById(string id)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            MT_VENDOR detail = context.MT_VENDOR.Where(m => m.VND_ACC_NUM_VENDOR == id).FirstOrDefault();
            return detail;
        }

        public static string GetPaymentTermForCustomer(string MCD_FK_CUS, string COMPANY_CODE)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            //string query = "SELECT MT_PAYMENT.MPM_DETAIL FROM MT_PAYMENT LEFT JOIN MT_CUST_DATA ON MT_CUST_DATA.MCS_TERMS_PAYMENT_KEY = MT_PAYMENT.MPM_ROW_ID WHERE MT_CUST_DATA.MCS_STATUS = 'ACTIVE' AND MT_CUST_DATA.MCS_FK_CUS = :MCD_FK_CUS AND MT_CUST_DATA.MCS_SALSE_ORG = :COMPANY_CODE";
            string query = "( SELECT MT_PAYMENT.MPM_DETAIL FROM( SELECT * FROM MT_CUST_DATA WHERE MCS_SALSE_ORG = :COMPANY_CODE AND MCS_STATUS = 'ACTIVE') G1 LEFT OUTER JOIN MT_CUST_DETAIL ON G1.MCS_FK_CUS = MT_CUST_DETAIL.MCD_FK_CUS INNER JOIN MT_PAYMENT ON G1.MCS_TERMS_PAYMENT_KEY = MT_PAYMENT.MPM_ROW_ID AND G1.MCS_SALSE_ORG = MT_CUST_DETAIL.MCD_FK_COMPANY WHERE MT_CUST_DETAIL.MCD_NATION = 'I' AND MT_CUST_DETAIL.MCD_STATUS = 'ACTIVE' AND MT_CUST_DETAIL.MCD_FK_CUS = :MCD_FK_CUS)";
            string result = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("MCD_FK_CUS", MCD_FK_CUS), new OracleParameter("COMPANY_CODE", COMPANY_CODE) }).FirstOrDefault();
            return result;
        }

        public static string GetPaymentTermForVendor(string MVD_FK_VENDOR, string MVD_FK_COMPANY)
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            string query = "SELECT MT_PAYMENT.MPM_DETAIL FROM MT_PAYMENT LEFT JOIN MT_VENDOR_TERMS_PAY ON MT_VENDOR_TERMS_PAY.MVT_TERMS_PAY_KEY = MT_PAYMENT.MPM_ROW_ID LEFT JOIN MT_VENDOR_DATA ON MT_VENDOR_DATA.MVD_FK_TERMS_PAY = MT_VENDOR_TERMS_PAY.MVT_ROW_ID WHERE MT_VENDOR_TERMS_PAY.MVT_STATUS = 'ACTIVE' AND MT_VENDOR_DATA.MVD_FK_VENDOR = :MVD_FK_VENDOR AND MT_VENDOR_DATA.MVD_FK_COMPANY = :MVD_FK_COMPANY";
            string result = context.Database.SqlQuery<string>(query, new[] { new OracleParameter("MVD_FK_VENDOR", MVD_FK_VENDOR), new OracleParameter("MVD_FK_COMPANY", MVD_FK_COMPANY) }).FirstOrDefault();
            return result;
        }
        
        public static List<PAF_MASTER_CRITERIA> GetCustomerList()
        {
            List<PAF_MASTER_CRITERIA> customers = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m => m.MCD_NATION == "I" && m.MCD_STATUS == "ACTIVE").ToList();
                foreach (MT_CUST_DETAIL cust in customer_details)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = cust.MCD_FK_CUS;
                    criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                    criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                    criteria.PAYMENT_TERM_DETAIL = GetPaymentTermForCustomer(cust.MCD_FK_CUS, cust.MCD_FK_COMPANY);
                    customers.Add(criteria);
                }
            }

            return customers;
        }

        public static List<PAF_MASTER_CRITERIA> GetCustomerBitumenList()
        {
            List<PAF_MASTER_CRITERIA> customers = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m => m.MCD_NATION == "I" && m.MCD_FK_COMPANY == "1400" && m.MCD_STATUS == "ACTIVE").ToList();
                foreach (MT_CUST_DETAIL cust in customer_details)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = cust.MCD_FK_CUS;
                    criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                    criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                    criteria.PAYMENT_TERM_DETAIL = GetPaymentTermForCustomer(cust.MCD_FK_CUS, cust.MCD_FK_COMPANY);
                    customers.Add(criteria);
                }
            }

            return customers;
        }
        
        public static List<PAF_MASTER_CRITERIA> GetCustomerInterList()
        {
            List<PAF_MASTER_CRITERIA> customers = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m =>
                    (
                        (m.MCD_KTOKD == "TRDF" && m.MCD_NATION == "I") ||
                        (m.MCD_KTOKD == "TRDL" && m.MCD_NATION == "T") ||
                        m.MCD_KTOKD == null
                    ) &&
                    m.MCD_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_CUST_DETAIL cust in customer_details)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = cust.MCD_FK_CUS;
                    criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                    criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                    criteria.PAYMENT_TERM_DETAIL = GetPaymentTermForCustomer(cust.MCD_FK_CUS, cust.MCD_FK_COMPANY);
                    customers.Add(criteria);
                }
            }

            return customers;
        }

        public static List<PAF_MASTER_CRITERIA> GetCustomerDomList()
        {
            List<PAF_MASTER_CRITERIA> customers = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m => 
                    (m.MCD_KTOKD == "TRDF" || m.MCD_KTOKD == "TRDL" || m.MCD_KTOKD == null) && 
                    m.MCD_NATION == "I" && 
                    m.MCD_STATUS == "ACTIVE"
                ).ToList();

                foreach (MT_CUST_DETAIL cust in customer_details)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = cust.MCD_FK_CUS;
                    criteria.VALUE = cust.MCD_NAME_1 + " " + (cust.MCD_NAME_2 ?? "");
                    criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
                    customers.Add(criteria);
                }
            }

            return customers;
        }

        //// TODO: asking for clarify
        //public static List<PAF_MASTER_CRITERIA> GetCustomerList()
        //{
        //    List<PAF_MASTER_CRITERIA> customers = new List<PAF_MASTER_CRITERIA>();
        //    using (var context = new EntityCPAIEngine())
        //    {
        //        List<MT_CUST_DETAIL> customer_details = context.MT_CUST_DETAIL.Where(m => m.MCD_NATION == "I").ToList();
        //        foreach (MT_CUST_DETAIL cust in customer_details)
        //        {
        //            PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
        //            criteria.ID = cust.MCD_FK_CUS;
        //            criteria.VALUE = cust.MCD_NAME_1;
        //            criteria.COMPANY_CODE = cust.MCD_FK_COMPANY;
        //            customers.Add(criteria);
        //        }
        //    }

        //    return customers;
        //}

        public static List<PAF_MASTER_CRITERIA> GetProductInterList()
        {
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();

            using (var context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                   m.MMC_SYSTEM == "PAF_CMPS_INTER_SALE" &&
                   m.MMC_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_MATERIALS_CONTROL elem in materials)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = elem.MT_MATERIALS.MET_NUM;
                    criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    criteria.NAME = elem.MMC_NAME;
                    criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                    products.Add(criteria);
                }
            }
            return products;
        }
    
        public static List<PAF_MASTER_CRITERIA> GetProductDomList()
        {
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();

            using (var context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                   m.MMC_SYSTEM == "PAF_CMPS_DOM_SALE" &&
                   m.MMC_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_MATERIALS_CONTROL elem in materials)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = elem.MT_MATERIALS.MET_NUM;
                    criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    criteria.NAME = elem.MMC_NAME;
                    criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                    products.Add(criteria);
                }
            }
            return products;
        }

        public static List<PAF_MASTER_CRITERIA> GetPriceUnits()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CURRENCY");
            return results;
            //try
            //{
            //    using (var context = new EntityCPAIEngine())
            //    {
            //        GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CURRENCY").FirstOrDefault();
            //        string raw_currency = gc.GCG_VALUE;
            //        string[] currencies = JsonConvert.DeserializeObject<string[]>(raw_currency);
            //        foreach (string elem in currencies)
            //        {
            //            PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //            cur.ID = elem;
            //            cur.VALUE = elem;
            //            results.Add(cur);
            //        }
            //    }
            //}
            //catch (Exception e)
            //{

            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetQuantityUnits()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_VOLUMN_UNIT");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_VOLUMN_UNIT").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] volume_units = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in volume_units)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetCreditDetails()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<PAF_MT_CREDIT_DETAIL> credit_details = context.PAF_MT_CREDIT_DETAIL.ToList();
                foreach (PAF_MT_CREDIT_DETAIL elem in credit_details)
                {
                    PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                    cur.ID = elem.PMC_ROW_ID;
                    cur.VALUE = elem.PMC_NAME;
                    results.Add(cur);
                }
            }
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetCurrencyPerVolumnUnit()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CURRENCY_PER_VOLUMN_UNIT");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CURRENCY_PER_VOLUMN_UNIT").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] elems = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in elems)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetCompany()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            PAF_MASTER_CRITERIA com = null;
            com = new PAF_MASTER_CRITERIA { ID = "1100", VALUE = "TOP"};
            results.Add(com);
            com = new PAF_MASTER_CRITERIA { ID = "1400", VALUE = "TLB" };
            results.Add(com);
            com = new PAF_MASTER_CRITERIA { ID = "1300", VALUE = "TPX" };
            results.Add(com);
            com = new PAF_MASTER_CRITERIA { ID = "1700", VALUE = "LABIX" };
            results.Add(com);
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetIncoterms()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_INCOTERMS> incoterms = context.MT_INCOTERMS.ToList();
                foreach (MT_INCOTERMS elem in incoterms)
                {
                    PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                    cur.ID = elem.MIN_INCOTERMS;
                    cur.VALUE = elem.MIN_INCOTERMS;
                    cur.INCLUDED_FREIGHT = elem.PAF_MT_INCOTERMS.PMI_INCLUDED_FREIGHT == "Y";
                    results.Add(cur);
                }
            }
            return results;
        }
        public static List<PAF_MASTER_CRITERIA> GetCountry()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_COUNTRY> res = context.MT_COUNTRY.Where( m => m.MCT_CLIENT == "900").ToList();
                foreach (MT_COUNTRY elem in res)
                {
                    PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                    cur.ID = elem.MCT_ROW_ID;
                    cur.VALUE = elem.MCT_LANDX;
                    results.Add(cur);
                }
            }
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetBenchMark()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MKT_MST_MOPS_PRODUCT> res = context.MKT_MST_MOPS_PRODUCT.Where(m => m.M_MPR_EXPORT == "T").ToList();
                foreach (MKT_MST_MOPS_PRODUCT elem in res)
                {
                    PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                    cur.ID = elem.M_MPR_ID.ToString();
                    cur.VALUE = elem.M_MPR_PRDNAME;
                    results.Add(cur);
                }
            }
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetVendors()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            List<PAF_MASTER_CRITERIA> results = context.Database.SqlQuery<PAF_MASTER_CRITERIA>("SELECT MT_VENDOR.VND_ACC_NUM_VENDOR as ID, MT_VENDOR.VND_NAME1 || ' ' || MT_VENDOR.VND_NAME2 as VALUE, MT_VENDOR_DATA.MVD_FK_COMPANY as COMPANY_CODE, MT_PAYMENT.MPM_DETAIL as PAYMENT_TERM_DETAIL FROM MT_VENDOR LEFT JOIN MT_VENDOR_DATA ON MT_VENDOR.VND_ACC_NUM_VENDOR = MT_VENDOR_DATA.MVD_FK_VENDOR LEFT JOIN MT_VENDOR_TERMS_PAY ON MT_VENDOR_DATA.MVD_FK_TERMS_PAY = MT_VENDOR_TERMS_PAY.MVT_ROW_ID LEFT JOIN MT_PAYMENT ON MT_VENDOR_TERMS_PAY.MVT_TERMS_PAY_KEY = MT_PAYMENT.MPM_ROW_ID WHERE MT_VENDOR.VND_STATUS  = 'ACTIVE'").ToList();
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetPaymentTerm()
        {
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            using (var context = new EntityCPAIEngine())
            {
                List<MT_CUST_PAYMENT_TERM> res = context.MT_CUST_PAYMENT_TERM.ToList();
                foreach (MT_CUST_PAYMENT_TERM elem in res)
                {
                    PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
                    cur.ID = elem.MCP_PAYMENT_TERM ?? "";
                    cur.VALUE = elem.MCP_PAYMENT_TERM ?? "";
                    cur.FK_MT_CUST = elem.MCP_FK_MT_CUST;
                    results.Add(cur);
                }
            }
            return results;
        }

        public static List<PAF_MASTER_CRITERIA> ToleranceOptions()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_QUANTITY_TOLERANCE_OPTION");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_QUANTITY_TOLERANCE_OPTION").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] elems = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in elems)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetProductCMLAList()
        {
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();

            using (var context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                   m.MMC_SYSTEM == "PAF_CMLA_FORM_1" &&
                   m.MMC_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_MATERIALS_CONTROL elem in materials)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = elem.MT_MATERIALS.MET_NUM;
                    criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    criteria.NAME = elem.MMC_NAME;
                    criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                    products.Add(criteria);
                }
            }
            return products;
        }

        public static List<PAF_MASTER_CRITERIA> GetProductCMLAForm2List()
        {
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();

            using (var context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                   m.MMC_SYSTEM == "PAF_CMLA_FORM_2" &&
                   m.MMC_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_MATERIALS_CONTROL elem in materials)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = elem.MT_MATERIALS.MET_NUM;
                    criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    criteria.NAME = elem.MMC_NAME;
                    criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                    products.Add(criteria);
                }
            }
            return products;
        }

        public static List<PAF_MASTER_CRITERIA> GetProductCMLAImportList()
        {
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();

            using (var context = new EntityCPAIEngine())
            {
                List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                   m.MMC_SYSTEM == "PAF_CMLA_IMPORT" &&
                   m.MMC_STATUS == "ACTIVE"
                ).ToList();
                foreach (MT_MATERIALS_CONTROL elem in materials)
                {
                    PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                    criteria.ID = elem.MT_MATERIALS.MET_NUM;
                    criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                    criteria.NAME = elem.MMC_NAME;
                    criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                    products.Add(criteria);
                }
            }
            return products;
        }

        public static List<PAF_MASTER_CRITERIA> GetProductCMLABitumenList()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            List<PAF_MASTER_CRITERIA> products = new List<PAF_MASTER_CRITERIA>();
            List<MT_MATERIALS_CONTROL> materials = context.MT_MATERIALS_CONTROL.Where(m =>
                m.MMC_SYSTEM == "PAF_CMLA_BITUMEN" &&
                m.MMC_STATUS == "ACTIVE"
            ).ToList();
            foreach (MT_MATERIALS_CONTROL elem in materials)
            {
                PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                criteria.ID = elem.MT_MATERIALS.MET_NUM;
                criteria.VALUE = elem.MT_MATERIALS.MET_MAT_DES_ENGLISH;
                criteria.NAME = elem.MMC_NAME;
                criteria.MET_PRODUCT_DENSITY = elem.MT_MATERIALS.MET_PRODUCT_DENSITY;
                products.Add(criteria);
            }
            return products;
        }
        
        public static List<PAF_MASTER_CRITERIA> GetQuantityUnitsCMLA()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CMLA_VOLUMN_UNIT");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CMLA_VOLUMN_UNIT").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] volume_units = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in volume_units)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetCurrencyPerVolumnUnitCMLA()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CMLA_CURRENCY_PER_VOLUMN_UNIT");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CMLA_CURRENCY_PER_VOLUMN_UNIT").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] elems = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in elems)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }
        public static List<PAF_MASTER_CRITERIA> GetFormula()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CMLA_FORMULA");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CMLA_FORMULA").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] elems = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in elems)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }
        public static List<PAF_MASTER_CRITERIA> GetPackaging()
        {
            List<PAF_MASTER_CRITERIA> results = GetDataFromGlobalConfigArray("PAF_CMLA_PACKAGING");
            return results;
            //List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            //using (var context = new EntityCPAIEngine())
            //{
            //    GLOBAL_CONFIG gc = context.GLOBAL_CONFIG.Where(m => m.GCG_KEY == "PAF_CMLA_PACKAGING").FirstOrDefault();
            //    string raw = gc.GCG_VALUE;
            //    string[] elems = JsonConvert.DeserializeObject<string[]>(raw);
            //    foreach (string elem in elems)
            //    {
            //        PAF_MASTER_CRITERIA cur = new PAF_MASTER_CRITERIA();
            //        cur.ID = elem;
            //        cur.VALUE = elem;
            //        results.Add(cur);
            //    }
            //}
            //return results;
        }

        public static List<PAF_MASTER_CRITERIA> GetBTMTier()
        {
            EntityCPAIEngine context = new EntityCPAIEngine();
            List<PAF_MASTER_CRITERIA> results = new List<PAF_MASTER_CRITERIA>();
            List<PAF_MT_BTM_TIER> btm_tiers = context.PAF_MT_BTM_TIER.ToList();
            foreach (PAF_MT_BTM_TIER elem in btm_tiers)
            {
                PAF_MASTER_CRITERIA criteria = new PAF_MASTER_CRITERIA();
                criteria.ID = elem.PMT_ROW_ID;
                criteria.NAME = elem.PMT_NAME;
                criteria.GROUP = elem.PMT_GROUP;
                criteria.DETAIL = elem.PMT_DETAIL;
                results.Add(criteria);
            }
            return results;
        }

        public static double GetHSFO(HSFO_FX_CRITERIA criteria)
        {
            string FROM = criteria.DATE_FROM.ToString("yyyyMMdd");
            string TO = criteria.DATE_TO.ToString("yyyyMMdd");
            string query = @"
                SELECT
                 AVG(MKT_TRN_MOPS_B.T_MPB_VALUE)
                FROM MKT_TRN_MOPS_B
                WHERE
                MKT_TRN_MOPS_B.T_MPB_VALTYPE = 'M'
                AND MKT_TRN_MOPS_B.T_MPB_VALDATE BETWEEN :T_MPB_VALDATE_FROM AND :T_MPB_VALDATE_TO
                AND MKT_TRN_MOPS_B.T_MPB_PRDCODE = 'PUADV00'
                AND MKT_TRN_MOPS_B.T_MPB_ENABLED = 'T'
            ";
            EntityCPAIEngine context = new EntityCPAIEngine();
            double result = 0;
            try
            {
                result = context.Database.SqlQuery<double>(query, new[] { new OracleParameter("T_MPB_VALDATE_FROM", FROM), new OracleParameter("T_MPB_VALDATE_TO", TO) }).DefaultIfEmpty(0).FirstOrDefault();
                result = Math.Round(result, 2);
            }
            catch (Exception e)
            {
                result = 0;
            }
            return result;
        }

        public static double GetFX(HSFO_FX_CRITERIA criteria)
        {
            //Nullable<Decimal> result = 0;
            string FROM = criteria.DATE_FROM.ToString("yyyyMMdd");
            string TO = criteria.DATE_TO.ToString("yyyyMMdd");
            string query = @"
                SELECT
                    AVG(MKT_TRN_FX_B.T_FXB_VALUE1)
                FROM MKT_TRN_FX_B
                WHERE 
                    MKT_TRN_FX_B.T_FXB_VALTYPE = 'M' 
                    AND MKT_TRN_FX_B.T_FXB_CUR = 'USD'
                    AND MKT_TRN_FX_B.T_FXB_VALDATE BETWEEN :T_FXB_VALDATE_FROM AND :T_FXB_VALDATE_TO
                    AND MKT_TRN_FX_B.T_FXB_ENABLED = 'T'
            ";

            EntityCPAIEngine context = new EntityCPAIEngine();
            double result = 0;
            try
            {
                result = context.Database.SqlQuery<double>(query, new[] { new OracleParameter("T_FXB_VALDATE_FROM", FROM), new OracleParameter("T_FXB_VALDATE_TO", TO) }).DefaultIfEmpty(0).FirstOrDefault();
                result = Math.Round(result, 2);
            }
            catch (Exception e)
            {
                result = 0;
            }
            return result;
        }
    }
}
