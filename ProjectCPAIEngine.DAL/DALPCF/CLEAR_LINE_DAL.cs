﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class CLEAR_LINE_DAL
    {
        public void SaveClearLineCrude(CLEAR_LINE_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.CLEAR_LINE_CRUDE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveClearLineCrude(CLEAR_LINE_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                context.CLEAR_LINE_CRUDE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateClearLineCrude(CLEAR_LINE_CRUDE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _ClearLineCrude = context.CLEAR_LINE_CRUDE.Where(x=>x.CLC_TRIP_NO.ToUpper().Equals(data.CLC_TRIP_NO.ToUpper()) 
                                                                      && x.CLC_CRUDE_TYPE_ID.ToUpper().Equals(data.CLC_CRUDE_TYPE_ID.ToUpper())
                                                                      && x.CLC_CUST_NUM.ToUpper().Equals(data.CLC_CUST_NUM.ToUpper()) 
                                                                        ).ToList().FirstOrDefault();
                    if (_ClearLineCrude != null)
                    {
                        _ClearLineCrude.CLC_TRIP_NO = data.CLC_TRIP_NO;
                        _ClearLineCrude.CLC_PO_NO = data.CLC_PO_NO;
                        _ClearLineCrude.CLC_CUST_NUM = data.CLC_CUST_NUM;
                        _ClearLineCrude.CLC_DELIVERY_DATE = data.CLC_DELIVERY_DATE;
                        _ClearLineCrude.CLC_DUE_DATE = data.CLC_DUE_DATE;
                        _ClearLineCrude.CLC_PLANT = data.CLC_PLANT;
                        _ClearLineCrude.CLC_CRUDE_TYPE_ID = data.CLC_CRUDE_TYPE_ID;
                        _ClearLineCrude.CLC_VOLUME_BBL = data.CLC_VOLUME_BBL;
                        _ClearLineCrude.CLC_VOLUME_MT = data.CLC_VOLUME_MT;
                        _ClearLineCrude.CLC_VOLUME_LITE = data.CLC_VOLUME_LITE;
                        _ClearLineCrude.CLC_OUTTURN_BBL = data.CLC_OUTTURN_BBL;
                        _ClearLineCrude.CLC_OUTTURN_LITE = data.CLC_OUTTURN_LITE;
                        _ClearLineCrude.CLC_OUTTURN_MT = data.CLC_OUTTURN_MT;
                        _ClearLineCrude.CLC_PRICE_PER = data.CLC_PRICE_PER;
                        _ClearLineCrude.CLC_PRICE = data.CLC_PRICE;
                        _ClearLineCrude.CLC_UNIT_ID = data.CLC_UNIT_ID;
                        _ClearLineCrude.CLC_EXCHANGE_RATE = data.CLC_EXCHANGE_RATE;
                        _ClearLineCrude.CLC_TYPE = data.CLC_TYPE;
                        _ClearLineCrude.CLC_CREATE_DATE = data.CLC_CREATE_DATE;
                        _ClearLineCrude.CLC_CREATE_BY = data.CLC_CREATE_BY;
                        _ClearLineCrude.CLC_LAST_MODIFY_DATE = data.CLC_LAST_MODIFY_DATE;
                        _ClearLineCrude.CLC_LAST_MODIFY_BY = data.CLC_LAST_MODIFY_BY;
                        _ClearLineCrude.CLC_STATUS = data.CLC_STATUS;
                        _ClearLineCrude.CLC_CREATE_TIME = data.CLC_CREATE_TIME;
                        _ClearLineCrude.CLC_LAST_MODIFY_TIME = data.CLC_LAST_MODIFY_TIME;
                        _ClearLineCrude.CLC_RELEASE = data.CLC_RELEASE;
                        _ClearLineCrude.CLC_TANK = data.CLC_TANK;
                        _ClearLineCrude.CLC_SALEORDER = data.CLC_SALEORDER;
                        _ClearLineCrude.CLC_PRICE_RELEASE = data.CLC_PRICE_RELEASE;
                        _ClearLineCrude.CLC_REMARK = data.CLC_REMARK;
                        _ClearLineCrude.CLC_DISTRI_CHANN = data.CLC_DISTRI_CHANN;
                        _ClearLineCrude.CLC_TABLE_NAME = data.CLC_TABLE_NAME;
                        _ClearLineCrude.CLC_TOTAL = data.CLC_TOTAL;
                        _ClearLineCrude.CLC_MEMO = data.CLC_MEMO;
                        _ClearLineCrude.CLC_DATE_SAP = data.CLC_DATE_SAP;
                        _ClearLineCrude.CLC_FI_DOC = data.CLC_FI_DOC;
                        _ClearLineCrude.CLC_DO_NO = data.CLC_DO_NO;
                        _ClearLineCrude.CLC_STATUS_TO_SAP = data.CLC_STATUS_TO_SAP;
                        _ClearLineCrude.CLC_VESSEL_ID = data.CLC_VESSEL_ID;
                        _ClearLineCrude.CLC_UNIT_TOTAL = data.CLC_UNIT_TOTAL;
                        _ClearLineCrude.CLC_FI_DOC_REVERSE = data.CLC_FI_DOC_REVERSE;
                        _ClearLineCrude.CLC_INCOTERM_TYPE = data.CLC_INCOTERM_TYPE;
                        _ClearLineCrude.CLC_TEMP = data.CLC_TEMP;
                        _ClearLineCrude.CLC_DENSITY = data.CLC_DENSITY;
                        _ClearLineCrude.CLC_STORAGE_LOCATION = data.CLC_STORAGE_LOCATION;
                        _ClearLineCrude.CLC_UPDATE_DO = data.CLC_UPDATE_DO;
                        _ClearLineCrude.CLC_UPDATE_GI = data.CLC_UPDATE_GI;
                        _ClearLineCrude.CLC_INVOICE_FIGURE = data.CLC_INVOICE_FIGURE;
                        _ClearLineCrude.CLC_SALE_UNIT = data.CLC_SALE_UNIT;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateClearLineCrude(CLEAR_LINE_CRUDE data, EntityCPAIEngine context)
        {
            try
            {
                var _ClearLineCrude = context.CLEAR_LINE_CRUDE.Where(x => x.CLC_TRIP_NO.ToUpper().Equals(data.CLC_TRIP_NO.ToUpper())
                                                                      && x.CLC_CRUDE_TYPE_ID.ToUpper().Equals(data.CLC_CRUDE_TYPE_ID.ToUpper())
                                                                      && x.CLC_CUST_NUM.ToUpper().Equals(data.CLC_CUST_NUM.ToUpper())
                                                                        ).ToList().FirstOrDefault();
                if (_ClearLineCrude != null)
                {
                    _ClearLineCrude.CLC_TRIP_NO = data.CLC_TRIP_NO;
                    _ClearLineCrude.CLC_PO_NO = data.CLC_PO_NO;
                    _ClearLineCrude.CLC_CUST_NUM = data.CLC_CUST_NUM;
                    _ClearLineCrude.CLC_DELIVERY_DATE = data.CLC_DELIVERY_DATE;
                    _ClearLineCrude.CLC_DUE_DATE = data.CLC_DUE_DATE;
                    _ClearLineCrude.CLC_PLANT = data.CLC_PLANT;
                    _ClearLineCrude.CLC_CRUDE_TYPE_ID = data.CLC_CRUDE_TYPE_ID;
                    _ClearLineCrude.CLC_VOLUME_BBL = data.CLC_VOLUME_BBL;
                    _ClearLineCrude.CLC_VOLUME_MT = data.CLC_VOLUME_MT;
                    _ClearLineCrude.CLC_VOLUME_LITE = data.CLC_VOLUME_LITE;
                    _ClearLineCrude.CLC_OUTTURN_BBL = data.CLC_OUTTURN_BBL;
                    _ClearLineCrude.CLC_OUTTURN_LITE = data.CLC_OUTTURN_LITE;
                    _ClearLineCrude.CLC_OUTTURN_MT = data.CLC_OUTTURN_MT;
                    _ClearLineCrude.CLC_PRICE_PER = data.CLC_PRICE_PER;
                    _ClearLineCrude.CLC_PRICE = data.CLC_PRICE;
                    _ClearLineCrude.CLC_UNIT_ID = data.CLC_UNIT_ID;
                    _ClearLineCrude.CLC_EXCHANGE_RATE = data.CLC_EXCHANGE_RATE;
                    _ClearLineCrude.CLC_TYPE = data.CLC_TYPE;
                    _ClearLineCrude.CLC_CREATE_DATE = data.CLC_CREATE_DATE;
                    _ClearLineCrude.CLC_CREATE_BY = data.CLC_CREATE_BY;
                    _ClearLineCrude.CLC_LAST_MODIFY_DATE = data.CLC_LAST_MODIFY_DATE;
                    _ClearLineCrude.CLC_LAST_MODIFY_BY = data.CLC_LAST_MODIFY_BY;
                    _ClearLineCrude.CLC_STATUS = data.CLC_STATUS;
                    _ClearLineCrude.CLC_CREATE_TIME = data.CLC_CREATE_TIME;
                    _ClearLineCrude.CLC_LAST_MODIFY_TIME = data.CLC_LAST_MODIFY_TIME;
                    _ClearLineCrude.CLC_RELEASE = data.CLC_RELEASE;
                    _ClearLineCrude.CLC_TANK = data.CLC_TANK;
                    _ClearLineCrude.CLC_SALEORDER = data.CLC_SALEORDER;
                    _ClearLineCrude.CLC_PRICE_RELEASE = data.CLC_PRICE_RELEASE;
                    _ClearLineCrude.CLC_REMARK = data.CLC_REMARK;
                    _ClearLineCrude.CLC_DISTRI_CHANN = data.CLC_DISTRI_CHANN;
                    _ClearLineCrude.CLC_TABLE_NAME = data.CLC_TABLE_NAME;
                    _ClearLineCrude.CLC_TOTAL = data.CLC_TOTAL;
                    _ClearLineCrude.CLC_MEMO = data.CLC_MEMO;
                    _ClearLineCrude.CLC_DATE_SAP = data.CLC_DATE_SAP;
                    _ClearLineCrude.CLC_FI_DOC = data.CLC_FI_DOC;
                    _ClearLineCrude.CLC_DO_NO = data.CLC_DO_NO;
                    _ClearLineCrude.CLC_STATUS_TO_SAP = data.CLC_STATUS_TO_SAP;
                    _ClearLineCrude.CLC_VESSEL_ID = data.CLC_VESSEL_ID;
                    _ClearLineCrude.CLC_UNIT_TOTAL = data.CLC_UNIT_TOTAL;
                    _ClearLineCrude.CLC_FI_DOC_REVERSE = data.CLC_FI_DOC_REVERSE;
                    _ClearLineCrude.CLC_INCOTERM_TYPE = data.CLC_INCOTERM_TYPE;
                    _ClearLineCrude.CLC_TEMP = data.CLC_TEMP;
                    _ClearLineCrude.CLC_DENSITY = data.CLC_DENSITY;
                    _ClearLineCrude.CLC_STORAGE_LOCATION = data.CLC_STORAGE_LOCATION;
                    _ClearLineCrude.CLC_UPDATE_DO = data.CLC_UPDATE_DO;
                    _ClearLineCrude.CLC_UPDATE_GI = data.CLC_UPDATE_GI;
                    _ClearLineCrude.CLC_INVOICE_FIGURE = data.CLC_INVOICE_FIGURE;
                    _ClearLineCrude.CLC_SALE_UNIT = data.CLC_SALE_UNIT;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveClearLinePrice(CLEAR_LINE_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.CLEAR_LINE_PRICE.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveClearLinePrice(CLEAR_LINE_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                context.CLEAR_LINE_PRICE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateClearLinePrice(CLEAR_LINE_PRICE data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _ClearLinePrice = context.CLEAR_LINE_PRICE.Where(x=>x.CLP_TRIP_NO.ToUpper().Equals(data.CLP_TRIP_NO.ToUpper())                                                                                                                                            
                                                                      && x.CLP_NUM.Equals(data.CLP_NUM)).ToList().FirstOrDefault();
                    if (_ClearLinePrice != null)
                    {
                        _ClearLinePrice.CLP_TRIP_NO = data.CLP_TRIP_NO;
                        _ClearLinePrice.CLP_PRICE = data.CLP_PRICE;
                        _ClearLinePrice.CLP_DATE_PRICE = data.CLP_DATE_PRICE;
                        _ClearLinePrice.CLP_NUM = data.CLP_NUM;
                        _ClearLinePrice.CLP_UNIT_ID = data.CLP_UNIT_ID;
                        _ClearLinePrice.CLP_RELEASE = data.CLP_RELEASE;
                        _ClearLinePrice.CLP_INVOICE = data.CLP_INVOICE;
                        _ClearLinePrice.CLP_MEMO = data.CLP_MEMO;
                        _ClearLinePrice.CLP_UNIT_INV = data.CLP_UNIT_INV;

                        context.SaveChanges();

                    }
                    else
                    {
                        context.CLEAR_LINE_PRICE.Add(data);
                        context.SaveChanges();
                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateClearLinePrice(CLEAR_LINE_PRICE data, EntityCPAIEngine context)
        {
            try
            {
                var _ClearLinePrice = context.CLEAR_LINE_PRICE.Where(x => x.CLP_TRIP_NO.ToUpper().Equals(data.CLP_TRIP_NO.ToUpper())
                                                                      && x.CLP_NUM.Equals(data.CLP_NUM)).ToList().FirstOrDefault();
                if (_ClearLinePrice != null)
                {
                    _ClearLinePrice.CLP_TRIP_NO = data.CLP_TRIP_NO;
                    _ClearLinePrice.CLP_PRICE = data.CLP_PRICE;
                    _ClearLinePrice.CLP_DATE_PRICE = data.CLP_DATE_PRICE;
                    _ClearLinePrice.CLP_NUM = data.CLP_NUM;
                    _ClearLinePrice.CLP_UNIT_ID = data.CLP_UNIT_ID;
                    _ClearLinePrice.CLP_RELEASE = data.CLP_RELEASE;
                    _ClearLinePrice.CLP_INVOICE = data.CLP_INVOICE;
                    _ClearLinePrice.CLP_MEMO = data.CLP_MEMO;
                    _ClearLinePrice.CLP_UNIT_INV = data.CLP_UNIT_INV;

                    context.SaveChanges();
                }
                else
                {
                    context.CLEAR_LINE_PRICE.Add(data);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
