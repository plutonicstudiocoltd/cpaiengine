﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectCPAIEngine.DAL.Entity;
using System.Data.Objects.SqlClient;

namespace ProjectCPAIEngine.DAL.DALPCF
{
    public class CUSTOMS_VAT_DAL
    {
        public PCF_CUSTOMS_VAT GetCustomVatsByTripAndMatNo(string pTripNo, decimal pMatItemNo) {
            PCF_CUSTOMS_VAT rslt = null;
            try {
                using (var context = new EntityCPAIEngine())
                {
                    rslt = (PCF_CUSTOMS_VAT)(from cv in context.PCF_CUSTOMS_VAT where cv.TRIP_NO == pTripNo && cv.MAT_ITEM_NO == pMatItemNo select cv).ToList()[0];
                }
            }
            catch (Exception ex){
                throw ex;
            }
            return rslt;
        }

        public void Save(PCF_CUSTOMS_VAT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {

                    context.PCF_CUSTOMS_VAT.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(PCF_CUSTOMS_VAT data, EntityCPAIEngine context)
        {
            try
            {
                context.PCF_CUSTOMS_VAT.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSapStatus(PCF_CUSTOMS_VAT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _CustomVat = context.PCF_CUSTOMS_VAT.Find(data.TRIP_NO, data.MAT_ITEM_NO);
                    if (_CustomVat != null)
                    {
                        _CustomVat.TRIP_NO = data.TRIP_NO;
                        _CustomVat.MAT_ITEM_NO = data.MAT_ITEM_NO;
                        _CustomVat.CUSTOMS_PRICE = data.CUSTOMS_PRICE;
                        _CustomVat.FREIGHT_AMOUNT = data.FREIGHT_AMOUNT;
                        _CustomVat.INSURANCE_RATE = data.INSURANCE_RATE;
                        _CustomVat.ROE = data.ROE;
                        _CustomVat.FOB = data.FOB;
                        _CustomVat.FRT = data.FRT;
                        _CustomVat.CFR = data.CFR;
                        _CustomVat.INS = data.INS;
                        _CustomVat.CIF_SUM_INS = data.CIF_SUM_INS;
                        _CustomVat.PREMIUM = data.PREMIUM;
                        _CustomVat.TOTAL_USD_100 = data.TOTAL_USD_100;
                        _CustomVat.BAHT_100 = data.BAHT_100;
                        _CustomVat.VAT = data.VAT;
                        _CustomVat.TAX_BASE = data.TAX_BASE;
                        _CustomVat.CORRECT_VAT = data.CORRECT_VAT;
                        _CustomVat.IMPORT_DUTY = data.IMPORT_DUTY;
                        _CustomVat.EXCISE_TAX = data.EXCISE_TAX;
                        _CustomVat.MUNICIPAL_TAX = data.MUNICIPAL_TAX;
                        _CustomVat.OIL_FUEL_FUND = data.OIL_FUEL_FUND;
                        _CustomVat.ENERY_OIL_FUEL_FUND = data.ENERY_OIL_FUEL_FUND;
                        _CustomVat.DEPOSIT_OF_EXCISE_TAX = data.DEPOSIT_OF_EXCISE_TAX;
                        _CustomVat.DEPOSIT_OF_MUNICIPAL_TAX = data.DEPOSIT_OF_MUNICIPAL_TAX;
                        _CustomVat.DEPOSIT_OF_IMPORT_DUTY = data.DEPOSIT_OF_IMPORT_DUTY;
                        _CustomVat.CAL_VOLUME_UNIT = data.CAL_VOLUME_UNIT;
                        _CustomVat.DOCUMENT_DATE = data.DOCUMENT_DATE;
                        _CustomVat.POSTING_DATE = data.POSTING_DATE;
                        _CustomVat.SAP_FI_DOC = data.SAP_FI_DOC;
                        _CustomVat.SAP_FI_DOC_STATUS = data.SAP_FI_DOC_STATUS;
                        _CustomVat.SAP_FI_DOC_ERROR = data.SAP_FI_DOC_ERROR;
                        //_CustomVat.SAP_INVOICE_DOC = data.SAP_INVOICE_DOC;
                        //_CustomVat.SAP_INVOICE_DOC_STATUS = data.SAP_INVOICE_DOC_STATUS;
                        //_CustomVat.SAP_INVOICE_DOC_ERROR = data.SAP_INVOICE_DOC_ERROR;
                        //_CustomVat.SAP_FI_DOC_INVOICE = data.SAP_FI_DOC_INVOICE;
                        //_CustomVat.SAP_FI_DOC_INVOICE_STATUS = data.SAP_FI_DOC_INVOICE_STATUS;
                        //_CustomVat.SAP_FI_DOC_INVOICE_ERROR = data.SAP_FI_DOC_INVOICE_ERROR;
                        _CustomVat.BAHT_105 = data.BAHT_105;

                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PCF_CUSTOMS_VAT data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _CustomVat = context.PCF_CUSTOMS_VAT.Find(data.TRIP_NO,data.MAT_ITEM_NO);
                    if(_CustomVat != null)
                    {
                        _CustomVat.TRIP_NO = data.TRIP_NO;
                        _CustomVat.MAT_ITEM_NO = data.MAT_ITEM_NO;
                        _CustomVat.CUSTOMS_PRICE = data.CUSTOMS_PRICE;
                        _CustomVat.FREIGHT_AMOUNT = data.FREIGHT_AMOUNT;
                        _CustomVat.INSURANCE_RATE = data.INSURANCE_RATE;
                        _CustomVat.ROE = data.ROE;
                        _CustomVat.FOB = data.FOB;
                        _CustomVat.FRT = data.FRT;
                        _CustomVat.CFR = data.CFR;
                        _CustomVat.INS = data.INS;
                        _CustomVat.CIF_SUM_INS = data.CIF_SUM_INS;
                        _CustomVat.PREMIUM = data.PREMIUM;
                        _CustomVat.TOTAL_USD_100 = data.TOTAL_USD_100;
                        _CustomVat.BAHT_100 = data.BAHT_100;
                        _CustomVat.VAT = data.VAT;
                        _CustomVat.TAX_BASE = data.TAX_BASE;
                        _CustomVat.CORRECT_VAT = data.CORRECT_VAT;
                        _CustomVat.IMPORT_DUTY = data.IMPORT_DUTY;
                        _CustomVat.EXCISE_TAX = data.EXCISE_TAX;
                        _CustomVat.MUNICIPAL_TAX = data.MUNICIPAL_TAX;
                        _CustomVat.OIL_FUEL_FUND = data.OIL_FUEL_FUND;
                        _CustomVat.ENERY_OIL_FUEL_FUND = data.ENERY_OIL_FUEL_FUND;
                        _CustomVat.DEPOSIT_OF_EXCISE_TAX = data.DEPOSIT_OF_EXCISE_TAX;
                        _CustomVat.DEPOSIT_OF_MUNICIPAL_TAX = data.DEPOSIT_OF_MUNICIPAL_TAX;
                        _CustomVat.DEPOSIT_OF_IMPORT_DUTY = data.DEPOSIT_OF_IMPORT_DUTY;
                        _CustomVat.CAL_VOLUME_UNIT = data.CAL_VOLUME_UNIT;
                        _CustomVat.DOCUMENT_DATE = data.DOCUMENT_DATE;
                        _CustomVat.POSTING_DATE = data.POSTING_DATE;
                        _CustomVat.SAP_FI_DOC = data.SAP_FI_DOC;
                        _CustomVat.SAP_FI_DOC_STATUS = data.SAP_FI_DOC_STATUS;
                        _CustomVat.SAP_FI_DOC_ERROR = data.SAP_FI_DOC_ERROR;
                        _CustomVat.SAP_INVOICE_DOC = data.SAP_INVOICE_DOC;
                        _CustomVat.SAP_INVOICE_DOC_STATUS = data.SAP_INVOICE_DOC_STATUS;
                        _CustomVat.SAP_INVOICE_DOC_ERROR = data.SAP_INVOICE_DOC_ERROR;
                        _CustomVat.SAP_FI_DOC_INVOICE = data.SAP_FI_DOC_INVOICE;
                        _CustomVat.SAP_FI_DOC_INVOICE_STATUS = data.SAP_FI_DOC_INVOICE_STATUS;
                        _CustomVat.SAP_FI_DOC_INVOICE_ERROR = data.SAP_FI_DOC_INVOICE_ERROR;
                        _CustomVat.BAHT_105 = data.BAHT_105;

                        context.SaveChanges();

                    }
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Update(PCF_CUSTOMS_VAT data, EntityCPAIEngine context)
        {
            try
            {
                var _CustomVat = context.PCF_CUSTOMS_VAT.Find(data.TRIP_NO, data.MAT_ITEM_NO);
                if (_CustomVat != null)
                {
                    _CustomVat.TRIP_NO = data.TRIP_NO;
                    _CustomVat.MAT_ITEM_NO = data.MAT_ITEM_NO;
                    _CustomVat.CUSTOMS_PRICE = data.CUSTOMS_PRICE;
                    _CustomVat.FREIGHT_AMOUNT = data.FREIGHT_AMOUNT;
                    _CustomVat.INSURANCE_RATE = data.INSURANCE_RATE;
                    _CustomVat.ROE = data.ROE;
                    _CustomVat.FOB = data.FOB;
                    _CustomVat.FRT = data.FRT;
                    _CustomVat.CFR = data.CFR;
                    _CustomVat.INS = data.INS;
                    _CustomVat.CIF_SUM_INS = data.CIF_SUM_INS;
                    _CustomVat.PREMIUM = data.PREMIUM;
                    _CustomVat.TOTAL_USD_100 = data.TOTAL_USD_100;
                    _CustomVat.BAHT_100 = data.BAHT_100;
                    _CustomVat.VAT = data.VAT;
                    _CustomVat.TAX_BASE = data.TAX_BASE;
                    _CustomVat.CORRECT_VAT = data.CORRECT_VAT;
                    _CustomVat.IMPORT_DUTY = data.IMPORT_DUTY;
                    _CustomVat.EXCISE_TAX = data.EXCISE_TAX;
                    _CustomVat.MUNICIPAL_TAX = data.MUNICIPAL_TAX;
                    _CustomVat.OIL_FUEL_FUND = data.OIL_FUEL_FUND;
                    _CustomVat.ENERY_OIL_FUEL_FUND = data.ENERY_OIL_FUEL_FUND;
                    _CustomVat.DEPOSIT_OF_EXCISE_TAX = data.DEPOSIT_OF_EXCISE_TAX;
                    _CustomVat.DEPOSIT_OF_MUNICIPAL_TAX = data.DEPOSIT_OF_MUNICIPAL_TAX;
                    _CustomVat.DEPOSIT_OF_IMPORT_DUTY = data.DEPOSIT_OF_IMPORT_DUTY;
                    _CustomVat.CAL_VOLUME_UNIT = data.CAL_VOLUME_UNIT;
                    _CustomVat.DOCUMENT_DATE = data.DOCUMENT_DATE;
                    _CustomVat.POSTING_DATE = data.POSTING_DATE;
                    _CustomVat.SAP_FI_DOC = data.SAP_FI_DOC;
                    _CustomVat.SAP_FI_DOC_STATUS = data.SAP_FI_DOC_STATUS;
                    _CustomVat.SAP_FI_DOC_ERROR = data.SAP_FI_DOC_ERROR;
                    _CustomVat.SAP_INVOICE_DOC = data.SAP_INVOICE_DOC;
                    _CustomVat.SAP_INVOICE_DOC_STATUS = data.SAP_INVOICE_DOC_STATUS;
                    _CustomVat.SAP_INVOICE_DOC_ERROR = data.SAP_INVOICE_DOC_ERROR;
                    _CustomVat.SAP_FI_DOC_INVOICE = data.SAP_FI_DOC_INVOICE;
                    _CustomVat.SAP_FI_DOC_INVOICE_STATUS = data.SAP_FI_DOC_INVOICE_STATUS;
                    _CustomVat.SAP_FI_DOC_INVOICE_ERROR = data.SAP_FI_DOC_INVOICE_ERROR;
                    _CustomVat.BAHT_105 = data.BAHT_105;

                    context.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string sapDoc)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _CustomVat = context.PCF_CUSTOMS_VAT.Where(x => x.SAP_FI_DOC.Trim().ToUpper() == sapDoc.Trim().ToUpper()).FirstOrDefault();
                    if (_CustomVat != null)
                    {                        
                        _CustomVat.SAP_FI_DOC = sapDoc.Trim();                        
                        context.SaveChanges();

                    }
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string sapDoc, EntityCPAIEngine context)
        {
            try
            {
                var _CustomVat = context.PCF_CUSTOMS_VAT.Where(x => x.SAP_FI_DOC.Trim().ToUpper() == sapDoc.Trim().ToUpper()).FirstOrDefault();
                if (_CustomVat != null)
                {                    
                    _CustomVat.SAP_FI_DOC = string.Empty;
                    _CustomVat.SAP_FI_DOC_STATUS = string.Empty;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
