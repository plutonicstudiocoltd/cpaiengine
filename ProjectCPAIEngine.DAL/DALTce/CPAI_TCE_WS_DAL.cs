﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALTce
{
    public class CPAI_TCE_WS_DAL
    {
        public static CPAI_TCE_WS GetTCE(string transactionID)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_WS 
                            where v.CTW_ROW_ID == transactionID
                            select v).Distinct();
                return query.ToList().FirstOrDefault();
            }
        }

        public static CPAI_TCE_WS getTceTxn(string charterIn, string status)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_WS
                             where v.CTW_FK_CHI_DATA == charterIn && v.CTW_STATUS == status
                             select v).Distinct();
                return query.ToList().FirstOrDefault();
            }
        }

        public static bool isDuplicate(string charterIn,string status)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_WS
                             where v.CTW_FK_CHI_DATA == charterIn && v.CTW_STATUS == status 
                             select v).Distinct();
                if (query.Count() > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }

        public  CPAI_TCE_WS GetTCEByCharterIn(string charterIn)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.CPAI_TCE_WS
                             where v.CTW_FK_CHI_DATA == charterIn 
                             select v).Distinct();
                return query.ToList().FirstOrDefault();
            }
        }

        public void Save(CPAI_TCE_WS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_TCE_WS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_TCE_WS data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_TCE_WS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_TCE_WS data, EntityCPAIEngine context)
        {
            try
            {
                
                    var _search = context.CPAI_TCE_WS.Find(data.CTW_ROW_ID);
                    #region Set Value
                    _search.CTW_REASON = data.CTW_REASON;
                    _search.CTW_STATUS = data.CTW_STATUS;
                    _search.CTW_UPDATED_BY = data.CTW_UPDATED_BY;
                    _search.CTW_UPDATED_DATE = data.CTW_UPDATED_DATE;
                    #endregion
                    context.SaveChanges();
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_TCE_WS WHERE CTW_ROW_ID = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_TCE_WS WHERE CTW_ROW_ID = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Calculate

        // Calculate Date
        public List<DateTime> CalTargetDate(DateTime LaycanLoadFrom, string HolidayType)
        {
            DateTime DateBack20 = CalTargetDate(LaycanLoadFrom);
            List<DateTime> rtnDate = new List<DateTime>();

            DateTime DateTemp = new DateTime();
           

            if(isHoliday(DateBack20, HolidayType) == false)
            {
                
                DateTemp = DateBack20;

                rtnDate.Add(DateTemp);
                DateTemp = DateTemp.AddDays(-1);

                int intBack1Day = 1;
                do
                {
                    // Date Back 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intBack1Day -= 1;
                    }

                    DateTemp = DateTemp.AddDays(-1);

                } while (intBack1Day > 0);

                DateTemp = DateBack20.AddDays(1);
                int intForword1Day = 1;
                do
                {
                    // Date Forword 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intForword1Day -= 1;
                    }
                    DateTemp = DateTemp.AddDays(1);

                } while (intForword1Day > 0);

                // Forword 3 Day
                //DateTemp = DateBack20;
                //int intForwoard3Day = 3;
                //do
                //{
                //    // Date Back 3 Day
                //    if (isHoliday(DateTemp, HolidayType) == false)
                //    {
                //       rtnDate.Add(DateTemp);
                //        intForwoard3Day -= 1;
                //    }

                //    DateTemp = DateTemp.AddDays(1);

                //} while (intForwoard3Day > 0);
            }
            else
            {
                DateTemp = DateBack20;
                int intBack2Day = 2;
                do
                {
                    // Date Back 2 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intBack2Day -= 1;
                       
                    }
                   
                    DateTemp = DateTemp.AddDays(-1);
                   
                } while (intBack2Day > 0);

                DateTemp = DateBack20;
                int intForword1Day = 1;
                do
                {
                    // Date Forword 1 Day
                    if (isHoliday(DateTemp, HolidayType) == false)
                    {
                        rtnDate.Add(DateTemp);
                        intForword1Day -= 1;
                    }
                    DateTemp = DateTemp.AddDays(1);

                } while (intForword1Day > 0);
            }

            return rtnDate;
        }

        // Check is Holiday
        public bool isHoliday(DateTime checkDate, string HolidayType)
        {
          

            if(isHolidayDB(checkDate, HolidayType) == true)
            {
                return true;    // is holiday
            }
            else if (checkDate.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;    // is holiday
            }
            else if (checkDate.DayOfWeek == DayOfWeek.Saturday)
            {
                return true;    // is holiday
            }
            else if (isTD_NA(checkDate) == true)
            {
                return true;    // is TD NA
            }
            else
            {
                // Not Holiday
                return false;
            }

        }

        // Check is Holiday in DB
        public bool isHolidayDB(DateTime checkDate, string HolidayType)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                var holiday = entity.MT_HOLIDAY.Where(x => x.MH_HOL_TYPE.ToUpper().Equals(HolidayType.ToUpper()) && DbFunctions.TruncateTime(x.MH_HOL_DATE.Value) == DbFunctions.TruncateTime(checkDate));
                if (holiday.ToList().Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        // Check is Holiday in DB
        public bool isTD_NA(DateTime checkDate)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                var td = entity.MT_CRUDE_ROUTES.Where(x => DbFunctions.TruncateTime(x.MCR_DATE) == DbFunctions.TruncateTime(checkDate) && x.MCR_TD2.Equals("NA") && x.MCR_TD3.Equals("NA") && x.MCR_TD15.Equals("NA") && x.MCR_STATUS.Equals("ACTIVE"));
                if (td.ToList().Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public DateTime CalTargetDate(DateTime LaycanLoadFrom)
        {
            DateTime TargetDate = LaycanLoadFrom.AddDays(-20);
           

            return TargetDate;
        }

        public KeyValuePair<double?,string> CalEverAgeWS(DateTime LaycanFromDate, string TD, string HolidayType)
        {
            string rtnMessage = "Can not calculate because the {0} value was not found on {1}.";

            MT_CRUDE_ROUTES CR = new MT_CRUDE_ROUTES();
            double? iSum = 0;
            string mesg = "";
            List<MT_CRUDE_ROUTES> list;
            
            List<DateTime> ListDateTD = new List<DateTime>();

            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {

                ListDateTD = CalTargetDate(LaycanFromDate, HolidayType);

                foreach (DateTime dtTD in ListDateTD)
                {
                    string dateTD = dtTD.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    list = entity.MT_CRUDE_ROUTES.Where(x => DbFunctions.TruncateTime(dtTD) == DbFunctions.TruncateTime(x.MCR_DATE) && x.MCR_STATUS == "ACTIVE").ToList();

                    if(list.Count == 0)
                    {
                        mesg = String.Format("Crude route not found on {0}.", dateTD);
                        iSum = null;
                        break;
                    }
                    foreach (var item in list)
                    {
                        if (TD.Equals("TD2"))
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD2) || Convert.ToString(item.MCR_TD2) == "NA")
                            {
                                mesg = String.Format(rtnMessage, "TD2", dateTD);
                                iSum = null;
                                break;
                            }
                               
                            iSum += Convert.ToDouble(item.MCR_TD2);

                        }

                        else if (TD.Equals("TD3"))
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD3) || Convert.ToString(item.MCR_TD3) == "NA")
                            {
                                mesg = String.Format(rtnMessage, "TD3", dateTD);
                                iSum = null;
                                break;
                            }

                            iSum += Convert.ToDouble(item.MCR_TD3);

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(item.MCR_TD15) || Convert.ToString(item.MCR_TD15) == "NA")
                            {
                                mesg = String.Format(rtnMessage, "TD15", dateTD);
                                iSum = null;
                                break;
                            }

                            iSum += Convert.ToDouble(item.MCR_TD15);
                        }

                    }
                }
            }

            if(iSum != null)
                iSum = Math.Round(Convert.ToDouble(iSum) / 3, 2);

            return new KeyValuePair<double?, string>(iSum, mesg);
        }

        public CPAI_TCE_WS GetByID(string CTW_ROW_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_TCE_WS.Where(x => x.CTW_ROW_ID.ToUpper() == CTW_ROW_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CPAI_TCE_WS GetByCharterinID(string CHI_DATA_ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_TCE_WS.Where(x => x.CTW_FK_CHI_DATA.ToUpper() == CHI_DATA_ID.ToUpper()).FirstOrDefault();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DateTime GetLastFixtureDate()
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = (from p in context.CPAI_TCE_WS.AsEnumerable()
                                 where p.CTW_STATUS == "SUBMIT"
                                 select new { fixDate = DateTime.ParseExact(p.CTW_FIXTURE_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) }).ToList();
                    //foreach(var item in _Data)
                    //{
                    //    var d = item.fixDate;
                    //}
                    if(_Data.Count > 0)
                    {
                        return _Data.Max(p => p.fixDate);
                    }
                    else
                    {
                        return DateTime.Now;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public string Calculate()
        //{
        //    DateTime dt = new DateTime(2016, 06, 23);//input laycan_load_from
        //    DateTime dtTarget = new DateTime();
        //    double dEverage = 0;
        //    double dWSSaving = 0;
        //    double dFixture = 0;//input
        //    double dSaving = 0;
        //    double dMinLoad = 270;
        //    double dFlatRate = 13.58;

        //    dtTarget = CalTargetDate(dt);

        //    dEverage = CalEverageWS(dtTarget, "TD2");

        //    dWSSaving = dEverage - dFixture;

        //    dSaving = dWSSaving * dMinLoad * 10 * dFlatRate;

        //    return "";
        //}

        //public DateTime CalTargetDate(DateTime LaycanLoadFrom)
        //{
        //    DateTime TargetDate = LaycanLoadFrom.AddDays(-20);
        //    if (TargetDate.DayOfWeek == DayOfWeek.Sunday)
        //    {
        //        TargetDate = TargetDate.AddDays(-2);
        //    }
        //    else if (TargetDate.DayOfWeek == DayOfWeek.Saturday)
        //    {
        //        TargetDate = TargetDate.AddDays(-1);
        //    }

        //    return TargetDate;
        //}

        //public double CalEverageWS(DateTime TargetDate, string TD)
        //{
        //    MT_CRUDE_ROUTES CR = new MT_CRUDE_ROUTES();
        //    double iSum = 0;
        //    List<MT_CRUDE_ROUTES> list;
        //    DateTime tmpDate = new DateTime();
        //    tmpDate = TargetDate;
        //    using (EntityCPAIEngine entity = new EntityCPAIEngine())
        //    {
        //        for (int i = 1; i < 4; i++)
        //        {

        //            list = entity.MT_CRUDE_ROUTES.Where(x => DbFunctions.TruncateTime(tmpDate) == DbFunctions.TruncateTime(x.MCR_DATE)).ToList();

        //            if (TD.Equals("TD2"))
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD2);
        //                }
        //            }
        //            else if (TD.Equals("TD3"))
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD3);
        //                }
        //            }
        //            else
        //            {
        //                foreach (var item in list)
        //                {
        //                    iSum += Convert.ToDouble(item.MCR_TD15);
        //                }
        //            }
        //            tmpDate = TargetDate.AddDays(-i);

        //        }
        //    }

        //    iSum = Math.Round(iSum / 3, 2);

        //    return iSum;
        //}
        #endregion
    }
}
