﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ProjectCPAIEngine.DAL.DALVCOOL
{
    public class VCO_CRUDE_COMPARISON_DAL
    {

        public void Save(VCO_CRUDE_COMPARISON data, EntityCPAIEngine context)
        {
            try
            {
                context.VCO_CRUDE_COMPARISON.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string VCO_DATA, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM VCO_CRUDE_COMPARISON WHERE VCCP_FK_VCO_DATA = '" + VCO_DATA + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<VCO_CRUDE_COMPARISON> GetByDataID(string ID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.VCO_CRUDE_COMPARISON.Where(x => x.VCCP_FK_VCO_DATA.ToUpper() == ID.ToUpper()).ToList();
                    return _Data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
