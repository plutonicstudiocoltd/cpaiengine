﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVCOOL
{
    public class VCO_ETA_DATE_HISTORY_DAL
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();

        public void Save(VCO_ETA_DATE_HISTORY vcoData, EntityCPAIEngine context)
        {
            try
            {
                context.VCO_ETA_DATE_HISTORY.Add(vcoData);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<VCO_ETA_DATE_HISTORY> GetAllByID(string VCDA_ROW_ID)
        {
            try
            {                                 
                var _Data = context.VCO_ETA_DATE_HISTORY.Where(x => x.VCDH_FK_VCO_DATA.ToUpper() == VCDA_ROW_ID.ToUpper()).ToList();
                return _Data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
         
        }



        public void DeleteByID(string VCDA_ROW_ID)
        {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Database.ExecuteSqlCommand("DELETE FROM VCO_ETA_DATE_HISTORY WHERE VCDA_ROW_ID = '" + VCDA_ROW_ID + "'");
                        context.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        string res = ex.Message;
                        dbContextTransaction.Rollback();
                    }
                };
            }
       }
   
}
