﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_ATTACH_DAL
    {
        public List<CPAI_VESSEL_SCHEDULE_ATTACH> GetFileByDataID(string ACTID, string TYPE)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _actFile = context.CPAI_VESSEL_SCHEDULE_ATTACH.Where(x => x.VSAF_FK_VESSEL_SCHEDULE_ACT.ToUpper() == ACTID.ToUpper()).ToList();
                    if (string.IsNullOrEmpty(TYPE) != true)
                    {
                        _actFile = _actFile.Where(x => x.VSAF_TYPE.ToUpper() == TYPE.ToUpper()).ToList();
                    }
                    return _actFile;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCHEDULE_ATTACH data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_ATTACH.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
