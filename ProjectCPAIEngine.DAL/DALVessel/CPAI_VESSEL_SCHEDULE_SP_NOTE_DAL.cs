﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCHEDULE_SP_NOTE_DAL
    {
        public void Save(CPAI_VESSEL_SCHEDULE_SP_NOTE data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCHEDULE_SP_NOTE.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateAllFlags(string month_year, EntityCPAIEngine context)
        {
            try
            {

                var _search = context.CPAI_VESSEL_SCHEDULE_SP_NOTE.Where(x => x.VSSP_MONTH_YEAR == month_year).ToList();
                #region Set Value
                _search.ForEach(x => x.VSSP_FLAG = "Y");
                #endregion
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CPAI_VESSEL_SCHEDULE_SP_NOTE GetLastest(string month_year)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _Data = context.CPAI_VESSEL_SCHEDULE_SP_NOTE.Where(x => x.VSSP_FLAG == "N" && x.VSSP_MONTH_YEAR == month_year).FirstOrDefault();
                    if (_Data != null)
                    {
                        return _Data;
                    } else
                    {
                        return new CPAI_VESSEL_SCHEDULE_SP_NOTE();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
