﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCH_S_ACTIVITYS_DAL
    {
        public void Save(CPAI_VESSEL_SCH_S_ACTIVITYS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCH_S_ACTIVITYS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCH_S_ACTIVITYS data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCH_S_ACTIVITYS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CPAI_VESSEL_SCH_S_ACTIVITYS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _search = context.CPAI_VESSEL_SCH_S_ACTIVITYS.Find(data.VASS_ROW_ID);
                    #region Set Value
                    _search.VASS_UPDATED = data.VASS_UPDATED;
                    _search.VASS_UPDATED_BY = data.VASS_UPDATED_BY;
                    #endregion
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCH_S_ACTIVITYS WHERE VASS_FK_VESSEL_SCH_S = '" + TransID + "'");
                            context.SaveChanges();
                            dbContextTransaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                        }
                    };
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string TransID, EntityCPAIEngine context)
        {
            try
            {
                context.Database.ExecuteSqlCommand("DELETE FROM CPAI_VESSEL_SCH_S_ACTIVITYS WHERE VASS_FK_VESSEL_SCH_S = '" + TransID + "'");
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCH_S_ACTIVITYS> GetActivityByDataID (string ACTID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _act = context.CPAI_VESSEL_SCH_S_ACTIVITYS.Where(x => x.VASS_FK_VESSEL_SCH_S.ToUpper() == ACTID.ToUpper()).ToList();
                    return _act;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
    }
}
