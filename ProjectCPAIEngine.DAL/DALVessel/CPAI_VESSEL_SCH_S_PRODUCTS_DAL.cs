﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.DALVessel
{
    public class CPAI_VESSEL_SCH_S_PRODUCTS_DAL
    {
        public void Save(CPAI_VESSEL_SCH_S_PRODUCTS data)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    context.CPAI_VESSEL_SCH_S_PRODUCTS.Add(data);
                    context.SaveChanges();
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Save(CPAI_VESSEL_SCH_S_PRODUCTS data, EntityCPAIEngine context)
        {
            try
            {
                context.CPAI_VESSEL_SCH_S_PRODUCTS.Add(data);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAI_VESSEL_SCH_S_PRODUCTS> GetProductByDataID(string SCHSID)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var _chosProduct = context.CPAI_VESSEL_SCH_S_PRODUCTS.Where(x => x.VSPS_FK_VESSEL_SCH_S_PORT.ToUpper() == SCHSID.ToUpper()).ToList();
                    return _chosProduct;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
