//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CHOT_A_OTHER_COSTS
    {
        public string OMAO_ROW_ID { get; set; }
        public string OMAO_FK_CHOT_DATA { get; set; }
        public string OMAO_ORDER { get; set; }
        public string OMAO_DETAIL { get; set; }
        public string OMAO_VALUE { get; set; }
        public System.DateTime OMAO_CREATED { get; set; }
        public string OMAO_CREATED_BY { get; set; }
        public System.DateTime OMAO_UPDATED { get; set; }
        public string OMAO_UPDATED_BY { get; set; }
    
        public virtual CHOT_DATA CHOT_DATA { get; set; }
    }
}
