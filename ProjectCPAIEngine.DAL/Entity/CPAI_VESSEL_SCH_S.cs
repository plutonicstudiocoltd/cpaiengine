//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class CPAI_VESSEL_SCH_S
    {
        public CPAI_VESSEL_SCH_S()
        {
            this.CPAI_VESSEL_SCH_S_PORT = new HashSet<CPAI_VESSEL_SCH_S_PORT>();
            this.CPAI_VESSEL_SCH_S_ACTIVITYS = new HashSet<CPAI_VESSEL_SCH_S_ACTIVITYS>();
        }
    
        public string VSDS_ROW_ID { get; set; }
        public string VSDS_FK_VEHICLE { get; set; }
        public Nullable<System.DateTime> VSDS_DATE_START { get; set; }
        public Nullable<System.DateTime> VSDS_DATE_END { get; set; }
        public string VSDS_TC { get; set; }
        public string VSDS_NOTE { get; set; }
        public string VSDS_STATUS { get; set; }
        public System.DateTime VSDS_CREATED_DATE { get; set; }
        public string VSDS_CREATED_BY { get; set; }
        public System.DateTime VSDS_UPDATED_DATE { get; set; }
        public string VSDS_UPDATED_BY { get; set; }
    
        public virtual ICollection<CPAI_VESSEL_SCH_S_PORT> CPAI_VESSEL_SCH_S_PORT { get; set; }
        public virtual MT_VEHICLE MT_VEHICLE { get; set; }
        public virtual ICollection<CPAI_VESSEL_SCH_S_ACTIVITYS> CPAI_VESSEL_SCH_S_ACTIVITYS { get; set; }
    }
}
