//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_ANNUAL_FW_HEDGE_TYPE
    {
        public HEDG_ANNUAL_FW_HEDGE_TYPE()
        {
            this.HEDG_ANNUAL_FW_DETAIL = new HashSet<HEDG_ANNUAL_FW_DETAIL>();
        }
    
        public string HAFH_ROW_ID { get; set; }
        public string HAFH_FK_HEDG_ANNUAL_FW { get; set; }
        public string HAFH_FK_HEDG_MT_HEDGE_TYPE { get; set; }
        public System.DateTime HAFH_CREATED_DATE { get; set; }
        public string HAFH_CREATED_BY { get; set; }
        public System.DateTime HAFH_UPDATED_DATE { get; set; }
        public string HAFH_UPDATED_BY { get; set; }
        public string HAFH_ORDER { get; set; }
        public string HAFH_HEDGE_TYPE_NAME { get; set; }
    
        public virtual HEDG_MT_HEDGE_TYPE HEDG_MT_HEDGE_TYPE { get; set; }
        public virtual HEDG_ANNUAL_FW HEDG_ANNUAL_FW { get; set; }
        public virtual ICollection<HEDG_ANNUAL_FW_DETAIL> HEDG_ANNUAL_FW_DETAIL { get; set; }
    }
}
