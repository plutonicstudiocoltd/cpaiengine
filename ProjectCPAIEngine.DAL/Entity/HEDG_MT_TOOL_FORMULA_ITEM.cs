//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_MT_TOOL_FORMULA_ITEM
    {
        public string HTFI_ROW_ID { get; set; }
        public string HTFI_FK_HEDG_MT_TOOL_FORMULA { get; set; }
        public string HTFI_ORDER { get; set; }
        public string HTFI_TYPE { get; set; }
        public string HTFI_VALUE { get; set; }
        public System.DateTime HTFI_CREATED_DATE { get; set; }
        public string HTFI_CREATED_BY { get; set; }
        public System.DateTime HTFI_UPDATED_DATE { get; set; }
        public string HTFI_UPDATED_BY { get; set; }
    
        public virtual HEDG_MT_TOOL_FORMULA HEDG_MT_TOOL_FORMULA { get; set; }
    }
}
