//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class HEDG_STR_CHOICE_FW
    {
        public string HSCF_ROW_ID { get; set; }
        public string HSCF_FK_HEDG_STR_CP { get; set; }
        public string HSCF_FK_HEDG_MT_PROD_FW { get; set; }
        public string HSCF_ORDER { get; set; }
        public System.DateTime HSCF_CREATED_DATE { get; set; }
        public string HSCF_CREATED_BY { get; set; }
        public System.DateTime HSCF_UPDATED_DATE { get; set; }
        public string HSCF_UPDATED_BY { get; set; }
    
        public virtual HEDG_MT_PROD_MKT_FW HEDG_MT_PROD_MKT_FW { get; set; }
        public virtual HEDG_STR_CHOICE_PRODUCT HEDG_STR_CHOICE_PRODUCT { get; set; }
    }
}
