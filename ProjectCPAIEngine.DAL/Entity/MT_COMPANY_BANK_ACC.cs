//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class MT_COMPANY_BANK_ACC
    {
        public string MBA_ROW_ID { get; set; }
        public string MBA_FK_COMPANY_CODE { get; set; }
        public string MBA_ORDER { get; set; }
        public string MBA_ACC_TYPE { get; set; }
        public string MBA_ACC_NAME { get; set; }
        public string MBA_ACC_NUMBER { get; set; }
        public string MBA_IS_DEFAULT_YAN { get; set; }
        public Nullable<System.DateTime> MBA_UPDATED_DATE { get; set; }
        public string MBA_UPDATED_BY { get; set; }
        public Nullable<System.DateTime> MBA_CREATED_DATE { get; set; }
        public string MBA_CREATED_BY { get; set; }
    
        public virtual MT_COMPANY MT_COMPANY { get; set; }
    }
}
