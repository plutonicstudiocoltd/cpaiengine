//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAFN_DATA
    {
        public PAFN_DATA()
        {
            this.PAFN_EVALUATION = new HashSet<PAFN_EVALUATION>();
        }
    
        public string PDT_ROW_ID { get; set; }
        public string PDT_REQ_BY { get; set; }
        public string PDT_FOR { get; set; }
        public string PDT_FK_MATERIALS { get; set; }
        public string PDT_FORM_NO { get; set; }
        public string PDT_FORM_FROM { get; set; }
        public string PDT_CURRENCY_UNIT { get; set; }
        public Nullable<System.DateTime> PDT_FORM_DATE { get; set; }
        public string PDT_FORM_TYPE { get; set; }
        public string PDT_FORM_TYPE_DETAIL { get; set; }
        public string PDT_FORM_METHOD { get; set; }
        public string PDT_SOURCE { get; set; }
        public string PDT_BRIEF { get; set; }
        public string PDT_NOTE { get; set; }
        public string PDT_REASON { get; set; }
        public string PDT_FORM { get; set; }
        public string PDT_STATUS { get; set; }
        public Nullable<System.DateTime> PDT_CREATED { get; set; }
        public string PDT_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PDT_UPDATED { get; set; }
        public string PDT_UPDATED_BY { get; set; }
    
        public virtual MT_COMPANY MT_COMPANY { get; set; }
        public virtual MT_MATERIALS MT_MATERIALS { get; set; }
        public virtual PAFN_MT PAFN_MT { get; set; }
        public virtual PAFN_MT PAFN_MT1 { get; set; }
        public virtual PAFN_MT PAFN_MT2 { get; set; }
        public virtual PAFN_MT PAFN_MT3 { get; set; }
        public virtual PAFN_MT PAFN_MT4 { get; set; }
        public virtual ICollection<PAFN_EVALUATION> PAFN_EVALUATION { get; set; }
    }
}
