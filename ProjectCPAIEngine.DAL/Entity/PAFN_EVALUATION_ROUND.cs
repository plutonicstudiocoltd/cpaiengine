//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAFN_EVALUATION_ROUND
    {
        public string PER_ROW_ID { get; set; }
        public string PER_FK_PAFN_EVALUATION_TIER { get; set; }
        public Nullable<decimal> PER_ROUND { get; set; }
        public string PER_FORMULA { get; set; }
        public Nullable<decimal> PER_PREMIUM { get; set; }
        public Nullable<decimal> PER_FIXED { get; set; }
        public Nullable<System.DateTime> PER_CREATED { get; set; }
        public string PER_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PER_UPDATED { get; set; }
        public string PER_UPDATED_BY { get; set; }
    
        public virtual PAFN_EVALUATION_TIER PAFN_EVALUATION_TIER { get; set; }
        public virtual PAFN_MT PAFN_MT { get; set; }
    }
}
