//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PAF_ROUND_ITEMS
    {
        public string PRI_ROW_ID { get; set; }
        public string PRI_FK_PAF_ROUND { get; set; }
        public string PRI_FLOAT { get; set; }
        public Nullable<decimal> PRI_FIXED { get; set; }
        public Nullable<System.DateTime> PRI_CREATED { get; set; }
        public string PRI_CREATED_BY { get; set; }
        public Nullable<System.DateTime> PRI_UPDATED { get; set; }
        public string PRI_UPDATED_BY { get; set; }
    
        public virtual PAF_ROUND PAF_ROUND { get; set; }
    }
}
