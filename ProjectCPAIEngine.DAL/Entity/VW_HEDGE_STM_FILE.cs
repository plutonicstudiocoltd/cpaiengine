//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectCPAIEngine.DAL.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_HEDGE_STM_FILE
    {
        public string HAF_ROW_ID { get; set; }
        public string HAF_FK_MT_COMPANY { get; set; }
        public string HAF_STATUS { get; set; }
        public Nullable<System.DateTime> HAF_APPROVED_DATE { get; set; }
        public string HAF_NOTE { get; set; }
        public string HAF_VERSION { get; set; }
        public System.DateTime HAF_CREATED_DATE { get; set; }
        public string HAF_CREATED_BY { get; set; }
        public System.DateTime HAF_UPDATED_DATE { get; set; }
        public string HAF_UPDATED_BY { get; set; }
        public string HAF_HISTORY_REF { get; set; }
        public string HAF_FK_MT_COMPANY_2 { get; set; }
        public string HAAF_ROW_ID { get; set; }
        public string HAAF_FK_HEDG_ANNUAL { get; set; }
        public string HAAF_TYPE { get; set; }
        public string HAAF_PATH { get; set; }
        public string HAAF_INFO { get; set; }
        public System.DateTime HAAF_CREATED { get; set; }
        public string HAAF_CREATED_BY { get; set; }
        public System.DateTime HAAF_UPDATED { get; set; }
        public string HAAF_UPDATED_BY { get; set; }
        public string HASN_ROW_ID { get; set; }
        public string HASN_FK_HEDG_ANNUAL { get; set; }
        public string HASN_SUBJECT { get; set; }
        public Nullable<System.DateTime> HASN_APPROVED_DATE { get; set; }
        public string HASN_REF_NO { get; set; }
        public string HASN_NOTE { get; set; }
        public System.DateTime HASN_CREATED_DATE { get; set; }
        public string HASN_CREATED_BY { get; set; }
        public System.DateTime HASN_UPDATED_DATE { get; set; }
        public string HASN_UPDATED_BY { get; set; }
        public string HASF_ROW_ID { get; set; }
        public string HASF_FK_HEDG_ANNUAL_SN { get; set; }
        public string HASF_TYPE { get; set; }
        public string HASF_PATH { get; set; }
        public string HASF_INFO { get; set; }
        public System.DateTime HASF_CREATED_DATE { get; set; }
        public string HASF_CREATED_BY { get; set; }
        public System.DateTime HASF_UPDATED_DATE { get; set; }
        public string HASF_UPDATED_BY { get; set; }
    }
}
