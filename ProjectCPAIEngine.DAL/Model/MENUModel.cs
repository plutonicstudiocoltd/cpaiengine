﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.Model
{
    public class MENUModel
    {
        public string MEU_ROW_ID { get; set; }
        public string MEU_GROUP_MENU { get; set; }
        public string MEU_PARENT_ID { get; set; }
        public string LNG_DESCRIPTION { get; set; }
        public string MEU_URL { get; set; }
        public string MEU_URL_DIRECT { get; set; }
        public string MEU_IMG { get; set; }
        public string MEU_LEVEL { get; set; }
        public string MEU_LIST_NO { get; set; }
        public string MEU_CONTROL_TYPE { get; set; }
    }
}
