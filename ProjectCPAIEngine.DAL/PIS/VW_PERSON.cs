﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.DAL.PIS
{
    public class VW_PERSON
    {

        public static List<VW_PERSONAL_ALL> GetAllUser()
        {
            using (pisEntities context = new pisEntities())
            {
                return context.VW_PERSONAL_ALL.ToList();
            }
        }

        public static List<VW_PERSONAL_ALL> GetUserbyName(string name)
        {
            using (pisEntities context = new pisEntities())
            {
                return context.VW_PERSONAL_ALL.Where(x => x.ENFIRSTNAME.Contains(name)).ToList();
            }
        }

        public static List<VW_PERSONAL_ALL> GetUserbyFullName(string fullname)
        {
            using (pisEntities context = new pisEntities())
            {
                return context.VW_PERSONAL_ALL.Where(x => x.ENName.Contains(fullname)).ToList();
            }
        }
    }
}
