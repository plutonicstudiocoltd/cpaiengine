﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.DAL.DALBunker;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Model;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.DAL.CPAI;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{

    public class CharterInController : BaseController
    {
        public List<ButtonAction> ButtonListCharterIn
        {
            get
            {
                if (Session["ButtonListCharterIn"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListCharterIn"];
            }
            set { Session["ButtonListCharterIn"] = value; }
        }
        ShareFn _FN = new ShareFn();
        public string MCCTypeSS
        {
            get
            {
                string MCC = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
                return MCC;
            }
            set
            {
                Session["Charter_Type"] = value;
            }
        }
        const string returnPage = "../web/MainBoards.aspx";


        public ActionResult Index()
        {
            CharterInViewModel model;

            if (TempData["CharterIn_Model"] == null)
            {
                model = new CharterInViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt(); //CMCS FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterIn(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }
                    //if (Request.QueryString["PURNO"] != null && Request.QueryString["isDup"] == null)
                    //{
                    //    ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
                    //}
                    if (Request.QueryString["Reason"] != null)
                    {
                        model.chi_reason = Request.QueryString["Reason"].ToString().Decrypt();
                    }

                    if (resData != null && !(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = returnPage;
                    }
                }
                else
                {
                    model.chi_proposed_for_approve = new ChiProposedForApprove();
                    model.chi_proposed_for_approve.charter_patry = "Shell Voy 5";
                    model.chi_proposed_for_approve.crude_oil_washing = "Marpol’s regulation";
                    model.chi_proposed_for_approve.laytime = "120 hrs";
                    model.chi_proposed_for_approve.exten_cost = "50/50 Laden passage between Thaioil and the owner but max 30 K USD to Thaioil";
                    model.chi_proposed_for_approve.laden_speed = "13 knots/hr";
                }
            }
            else
            {
                model = TempData["CharterIn_Model"] as CharterInViewModel;
                model.chi_reason = TempData["CharterIn_Reason"] as string;
                TempData["CharterIn_Model"] = null;
                TempData["CharterIn_Reason"] = null;
            }

            initializeDropDownList(ref model);

            return View(model);
        }

        public ActionResult CharterInCMMT()
        {
            CharterInViewModel model;
            if (TempData["CharterIn_Model"] == null)
            {
                model = new CharterInViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt(); //CMMT FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterIn(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }
                    //if (Request.QueryString["PURNO"] != null && Request.QueryString["isDup"] == null)
                    //{
                    //    ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
                    //}
                    if (Request.QueryString["Reason"] != null)
                    {
                        model.chit_reason = Request.QueryString["Reason"].ToString().Decrypt();
                    }

                    if (resData != null && !(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = "../" + returnPage;

                    }
                }
                else
                {
                    model.chit_proposed_for_approve = new ChitProposedForApprove();
                    //model.chit_proposed_for_approve.charter_patry = "Shell Voy 5";
                    //model.chit_proposed_for_approve.laytime = "120 hrs";
                    //model.chit_proposed_for_approve.exten_cost = "50/50 Laden passage between Thaioil and the owner but max 30 K USD to Thaioil";
                }
            }
            else
            {
                model = TempData["CharterIn_Model"] as CharterInViewModel;
                model.chit_reason = TempData["CharterIn_Reason"] as string;
                TempData["CharterIn_Model"] = null;
                TempData["CharterIn_Reason"] = null;
            }

            initializeDropDownList(ref model);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, CharterInViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            this.updateModel(form, model);

            ResponseData resData;
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE)
            {
                resData = SaveDataCharterIn(_status, model, note, json_uploadFile, attached_explain);
            }
            else
            {
                resData = SaveDataCharterInCMMT(_status, model, note, json_uploadFile, attached_explain);
            }

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    //string purno = resData.resp_parameters[0].v.Encrypt();
                    string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chi_reason.Encrypt() : model.chit_reason.Encrypt();
                    string path = string.Format("~/CPAIMVC/CharterIn?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                    if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterIn/CharterInCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                    return Redirect(path);
                }
            }

            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                return View("CharterInCMMT", model);
            }
            else
            {
                return View("Index", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, CharterInViewModel model)
        {
            string note = form["hdfNoteAction"];
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            string attached_explain = form["hdfExplanFileUpload"];
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            this.updateModel(form, model);

            ResponseData resData;
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE)
            {
                resData = SaveDataCharterIn(_status, model, note, json_uploadFile, attached_explain);
            }
            else
            {
                resData = SaveDataCharterInCMMT(_status, model, note, json_uploadFile, attached_explain);
            }

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    //string purno = resData.resp_parameters[0].v.Encrypt();
                    string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chi_reason.Encrypt() : model.chit_reason.Encrypt();
                    string path = string.Format("~/CPAIMVC/CharterIn?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                    if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterIn/CharterInCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                    return Redirect(path);
                }
            }
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                return View("CharterInCMMT", model);
            }
            else
            {
                return View("Index", model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenPDF")]
        public ActionResult GenPDF(FormCollection form, CharterInViewModel model)
        {
            string path = "";
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                path = "~/Web/Report/CharteringInCMMTReport.aspx";
            }
            else
            {
                path = "~/Web/Report/CharteringInReport.aspx";
            }
            if (Request.QueryString["TranID"] != null && Request.QueryString["isDup"] == null)
            {
                string tranID = string.IsNullOrEmpty(model.taskID) ? Request.QueryString["TranID"].ToString() : model.taskID.Encrypt();
                //LoadDataCharterIn(tranID, ref model);
                path += "?TranID=" + tranID;
            }
            else
            {
                //this.updateModel(form, model, true);
                this.updateModel(form, model);

                if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                {
                    ChitRootObject obj = new ChitRootObject();
                    obj.chit_chartering_method = model.chit_chartering_method;
                    obj.chit_evaluating = model.chit_evaluating;
                    obj.chit_negotiation_summary = model.chit_negotiation_summary;
                    obj.chit_proposed_for_approve = model.chit_proposed_for_approve;
                    obj.chit_reason = model.chit_reason;
                    obj.chit_note = model.chit_note;
                    obj.chit_detail = model.chit_detail;
                    obj.approve_items = model.approve_items;
                    obj.explanationAttach = form["hdfExplanFileUpload"].ToString();
                    //var json = new JavaScriptSerializer().Serialize(obj);
                    Session["ChitRootObject"] = obj;
                }
                else
                {
                    ChiRootObject obj = new ChiRootObject();
                    obj.chi_cargo_detail = model.chi_cargo_detail;
                    obj.chi_chartering_method = model.chi_chartering_method;
                    obj.chi_evaluating = model.chi_evaluating;
                    obj.chi_negotiation_summary = model.chi_negotiation_summary;
                    obj.chi_proposed_for_approve = model.chi_proposed_for_approve;
                    obj.chi_reason = model.chi_reason;
                    obj.chi_note = model.chi_note;
                    obj.approve_items = model.approve_items;

                    obj.explanationAttach = form["hdfExplanFileUpload"].ToString();
                    //var json = new JavaScriptSerializer().Serialize(obj);
                    Session["ChiRootObject"] = obj;
                }
            }

            return Redirect(path);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, CharterInViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            this.updateModel(form, model);
            ReturnValue rtn = new ReturnValue();
            CharterInServiceModel CharterInService = new CharterInServiceModel();
            if (ButtonListCharterIn != null)
            {
                var _lstButton = ButtonListCharterIn.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _evnt = _lstButton[0].name;
                    if (_evnt == "WS")
                    {
                        string CharterinTranID = model.taskID.Encrypt();
                        string path = "~/CPAIMVC/GraphBoard/AddFixture?CharterinTranID=" + CharterinTranID;

                        return Redirect(path);
                    }
                    string _xml = _lstButton[0].call_xml;
                    //string note = _lstButton[0].name.ToUpper() == "REJECT" || _lstButton[0].name.ToUpper() == "CANCEL" ? form["hdfNoteAction"] : "-";
                    string temp = form["hdfNoteAction"];
                    string note = string.IsNullOrEmpty(temp) ? "-" : temp;

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    model.explanationAttach = form["hdfExplanFileUpload"];
                    model.explanationAttachTrader = form["hdfExplanFileUploadTrader"];
                    string json;
                    if (type.Decrypt() == ConstantPrm.SYSTEMTYPE.CRUDE)
                    {
                        ChiRootObject obj = new ChiRootObject();
                        if (String.IsNullOrEmpty(model.tripNumber))
                        {
                            obj.chi_cargo_detail = model.chi_cargo_detail;
                        }
                        else
                        {
                            obj.chi_cargo_detail = model.chi_cargo_detail_TripNo;
                        }

                        obj.chi_chartering_method = model.chi_chartering_method;
                        obj.chi_evaluating = model.chi_evaluating;
                        obj.chi_negotiation_summary = model.chi_negotiation_summary;
                        obj.chi_proposed_for_approve = model.chi_proposed_for_approve;
                        obj.chi_reason = model.chi_reason;
                        obj.chi_note = model.chi_note;
                        obj.approve_items = model.approve_items;
                        obj.explanationAttach = model.explanationAttach;
                        obj.tripNumber = model.tripNumber; //Hide Trip No
                        json = new JavaScriptSerializer().Serialize(obj);

                        ////Save Note
                        rtn = new ReturnValue();
                        CharterInService = new CharterInServiceModel(); 
                        rtn = CharterInService.AddCharterNote(model, lbUserName, model.taskID);
                        if (rtn.Status == false)
                        {
                            TempData["res_message"] = rtn.Message;
                            initializeDropDownList(ref model);
                            TempData["CharterIn_Model"] = model;
                            return RedirectToAction("Index", "CharterIn");
                        }
                    }
                    else
                    {
                        ChitRootObject obj = new ChitRootObject();
                        obj.chit_chartering_method = model.chit_chartering_method;
                        obj.chit_evaluating = model.chit_evaluating;
                        obj.chit_negotiation_summary = model.chit_negotiation_summary;
                        obj.chit_proposed_for_approve = model.chit_proposed_for_approve;
                        obj.chit_reason = model.chit_reason;
                        obj.chit_note = model.chit_note;
                        obj.chit_detail = model.chit_detail;
                        obj.approve_items = model.approve_items;
                        obj.explanationAttach = model.explanationAttach;
                        obj.explanationAttachTrader = model.explanationAttachTrader;
                        json = new JavaScriptSerializer().Serialize(obj);
                        if (model.pageStatus == "WAITING_CONFIRM")
                        {
                            if(_evnt == "SUBMIT")
                            {
                                rtn = new ReturnValue();
                                CharterInService = new CharterInServiceModel();
                                rtn = CharterInService.AddCharterFor(model, lbUserName, model.taskID);
                                rtn = CharterInService.AddTraderNote(model, lbUserName, model.taskID);
                                /////SAVE UPLOAD FILE TRADER
                                if(!String.IsNullOrEmpty(model.explanationAttachTrader))
                                {
                                    if (rtn.Status == true)
                                    {
                                        rtn = new ReturnValue();
                                        CharterInService = new CharterInServiceModel();
                                        rtn = CharterInService.AddCharterTraderUploadCMMT(model, lbUserName, model.taskID);
                                    }
                                }                               
                                if (rtn.Status == true)
                                {
                                    rtn = CharterInService.AddCharterTraderAttachCMMT(form["hdfFileUpload"], lbUserName, model.taskID);
                                }
                                if (rtn.Status == false)
                                {
                                    TempData["res_message"] = rtn.Message;
                                    initializeDropDownList(ref model);
                                    if (type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                                    {
                                        TempData["CharterIn_Model"] = model;
                                        return RedirectToAction("CharterInCMMT", "CharterIn");
                                    }
                                }
                            }                            
                        }else if(model.pageStatus == "WAITING_CONFIRM_SH")
                        {
                            if (_evnt == "VERIFY")
                            {
                                rtn = new ReturnValue();
                                CharterInService = new CharterInServiceModel(); 
                                rtn = CharterInService.AddTraderNote(model, lbUserName, model.taskID);
                                if (rtn.Status == false)
                                {
                                    TempData["res_message"] = rtn.Message;
                                    initializeDropDownList(ref model);
                                    if (type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
                                    {
                                        TempData["CharterIn_Model"] = model;
                                        return RedirectToAction("CharterInCMMT", "CharterIn");
                                    }
                                }
                            }
                        }
                        ////Save Note
                        rtn = new ReturnValue();
                        CharterInService = new CharterInServiceModel();
                        rtn = CharterInService.AddCharterNoteCMMT(model, lbUserName, model.taskID);
                                               
                        if (rtn.Status == false)
                        {
                            TempData["res_message"] = rtn.Message;
                            initializeDropDownList(ref model);
                            TempData["CharterIn_Model"] = model;
                            return RedirectToAction("CharterInCMMT", "CharterIn");
                        }
                    }
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);

                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            //string purno = resData.resp_parameters[0].v.Encrypt();
                            //string reason = MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE ? model.chi_reason.Encrypt() : model.chit_reason.Encrypt();
                            string reason = note.Encrypt();
                            string path = string.Format("~/CPAIMVC/CharterIn?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterIn/CharterInCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownList(ref model);
            if (type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                if(model.groupNameList == null)
                {
                    model.groupNameList = new List<string>();
                }
                TempData["CharterIn_Model"] = model;
                return RedirectToAction("CharterInCMMT", "CharterIn");
            }
            else
            {
                TempData["CharterIn_Model"] = model;
                return RedirectToAction("Index", "CharterIn");
            }
        }

        [HttpPost]
        public string getVenderCommission(string id)
        {
            return VendorDAL.GetVendorCommission(id);
        }

        [HttpPost]
        public string getChartererDataById(string id)
        {
            return CharterInServiceModel.getChartererById(id);
        }

        public ActionResult Search()
        {
            CharteringSearchViewModel CharteringModel = new CharteringSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CHARTERING);

            msdata = msdata.OrderBy(x => x).ToList();

            CharteringModel.createdByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            if (Request.QueryString["Type"] != null)
                MCCTypeSS = Request.QueryString["Type"];
            ViewBag.getVessel = DropdownServiceModel.getVehicle(MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL ? "CHIVESMT" : "CHIVESCS", true);
            ViewBag.getBroker = DropdownServiceModel.getVendorFreight(MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL ? "CHIBROMT" : "CHIBROCS", true);
            ViewBag.getCharter = DropdownServiceModel.getCustomer("CHR_FRE").OrderBy(x => x.Text).ToList();
            ViewBag.TypeDecrypt = MCCTypeSS.Decrypt();
            ViewBag.getUsers = userList;

            CharteringModel.vessel = ViewBag.getVessel;
            CharteringModel.broker = ViewBag.getBroker;
            CharteringModel.charterer = ViewBag.getCharter;
            CharteringModel.created_by = ViewBag.getUsers;
            CharteringModel.sDate = "";
            CharteringModel.sLaycan = "";
            CharteringModel.sOwner = "";
            CharteringModel.sCreateBy = "";
            return View(CharteringModel);
        }

        [HttpPost]
        public ActionResult SearchResult(FormCollection fm, CharteringSearchViewModel model)
        {
            CharteringSearchViewModel CharteringModel = new CharteringSearchViewModel();
            UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            List<USERS> findUserByGroup = objUserGroup.findUserByGroup(findByUserAndSystem.USG_USER_GROUP);

            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CHARTERING);
            msdata = msdata.OrderBy(x => x).ToList();

            CharteringModel.createdByUser = findUserByGroup;

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            //userList = userList.Distinct().ToArray();

            ViewBag.getVessel = DropdownServiceModel.getVehicle(MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL ? "CHIVESMT" : "CHIVESCS", true);
            ViewBag.getBroker = DropdownServiceModel.getVendorFreight(MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL ? "CHIBROMT" : "CHIBROCS", true);
            ViewBag.getCharter = DropdownServiceModel.getCustomer("CHR_FRE").OrderBy(x => x.Text).ToList();
            ViewBag.TypeDecrypt = MCCTypeSS.Decrypt();
            userList = userList.Distinct().ToList();
            ViewBag.getUsers = userList.Distinct().ToList();

            string datePurchase = model.sDate; //fm["datePurchaseField"].ToString();
            string stDate = "";
            string enDate = "";
            string vessel = model.sVessel; // fm["vessel"].ToString();
            //string flatRate = fm["flatRate"].ToString();
            string laycan = model.sLaycan; //fm["laycan"].ToString();
            string laycan_start = "";
            string laycan_end = "";
            string broker = model.sBroker; //fm["broker"].ToString();
            string charter = model.sCharterer;
            string owner = model.sOwner; // fm["owner"].ToString();
            string userlist = "";
            if (fm["userList"] != null)
            {
                userlist = fm["userList"].ToString().Replace(",", "|");
            }

            //if (!datePurchase.Equals(""))
            //if(fm["userList"] != null)
            //{
            //    userlist = fm["userList"].ToString().Replace(",", "|");
            //}
            if (!String.IsNullOrEmpty(model.sCreateBySelected))
            {
                var createList = model.sCreateBySelected.Split('|');
                var create = "";
                userlist = model.sCreateBySelected.ToString().Replace(",", "|");
                foreach (var item in createList)
                {
                    create += ",'" + item + "'";
                }
                CharteringModel.sCreateBySelected = "[" + create.Substring(1, create.Length - 1) + "]";
                CharteringModel.sCreateBy = "";
            }

            if (!String.IsNullOrEmpty(datePurchase))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            if (!String.IsNullOrEmpty(laycan))
            {
                string[] s = laycan.Split(new[] { " to " }, StringSplitOptions.None);
                laycan_start = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                laycan_end = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_Chartertrx res = SearchCharterInData(stDate, enDate, vessel, charter, datePurchase, "", userlist, laycan, broker, owner, laycan_start, laycan_end);
            List_Chartertrx newRes = new List_Chartertrx();
            newRes.CharteringTransaction = new List<CharteringEncrypt>();
            var chkRole = false;
            if (!String.IsNullOrEmpty(lbUserID))
            {
                using (var context = new EntityCPAIEngine())
                {
                    var usersRow = "";
                    var userDetail = context.USERS.Where(a => a.USR_LOGIN.ToUpper() == lbUserID.ToUpper()).ToList();
                    foreach (var item in userDetail)
                    {
                        usersRow = item.USR_ROW_ID;
                    }
                    if (!String.IsNullOrEmpty(usersRow))
                    {
                        var user_role = context.CPAI_USER_GROUP.Where(a => a.USG_FK_USERS.ToUpper() == usersRow.ToUpper() && a.USG_USER_SYSTEM == "CHARTERING").ToList();
                        var i = 0;
                        foreach(var x in user_role)
                        {
                            if(x.USG_USER_GROUP == "CMLA" || x.USG_USER_GROUP == "CMLA_SH" || x.USG_USER_GROUP == "CMPSII"|| x.USG_USER_GROUP == "CMPSIE"|| x.USG_USER_GROUP == "CMPSDS"|| x.USG_USER_GROUP == "CMPSIE_SH" || x.USG_USER_GROUP == "CMPSII_SH" || x.USG_USER_GROUP == "CMPSDS_SH")
                            {
                                i++;
                            }
                        }

                        chkRole = i > 0 ? true: false;
                        if (chkRole)
                        {
                            foreach (var item in res.CharteringTransaction)
                            {
                                var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_ROW_ID == item.transaction_id);
                                if (chitDetail != null)
                                {
                                    foreach (var z in user_role)
                                    {
                                        if (chitDetail.IDAT_CHARTER_FOR == "CMPSII" || chitDetail.IDAT_CHARTER_FOR == "CMPSIE" || chitDetail.IDAT_CHARTER_FOR == "CMPSDS")
                                        {
                                            if(z.USG_USER_GROUP == "CMPSII_SH" || z.USG_USER_GROUP == "CMPSIE_SH" || z.USG_USER_GROUP == "CMPSDS_SH")
                                            {
                                                if (item.status != "DRAFT")
                                                {
                                                    newRes.CharteringTransaction.Add(item);
                                                }
                                            }else if(z.USG_USER_GROUP == chitDetail.IDAT_CHARTER_FOR)
                                            {
                                                if (item.status != "DRAFT")
                                                {
                                                    newRes.CharteringTransaction.Add(item);
                                                }
                                            }
                                        }else if(chitDetail.IDAT_CHARTER_FOR == "CMLA")
                                        {
                                            if (z.USG_USER_GROUP == "CMLA_SH")
                                            {
                                                if (item.status != "DRAFT")
                                                {
                                                    newRes.CharteringTransaction.Add(item);
                                                }
                                            }
                                            else if (z.USG_USER_GROUP == chitDetail.IDAT_CHARTER_FOR)
                                            {
                                                if (item.status != "DRAFT")
                                                {
                                                    newRes.CharteringTransaction.Add(item);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    } 
                }
               
            }
            if (chkRole)
            {
                CharteringModel.CharteringTransaction = newRes.CharteringTransaction.OrderByDescending(x => x.transaction_id).ToList();
                CharteringModel.vessel = ViewBag.getVessel;
                for (int i = 0; i < newRes.CharteringTransaction.Count; i++)
                {
                    CharteringModel.CharteringTransaction[i].cust_short = MT_VENDOR_DAL.GetNameShort(CharteringModel.CharteringTransaction[i].cust_id);
                    CharteringModel.CharteringTransaction[i].charterer_name = CharteringModel.CharteringTransaction[i].charterer_name;
                    CharteringModel.CharteringTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CharteringModel.CharteringTransaction[i].date_purchase, "MMMM dd yyyy");
                    CharteringModel.CharteringTransaction[i].Transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].transaction_id.Encrypt();
                    CharteringModel.CharteringTransaction[i].Req_transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].req_transaction_id.Encrypt();
                    CharteringModel.CharteringTransaction[i].Purchase_no_Encrypted = CharteringModel.CharteringTransaction[i].purchase_no.Encrypt();
                    CharteringModel.CharteringTransaction[i].reason_Encrypted = CharteringModel.CharteringTransaction[i].reason.Encrypt();
                    if (Session["Charter_Type"] != null)
                        CharteringModel.CharteringTransaction[i].type_Encrypted = Session["Charter_Type"].ToString();

                    var vesselName = CharteringModel.vessel.SingleOrDefault(a => a.Value == CharteringModel.CharteringTransaction[i].vessel_id);

                    CharteringModel.CharteringTransaction[i].vessel = vesselName == null ? CharteringModel.CharteringTransaction[i].vessel : vesselName.Text;

                }
            }
            else
            {
                CharteringModel.CharteringTransaction = res.CharteringTransaction.OrderByDescending(x => x.transaction_id).ToList();
                CharteringModel.vessel = ViewBag.getVessel;
                for (int i = 0; i < res.CharteringTransaction.Count; i++)
                {
                    CharteringModel.CharteringTransaction[i].cust_short = MT_VENDOR_DAL.GetNameShort(CharteringModel.CharteringTransaction[i].cust_id);
                    CharteringModel.CharteringTransaction[i].charterer_name = CharteringModel.CharteringTransaction[i].charterer_name;
                    CharteringModel.CharteringTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(CharteringModel.CharteringTransaction[i].date_purchase, "MMMM dd yyyy");
                    CharteringModel.CharteringTransaction[i].Transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].transaction_id.Encrypt();
                    CharteringModel.CharteringTransaction[i].Req_transaction_id_Encrypted = CharteringModel.CharteringTransaction[i].req_transaction_id.Encrypt();
                    CharteringModel.CharteringTransaction[i].Purchase_no_Encrypted = CharteringModel.CharteringTransaction[i].purchase_no.Encrypt();
                    CharteringModel.CharteringTransaction[i].reason_Encrypted = CharteringModel.CharteringTransaction[i].reason.Encrypt();
                    if (Session["Charter_Type"] != null)
                        CharteringModel.CharteringTransaction[i].type_Encrypted = Session["Charter_Type"].ToString();

                    var vesselName = CharteringModel.vessel.SingleOrDefault(a => a.Value == CharteringModel.CharteringTransaction[i].vessel_id);

                    CharteringModel.CharteringTransaction[i].vessel = vesselName == null ? CharteringModel.CharteringTransaction[i].vessel : vesselName.Text;

                }
            } 

            CharteringModel.broker = ViewBag.getBroker;
            CharteringModel.created_by = ViewBag.getUsers;
            CharteringModel.charterer = ViewBag.getCharter;

            return View("Search", CharteringModel);
        }


        private void updateModel(FormCollection form, CharterInViewModel model, bool isTextMode = false)
        {
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE)
            {
                //Assign Laycan
                if (!string.IsNullOrEmpty(model.ev_tempLaycan))
                {
                    string[] ev_laycan = model.ev_tempLaycan.Split(' ');
                    model.chi_evaluating.laycan_from = ev_laycan[0];
                    model.chi_evaluating.laycan_to = ev_laycan[2];
                }

                if (!string.IsNullOrEmpty(model.pa_tempLaycan))
                {
                    string[] pa_laycan = model.pa_tempLaycan.Split(' ');
                    model.chi_proposed_for_approve.laycan_from = pa_laycan[0];
                    model.chi_proposed_for_approve.laycan_to = pa_laycan[2];
                }

                //Assign loadport order and discharge port order

                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{//Hide Trip No

                if (model.checkSelectCargo == "M")
                {
                    model.tripNumber = null;
                }
                else
                {
                    if (model.chi_cargo_detail_TripNo != null)
                    {
                        model.chi_cargo_detail_TripNo.route = model.chi_cargo_detail.route;
                        model.chi_cargo_detail_TripNo.discharge_ports = new List<ChiDisPort>();
                        foreach (var item in model.chi_cargo_detail.discharge_ports)
                        {
                            model.chi_cargo_detail_TripNo.discharge_ports.Add(item);
                        }
                    }

                }
                List<SelectListItem> port = DropdownServiceModel.getPortName(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "PORT_CMMT" : "PORT_CMCS");
                List<SelectListItem> crude = DropdownServiceModel.getMaterial(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "MAT_CMMT" : "MAT_CMCS");
                List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHISUPMT" : "CHISUPCS", true);

                if (model.chi_cargo_detail != null)
                {
                    if (model.chi_cargo_detail.chi_load_ports != null)
                    {
                        for (int i = 0; i < model.chi_cargo_detail.chi_load_ports.Count; i++)
                        {
                            model.chi_cargo_detail.chi_load_ports[i].order_port = (i + 1) + "";
                            if (isTextMode)
                            {
                                model.chi_cargo_detail.chi_load_ports[i].port = port.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].port).FirstOrDefault().Text;
                                model.chi_cargo_detail.chi_load_ports[i].crude = crude.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].crude).FirstOrDefault().Text;
                                model.chi_cargo_detail.chi_load_ports[i].supplier = suplier.Where(x => x.Value == model.chi_cargo_detail.chi_load_ports[i].supplier).FirstOrDefault().Text;
                            }
                        }
                    }

                    if (model.chi_cargo_detail.discharge_ports != null)
                    {
                        for (int i = 0; i < model.chi_cargo_detail.discharge_ports.Count; i++)
                        {
                            model.chi_cargo_detail.discharge_ports[i].dis_order_port = (i + 1) + "";
                        }
                    }

                }
                //}

                //Assign chartering method type, reason
                if (model.chi_chartering_method == null)
                {
                    model.chi_chartering_method = new ChiCharteringMethod();
                }

                model.chi_chartering_method.chi_type_of_fixings = new List<ChiTypeOfFixing>();
                string type = form["chi_chartering_method_chi_type_of_fixings"].ToString();
                if (type == "M")
                {
                    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "Y" });
                    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "N" });

                }
                else
                {
                    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Marketing Order", type_flag = "N" });
                    model.chi_chartering_method.chi_type_of_fixings.Add(new ChiTypeOfFixing { type_value = "Private Order", type_flag = "Y" });
                }

                //Assign round_no, order no., order type and final flag
                List<SelectListItem> broker = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIBROMT" : "CHIBROCS", true);
                List<SelectListItem> vendor = DropdownServiceModel.getVendor(true);
                List<SelectListItem> vehicle = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
                if (model.chi_negotiation_summary != null && form["negotiation_summary_offers_items_final_flag"] != null)
                {
                    for (int i = 0; i < model.chi_negotiation_summary.chi_offers_items.Count; i++)
                    {
                        if (form["negotiation_summary_offers_items_final_flag"].ToString() == i + "")
                        {
                            model.chi_negotiation_summary.chi_offers_items[i].final_flag = "Y";
                        }
                        else
                        {
                            model.chi_negotiation_summary.chi_offers_items[i].final_flag = "N";
                        }
                        if (isTextMode)
                        {
                            model.chi_negotiation_summary.chi_offers_items[i].broker = broker.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].broker).FirstOrDefault().Text;
                            model.chi_negotiation_summary.chi_offers_items[i].vessel = vehicle.Where(x => x.Value == model.chi_negotiation_summary.chi_offers_items[i].vessel).FirstOrDefault().Text;
                        }
                        if (model.chi_negotiation_summary.chi_offers_items[i].chi_round_items != null)
                        {
                            for (int j = 0; j < model.chi_negotiation_summary.chi_offers_items[i].chi_round_items.Count; j++)
                            {
                                model.chi_negotiation_summary.chi_offers_items[i].chi_round_items[j].round_no = (j + 1) + "";
                                for (int k = 0; k < model.chi_negotiation_summary.chi_offers_items[i].chi_round_items[j].chi_round_info.Count; k++)
                                {
                                    model.chi_negotiation_summary.chi_offers_items[i].chi_round_items[j].chi_round_info[k].order = (k + 1) + "";
                                    if (j != 0)
                                    {
                                        model.chi_negotiation_summary.chi_offers_items[i].chi_round_items[j].chi_round_info[k].type = model.chi_negotiation_summary.chi_offers_items[i].chi_round_items[0].chi_round_info[k].type;
                                    }
                                }
                            }
                        }
                    }
                }

                //Assign reason
                TempData["CharterIn_Reason"] = model.chi_reason;
                model.chi_reason = !(string.IsNullOrEmpty(form["hdfNoteAction"])) ? form["hdfNoteAction"].ToString() : !(string.IsNullOrEmpty(Request.QueryString["Reason"])) ? Request.QueryString["Reason"].ToString().Decrypt() : "-";

                if (isTextMode)
                {
                    model.chi_proposed_for_approve.charterer_broker = broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault().Text;
                    model.chi_proposed_for_approve.owner_broker = model.chit_proposed_for_approve.owner;
                    model.chi_proposed_for_approve.vessel_name = model.chit_proposed_for_approve.vessel_name;
                }
            }
            else
            {

                if (!string.IsNullOrEmpty(model.ev_tempLaycan))
                {
                    string[] pa_laycan = model.pa_tempLaycan.Split(new[] { " to " }, StringSplitOptions.None);
                    model.chit_proposed_for_approve.laycan_from = pa_laycan[0];
                    model.chit_proposed_for_approve.laycan_to = pa_laycan[1];
                }

                //Assign loadport order and discharge port order
                List<SelectListItem> port = DropdownServiceModel.getPortName(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "PORT_CMMT" : "PORT_CMCS");
                List<SelectListItem> crude = DropdownServiceModel.getMaterial(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "MAT_CMMT" : "MAT_CMCS");
                List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHISUPMT" : "CHISUPCS", true);


                //2017/07/13 Kiattisak Add CargoDetail 
                if (model.chit_detail.chit_cargo_qty != null)
                {
                    for (int i = 0; i < model.chit_detail.chit_cargo_qty.Count; i++)
                    {
                        model.chit_detail.chit_cargo_qty[i].order = (i + 1) + "";
                    }
                }
                //Assign chartering method type, reason
                if (model.chit_chartering_method == null)
                {
                    model.chit_chartering_method = new ChitCharteringMethod();
                }

                if (model.chit_detail.bss != null && model.chit_detail.bss != "")
                {
                    for (int i = 0; i < model.chit_detail.chit_discharge_port_control.Count; i++)
                    {
                        model.chit_detail.chit_discharge_port_control[i].PortOrder = (i + 1) + "";

                    }
                    for (int i = 0; i < model.chit_detail.chit_load_port_control.Count; i++)
                    {
                        model.chit_detail.chit_load_port_control[i].PortOrder = (i + 1) + "";

                    }

                }

                //Assign round_no, order no., order type and final flag
                List<SelectListItem> broker = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIBROMT" : "CHIBROCS", true);
                List<SelectListItem> vehicle = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
                List<SelectListItem> cust = DropdownServiceModel.getCustomer("CHR_FRE").OrderBy(x => x.Text).ToList();
                List<SelectListItem> vendor = DropdownServiceModel.getVendorFreight("CHISUPMT", true);
                string OwnerName = "";
                if (model.chit_negotiation_summary != null && form["negotiation_summary_offers_items_final_flag"] != null)
                {
                    for (int i = 0; i < model.chit_negotiation_summary.chit_offers_items.Count; i++)
                    {
                        if (form["negotiation_summary_offers_items_final_flag"].ToString() == i + "")
                        {
                            model.chit_negotiation_summary.chit_offers_items[i].final_flag = "Y";
                        }
                        else
                        {
                            model.chit_negotiation_summary.chit_offers_items[i].final_flag = "N";
                        }
                        if (isTextMode)
                        {
                            model.chit_negotiation_summary.chit_offers_items[i].broker = (model.chit_negotiation_summary.chit_offers_items[i].broker == "") ? "" : broker.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].broker).FirstOrDefault().Text;
                            model.chit_negotiation_summary.chit_offers_items[i].vessel = model.chit_negotiation_summary.chit_offers_items[i].vessel;
                            model.chit_negotiation_summary.chit_offers_items[i].owner = model.chit_negotiation_summary.chit_offers_items[i].owner;
                            if (model.chit_negotiation_summary.chit_offers_items[i].final_flag.ToUpper() == "Y") OwnerName = model.chit_negotiation_summary.chit_offers_items[i].owner;
                        }
                        if (model.chit_negotiation_summary.chit_offers_items[i].chit_round_items != null)
                        {
                            for (int j = 0; j < model.chit_negotiation_summary.chit_offers_items[i].chit_round_items.Count; j++)
                            {
                                model.chit_negotiation_summary.chit_offers_items[i].chit_round_items[j].round_no = (j + 1) + "";
                                for (int k = 0; k < model.chit_negotiation_summary.chit_offers_items[i].chit_round_items[j].chit_round_info.Count; k++)
                                {
                                    model.chit_negotiation_summary.chit_offers_items[i].chit_round_items[j].chit_round_info[k].order = (k + 1) + "";
                                    if (j != 0)
                                    {
                                        model.chit_negotiation_summary.chit_offers_items[i].chit_round_items[j].chit_round_info[k].type = model.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[k].type;
                                    }
                                }
                            }
                        }
                    }
                }

                //Assign reason
                TempData["CharterIn_Reason"] = model.chit_reason;
                model.chit_reason = !(string.IsNullOrEmpty(form["hdfNoteAction"])) ? form["hdfNoteAction"].ToString() : !(string.IsNullOrEmpty(Request.QueryString["Reason"])) ? Request.QueryString["Reason"].ToString().Decrypt() : "-";

                if (isTextMode)
                {
                    //model.chit_proposed_for_approve.charterer_broker = broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chi_proposed_for_approve.charterer_broker).FirstOrDefault().Text;
                    //model.chit_proposed_for_approve.vessel_name = model.chit_proposed_for_approve.vessel_name).FirstOrDefault() == null ? "" : vehicle.Where(x => x.Value == model.chit_proposed_for_approve.vessel_name).FirstOrDefault().Text;
                    //model.chit_proposed_for_approve.broker_name = broker.Where(x => x.Value == model.chit_proposed_for_approve.broker_name).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chit_proposed_for_approve.broker_name).FirstOrDefault().Text;
                    model.chit_detail.charterer = cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault() == null ? "" : cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault().Text;
                    model.chit_proposed_for_approve.owner = OwnerName;
                }

                if (model.chit_detail != null)
                {
                    if (String.IsNullOrEmpty(model.chit_detail.chartererName))
                    {
                        model.chit_detail.chartererName = cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault() == null ? "" : cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault().Text;
                    }

                }

                if (model.chit_negotiation_summary != null)
                {
                    if (model.chit_negotiation_summary.chit_offers_items != null)
                    {
                        foreach (var item in model.chit_negotiation_summary.chit_offers_items)
                        {
                            if (item.final_flag == "Y")
                            {
                                if (String.IsNullOrEmpty(model.chit_proposed_for_approve.broker_nameText))
                                {
                                    model.chit_proposed_for_approve.broker_nameText = item.owner;
                                }
                            }
                        }
                    }
                }
            }

        }

        private void initializeDropDownList(ref CharterInViewModel model)
        {
            model.vesselSize = new List<VesselSize>();
            model.broker = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIBROMT" : "CHIBROCS", true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? true : false);
            model.pricesTerm = getPricesTerm();
            model.vehicle = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
            model.cust = DropdownServiceModel.getCustomer("CHR_FRE").OrderBy(x => x.Text).ToList();

            model.vendor = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHISUPMT" : "CHISUPCS", true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? true : false);
            model.materials = DropdownServiceModel.getMaterial(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "MAT_CMMT" : "MAT_CMCS");
            model.portName = DropdownServiceModel.getPortName(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "PORT_CMMT" : "PORT_CMCS");

            model.vessel = getVesselSize(model);
            model.freight = getFreight();
            model.vesseltype = getVesselType();

            model.freight_type_list = getFreight();

            model.broker_list = new List<SelectListItem>();
            model.pricesTerm_list = getPricesTerm();
            model.product_list = new List<SelectListItem>();
            model.vessel_list = new List<SelectListItem>();
            model.type_list = getVesselType();
             
            model.ddlCharter_For = getCharter_For();
            model.quantity_list = getQuantity();

            model.market_ref = GetMasterTD();
            model.cargo_list = getCargo();
            model.cargo_Unit = getCargoUnit();
            model.charterer_list = getCharterer();
            model.bss_list = getBSS();
            model.NegoUnit = getNegoUnit();
            model.portTypeLoad = getPortList("l", "", "", "", "");
            model.portTypeDisCharge = getPortList("d", "", "", "", "");

            model.by_offer = getOfferBy();

            /////CMCS ///Trip No
            if (MCCTypeSS.Decrypt().ToUpper() != ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                model.tripNumberList = new List<SelectListItem>();
                model.LoadingMonthList = new List<LoadingMonth>();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var items = context.VW_PCF_WAITING_CHARTERING.Distinct().OrderBy(a => a.TRIP_NO).ToList();
                    foreach (var item in items)
                    {
                        var chkDuplicate = model.tripNumberList.SingleOrDefault(a => a.Value == item.TRIP_NO);
                        if (chkDuplicate == null)
                        {
                            model.tripNumberList.Add(new SelectListItem
                            {
                                Value = item.TRIP_NO,
                                Text = item.TRIP_NO
                            });
                        }
                        if (item.LOADING_MONTH != null)
                        {
                            int month = Convert.ToInt32(item.LOADING_MONTH);
                            var dt = new DateTime(2012, month, 1);
                            var monthAbbr = dt.ToString("MMM");
                            model.LoadingMonthList.Add(new LoadingMonth
                            {
                                tripNumber = item.TRIP_NO,
                                month = monthAbbr,
                                year = item.LOADING_YEAR
                            });
                        }
                    } 
                    if (!String.IsNullOrEmpty(model.purno))
                    {
                        if (String.IsNullOrEmpty(model.tripNumber))
                        {
                            var purno = model.purno;
                            var tranId = model.taskID;
                            var chi_data = context.CHI_DATA.SingleOrDefault(a => a.IDA_PURCHASE_NO == purno && a.IDA_ROW_ID == tranId);
                            if (chi_data != null)
                            { 
                                model.tripNumber = chi_data.IDA_TRIPNO;
                                var chkSelected = model.tripNumberList.SingleOrDefault(a => a.Value == chi_data.IDA_TRIPNO);
 
                                if (chkSelected == null)
 
                                {
                                    model.tripNumberList.Add(new SelectListItem
                                    {
                                        Value = chi_data.IDA_TRIPNO,
                                        Text = chi_data.IDA_TRIPNO
                                    });
                                }
                            }
                        }
                    }
                    if (String.IsNullOrEmpty(model.tripNumber))
                    {
                        model.checkSelectCargo = "M";
                    }
                    else
                    {
                        model.checkSelectCargo = "T";
                    }
                }
                if (model.chi_cargo_detail != null)
                {
                    if (model.chi_cargo_detail.chi_load_ports != null)
                    {
                        if (model.pageMode != null)
                        {
                            if (model.pageMode.ToUpper() == "EDIT")
                            {
                                List<SelectListItem> port = DropdownServiceModel.getPortName(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "PORT_CMMT" : "PORT_CMCS");
                                List<SelectListItem> crude = DropdownServiceModel.getMaterial(true, "", (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "MAT_CMMT" : "MAT_CMCS");
                                List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHISUPMT" : "CHISUPCS", true);

                                for (var i = 0; i < model.chi_cargo_detail.chi_load_ports.Count; i++)
                                {
                                    var portText = model.chi_cargo_detail.chi_load_ports[i].port;
                                    var chkPort = port.SingleOrDefault(x => x.Value == portText);
                                    if (chkPort != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].portName = chkPort.Text;
                                    }
                                    var crudeText = model.chi_cargo_detail.chi_load_ports[i].crude;
                                    var chkCrude = crude.SingleOrDefault(x => x.Value == crudeText);
                                    if (chkCrude != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].crudeName = chkCrude.Text;
                                    }
                                    var supplierText = model.chi_cargo_detail.chi_load_ports[i].supplier;
                                    var chkSuplier = suplier.SingleOrDefault(x => x.Value == supplierText);
                                    if (chkSuplier != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].supplierName = chkSuplier.Text;
                                    }
                                }

                                if (!String.IsNullOrEmpty(model.tripNumber))
                                {
                                    model.chi_cargo_detail_TripNo = new ChiCargoDetail();
                                    model.chi_cargo_detail_TripNo.chi_load_ports = new List<ChiLoadPort>();
                                    foreach (var item in model.chi_cargo_detail.chi_load_ports)
                                    {
                                        model.chi_cargo_detail_TripNo.chi_load_ports.Add(item);
                                    }
                                    //model.chi_cargo_detail.chi_load_ports = new List<ChiLoadPort>();
                                    model.chi_cargo_detail_TripNo.discharge_ports = new List<ChiDisPort>();
                                    foreach (var item in model.chi_cargo_detail.discharge_ports)
                                    {
                                        model.chi_cargo_detail_TripNo.discharge_ports.Add(item);
                                    }
                                    model.chi_cargo_detail.discharge_ports = new List<ChiDisPort>();
                                    model.chi_cargo_detail_TripNo.route = model.chi_cargo_detail.route;
                                }
                            }
                            else
                            {
                                using (EntityCPAIEngine context = new EntityCPAIEngine())
                                {
                                    for (var i = 0; i < model.chi_cargo_detail.chi_load_ports.Count; i++)
                                    {
                                        var portId = Convert.ToDecimal(model.chi_cargo_detail.chi_load_ports[i].port);
                                        var chkPort = context.MT_PORT.SingleOrDefault(a => a.MLP_LOADING_PORT_ID == portId);
                                        if (chkPort != null)
                                        {
                                            model.chi_cargo_detail.chi_load_ports[i].portName = chkPort.MLP_LOADING_PORT_NAME;
                                        }
                                        var crudeId = model.chi_cargo_detail.chi_load_ports[i].crude;
                                        var chkCrude = context.MT_MATERIALS.SingleOrDefault(a => a.MET_NUM == crudeId);
                                        if (chkCrude != null)
                                        {
                                            model.chi_cargo_detail.chi_load_ports[i].crudeName = chkCrude.MET_MAT_DES_ENGLISH;
                                        }
                                        var SupplierId = model.chi_cargo_detail.chi_load_ports[i].supplier;
                                        var chkSupplier = context.MT_VENDOR.SingleOrDefault(a => a.VND_ACC_NUM_VENDOR == SupplierId);
                                        if (chkSupplier != null)
                                        {
                                            model.chi_cargo_detail.chi_load_ports[i].supplierName = chkSupplier.VND_NAME1;
                                        }
                                    }
                                    if (!String.IsNullOrEmpty(model.tripNumber))
                                    {
                                        model.chi_cargo_detail_TripNo = new ChiCargoDetail();
                                        model.chi_cargo_detail_TripNo.chi_load_ports = new List<ChiLoadPort>();
                                        foreach (var item in model.chi_cargo_detail.chi_load_ports)
                                        {
                                            model.chi_cargo_detail_TripNo.chi_load_ports.Add(item);
                                        }
                                        //model.chi_cargo_detail.chi_load_ports = new List<ChiLoadPort>();
                                        model.chi_cargo_detail_TripNo.discharge_ports = new List<ChiDisPort>();
                                        foreach (var item in model.chi_cargo_detail.discharge_ports)
                                        {
                                            model.chi_cargo_detail_TripNo.discharge_ports.Add(item);
                                        }
                                        //model.chi_cargo_detail.discharge_ports = new List<ChiDisPort>();
                                        model.chi_cargo_detail_TripNo.route = model.chi_cargo_detail.route;
                                    }
                                }
                            }
                        }
                        else
                        {
                            using (EntityCPAIEngine context = new EntityCPAIEngine())
                            {
                                for (var i = 0; i < model.chi_cargo_detail.chi_load_ports.Count; i++)
                                {
                                    var portId = Convert.ToDecimal(model.chi_cargo_detail.chi_load_ports[i].port);
                                    var chkPort = context.MT_PORT.SingleOrDefault(a => a.MLP_LOADING_PORT_ID == portId);
                                    if (chkPort != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].portName = chkPort.MLP_LOADING_PORT_NAME;
                                    }
                                    var crudeId = model.chi_cargo_detail.chi_load_ports[i].crude;
                                    var chkCrude = context.MT_MATERIALS.SingleOrDefault(a => a.MET_NUM == crudeId);
                                    if (chkCrude != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].crudeName = chkCrude.MET_MAT_DES_ENGLISH;
                                    }
                                    var SupplierId = model.chi_cargo_detail.chi_load_ports[i].supplier;
                                    var chkSupplier = context.MT_VENDOR.SingleOrDefault(a => a.VND_ACC_NUM_VENDOR == SupplierId);
                                    if (chkSupplier != null)
                                    {
                                        model.chi_cargo_detail.chi_load_ports[i].supplierName = chkSupplier.VND_NAME1;
                                    }
                                }
                                if (!String.IsNullOrEmpty(model.tripNumber))
                                {
                                    model.chi_cargo_detail_TripNo = new ChiCargoDetail();
                                    model.chi_cargo_detail_TripNo.chi_load_ports = new List<ChiLoadPort>();
                                    foreach (var item in model.chi_cargo_detail.chi_load_ports)
                                    {
                                        model.chi_cargo_detail_TripNo.chi_load_ports.Add(item);
                                    }
                                    // model.chi_cargo_detail.chi_load_ports = new List<ChiLoadPort>();
                                    model.chi_cargo_detail_TripNo.discharge_ports = new List<ChiDisPort>();
                                    foreach (var item in model.chi_cargo_detail.discharge_ports)
                                    {
                                        model.chi_cargo_detail_TripNo.discharge_ports.Add(item);
                                    }
                                    //model.chi_cargo_detail.discharge_ports = new List<ChiDisPort>();
                                    model.chi_cargo_detail_TripNo.route = model.chi_cargo_detail.route;
                                }
                            }
                        }
                    }
                }
            } 

        }

        private List<SelectListItem> getVesselSize(CharterInViewModel model = null)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            List<SelectListItem> vesselSize = new List<SelectListItem>();
            if ((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE))
            {
                if (model != null)
                {
                    model.vesselSize = setting.vessel_size;
                }
                //Send vessel size list to view

                for (int i = 0; i < setting.vessel_size.Count; i++)
                {
                    vesselSize.Add(new SelectListItem { Text = setting.vessel_size[i].key, Value = setting.vessel_size[i].key });
                }
            }


            return vesselSize;
        }

        private List<SelectListItem> getCharter_For()
        {
            //LoadMaster From JSON
            // Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            Setting setting = JSONSetting.getSetting("JSON_CHARTER_IN_VESSEL");
            List<SelectListItem> charter_For = new List<SelectListItem>();
            for (int i = 0; i < setting.charter_For.Count; i++)
            {
                charter_For.Add(new SelectListItem { Text = setting.charter_For[i].text, Value = setting.charter_For[i].value });
            }
            return charter_For;
        }

        private List<SelectListItem> getFreight()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> freight = new List<SelectListItem>();
            for (int i = 0; i < setting.freight.Count; i++)
            {
                freight.Add(new SelectListItem { Text = setting.freight[i], Value = setting.freight[i] });
            }
            return freight;
        }

        private List<SelectListItem> getQuantity()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> quantity = new List<SelectListItem>();
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                if (setting.type_of_vessel != null)
                {
                    for (int i = 0; i < setting.type_of_vessel.Count; i++)
                    {
                        quantity.Add(new SelectListItem { Text = setting.type_of_vessel[i], Value = setting.type_of_vessel[i] });
                    }
                }
            }

            return quantity;
        }

        private List<SelectListItem> getVesselType()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> vesseltype = new List<SelectListItem>();
            if (setting.type_of_vessel != null)
            {
                for (int i = 0; i < setting.type_of_vessel.Count; i++)
                {
                    vesseltype.Add(new SelectListItem { Text = setting.type_of_vessel[i], Value = setting.type_of_vessel[i] });
                }
            }
            return vesseltype;
        }

        private List<SelectListItem> getPricesTerm()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> pricesTerm = new List<SelectListItem>();
            for (int i = 0; i < setting.prices_term.Count; i++)
            {
                pricesTerm.Add(new SelectListItem { Text = setting.prices_term[i], Value = setting.prices_term[i] });
            }
            return pricesTerm;
        }

        private List<SelectListItem> GetMasterTD()
        {
            DALGraphBoard _dalGraphBoard = new DALGraphBoard();
            List<SelectListItem> lstTD = new List<SelectListItem>();
            List<string> lstdalTD = _dalGraphBoard.GetTD();
            foreach (string _item in lstdalTD)
            {
                lstTD.Add(new SelectListItem() { Text = _item, Value = _item });
            }

            return lstTD;
        }

        private List<SelectListItem> getCharterer()
        {
            return DropdownServiceModel.getCustomer("CHR_FRE");
        }

        private List<SelectListItem> getCargo()
        {
            return DropdownServiceModel.getMaterial(false, "", "MAT_CMMT");
        }

        private List<SelectListItem> getCargoUnit()
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.CRUDE) ? "JSON_CHARTER_IN_CRUDE" : "JSON_CHARTER_IN_VESSEL");
            //Send vessel size list to view
            List<SelectListItem> quantity = new List<SelectListItem>();
            if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL)
            {
                if (setting.unitChit != null)
                {
                    for (int i = 0; i < setting.unitChit.Count; i++)
                    {
                        quantity.Add(new SelectListItem { Text = setting.unitChit[i], Value = setting.unitChit[i] });
                    }
                }
            }

            return quantity;
        }

        private List<SelectListItem> getBSS()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            //get built from the first key of vehicle.
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    selectList.Add(new SelectListItem { Text = "" + i + ":" + j, Value = i + ":" + j });
                }
            }
            return selectList;
        }

        private List<SelectListItem> getNegoUnit()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem { Text = "USD", Value = "USD" });
            selectList.Add(new SelectListItem { Text = "THB", Value = "THB" });
            return selectList;
        }

        private ResponseData SaveDataCharterIn(DocumentActionStatus _status, CharterInViewModel model, string note, string json_fileUpload, string attached_explain = "")
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_2; NextState = ConstantPrm.ACTION.WAITING_CERTIFIED; }
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : ConstantPrm.SYSTEMTYPE.CRUDE;
            model.explanationAttach = attached_explain;

            ChiRootObject obj = new ChiRootObject();
            if (model.checkSelectCargo == "M")
            {
                obj.chi_cargo_detail = model.chi_cargo_detail;
            }
            else
            {
                if (model.chi_cargo_detail_TripNo == null)
                {
                    model.chi_cargo_detail_TripNo = new ChiCargoDetail();
                    model.chi_cargo_detail_TripNo.chi_load_ports = new List<ChiLoadPort>();
                    model.chi_cargo_detail_TripNo.discharge_ports = new List<ChiDisPort>();
                    foreach (var item in model.chi_cargo_detail.discharge_ports)
                    {
                        model.chi_cargo_detail_TripNo.discharge_ports.Add(item);
                    }
                    model.chi_cargo_detail_TripNo.route = model.chi_cargo_detail.route;
                    obj.chi_cargo_detail = model.chi_cargo_detail_TripNo;
                }
                else
                {
                    obj.chi_cargo_detail = model.chi_cargo_detail_TripNo;
                }

            }

            obj.chi_chartering_method = model.chi_chartering_method;
            obj.chi_evaluating = model.chi_evaluating;
            obj.chi_negotiation_summary = model.chi_negotiation_summary;
            obj.chi_proposed_for_approve = model.chi_proposed_for_approve;
            obj.chi_reason = model.chi_reason;
            obj.chi_note = model.chi_note;
            obj.explanationAttach = model.explanationAttach;
            obj.tripNumber = model.tripNumber;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000006;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private ResponseData SaveDataCharterInCMMT(DocumentActionStatus _status, CharterInViewModel model, string note, string json_fileUpload, string attached_explain = "")
        {

            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_VERIFY; }
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString().Decrypt() : ConstantPrm.SYSTEMTYPE.VESSEL;
            model.explanationAttach = attached_explain;

            ChitRootObject obj = new ChitRootObject();
            obj.chit_chartering_method = model.chit_chartering_method;
            obj.chit_evaluating = model.chit_evaluating;
            obj.chit_negotiation_summary = model.chit_negotiation_summary;
            obj.chit_proposed_for_approve = model.chit_proposed_for_approve;
            obj.chit_reason = model.chit_reason;
            obj.chit_note = model.chit_note;
            obj.explanationAttach = model.explanationAttach;
            obj.chit_detail = model.chit_detail;
            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000025;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        private ResponseData LoadDataCharterIn(string TransactionID, ref CharterInViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CHARTERING });
            req.Req_parameters.P.Add(new P { K = "type", V = type.Decrypt() });
            req.Extra_xml = "";
            MCCTypeSS = type;

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }

            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CharterInViewModel>(_model.data_detail);

                ////GET FILE UPLOAD TRADER
                using (var context = new EntityCPAIEngine())
                {
                    var traATT = context.CHIT_ATTACH_FILE.Where(a => a.ITAF_FK_CHIT_DATA == TransactionID && a.ITAF_TYPE == "TRA").ToList();
                    foreach(var z in traATT)
                    {
                        model.explanationAttachTrader += z.ITAF_PATH +":"+z.ITAF_INFO + "|";
                    }
                }

                if (model.chi_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chi_evaluating.laycan_from) && !string.IsNullOrEmpty(model.chi_evaluating.laycan_to))
                    {
                        model.ev_tempLaycan = model.chi_evaluating.laycan_from + " to " + model.chi_evaluating.laycan_to;
                    }
                    if (!string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chi_proposed_for_approve.laycan_from + " to " + model.chi_proposed_for_approve.laycan_to;
                    }
                }
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = ((type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "../" : "") + returnPage;
            }

            if (!String.IsNullOrEmpty(lbUserID))
            {
                using (var context = new EntityCPAIEngine())
                {
                    var usersRow = "";
                    var userDetail = context.USERS.Where(a => a.USR_LOGIN.ToUpper() == lbUserID.ToUpper()).ToList();
                    foreach (var item in userDetail)
                    {
                        usersRow = item.USR_ROW_ID;
                    }
                    if (!String.IsNullOrEmpty(usersRow))
                    {
                        var user_role = context.CPAI_USER_GROUP.Where(a => a.USG_FK_USERS.ToUpper() == usersRow.ToUpper() && a.USG_USER_SYSTEM == "CHARTERING").ToList();
                        model.groupNameList = new List<string>();
                        foreach (var item in user_role)
                        {
                            model.groupNameList.Add(item.USG_USER_GROUP);
                        }
                    }
                }
            }

            if (!(MCCTypeSS.Decrypt().ToUpper() != ConstantPrm.SYSTEMTYPE.VESSEL))
            {
                var purno = "";
                var taskId = "";
                if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
                {
                    purno = resData.resp_parameters[0].v;
                }
                if (!String.IsNullOrEmpty(purno))
                { 
                    using (var context = new EntityCPAIEngine())
                    {
                        var cherterInDetail = context.CHIT_DATA.Where(a => a.IDAT_PURCHASE_NO == purno).ToList();
                        foreach (var item in cherterInDetail)
                        {
                            model.chit_detail.charterFor = item.IDAT_CHARTER_FOR;
                            taskId = item.IDAT_ROW_ID;
                            if(!String.IsNullOrEmpty(item.IDAT_SELECT_CHARTER_FOR))
                            {
                                model.chit_negotiation_summary.select_charter_for = item.IDAT_SELECT_CHARTER_FOR;
                            }
                        }
                    }
                }
                

                if(model.chit_negotiation_summary != null)
                {
                    using (var context = new EntityCPAIEngine())
                    {
                        foreach(var item in model.chit_negotiation_summary.chit_offers_items)
                        {
                            var userDetail = context.CHIT_OFFER_ITEMS.Where(a => a.ITOI_FK_CHIT_DATA == taskId && a.ITOI_FK_VENDOR_BK == item.broker).ToList();
                            foreach(var z in userDetail)
                            {
                                item.row_Id = z.ITOI_ROW_ID;
                            }
                        }
                    }
                }
                if(!String.IsNullOrEmpty(TransactionID))
                {
                    using (var context = new EntityCPAIEngine())
                    {
                        FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
                        List<String> lstStatus = new List<string>();
                        var add_s = "ALL";
                        lstStatus.Add(add_s);
                        List<string> lstIndexValues = new List<string>();
                        for(var i =0; i <20; i++)
                        {
                            if(i == 0)
                            {
                                lstIndexValues.Add("CHARTERING");
                            }else if(i == 1)
                            {
                                lstIndexValues.Add("VESSEL");
                            }else
                            {
                                lstIndexValues.Add(null);
                            }
                        }
                        var result = ftxMan.findCPAIBunkerTransaction("25", lstStatus, lstIndexValues, null, null);
                        var funcDetail = result.Where(a => a.FUNCTION_TRANSACTION.FTX_TRANS_ID == TransactionID).ToList();
                        foreach(var item in funcDetail)
                        {
                            model.pageStatus = item.FUNCTION_TRANSACTION.FTX_INDEX4;
                        }
                        if(model.pageStatus == "WAITING_CONFIRM")
                        {
                            model.chit_negotiation_summary.select_charter_for = "";
                        }
                    }                        
                }
                if(String.IsNullOrEmpty(model.chit_detail.ReasonTrader))
                {
                    using (var context = new EntityCPAIEngine())
                    {
                        var userDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_ROW_ID == taskId);
                        if(userDetail != null)
                        {
                            model.chit_detail.ReasonTrader = userDetail.IDAT_CHARTER_FOR_CMMENT;
                        }
                    }
                }

            }
                

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            string _btn = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            model.buttonCode += _btn;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }


            if (model.chit_detail != null)
            {
                model.chit_detail.TempPageMode = (model.pageMode == null) ? "EDIT" : model.pageMode;
                model.BSSDataTmp = new JavaScriptSerializer().Serialize(model.chit_detail);
                if (model.chit_proposed_for_approve != null)
                {

                    if (!string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                        model.ev_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                    }
                    if (!string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                        model.ev_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                    }
                }
            }

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref CharterInViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.VERIFIED).ToList().Count > 0 || Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.CERTIFIED).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }
                string _btn = "";
                foreach (ButtonAction _button in Button)
                {
                    if(_button.name == "CERTIFIED")
                    {
                        _button.name = "ENDORSED";
                    }
                    if (_button.name == "WS" && model.chi_evaluating.freight == "LUMPSUM")
                    {
                        // Not display button
                        _btn = "";
                    }
                    else
                    {
                         if(_button.name == "SUBMIT" || _button.name == "REJECT")
                        {
                            if(model.pageStatus == "WAITING_CONFIRM_SH")
                            {
                                var chk = 0;
                                foreach (var z in model.groupNameList)
                                {
                                    if (z == "CMPSIE_SH" || z == "CMPSII_SH" || z == "CMPSDS_SH")
                                    {
                                        if (model.chit_detail.charterFor == "CMPSIE" || model.chit_detail.charterFor == "CMPSII" || model.chit_detail.charterFor == "CMPSDS")
                                        {
                                            // _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                            chk = 1;
                                        }
                                    }
                                    else if (z == "CMLA_SH")
                                    {
                                        if (model.chit_detail.charterFor == "CMLA")
                                        {
                                            //_btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                            chk = 1;
                                        }
                                    }
                                }
                                if (chk == 1)
                                {
                                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                }
                                //    if (model.groupName == "CMPSIE_SH" || model.groupName == "CMPSII_SH" || model.groupName == "CMPSDS_SH")
                                //{                                     
                                //    if(model.chit_detail.charterFor == "CMPSIE" || model.chit_detail.charterFor == "CMPSII" || model.chit_detail.charterFor == "CMPSDS")
                                //    {
                                //        _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                //    }
                                //}else if(model.groupName == "CMLA_SH")
                                //{
                                //    if(model.chit_detail.charterFor == "CMLA")
                                //    {
                                //        _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                //    }
                                //} 
                            }
                            else
                            {
                                var chk = 0;
                                foreach (var z in model.groupNameList)
                                {
                                    if (z == "CMPSIE" || z == "CMPSII" || z == "CMPSDS" || z == "CMLA")
                                    {
                                        if (z == model.chit_detail.charterFor)
                                        {
                                            //_btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                            chk = 1;
                                        }
                                    }
                                    else
                                    {
                                        // _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                        chk = 1;
                                    }
                                }
                                if(chk == 1)
                                {
                                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                }
                                //    if (model.groupName == "CMPSIE" || model.groupName == "CMPSII" || model.groupName == "CMPSDS" || model.groupName == "CMLA")
                                //{
                                //    if (model.groupName == model.chit_detail.charterFor)
                                //    {
                                //        _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";

                                //    }
                                //}
                                //else
                                //{
                                //    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                //}
                            } 
                        }
                        else if (_button.name == "VERIFY")
                        {
                            if (model.pageStatus == "WAITING_CONFIRM_SH")
                            {
                                var chk = 0;
                                foreach(var z in model.groupNameList)
                                {
                                    if (z == "CMPSIE_SH" || z == "CMPSII_SH" || z == "CMPSDS_SH")
                                    {
                                        if (model.chit_detail.charterFor == "CMPSIE" || model.chit_detail.charterFor == "CMPSII" || model.chit_detail.charterFor == "CMPSDS")
                                        {
                                            chk = 1;
                                           // _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                        }
                                    }
                                    else if (z == "CMLA_SH")
                                    {
                                        if (model.chit_detail.charterFor == "CMLA")
                                        {
                                            chk = 1;
                                           // _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                        }
                                    }
                                }
                                if(chk == 1)
                                {
                                    _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                }

                                //if (model.groupName == "CMPSIE_SH" || model.groupName == "CMPSII_SH" || model.groupName == "CMPSDS_SH")
                                //{
                                //    if (model.chit_detail.charterFor == "CMPSIE" || model.chit_detail.charterFor == "CMPSII" || model.chit_detail.charterFor == "CMPSDS")
                                //    {
                                //        _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                //    }
                                //}
                                //else if (model.groupName == "CMLA_SH")
                                //{
                                //    if (model.chit_detail.charterFor == "CMLA")
                                //    {
                                //        _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                                //    }
                                //}
                            }
                            else
                            {
                                _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                            }
                        }                        
                        else
                        {
                            _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                        }
                    }

                    model.buttonCode += _btn;
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListCharterIn = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        private List_Chartertrx SearchCharterInData(string sDate, string eDate, string vessel, string charter, string date_purchase, string flat_rate, string create_by, string laycan, string broker, string owner, string laycan_start, string laycan_end)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000004;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CHARTERING });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = MCCTypeSS.Decrypt() == ConstantPrm.SYSTEMTYPE.VESSEL ? CHARTER_IN_CMMT_FUNCTION_CODE : CHARTER_IN_FUNCTION_CODE });
            req.Req_parameters.P.Add(new P { K = "index_5", V = vessel });
            req.Req_parameters.P.Add(new P { K = "index_7", V = flat_rate });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = charter });
            //req.Req_parameters.P.Add(new P { K = "index_10", V = laycan });
            req.Req_parameters.P.Add(new P { K = "index_11", V = broker });
            req.Req_parameters.P.Add(new P { K = "index_12", V = owner });
            req.Req_parameters.P.Add(new P { K = "index_19", V = laycan_start });
            req.Req_parameters.P.Add(new P { K = "index_20", V = laycan_end });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_Chartertrx _model = ShareFunction.DeserializeXMLFileToObject<List_Chartertrx>(_DataJson);
            if (_model == null) _model = new List_Chartertrx();
            if (_model.CharteringTransaction == null) _model.CharteringTransaction = new List<CharteringEncrypt>();
            return _model;
        }

        public ActionResult ApproveCharterIn()
        {
            //ForTest
            //http://localhost:50131/CPAIMVC/CharterIn/ApproveCharterIn?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        //if (item.UserType.Equals("CRUDE"))
                        //{
                        //    urlPage = "BunkerPreDuePurchase.aspx";
                        //}
                        //else
                        //{
                        //    urlPage = "BunkerPreDuePurchaseCMMT.aspx";
                        //}
                        MCCTypeSS = item.UserType.Encrypt();
                        urlPage = item.UserType == ConstantPrm.SYSTEMTYPE.CRUDE ? "CharterIn" : "CharterIn/CharterInCMMT";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt() + "&Type=" + MCCTypeSS;
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        [HttpGet]
        public ActionResult NewCharterInCMMT(CharterInCMMTViewModel model)
        {
            CharterInCMMTViewModel ViewModel = new CharterInCMMTViewModel();

            ViewModel.freight_type_list = new List<SelectListItem>();
            ViewModel.freight_type_list.Add(new SelectListItem { Text = "xx", Value = "xxx" });

            return View(ViewModel);
        }

        public ActionResult CharterInCMMT_New()
        {
            CharterInViewModel model;
            if (TempData["CharterIn_Model"] == null)
            {
                model = new CharterInViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                MCCTypeSS = ConstantPrm.SYSTEMTYPE.VESSEL.Encrypt(); //CMMT FOR SURE
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                    MCCTypeSS = Request.QueryString["Type"].ToString();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    ResponseData resData = LoadDataCharterIn_New(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }
                    //if (Request.QueryString["PURNO"] != null && Request.QueryString["isDup"] == null)
                    //{
                    //    ViewBag.PURNO = Request.QueryString["PURNO"].ToString().Decrypt();
                    //}
                    if (Request.QueryString["Reason"] != null)
                    {
                        model.chit_reason = Request.QueryString["Reason"].ToString().Decrypt();
                    }

                    if (resData != null && !(resData.result_code == "1" || resData.result_code == "2"))
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                        TempData["returnPage"] = "../" + returnPage;

                    }
                }
                else
                {
                    model.chit_proposed_for_approve = new ChitProposedForApprove();
                    // model.chit_proposed_for_approve.charter_patry = "Shell Voy 5";
                    // model.chit_proposed_for_approve.laytime = "120 hrs";
                    //model.chit_proposed_for_approve.exten_cost = "50/50 Laden passage between Thaioil and the owner but max 30 K USD to Thaioil";
                }
            }
            else
            {
                model = TempData["CharterIn_Model"] as CharterInViewModel;
                model.chit_reason = TempData["CharterIn_Reason"] as string;
                TempData["CharterIn_Model"] = null;
                TempData["CharterIn_Reason"] = null;
            }

            initializeDropDownList(ref model);

            return View(model);
        }

        private ResponseData LoadDataCharterIn_New(string TransactionID, ref CharterInViewModel model)
        {
            string type = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            //req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CHARTERING });
            req.Req_parameters.P.Add(new P { K = "type", V = type.Decrypt() });
            req.Extra_xml = "";
            MCCTypeSS = type;

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CharterInViewModel>(_model.data_detail);
                if (model.chi_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chi_evaluating.laycan_from) && !string.IsNullOrEmpty(model.chi_evaluating.laycan_to))
                    {
                        model.ev_tempLaycan = model.chi_evaluating.laycan_from + " to " + model.chi_evaluating.laycan_to;
                    }
                    if (!string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chi_proposed_for_approve.laycan_from + " to " + model.chi_proposed_for_approve.laycan_to;
                    }
                }
                if (model.chit_evaluating != null)
                {
                    if (!string.IsNullOrEmpty(model.chit_evaluating.laycan_from) && !string.IsNullOrEmpty(model.chit_evaluating.laycan_to))
                    {
                        model.ev_tempLaycan = model.chit_evaluating.laycan_from + " to " + model.chit_evaluating.laycan_to;
                    }
                    if (!string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_to))
                    {
                        model.pa_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                    }
                }

            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = ((type.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "../" : "") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }
            string _btn = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            model.buttonCode += _btn;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }

            return resData;
        }

        [HttpGet]
        public string getPortListString(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            return new JavaScriptSerializer().Serialize(CharterInServiceModel.getPortJetty(fromType, portType, country, location, name)); ;
        }

        public List<ChitListDropDownKey> getPortList(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            return CharterInServiceModel.getPortJetty(fromType, portType, country, location, name);
        }


        [HttpPost]
        public ActionResult LoadPortTable(string tripNumber)
        {
            CharterInViewModel vm = new CharterInViewModel();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var waitingChartering = context.VW_PCF_WAITING_CHARTERING.Where(a => a.TRIP_NO == tripNumber).ToList();
                    vm.chi_cargo_detail_TripNo = new ChiCargoDetail();
                    vm.chi_cargo_detail_TripNo.chi_load_ports = new List<ChiLoadPort>();
                    foreach (var item in waitingChartering.OrderBy(a => a.ITEM_NO))
                    {
                        var order = item.ITEM_NO.ToString();
                        var index = order.Length;
                        order = order.Substring(0, index - 1);
                        var findMatName = context.MT_MATERIALS.SingleOrDefault(a => a.MET_NUM == item.MAT_NUM);
                        var matName = "";
                        if (findMatName != null)
                        {
                            matName = findMatName.MET_MAT_DES_ENGLISH;
                        }

                        var loadingDate = "";
                        if (item.LOADING_DATE_FROM != null && item.LOADING_DATE_TO != null)
                        {
                            var monthFrom = item.LOADING_DATE_FROM.Value.ToString("MMM");
                            var monthTo = item.LOADING_DATE_TO.Value.ToString("MMM");

                            var dayFrom = item.LOADING_DATE_FROM.Value.ToString("dd");
                            var dayTo = item.LOADING_DATE_TO.Value.ToString("dd");

                            if (monthFrom == monthTo)
                            {
                                if (dayFrom == dayTo)
                                {
                                    loadingDate = dayFrom + " " + monthFrom;
                                }
                                else
                                {
                                    loadingDate = dayFrom + "-" + dayTo + " " + monthFrom;
                                }
                            }
                            else
                            {
                                var loadingDateFrom = item.LOADING_DATE_FROM.Value.ToString("dd MMM");
                                var loadingDateTo = item.LOADING_DATE_TO.Value.ToString("dd MMM");
                                loadingDate = loadingDateFrom + " - " + loadingDateTo;
                            }
                        }

                        vm.chi_cargo_detail_TripNo.chi_load_ports.Add(new ChiLoadPort
                        {
                            order_port = order,
                            portName = item.LOADING_PORT_NAME,
                            port = item.LOADING_PORT_CODE.ToString(),
                            crudeName = matName,
                            crude = item.MAT_NUM,
                            supplierName = item.SUPPLIER_NAME,
                            supplier = item.SUPPLIER_CODE,
                            kbbls = item.KB.ToString(),
                            ktons = item.KT.ToString(),
                            tolerance = item.TOLERANCE,
                            date = loadingDate,
                        });
                    }
                }
                initializeDropDownList(ref vm);
            }
            catch (Exception ex)
            {

            }
            return PartialView("_LoadPort", vm);
        }

        private List<SelectListItem> getOfferBy()
        {
            Setting setting = JSONSetting.getSetting("JSON_FREIGHT");
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.fcl_offer_by.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.fcl_offer_by[i], Value = setting.fcl_offer_by[i] });
            }
            return list;
        }
    }
}