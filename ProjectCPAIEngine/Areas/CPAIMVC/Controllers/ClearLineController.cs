﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class ClearLineController:BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "ClearLine");
        }
        
        [HttpGet]
        public ActionResult Search()
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();         
            ClearLineViewModel model = new ClearLineViewModel();
            //if (TempData["tempClearLine"] != null)
            //{
            //    model = initialModel();
            //    ClearLineViewModel temp_model = (ClearLineViewModel)TempData["tempClearLine"];
            //    model.ClearLine_Search = temp_model.ClearLine_Search;
            //    TempData.Remove("tempClearLine");
            //}
            //else
            //{
                model = initialModel("C");
            //}

            //serviceModel.setInitail_ddl(ref model);
            model.Mode = "C";
            model.ClearLine_Search = null;
            return View("Search", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            pModel.Mode = "C";
            serviceModel.Search(ref pModel);
            ClearLineViewModel model = initialModel();
            model.ClearLine_Search = pModel.ClearLine_Search;
            //TempData["tempClearLine"] = pModel;
            return View(model);
            //return RedirectToAction("Search",pModel);
        }


        [HttpGet]
        public ActionResult AddEdit(string pTripNo)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ClearLineViewModel model = initialModel();

            if (String.IsNullOrEmpty(pTripNo))
            {
                //Add new
                model.ClearLineDetail.sTripNo = "";
                model.ClearLineDetail.sPriceStatus = "";
                serviceModel.Add(ref model);
            }
            else
            {
                //Edit
                model.ClearLineDetail.sTripNo = pTripNo;
                model.ClearLineDetail.sPriceStatus = "";
                serviceModel.Edit(ref model);
            }

            serviceModel.setInitail_ddl(ref model);

            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddEdit")]
        public ActionResult AddEdit()
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ClearLineViewModel model = initialModel();
            serviceModel.Add(ref model);
            model.Mode = "C";
            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(FormCollection frm, ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ReturnValue rtn = new ReturnValue();

            rtn = serviceModel.SaveData(ref pModel);
            TempData["Mode"] = pModel.Mode;
            if (rtn != null)
            {
                TempData["ClearLine"] = pModel;
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                
            }
            else
            {
                TempData["ClearLine"] = pModel;
                TempData["ReponseMessage"] = null;
                TempData["ReponseStatus"] = null;                
            }

            //serviceModel.setInitail_ddl(ref pModel);

            return RedirectToAction("Save" ,"ClearLine");
        }

        [HttpGet]
        public ActionResult Save()
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            var mode = TempData["Mode"] != null ? TempData["Mode"].ToString() : "C";
            ClearLineViewModel model = initialModel(mode);
            
            if (TempData["ClearLine"] != null)
            {
                ClearLineViewModel temp_model = (ClearLineViewModel)TempData["ClearLine"];
                model.ClearLineDetail = temp_model.ClearLineDetail;
                TempData.Remove("ClearLine");
            }
            model.Mode = mode;
            return View("AddEdit", model);
        }

        public ClearLineViewModel initialModel(string mode = "C")
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ClearLineViewModel model = new ClearLineViewModel();

            model.ClearLine_Search = new ClearLineViewModel_Search();
            model.ClearLine_Search.SearchData = new List<ClearLineViewModel_SearchData>();

            model.ClearLine_Search.sTripNo = "";
            model.ClearLineDetail = new ClearLineDetailViewModel();
            model.ClearLineDetail.sTripNo = "";
            model.ClearLineDetail.sPriceStatus = "";


            model.ddl_Vessel = ClearLineServiceModel.getVehicle("PRODUCT", true, "");
            model.json_Vessel = ClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Vessel);
            model.ddl_Crude = (mode=="C")? ClearLineServiceModel.getMaterialClearLine():ClearLineServiceModel.getMaterialThroughput();
            model.json_Crude = ClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Crude);
            model.ddl_Customer = ClearLineServiceModel.getCustomer();
            model.json_Customer = ClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Customer);

            model.ddl_Plant = new List<SelectListItem>();
            model.ddl_Plant = ClearLineServiceModel.getPlant("1100,1400");

            model.ddl_Unit = new List<SelectListItem>();
            model.ddl_Unit = ClearLineServiceModel.getUnit();

            model.ddl_UnitPrice = new List<SelectListItem>();
            model.ddl_UnitPrice = ClearLineServiceModel.getUnitPrice();

            model.ddl_UnitTotal = new List<SelectListItem>();
            model.ddl_UnitTotal = ClearLineServiceModel.getUnitTotal();

            model.ddl_PriceStatus = new List<SelectListItem>();
            model.ddl_PriceStatus = ClearLineServiceModel.getPriceStatus();


            model.ddl_UnitINV = new List<SelectListItem>();
            model.ddl_UnitINV = ClearLineServiceModel.getUnitINV();

            model.ddl_SaleUnitType = new List<SelectListItem>();
            model.ddl_SaleUnitType = ClearLineServiceModel.getSaleUnitType();

            model.ddl_InvoiceFigureType = new List<SelectListItem>();
            model.ddl_InvoiceFigureType = ClearLineServiceModel.getInvoiceFigureType();

            model.ddl_Company = new List<SelectListItem>();
            model.ddl_Company = DropdownServiceModel.getCompany();
            return model;
        }

        //[HttpGet]
        //public ActionResult Create(SOClearLineViewModel pModel, string pTripNo)
        //{
        //    ClearLineViewModel serviceModel = new ClearLineViewModel();
        //    //SOClearLineViewModel model = new SOClearLineViewModel();

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(FormCollection frm, ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ReturnValue rtn = new ReturnValue();
       

            #region "For Use"

            #endregion

            //For Test
            rtn = serviceModel.CreateSO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
            TempData["tempSOClearLine"] = pModel;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["ReponseMessage"] = rtn.Message;
            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Update")]
        public ActionResult Update(FormCollection frm, ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();

            //var selectedTripNo = frm["SelectedTripNo"].Split(',');
            //for (var k = 0; k < selectedTripNo.Length - 1; k++)
            //{
            //    if (!String.IsNullOrEmpty(selectedTripNo[k]))
            //    {
            //        //pModel.selectedTripNo = selectedTripNo[k];
            //        break;
            //    }
            //}

            #region "For Use"
            //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
            //{
            //    if (pModel.SOClearLine_Search.SearchData != null)
            //    {
            //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
            //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
            //        {
            //            serviceModel.UpdateSO(ref pModel);
            //        }
            //    }
            //}
            #endregion

            //For Test
            serviceModel.UpdateSO(ref pModel);


            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
            TempData["tempSOClearLine"] = pModel;

            return View("Search", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Cancel")]
        public ActionResult Cancel(FormCollection frm, ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();

            //var selectedTripNo = frm["SelectedTripNo"].Split(',');
            //for (var k = 0; k < selectedTripNo.Length - 1; k++)
            //{
            //    if (!String.IsNullOrEmpty(selectedTripNo[k]))
            //    {
            //        //pModel.selectedTripNo = selectedTripNo[k];
            //        break;
            //    }
            //}
            #region "For Use"
            //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
            //{
            //    if (pModel.SOClearLine_Search.SearchData != null)
            //    {
            //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
            //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
            //        {
            //            serviceModel.CancelSO(ref pModel);
            //        }
            //    }
            //}
            #endregion

            //For Test
            serviceModel.CancelSO(ref pModel);

            serviceModel.setInitail_ddl(ref pModel);

            if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
            TempData["tempSOClearLine"] = pModel;

            return View("Search", pModel);
        }


        /// <summary>
        /// Search for Through Put Fee Transaction.       
        /// </summary>
        /// <returns>model</returns>
        public ActionResult SearchThroughput()
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ClearLineViewModel model = new ClearLineViewModel();
            model = initialModel("T");
            model.Mode = "T";
            return View("Search", model);
        }



        [HttpPost]
        [MultipleButton(Name = "action", Argument = "SearchThroughput")]
        public ActionResult SearchThroughput(ClearLineViewModel pModel)
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            pModel.Mode = "T";
            serviceModel.Search(ref pModel);
            ClearLineViewModel model = initialModel();
            model.ClearLine_Search = pModel.ClearLine_Search;
            model.Mode = pModel.Mode;            
            return View("Search", model);           
        }

        /// <summary>
        /// Call page for add Through Put Fee Transaction.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddThroughput")]
        public ActionResult AddThroughput()
        {
            ClearLineServiceModel serviceModel = new ClearLineServiceModel();
            ClearLineViewModel model = initialModel("T");
            //serviceModel.Add(ref model);
            model.Mode = "T";
            return View("AddEdit", model);
        }
    }
}