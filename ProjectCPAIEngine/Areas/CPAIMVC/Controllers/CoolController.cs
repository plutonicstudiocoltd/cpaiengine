﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.Entity;
using System.IO;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALMaster;
using OfficeOpenXml;
using Excel;
using System.Data;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Xml.Linq;
using System.Xml;
using System.Net;
using System.Drawing;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL;
using com.pttict.engine.dal.Entity;
using System.Globalization;
using System.Text.RegularExpressions;
using OfficeOpenXml.Drawing.Chart;
using System.Threading;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public partial class CoolController : BaseController
    {
        public List<ButtonAction> ButtonListCool
        {
            get
            {
                if (Session["ButtonListCool"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListCool"];
            }
            set { Session["ButtonListCool"] = value; }
        }
        ShareFn _FN = new ShareFn();
        const string returnPage = "/web/MainBoards.aspx";
        const string JSON_COOL_CRUDE = "JSON_COOL_CRUDE";

        public string temp_tranID
        {
            get
            {
                if (Session["TranIDCool"] == null) return null;
                else return (string)Session["TranIDCool"];
            }
            set { Session["TranIDCool"] = value; }
        }

        public ActionResult Index()
        {
            CoolViewModel model;
            if (TempData["CoolViewModel"] == null)
            {
                model = new CoolViewModel();

                model.pageMode = "EDIT"; //PageMode is EDIT, EDIT_REASON or READ
                model.buttonMode = "AUTO"; //ButtonMode is AUTO or MANUAL
                if (Request.QueryString["Type"] != null)
                {
                    ViewBag.Type = Request.QueryString["Type"].ToString().Decrypt();
                }
                if (Request.QueryString["TranID"] != null)
                {
                    string tranID = Request.QueryString["TranID"].ToString().Decrypt();
                    temp_tranID = tranID;
                    ResponseData resData = LoadDataCool(tranID, ref model);
                    if (Request.QueryString["isDup"] == null)
                    {
                        model.taskID = tranID;
                    }

                    if (resData != null)
                    {
                        if (!(resData.result_code == "1" || resData.result_code == "2"))
                        {
                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
                        }
                    }
                    if (model.data == null)
                    {
                        if (!string.IsNullOrEmpty(lbUserID))
                        {
                            var units = CoolServiceModel.getUserGroup(lbUserID);
                            if (units != null && units.Any())
                            {
                                Const.User.UserGroup = string.Join("/", units);
                            }
                        }

                        model.data = new Model.CooData();
                        model.data.name = Const.User.UserName;
                        model.data.area_unit = Const.User.UserGroup;
                        model.data.workflow_status = ConstantPrm.ACTION.DRAFT;
                        model.data.date_purchase = DateTime.Now.ToString(CoolViewModel.FORMAT_DATETIME);
                        model.data.approval_date = DateTime.Now.AddDays(Convert.ToDouble(getApprovalDay())).ToString(CoolViewModel.FORMAT_DATETIME);
                        model.data.type_of_cam = "N";
                    }
                    if (model.experts == null)
                    {
                    }
                }
                else
                {
                    if (model.data == null)
                    {
                        if (!string.IsNullOrEmpty(lbUserID))
                        {
                            var units = CoolServiceModel.getUserGroup(lbUserID);
                            if (units != null && units.Any())
                            {
                                Const.User.UserGroup = string.Join("/", units);
                            }
                        }

                        model.data = new Model.CooData();
                        model.data.name = Const.User.UserName;
                        model.data.area_unit = Const.User.UserGroup;
                        model.data.workflow_status = ConstantPrm.ACTION.DRAFT;
                        model.data.date_purchase = DateTime.Now.ToString(CoolViewModel.FORMAT_DATETIME);
                        model.data.approval_date = DateTime.Now.AddDays(Convert.ToDouble(getApprovalDay())).ToString(CoolViewModel.FORMAT_DATETIME);
                        model.data.type_of_cam = "N";
                    }
                    if (model.experts == null)
                    {
                    }
                }
            }
            else
            {
                model = TempData["CoolViewModel"] as CoolViewModel;
                TempData["CoolViewModel"] = null;
            }

            initializeDropDownList(ref model);
            initializeExperts(ref model);
            ModelState.Clear();
            return View(model);
        }

        public ActionResult Search()
        {
            CoolSearchViewModel model;
            if (TempData["CoolSearchViewModel"] == null)
            {
                model = new CoolSearchViewModel();
                if (!string.IsNullOrEmpty(lbUserID))
                {
                    var units = CoolServiceModel.getUserGroup(lbUserID);
                    if (units != null && units.Any())
                    {
                        Const.User.UserGroup = string.Join("|", units);
                        if (units.Contains("SCEP"))
                        {
                            model.pageMode = "EDIT";
                        }
                        else
                        {
                            model.pageMode = "EDIT_REASON";
                        }
                    }
                }
                List_Cooltrx res = SearchCoolData();
                if (res != null)
                    model.CoolTransaction = res.CoolTransaction.OrderByDescending(x => x.transaction_id).ThenBy(x => x.products).ToList();

                model.CoolGroupTransaction = new List<CoolGroupEncrypt>();

                if (model.CoolTransaction != null)
                {
                    foreach (var item in model.CoolTransaction)
                    {
                        // ENCRYPTE ITEM TRANSACTION //
                        item.date_purchase = _FN.ConvertDateTimeFormatBackFormat(item.date_purchase, "MMMM dd yyyy");
                        item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                        item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
                        item.Purchase_no_Encrypted = item.purchase_no.Encrypt();
                        item.reason_Encrypted = item.reason.Encrypt();

                        // SPLIT SUPPORT DOC FILE BY TYPE //
                        item.support_doc_file1 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "SGSi");
                        item.support_doc_file2 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Process Comment");
                        item.support_doc_file3 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Lab Result");
                        item.support_doc_file4 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Process Experience");
                        item.support_doc_file5 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "TLB");
                        item.support_doc_file6 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Etc.");
                        item.support_doc_file_ext1 = GetFileSupportingDocumentByExtension(item.support_doc_file1);
                        item.support_doc_file_ext2 = GetFileSupportingDocumentByExtension(item.support_doc_file2);
                        item.support_doc_file_ext3 = GetFileSupportingDocumentByExtension(item.support_doc_file3);
                        item.support_doc_file_ext4 = GetFileSupportingDocumentByExtension(item.support_doc_file4);
                        item.support_doc_file_ext5 = GetFileSupportingDocumentByExtension(item.support_doc_file5);
                        item.support_doc_file_ext6 = GetFileSupportingDocumentByExtension(item.support_doc_file6);
                        item.draft_cam_file = GetFileNameByPath(item.draft_cam_file);
                        item.final_cam_file = GetFileNameByPath(item.final_cam_file);
                        // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                        if (!string.IsNullOrEmpty(item.final_cam_file))
                        {
                            item.draft_cam_file = string.Empty;
                        }
                        // ADD ITEM INTO EXIST GROUP TRANSACTION //
                        if (model.CoolGroupTransaction.Any(i => i.products == item.products && i.origin == item.origin))
                        {
                            model.CoolGroupTransaction.Where(i => i.products == item.products && i.origin == item.origin).First().CoolTransaction.Add(item);
                        }
                        // CREATE NEW GROUP TRANSACTION FROM ITEM //
                        else
                        {
                            model.CoolGroupTransaction.Add(new CoolGroupEncrypt()
                            {
                                assay_date = item.assay_date,
                                assay_file = item.assay_file,
                                assay_from = item.assay_from,
                                assay_ref = item.assay_ref,
                                categories = item.categories,
                                characteristic = item.characteristic,
                                create_by = item.create_by,
                                date_purchase = item.date_purchase,
                                density = item.density,
                                draft_cam_file = item.draft_cam_file,
                                final_cam_file = item.final_cam_file,
                                kerogen = item.kerogen,
                                maturity = item.maturity,
                                origin = item.origin,
                                products = item.products,
                                purchase_no = item.purchase_no,
                                reason = item.reason,
                                remark = item.remark,
                                req_transaction_id = item.req_transaction_id,
                                status = item.status,
                                status_description = item.status_description,
                                support_doc_file = item.support_doc_file,
                                support_doc_file1 = item.support_doc_file1,
                                support_doc_file2 = item.support_doc_file2,
                                support_doc_file3 = item.support_doc_file3,
                                support_doc_file4 = item.support_doc_file4,
                                support_doc_file5 = item.support_doc_file5,
                                support_doc_file6 = item.support_doc_file6,
                                support_doc_file_ext1 = item.support_doc_file_ext1,
                                support_doc_file_ext2 = item.support_doc_file_ext2,
                                support_doc_file_ext3 = item.support_doc_file_ext3,
                                support_doc_file_ext4 = item.support_doc_file_ext4,
                                support_doc_file_ext5 = item.support_doc_file_ext5,
                                support_doc_file_ext6 = item.support_doc_file_ext6,

                                total_acid_number = item.total_acid_number,
                                total_sulphur = item.total_sulphur,
                                transaction_id = item.transaction_id,
                                type = item.type,
                                Transaction_id_Encrypted = item.Transaction_id_Encrypted,
                                Req_transaction_id_Encrypted = item.Req_transaction_id_Encrypted,
                                Purchase_no_Encrypted = item.Purchase_no_Encrypted,
                                reason_Encrypted = item.reason_Encrypted,
                                CoolTransaction = new List<CoolEncrypt>()
                            });
                        }
                    }
                }
            }
            else
            {
                model = TempData["CoolSearchViewModel"] as CoolSearchViewModel;
                TempData["CoolSearchViewModel"] = null;
            }
            initializeSearch(ref model);
            List<string> unitGroup = CoolServiceModel.getUserGroup(lbUserID);

            if (unitGroup.Contains("EXPERT"))
            {
                ViewBag.unitGroup = "EXPERT";
            }
            else if (unitGroup.Contains("EXPERT_SH"))
            {
                ViewBag.unitGroup = "EXPERT_SH";
            }

            return View(model);
        }

        public ActionResult Tracking()
        {
            CoolTrackingViewModel model;
            if (TempData["CoolTrackingViewModel"] == null)
            {
                model = new CoolTrackingViewModel();
                if (!string.IsNullOrEmpty(lbUserID))
                {
                    var units = CoolServiceModel.getUserGroup(lbUserID);
                    if (units != null && units.Any())
                    {
                        Const.User.UserGroup = string.Join("|", units);
                        if (units.Contains("SCEP"))
                        {
                            model.pageMode = "EDIT";
                        }
                        else if (units.Contains("SCEP_SH"))
                        {
                            model.pageMode = "EDIT_REASON";
                        }
                        else
                        {
                            model.pageMode = "READ";
                        }
                    }
                }
            }
            else
            {
                model = TempData["CoolTrackingViewModel"] as CoolTrackingViewModel;
                TempData["CoolTrackingViewModel"] = null;
            }
            List<string> userGroup = CoolServiceModel.getUserGroup(lbUserID);
            TempData["isSCEP"] = userGroup.Contains("SCEP") || userGroup.Contains("SCEP_SH");
            initializeTracking(ref model);
            return View(model);
        }

        public ActionResult CAMComparison()
        {
            CoolCAMComparisonViewModel model;
            if (TempData["CoolCAMComparisonViewModel"] == null)
            {
                model = new CoolCAMComparisonViewModel();
                model.pageMode = "READ";
            }
            else
            {
                model = TempData["CoolCAMComparisonViewModel"] as CoolCAMComparisonViewModel;
                TempData["CoolCAMComparisonViewModel"] = null;
            }
            initializeCAMComparison(ref model);
            return View(model);
        }

        public ActionResult TimeReport()
        {
            CoolTimeReport model = new CoolTimeReport();
            model.file_name = string.Format("TimeReport_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            TempData["isSearch"] = false;
            return View(model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "SearchTimeReport")]
        public ActionResult SearchTimeReport(FormCollection fm, CoolTimeReport model)
        {
            //model.file_path = GenerateTimeReport(model);
            model.coolTimeReportData = GetTimeReport(model);
            TempData["isSearch"] = true;
            return View("TimeReport", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "PrintTimeReport")]
        public ActionResult PrintTimeReport(FormCollection fm, CoolTimeReport model)
        {
            model.file_path = GenerateTimeReport(model, true);
            return View("TimeReport", model);
        }

        public ActionResult Assay(CoolViewModel model)
        {
            model = new CoolViewModel() { data = new CooData() { approval_date = DateTime.Now.ToString() } };
            return View(model);
        }

        public ActionResult GenerateView(string cam_file)
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchResult(FormCollection fm, CoolSearchViewModel model)
        {
            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.COOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;
            string sAssayDate_Start = string.Empty;
            string sAssayDate_End = string.Empty;
            if (!string.IsNullOrEmpty(model.sAssayDate))
            {
                string[] s = model.sAssayDate.Split(new[] { " to " }, StringSplitOptions.None);
                sAssayDate_Start = s[0];
                sAssayDate_End = s[1];
            }

            List_Cooltrx res = SearchCoolData(
                system: system,
                type: type,
                cam_status: model.sCAMStatus,
                assay_ref: model.sAssayRef,
                products: model.sCrudeName,
                origin: model.sCountry,
                categories: model.sCrudeCategories,
                kerogen: model.sCrudeKerogen,
                characteristic: model.sCrudeCharacteristic,
                maturity: model.sCrudeMaturity,
                assay_date_start: sAssayDate_Start,
                assay_date_end: sAssayDate_End
                );

            if (string.IsNullOrEmpty(model.sOrderBy))
            {
                model.CoolTransaction = res.CoolTransaction.OrderByDescending(x => x.transaction_id).ToList();
            }
            else
            {
                if (model.sOrderBy.Contains("Crude Name"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.products).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Country"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.origin).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Assay Reference No."))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.assay_ref).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Assay Date"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => DateTime.ParseExact(x.assay_date, "dd-MMM-yyyy", CultureInfo.InvariantCulture)).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Crude Kerogen Type"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.kerogen).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Crude Characteristic"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.characteristic).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Crude Maturity"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.maturity).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Crude Categories"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.categories).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Density"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.density).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Total Sulphur"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.total_sulphur).ThenByDescending(x => x.transaction_id).ToList();
                }
                else if (model.sOrderBy.Contains("Total Acid Number"))
                {
                    model.CoolTransaction = res.CoolTransaction.OrderBy(x => x.total_acid_number).ThenByDescending(x => x.transaction_id).ToList();
                }
            }

            model.CoolGroupTransaction = new List<CoolGroupEncrypt>();

            if (model.CoolTransaction != null)
            {
                foreach (var item in model.CoolTransaction)
                {
                    // ENCRYPTE ITEM TRANSACTION //
                    item.date_purchase = _FN.ConvertDateTimeFormatBackFormat(item.date_purchase, "MMMM dd yyyy");
                    item.Transaction_id_Encrypted = item.transaction_id.Encrypt();
                    item.Req_transaction_id_Encrypted = item.req_transaction_id.Encrypt();
                    item.Purchase_no_Encrypted = item.purchase_no.Encrypt();
                    item.reason_Encrypted = item.reason.Encrypt();

                    // SPLIT SUPPORT DOC FILE BY TYPE //
                    item.support_doc_file1 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "SGSi");
                    item.support_doc_file2 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Process Comment");
                    item.support_doc_file3 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Lab Result");
                    item.support_doc_file4 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Process Experience");
                    item.support_doc_file5 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "TLB");
                    item.support_doc_file6 = GetFileSupportingDocumentByType(item.support_doc_file, item.experts_support_doc_file, "Etc.");

                    item.support_doc_file_ext1 = GetFileSupportingDocumentByExtension(item.support_doc_file1);
                    item.support_doc_file_ext2 = GetFileSupportingDocumentByExtension(item.support_doc_file2);
                    item.support_doc_file_ext3 = GetFileSupportingDocumentByExtension(item.support_doc_file3);
                    item.support_doc_file_ext4 = GetFileSupportingDocumentByExtension(item.support_doc_file4);
                    item.support_doc_file_ext5 = GetFileSupportingDocumentByExtension(item.support_doc_file5);
                    item.support_doc_file_ext6 = GetFileSupportingDocumentByExtension(item.support_doc_file6);

                    item.draft_cam_file = GetFileNameByPath(item.draft_cam_file);
                    item.final_cam_file = GetFileNameByPath(item.final_cam_file);
                    // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                    if (!string.IsNullOrEmpty(item.final_cam_file))
                    {
                        item.draft_cam_file = string.Empty;
                    }
                    // ADD ITEM INTO EXIST GROUP TRANSACTION //
                    if (model.CoolGroupTransaction.Any(i => i.products == item.products && i.origin == item.origin))
                    {
                        model.CoolGroupTransaction.Where(i => i.products == item.products && i.origin == item.origin).First().CoolTransaction.Add(item);
                    }
                    // CREATE NEW GROUP TRANSACTION FROM ITEM //
                    else
                    {
                        model.CoolGroupTransaction.Add(new CoolGroupEncrypt()
                        {
                            assay_date = item.assay_date,
                            assay_file = item.assay_file,
                            assay_from = item.assay_from,
                            assay_ref = item.assay_ref,
                            categories = item.categories,
                            characteristic = item.characteristic,
                            create_by = item.create_by,
                            date_purchase = item.date_purchase,
                            density = item.density,
                            draft_cam_file = item.draft_cam_file,
                            final_cam_file = item.final_cam_file,
                            kerogen = item.kerogen,
                            maturity = item.maturity,
                            origin = item.origin,
                            products = item.products,
                            purchase_no = item.purchase_no,
                            reason = item.reason,
                            remark = item.remark,
                            req_transaction_id = item.req_transaction_id,
                            status = item.status,
                            status_description = item.status_description,
                            support_doc_file = item.support_doc_file,
                            support_doc_file1 = item.support_doc_file1,
                            support_doc_file2 = item.support_doc_file2,
                            support_doc_file3 = item.support_doc_file3,
                            support_doc_file4 = item.support_doc_file4,
                            support_doc_file5 = item.support_doc_file5,
                            support_doc_file6 = item.support_doc_file6,
                            support_doc_file_ext1 = item.support_doc_file_ext1,
                            support_doc_file_ext2 = item.support_doc_file_ext2,
                            support_doc_file_ext3 = item.support_doc_file_ext3,
                            support_doc_file_ext4 = item.support_doc_file_ext4,
                            support_doc_file_ext5 = item.support_doc_file_ext5,
                            support_doc_file_ext6 = item.support_doc_file_ext6,

                            total_acid_number = item.total_acid_number,
                            total_sulphur = item.total_sulphur,
                            transaction_id = item.transaction_id,
                            type = item.type,
                            Transaction_id_Encrypted = item.Transaction_id_Encrypted,
                            Req_transaction_id_Encrypted = item.Req_transaction_id_Encrypted,
                            Purchase_no_Encrypted = item.Purchase_no_Encrypted,
                            reason_Encrypted = item.reason_Encrypted,
                            CoolTransaction = new List<CoolEncrypt>()
                        });
                    }
                }
            }


            TempData["CoolSearchViewModel"] = model;
            return RedirectToAction("Search", "Cool");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "TrackingResult")]
        public ActionResult TrackingResult(FormCollection fm, CoolTrackingViewModel model)
        {
            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.COOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;
            List_CoolTrackingtrx res = SearchTrackingData(
                system: system,
                type: type,
                purchase_no: model.sPurchaseNo,
                assay_ref: model.sAssayRef,
                products: model.sCrudeName,
                origin: model.sCountry,
                categories: model.sCrudeCategories,
                kerogen: model.sCrudeKerogen,
                characteristic: model.sCrudeCharacteristic,
                maturity: model.sCrudeMaturity,
                areas: model.sArea,
                units: model.sUnit
                );
            model.CoolTrackingTransaction = res.CoolTrackingTransaction.OrderByDescending(x => x.transaction_id).ToList();
            if (model.CoolTrackingTransaction != null)
            {
                for (int i = 0; i < model.CoolTrackingTransaction.Count; i++)
                {
                    model.CoolTrackingTransaction[i].date_purchase = _FN.ConvertDateTimeFormatBackFormat(model.CoolTrackingTransaction[i].date_purchase, "dd-MMM-yyyy");
                    model.CoolTrackingTransaction[i].Transaction_id_Encrypted = model.CoolTrackingTransaction[i].transaction_id.Encrypt();
                    model.CoolTrackingTransaction[i].Req_transaction_id_Encrypted = model.CoolTrackingTransaction[i].req_transaction_id.Encrypt();
                    model.CoolTrackingTransaction[i].Purchase_no_Encrypted = model.CoolTrackingTransaction[i].purchase_no.Encrypt();
                    model.CoolTrackingTransaction[i].reason_Encrypted = model.CoolTrackingTransaction[i].reason.Encrypt();

                    model.CoolTrackingTransaction[i].draft_tracking = GetDraftStatusTracking(model.CoolTrackingTransaction[i].draft_cam_status);
                    model.CoolTrackingTransaction[i].final_tracking = GetFinalStatusTracking(model.CoolTrackingTransaction[i].final_cam_status);
                    model.CoolTrackingTransaction[i].expert_tracking = GetExpertTracking(model.CoolTrackingTransaction[i].expert_units);

                    // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                    if (!string.IsNullOrEmpty(model.CoolTrackingTransaction[i].final_cam_file))
                    {
                        model.CoolTrackingTransaction[i].draft_cam_file = string.Empty;
                    }

                    // SET NOTE WHEN TRANSATION WAS CANCELLED //
                    if (model.CoolTrackingTransaction[i].status != ConstantPrm.ACTION.CANCEL)
                    {
                        model.CoolTrackingTransaction[i].note = string.Empty;
                        model.CoolTrackingTransaction[i].updated_by = string.Empty;

                    } else
                    {
                        foreach (var tracking  in model.CoolTrackingTransaction[i].expert_tracking)
                        {
                            tracking.status = ConstantPrm.ACTION.INACTIVE;
                        }
                    }

                    model.CoolTrackingTransaction[i].status_description = CoolServiceModel.getWorkflowStatusDescription(model.CoolTrackingTransaction[i].status);
                }
            }
            TempData["CoolTrackingViewModel"] = model;
            return RedirectToAction("Tracking", "Cool");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "CAMComparisonResult")]
        public ActionResult CAMComparisonResult(FormCollection fm, CoolCAMComparisonViewModel model)
        {
            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.COOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;
            List_CoolCAMComparisontrx res = SearchCAMComparisonData(
                system: system,
                type: type,
                purchase_no: model.sPurchaseNo,
                assay_ref: model.sAssayRef,
                products: model.sCrudeName,
                origin: model.sCountry,
                categories: model.sCrudeCategories,
                kerogen: model.sCrudeKerogen,
                characteristic: model.sCrudeCharacteristic,
                maturity: model.sCrudeMaturity,
                areas: model.sArea,
                units: model.sUnit
                );
            model.CoolCAMComparisonTransaction = res.CoolCAMComparisonTransaction.OrderByDescending(x => x.transaction_id).ToList();
            if (model.CoolCAMComparisonTransaction != null)
            {
                for (int i = 0; i < model.CoolCAMComparisonTransaction.Count; i++)
                {
                    model.CoolCAMComparisonTransaction[i].date_purchase = _FN.ConvertDateTimeFormatBackFormat(model.CoolCAMComparisonTransaction[i].date_purchase, "dd-MMM-yyyy");
                    model.CoolCAMComparisonTransaction[i].Transaction_id_Encrypted = model.CoolCAMComparisonTransaction[i].transaction_id.Encrypt();
                    model.CoolCAMComparisonTransaction[i].Req_transaction_id_Encrypted = model.CoolCAMComparisonTransaction[i].req_transaction_id.Encrypt();
                    model.CoolCAMComparisonTransaction[i].Purchase_no_Encrypted = model.CoolCAMComparisonTransaction[i].purchase_no.Encrypt();
                    model.CoolCAMComparisonTransaction[i].reason_Encrypted = model.CoolCAMComparisonTransaction[i].reason.Encrypt();

                    model.CoolCAMComparisonTransaction[i].draft_tracking = GetDraftStatusTracking(model.CoolCAMComparisonTransaction[i].draft_cam_status);
                    model.CoolCAMComparisonTransaction[i].final_tracking = GetFinalStatusTracking(model.CoolCAMComparisonTransaction[i].final_cam_status);
                    model.CoolCAMComparisonTransaction[i].expert_tracking = GetExpertTracking(model.CoolCAMComparisonTransaction[i].expert_units);

                    // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                    if (!string.IsNullOrEmpty(model.CoolCAMComparisonTransaction[i].final_cam_file))
                    {
                        model.CoolCAMComparisonTransaction[i].draft_cam_file = string.Empty;
                    }

                    // SET NOTE WHEN TRANSATION WAS CANCELLED //
                    if (model.CoolCAMComparisonTransaction[i].status != ConstantPrm.ACTION.CANCEL)
                    {
                        model.CoolCAMComparisonTransaction[i].note = string.Empty;
                        model.CoolCAMComparisonTransaction[i].updated_by = string.Empty;
                        model.CoolCAMComparisonTransaction[i].updated_by = string.Empty;
                    }
                }
            }

            model.CoolCAMComparisonTransaction = model.CoolCAMComparisonTransaction.OrderBy(m => m.products).ThenBy(m => m.assay_ref).ToList() ;
            model.pageMode = "EDIT";
            TempData["CoolCAMComparisonViewModel"] = model;
            return RedirectToAction("CAMComparison", "Cool");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "CAMComparisonExcel")]
        public ActionResult CAMComparisonExcel(FormCollection fm, CoolCAMComparisonViewModel model)
        {
            if (model != null)
            {
                string fileUrl = string.Empty;
                string fileName = string.Empty;
                fileName = GenerateComparison(model, false, out fileUrl);
                if (!string.IsNullOrEmpty(fileName))
                {
                    model.fileName = fileName;
                    model.fileUrl = fileUrl;
                }
                model.pageMode = "VIEW";
            }
            TempData["CoolCAMComparisonViewModel"] = model;
            return RedirectToAction("CAMComparison", "Cool");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "CancelResult")]
        public ActionResult CancelResult(FormCollection form, CoolTrackingViewModel model)
        {
            if (model != null)
            {
                string taskID = model.taskID.Decrypt();
                string reqID = model.reqID.Decrypt();
                DocumentActionStatus _status = DocumentActionStatus.Cancel;
                CoolViewModel modelCancel = new CoolViewModel();
                ResponseData resLoadData = LoadDataCool(taskID, ref modelCancel);
                modelCancel.taskID = taskID;
                modelCancel.reqID = reqID;
                string note = form["hdfNoteAction"];
                ResponseData resSaveData = SaveDataCool(_status: _status, model: modelCancel, note: note);
                TempData["res_message"] = string.IsNullOrEmpty(resSaveData.response_message) ? resSaveData.result_desc : resSaveData.response_message;
            }
            else
            {
                TempData["res_message"] = "System not found record";
            }

            if (string.IsNullOrEmpty(form["hdfNoteAction"]))
            {
                TempData["res_message"] = "Please enter reason for cancel";
            }
            string userGroup = Const.User.UserGroup;
            string system = ConstantPrm.SYSTEM.COOL;
            string type = ConstantPrm.SYSTEMTYPE.CRUDE;
            List_CoolTrackingtrx res = SearchTrackingData(
                system: system,
                type: type,
                purchase_no: model.sPurchaseNo,
                assay_ref: model.sAssayRef,
                products: model.sCrudeName,
                origin: model.sCountry,
                categories: model.sCrudeCategories,
                kerogen: model.sCrudeKerogen,
                characteristic: model.sCrudeCharacteristic,
                maturity: model.sCrudeMaturity,
                areas: model.sArea,
                units: model.sUnit
                );
            model.CoolTrackingTransaction = res.CoolTrackingTransaction.OrderByDescending(x => x.transaction_id).ToList();
            if (model.CoolTrackingTransaction != null)
            {
                for (int i = 0; i < model.CoolTrackingTransaction.Count; i++)
                {
                    model.CoolTrackingTransaction[i].date_purchase = _FN.ConvertDateTimeFormatBackFormat(model.CoolTrackingTransaction[i].date_purchase, "dd-MMM-yyyy");
                    model.CoolTrackingTransaction[i].Transaction_id_Encrypted = model.CoolTrackingTransaction[i].transaction_id.Encrypt();
                    model.CoolTrackingTransaction[i].Req_transaction_id_Encrypted = model.CoolTrackingTransaction[i].req_transaction_id.Encrypt();
                    model.CoolTrackingTransaction[i].Purchase_no_Encrypted = model.CoolTrackingTransaction[i].purchase_no.Encrypt();
                    model.CoolTrackingTransaction[i].reason_Encrypted = model.CoolTrackingTransaction[i].reason.Encrypt();

                    model.CoolTrackingTransaction[i].draft_tracking = GetDraftStatusTracking(model.CoolTrackingTransaction[i].draft_cam_status);
                    model.CoolTrackingTransaction[i].final_tracking = GetFinalStatusTracking(model.CoolTrackingTransaction[i].final_cam_status);
                    model.CoolTrackingTransaction[i].expert_tracking = GetExpertTracking(model.CoolTrackingTransaction[i].expert_units);

                    // SWITCH DOWNLOAD DRAFT/FINAL CAM FILE WHEN FINAL CAM WAS GENERATED //
                    if (!string.IsNullOrEmpty(model.CoolTrackingTransaction[i].final_cam_file))
                    {
                        model.CoolTrackingTransaction[i].draft_cam_file = string.Empty;
                    }

                    // SET NOTE WHEN TRANSATION WAS CANCELLED //
                    if (model.CoolTrackingTransaction[i].status != ConstantPrm.ACTION.CANCEL)
                    {
                        model.CoolTrackingTransaction[i].note = string.Empty;
                        model.CoolTrackingTransaction[i].updated_by = string.Empty;

                    }
                    else
                    {
                        foreach (var tracking in model.CoolTrackingTransaction[i].expert_tracking)
                        {
                            tracking.status = ConstantPrm.ACTION.INACTIVE;
                        }
                    }
                    model.CoolTrackingTransaction[i].status_description = CoolServiceModel.getWorkflowStatusDescription(model.CoolTrackingTransaction[i].status);
                }
            }
            TempData["CoolTrackingViewModel"] = model;
            return RedirectToAction("Tracking", "Cool");
        }



        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Upload")]
        public ActionResult Upload(FormCollection form, CoolViewModel model)
        {
            string current_assay_ref = model.data.assay_ref;
            ResponseData resFile = SaveFileCool(form: ref form, model: ref model);
            initializeAssay(ref model);
            initializeDropDownList(ref model);
            string assay_ref = model.data.assay_ref;
            // RETRIEVE SUPPORTING DOCUMENT WHEN UPLOAD NEW ASSAY REFERENCE //
            if (current_assay_ref != assay_ref)
            {
                initializeSupportingDocument(ref model);
            }

            ModelState.Clear();
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, CoolViewModel model)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            string note = form["hdfNoteAction"];
            ResponseData resFile = SaveFileCool(form: ref form, model: ref model);
            ResponseData resData = SaveDataCool(_status: _status, model: model, note: note);

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string purno = resData.resp_parameters[0].v.Encrypt();
                    string path = string.Format("~/CPAIMVC/Cool?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                    return Redirect(path);
                }
            }

            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, CoolViewModel model)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            string note = form["hdfNoteAction"];
            ResponseData resfile = SaveFileCool(form: ref form, model: ref model);
            ResponseData resData = SaveDataCool(_status: _status, model: model, note: note);
            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string purno = resData.resp_parameters[0].v.Encrypt();
                    string path = string.Format("~/CPAIMVC/Cool?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                    return Redirect(path);
                }
            }
            ModelState.Clear();
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, CoolViewModel model)
        {
            if (ButtonListCool != null)
            {
                var _lstButton = ButtonListCool.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    ResponseData resFile = SaveFileCool(form: ref form, model: ref model);

                    // GENERATE DRAFT CAM: SCEP SECTION HEAD APPROVE
                    if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.APPROVE)
                    {
                        model.data.draft_cam_file = GenerateExcel(model, isOnlyPath: true);
                    }

                    string _xml = _lstButton[0].call_xml;

                    model.note = form["hdfNoteAction"] != null ? form["hdfNoteAction"].ToString() : model.note;
                    string json_fileUpload = string.Empty;
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    var json = new JavaScriptSerializer().Serialize(model);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = "-" });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = json_fileUpload });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2" || resData.result_code == "10000033")
                        {
                            // RE-GENERATE DRAFT CAM: SCEP SECTION HEAD APPROVE
                            if (_lstButton[0].name.ToUpper() == ConstantPrm.ACTION.APPROVE)
                            {
                                GenerateExcel(model);
                            }
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string purno = resData.resp_parameters[0].v.Encrypt();
                            string path = string.Format("~/CPAIMVC/Cool?TranID={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, purno);
                            if (resData.result_code == "10000033")
                            {
                                TempData["res_message"] = "This document has been changed by others. Please check it again.";
                            }
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "An error occurred on the system, Please contact the system administrator.";
            }

            TempData["CoolViewModel"] = model;
            return RedirectToAction("Index", "Cool");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, CoolViewModel model)
        {
            return RedirectToAction("Search");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "GenPDF")]
        public ActionResult GenPDF(FormCollection form, CoolViewModel model)
        {
            GenerateExcel(model: model, isDownload: true);
            initializeDropDownList(ref model);
            return View("Index", model);
        }


        [HttpGet]
        public JsonResult getCrudeProcessingUnit(string crude, string date)
        {
            DocumentManagementServiceModel service = new DocumentManagementServiceModel();
            CPUViewModel model = new CPUViewModel();
            service.getCPUState(ref model, crude, date);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region DataServices
        private ResponseData LoadDataCool(string TransactionID, ref CoolViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000043;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "user_group", V = Const.User.UserGroup });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CoolViewModel>(_model.data_detail);
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = JSONSetting.getGlobalConfig("ROOT_URL") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }
            else
            {
                if (Request.QueryString["isDup"] == null)
                {
                    model.buttonMode = "MANUAL";
                    model.pageMode = "READ";
                }
                else
                {
                    model.buttonMode = "AUTO";
                    model.pageMode = "EDIT";
                }
            }

            string _btnPrint = "<input type='submit' id='btnPrint' value='PRINT' class='" + _FN.GetDefaultButtonCSS("btnPrint") + "' name='action:GenPDF' onclick=\"$(\'form\').attr(\'target\', \'_blank\');\" />";
            string _btnBack = "<input type='submit' id='btnBack' value='BACK' class='" + _FN.GetDefaultButtonCSS("btnBack") + "' name='action:Back' onclick=\"$(\'form\').attr(\'target\', \'_self\');\" />";

            //if (model.data.workflow_status != ConstantPrm.ACTION.DRAFT && model.data.workflow_status != ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM)
            //{
            //    model.buttonCode += _btnPrint;
            //}
            if (!string.IsNullOrEmpty(model.data.created_by) && model.data.created_by.ToLower() != "migrate")
            {
                model.buttonCode += _btnPrint;
            }
            model.buttonCode += _btnBack;

            if (resData.resp_parameters.Count > 0 && Request.QueryString["isDup"] == null)
            {
                model.purno = resData.resp_parameters[0].v;
            }

            return resData;
        }

        private ResponseData SaveDataCool(DocumentActionStatus _status, CoolViewModel model, string note = "", string json_fileUpload = "")
        {
            string CurrentAction = "";
            string NextState = "";
            string StateName = "CPAIVerifyRequireInputContinueState";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.APPROVE_1; NextState = ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM; }
            else if (DocumentActionStatus.Cancel == _status) { CurrentAction = ConstantPrm.ACTION.CANCEL; NextState = ConstantPrm.ACTION.CANCEL; }

            CooRootObject obj = new CooRootObject();

            obj.data = model.data;
            obj.experts = model.experts;
            obj.approve_items = model.approve_items;
            obj.comment_draft_cam = model.comment_draft_cam;
            obj.comment_final_cam = model.comment_final_cam;
            obj.support_doc_file = model.support_doc_file;
            obj.note = note;

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000040;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            if (CurrentAction == ConstantPrm.ACTION.CANCEL)
            {
                req.req_transaction_id = model.reqID;
                req.state_name = StateName;
            }
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.COOL });
            req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.req_parameters.p.Add(new p { k = "attach_items", v = json_fileUpload });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            resData = service.CallService(req);
            return resData;
        }

        private ResponseData SaveFileCool(ref FormCollection form, ref CoolViewModel model)
        {
            ResponseData resFile = new ResponseData();
            try
            {
                if (Request.Files != null && Request.Files.Count > 0)
                {
                    HttpPostedFileBase fileAssay = Request.Files["fileAssay"];
                    if (fileAssay != null && fileAssay.ContentLength > 0)
                    {
                        model.data.assay_file = UploadFile(fileAssay, "ASSAY");
                    }

                    HttpPostedFileBase fileCAM = Request.Files["fileCAM"];
                    if (fileCAM != null && fileCAM.ContentLength > 0)
                    {
                        model.data.cam_file = UploadFile(fileCAM, "SPIRAL");
                    }
                }
                if (!string.IsNullOrEmpty(form["fileSupportingDocument"]))
                {
                    model.support_doc_file = form["fileSupportingDocument"];
                }
            }
            catch (Exception ex)
            {
                resFile.response_message = ex.Message;
            }
            return resFile;
        }

        private List_Cooltrx SearchCoolData(string system = "", string type = "", string action = "", string cam_status = "", string date_purchase = "", string purchase_no = "", string create_by = "", string assay_ref = "", string products = "", string origin = "", string categories = "", string kerogen = "", string characteristic = "", string maturity = "", string assay_date_start = "", string assay_date_end = "")
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000039;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = !string.IsNullOrEmpty(Const.User.UserGroup) ? Const.User.UserGroup.Contains("SCEP") || Const.User.UserGroup.Contains("SCEP_SH") ? COOL_FUNCTION_CODE : Const.User.UserGroup.Contains("EXPERT_SH") ? COOL_EXPERT_SH_FUNCTION_CODE : Const.User.UserGroup.Contains("EXPERT") ? COOL_EXPERT_FUNCTION_CODE : COOL_FUNCTION_CODE : COOL_FUNCTION_CODE });

            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "index_3", V = action });
            req.Req_parameters.P.Add(new P { K = "index_4", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_5", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_6", V = date_purchase });
            req.Req_parameters.P.Add(new P { K = "index_7", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_8", V = purchase_no });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = assay_ref });
            req.Req_parameters.P.Add(new P { K = "index_11", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_12", V = products });
            req.Req_parameters.P.Add(new P { K = "index_13", V = origin });
            req.Req_parameters.P.Add(new P { K = "index_14", V = categories });
            req.Req_parameters.P.Add(new P { K = "index_15", V = kerogen });
            req.Req_parameters.P.Add(new P { K = "index_16", V = characteristic });
            req.Req_parameters.P.Add(new P { K = "index_17", V = maturity });
            req.Req_parameters.P.Add(new P { K = "index_19", V = assay_date_start });
            req.Req_parameters.P.Add(new P { K = "index_20", V = assay_date_end });
            req.Req_parameters.P.Add(new P { K = "tracking", V = "N" });
            req.Req_parameters.P.Add(new P { K = "boarding", V = "N" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_Cooltrx _model = ShareFunction.DeserializeXMLFileToObject<List_Cooltrx>(_DataJson);
            if (_model == null)
            {
                _model = new List_Cooltrx();

            }
            if (_model.CoolTransaction == null)
            {
                _model.CoolTransaction = new List<CoolEncrypt>();
            }
            else
            {
                foreach (var CoolTransaction in _model.CoolTransaction)
                {
                    CoolTransaction.status_description = CoolServiceModel.getWorkflowStatusDescription(CoolTransaction.status);
                }

                if (cam_status == "Draft CAM")
                {
                    _model.CoolTransaction = _model.CoolTransaction.Where(x => x.status != "APPROVED").ToList();
                }
                else if (cam_status == "Final CAM")
                {
                    _model.CoolTransaction = _model.CoolTransaction.Where(x => x.status == "APPROVED").ToList();
                }
            }
            return _model;
        }

        private List_CoolTrackingtrx SearchTrackingData(string system = "", string type = "", string action = "", string date_purchase = "", string purchase_no = "", string create_by = "", string assay_ref = "", string products = "", string origin = "", string categories = "", string kerogen = "", string characteristic = "", string maturity = "", string areas = "", string units = "")
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000039;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = COOL_FUNCTION_CODE });

            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "index_3", V = action });
            req.Req_parameters.P.Add(new P { K = "index_4", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_5", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_6", V = date_purchase });
            req.Req_parameters.P.Add(new P { K = "index_7", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_8", V = purchase_no });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = assay_ref });
            req.Req_parameters.P.Add(new P { K = "index_11", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_12", V = products });
            req.Req_parameters.P.Add(new P { K = "index_13", V = origin });
            req.Req_parameters.P.Add(new P { K = "index_14", V = categories });
            req.Req_parameters.P.Add(new P { K = "index_15", V = kerogen });
            req.Req_parameters.P.Add(new P { K = "index_16", V = characteristic });
            req.Req_parameters.P.Add(new P { K = "index_17", V = maturity });
            req.Req_parameters.P.Add(new P { K = "index_19", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_20", V = "" });
            req.Req_parameters.P.Add(new P { K = "areas", V = areas });
            req.Req_parameters.P.Add(new P { K = "units", V = units });
            req.Req_parameters.P.Add(new P { K = "tracking", V = "T" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_CoolTrackingtrx _model = ShareFunction.DeserializeXMLFileToObject<List_CoolTrackingtrx>(_DataJson);
            if (_model == null) _model = new List_CoolTrackingtrx();
            if (_model.CoolTrackingTransaction == null) _model.CoolTrackingTransaction = new List<CoolTrackingEncrypt>();
            return _model;
        }

        private List_CoolCAMComparisontrx SearchCAMComparisonData(string system = "", string type = "", string action = "", string date_purchase = "", string purchase_no = "", string create_by = "", string assay_ref = "", string products = "", string origin = "", string categories = "", string kerogen = "", string characteristic = "", string maturity = "", string areas = "", string units = "")
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000039;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "function_code", V = COOL_FUNCTION_CODE });

            req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.COOL });
            req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "index_3", V = action });
            req.Req_parameters.P.Add(new P { K = "index_4", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_5", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_6", V = date_purchase });
            req.Req_parameters.P.Add(new P { K = "index_7", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_8", V = purchase_no });
            req.Req_parameters.P.Add(new P { K = "index_9", V = create_by });
            req.Req_parameters.P.Add(new P { K = "index_10", V = assay_ref });
            req.Req_parameters.P.Add(new P { K = "index_11", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_12", V = products });
            req.Req_parameters.P.Add(new P { K = "index_13", V = origin });
            req.Req_parameters.P.Add(new P { K = "index_14", V = categories });
            req.Req_parameters.P.Add(new P { K = "index_15", V = kerogen });
            req.Req_parameters.P.Add(new P { K = "index_16", V = characteristic });
            req.Req_parameters.P.Add(new P { K = "index_17", V = maturity });
            req.Req_parameters.P.Add(new P { K = "index_19", V = "" });
            req.Req_parameters.P.Add(new P { K = "index_20", V = "" });
            req.Req_parameters.P.Add(new P { K = "areas", V = areas });
            req.Req_parameters.P.Add(new P { K = "units", V = units });
            req.Req_parameters.P.Add(new P { K = "tracking", V = "T" });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_CoolCAMComparisontrx _model = ShareFunction.DeserializeXMLFileToObject<List_CoolCAMComparisontrx>(_DataJson);
            if (_model == null) _model = new List_CoolCAMComparisontrx();
            if (_model.CoolCAMComparisonTransaction == null) _model.CoolCAMComparisonTransaction = new List<CoolCAMComparisonEncrypt>();
            else _model.CoolCAMComparisonTransaction = _model.CoolCAMComparisonTransaction.Where(x => x.status == "APPROVED").ToList();
            return _model;
        }

        #endregion

        #region FileServices
        private string UploadFile(HttpPostedFileBase requestFile, string fileType = "")
        {
            if (Request != null)
            {
                var file = requestFile;

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string type = "CRUDE";
                    string FNID = "F10000040";
                    string rootPath = Request.PhysicalApplicationPath;
                    string subPath = "Web/FileUpload/COOL" + (!string.IsNullOrEmpty(fileType) ? ("/" + fileType.ToUpper()).ToString() : "");
                    fileName = string.Format("{0}_{1}_{2}_{3}", type.ToUpper(), DateTime.Now.ToString("yyyyMMddHHmmss"), FNID, Path.GetFileName(file.FileName));
                    if (!Directory.Exists(rootPath + subPath))
                        Directory.CreateDirectory(rootPath + subPath);
                    var path = Path.Combine(Server.MapPath("~/" + subPath), fileName);
                    file.SaveAs(path);
                    return fileName;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        private string GetFileSupportingDocumentJson(string fileSupportingDocument)
        {
            string[] _split = fileSupportingDocument.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return string.Empty;
            }
        }

        private List<string> GetFileSupportingDocumentByType(string fileSupportingDocument, string fileExpertSupportingDocument, string documentType)
        {
            List<string> documentPath = new List<string>();
            try
            {
                // SUPPORTING DOCUMENT //
                if (!string.IsNullOrEmpty(fileSupportingDocument))
                {
                    string[] itemFileSupportingDocuments = fileSupportingDocument.Split('|');
                    if (itemFileSupportingDocuments.Length > 0)
                    {
                        foreach (var itemFileSupportingDocument in itemFileSupportingDocuments)
                        {
                            if (!string.IsNullOrEmpty(itemFileSupportingDocument))
                            {
                                string[] splitVal = itemFileSupportingDocument.Split(':');
                                if (splitVal.Length > 1)
                                {
                                    string filePath = splitVal[0];
                                    string fileType = splitVal[1];
                                    if (fileType == documentType)
                                    {
                                        documentPath.Add(filePath);
                                    }
                                }
                            }
                        }
                    }
                }
                // EXPERTS SUPPORTING DOCUMENT //
                if (!string.IsNullOrEmpty(fileExpertSupportingDocument))
                {
                    string[] itemFileExpertSupportingDocumentGroups = fileExpertSupportingDocument.Split('#');
                    foreach (string itemFileExpertSupportingDocumentGroup in itemFileExpertSupportingDocumentGroups)
                    {
                        string[] itemFileExpertSupportingDocumentAttributes = itemFileExpertSupportingDocumentGroup.Split(';');
                        if (itemFileExpertSupportingDocumentAttributes.Length >= 3)
                        {
                            string itemFileExpertSupportingDocumentArea = itemFileExpertSupportingDocumentAttributes[0];
                            string itemFileExpertSupportingDocumentUnit = itemFileExpertSupportingDocumentAttributes[1];
                            string itemFileExpertSupportingDocumentPath = itemFileExpertSupportingDocumentAttributes[2];

                            if (!string.IsNullOrEmpty(itemFileExpertSupportingDocumentPath))
                            {
                                string fileExpertSupportingDocumentPath = itemFileExpertSupportingDocumentPath;
                                string[] itemFileExpertSupportingDocuments = fileExpertSupportingDocumentPath.Split('|');
                                if (itemFileExpertSupportingDocuments.Length > 0)
                                {
                                    foreach (var itemFileExpertSupportingDocument in itemFileExpertSupportingDocuments)
                                    {
                                        if (!string.IsNullOrEmpty(itemFileExpertSupportingDocument))
                                        {
                                            string[] splitVal = itemFileExpertSupportingDocument.Split(':');
                                            if (splitVal.Length > 1)
                                            {
                                                string filePath = splitVal[0];
                                                string fileType = splitVal[1];
                                                if (fileType == documentType)
                                                {
                                                    documentPath.Add(filePath);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex) { }
            return documentPath;
        }

        private List<string> GetFileSupportingDocumentByExtension(List<string> fileSupportingDocument)
        {
            List<string> fileSupportingDocumentExtension = new List<string>();
            try
            {
                if (fileSupportingDocument != null && fileSupportingDocument.Any())
                {
                    foreach (var itemFileSupportingDocument in fileSupportingDocument)
                    {
                        if (itemFileSupportingDocument.ToLower().Contains(".doc"))
                        {
                            fileSupportingDocumentExtension.Add("doc");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".xls"))
                        {
                            fileSupportingDocumentExtension.Add("xls");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".ppt"))
                        {
                            fileSupportingDocumentExtension.Add("ppt");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".pdf"))
                        {
                            fileSupportingDocumentExtension.Add("pdf");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".msg"))
                        {
                            fileSupportingDocumentExtension.Add("msg");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".jpg"))
                        {
                            fileSupportingDocumentExtension.Add("img");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".jpeg"))
                        {
                            fileSupportingDocumentExtension.Add("img");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".png"))
                        {
                            fileSupportingDocumentExtension.Add("img");
                        }
                        else if (itemFileSupportingDocument.ToLower().Contains(".gif"))
                        {
                            fileSupportingDocumentExtension.Add("img");
                        }
                        else
                        {
                            fileSupportingDocumentExtension.Add("other");
                        }
                    }
                }

            }
            catch (Exception) { }
            return fileSupportingDocumentExtension;
        }

        public static string GetFileNameByPath(string filePath)
        {
            string fileName = filePath;
            try
            {
                string fileName1 = fileName;
                string[] path1 = fileName1.Split('\\');
                if (path1.Length > 0)
                {
                    fileName1 = path1[path1.Length - 1];
                }
                string fileName2 = fileName1;
                string[] path2 = fileName2.Split('/');
                if (path2.Length > 0)
                {
                    fileName2 = path2[path2.Length - 1];
                }
                fileName = fileName2;
            }
            catch (Exception) { }
            return fileName;
        }
        #endregion

        #region Initialization
        private void BindButtonPermission(List<ButtonAction> Button, ref CoolViewModel model)
        {
            if (Request.QueryString["isDup"] == null)
            {
                model.buttonMode = "MANUAL";
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
                {
                    model.pageMode = "READ";
                }
                if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.APPROVE).ToList().Count > 0)
                {
                    model.pageMode = "EDIT_REASON";
                }
                foreach (ButtonAction _button in Button)
                {
                    if (_button.name == "EDIT COMMENT")
                    {
                        if (!string.IsNullOrEmpty(model.data.created_by) && model.data.created_by.ToLower() != "migrate")
                        {
                            string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                            model.buttonCode += _btn;
                        }
                    }
                    else
                    {
                        string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                        model.buttonCode += _btn;
                    }
                }
                model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
                ButtonListCool = Button;
            }
            else
            {
                model.buttonMode = "AUTO";
                model.pageMode = "EDIT";
            }
        }

        private void initializeDropDownList(ref CoolViewModel model)
        {
            model.approvalDay = getApprovalDay();
            model.ddlPriority = getWorkflowPriority();
            model.ddlAssayFrom = getAssayFrom();
            model.ddlCrudeCategories = getCrudeCategories();
            model.ddlCrudeKerogen = getCrudeKerogen();
            model.ddlCrudeCharacteristic = getCrudeCharacteristic();
            model.ddlCrudeMaturity = getCrudeMaturity();
            model.ddlCrudeReference = getCrudeReference(model.data.country, model.data.crude_name);
            model.ddlFileSupportingDocumentType = getFileType();
        }

        private void initializeExperts(ref CoolViewModel model)
        {
            if (model.experts == null)
            {
                model.experts = new CooExperts();
                List<CooArea> areas = CoolServiceModel.getCooArea();
                if (areas != null && areas.Any())
                {
                    model.experts.areas = areas;
                }
            }
        }

        private void initializeAssay(ref CoolViewModel model)
        {
            if (!string.IsNullOrEmpty(model.data.assay_file))
            {
                try
                {
                    string subPath = "~/Web/FileUpload/COOL/Assay";
                    string fileName = model.data.assay_file;
                    string path = Path.Combine(Server.MapPath(subPath), fileName);

                    // FILE STREAM IS EXCAL TYPE //
                    try
                    {
                        WebRequest request = WebRequest.Create(path);
                        using (var response = request.GetResponse())
                        {
                            using (var stream = response.GetResponseStream())
                            {
                                //can't use EPPlus because it doesn't support xls file.
                                //var excelPackage = new ExcelPackage(stream);
                                //ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets.First();

                                string cell_assay_ref = getAssayPosition(AssayPosition.REFERENCE);
                                string cell_crude_name = getAssayPosition(AssayPosition.CRUDE_NAME);
                                string cellOrigin = getAssayPosition(AssayPosition.ORIGIN);
                                string cellDate = getAssayPosition(AssayPosition.ASSAY_DATE);

                                List<SelectListItem> countries = DropdownServiceModel.getCountryCrudePurchase();
                                List<SelectListItem> materials = DropdownServiceModel.getMaterial();

                                //var assay_ref = excelWorksheet.Cells[cell_assay_ref].Value != null ? excelWorksheet.Cells[cell_assay_ref].Text : string.Empty;
                                //var crude_name = excelWorksheet.Cells[cell_crude_name].Value != null ? excelWorksheet.Cells[cell_crude_name].Text : string.Empty;
                                //var crude_id = materials.Where(i => i.Text == crude_name).Any() ? materials.Where(i => i.Text == crude_name).First().Value : string.Empty;
                                //var assay_date = excelWorksheet.Cells[cellDate].Value != null ? DateTime.Parse(excelWorksheet.Cells[cellDate].Text).ToString(CoolViewModel.FORMAT_DATE) : string.Empty;
                                //var country = excelWorksheet.Cells[cellOrigin].Value != null ? excelWorksheet.Cells[cellOrigin].Text : string.Empty;
                                //var origin = countries.Where(i => i.Text == country).Any() ? countries.Where(i => i.Text == country).First().Value : string.Empty;

                                IExcelDataReader reader = null;

                                if (fileName.EndsWith(".xls"))
                                {
                                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                                }
                                else if (fileName.EndsWith(".xlsx"))
                                {
                                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                                }

                                reader.IsFirstRowAsColumnNames = false;

                                DataSet ds = reader.AsDataSet();
                                reader.Close();

                                string assay_ref = "";
                                string crude_name = "";
                                string crude_id = "";
                                string assay_date = "";
                                string country = "";
                                string origin = "";

                                if (ds.Tables.Count > 0)
                                {
                                    System.Data.DataTable dt = ds.Tables[0];
                                    Regex numAlpha = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
                                    Match match = numAlpha.Match(cell_assay_ref);

                                    string alpha = match.Groups["Alpha"].Value;
                                    string num = match.Groups["Numeric"].Value;
                                    int col = ShareFn.ExcelColumnNameToNumber(alpha);
                                    int row = Int32.Parse(num);
                                    assay_ref = dt.Rows[row - 1][col - 1].ToString();

                                    match = numAlpha.Match(cell_crude_name);
                                    alpha = match.Groups["Alpha"].Value;
                                    num = match.Groups["Numeric"].Value;
                                    col = ShareFn.ExcelColumnNameToNumber(alpha);
                                    row = Int32.Parse(num);
                                    crude_name = dt.Rows[row - 1][col - 1].ToString();

                                    match = numAlpha.Match(cellDate);
                                    alpha = match.Groups["Alpha"].Value;
                                    num = match.Groups["Numeric"].Value;
                                    col = ShareFn.ExcelColumnNameToNumber(alpha);
                                    row = Int32.Parse(num);
                                    if (fileName.EndsWith(".xls"))
                                    {
                                        if (DateTime.FromOADate(Double.Parse(dt.Rows[row - 1][col - 1].ToString())).Year < 2000)
                                        {
                                            TempData["res_message"] = "Invalid assay date, please check date again.";
                                            return;
                                        }
                                        else
                                        {
                                            assay_date = DateTime.FromOADate(Double.Parse(dt.Rows[row - 1][col - 1].ToString())).ToString("dd-MMM-yyyy");
                                        }
                                    }
                                    else if (fileName.EndsWith(".xlsx"))
                                    {
                                        if (DateTime.Parse(dt.Rows[row - 1][col - 1].ToString()).Year < 2000)
                                        {
                                            TempData["res_message"] = "Invalid assay date, please check date again.";
                                        }
                                        else
                                        {
                                            assay_date = DateTime.Parse(dt.Rows[row - 1][col - 1].ToString()).ToString("dd-MMM-yyyy");
                                        }
                                    }
                                    

                                    match = numAlpha.Match(cellOrigin);
                                    alpha = match.Groups["Alpha"].Value;
                                    num = match.Groups["Numeric"].Value;
                                    col = ShareFn.ExcelColumnNameToNumber(alpha);
                                    row = Int32.Parse(num);
                                    country = dt.Rows[row - 1][col - 1].ToString();

                                    crude_id = materials.Where(i => i.Text == crude_name).Any() ? materials.Where(i => i.Text == crude_name).First().Value : string.Empty;
                                    origin = countries.Where(i => i.Text == country).Any() ? countries.Where(i => i.Text == country).First().Value : string.Empty;
                                }

                                model.data.crude_id = !string.IsNullOrEmpty(crude_id) ? crude_id : model.data.crude_id;
                                model.data.crude_name = !string.IsNullOrEmpty(crude_name) ? crude_name : model.data.crude_name;
                                model.data.assay_date = !string.IsNullOrEmpty(assay_date) ? assay_date : model.data.assay_date;
                                model.data.country = !string.IsNullOrEmpty(country) ? country : model.data.country;
                                model.data.origin = !string.IsNullOrEmpty(origin) ? origin : model.data.origin;

                                // CHECK EXISTS ASSAY REFERENCE WITH ANOTHER COOL //
                                var purchase_no = model.purno;
                                List<SelectListItem> assays = getAssayReference(purchase_no, assay_ref).ToList();
                                if (assays.Any())
                                {
                                    model.data.assay_id = !string.IsNullOrEmpty(assay_ref) ? assay_ref : model.data.assay_ref;
                                    model.data.assay_ref = string.Empty;
                                    TempData["res_message"] = "Assay reference already use in another transaction.";
                                }
                                else
                                {
                                    model.data.assay_id = string.Empty;
                                    model.data.assay_ref = !string.IsNullOrEmpty(assay_ref) ? assay_ref : model.data.assay_ref;
                                }
                            }
                        }
                    }
                    // FILE STREAM IS HTML TYPE //
                    catch (Exception)
                    {
                        using (StreamReader readStream = new StreamReader(path, System.Text.UTF8Encoding.Default))
                        {
                            string documentContents = readStream.ReadToEnd();
                            if (documentContents.Length > 0)
                            {
                                string document_assay_ref = documentContents.Substring(documentContents.IndexOf(AssayPosition.REFERENCE));
                                string document_crude_name = documentContents.Substring(documentContents.IndexOf(AssayPosition.CRUDE_NAME));
                                string document_origin = documentContents.Substring(documentContents.LastIndexOf(AssayPosition.ORIGIN));
                                string document_assay_date = documentContents.Substring(documentContents.LastIndexOf(AssayPosition.ASSAY_DATE));
                                document_assay_ref = getInnerDocument(document_assay_ref);
                                document_crude_name = getInnerDocument(document_crude_name);
                                document_origin = getInnerDocument(document_origin);
                                document_assay_date = getInnerDocument(document_assay_date);

                                List<SelectListItem> countries = DropdownServiceModel.getCountryCrudePurchase();
                                List<SelectListItem> materials = DropdownServiceModel.getMaterial();

                                string assay_ref = !string.IsNullOrEmpty(document_assay_ref) ? document_assay_ref : string.Empty;
                                string crude_name = !string.IsNullOrEmpty(document_crude_name) ? document_crude_name : string.Empty;
                                string crude_id = materials.Where(i => i.Text == crude_name).Any() ? materials.Where(i => i.Text == crude_name).First().Value : string.Empty;
                                string assay_date = !string.IsNullOrEmpty(document_assay_date) ? DateTime.Parse(document_assay_date).ToString(CoolViewModel.FORMAT_DATE) : string.Empty;
                                string country = !string.IsNullOrEmpty(document_origin) ? document_origin : string.Empty;
                                string origin = countries.Where(i => i.Text == country).Any() ? countries.Where(i => i.Text == country).First().Value : string.Empty;

                                model.data.assay_ref = !string.IsNullOrEmpty(assay_ref) ? assay_ref : model.data.assay_ref;
                                model.data.crude_id = !string.IsNullOrEmpty(crude_id) ? crude_id : model.data.crude_id;
                                model.data.crude_name = !string.IsNullOrEmpty(crude_name) ? crude_name : model.data.crude_name;
                                model.data.assay_date = !string.IsNullOrEmpty(assay_date) ? assay_date : model.data.assay_date;
                                model.data.country = !string.IsNullOrEmpty(country) ? country : model.data.country;
                                model.data.origin = !string.IsNullOrEmpty(origin) ? origin : model.data.origin;

                                //if (string.IsNullOrEmpty(crude_id))
                                //{
                                //    TempData["res_message"] = "Crude name can not be found in system.";
                                //}
                                //else if (string.IsNullOrEmpty(origin))
                                //{
                                //    TempData["res_message"] = "Country can not be found in system.";
                                //}
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    TempData["res_message"] = "Invalid assay format, please check file again.";
                }
            }
        }

        private string getInnerDocument(string text)
        {
            var splitStart = '>';
            var splitEnd = '<';
            var splitLine = '\n';
            int indexLine = 1;
            string[] lines = text.Split(splitLine);
            string line = string.Empty;
            if (lines.Length > indexLine)
            {
                line = lines[indexLine];
                line = line.Substring((line.IndexOf(splitStart) + indexLine), line.LastIndexOf(splitEnd) - (line.IndexOf(splitStart) + indexLine));
            }
            return line;
        }

        private void initializeSearch(ref CoolSearchViewModel model)
        {
            model.crude = getCrudeName();
            model.country = DropdownServiceModel.getCountryCrudePurchase();
            model.crudeCategories = getCrudeCategories();
            model.crudeKerogen = getCrudeKerogen();
            model.crudeCharacteristic = getCrudeCharacteristic();
            model.crudeMaturity = getCrudeMaturity();
            model.camStatus = getCAMStatus();
            model.orderBy = getOrderBy();
        }

        private void initializeTracking(ref CoolTrackingViewModel model)
        {
            model.crude = getCrudeName();
            model.country = DropdownServiceModel.getCountryCrudePurchase();
            model.crudeCategories = getCrudeCategories();
            model.crudeKerogen = getCrudeKerogen();
            model.crudeCharacteristic = getCrudeCharacteristic();
            model.crudeMaturity = getCrudeMaturity();
            model.area = getCooArea();
            model.unit = getCooUnit();
        }

        private void initializeCAMComparison(ref CoolCAMComparisonViewModel model)
        {
            model.country = DropdownServiceModel.getCountryCrudePurchase();
            model.crudeCategories = getCrudeCategories();
            model.crudeKerogen = getCrudeKerogen();
            model.crudeCharacteristic = getCrudeCharacteristic();
            model.crudeMaturity = getCrudeMaturity();
            model.area = getCooArea();
            model.unit = getCooUnit();
        }

        private void initializeTimeReport(ref CoolTimeReport model)
        {

        }

        private void initializeSupportingDocument(ref CoolViewModel model)
        {
            if (model != null && model.data != null)
            {
                string country = model.data.country;
                string crude_name = model.data.crude_name;
                string pur_no = model.purno ?? string.Empty;
                model.support_doc_file = getSupportingDocument(country, crude_name, pur_no);
            }
        }

        #endregion

        #region DropDownListServices
        private string getApprovalDay()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            string day = "0";
            if (setting.approval_day.Any())
            {
                day = setting.approval_day.First();
            }
            return day;
        }

        private string getAssayPosition(string key)
        {
            string value = string.Empty;
            try
            {
                Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
                if (setting.assay_position != null && setting.assay_position.Where(i => i.key.Equals(key)).Any())
                {
                    value = setting.assay_position.Where(i => i.key.Equals(key)).First().value;
                }
            }
            catch (Exception ex)
            {
            }
            return value;
        }

        private string getSupportingDocument(string country = "", string crude_name = "", string pur_no = "")
        {
            string supportingDocument = string.Empty;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    if (!string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(crude_name))
                    {
                        var qDis = (from l in context.COO_DATA
                                    where l.CODA_COUNTRY == country && l.CODA_CRUDE_NAME == crude_name && l.CODA_PURCHASE_NO != pur_no
                                    orderby l.CODA_REQUESTED_DATE descending
                                    select l.COO_SUP_DOC).First();
                        if (qDis != null && qDis.Any())
                        {
                            foreach (var item in qDis.OrderBy(i => i.COSD_ORDER))
                            {
                                supportingDocument += string.Concat(item.COSD_FILE_PATH, ":", item.COSD_FILE_TYPE, "|");
                            }
                        }
                    }
                }
                return supportingDocument;
            }
            catch (Exception)
            {
                return supportingDocument;
            }
        }

        private List<SelectListItem> getAssayReference(string purchase_no = "", string assay_ref = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    if (!string.IsNullOrEmpty(assay_ref))
                    {
                        var qDis = (from l in context.COO_DATA
                                    where l.CODA_ASSAY_REF_NO == assay_ref && l.CODA_PURCHASE_NO != purchase_no
                                    && l.CODA_STATUS != "CANCEL"
                                    select new { Purchase_No = l.CODA_PURCHASE_NO, Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();
                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Purchase_No });
                        }
                    }
                }
                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
        }

        private List<SelectListItem> getCrudeReference(string country = "", string crude_name = "")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    if (!string.IsNullOrEmpty(country) && !string.IsNullOrEmpty(crude_name))
                    {
                        var qDis = (from l in context.COO_DATA
                                    where l.CODA_COUNTRY == country && l.CODA_CRUDE_NAME == crude_name && l.CODA_STATUS.Equals("APPROVED")
                                    select new { Assay_Ref = l.CODA_ASSAY_REF_NO }).Distinct().ToList();
                        foreach (var item in qDis)
                        {
                            selectList.Add(new SelectListItem { Text = item.Assay_Ref, Value = item.Assay_Ref });
                        }
                    }
                }
                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
        }

        private List<SelectListItem> getCrudeName()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var qDis = (from l in context.COO_DATA
                                where !l.CODA_STATUS.Equals("CANCEL")
                                select new { crude_name = l.CODA_CRUDE_NAME }).Distinct().ToList();
                    qDis = qDis.OrderBy(x => x.crude_name).ToList();
                    foreach (var item in qDis)
                    {
                        selectList.Add(new SelectListItem { Text = item.crude_name, Value = item.crude_name });
                    }
                }
                return selectList;
            }
            catch (Exception)
            {
                return selectList;
            }
        }

        private List<SelectListItem> getWorkflowPriority()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.workflow_priority.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.workflow_priority[i], Value = setting.workflow_priority[i] });
            }
            return list;
        }

        private List<SelectListItem> getCrudeCategories()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.crude_categories.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_categories[i], Value = setting.crude_categories[i] });
            }
            return list;
        }

        private List<SelectListItem> getCrudeKerogen()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.crude_kerogen.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_kerogen[i], Value = setting.crude_kerogen[i] });
            }
            return list;
        }

        private List<SelectListItem> getCrudeCharacteristic()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.crude_characteristic.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_characteristic[i], Value = setting.crude_characteristic[i] });
            }
            return list;
        }

        private List<SelectListItem> getCrudeMaturity()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.crude_maturity.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.crude_maturity[i], Value = setting.crude_maturity[i] });
            }
            return list;
        }

        private List<SelectListItem> getAssayFrom()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.assay_from.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.assay_from[i], Value = setting.assay_from[i] });
            }
            return list;
        }

        private List<SelectListItem> getFileType()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.file_type.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.file_type[i], Value = setting.file_type[i] });
            }
            return list;
        }

        private List<SelectListItem> getCAMStatus()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.cam_status.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.cam_status[i], Value = setting.cam_status[i] });
            }
            return list;
        }

        private List<SelectListItem> getOrderBy()
        {
            Setting setting = JSONSetting.getSetting(JSON_COOL_CRUDE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.order_by.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.order_by[i], Value = setting.order_by[i] });
            }
            return list;
        }

        private List<SelectListItem> getUser()
        {
            List<SelectListItem> userList = new List<SelectListItem>();
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.COOL);
            msdata = msdata.OrderBy(x => x).ToList();
            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            return userList;
        }

        private List<SelectListItem> getCooArea()
        {
            List<SelectListItem> areaList = new List<SelectListItem>();
            var areaData = CoolServiceModel.getCooArea();
            foreach (var a in areaData)
            {
                areaList.Add(new SelectListItem { Text = a.name, Value = a.name });
            }
            return areaList;
        }

        private List<SelectListItem> getCooUnit()
        {
            List<SelectListItem> unitList = new List<SelectListItem>();
            var areaData = CoolServiceModel.getCooArea();
            if (areaData != null && areaData.Any())
            {
                foreach (var area in areaData)
                {
                    if (area.units != null && area.units.Any())
                    {
                        foreach (var unit in area.units)
                        {
                            unitList.Add(new SelectListItem { Text = unit.name, Value = unit.name, Group = new SelectListGroup() { Name = area.name } });
                        }
                    }
                }
            }
            return unitList;
        }
        #endregion

        #region ExcelServices

        public string GenerateComparison(CoolCAMComparisonViewModel model, bool isDownload, out string file_url)
        {
            var row = model.rowData.Split(',');
            List<Crude> listCrude = new List<Crude>();
            if (row.Count() != 0 && row != null)
            {
                foreach (var item in row)
                {
                    Crude crude = new Crude();
                    var crude_data = item.Split('|');
                    var crudeName = crude_data[0];
                    var crudeCountry = crude_data[1];
                    var crudeAssayRef = crude_data[2];

                    crude.crude_name = crudeName;
                    crude.crude_country = crudeCountry;
                    crude.crude_assay_ref_no = crudeAssayRef;
                    listCrude.Add(crude);
                }
            }
            model.crude = listCrude;
            CoolServiceModel serviceModel = new CoolServiceModel();
            string file_name = string.Format("CAM_{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string virture_path = Path.Combine("Web", "FileUpload", "COOL", file_name);
            string physical_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, virture_path);
            file_url = virture_path;
            List<CAMTemplate> templates = serviceModel.getCAMComparisonTemplate(ref model);
            if (templates != null)
            {
                FileInfo excelFile = new FileInfo(physical_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("CAM Template");


                    foreach (var template in templates)
                    {
                        // SET PANEL FREEZE
                        if (!string.IsNullOrEmpty(template.freeze_col) && !string.IsNullOrEmpty(template.freeze_row))
                        {
                            var start_row_freeze = Convert.ToInt32(model.hide_row_start);
                            var end_row_freeze = Convert.ToInt32(model.hide_row_end);
                            var num_row_freeze = (end_row_freeze - start_row_freeze) + 1;

                            int rowNumber = int.Parse(template.freeze_row) - num_row_freeze;
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.freeze_col);
                            ws.View.FreezePanes(rowNumber, columnNumber);
                        }

                        //SET COLUMN STYLES
                        if (template.column != null && template.row == null)
                        {
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.column);

                            // SET COLUMN WIDTH
                            if (!string.IsNullOrEmpty(template.width))
                            {
                                double columnWidth = double.Parse(template.width);
                                ws.Column(columnNumber).Width = columnWidth;
                            }

                            // SET COLUMN HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Column(columnNumber).Hidden = template.hidden == "Y" ? true : false;
                            }
                        }
                        //SET ROW STYLES
                        else if (template.column == null && template.row != null)
                        {
                            int rowNumber = int.Parse(template.row);

                            // SET ROW HEIGHT 
                            if (!string.IsNullOrEmpty(template.row))
                            {
                                double rowHeight = 21; //double.Parse(template.height);
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }

                            // SET ROW HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                //ws.Row(rowNumber).Hidden = template.hidden == "Y" ? true : false;
                                double rowHeight = 0;
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }


                        }
                        else if (template.column != null && template.row != null)
                        {
                            string[] Columns = template.column.Split('|');
                            string[] Rows = template.row.Split('|');
                            string Cell = Columns.First() + Rows.First() + ':' + Columns.Last() + Rows.Last();
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(Columns.First());
                            int rowNumber = int.Parse(Rows.First());

                            #region CELL VALUE

                            // SET CELL VALUE //
                            if (!string.IsNullOrEmpty(template.value))
                            {
                                if (CoolServiceModel.IsNumeric(template.value))
                                {
                                    ws.Cells[Cell].Value = Decimal.Parse(template.value);
                                }
                                else
                                {
                                    ws.Cells[Cell].Value = template.value;
                                }
                            }

                            // SET CELL FORMULA //
                            if (!string.IsNullOrEmpty(template.formula))
                            {
                                ws.Cells[Cell].Formula = template.formula;
                            }

                            // SET CELL MERGE // 
                            if (!string.IsNullOrEmpty(template.merge)) //if ((!string.IsNullOrEmpty(Columns.Last()) || !string.IsNullOrEmpty(Rows.Last())) && !string.IsNullOrEmpty(template.merge))
                            {
                                try
                                {
                                    ws.Cells[Cell].Merge = template.merge == "Y" ? true : false;
                                }
                                catch (Exception ex)
                                {
                                }
                            }

                            // SET CELL BACKGROUND
                            if (!string.IsNullOrEmpty(template.bg_color))
                            {
                                ws.Cells[Cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells[Cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(template.bg_color));
                            }

                            // SET CELL BORDER
                            if (!string.IsNullOrEmpty(template.border_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                if (!string.IsNullOrEmpty(template.border_color) && template.border_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                }

                            }
                            // SET CELL TEXT ROTATION //
                            if (!string.IsNullOrEmpty(template.alignment))
                            {
                                if (template.alignment == "V")
                                    ws.Cells[Cell].Style.TextRotation = 90;
                            }

                            // SET CELL FONT BOLD
                            if (!string.IsNullOrEmpty(template.font_bold))
                            {
                                ws.Cells[Cell].Style.Font.Bold = template.font_bold == "Y" ? true : false;
                            }

                            // SET CELL FONT COLOR
                            if (!string.IsNullOrEmpty(template.font_color))
                            {
                                ws.Cells[Cell].Style.Font.Color.SetColor(ColorTranslator.FromHtml(template.font_color));
                            }

                            // SET CELL FONT SIZE
                            if (!string.IsNullOrEmpty(template.font_size))
                            {
                                ws.Cells[Cell].Style.Font.Size = float.Parse(template.font_size);
                            }

                            // SET CELL NUMBER FORMAT //
                            if (!string.IsNullOrEmpty(template.number_format))
                            {
                                ws.Cells[Cell].Style.Numberformat.Format = template.number_format;
                            }

                            // SET CELL INDENT //
                            if (!string.IsNullOrEmpty(template.indent))
                            {
                                if (template.indent == "Y")
                                    ws.Cells[Cell].Value = "   -" + ws.Cells[Cell].Value;
                            }

                            // SET CELL CENTER
                            if (!string.IsNullOrEmpty(template.center))
                            {
                                if (template.center == "Y")
                                {
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }

                            // SET CELL FONT ITALIC
                            if (!string.IsNullOrEmpty(template.font_italic))
                            {
                                ws.Cells[Cell].Style.Font.Italic = template.font_italic == "Y" ? true : false;
                            }

                            // SET CELL FONT UNDERLINE
                            if (!string.IsNullOrEmpty(template.font_underline))
                            {
                                ws.Cells[Cell].Style.Font.UnderLine = template.font_underline == "Y" ? true : false;
                            }

                            // SET CELL FONT TEXTWRAP
                            if (!string.IsNullOrEmpty(template.font_textwrap))
                            {
                                ws.Cells[Cell].Style.WrapText = template.font_textwrap == "Y" ? true : false;
                            }

                            // SET CELL FONT HORIZONTAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_horizontal_alignment))
                            {
                                if (template.font_horizontal_alignment == "L")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                else if (template.font_horizontal_alignment == "C")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                else if (template.font_horizontal_alignment == "R")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            }

                            // SET CELL FONT VERTICAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_vertical_alignment))
                            {
                                if (template.font_vertical_alignment == "T")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                else if (template.font_vertical_alignment == "M")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                else if (template.font_vertical_alignment == "B")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                            }

                            // SET CELL BORDER TOP STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_top_style))
                            {
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_top_style);
                                if (!string.IsNullOrEmpty(template.border_top_color) && template.border_top_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_top_color));
                                }
                            }

                            // SET CELL BORDER BOTTOM STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_bottom_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_bottom_style);
                                if (!string.IsNullOrEmpty(template.border_bottom_color) && template.border_bottom_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_bottom_color));
                                }
                            }

                            // SET CELL BORDER LEFT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_left_style))
                            {
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_left_style);
                                if (!string.IsNullOrEmpty(template.border_left_color) && template.border_left_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_left_color));
                                }
                            }

                            // SET CELL BORDER RIGHT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_right_style))
                            {
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_right_style);
                                if (!string.IsNullOrEmpty(template.border_right_color) && template.border_right_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_right_color));
                                }
                            }
                            #endregion
                        }
                    }

                    #region VISIBILITY COLUMN AND ROW 
                    if (model != null)
                    {
                        if (!string.IsNullOrEmpty(model.footer_column_start) && !string.IsNullOrEmpty(model.footer_column_end))
                        {
                            var start_col = ShareFn.ExcelColumnNameToNumber(model.footer_column_start);
                            var end_col = ShareFn.ExcelColumnNameToNumber(model.footer_column_end);
                            var num_col = (end_col - start_col) + 1;
                            ws.DeleteColumn(start_col, num_col);

                            ws.Cells[string.Format("{0}1:{0}{1}", model.footer_column_start, model.footer_row_end)].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            ws.Cells[string.Format("{0}1:{0}{1}", model.footer_column_start, model.footer_row_end)].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml("#000000"));

                        }

                        if (!string.IsNullOrEmpty(model.hide_column_start) && !string.IsNullOrEmpty(model.hide_column_end))
                        {
                            var start_col = ShareFn.ExcelColumnNameToNumber(model.hide_column_start);
                            var end_col = ShareFn.ExcelColumnNameToNumber(model.hide_column_end);
                            var num_col = (end_col - start_col) + 1;
                            ws.DeleteColumn(start_col, num_col);
                        }

                        if (!string.IsNullOrEmpty(model.footer_row_start) && !string.IsNullOrEmpty(model.footer_row_end))
                        {
                            var start_row = Convert.ToInt32(model.footer_row_start);
                            var end_row = Convert.ToInt32(model.footer_row_end);
                            var num_row = (end_row - start_row) + 1;
                            ws.DeleteRow(start_row, num_row);
                        }

                        if (!string.IsNullOrEmpty(model.hide_row_start) && !string.IsNullOrEmpty(model.hide_row_end))
                        {
                            var start_row = Convert.ToInt32(model.hide_row_start);
                            var end_row = Convert.ToInt32(model.hide_row_end);
                            var num_row = (end_row - start_row) + 1;
                            ws.DeleteRow(start_row, num_row);
                        }

                    }

                    #endregion

                    package.Save();
                    return physical_path;

                }
            }


            return null;
        }

        public string GenerateExcel(CoolViewModel model, bool isDownload = false, bool isFinal = false, bool isOnlyPath = false)
        {
            string assay_ref = CoolServiceModel.getAssayRefByID(model.taskID);
            string file_name = string.Format("{0}.xlsx", assay_ref);
            string sub_path = isFinal == true ? "FINAL" : "DRAFT";
            string virture_path = Path.Combine("Web", "FileUpload", "COOL", sub_path);
            string physical_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, virture_path);
            string full_path = Path.Combine(physical_path, file_name);

            if (isOnlyPath)
                return Path.Combine(virture_path, file_name);

            if (!Directory.Exists(physical_path))
                Directory.CreateDirectory(physical_path);

            FileInfo current_file = new FileInfo(full_path);
            if (current_file.Exists)
            {
                string backup_file_name = string.Format("{0}_{1}.xlsx", assay_ref, DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                string backup_file_path = Path.Combine(physical_path, backup_file_name);
                current_file.MoveTo(backup_file_path);
            }



            FileInfo excelFile = new FileInfo(full_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                CoolServiceModel serviceModel = new CoolServiceModel();
                List<CAMTemplate> camTemplates = serviceModel.getCAMTemplate(model.taskID, isFinal);
                List<ExpertTemplate> expertTemplates = serviceModel.getExpertComment(model.taskID);
                List<SummaryTemplate> summaryTemplates = serviceModel.getSummary(model.taskID);

                #region CAM Template
                if (camTemplates != null)
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("CAM Template");



                    foreach (var template in camTemplates)
                    {

                        // SET PANEL FREEZE
                        if (!string.IsNullOrEmpty(template.freeze_col) && !string.IsNullOrEmpty(template.freeze_row))
                        {
                            int rowNumber = (!string.IsNullOrEmpty(template.freeze_row)) ? int.Parse(template.freeze_row) : 0;
                            int columnNumber = (!string.IsNullOrEmpty(template.freeze_col)) ? ShareFn.ExcelColumnNameToNumber(template.freeze_col) : 0;
                            if (rowNumber != 0 && columnNumber != 0)
                            {
                                ws.View.FreezePanes(rowNumber, columnNumber);
                            }

                        }

                        //SET COLUMN STYLES
                        if (template.column == "U")
                        {
                            var testcolum = template.column;
                        }
                        if (template.column != null && template.row == null)
                        {
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.column);

                            // SET COLUMN WIDTH
                            if (!string.IsNullOrEmpty(template.width))
                            {
                                double columnWidth = double.Parse(template.width);
                                ws.Column(columnNumber).Width = columnWidth;
                            }

                            // SET COLUMN HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Column(columnNumber).Hidden = template.hidden == "Y" ? true : false;
                            }
                        }
                        //SET ROW STYLES
                        else if (template.column == null && template.row != null)
                        {
                            int rowNumber = int.Parse(template.row);

                            // SET ROW HEIGHT 
                            if (!string.IsNullOrEmpty(template.row))
                            {
                                double rowHeight = 21; //double.Parse(template.height);
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }

                            // SET ROW HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Row(rowNumber).Hidden = template.hidden == "Y" ? true : false;
                                if (template.hidden == "Y")
                                {
                                    double rowHeight = 0;
                                    ws.Row(rowNumber).CustomHeight = true;
                                    ws.Row(rowNumber).Height = rowHeight;
                                }

                            }
                        }
                        else if (template.column != null && template.row != null)
                        {
                            string[] Columns = template.column.Split('|');
                            string[] Rows = template.row.Split('|');
                            string Cell = Columns.First() + Rows.First() + ':' + Columns.Last() + Rows.Last();
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(Columns.First());
                            int rowNumber = int.Parse(Rows.First());

                            #region SetValueCells
                            // SET CELL VALUE //
                            if (!string.IsNullOrEmpty(template.value))
                            {
                                if (CoolServiceModel.IsNumeric(template.value))
                                {
                                    ws.Cells[Cell].Value = Decimal.Parse(template.value);
                                }
                                else
                                {
                                    ws.Cells[Cell].Value = template.value;
                                }
                            }

                            // SET CELL FORMULA //
                            if (!string.IsNullOrEmpty(template.formula))
                            {
                                ws.Cells[Cell].Formula = template.formula;
                            }

                            //SET VALUE IMAGE FOR NOTE
                            int imageHeight = 250;
                            int imageWidth = 600;
                            int imageOffset = 40;
                            string CellImage = "A" + Rows.First();
                            if (template.image != null && template.image.Any())
                            {
                                var setHeight = (imageHeight * template.image.Count());
                                ws.Row(rowNumber).Height = setHeight;
                               
                                template.font_vertical_alignment = "T";
                                ws.Cells[CellImage].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                for (int imgIndex = 0; imgIndex < template.image.Count(); imgIndex++)
                                {
                                    string path = "Web/FileUpload/SPEC";

                                    string imgPath;
                                    Image img;
                                    try
                                    {
                                        var serverHostingMapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + path);
                                        imgPath = Path.Combine(serverHostingMapPath, template.image[imgIndex]);// Path.Combine(Server.MapPath("~/" + path), template.image[imgIndex]);
                                        if (!string.IsNullOrEmpty(imgPath))
                                        {
                                            img = Image.FromFile(imgPath);
                                            var dwg = ws.Drawings.AddPicture(string.Format("{0}{1}", Cell, imgIndex), img);
                                            dwg.SetSize(imageWidth, imageHeight);
                                            dwg.SetPosition(rowNumber - 1, (imgIndex * imageWidth) + imageOffset, columnNumber - 1, imageOffset - 20);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }
                            #endregion

                            #region SetFormatCells
                            // SET CELL MERGE // 
                            if (!string.IsNullOrEmpty(template.merge)) //if ((!string.IsNullOrEmpty(Columns.Last()) || !string.IsNullOrEmpty(Rows.Last())) && !string.IsNullOrEmpty(template.merge))
                            {
                                try
                                {
                                    ws.Cells[Cell].Merge = template.merge == "Y" ? true : false;
                                }
                                catch (Exception ex)
                                {

                                }
                            }




                            // SET CELL BACKGROUND
                            if (!string.IsNullOrEmpty(template.bg_color))
                            {
                                ws.Cells[Cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells[Cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(template.bg_color));
                            }

                            // SET CELL BORDER
                            if (!string.IsNullOrEmpty(template.border_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                if (!string.IsNullOrEmpty(template.border_color) && template.border_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                }
                            }

                            // SET CELL TEXT ROTATION //
                            if (!string.IsNullOrEmpty(template.alignment))
                            {
                                if (template.alignment == "V")
                                    ws.Cells[Cell].Style.TextRotation = 90;
                            }

                            // SET CELL FONT NAME
                            if (!string.IsNullOrEmpty(template.font_name))
                            {
                                ws.Cells[Cell].Style.Font.Name = template.font_name;
                            }

                            // SET CELL FONT BOLD
                            if (!string.IsNullOrEmpty(template.font_bold))
                            {
                                ws.Cells[Cell].Style.Font.Bold = template.font_bold == "Y" ? true : false;
                            }

                            // SET CELL FONT COLOR
                            if (!string.IsNullOrEmpty(template.font_color))
                            {
                                ws.Cells[Cell].Style.Font.Color.SetColor(ColorTranslator.FromHtml(template.font_color));
                            }

                            // SET CELL FONT SIZE
                            if (!string.IsNullOrEmpty(template.font_size))
                            {
                                ws.Cells[Cell].Style.Font.Size = float.Parse(template.font_size);
                            }

                            // SET CELL NUMBER FORMAT //
                            if (!string.IsNullOrEmpty(template.number_format))
                            {
                                ws.Cells[Cell].Style.Numberformat.Format = template.number_format;
                            }

                            // SET CELL INDENT //
                            if (!string.IsNullOrEmpty(template.indent))
                            {
                                if (template.indent == "Y")
                                    ws.Cells[Cell].Value = "   -" + ws.Cells[Cell].Value;
                            }

                            // SET CELL CENTER
                            if (!string.IsNullOrEmpty(template.center))
                            {
                                if (template.center == "Y")
                                {
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }

                            // SET CELL FONT ITALIC
                            if (!string.IsNullOrEmpty(template.font_italic))
                            {
                                ws.Cells[Cell].Style.Font.Italic = template.font_italic == "Y" ? true : false;
                            }

                            // SET CELL FONT UNDERLINE
                            if (!string.IsNullOrEmpty(template.font_underline))
                            {
                                ws.Cells[Cell].Style.Font.UnderLine = template.font_underline == "Y" ? true : false;
                            }

                            // SET CELL FONT TEXTWRAP
                            if (!string.IsNullOrEmpty(template.font_textwrap))
                            {
                                ws.Cells[Cell].Style.WrapText = template.font_textwrap == "Y" ? true : false;
                            }

                            // SET CELL FONT HORIZONTAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_horizontal_alignment))
                            {
                                if (template.font_horizontal_alignment == "L")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                else if (template.font_horizontal_alignment == "C")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                else if (template.font_horizontal_alignment == "R")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            }

                            // SET CELL FONT VERTICAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_vertical_alignment))
                            {
                                if (template.font_vertical_alignment == "T")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                else if (template.font_vertical_alignment == "M")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                else if (template.font_vertical_alignment == "B")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                            }

                            // SET CELL BORDER TOP STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_top_style))
                            {
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_top_style);
                                if (!string.IsNullOrEmpty(template.border_top_color) && template.border_top_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_top_color));
                                }
                            }

                            // SET CELL BORDER BOTTOM STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_bottom_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_bottom_style);
                                if (!string.IsNullOrEmpty(template.border_bottom_color) && template.border_bottom_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_bottom_color));
                                }
                            }

                            // SET CELL BORDER LEFT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_left_style))
                            {
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_left_style);
                                if (!string.IsNullOrEmpty(template.border_left_color) && template.border_left_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_left_color));
                                }
                            }

                            // SET CELL BORDER RIGHT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_right_style))
                            {
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_right_style);
                                if (!string.IsNullOrEmpty(template.border_right_color) && template.border_right_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_right_color));
                                }
                            }


                            #endregion
                        }

                    }





                    // SET PAGE BRAKE
                    var max_column = camTemplates.Where(i => !string.IsNullOrEmpty(i.column) && !i.column.Contains("|")).Select(i => ShareFn.ExcelColumnNameToNumber(i.column ?? "A")).OrderByDescending(i => i).FirstOrDefault();
                    var max_row = camTemplates.Where(i => !string.IsNullOrEmpty(i.row) && !i.row.Contains("|")).Select(i => Convert.ToInt32(i.row)).OrderByDescending(i => i).FirstOrDefault();
                    ws.PrinterSettings.Orientation = eOrientation.Portrait;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.FitToPage = true;
                    ws.PrinterSettings.FitToWidth = 1;
                    ws.PrinterSettings.FitToHeight = 0;
                    ws.PrinterSettings.PrintArea = ws.Cells[1, 1, max_row, max_column];
                    ws.View.PageBreakView = true;
                }
                #endregion

                #region Expert Comment Template
                if (expertTemplates != null)
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Expert Comment");
                    foreach (var template in expertTemplates)
                    {
                        // SET PANEL FREEZE
                        if (!string.IsNullOrEmpty(template.freeze_col) && !string.IsNullOrEmpty(template.freeze_row))
                        {
                            int rowNumber = int.Parse(template.freeze_row);
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.freeze_col);
                            ws.View.FreezePanes(rowNumber, columnNumber);
                        }

                        //SET COLUMN STYLES
                        if (template.column != null && template.row == null)
                        {
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.column);

                            // SET COLUMN WIDTH
                            if (!string.IsNullOrEmpty(template.width))
                            {
                                double columnWidth = double.Parse(template.width);
                                ws.Column(columnNumber).Width = columnWidth;
                            }

                            // SET COLUMN HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Column(columnNumber).Hidden = template.hidden == "Y" ? true : false;
                            }
                        }
                        //SET ROW STYLES
                        else if (template.column == null && template.row != null)
                        {
                            int rowNumber = int.Parse(template.row);

                            // SET ROW HEIGHT 
                            if (!string.IsNullOrEmpty(template.row))
                            {
                                double rowHeight = 21; //double.Parse(template.height);
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }

                            // SET ROW HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Row(rowNumber).Hidden = template.hidden == "Y" ? true : false;
                                if (template.hidden == "Y")
                                {
                                    double rowHeight = 0;
                                    ws.Row(rowNumber).CustomHeight = true;
                                    ws.Row(rowNumber).Height = rowHeight;
                                }
                            }
                        }
                        else if (template.column != null && template.row != null)
                        {
                            string[] Columns = template.column.Split('|');
                            string[] Rows = template.row.Split('|');
                            string Cell = Columns.First() + Rows.First() + ':' + Columns.Last() + Rows.Last();
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(Columns.First());
                            int rowNumber = int.Parse(Rows.First());

                            #region SetValueCells
                            // SET CELL VALUE //
                            if (!string.IsNullOrEmpty(template.value))
                            {
                                ws.Cells[Cell].Value = template.value;
                            }

                            // SET CELL FORMULA //
                            if (!string.IsNullOrEmpty(template.formula))
                            {
                                ws.Cells[Cell].Formula = template.formula;
                            }

                            // SET CELL HYPER LINK //
                            if (!string.IsNullOrEmpty(template.hyperlink))
                            {
                                //ws.Cells[Cell].Formula = "HYPERLINK(\"" + template.hyperlink + "\",\"" + template.text + "\")";
                                template.font_color = "#0000FF";
                                template.font_underline = "Y";
                                ws.Cells[Cell].Hyperlink = new Uri(template.hyperlink);
                                ws.Cells[Cell].Value = template.text;
                            }

                            //SET VALUE IMAGE FOR NOTE
                            int imageHeight = 200;
                            int imageWidth = 200;
                            int imageOffset = 40;
                            string CellImage = "A" + Rows.First();
                            if (template.image != null && template.image.Any())
                            {
                                var setHeight = (imageHeight * template.image.Count());
                                ws.Row(rowNumber).Height = setHeight;
                                ws.Cells[CellImage].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                                for (int imgIndex = 0; imgIndex < template.image.Count(); imgIndex++)
                                {
                                    string path = "Web/FileUpload/SPEC";
                                    var imgPath = Path.Combine(Server.MapPath("~/" + path), template.image[imgIndex]);
                                    Image img = Image.FromFile(imgPath);
                                    var dwg = ws.Drawings.AddPicture(string.Format("{0}{1}", Cell, imgIndex), img);
                                    dwg.SetSize(imageWidth, imageHeight);
                                    dwg.SetPosition(rowNumber - 1, (imgIndex * imageWidth) + imageOffset, columnNumber, imageOffset);
                                }
                            }
                            #endregion

                            #region SetFormatCells
                            // SET CELL MERGE // 
                            if (!string.IsNullOrEmpty(template.merge)) //if ((!string.IsNullOrEmpty(Columns.Last()) || !string.IsNullOrEmpty(Rows.Last())) && !string.IsNullOrEmpty(template.merge))
                            {
                                ws.Cells[Cell].Merge = template.merge == "Y" ? true : false;
                            }

                            // SET CELL BACKGROUND
                            if (!string.IsNullOrEmpty(template.bg_color))
                            {
                                ws.Cells[Cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells[Cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(template.bg_color));
                            }

                            // SET CELL BORDER
                            if (!string.IsNullOrEmpty(template.border_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                if (!string.IsNullOrEmpty(template.border_color) && template.border_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                }
                            }

                            // SET CELL TEXT ROTATION //
                            if (!string.IsNullOrEmpty(template.alignment))
                            {
                                if (template.alignment == "V")
                                    ws.Cells[Cell].Style.TextRotation = 90;
                            }

                            // SET CELL FONT NAME
                            if (!string.IsNullOrEmpty(template.font_name))
                            {
                                ws.Cells[Cell].Style.Font.Name = template.font_name;
                            }

                            // SET CELL FONT BOLD
                            if (!string.IsNullOrEmpty(template.font_bold))
                            {
                                ws.Cells[Cell].Style.Font.Bold = template.font_bold == "Y" ? true : false;
                            }

                            // SET CELL FONT COLOR
                            if (!string.IsNullOrEmpty(template.font_color))
                            {
                                ws.Cells[Cell].Style.Font.Color.SetColor(ColorTranslator.FromHtml(template.font_color));
                            }

                            // SET CELL FONT SIZE
                            if (!string.IsNullOrEmpty(template.font_size))
                            {
                                ws.Cells[Cell].Style.Font.Size = float.Parse(template.font_size);
                            }

                            // SET CELL NUMBER FORMAT //
                            if (!string.IsNullOrEmpty(template.number_format))
                            {
                                ws.Cells[Cell].Style.Numberformat.Format = template.number_format;
                            }

                            // SET CELL INDENT //
                            if (!string.IsNullOrEmpty(template.indent))
                            {
                                if (template.indent == "Y")
                                    ws.Cells[Cell].Value = "   -" + ws.Cells[Cell].Value;
                            }

                            // SET CELL CENTER
                            if (!string.IsNullOrEmpty(template.center))
                            {
                                if (template.center == "Y")
                                {
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }

                            // SET CELL FONT ITALIC
                            if (!string.IsNullOrEmpty(template.font_italic))
                            {
                                ws.Cells[Cell].Style.Font.Italic = template.font_italic == "Y" ? true : false;
                            }

                            // SET CELL FONT UNDERLINE
                            if (!string.IsNullOrEmpty(template.font_underline))
                            {
                                ws.Cells[Cell].Style.Font.UnderLine = template.font_underline == "Y" ? true : false;
                            }

                            // SET CELL FONT TEXTWRAP
                            if (!string.IsNullOrEmpty(template.font_textwrap))
                            {
                                ws.Cells[Cell].Style.WrapText = template.font_textwrap == "Y" ? true : false;
                            }

                            // SET CELL FONT HORIZONTAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_horizontal_alignment))
                            {
                                if (template.font_horizontal_alignment == "L")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                else if (template.font_horizontal_alignment == "C")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                else if (template.font_horizontal_alignment == "R")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            }

                            // SET CELL FONT VERTICAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_vertical_alignment))
                            {
                                if (template.font_vertical_alignment == "T")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                else if (template.font_vertical_alignment == "M")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                else if (template.font_vertical_alignment == "B")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                            }

                            // SET CELL BORDER TOP STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_top_style))
                            {
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_top_style);
                                if (!string.IsNullOrEmpty(template.border_top_color) && template.border_top_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_top_color));
                                }
                            }

                            // SET CELL BORDER BOTTOM STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_bottom_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_bottom_style);
                                if (!string.IsNullOrEmpty(template.border_bottom_color) && template.border_bottom_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_bottom_color));
                                }
                            }

                            // SET CELL BORDER LEFT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_left_style))
                            {
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_left_style);
                                if (!string.IsNullOrEmpty(template.border_left_color) && template.border_left_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_left_color));
                                }
                            }

                            // SET CELL BORDER RIGHT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_right_style))
                            {
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_right_style);
                                if (!string.IsNullOrEmpty(template.border_right_color) && template.border_right_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_right_color));
                                }
                            }
                            #endregion
                        }
                    }
                    // SET PAGE BRAKE
                    var max_column = expertTemplates.Where(i => !string.IsNullOrEmpty(i.column) && !i.column.Contains("|")).Select(i => ShareFn.ExcelColumnNameToNumber(i.column ?? "A")).OrderByDescending(i => i).FirstOrDefault();
                    var max_row = expertTemplates.Where(i => !string.IsNullOrEmpty(i.row) && !i.row.Contains("|")).Select(i => Convert.ToInt32(i.row)).OrderByDescending(i => i).FirstOrDefault();
                    ws.PrinterSettings.Orientation = eOrientation.Portrait;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.FitToPage = true;
                    ws.PrinterSettings.FitToWidth = 1;
                    ws.PrinterSettings.FitToHeight = 0;
                    ws.PrinterSettings.PrintArea = ws.Cells[1, 1, max_row, max_column];
                    ws.View.PageBreakView = true;
                }
                #endregion

                #region Summary Template
                if (summaryTemplates != null)
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Summary");
                    foreach (var template in summaryTemplates)
                    {
                        // SET PANEL FREEZE
                        if (!string.IsNullOrEmpty(template.freeze_col) && !string.IsNullOrEmpty(template.freeze_row))
                        {
                            int rowNumber = int.Parse(template.freeze_row);
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.freeze_col);
                            ws.View.FreezePanes(rowNumber, columnNumber);
                        }

                        //SET COLUMN STYLES
                        if (template.column != null && template.row == null)
                        {
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(template.column);

                            // SET COLUMN WIDTH
                            if (!string.IsNullOrEmpty(template.width))
                            {
                                double columnWidth = double.Parse(template.width);
                                ws.Column(columnNumber).Width = columnWidth;
                            }

                            // SET COLUMN HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Column(columnNumber).Hidden = template.hidden == "Y" ? true : false;
                            }
                        }
                        //SET ROW STYLES
                        else if (template.column == null && template.row != null)
                        {
                            int rowNumber = int.Parse(template.row);

                            // SET ROW HEIGHT 
                            if (!string.IsNullOrEmpty(template.row))
                            {
                                double rowHeight = 21; //double.Parse(template.height);
                                ws.Row(rowNumber).CustomHeight = true;
                                ws.Row(rowNumber).Height = rowHeight;
                            }

                            // SET ROW HIDDEN 
                            if (!string.IsNullOrEmpty(template.hidden))
                            {
                                ws.Row(rowNumber).Hidden = template.hidden == "Y" ? true : false;
                                if (template.hidden == "Y")
                                {
                                    double rowHeight = 0;
                                    ws.Row(rowNumber).CustomHeight = true;
                                    ws.Row(rowNumber).Height = rowHeight;
                                }
                            }
                        }
                        else if (template.column != null && template.row != null)
                        {
                            string[] Columns = template.column.Split('|');
                            string[] Rows = template.row.Split('|');
                            string Cell = Columns.First() + Rows.First() + ':' + Columns.Last() + Rows.Last();
                            int columnNumber = ShareFn.ExcelColumnNameToNumber(Columns.First());
                            int rowNumber = int.Parse(Rows.First());

                            #region SetValueCells
                            // SET CELL VALUE //
                            if (!string.IsNullOrEmpty(template.value))
                            {
                                ws.Cells[Cell].Value = template.value;
                            }

                            // SET CELL FORMULA //
                            if (!string.IsNullOrEmpty(template.formula))
                            {
                                ws.Cells[Cell].Formula = template.formula;
                            }

                            //SET VALUE IMAGE FOR NOTE
                            int imageHeight = 200;
                            int imageWidth = 200;
                            int imageOffset = 40;
                            string CellImage = "A" + Rows.First();
                            if (template.image != null && template.image.Any())
                            {
                                var setHeight = (imageHeight * template.image.Count());
                                ws.Row(rowNumber).Height = setHeight;
                                ws.Cells[CellImage].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;

                                for (int imgIndex = 0; imgIndex < template.image.Count(); imgIndex++)
                                {
                                    string path = "Web/FileUpload/SPEC";
                                    var imgPath = Path.Combine(Server.MapPath("~/" + path), template.image[imgIndex]);
                                    Image img = Image.FromFile(imgPath);
                                    var dwg = ws.Drawings.AddPicture(string.Format("{0}{1}", Cell, imgIndex), img);
                                    dwg.SetSize(imageWidth, imageHeight);
                                    dwg.SetPosition(rowNumber - 1, (imgIndex * imageWidth) + imageOffset, columnNumber, imageOffset);
                                }
                            }
                            #endregion

                            #region SetFormatCells
                            // SET CELL MERGE // 
                            if (!string.IsNullOrEmpty(template.merge)) //if ((!string.IsNullOrEmpty(Columns.Last()) || !string.IsNullOrEmpty(Rows.Last())) && !string.IsNullOrEmpty(template.merge))
                            {
                                ws.Cells[Cell].Merge = template.merge == "Y" ? true : false;
                            }

                            // SET CELL BACKGROUND
                            if (!string.IsNullOrEmpty(template.bg_color))
                            {
                                ws.Cells[Cell].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ws.Cells[Cell].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml(template.bg_color));
                            }

                            // SET CELL BORDER
                            if (!string.IsNullOrEmpty(template.border_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_style);
                                if (!string.IsNullOrEmpty(template.border_color) && template.border_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_color));
                                }
                            }

                            // SET CELL TEXT ROTATION //
                            if (!string.IsNullOrEmpty(template.alignment))
                            {
                                if (template.alignment == "V")
                                    ws.Cells[Cell].Style.TextRotation = 90;
                            }

                            // SET CELL FONT NAME
                            if (!string.IsNullOrEmpty(template.font_name))
                            {
                                ws.Cells[Cell].Style.Font.Name = template.font_name;
                            }

                            // SET CELL FONT BOLD
                            if (!string.IsNullOrEmpty(template.font_bold))
                            {
                                ws.Cells[Cell].Style.Font.Bold = template.font_bold == "Y" ? true : false;
                            }

                            // SET CELL FONT COLOR
                            if (!string.IsNullOrEmpty(template.font_color))
                            {
                                ws.Cells[Cell].Style.Font.Color.SetColor(ColorTranslator.FromHtml(template.font_color));
                            }

                            // SET CELL FONT SIZE
                            if (!string.IsNullOrEmpty(template.font_size))
                            {
                                ws.Cells[Cell].Style.Font.Size = float.Parse(template.font_size);
                            }

                            // SET CELL NUMBER FORMAT //
                            if (!string.IsNullOrEmpty(template.number_format))
                            {
                                ws.Cells[Cell].Style.Numberformat.Format = template.number_format;
                            }

                            // SET CELL INDENT //
                            if (!string.IsNullOrEmpty(template.indent))
                            {
                                if (template.indent == "Y")
                                    ws.Cells[Cell].Value = "   -" + ws.Cells[Cell].Value;
                            }

                            // SET CELL CENTER
                            if (!string.IsNullOrEmpty(template.center))
                            {
                                if (template.center == "Y")
                                {
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                }
                            }

                            // SET CELL FONT ITALIC
                            if (!string.IsNullOrEmpty(template.font_italic))
                            {
                                ws.Cells[Cell].Style.Font.Italic = template.font_italic == "Y" ? true : false;
                            }

                            // SET CELL FONT UNDERLINE
                            if (!string.IsNullOrEmpty(template.font_underline))
                            {
                                ws.Cells[Cell].Style.Font.UnderLine = template.font_underline == "Y" ? true : false;
                            }

                            // SET CELL FONT TEXTWRAP
                            if (!string.IsNullOrEmpty(template.font_textwrap))
                            {
                                ws.Cells[Cell].Style.WrapText = template.font_textwrap == "Y" ? true : false;
                            }

                            // SET CELL FONT HORIZONTAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_horizontal_alignment))
                            {
                                if (template.font_horizontal_alignment == "L")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                else if (template.font_horizontal_alignment == "C")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                else if (template.font_horizontal_alignment == "R")
                                    ws.Cells[Cell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            }

                            // SET CELL FONT VERTICAL ALIGNMENT
                            if (!string.IsNullOrEmpty(template.font_vertical_alignment))
                            {
                                if (template.font_vertical_alignment == "T")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                                else if (template.font_vertical_alignment == "M")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                                else if (template.font_vertical_alignment == "B")
                                    ws.Cells[Cell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                            }

                            // SET CELL BORDER TOP STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_top_style))
                            {
                                ws.Cells[Cell].Style.Border.Top.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_top_style);
                                if (!string.IsNullOrEmpty(template.border_top_color) && template.border_top_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Top.Color.SetColor(ColorTranslator.FromHtml(template.border_top_color));
                                }
                            }

                            // SET CELL BORDER BOTTOM STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_bottom_style))
                            {
                                ws.Cells[Cell].Style.Border.Bottom.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_bottom_style);
                                if (!string.IsNullOrEmpty(template.border_bottom_color) && template.border_bottom_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Bottom.Color.SetColor(ColorTranslator.FromHtml(template.border_bottom_color));
                                }
                            }

                            // SET CELL BORDER LEFT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_left_style))
                            {
                                ws.Cells[Cell].Style.Border.Left.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_left_style);
                                if (!string.IsNullOrEmpty(template.border_left_color) && template.border_left_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Left.Color.SetColor(ColorTranslator.FromHtml(template.border_left_color));
                                }
                            }

                            // SET CELL BORDER RIGHT STYLE AND COLOR
                            if (!string.IsNullOrEmpty(template.border_right_style))
                            {
                                ws.Cells[Cell].Style.Border.Right.Style = (OfficeOpenXml.Style.ExcelBorderStyle)Int32.Parse(template.border_right_style);
                                if (!string.IsNullOrEmpty(template.border_right_color) && template.border_right_style != "0")
                                {
                                    ws.Cells[Cell].Style.Border.Right.Color.SetColor(ColorTranslator.FromHtml(template.border_right_color));
                                }
                            }
                            #endregion
                        }
                    }
                    // SET PAGE BRAKE
                    var max_column = summaryTemplates.Where(i => !string.IsNullOrEmpty(i.column) && !i.column.Contains("|")).Select(i => ShareFn.ExcelColumnNameToNumber(i.column ?? "A")).OrderByDescending(i => i).FirstOrDefault();
                    var max_row = summaryTemplates.Where(i => !string.IsNullOrEmpty(i.row) && !i.row.Contains("|")).Select(i => Convert.ToInt32(i.row)).OrderByDescending(i => i).FirstOrDefault();
                    ws.PrinterSettings.Orientation = eOrientation.Portrait;
                    ws.PrinterSettings.PaperSize = ePaperSize.A4;
                    ws.PrinterSettings.FitToPage = true;
                    ws.PrinterSettings.FitToWidth = 1;
                    ws.PrinterSettings.FitToHeight = 0;
                    ws.PrinterSettings.PrintArea = ws.Cells[1, 1, max_row, max_column];
                    ws.View.PageBreakView = true;
                }
                #endregion

                //save excel
                int wait_excel = 0;
                while (wait_excel < 10)
                {
                    if (!excelFile.Exists)
                    {
                        package.Save();
                        break;
                    } else
                    {
                        if (ShareFn.IsFileReady(full_path))
                        {
                            package.Save();
                            break;
                        }
                    }
                    wait_excel++;
                    Thread.Sleep(1000);
                }
                if (isDownload)
                {
                    var title_name = file_name;
                    Response.Clear();
                    Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                    Response.AddHeader("content-type", "application/Excel");
                    Response.WriteFile(full_path);
                    Response.End();
                }
                return Path.Combine(virture_path, file_name);
            }
        }

        private void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            _borders.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
        }

        private void AddImage(ExcelWorksheet oSheet, int rowIndex, int colIndex, string imagePath)
        {
            System.Drawing.Bitmap image = new System.Drawing.Bitmap(imagePath);
            if (image != null)
            {
                var excelImage = oSheet.Drawings.AddPicture("Attached_Image", image);
                excelImage.From.Column = colIndex;
                excelImage.From.Row = rowIndex;
                excelImage.SetSize(400, 300);
                // 2x2 px space for better alignment
                excelImage.From.ColumnOff = Pixel2MTU(2);
                excelImage.From.RowOff = Pixel2MTU(2);
            }
        }

        public int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }

        public string GenerateTimeReport(CoolTimeReport model, bool isDownload = false)
        {
            string file_name = model.file_name;
            string virture_path = Path.Combine("Web", "FileUpload", "COOL");
            string physical_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, virture_path);
            string full_path = Path.Combine(physical_path, file_name);

            if (!Directory.Exists(physical_path))
                Directory.CreateDirectory(physical_path);

            FileInfo currentFile = new FileInfo(full_path);
            if (currentFile.Exists)
            {
                currentFile.Delete();
            }
            FileInfo excelFile = new FileInfo(full_path);
            using (ExcelPackage package = new ExcelPackage(excelFile))
            {
                DateTime start = DateTime.ParseExact(model.period_month_from + "/" + model.period_year_from, "MMM/yyyy", CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(model.period_month_to + "/" + model.period_year_to, "MMM/yyyy", CultureInfo.InvariantCulture);
                end = new DateTime(end.Year, end.Month, DateTime.DaysInMonth(end.Year, end.Month));
                COO_DATA_DAL dataDal = new COO_DATA_DAL();
                List<COO_DATA> data = dataDal.GetByDateRange(start, end);

                if (!string.IsNullOrEmpty(model.crude_name))
                    data = data.Where(x => x.CODA_CRUDE_NAME.Contains(model.crude_name)).ToList();

                #region Time Report
                if (data != null && data.Count > 0)
                {
                    List<CoolTimeReportDetail> timeReports = new List<CoolTimeReportDetail>();
                    string prev_month_year = "";
                    int count = 1;
                    foreach (COO_DATA d in data)
                    {
                        COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                        List<COO_EXPERT> experts = expertDal.GetAllByID(d.CODA_ROW_ID, true);
                        string month_year = d.CODA_CREATED.ToString("MMM\\'yy");
                        if (prev_month_year == "")
                        {
                            prev_month_year = month_year;
                        }
                        CoolTimeReportDetail timeReport = timeReports.Where(x => x.month_year == month_year).FirstOrDefault();
                        if (timeReport == null)
                        {
                            timeReport = new CoolTimeReportDetail();
                            timeReport.month_year = month_year;
                            timeReport.crude = new List<CoolTimeReportDetail.Crude>();
                        }

                        CoolTimeReportDetail.Crude crude = new CoolTimeReportDetail.Crude();
                        crude.name = d.CODA_CRUDE_NAME;
                        crude.a_time_day = (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).HasValue ? (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).Value.TotalDays : 0.0;
                        crude.d_time_day = (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).HasValue ? (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).Value.TotalDays : 0.0;
                        crude.a_time_hour = (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).HasValue ? (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).Value.TotalHours : 0.0;
                        crude.d_time_hour = (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).HasValue ? (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).Value.TotalHours : 0.0;
                        if (experts != null && experts.Count > 0)
                        {
                            COO_EXPERT lastExpert = experts.OrderByDescending(x => x.COEX_APPROVE_DATE).FirstOrDefault();
                            if (lastExpert != null)
                            {
                                crude.b_time_day = (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalDays : 0.0;
                                crude.c_time_day = (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).HasValue ? (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).Value.TotalDays : 0.0;
                                crude.b_time_hour = (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalHours : 0.0;
                                crude.c_time_hour = (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).HasValue ? (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).Value.TotalHours : 0.0;
                            }
                            else
                            {
                                crude.b_time_day = 0.0;
                                crude.c_time_day = 0.0;
                                crude.b_time_hour = 0.0;
                                crude.c_time_hour = 0.0;
                            }
                            crude.unit = new List<CoolTimeReportDetail.Unit>();
                            foreach (COO_EXPERT expert in experts)
                            {
                                CoolTimeReportDetail.Unit unit = new CoolTimeReportDetail.Unit();
                                unit.name = expert.COEX_UNIT;
                                unit.expert_time_hour = (expert.COEX_SUBMIT_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (expert.COEX_SUBMIT_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalHours : 0.0;
                                unit.expert_sh_time_hour = (expert.COEX_APPROVE_DATE - expert.COEX_SUBMIT_DATE).HasValue ? (expert.COEX_APPROVE_DATE - expert.COEX_SUBMIT_DATE).Value.TotalHours : 0.0;
                                crude.unit.Add(unit);
                            }
                        }
                        else
                        {
                            crude.b_time_day = 0.0;
                            crude.c_time_day = 0.0;
                            crude.b_time_hour = 0.0;
                            crude.c_time_hour = 0.0;
                        }
                        timeReport.working_time += crude.a_time_day + crude.b_time_day + crude.c_time_day + crude.d_time_day;
                        timeReport.crude.Add(crude);
                        if (prev_month_year != month_year || count == 1)
                        {
                            timeReports.Add(timeReport);
                            prev_month_year = month_year;
                        }
                        count++;
                    }

                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("Time Report");

                    //Initialize columns
                    ws.Column(2).Width = 15;
                    ws.Column(3).Width = 40;
                    ws.Column(4).Width = 10;
                    ws.Cells.Style.Numberformat.Format = "#,##0.00";
                    ws.Cells.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    int row = 1;
                    int start_boder_row = 0;
                    int end_boder_row = 0;

                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Time Report from " + start.ToString("MMM\\'yy") + " to " + end.ToString("MMM\\'yy");
                    row++;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Crude: All";
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Step A: เวลาของขั้นตอนหลังจาก ที่ SCEP submit draft CAM ถึงระยะเวลาที่ SCEP section head approve draft CAM";
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Step B: เวลาของขั้นตอนหลังจาก ที่ SCEP section head approve draft CAM ถึงระยะเวลาที่ Expert section head คนสุดท้าย comment and approve";
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Step C: เวลาของขั้นตอนหลังจาก Expert section head comment and approve ถึงระยะเวลาที่ SCEP admin submit Final CAM";
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Step D: เวลาของขั้นตอนหลังจาก SCEP admin conclude Final CAM ถึงระยะเวลาที่ SCEP section head approve Final CAM";
                    row += 4;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "No. Crude: " + data.Count;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Average time per step";

                    row += 35;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Summary";
                    row++;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Number of crude and working time";
                    row++;
                    start_boder_row = row;
                    ws.Cells[row, 2].Value = "Month";
                    ws.Cells[row, 2].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 2].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 3].Value = "Number of CAM";
                    ws.Cells[row, 3].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 4].Value = "Average working time per crude (days)";
                    ws.Cells[row, 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells["D" + row + ":" + "G" + row].Merge = true;
                    row++;

                    for (int i = 0; i < timeReports.Count; i++)
                    {
                        ws.Cells[row, 2].Value = timeReports[i].month_year;
                        ws.Cells[row, 3].Value = timeReports[i].crude.Count;
                        ws.Cells[row, 4].Value = timeReports[i].working_time;
                        ws.Cells["D" + row + ":" + "G" + row].Merge = true;
                        row++;
                    }

                    end_boder_row = row;

                    AllBorders(ws.Cells["B" + start_boder_row + ":" + "G" + (end_boder_row - 1)].Style.Border);

                    row += 4;
                    ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    ws.Cells[row++, 2].Value = "Working time (Each Step)";
                    row++;
                    start_boder_row = row;
                    ws.Cells["B" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["B" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells["B" + row].Value = "Month";
                    ws.Cells["B" + row + ":B" + (row + 1)].Merge = true;
                    ws.Cells["C" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["C" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells["C" + row].Value = "Crude";
                    ws.Cells["C" + row + ":C" + (row + 1)].Merge = true;
                    ws.Cells["D" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells["D" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells["D" + row].Value = "Working time (hrs)";
                    ws.Cells["D" + row + ":H" + row].Merge = true;
                    row++;
                    ws.Cells[row, 4].Value = "A";
                    ws.Cells[row, 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 5].Value = "B";
                    ws.Cells[row, 5].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 5].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 6].Value = "C";
                    ws.Cells[row, 6].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 6].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 7].Value = "D";
                    ws.Cells[row, 7].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 7].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    ws.Cells[row, 8].Value = "Total";
                    ws.Cells[row, 8].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ws.Cells[row, 8].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    row++;

                    int count_average = 0;
                    double _a = 0.0;
                    double _b = 0.0;
                    double _c = 0.0;
                    double _d = 0.0;
                    double total = 0.0;
                    for (int i = 0; i < timeReports.Count; i++)
                    {
                        ws.Cells[row, 2].Value = timeReports[i].month_year;
                        int start_row = row;
                        for (int j = 0; j < timeReports[i].crude.Count; j++)
                        {
                            ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            ws.Cells[row, 3].Value = timeReports[i].crude[j].name;
                            ws.Cells[row, 4].Value = timeReports[i].crude[j].a_time_hour;
                            ws.Cells[row, 5].Value = timeReports[i].crude[j].b_time_hour;
                            ws.Cells[row, 6].Value = timeReports[i].crude[j].c_time_hour;
                            ws.Cells[row, 7].Value = timeReports[i].crude[j].d_time_hour;
                            ws.Cells[row, 8].Formula = "SUM(D" + row + ":G" + row + ")";
                            _a += timeReports[i].crude[j].a_time_hour;
                            _b += timeReports[i].crude[j].b_time_hour;
                            _c += timeReports[i].crude[j].c_time_hour;
                            _d += timeReports[i].crude[j].d_time_hour;
                            total += (timeReports[i].crude[j].a_time_hour + timeReports[i].crude[j].b_time_hour + timeReports[i].crude[j].c_time_hour + timeReports[i].crude[j].d_time_hour);
                            row++;
                        }
                        ws.Cells[row, 3].Value = "Average";
                        ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        ws.Cells[row, 4].Formula = "AVERAGE(D" + start_row + ":D" + (row - 1) + ")";
                        ws.Cells[row, 5].Formula = "AVERAGE(E" + start_row + ":E" + (row - 1) + ")";
                        ws.Cells[row, 6].Formula = "AVERAGE(F" + start_row + ":F" + (row - 1) + ")";
                        ws.Cells[row, 7].Formula = "AVERAGE(G" + start_row + ":G" + (row - 1) + ")";
                        ws.Cells[row, 8].Formula = "AVERAGE(H" + start_row + ":H" + (row - 1) + ")";
                        count_average++;
                        ws.Cells["AY" + (count_average)].Value = timeReports[i].month_year;
                        ws.Cells["AZ" + (count_average)].Formula = "AVERAGE(H" + start_row + ":H" + (row - 1) + ")";
                        row++;
                    }

                    end_boder_row = row;

                    AllBorders(ws.Cells["B" + start_boder_row + ":" + "H" + (end_boder_row - 1)].Style.Border);

                    ws.Cells["BA1"].Value = _a / data.Count;
                    ws.Cells["BA2"].Value = _b / data.Count;
                    ws.Cells["BA3"].Value = _c / data.Count;
                    ws.Cells["BA4"].Value = _d / data.Count;
                    ws.Cells["BA5"].Value = total / data.Count;

                    ws.Cells["BB1"].Value = "A";
                    ws.Cells["BB2"].Value = "B";
                    ws.Cells["BB3"].Value = "C";
                    ws.Cells["BB4"].Value = "D";
                    ws.Cells["BB5"].Value = "Total";

                    var chart2 = ws.Drawings.AddChart("chart2", eChartType.LineMarkers) as ExcelLineChart;
                    chart2.SetSize(476, 300);
                    chart2.SetPosition(615, 65);
                    chart2.DataLabel.ShowValue = false;
                    chart2.Title.Text = "Total (hrs)";
                    var series2 = chart2.Series.Add("AZ1:AZ" + count_average, "AY1:AY" + count_average);
                    series2.Header = "Total (hrs)";

                    var chart = ws.Drawings.AddChart("chart", eChartType.ColumnClustered) as ExcelBarChart;
                    chart.SetSize(476, 300);
                    chart.SetPosition(300, 65);
                    chart.DataLabel.ShowValue = true;
                    chart.Title.Text = "Average time (hrs)";
                    var series = chart.Series.Add("BA1:BA5", "BB1:BB5");
                    series.Header = "Average";

                    MT_UNIT_DAL unitDal = new MT_UNIT_DAL();
                    List<MT_UNIT> mt_unit = unitDal.getAllUnits();
                    if (mt_unit != null && mt_unit.Count > 0)
                    {
                        #region Expert
                        row += 4;
                        ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        ws.Cells[row++, 2].Value = "Expert comment working time";
                        row++;
                        start_boder_row = row;
                        ws.Cells["B" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["B" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["B" + row].Value = "Month";
                        ws.Cells["B" + row + ":B" + (row + 1)].Merge = true;
                        ws.Cells["C" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["C" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["C" + row].Value = "Crude";
                        ws.Cells["C" + row + ":C" + (row + 1)].Merge = true;
                        ws.Cells["D" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["D" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["D" + row].Value = "Working time (hrs)";
                        ws.Cells[row, 4, row, mt_unit.Count + 4].Merge = true;
                        row++;
                        for (int i = 0; i < mt_unit.Count; i++)
                        {
                            ws.Cells[row, (i + 4)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ws.Cells[row, (i + 4)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                            ws.Cells[row, (i + 4)].Value = mt_unit[i].MUN_NAME;
                        }
                        ws.Cells[row, mt_unit.Count + 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells[row, mt_unit.Count + 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells[row++, mt_unit.Count + 4].Value = "Max";

                        for (int i = 0; i < timeReports.Count; i++)
                        {
                            ws.Cells[row, 2].Value = timeReports[i].month_year;
                            int start_row = row;
                            for (int j = 0; j < timeReports[i].crude.Count; j++)
                            {
                                ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                ws.Cells[row, 3].Value = timeReports[i].crude[j].name;
                                for (int k = 0; k < mt_unit.Count; k++)
                                {
                                    CoolTimeReportDetail.Unit cool_unit = timeReports[i].crude[j].unit != null ? timeReports[i].crude[j].unit.Where(x => x.name == mt_unit[k].MUN_NAME).FirstOrDefault() : null;
                                    if (cool_unit != null)
                                        ws.Cells[row, (k + 4)].Value = cool_unit.expert_time_hour;
                                    else
                                        ws.Cells[row, (k + 4)].Value = "-";
                                }
                                string final_col = ShareFn.GetExcelColumnName(mt_unit.Count + 3);
                                ws.Cells[row, mt_unit.Count + 4].Formula = "IF(MAX(D" + row + ":" + final_col + row + ") = 0.00, \"-\", MAX(D" + row + ":" + final_col + row + "))";
                                row++;
                            }
                            ws.Cells[row, 3].Value = "Average";
                            ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            for (int k = 0; k < mt_unit.Count + 1; k++)
                            {
                                string current_col = ShareFn.GetExcelColumnName(k + 4);
                                ws.Cells[row, (k + 4)].Formula = "IFERROR(AVERAGE(" + current_col + start_row + ":" + current_col + (row - 1) + "), \"-\")";
                            }
                            row++;
                        }
                        #endregion

                        end_boder_row = row;

                        AllBorders(ws.Cells[start_boder_row, 2, end_boder_row - 1, mt_unit.Count + 4].Style.Border);

                        #region Expert SH
                        row += 4;
                        ws.Cells[row, 2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        ws.Cells[row++, 2].Value = "Expert section head approve working time";
                        row++;
                        start_boder_row = row;
                        ws.Cells["B" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["B" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["B" + row].Value = "Month";
                        ws.Cells["B" + row + ":B" + (row + 1)].Merge = true;
                        ws.Cells["C" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["C" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["C" + row].Value = "Crude";
                        ws.Cells["C" + row + ":C" + (row + 1)].Merge = true;
                        ws.Cells["D" + row].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells["D" + row].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells["D" + row].Value = "Working time (hrs)";
                        ws.Cells[row, 4, row, mt_unit.Count + 4].Merge = true;
                        row++;
                        for (int i = 0; i < mt_unit.Count; i++)
                        {
                            ws.Cells[row, (i + 4)].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ws.Cells[row, (i + 4)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                            ws.Cells[row, (i + 4)].Value = mt_unit[i].MUN_NAME;
                        }
                        ws.Cells[row, mt_unit.Count + 4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ws.Cells[row, mt_unit.Count + 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                        ws.Cells[row++, mt_unit.Count + 4].Value = "Max";

                        for (int i = 0; i < timeReports.Count; i++)
                        {
                            ws.Cells[row, 2].Value = timeReports[i].month_year;
                            int start_row = row;
                            for (int j = 0; j < timeReports[i].crude.Count; j++)
                            {
                                ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                ws.Cells[row, 3].Value = timeReports[i].crude[j].name;
                                for (int k = 0; k < mt_unit.Count; k++)
                                {
                                    CoolTimeReportDetail.Unit cool_unit = timeReports[i].crude[j].unit != null ? timeReports[i].crude[j].unit.Where(x => x.name == mt_unit[k].MUN_NAME).FirstOrDefault() : null;
                                    if (cool_unit != null)
                                        ws.Cells[row, (k + 4)].Value = cool_unit.expert_sh_time_hour;
                                    else
                                        ws.Cells[row, (k + 4)].Value = "-";
                                }
                                string final_col = ShareFn.GetExcelColumnName(mt_unit.Count + 3);
                                ws.Cells[row, mt_unit.Count + 4].Formula = "IF(MAX(D" + row + ":" + final_col + row + ") = 0.00, \"-\", MAX(D" + row + ":" + final_col + row + "))";
                                row++;
                            }
                            ws.Cells[row, 3].Value = "Average";
                            ws.Cells[row, 3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            for (int k = 0; k < mt_unit.Count + 1; k++)
                            {
                                string current_col = ShareFn.GetExcelColumnName(k + 4);
                                ws.Cells[row, (k + 4)].Formula = "IFERROR(AVERAGE(" + current_col + start_row + ":" + current_col + (row - 1) + "), \"-\")";
                            }
                            row++;
                        }
                        #endregion
                        end_boder_row = row;

                        AllBorders(ws.Cells[start_boder_row, 2, end_boder_row - 1, mt_unit.Count + 4].Style.Border);
                    }

                    //AllBorders(ws.Cells["A1:H" + row].Style.Border);

                    //save excel
                    package.Save();
                    if (isDownload)
                    {
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment; filename=" + file_name);
                        Response.AddHeader("content-type", "application/Excel");
                        Response.WriteFile(full_path);
                        Response.End();
                    }
                }
                #endregion

                return Path.Combine(virture_path, file_name);
            }
        }

        public CoolTimeReportData GetTimeReport(CoolTimeReport model, bool isDownload = false)
        {
            CoolTimeReportData ctr = new CoolTimeReportData();
            List<CoolTimeReportDetail> timeReports = new List<CoolTimeReportDetail>();
            DateTime start = DateTime.ParseExact(model.period_month_from + "/" + model.period_year_from, "MMM/yyyy", CultureInfo.InvariantCulture);
            DateTime end = DateTime.ParseExact(model.period_month_to + "/" + model.period_year_to, "MMM/yyyy", CultureInfo.InvariantCulture);
            end = new DateTime(end.Year, end.Month, DateTime.DaysInMonth(end.Year, end.Month));
            COO_DATA_DAL dataDal = new COO_DATA_DAL();
            List<COO_DATA> data = dataDal.GetByDateRange(start, end);

            if (!string.IsNullOrEmpty(model.crude_name))
                data = data.Where(x => x.CODA_CRUDE_NAME.ToUpper().Contains(model.crude_name.ToUpper())).ToList();

            #region Time Report
            if (data != null && data.Count > 0)
            {
                ctr.timeReportTitle = "Time Report from " + start.ToString("MMM\\'yy") + " to " + end.ToString("MMM\\'yy");
                string prev_month_year = "";
                int count = 1;
                foreach (COO_DATA d in data)
                {
                    COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                    List<COO_EXPERT> experts = expertDal.GetAllByID(d.CODA_ROW_ID, true);
                    string month_year = d.CODA_CREATED.ToString("MMM\\’yy");
                    if (prev_month_year == "")
                    {
                        prev_month_year = month_year;
                    }
                    CoolTimeReportDetail timeReport = timeReports.Where(x => x.month_year == month_year).FirstOrDefault();
                    if (timeReport == null)
                    {
                        timeReport = new CoolTimeReportDetail();
                        timeReport.month_year = month_year;
                        timeReport.crude = new List<CoolTimeReportDetail.Crude>();
                    }

                    CoolTimeReportDetail.Crude crude = new CoolTimeReportDetail.Crude();
                    crude.name = d.CODA_CRUDE_NAME;
                    crude.a_time_day = (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).HasValue ? (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).Value.TotalDays : 0.0;
                    crude.d_time_day = (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).HasValue ? (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).Value.TotalDays : 0.0;
                    crude.a_time_hour = (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).HasValue ? (d.CODA_DRAFT_APPROVE_DATE - d.CODA_DRAFT_SUBMIT_DATE).Value.TotalHours : 0.0;
                    crude.d_time_hour = (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).HasValue ? (d.CODA_FINAL_APPROVE_DATE - d.CODA_FINAL_SUBMIT_DATE).Value.TotalHours : 0.0;
                    if (experts != null && experts.Count > 0)
                    {
                        COO_EXPERT lastExpert = experts.OrderByDescending(x => x.COEX_APPROVE_DATE).FirstOrDefault();
                        if (lastExpert != null)
                        {
                            crude.b_time_day = (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalDays : 0.0;
                            crude.c_time_day = (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).HasValue ? (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).Value.TotalDays : 0.0;
                            crude.b_time_hour = (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (lastExpert.COEX_APPROVE_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalHours : 0.0;
                            crude.c_time_hour = (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).HasValue ? (d.CODA_FINAL_SUBMIT_DATE - lastExpert.COEX_APPROVE_DATE).Value.TotalHours : 0.0;
                        }
                        else
                        {
                            crude.b_time_day = 0.0;
                            crude.c_time_day = 0.0;
                            crude.b_time_hour = 0.0;
                            crude.c_time_hour = 0.0;
                        }
                        crude.unit = new List<CoolTimeReportDetail.Unit>();
                        foreach (COO_EXPERT expert in experts)
                        {
                            CoolTimeReportDetail.Unit unit = new CoolTimeReportDetail.Unit();
                            unit.name = expert.COEX_UNIT;
                            unit.expert_time_hour = (expert.COEX_SUBMIT_DATE - d.CODA_DRAFT_APPROVE_DATE).HasValue ? (expert.COEX_SUBMIT_DATE - d.CODA_DRAFT_APPROVE_DATE).Value.TotalHours : 0.0;
                            unit.expert_sh_time_hour = (expert.COEX_APPROVE_DATE - expert.COEX_SUBMIT_DATE).HasValue ? (expert.COEX_APPROVE_DATE - expert.COEX_SUBMIT_DATE).Value.TotalHours : 0.0;
                            crude.unit.Add(unit);
                        }
                    }
                    else
                    {
                        crude.b_time_day = 0.0;
                        crude.c_time_day = 0.0;
                        crude.b_time_hour = 0.0;
                        crude.c_time_hour = 0.0;
                    }
                    timeReport.working_time += crude.a_time_day + crude.b_time_day + crude.c_time_day + crude.d_time_day;
                    timeReport.crude.Add(crude);
                    if (prev_month_year != month_year || count == 1)
                    {
                        timeReports.Add(timeReport);
                        prev_month_year = month_year;
                    }
                    count++;
                }

                List<NumberOfCrude> nocList = new List<NumberOfCrude>();
                foreach (var a in timeReports)
                {
                    NumberOfCrude noc = new NumberOfCrude();
                    noc.month = a.month_year;
                    noc.numberOfCam = a.crude.Count().ToString();
                    noc.avgWorkingTime = a.working_time;
                    nocList.Add(noc);
                }
                ctr.numberOfCrude = nocList;
                int countCrude = 0;
                ctr.timeA_sumAvg = 0;
                ctr.timeB_sumAvg = 0;
                ctr.timeC_sumAvg = 0;
                ctr.timeD_sumAvg = 0;
                ctr.timeTotal_sumAvg = 0;

                List<WorkingTime> wtList = new List<WorkingTime>();
                for (int i = 0; i < timeReports.Count; i++)
                {
                    WorkingTime wt = new WorkingTime();
                    List<WorkTimeDetail> wtdList = new List<WorkTimeDetail>();
                    for (int j = 0; j < timeReports[i].crude.Count; j++)
                    {
                        WorkTimeDetail wtd = new WorkTimeDetail();
                        wtd.crudeName = timeReports[i].crude[j].name.Replace("'", "’");
                        wtd.timeA = timeReports[i].crude[j].a_time_hour;
                        wtd.timeB = timeReports[i].crude[j].b_time_hour;
                        wtd.timeC = timeReports[i].crude[j].c_time_hour;
                        wtd.timeD = timeReports[i].crude[j].d_time_hour;
                        wtd.timeTotal += (timeReports[i].crude[j].a_time_hour + timeReports[i].crude[j].b_time_hour + timeReports[i].crude[j].c_time_hour + timeReports[i].crude[j].d_time_hour);

                        ctr.timeA_sumAvg += wtd.timeA;
                        ctr.timeB_sumAvg += wtd.timeB;
                        ctr.timeC_sumAvg += wtd.timeC;
                        ctr.timeD_sumAvg += wtd.timeD;
                        ctr.timeTotal_sumAvg += wtd.timeTotal;
                        countCrude++;

                        wtdList.Add(wtd);
                    }
                    wt.month = timeReports[i].month_year;
                    wt.timeA_avg = wtdList.Select(x => x.timeA).Average();
                    wt.timeB_avg = wtdList.Select(x => x.timeB).Average();
                    wt.timeC_avg = wtdList.Select(x => x.timeC).Average();
                    wt.timeD_avg = wtdList.Select(x => x.timeD).Average();
                    wt.timeTotal_avg = wtdList.Select(x => x.timeTotal).Average();
                    wt.workTimeDetail = wtdList;
                    wtList.Add(wt);
                }

                ctr.timeA_sumAvg = ctr.timeA_sumAvg / countCrude;
                ctr.timeB_sumAvg = ctr.timeB_sumAvg / countCrude;
                ctr.timeC_sumAvg = ctr.timeC_sumAvg / countCrude;
                ctr.timeD_sumAvg = ctr.timeD_sumAvg / countCrude;
                ctr.timeTotal_sumAvg = ctr.timeTotal_sumAvg / countCrude;

                ctr.workingTime = wtList;

                MT_UNIT_DAL unitDal = new MT_UNIT_DAL();
                List<MT_UNIT> mt_unit = unitDal.getAllUnits();
                if (mt_unit != null && mt_unit.Count > 0)
                {
                    ctr.columnCount = mt_unit.Count();
                    List<ExpertWorkingTime> ewtList = new List<ExpertWorkingTime>();
                    for (int i = 0; i < timeReports.Count; i++)
                    {
                        List<ExpertWorkingTimeDetail> ewtdList = new List<ExpertWorkingTimeDetail>();
                        int sKey = 1;
                        for (int j = 0; j < timeReports[i].crude.Count; j++)
                        {
                            ExpertWorkingTime ewt = new ExpertWorkingTime();
                            string sCrudeName = "";
                            for (int k = 0; k < mt_unit.Count; k++)
                            {
                                ExpertWorkingTimeDetail ewtd = new ExpertWorkingTimeDetail();
                                CoolTimeReportDetail.Unit cool_unit = timeReports[i].crude[j].unit != null ? timeReports[i].crude[j].unit.Where(x => x.name == mt_unit[k].MUN_NAME).FirstOrDefault() : null;
                                ewtd.crudeKey = sKey.ToString();
                                ewtd.crudeName = timeReports[i].crude[j].name;
                                ewtd.columnName = mt_unit[k].MUN_NAME;
                                ewtd.timeValue = cool_unit == null ? 0.0 : cool_unit.expert_time_hour;
                                ewtd.timeValueSH = cool_unit == null ? 0.0 : cool_unit.expert_sh_time_hour;
                                ewtdList.Add(ewtd);
                                sCrudeName = timeReports[i].crude[j].name;
                            }
                            ewt.month = timeReports[i].month_year;
                            ewt.crudeKey = sKey.ToString();
                            ewt.crudeName = sCrudeName;
                            ewt.expertWorkTimeDetail = ewtdList;
                            ewtList.Add(ewt);
                            sKey++;
                        }
                    }
                    ctr.expertWorkingTime = ewtList;
                }
            }
            #endregion
            return ctr;
        }

        #endregion

        #region Utilities
        //private Tracking GetDraftTracking(string status)
        //{
        //    Tracking draftTracking = new Tracking();
        //    Tracking finalTracking = new Tracking();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(status))
        //        {
        //            if (status == ConstantPrm.ACTION.DRAFT)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.DRAFT };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.SUBMIT };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_EXPERT_APPROVE)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_CREATE_FINAL_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_APPROVE_FINAL_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.SUBMIT };
        //            }
        //            else if (status == ConstantPrm.ACTION.APPROVED)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.APPROVED };
        //            }
        //            else
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.INACTIVE };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.INACTIVE };
        //            }
        //        }
        //    }
        //    catch (Exception) { }
        //    return draftTracking;
        //}

        //private Tracking GetFinalTracking(string status)
        //{
        //    Tracking draftTracking = new Tracking();
        //    Tracking finalTracking = new Tracking();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(status))
        //        {
        //            if (status == ConstantPrm.ACTION.DRAFT)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.DRAFT };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.SUBMIT };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_EXPERT_APPROVE)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_CREATE_FINAL_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.DRAFT };
        //            }
        //            else if (status == ConstantPrm.ACTION.WAITING_APPROVE_FINAL_CAM)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.SUBMIT };
        //            }
        //            else if (status == ConstantPrm.ACTION.APPROVED)
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.APPROVED };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.APPROVED };
        //            }
        //            else
        //            {
        //                draftTracking = new Tracking() { name = "draft", status = ConstantPrm.ACTION.INACTIVE };
        //                finalTracking = new Tracking() { name = "final", status = ConstantPrm.ACTION.INACTIVE };
        //            }
        //        }
        //    }
        //    catch (Exception) { }
        //    return finalTracking;
        //}

        private Tracking GetDraftStatusTracking(string draftStatus)
        {
            Tracking draftTracking = new Tracking();
            try
            {
                if (!string.IsNullOrEmpty(draftStatus))
                {
                    string[] splitVal = draftStatus.Split(':');
                    if (splitVal.Length > 3)
                    {
                        string unitDraft = "draft";
                        string statusDraft = splitVal[1];
                        string nameDraft = splitVal[2];
                        string dateDraft = splitVal[3];
                        if (statusDraft == ConstantPrm.ACTION.DRAFT) { statusDraft = ConstantPrm.ACTION.DRAFT; }
                        else if (statusDraft == ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM) { statusDraft = ConstantPrm.ACTION.SUBMIT; }
                        else if (statusDraft == ConstantPrm.ACTION.WAITING_EXPERT_APPROVE) { statusDraft = ConstantPrm.ACTION.APPROVED; }
                        else if (statusDraft == ConstantPrm.ACTION.WAITING_CREATE_FINAL_CAM) { statusDraft = ConstantPrm.ACTION.APPROVED; }
                        else if (statusDraft == ConstantPrm.ACTION.WAITING_APPROVE_FINAL_CAM) { statusDraft = ConstantPrm.ACTION.APPROVED; }
                        else if (statusDraft == ConstantPrm.ACTION.APPROVED) { statusDraft = ConstantPrm.ACTION.APPROVED; }
                        else { statusDraft = ConstantPrm.ACTION.INACTIVE; }
                        draftTracking = new Tracking() { unit = unitDraft, status = statusDraft, name = nameDraft, date = dateDraft };
                    }
                }
            }
            catch (Exception) { }
            return draftTracking;
        }

        private Tracking GetFinalStatusTracking(string finalStatus)
        {
            Tracking finalTracking = new Tracking();
            try
            {
                if (!string.IsNullOrEmpty(finalStatus))
                {
                    string[] splitVal = finalStatus.Split(':');
                    if (splitVal.Length > 3)
                    {
                        string unitFinal = "final";
                        string statusFinal = splitVal[1];
                        string nameFinal = splitVal[2];
                        string dateFinal = splitVal[3];
                        if (statusFinal == ConstantPrm.ACTION.DRAFT) { statusFinal = ConstantPrm.ACTION.DRAFT; }
                        else if (statusFinal == ConstantPrm.ACTION.WAITING_APPROVE_DRAFT_CAM) { statusFinal = ConstantPrm.ACTION.DRAFT; }
                        else if (statusFinal == ConstantPrm.ACTION.WAITING_EXPERT_APPROVE) { statusFinal = ConstantPrm.ACTION.DRAFT; }
                        else if (statusFinal == ConstantPrm.ACTION.WAITING_CREATE_FINAL_CAM) { statusFinal = ConstantPrm.ACTION.DRAFT; }
                        else if (statusFinal == ConstantPrm.ACTION.WAITING_APPROVE_FINAL_CAM) { statusFinal = ConstantPrm.ACTION.SUBMIT; }
                        else if (statusFinal == ConstantPrm.ACTION.APPROVED) { statusFinal = ConstantPrm.ACTION.APPROVED; }
                        else { statusFinal = ConstantPrm.ACTION.INACTIVE; }
                        finalTracking = new Tracking() { unit = unitFinal, status = statusFinal, name = nameFinal, date = dateFinal };
                    }
                }
            }
            catch (Exception) { }
            return finalTracking;
        }

        private List<Tracking> GetExpertTracking(string exportUnit)
        {
            List<Tracking> expertTracking = new List<Tracking>();
            try
            {
                if (!string.IsNullOrEmpty(exportUnit))
                {
                    string[] itemExportUnits = exportUnit.Split('|');
                    if (itemExportUnits.Length > 0)
                    {
                        foreach (var itemExportUnit in itemExportUnits)
                        {
                            if (!string.IsNullOrEmpty(itemExportUnit))
                            {
                                string[] splitVal = itemExportUnit.Split(':');
                                if (splitVal.Length > 3)
                                {
                                    string unitExportUnit = splitVal[0];
                                    string statusExportUnit = splitVal[1];
                                    string nameExportUnit = splitVal[2];
                                    string dateExportUnit = splitVal[3];
                                    if (statusExportUnit == ConstantPrm.ACTION.DRAFT) { statusExportUnit = ConstantPrm.ACTION.DRAFT; }
                                    else if (statusExportUnit == ConstantPrm.ACTION.SUBMIT) { statusExportUnit = ConstantPrm.ACTION.SUBMIT; }
                                    else if (statusExportUnit == ConstantPrm.ACTION.APPROVE) { statusExportUnit = ConstantPrm.ACTION.APPROVE; }
                                    else if (statusExportUnit == ConstantPrm.ACTION.REJECT) { statusExportUnit = ConstantPrm.ACTION.DRAFT; }     // DRAFT FOR EXPERT
                                    else if (statusExportUnit == ConstantPrm.ACTION.APPROVE_2) { statusExportUnit = ConstantPrm.ACTION.DRAFT; }     // DRAFT FOR EXPERT
                                    else if (statusExportUnit == ConstantPrm.ACTION.APPROVE_3) { statusExportUnit = ConstantPrm.ACTION.SUBMIT; }    // SUBMIT FOR EXPERT
                                    else if (statusExportUnit == ConstantPrm.ACTION.APPROVED) { statusExportUnit = ConstantPrm.ACTION.APPROVED; }
                                    else { statusExportUnit = ConstantPrm.ACTION.INACTIVE; }
                                    expertTracking.Add(new Tracking() { unit = unitExportUnit, status = statusExportUnit, name = nameExportUnit, date = dateExportUnit });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception) { }
            return expertTracking;
        }
        #endregion

        #region Approval
        public ActionResult ApproveCool()
        {
            //ForTest
            //http://localhost:50131/CPAIMVC/Cool/ApproveCool?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                                if (!string.IsNullOrEmpty(itemFunc.FTX_INDEX4))
                                {
                                    if (itemFunc.FTX_INDEX4 == "DRAFT" || itemFunc.FTX_INDEX4 == "WAITING APPROVE DRAFT CAM")
                                    {
                                        urlPage = "Cool";
                                    }
                                    else if (itemFunc.FTX_INDEX4 == "WAITING EXPERT APPROVAL")
                                    {
                                        urlPage = "Cool/CoolExpert";
                                    }
                                    else if (itemFunc.FTX_INDEX4 == "WAITING CREATE FINAL CAM" || itemFunc.FTX_INDEX4 == "WAITING APPROVE FINAL CAM" || itemFunc.FTX_INDEX4 == "APPROVED")
                                    {
                                        urlPage = "Cool/CoolFinalCAM";
                                    }
                                    else
                                    {
                                        urlPage = "Cool";
                                    }
                                }
                                else
                                {
                                    urlPage = "Cool";
                                }
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        path = "~/CPAIMVC/" + urlPage + "?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt();
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }
        #endregion
    }
}