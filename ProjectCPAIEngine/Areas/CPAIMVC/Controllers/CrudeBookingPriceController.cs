﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class CrudeBookingPriceController : BaseController
    {

        public ActionResult index()
        {
            return RedirectToAction("Create", "CrudeBookingPrice");
        }

        [HttpGet]
        public ActionResult Create()
        {
            CrudeBookingPriceViewModel model = new CrudeBookingPriceViewModel();
            return View(model);
        }

    }
}