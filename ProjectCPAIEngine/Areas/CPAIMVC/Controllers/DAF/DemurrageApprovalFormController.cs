﻿using com.pttict.engine.dal.Entity;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALDAF;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.DAF
{
    public class DemurrageApprovalFormController : BaseController
    {
        ShareFn _FN = new ShareFn();
        USER_GROUP_DAL ugd = new USER_GROUP_DAL();
        const string returnPage = "../web/MainBoards.aspx";
        const string JSON_CRUDE_PURCHASE = "JSON_CRUDE_PURCHASE";

        // GET: CPAIMVC/DemurrageApprovalForm
        //public ActionResult Index()
        //{
        //    return View();
        //}


        public ActionResult Search()
        {
            //UserGroupDAL objUserGroup = new UserGroupDAL();
            //CPAI_USER_GROUP findByUserAndSystem = objUserGroup.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.DAF);
            DAFSearchViewModel vm = new DAFSearchViewModel();
            return SearchResult(vm);
            //var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.CRUDE_PURCHASE);

            //msdata = msdata.OrderBy(x => x).ToList();

            //List<SelectListItem> userList = new List<SelectListItem>();

            //foreach (var a in msdata)
            //{
            //    userList.Add(new SelectListItem { Text = a, Value = a });
            //}

            //ViewBag.getFeedstock = getFeedStock();
            //ViewBag.getProduct = DropdownServiceModel.getMaterial();
            //ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            //ViewBag.getSupplier = DropdownServiceModel.getVendorFreight("FEED");
            //ViewBag.getUsers = userList.Distinct().ToList();
            //ViewBag.UserGroup = findByUserAndSystem.USG_USER_GROUP;

            //return View(vm);
        }

        [HttpPost]
        public ActionResult SearchResult(DAFSearchViewModel vm)
        {
            //UserGroupDAL objUserGroup = new UserGroupDAL();
            CPAI_USER_GROUP ug = ugd.findByUserAndSystem(lbUserName, ConstantPrm.SYSTEM.DAF);
            List<USERS> findUserByGroup = ugd.findUserByGroup(ug.USG_USER_GROUP);
            var msdata = MasterData.GetAllUer(lbUserName, ConstantPrm.SYSTEM.DAF);

            msdata = msdata.OrderBy(x => x).ToList();

            List<SelectListItem> userList = new List<SelectListItem>();

            foreach (var a in msdata)
            {
                userList.Add(new SelectListItem { Text = a, Value = a });
            }
            //userList = userList.Distinct().ToArray();

            DAF_MASTER_DAL masMan = new DAF_MASTER_DAL();
            List<SELECT_CRITERIA> vessels_raw = masMan.GetVesselList();
            List<SelectListItem> vessels_list = new List<SelectListItem>();
            foreach (var a in vessels_raw)
            {
                vessels_list.Add(new SelectListItem { Text = a.VALUE, Value = a.ID });
            }

            userList = userList.Distinct().ToList();
            ViewBag.getFeedstock = getFeedStock();
            ViewBag.getProduct = DropdownServiceModel.getMaterial();
            ViewBag.getCrude = MaterialsServiceModel.getMaterialsDDL();
            ViewBag.getSupplier = DropdownServiceModel.getVendorFreight("FEED");
            ViewBag.getVessels = vessels_list;
            ViewBag.getUsers = userList.Distinct().ToList();
            if(ug.USG_USER_GROUP != null && ug.USG_USER_GROUP.Length > 3)
            {
                ViewBag.UserGroup = ug.USG_USER_GROUP.Substring(0, 4);

            }


            string datePurchase = vm.search_DatePurchase;
            string stDate = "";
            string enDate = "";
            //string feedstock = fm["feedstock"].ToString();
            //string product = fm["product"].ToString();
            //string supplier = fm["supplier"].ToString();
            string userlist = "";
            if (vm.search_User != null)
            {
                userlist = vm.search_User.ToString().Replace(",", "|");
            }

            if (!String.IsNullOrEmpty(datePurchase))
            {
                string[] s = datePurchase.Split(new[] { " to " }, StringSplitOptions.None);
                stDate = _FN.ConvertDateFormat(s[0], true).Replace("-", "/");
                enDate = _FN.ConvertDateFormat(s[1], true).Replace("-", "/");
            }

            List_DAFtrx res = SearchDemurrageApprovalFormData(stDate, enDate, vm.vessel, userlist);
            vm.DAFTransaction = res.DAFTransaction.OrderByDescending(x => x.transaction_id).ToList();

            for (int i = 0; i < res.DAFTransaction.Count; i++)
            {
                vm.DAFTransaction[i].date_purchase = _FN.ConvertDateFormatBackFormat(vm.DAFTransaction[i].date_purchase, "MMMM dd yyyy");
            }

            return View("Search", vm);
        }

        private List_DAFtrx SearchDemurrageApprovalFormData(string sDate, string eDate, string vessel, string create_by)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000081;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = lbUserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.DAF });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = sDate });
            req.Req_parameters.P.Add(new P { K = "to_date", V = eDate });
            req.Req_parameters.P.Add(new P { K = "function_code", V = "80" });
            req.Req_parameters.P.Add(new P { K = "vessel", V = vessel });
            req.Req_parameters.P.Add(new P { K = "create_by", V = create_by });
            //req.Req_parameters.P.Add(new P { K = "index_10", V = product });
            //req.Req_parameters.P.Add(new P { K = "index_12", V = supplier });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_DAFtrx _model = ShareFunction.DeserializeXMLFileToObject<List_DAFtrx>(_DataJson);
            if (_model == null) _model = new List_DAFtrx();
            if (_model.DAFTransaction == null) _model.DAFTransaction = new List<DAFEncrypt>();
            return _model;
        }


        private List<SelectListItem> getFeedStock(bool isHidden = false)
        {
            //LoadMaster From JSON
            Setting setting = JSONSetting.getSetting(JSON_CRUDE_PURCHASE);
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 0; i < setting.feedstock.Count; i++)
            {
                list.Add(new SelectListItem { Text = setting.feedstock[i].key, Value = isHidden ? setting.feedstock[i].unit : setting.feedstock[i].key });
            }
            return list;
        }

        public ActionResult ApproveEndpoint()
        {
            //ForTest
            //http://localhost:50131/DAF/CPAIMVC/DemurrageApprovalForm/ApproveEndpoint?token=89b7e01a8af24dc98102e6be1d3470a3
            
            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        [NonAction]
        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        [NonAction]
        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {

                        urlPage = "~/CPAIMVC/MainApprovalForm/#/demurrage-approval/";
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;

                        DAF_DATA_DAL daf_dal = new DAF_DATA_DAL();
                        DAF_DATA _item = daf_dal.Get(strTransID);
                        urlPage += _item.DDA_USER_GROUP + "/" + strReqID + "/" + strTransID + "/edit";
                        path = urlPage;
                    }
                }
            }
            return path;
        }

    }
}