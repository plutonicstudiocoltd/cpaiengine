﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class GIClearLineController:BaseController
    {
        public ActionResult index()
        {
            GIClearLineViewModel model;

            if (TempData["tempGIClearLine"] != null)
            {
                model = (GIClearLineViewModel)TempData["tempGIClearLine"];
            }
            else
            {
                model = initialModel();
            }                

            return View(model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(GIClearLineViewModel pModel)
        {

            GIClearLineServiceModel serviceModel = new GIClearLineServiceModel();

            GIClearLineViewModel_Search searchModel = new GIClearLineViewModel_Search();
            searchModel = pModel.GIClearLine_Search;
           
            serviceModel.Search(ref pModel);          
            if (TempData["tempGIClearLine"] != null) { TempData.Remove("tempGIClearLine"); }
            serviceModel.setInitail_ddl(ref pModel);
            TempData["tempGIClearLine"] = pModel;
            setInitialModel(ref pModel);
            return View("Index", pModel);
        }

        [HttpGet]
        public ActionResult Search()
        {
            GIClearLineServiceModel serviceModel = new GIClearLineServiceModel();
            GIClearLineViewModel model = new GIClearLineViewModel();
            if (TempData["tempGIClearLine"] != null)
            {
                model = (GIClearLineViewModel)TempData["tempGIClearLine"];

                TempData.Remove("tempGIClearLine");
            }
            else
            {
                model = initialModel();
            }

            serviceModel.setInitail_ddl(ref model);

            return View("Index", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Update")]
        public ActionResult Update(FormCollection frm, GIClearLineViewModel pModel)
        {
            GIClearLineServiceModel serviceModel = new GIClearLineServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = serviceModel.UpdateData(ref pModel);

            if (TempData["tempGIClearLine"] != null) { TempData.Remove("tempGIClearLine"); }

            serviceModel.setInitail_ddl(ref pModel);

            TempData["tempGIClearLine"] = pModel;
            TempData["ReponseStatus"] = rtn.Status.ToString();
            TempData["ReponseMessage"] = rtn.Message;
           

            
            setInitialModel(ref pModel);
            return View("Index", pModel);
        }

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Cancel")]
        //public ActionResult Cancel(FormCollection frm, GIClearLineViewModel pModel)
        //{
        //    GIClearLineServiceModel serviceModel = new GIClearLineServiceModel();

        //    serviceModel.CancelData(ref pModel);

        //    if (TempData["tempGIClearLine"] != null) { TempData.Remove("tempGIClearLine"); }

        //    serviceModel.setInitail_ddl(ref pModel);

        //    TempData["tempGIClearLine"] = pModel;

        //    return View("Index", pModel);
        //}

        public GIClearLineViewModel initialModel()
        {
            GIClearLineServiceModel serviceModel = new GIClearLineServiceModel();
            GIClearLineViewModel model = new GIClearLineViewModel();
          
            model.GIClearLine_Search = new GIClearLineViewModel_Search();
            model.GIClearLine_Search.SearchData = new List<GIClearLineViewModel_SearchData>();
            
            model.ddl_StorageLoc = serviceModel.getStorageLocation();

            model.ddl_Type = DropdownServiceModel.getGICLEARLINEType();
        

            return model;
        }
        public void setInitialModel(ref GIClearLineViewModel pModel)
        {
            if(pModel.GIClearLine_Search == null) { pModel.GIClearLine_Search = new GIClearLineViewModel_Search(); }
            if (pModel.ddl_Type == null) { pModel.ddl_Type = new List<SelectListItem>(); }
            pModel.ddl_Type = DropdownServiceModel.getGICLEARLINEType();
        }
    }
}

//GIClearLine_Search