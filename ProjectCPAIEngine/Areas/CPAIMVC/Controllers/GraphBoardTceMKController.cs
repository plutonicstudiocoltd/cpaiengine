﻿using com.pttict.engine.utility;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.CPAI;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using static ProjectCPAIEngine.Model.TceMkDetail;

using ProjectCPAIEngine.Areas.CPAIMVC.Models;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class GraphBoardTceMKController : BaseController
    {
        ShareFn _FN = new ShareFn();
        DALGraphBoard _dalGraphBoard = new DALGraphBoard();
        string returnPage = "/CPAIMVC/graphboardTCEMK";
        private List<Transaction> SSVoyCharterIn
        {
            get
            {
                if (Session["SSVoyCharterIn"] == null) return new List<Transaction>();
                else return (List<Transaction>)Session["SSVoyCharterIn"];
            }
            set
            {
                Session["SSVoyCharterIn"] = value;
            }
        }
        private List<CHI_DATA> SSCHIData
        {
            get
            {
                if (Session["SSCHIData"] == null) return null;
                else return (List<CHI_DATA>)Session["SSCHIData"];
            }
            set
            {
                Session["SSCHIData"] = value;
            }
        }
        public ActionResult Index()
        {
            ViewModels.GraphBoardViewModel _Vmodel = new ViewModels.GraphBoardViewModel();
            _Vmodel.lstVesselSearch = getVehicleSearch();
            _Vmodel.lstVoyNo = new List<SelectListItem>();
            List<TCEMKData> _trData = new List<TCEMKData>();
            SSVoyCharterIn = null;
            _Vmodel.GraphDate = MakeGraph( new List<string>(), ref _trData);
            return View(_Vmodel);
        }
        public ActionResult AddTceMK()
        {
            TCEMKViewModel _tceMKViewModel = new TCEMKViewModel();
            TCEMK tceMK;
            string jsonTCEMK;

            if (Request.QueryString["transaction_id"] != null)
            {
                initializeDropDownListFixture(ref _tceMKViewModel);
                _tceMKViewModel.TceMKDetail = new TceMkDetail();
                LoadDataTCEMK(Request.QueryString["transaction_id"].Decrypt(), ref _tceMKViewModel);
                if(!string.IsNullOrEmpty(_tceMKViewModel.TceMKDetail.month))
                {
                    string[] splitmonth = _tceMKViewModel.TceMKDetail.month.Split('/');
                    if (splitmonth.Length >= 2)
                    {
                        _tceMKViewModel.LoadingMonth = splitmonth[0];
                        _tceMKViewModel.LoadingYear = splitmonth[1];
                    }
                    else
                    {
                        _tceMKViewModel.LoadingMonth = "";
                        _tceMKViewModel.LoadingYear = "";
                    }
                    
                }
                if (Request.QueryString["PURNO"] != null)
                {
                    _tceMKViewModel.TceMKDetail.voy_no = Request.QueryString["PURNO"].Decrypt();
                }

            }
            else
            {
                SSCHIData = null;
                _tceMKViewModel.TceMKDetail = new TceMkDetail();
                _tceMKViewModel.PageMode = true;
                initializeDropDownListFixture(ref _tceMKViewModel);
                _tceMKViewModel.LoadingMonth = DateTime.Now.ToString("MM");
                _tceMKViewModel.LoadingYear = DateTime.Now.ToString("yyyy");
                _tceMKViewModel.TceMKDetail.tc_num = "TC";

                jsonTCEMK = JSONSetting.getGlobalConfig("JSON_TCE_MK");
                if(string.IsNullOrEmpty(jsonTCEMK)==false)
                {
                    tceMK = (TCEMK)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonTCEMK.ToString(), typeof(TCEMK));
                    _tceMKViewModel.TceMKDetail.theoritical_days = tceMK.theoritical_days;
                    _tceMKViewModel.TceMKDetail.minload = tceMK.min_load;
                }
                else
                {
                    _tceMKViewModel.TceMKDetail.minload = "265000";
                    _tceMKViewModel.TceMKDetail.theoritical_days = "35";
                }
               
            }            

            return View(_tceMKViewModel);
        }
        public List<ButtonAction> ButtonListAddFixture
        {
            get
            {
                if (Session["ButtonListAddFixture"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListAddFixture"];
            }
            set { Session["ButtonListAddFixture"] = value; }
        }
        private void initializeDropDownListFixture(ref TCEMKViewModel _TCEMKViewModel)
        {
            _TCEMKViewModel.lstShipName = DropdownServiceModel.getVehicle("CRUDE", true);
            _TCEMKViewModel.lstTD = GetMasterTD();
            _TCEMKViewModel.lstPort = DropdownServiceModel.getPortName(true, "", "PORT_CMCS");
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, TCEMKViewModel model)
        {
            initializeDropDownListFixture(ref model);
            SaveData(DocumentActionStatus.Draft, ref model, form["hdfNoteAction"]);
            model.PageMode = true;
            return View("AddTceMK", model);
        }
        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, TCEMKViewModel model)
        {
            UpdateModel(form, model);
            initializeDropDownListFixture(ref model);
            SaveData(DocumentActionStatus.Submit, ref model, form["hdfNoteAction"]);
            return View("AddTceMK", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Back")]
        public ActionResult Back(FormCollection form, TCEMKViewModel model)
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, TCEMKViewModel model)
        {
            if (ButtonListAddFixture != null)
            {
                var _lstButton = ButtonListAddFixture.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _xml = _lstButton[0].call_xml;
                    string note = form["hdfNoteAction"];
                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"].ToString();
                    var json = new JavaScriptSerializer().Serialize(new TceMkRootObject() { tce_mk_detail = MakeModelTosave(model) });
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    if (resData != null && resData.result_code == "1")
                    {
                        if (_lstButton[0].name == ConstantPrm.ACTION.SAVE_DRAFT)
                        {
                            string PURNO = Request.QueryString["PURNO"];
                            string tranID = Request.QueryString["transaction_id"];
                            string reqID = Request.QueryString["Tran_Req_ID"].ToString();
                            string currUrl = string.Format("/CPAIMVC/GraphBoardTCEMK/AddTCEMK?transaction_id={0}&Tran_Req_ID={1}&PURNO={2}", tranID, reqID, PURNO);
                            TempData["res_message"] = resData.response_message;
                            TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + currUrl : currUrl;
                        }
                        else
                        {
                            TempData["res_message"] = resData.response_message;
                            TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
                        }
                    }
                    else
                    {
                        TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownListFixture(ref model);

            return View("AddTceMK", model);
        }

        #region-------------LoadMaster--------------------

        private List<SelectListItem> getVehicleSearch()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            selectList = DropdownServiceModel.getVehicleTceMK(true);
            return selectList;
        }

        private List<SelectListItem> GetMasterTD()
        {
            List<SelectListItem> lstTD = new List<SelectListItem>();
            List<string> lstdalTD = _dalGraphBoard.GetTD();
            foreach (string _item in lstdalTD)
            {
                lstTD.Add(new SelectListItem() { Text = _item, Value = _item });
            }
           
            return lstTD;
        }

        #endregion----------------------------------------

        #region-------------PageEvent---------------------
        public ActionResult getVoyCharterOutEvt(string DateSearch, string Vessel)
        {
            DateTime? _from = null; DateTime? _to = null;
            string[] Delivaeryrange = DateSearch.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2)
            {
                _from = _FN.ConvertDateFormatBackFormat("01/" + Delivaeryrange[0]);

                _to = _FN.ConvertDateFormatBackFormat("01/" + Delivaeryrange[1]);
            }
            else
            {
                _to = null;
                _from = null;
            }

            return Json(GetVoyCharterOut((_from==null)? _from:Convert.ToDateTime(_from),(_to==null)? _to: Convert.ToDateTime(_to), Vessel), JsonRequestBehavior.AllowGet);

        }

        public ActionResult getGraphData(string DateSearch, string VoyCharter, string Vessel, bool GenExcel = false)
        {
            List<string> lstVoy = VoyCharter.Split('|').ToList();
            if (string.IsNullOrEmpty(VoyCharter)) lstVoy = null;
            GraphBoardTceMKViewModel ViewModel = new GraphBoardTceMKViewModel();
            ViewModel.ActionMSG = "";


            DateTime? _from = null; DateTime? _to = null;
            string[] Delivaeryrange = DateSearch.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2)
            {
                _from = _FN.ConvertDateFormatBackFormat("01/" + Delivaeryrange[0]);

                _to = _FN.ConvertDateFormatBackFormat("01/" + Delivaeryrange[1]);
            }
            else
            {
                _to = null;
                _from = null;
                
            }

         
            SSVoyCharterIn = GetTransaction(_from, _to, Vessel);
       
            List<TCEMKData> lstTRData = new List<TCEMKData>();
            try
            {
                ViewModel.lstVoyNo = GetVoyCharterOut(SSVoyCharterIn);
                ViewModel.GraphDate = MakeGraph(lstVoy, ref lstTRData);
                ViewModel.TDData = lstTRData.OrderBy(x=>x.DateOrder).ToList();
                if (GenExcel)
                {
                    int Maxport = 0;int MaxDischange = 0;
                    MapTceDetailForExport(ref lstTRData,ref Maxport,ref MaxDischange);
                    ViewModel.ActionMSG = GenarateExcel(lstTRData.Where(x => x.Status == "" || x.Status == "SUBMIT").OrderBy(x => x.DateOrder).ToList(), lstVoy, Maxport, MaxDischange);
                }
            }
            catch (Exception ex)
            {
                ViewModel.ActionMSG = ex.Message;
            }
            return Json(ViewModel, JsonRequestBehavior.AllowGet);


        }

        private void MapTceDetailForExport(ref List<TCEMKData> lstTRData,ref int MaxPort,ref int MaxDisport)
        {
            List<string> lstIDWhere = new List<string>();
            foreach (var _item in lstTRData) { lstIDWhere.Add(_item.TranID); }
            List<TceMkDetail> lstTceDetail = new List<Model.TceMkDetail>();
            List<CPAI_TCE_MK> _lstTCE=  CPAI_TCE_MK_DAL.GetTCELst(lstIDWhere);
            List<CPAI_TCE_MK_PORT> _lstTCEPort = CPAI_TCE_MK_DAL.GetTCEPortLst(lstIDWhere);
            List<SelectListItem> listPortName = DropdownServiceModel.getPortName(true, "", "PORT_CMCS");
            foreach (var _item in lstTRData)
            {
                var _tce_mk = _lstTCE.Where(x => x.CTM_ROW_ID == _item.TranID).ToList();
                if (_tce_mk.Count > 0)
                {
                    DateTime _DTMonth = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat("01/" + _tce_mk[0].CTM_MONTH), System.Globalization.CultureInfo.InvariantCulture);

                    //_item.TCEDay = _DTMonth.ToString("dd MMM yyyy");
                    _item.TCEDay = _tce_mk[0].CTM_TCE_DAY;
                    _item.VesselName = DropdownServiceModel.getVehicle("CRUDE", true).Where(x=>x.Value== _tce_mk[0].CTM_FK_MT_VEHICLE).ToList().Select(x=>x.Text).FirstOrDefault();
                   _item.VoyNo = _tce_mk[0].CTM_TASK_NO;
                    _item.OperatingDay = _tce_mk[0].CTM_OPERATING_DAYS;
                    _item.BunkerUseFO = _tce_mk[0].CTM_BUNKER_FO;
                    _item.BunkerUseGO = _tce_mk[0].CTM_BUNKER_GO;
                   _item.Month = _DTMonth.ToString("MMM yyyy");
                    _item.TimeCharterCost = _tce_mk[0].CTM_TIME_CHARTER_COST;
                    _item.FlatRate = _tce_mk[0].CTM_FLAT_RATE;
                    _item.WSBITR = _tce_mk[0].CTM_WS_BITR_TD;
                    _item.SpotVariableCost = _tce_mk[0].CTM_SPOT_VARIABLE_COST;

                   
                    var LoadPort = _lstTCEPort.Where(x => x.TMP_FK_TCE_MK == _item.TranID && x.TMP_PORT_TYPE!=null && x.TMP_PORT_TYPE.ToUpper().Trim() == "L").ToList();
                    double portCost = 0;
                    foreach(var _itemLPort in LoadPort)
                    {
                        portCost += _FN.strTodouble(_itemLPort.TMP_POST_COST);
                        string PortName = (listPortName.Where(x => x.Value == _itemLPort.TMP_FK_PORT.ToString()).Select(x => x.Text).ToList().FirstOrDefault());
                        _item.PortCharge.Add(new TceMkLoadPort() { port= PortName, port_value=_itemLPort.TMP_POST_COST});
                    }
                    var DisPort = _lstTCEPort.Where(x => x.TMP_FK_TCE_MK == _item.TranID && x.TMP_PORT_TYPE != null && x.TMP_PORT_TYPE.ToUpper().Trim() == "D").ToList();
                    foreach (var _itemDPort in DisPort)
                    {
                        portCost +=_FN.strTodouble(_itemDPort.TMP_POST_COST);
                        _item.DischangePort.Add(new TceMkDisPort() { port = (listPortName.Where(x => x.Value == _itemDPort.TMP_FK_PORT.ToString()).Select(x => x.Text).ToList().FirstOrDefault()), port_value = _itemDPort.TMP_POST_COST });

                    }
                    if (_item.PortCharge.Count > MaxPort) MaxPort = _item.PortCharge.Count;
                    if (_item.DischangePort.Count > MaxDisport) MaxDisport = _item.PortCharge.Count;
                    //_item.TotalPortCost = portCost.ToString();
                    _item.totalfrieghtCost = _tce_mk[0].CTM_TOTAL_BUNKER_COST;// (_FN.strTodouble( _item.TimeCharterCost)+ portCost).ToString(); 
                    _item.TotalMarketCost = _tce_mk[0].CTM_TOTAL_MARKET_COST;
                    _item.Saving_LossFromMarket = _tce_mk[0].CTM_SAVING_LOSS_MARKET;
                    _item.CreateBy = _tce_mk[0].CTM_CREATED_BY;
                    _item.CharterOut = "";
                    _item.FOMT = _tce_mk[0].CTM_FO_MT;
                    _item.GOMT = _tce_mk[0].CTM_GO_MT;
                    _item.Demurrage = _tce_mk[0].CTM_DEMURRAGE;
                }
            }

        }

        public ActionResult getCHIDataInfo(string CHINo)
        {
            TceDetail _Detail = new Model.TceDetail();
            var CHIData = SSCHIData.Where(x => x.IDA_ROW_ID.ToUpper() == CHINo.ToUpper()).ToList();
            if (CHIData != null && CHIData.Count > 0)
            {//CHIData[0]
                _Detail.chi_data_id = CHIData[0].IDA_PURCHASE_NO;
                _Detail.date_fixture = DateTime.Now.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                _Detail.extra_cost = CHIData[0].IDA_P_EXTEN_COST;
                _Detail.flat_rate = CHIData[0].IDA_P_FLAT_RATE;
                _Detail.laycan_load_from = ((CHIData[0].IDA_P_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(CHIData[0].IDA_P_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)) + " to " + ((CHIData[0].IDA_P_LAYCAN_TO == null) ? "" : Convert.ToDateTime(CHIData[0].IDA_P_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                if (CHIData[0].CHI_OFFER_ITEMS != null && CHIData[0].CHI_OFFER_ITEMS.ToList().Count > 0)
                {

                    _Detail.dem = CHIData[0].CHI_OFFER_ITEMS.ToList()[0].IOI_FINAL_DEAL;
                    _Detail.ship_name = CHIData[0].CHI_OFFER_ITEMS.ToList()[0].IOI_FK_VEHICLE;
                    _Detail.ship_broker = CHIData[0].CHI_OFFER_ITEMS.ToList()[0].IOI_FK_VENDOR;
                    _Detail.ship_owner = CHIData[0].CHI_OFFER_ITEMS.ToList()[0].IOI_OWNER;
                    _Detail.td = "";
                    _Detail.top_fixture_value = "";
                    _Detail.min_load = "";
                    if (_Detail.dem != "" && (_Detail.dem.IndexOf("Demurrage") >= 0 || _Detail.dem.ToUpper().IndexOf("DEMURRAGE") >= 0))
                    {
                        if (_Detail.dem.IndexOf("Demurrage") >= 0) { _Detail.dem = _Detail.dem.Substring(_Detail.dem.IndexOf("Demurrage")); }
                        else if (_Detail.dem.ToUpper().IndexOf("DEMURRAGE") >= 0) { _Detail.dem = _Detail.dem.Substring(_Detail.dem.IndexOf("DEMURRAGE")); }
                    }
                }
                return Json(_Detail, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(_Detail, JsonRequestBehavior.AllowGet);
            }
        }

        private List<SelectListItem> GetVoyCharterOut(DateTime? _from, DateTime? _To, string Vessel = "")
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            SSVoyCharterIn = GetTransaction(_from, _To, Vessel);
            if (SSVoyCharterIn != null)
            {
                foreach (var item in SSVoyCharterIn.Where(x => x.status.ToUpper() == ConstantPrm.ACTION.SUBMIT).OrderByDescending(x => x.purchase_no).ToList())
                {
                    lst.Add(new SelectListItem() { Text = item.purchase_no, Value = item.purchase_no });
                }
            }

            return lst;
        }

        private List<SelectListItem> GetVoyCharterOut(List<Transaction> lstTransaction)
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            if (lstTransaction != null)
            {
                foreach (var item in lstTransaction.Where(x => x.status.ToUpper() == ConstantPrm.ACTION.SUBMIT).OrderByDescending(x => x.purchase_no).ToList())
                {
                    lst.Add(new SelectListItem() { Text = item.purchase_no, Value = item.purchase_no });
                }
            }

            return lst;
        }
        #endregion----------------------------------------

        private List<ViewModels.GraphBoardFromDate> MakeGraph(List<string> lstVoyNo, ref List<TCEMKData> LstTRData)
        {
            List<GraphBoardFromDate> LstGraphBoard = new List<GraphBoardFromDate>();
            //Generate Route
            #region--------------------Route--------------------


            foreach (var _item in SSVoyCharterIn)
            {
                if (lstVoyNo != null && lstVoyNo.Where(x => x == _item.purchase_no).ToList().Count<=0) continue;
                if (!string.IsNullOrEmpty(_item.month) && _item.month.Length == 7)
                {
                    DateTime _DTMonth = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat("01/" + _item.month), System.Globalization.CultureInfo.InvariantCulture);

                    TCEMKData _tdDataItem = new TCEMKData();
                    _tdDataItem.Status = _item.status;
                    _tdDataItem.TranID = _item.transaction_id;
                    _tdDataItem.TceDate = _DTMonth.ToString("yyyy/MM/dd");
                    _tdDataItem.TD = _item.td;
                    _tdDataItem.VesselName = _item.vessel;
                    _tdDataItem.VoyNo = _item.purchase_no;
                    _tdDataItem.Month = _item.month;
                    _tdDataItem.TCRate = _item.tc_rate;
                    _tdDataItem.OperatingDay = "";
                    _tdDataItem.FOMT = "";
                    _tdDataItem.GOMT = "";
                    _tdDataItem.TimeCharterCost = "";
                    _tdDataItem.PortCharge = new List<TceMkLoadPort>();
                    _tdDataItem.DischangePort = new List<TceMkDisPort>();
                    _tdDataItem.BunkerUseFO = "";
                    _tdDataItem.BunkerUseGO = "";
                    _tdDataItem.totalfrieghtCost = "";
                    _tdDataItem.FlatRate = "";
                    _tdDataItem.WSBITR = "";
                    _tdDataItem.TotalMarketCost = "";
                    _tdDataItem.Saving_LossFromMarket = "";
                    _tdDataItem.CharterOut = "";
                    _tdDataItem.SpotVariableCost = "";
                    _tdDataItem.CreateBy = "";
                    _tdDataItem.TCEBenefitDay = Decimal.Round(Decimal.Parse(_item.benefit_day)).ToString();
                    _tdDataItem.TCEBenefitVoy = _item.benefit_voy;
                    _tdDataItem.TCEDay = _item.tce_day;
                    _tdDataItem.ParamVal = "?transaction_id=" + _item.transaction_id.Encrypt() + "&Tran_Req_ID=" + _item.req_transaction_id.Encrypt()+"&PURNO="+ _item.purchase_no.Encrypt();
                    LstTRData.Add(_tdDataItem);

                }

            }
        


            #endregion------------------------------------------
            GraphBoardFromDate grphTCRate = new ViewModels.GraphBoardFromDate();
            GraphBoardFromDate grphTCEDay = new ViewModels.GraphBoardFromDate();
            List<List<double?>> _lstTCRate = new List<List<double?>>();
            List<List<double?>> _lstTCEday = new List<List<double?>>();

            foreach (var _item in LstTRData.OrderBy(x => x.TceDate))
            {
                DateTime _DTMonth = Convert.ToDateTime(_FN.ConvertDateFormatBackFormat("01/" + _item.Month), System.Globalization.CultureInfo.InvariantCulture);
                _item.DateOrder = _DTMonth.ToString("yyyyMMdd");
                _item.Month = _DTMonth.ToString("MMM yyyy");
                if (_item.Status.ToUpper() == ConstantPrm.ACTION.SUBMIT)
                {
                    if (string.IsNullOrEmpty(_item.TCRate) || _item.TCRate.ToUpper() == "NAN")
                    {
                        List<double?> str = new List<double?>();
                        str.Add(_FN.ToUnixTimestamp(_DTMonth));
                        str.Add(null);
                        _lstTCRate.Add(str);
                    }
                    else
                    {
                        List<double?> str = new List<double?>();
                        str.Add(_FN.ToUnixTimestamp(_DTMonth));
                        str.Add(double.Parse(_item.TCRate));
                        _lstTCRate.Add(str);
                    }
                    if (string.IsNullOrEmpty(_item.TCEDay) || _item.TCEDay.ToUpper() == "NAN")
                    {
                        List<double?> str = new List<double?>();
                        str.Add(_FN.ToUnixTimestamp(_DTMonth));
                        str.Add(null);
                        _lstTCEday.Add(str);
                    }
                    else
                    {
                        List<double?> str = new List<double?>();
                        str.Add(_FN.ToUnixTimestamp(_DTMonth));
                        str.Add(double.Parse(_item.TCEDay));
                        _lstTCEday.Add(str);
                    }
                }
            }

        
            grphTCRate.showInLegend = true;
            grphTCRate.visible = true;
            grphTCRate.name = "TCRate";
            grphTCRate.color = "#B600FF";
            grphTCRate.marker = new Marker() { symbol = "diamond", enabled = true, fillColor = "#FFFFFF", lineWidth = 5, lineColor = "#B600FF" };
            grphTCRate.data = _lstTCRate;


            grphTCEDay.showInLegend = true;
            grphTCEDay.visible = true;
            grphTCEDay.name = "TCE/Day";
            grphTCEDay.color = "#FFAE00";
            grphTCEDay.marker = new Marker() { symbol = "diamond", enabled = true, fillColor = "#FFFFFF", lineWidth = 5, lineColor = "#FFAE00" };
            grphTCEDay.data = _lstTCEday;
           

            LstGraphBoard.Add(grphTCRate);
            LstGraphBoard.Add(grphTCEDay);
            return LstGraphBoard;
        }

        private string GenarateExcel(List<TCEMKData> LstTRData, List<string> lstVoy,int Maxport,int MaxDisport)
        {

            try
            {
                Color _BorderColor = Color.Black;
                string _Time = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                //string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Areas", "CPAIMVC", "Content", "TmpFile", string.Format("GraphboardTceMK{0}.xlsx", _Time));
                string file_path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("GraphboardTceMK{0}.xlsx", _Time));
                string _urlPath = "";
                FileInfo excelFile = new FileInfo(file_path);
                using (ExcelPackage package = new ExcelPackage(excelFile))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.Add("TCE MK");
                   
                    ws.Cells.Style.Font.SetFromFont(new System.Drawing.Font("Calibri", 9));
                    
                    //ws.Cells["A1:P1"].Merge = true;
                    // ws.Cells["A1:P1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ws.Cells[1, 1].Value = LstTRData[0].VesselName;
                    ws.Cells[1, 1].Style.Font.Size = 10;
                    ws.Cells[1, 1].Style.Font.Bold = true;


                    ws.Cells["A2:P2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ws.Cells["A2:P2"].Style.Font.Bold = true;
                   
                    ws.Column(1).Width = 30;
                    int RowNo = 2;
                    
                    ws.Cells[RowNo, 1].Value = ""; RowNo++;
                    ws.Cells[RowNo, 1].Value = ""; RowNo++;
                    ws.Cells[RowNo, 1].Value = ""; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Month"; RowNo++;
                    ws.Cells[RowNo, 1].Value = ""; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                    ws.Cells[RowNo, 1].Value = "TC rate"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Operating days"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "FO (mt)"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "GO (mt)"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Time charter cost"; RowNo++;
                    //ws.Cells[RowNo, 1].Value = "Port"; RowNo++;
                    for (int i = 0; i < Maxport; i++)
                    {
                        ws.Cells[RowNo, 1].Value =(i==0)? "Port":""; RowNo++;//
                    }
                    //ws.Cells[RowNo, 1].Value = "No. of Loading Port "; RowNo++;
                    for (int i=0;i< Maxport; i++) {
                        ws.Cells[RowNo, 1].Value = "Port charge "; RowNo++;//
                    }
                    for (int i = 0; i < MaxDisport; i++)
                    {
                        ws.Cells[RowNo, 1].Value = "Discharge port" + (i + 1).ToString(); ; RowNo++;//
                    }                   
                    ws.Cells[RowNo, 1].Value = "Bunker used - FO"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Bunker used - GO"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Total freight cost (1)"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Flat rate"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "WS BITR TD2 (M-1)"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Total Market cost (2)"; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                    ws.Cells[RowNo, 1].Value = "Saving/Loss from Market (2)-(1)"; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                    ws.Cells[RowNo, 1].Value = "Charter out "; RowNo++;
                    ws.Cells[RowNo, 1].Value = "SPOT Variable cost (bunker+P/D)"; RowNo++;
                    ws.Cells[RowNo, 1].Value = "Demurrage"; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                    ws.Cells[RowNo, 1].Value = "TCE benefit/Day"; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                    ws.Cells[RowNo, 1].Value = "TCE benefit/Voy"; RowNo++;
                    ws.Cells[RowNo, 1].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[RowNo, 1].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                    ws.Cells[RowNo, 1].Value = "TCE /Day"; RowNo++;
                    int columnNo = 1;
                    foreach (var _item in LstTRData.OrderBy(x=>x.TceDate).ToList())
                    {
                        if (_item.Status != ConstantPrm.ACTION.SUBMIT) continue;
                        RowNo = 2;
                        columnNo++;
                        ws.Column(columnNo).Width = 15;
                        //System.Drawing.Color BLUE = System.Drawing.Color.DodgerBlue;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.CornflowerBlue);
                        ws.Cells[RowNo, columnNo].Value = _item.CreateBy; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.VoyNo; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = ""; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.SkyBlue);
                        ws.Cells[RowNo, columnNo].Value = _item.Month; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = ""; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                        ws.Cells[RowNo, columnNo].Value = _item.TCRate; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.OperatingDay; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.FOMT; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.GOMT; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.TimeCharterCost; RowNo++;
                        for (int i = 0; i < Maxport; i++)
                        {
                            if (_item.PortCharge.Count > i)
                            {
                                ws.Cells[RowNo, columnNo].Value = _item.PortCharge.OrderBy(x => x.port_order).ToList()[i].port; RowNo++;
                            }
                            else
                            {
                                ws.Cells[RowNo, columnNo].Value = ""; RowNo++;
                            }
                        }
                        for (int i = 0; i < Maxport; i++)
                        {
                            if (_item.PortCharge.Count > i)
                            {
                                ws.Cells[RowNo, columnNo].Value = _item.PortCharge.OrderBy(x=>x.port_order).ToList()[i].port_value; RowNo++;
                            }else
                            {
                                ws.Cells[RowNo, columnNo].Value = ""; RowNo++;
                            }
                        }
                        for (int i = 0; i < MaxDisport; i++)
                        {
                            if (_item.DischangePort.Count > i)
                            {
                                ws.Cells[RowNo, columnNo].Value = _item.DischangePort.OrderBy(x => x.port_order).ToList()[i].port_value; RowNo++;
                            }
                            else
                            {
                                ws.Cells[RowNo, columnNo].Value = ""; RowNo++;
                            }
                        }
                        ws.Cells[RowNo, columnNo].Value = _item.BunkerUseFO; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.BunkerUseGO; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.totalfrieghtCost; RowNo++;                        
                        ws.Cells[RowNo, columnNo].Value = _item.FlatRate; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.WSBITR; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.TotalMarketCost; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                        ws.Cells[RowNo, columnNo].Value = _item.Saving_LossFromMarket; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                        ws.Cells[RowNo, columnNo].Value = _item.CharterOut; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.SpotVariableCost; RowNo++;
                        ws.Cells[RowNo, columnNo].Value = _item.Demurrage; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                        ws.Cells[RowNo, columnNo].Value = _item.TCEBenefitDay; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                        ws.Cells[RowNo, columnNo].Value = _item.TCEBenefitVoy; RowNo++;
                        ws.Cells[RowNo, columnNo].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws.Cells[RowNo, columnNo].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.DarkKhaki);
                        ws.Cells[RowNo, columnNo].Value = _item.TCEDay; RowNo++;
                    }
                    AllBorders(ws.Cells[3, 1, RowNo, columnNo].Style.Border);
                    package.Save();
                    //_urlPath = _FN.GetSiteRootUrl(Path.Combine("Areas", "CPAIMVC", "Content", "TmpFile", string.Format("GraphboardTceMK{0}.xlsx", _Time)));
                    _urlPath = _FN.GetSiteRootUrl(Path.Combine("Web", "Report", "TmpFile", string.Format("GraphboardTceMK{0}.xlsx", _Time)));

                }
                return _urlPath;
            }
            catch (Exception ex)
            {
                throw new Exception("GenarateExcel >>" + ex.Message);
            }

        }
        private void AllBorders(OfficeOpenXml.Style.Border _borders)
        {
            _borders.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            _borders.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            _borders.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
            _borders.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
        }

        private void updateModel(FormCollection form, TCEMKViewModel model, bool isTextMode = false)
        {

        }

        private void BindButtonPermission(List<ButtonAction> Button)
        {
            foreach (ButtonAction _button in Button)
            {
                string _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                ViewBag.ButtonCode += _btn;
            }
            ViewBag.ButtonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            ButtonListAddFixture = Button;
        }

        private void SaveData(DocumentActionStatus _status, ref TCEMKViewModel _Vmodel, string NoteAction)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }
            var json = new JavaScriptSerializer().Serialize(new TceMkRootObject() { tce_mk_detail = MakeModelTosave(_Vmodel) });
            //add json to class for convert to xml.
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000015;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "current_action", V = CurrentAction });
            req.Req_parameters.P.Add(new P { K = "next_status", V = NextState });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_MK });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Req_parameters.P.Add(new P { K = "note", V = (NoteAction == "") ? "-" : NoteAction });
            req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            if (resData != null && resData.result_code == "1")
            {
                if (DocumentActionStatus.Draft == _status && resData.resp_parameters != null && resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList().Count > 0)
                {

                    string currUrl = string.Format("/CPAIMVC/GraphBoardTceMK/AddTceMK?TranID={1}&Tran_Req_ID={0}&PURNO={2}", resData.req_transaction_id.Encrypt(), resData.transaction_id.Encrypt(), resData.resp_parameters.Where(x => x.k.ToLower() == "purchase_no").ToList()[0].v.Encrypt());

                    TempData["res_message"] = resData.response_message;
                    TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + currUrl : currUrl;

                }
                else
                {
                    TempData["res_message"] = (resData.response_message==null)?resData.result_desc:resData.response_message;
                    TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
                }
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;

            }
        }

        private void UpdateModel(FormCollection from, TCEMKViewModel model)
        {

        }

        private TceMkDetail MakeModelTosave(TCEMKViewModel _model)
        {
            if (_model.TceMKDetail == null) _model.TceMKDetail = new TceMkDetail();
            if (_model.TceMKDetail.bunker_cost_fo == null) _model.TceMKDetail.bunker_cost_fo = "";
            if (_model.TceMKDetail.bunker_cost_go == null) _model.TceMKDetail.bunker_cost_go = "";
            if (_model.TceMKDetail.date_from == null) _model.TceMKDetail.date_from = "";
            if (_model.TceMKDetail.date_to == null) _model.TceMKDetail.date_to = "";
            if (_model.TceMKDetail.dis_ports == null) _model.TceMKDetail.dis_ports = new List<TceMkDisPort>();
            if (_model.TceMKDetail.load_ports == null) _model.TceMKDetail.load_ports = new List<TceMkLoadPort>();
            if (_model.TceMKDetail.flat_rate == null) _model.TceMKDetail.flat_rate = "";            
            if (_model.TceMKDetail.minload == null) _model.TceMKDetail.minload = "";
            if (_model.TceMKDetail.month == null) _model.TceMKDetail.month = "";
            if (_model.TceMKDetail.operating_days == null) _model.TceMKDetail.operating_days = "";
            if (_model.TceMKDetail.other_cost == null) _model.TceMKDetail.other_cost = "";
            if (_model.TceMKDetail.tc_rate == null) _model.TceMKDetail.tc_rate = "";
            if (_model.TceMKDetail.theoritical_days == null) _model.TceMKDetail.theoritical_days = "";
            if (_model.TceMKDetail.voy_no == null) _model.TceMKDetail.voy_no = "";
            if (_model.TceMKDetail.td == null) _model.TceMKDetail.td = "";
            if (_model.TceMKDetail.vessel == null) _model.TceMKDetail.vessel = "";

            for(int i=0;i< _model.TceMKDetail.dis_ports.Count;i++)
            {
                _model.TceMKDetail.dis_ports[i].port_order = (i + 1).ToString();
            }
            for (int i = 0; i < _model.TceMKDetail.load_ports.Count; i++)
            {
                _model.TceMKDetail.load_ports[i].port_order = (i + 1).ToString();
            }

            if (_model.TceMKDetail.date_from != "")
            {
                string[] splitDateLaycan = _model.TceMKDetail.date_from.SplitWord("to");
                if (splitDateLaycan.Length > 1)
                {
                    _model.TceMKDetail.date_from = splitDateLaycan[0].Trim();
                    _model.TceMKDetail.date_to = splitDateLaycan[1].Trim();
                }
                else
                {
                    _model.TceMKDetail.date_from = "";
                    _model.TceMKDetail.date_to = "";
                }
            }

            if (_model.TceMKDetail.loadingdate_from != "")
            {
                string[] splitDateLaycan = _model.TceMKDetail.loadingdate_from.SplitWord("to");
                if (splitDateLaycan.Length > 1)
                {
                    _model.TceMKDetail.loadingdate_from = splitDateLaycan[0].Trim();
                    _model.TceMKDetail.loadingdate_to = splitDateLaycan[1].Trim();
                }
                else
                {
                    _model.TceMKDetail.loadingdate_from = "";
                    _model.TceMKDetail.loadingdate_to = "";
                }
            }

            _model.TceMKDetail.month = _model.LoadingMonth + "/" + _model.LoadingYear;

            return _model.TceMKDetail;
        }

        private ResponseData LoadDataTCEMK(string FixtureID, ref TCEMKViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000017;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = FixtureID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_MK });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            model.PageMode = true;//false;ของเดิม Check อยู่แต่ Edit ก็สามารถเชคได้รอ Confirm ว่าต้องดักอะไรบ้าง
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            model.GenButton = true;
            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                if (_model.buttonDetail.Button.Where(x => x.name.ToUpper().IndexOf(ConstantPrm.ACTION.SAVE_DRAFT) >= 0).ToList().Count > 0)
                {
                    model.PageMode = true;
                }
                BindButtonPermission(_model.buttonDetail.Button);
                
            }
            else
            {
                model.PageMode = false;
            }
            if (_model.data_detail != null)
            {
                model.TceMKDetail = new JavaScriptSerializer().Deserialize<TceMkRootObject>(_model.data_detail).tce_mk_detail;
                model.TceMKDetail.date_from = model.TceMKDetail.date_from + " to " + model.TceMKDetail.date_to;
                model.TceMKDetail.loadingdate_from = model.TceMKDetail.loadingdate_from + " to " + model.TceMKDetail.loadingdate_to;
            }
            else
            {
                TempData["res_message"] = resData.response_message;
                TempData["returnPage"] = (ConstantPrm.AppName != "") ? "/" + ConstantPrm.AppName + returnPage : returnPage;
            }
            return resData;
        }

        private List<Transaction> GetTransaction(DateTime? _from, DateTime? _To,string Vessel="")
        {
            List<Transaction> _transaction = new List<Flow.Model.Transaction>();
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000016;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.TCE_MK });
            req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
            req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
            req.Req_parameters.P.Add(new P { K = "status", V = "" });
            req.Req_parameters.P.Add(new P { K = "from_date", V = "" });
            req.Req_parameters.P.Add(new P { K = "to_date", V = "" });
            if(_from!=null)req.Req_parameters.P.Add(new P { K = "index_18", V = Convert.ToDateTime(_from).ToString("MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)+"|"+ Convert.ToDateTime(_To).ToString("MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) });
            if(!string.IsNullOrEmpty( Vessel)) req.Req_parameters.P.Add(new P { K = "index_5", V = Vessel });
            //req.Req_parameters.P.Add(new P { K = "index_20", V = _To.ToString("MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) });


            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;

            List_TCEtrx _model = ShareFunction.DeserializeXMLFileToObject<List_TCEtrx>(_DataJson);
            if (_model != null && _model.TCETransaction != null && _model.TCETransaction.Count > 0)
            {
                if (_model.TCETransaction.Count > 0)
                {

                    _transaction = _model.TCETransaction;
                }
            }

            return _transaction;
        }
    }
}