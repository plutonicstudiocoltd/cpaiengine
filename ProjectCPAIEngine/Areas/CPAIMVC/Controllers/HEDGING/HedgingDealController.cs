﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.Utilities;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using com.pttict.engine.dal.Entity;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Model;
using System.Web;
using System.Text;
using SelectPdf;
using com.pttict.engine.downstream;
using DSMail.service;
using DSMail.model;
using ProjectCPAIEngine.Flow.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingDealController : BaseController
    {
        #region Initial
        public string MCCTypeSS
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : string.Empty;
            }
            set
            {
                Session["Type"] = value;
            }
        }

        public List<ButtonAction> ButtonListHedgingDeal
        {
            get
            {
                if (Session["ButtonListHedgingDeal"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListHedgingDeal"];
            }
            set { Session["ButtonListHedgingDeal"] = value; }
        }

        ShareFn _FN = new ShareFn();

        public HedgingDealViewModel initialModel(string systemType)
        {
            HedgingDealViewModel resultModel = new HedgingDealViewModel();
            resultModel.HedgingDeal_Search = new HedgingDealViewModel_Search();
            resultModel.HedgingDeal_Search.sSearchData = new List<HedgingDealViewModel_SearchData>();

            resultModel.systemType = systemType;
            resultModel.ddlStatus = DropdownServiceModel.getStatus(false, string.Empty, DropdownServiceModel.HedgeStatus.Deal);
            resultModel.ddlHedged_Type = DropdownServiceModel.getHedgeType(false, string.Empty);
            resultModel.ddlTool = DropdownServiceModel.getTool(false, string.Empty);
            resultModel.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            resultModel.ddlCompany = DropdownServiceModel.getCompanyForDeal(false, string.Empty, "HG_CP_TOP");
            resultModel.ddlCounterParty = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_OTH");
            resultModel.ddlTrade_For = DropdownServiceModel.getTradeFor(false, string.Empty);

            resultModel.ddlCreateBy = new List<SelectListItem>();
            resultModel.ddlCreateBy.Add(new SelectListItem { Text = "Test1", Value = "TEST1" });
            resultModel.ddlCreateBy.Add(new SelectListItem { Text = "Test2", Value = "TEST2" });

            return resultModel;
        }

        private void initializeDropDownList(ref HedgingDealViewModel model)
        {
            model.ddlCompany = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_TOP").ToList();
            model.ddlCounterParty = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_OTH").ToList();
            model.ddlUnitFee = DropdownServiceModel.getUnitFee(false, string.Empty);
            model.ddlUnitPrice = DropdownServiceModel.getUnitPrice(false, string.Empty);
            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
            model.ddlUnderlyingName = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            model.ddlTool = DropdownServiceModel.getTool(false, "TOOL");
            model.ddlUnitPayment = DropdownServiceModel.getUnitPayment(false, string.Empty);

            if (model.hedgingDeal_Detail != null)
            {
                string underlying = "";
                string deal_date = "";
                if (model.hedgingDeal_Detail.underlying != null)
                    underlying = model.hedgingDeal_Detail.underlying.Split('|')[0];
                if (model.hedgingDeal_Detail.deal_date != null)
                    deal_date = model.hedgingDeal_Detail.deal_date.Substring(0, 10);

                if (model.hedgingDeal_Detail.underlying != null && model.hedgingDeal_Detail.deal_date != null)
                    model.ddlHedged_Type = DropdownServiceModel.getHedgeTypeForDeal(false, string.Empty, "06/07/2017", "MAT001");
                //model.ddlHedged_Type = DropdownServiceModel.getHedgeTypeForDeal(false, string.Empty, deal_date, underlying);//Main

                if (model.hedgingDeal_Detail.tenor.start_date != null && model.hedgingDeal_Detail.tenor.end_date != null)
                    model.hedgingDeal_Detail.tenor.start_end_date = model.hedgingDeal_Detail.tenor.start_date + " to " + model.hedgingDeal_Detail.tenor.end_date;
            }

            ViewBag.topId = getTopID();
        }
        #endregion

        #region Search

        [HttpGet]
        public ActionResult Search()
        {
            HedgingDealViewModel resultModel = initialModel(MCCTypeSS.Decrypt());
            ViewBag.TypeDecrypt = resultModel.systemType;
            ViewBag.SearchNew = "NEW";
            return View(resultModel);
        }

        [HttpPost]
        public ActionResult Search(HedgingDealViewModel postModel)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingDealViewModel model = initialModel(postModel.systemType);
            HedgingDealViewModel_Search search = new HedgingDealViewModel_Search();
            HedgingDealServiceModel service = new HedgingDealServiceModel();

            search = postModel.HedgingDeal_Search;
            search.systemType = postModel.systemType;
            rtn = service.Search(ref search);
            model.HedgingDeal_Search = search;
            modifyField(ref model);
            ViewBag.SearchNew = null;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["ReponseMessage"] = rtn.Message;
            return View(model);
        }

        private void modifyField(ref HedgingDealViewModel model)
        {
            if (model.HedgingDeal_Search.sSearchData != null)
            {
                foreach (var item in model.HedgingDeal_Search.sSearchData)
                {
                    var nameTool = model.ddlTool.SingleOrDefault(a => a.Value == item.dTool);
                    var nameUnderly = model.ddlUnderlying.SingleOrDefault(a => a.Value == item.dUnderlying);
                    var nameHedged = model.ddlHedged_Type.SingleOrDefault(a => a.Value == item.dHedged_Type);
                    var nameCompany = model.ddlCompany.SingleOrDefault(a => a.Value == item.dSeller);
                    var nameCounterparty = model.ddlCounterParty.SingleOrDefault(a => a.Value == item.dBuyer);

                    if (nameTool != null)
                        item.dTool = nameTool.Text;
                    if (nameUnderly != null)
                        item.dUnderlying = nameUnderly.Text;
                    if (nameHedged != null)
                        item.dHedged_Type = nameHedged.Text;
                    if (nameCompany != null)
                        item.dSeller = nameCompany.Text;
                    if (nameCounterparty != null)
                        item.dBuyer = nameCounterparty.Text;
                }
            }
        }

        #endregion

        #region MethodGet

        private ResponseData LoadData(string TransactionID, string Type, string Screen_Type, ref HedgingDealViewModel model)
        {
            const string returnPage = "../web/MainBoards.aspx";

            RequestCPAI req = new RequestCPAI();
            if (Screen_Type == "PREDEAL")
                req.Function_id = ConstantPrm.FUNCTION.F10000055;
            else if (Screen_Type == "DEAL")
                req.Function_id = ConstantPrm.FUNCTION.F10000049;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_DEAL });
            if (Screen_Type == "PREDEAL")
                req.Req_parameters.P.Add(new P { K = "type", V = Type });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);

            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.data_detail != null)
            {
                var modelRoot = new JavaScriptSerializer().Deserialize<HedgDealRootObject>(_model.data_detail);

                model.hedgingDeal_Detail = modelRoot.hedging_deal;
                model.hedgingDeal_reason = !string.IsNullOrEmpty(model.hedgingDeal_Detail.reason) ? model.hedgingDeal_Detail.reason : "-";

                if (model.hedgingDeal_Detail != null)
                {
                    if (!string.IsNullOrEmpty(model.hedgingDeal_Detail.unit_price) && !string.IsNullOrEmpty(model.hedgingDeal_Detail.volume_month_total_unit) && !string.IsNullOrEmpty(model.hedgingDeal_Detail.volume_month_unit))
                    {
                        model.hedgingDeal_Detail.underlying = model.hedgingDeal_Detail.underlying + "|" + model.hedgingDeal_Detail.unit_price + "|" + model.hedgingDeal_Detail.volume_month_total_unit + "|" + model.hedgingDeal_Detail.volume_month_unit;
                    }

                    if (!string.IsNullOrEmpty(model.hedgingDeal_Detail.fw_annual_id) && !string.IsNullOrEmpty(model.hedgingDeal_Detail.fw_hedged_type))
                    {
                        model.hedgingDeal_Detail.fw_annual_id = model.hedgingDeal_Detail.fw_annual_id + "|" + model.hedgingDeal_Detail.fw_hedged_type;
                    }
                }

                //if (model.chi_evaluating != null)
                //{
                //    if (!string.IsNullOrEmpty(model.chi_evaluating.laycan_from) && !string.IsNullOrEmpty(model.chi_evaluating.laycan_to))
                //    {
                //        model.ev_tempLaycan = model.chi_evaluating.laycan_from + " to " + model.chi_evaluating.laycan_to;
                //    }
                //    if (!string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chi_proposed_for_approve.laycan_to))
                //    {
                //        model.pa_tempLaycan = model.chi_proposed_for_approve.laycan_from + " to " + model.chi_proposed_for_approve.laycan_to;
                //    }
                //}
                //if (model.chit_evaluating != null)
                //{
                //    if (!string.IsNullOrEmpty(model.chit_evaluating.laycan_from) && !string.IsNullOrEmpty(model.chit_evaluating.laycan_to))
                //    {
                //        model.ev_tempLaycan = model.chit_evaluating.laycan_from + " to " + model.chit_evaluating.laycan_to;
                //    }
                //    if (!string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_from) && !string.IsNullOrEmpty(model.chit_proposed_for_approve.laycan_to))
                //    {
                //        model.pa_tempLaycan = model.chit_proposed_for_approve.laycan_from + " to " + model.chit_proposed_for_approve.laycan_to;
                //    }
                //}
            }
            else
            {
                TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                TempData["returnPage"] = ((Type.ToUpper() == ConstantPrm.SYSTEMTYPE.OTHER) ? "../" : "") + returnPage;
            }

            if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
            {
                model.buttonMode = "MANUAL";
                BindButtonPermission(_model.buttonDetail.Button, ref model);
            }

            return resData;
        }

        private void BindButtonPermission(List<ButtonAction> Button, ref HedgingDealViewModel model)
        {
            model.buttonMode = "MANUAL";
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
            {
                model.pageMode = "READ";
            }
            string _btn = "";
            foreach (ButtonAction _button in Button)
            {
                _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                model.buttonCode += _btn;
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            // model.buttonCode += "<input type='submit' value='MANUAL' id='btnSaveDraft' style='display: none;' name='action:SaveDraft' />";
            ButtonListHedgingDeal = Button;
        }

        private void updateModel(ref HedgingDealViewModel_Detail model)
        {
            if (model != null)
            {
                model.underlying = string.IsNullOrEmpty(model.underlying) ? "" : model.underlying.SplitWord("|")[0];

                if (!string.IsNullOrEmpty(model.tenor.start_end_date))
                {
                    string[] arr_date = model.tenor.start_end_date.SplitWord(" to ");
                    model.tenor.start_date = arr_date[0];
                    model.tenor.end_date = arr_date[1];
                }

                if (!string.IsNullOrEmpty(model.fw_annual_id))
                {
                    string[] arr_annual = model.fw_annual_id.SplitWord("|");
                    if (arr_annual.Length == 2)
                    {
                        model.fw_annual_id = arr_annual[0];
                        model.fw_hedged_type = arr_annual[1];
                    }
                }
            }
        }

        public string getTopID()
        {
            string topId = "CPAI1000017";
            return topId;
        }

        [HttpGet]
        public string getHedgedType(string date = "", string underlying = "")
        {
            return JSonConvertUtil.modelToJson(DropdownServiceModel.getHedgeTypeForDeal(false, string.Empty, "06/07/2017", "MAT001"));
            //return JSonConvertUtil.modelToJson(DropdownServiceModel.getHedgeTypeForDeal(false, string.Empty, date, underlying));//Main
        }

        [HttpGet]
        public ActionResult getCounterAdditional(string cp)
        {
            //string response = HedgingManagementCounterpartyServiceModel.getCounterAdditional("1000083");
            string response = HedgingManagementCounterpartyServiceModel.getCounterAdditional(cp);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSellerBuyer(string type)
        {
            var response = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, type).ToList();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        private void LoadXMLFromTest(ref HedgingDealViewModel model)
        {
            string PathTest = @"D:\Git\Json\Data_json_heading_deal.txt";
            if (System.IO.File.Exists(PathTest))
            {
                StreamReader _sr = new StreamReader(PathTest);
                string Line = _sr.ReadToEnd();

                model = new JavaScriptSerializer().Deserialize<HedgingDealViewModel>(Line);

                if (model.hedgingDeal_Detail != null)
                {
                    if (!string.IsNullOrEmpty(model.hedgingDeal_Detail.fw_annual_id))
                    {
                        model.hedgingDeal_Detail.fw_annual_id = model.hedgingDeal_Detail.fw_annual_id + "|" + model.hedgingDeal_Detail.fw_hedged_type;
                    }
                }
            }
        }

        #region Comment
        //public List<SellBuyPutCall> GetSellBuyPutCall()
        //{
        //    List<SellBuyPutCall> obj = new List<SellBuyPutCall>();
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t004", Order = 1, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t004", Order = 2, Name = "Buy Call", Value = "BuyCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t004", Order = 1, Name = "Buy Put", Value = "BuyPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t004", Order = 2, Name = "Sell Call", Value = "SellCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t005", Order = 1, Name = "Buy Put", Value = "BuyPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t005", Order = 2, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t005", Order = 3, Name = "Buy Call", Value = "BuyCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t005", Order = 1, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t005", Order = 2, Name = "Buy Put", Value = "BuyPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t005", Order = 3, Name = "Sell Call", Value = "SellCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t006", Order = 1, Name = "Sell Call", Value = "SellCall" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t006", Order = 1, Name = "Buy Call", Value = "BuyCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "t007", Order = 1, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "t007", Order = 1, Name = "Buy Put", Value = "BuyPut" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "Way", Order = 1, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "Way", Order = 2, Name = "Sell Call", Value = "SellCall" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "Way", Order = 3, Name = "Buy Put", Value = "BuyCall" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Seller", Tool = "Way", Order = 4, Name = "Buy Call", Value = "BuyCall" });

        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "Way", Order = 1, Name = "Sell Put", Value = "SellPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "Way", Order = 2, Name = "Sell Call", Value = "SellCall" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "Way", Order = 3, Name = "Buy Put", Value = "BuyPut" });
        //    obj.Add(new SellBuyPutCall { BuySell = "Buyer", Tool = "Way", Order = 4, Name = "Buy Call", Value = "SellCall" });

        //    return obj;
        //}


        //[HttpGet]
        //public ActionResult GetSellBuyPutCallByToolType(string tool, string type)
        //{
        //    if (tool == "2-Way" || tool == "3-Way" || tool == "4-Way")
        //        tool = "Way";
        //    var response = GetSellBuyPutCall().Where(m => m.Tool == tool && m.BuySell == type).ToList();
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        //[HttpGet]
        //public ActionResult GetJsonFramework(string hedgeType)
        //{
        //    string allText = System.IO.File.ReadAllText(@"D:\Git\Json\Data_json_heading_deal_Framework.txt");
        //    return Content(allText, "application/json");
        //}

        //[HttpGet]
        //public ActionResult GetJsonCommitteeFramework(string hedgeType)
        //{
        //    string allText = System.IO.File.ReadAllText(@"D:\Git\Json\Data_json_heading_deal_Committee_Framework.txt");
        //    return Content(allText, "application/json");
        //}
        #endregion

        #endregion

        #region Main

        public ActionResult Index()
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            //LoadXMLFromTest(ref model);
            initializeDropDownList(ref model);
            model.screenMode = "PREDEAL";
            ViewBag.screenMode = model.screenMode;
            model.pageMode = "NEW";

            return View(model);
        }

        public ActionResult Edit(string TranID, string Tran_Req_ID, string PURNO, string Type, string System, string Screen_Type)
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            initializeDropDownList(ref model);
            //string transID = HedgingDealServiceModel.getFNTransactionIDByDealNo(Predeal_No.Decrypt());
            LoadData(TranID.Decrypt(), Type.Decrypt(), Screen_Type.Decrypt(), ref model);
            model.screenMode = Screen_Type.Decrypt();
            ViewBag.screenMode = model.screenMode;

            if(model.hedgingDeal_Detail != null)
            {
                ViewBag.status = model.hedgingDeal_Detail.status ;

                if (model.hedgingDeal_Detail.status == "DRAFT" || model.hedgingDeal_Detail.status == "NEW")
                    model.pageMode = "EDIT";
                else
                    model.pageMode = "VIEW";

                return View("Index", model);
            }
            else
            {
                return RedirectToAction("Search", "HedgingDeal");
            }
          

            
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, HedgingDealViewModel model)
        {
            string note = form["hdfNoteAction"];
            DocumentActionStatus _status = DocumentActionStatus.Draft;

            ResponseData resData = null;
            if (model.screenMode == "PREDEAL")
                resData = SaveDataPreDeal(_status, model.hedgingDeal_Detail, note);
            else if (model.screenMode == "DEAL")
                resData = SaveDataDeal(_status, model.hedgingDeal_Detail, note);

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string deal_no = resData.resp_parameters[0].v.Encrypt();
                    string Type = MCCTypeSS.Encrypt(); //(model.hedgingDeal_Detail.labix_trade_thru_top == "T" ? "OTHER" : "TOP").Encrypt();
                    string System = ConstantPrm.SYSTEM.HEDG_DEAL.Encrypt();
                    string screen_type = "PREDEAL".Encrypt();
                    if (resData.resp_parameters[0].v.SplitWord("-")[0] == "DEAL")
                        screen_type = "DEAL".Encrypt();
                    string reason = model.hedgingDeal_reason.Encrypt();

                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    string path = string.Format("~/CPAIMVC/HedgingDeal/Edit?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Type={3}&System={4}&Screen_Type={5}", tranID, reqID, deal_no, Type, System, screen_type);
                    return Redirect(path);
                }
            }
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, HedgingDealViewModel model)
        {
            string note = form["hdfNoteAction"];
            DocumentActionStatus _status = DocumentActionStatus.Submit;

            ResponseData resData = null;
            if (model.screenMode == "PREDEAL")
                resData = SaveDataPreDeal(_status, model.hedgingDeal_Detail, note);
            else if (model.screenMode == "DEAL")
                resData = SaveDataDeal(_status, model.hedgingDeal_Detail, note);

            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
            initializeDropDownList(ref model);

            if (resData != null)
            {
                if (resData.result_code == "1" || resData.result_code == "2")
                {
                    string tranID = resData.transaction_id.Encrypt();
                    string reqID = resData.req_transaction_id.Encrypt();
                    string deal_no = resData.resp_parameters[0].v.Encrypt();
                    string Type = MCCTypeSS.Encrypt(); //(model.hedgingDeal_Detail.labix_trade_thru_top == "T" ? "OTHER" : "TOP").Encrypt();
                    string System = ConstantPrm.SYSTEM.HEDG_DEAL.Encrypt();
                    string screen_type = "PREDEAL".Encrypt();
                    if (resData.resp_parameters[0].v.SplitWord("-")[0] == "DEAL")
                        screen_type = "DEAL".Encrypt();
                    string reason = model.hedgingDeal_reason.Encrypt();

                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    string path = string.Format("~/CPAIMVC/HedgingDeal/Edit?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Type={3}&System={4}&Screen_Type={5}", tranID, reqID, deal_no, Type, System, screen_type);
                    return Redirect(path);
                }
            }
            return View("Index", model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, HedgingDealViewModel model)
        {
            if (ButtonListHedgingDeal != null)
            {
                var _lstButton = ButtonListHedgingDeal.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _evnt = _lstButton[0].name;
                    string _xml = _lstButton[0].call_xml;

                    string temp = form["hdfNoteAction"];
                    string note = string.IsNullOrEmpty(temp) ? "-" : temp;

                    List<ReplaceParam> _param = new List<ReplaceParam>();

                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();

                    HedgingDealViewModel_Detail dealDetail = model.hedgingDeal_Detail;
                    this.updateModel(ref dealDetail);

                    HedgDealRootObject obj = new HedgDealRootObject();
                    obj.hedging_deal = dealDetail;

                    var json = new JavaScriptSerializer().Serialize(obj);

                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });

                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);


                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2")
                        {
                            //string tranID = resData.transaction_id.Encrypt();
                            //string reqID = resData.req_transaction_id.Encrypt();
                            //string reason = "";//note.Encrypt();

                            //TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            //string path = string.Format("~/CPAIMVC/CharterIn?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            //if (MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) path = string.Format("~/CPAIMVC/CharterIn/CharterInCMMT?TranID={0}&Tran_Req_ID={1}&Reason={2}&Type={3}", tranID, reqID, reason, MCCTypeSS);
                            //return Redirect(path);


                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            string reason = note.Encrypt();
                            string deal_no = resData.resp_parameters[0].v.Encrypt();
                            string Type = MCCTypeSS.Encrypt();//(model.hedgingDeal_Detail.labix_trade_thru_top == "T" ? "OTHER" : "TOP").Encrypt();
                            string System = ConstantPrm.SYSTEM.HEDG_DEAL.Encrypt();
                            string screen_type = "PREDEAL".Encrypt();
                            if (resData.resp_parameters[0].v.SplitWord("-")[0] == "DEAL")
                                screen_type = "DEAL".Encrypt();

                            TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                            string path = string.Format("~/CPAIMVC/HedgingDeal/Edit?TranID={0}&Tran_Req_ID={1}&PURNO={2}&Type={3}&System={4}&Screen_Type={5}&Reason={6}", tranID, reqID, deal_no, Type, System, screen_type, reason);
                            return Redirect(path);
                        }
                    }
                }
            }
            else
            {
                TempData["res_message"] = "เกิดข้อผิดพลาด";
            }

            initializeDropDownList(ref model);
            TempData["HedginDeal_Model"] = model;
            return RedirectToAction("Index", "HedgingDeal");
        }

        private ResponseData SaveDataPreDeal(DocumentActionStatus _status, HedgingDealViewModel_Detail model, string note)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status)
            {
                CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT;
                model.status_tracking = CPAIConstantUtil.STATUS_DRAFT;
            }
            else if (DocumentActionStatus.Submit == _status)
            {
                CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT;
                model.status_tracking = CPAIConstantUtil.STATUS_SUBMIT;
            }

            this.updateModel(ref model);
            model.status = CurrentAction;

            HedgDealRootObject obj = new HedgDealRootObject();
            obj.hedging_deal = model;

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000053;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p
            {
                k = "channel",
                v = ConstantPrm.ENGINECONF.WEBChannel
            });
            req.req_parameters.p.Add(new p
            {
                k = "data_detail_input",
                v = json
            });

            var type = model.labix_trade_thru_top == "T" ? ConstantPrm.SYSTEMTYPE.OTHER : ConstantPrm.SYSTEMTYPE.TOP; //MCCTypeSS.Encrypt();
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.HEDG_DEAL });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        private ResponseData SaveDataDeal(DocumentActionStatus _status, HedgingDealViewModel_Detail model, string note)
        {
            string CurrentAction = "";
            string NextState = "";
            if (DocumentActionStatus.Draft == _status) { CurrentAction = ConstantPrm.ACTION.DRAFT; NextState = ConstantPrm.ACTION.DRAFT; }
            else if (DocumentActionStatus.Submit == _status) { CurrentAction = ConstantPrm.ACTION.SUBMIT; NextState = ConstantPrm.ACTION.SUBMIT; }

            HedgDealRootObject obj = new HedgDealRootObject();
            obj.hedging_deal = model;

            var json = new JavaScriptSerializer().Serialize(obj);

            RequestData req = new RequestData();
            req.function_id = ConstantPrm.FUNCTION.F10000037;
            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.state_name = "";
            req.req_parameters = new req_parameters();
            req.req_parameters.p = new List<p>();
            req.req_parameters.p.Add(new p
            {
                k = "channel",
                v = ConstantPrm.ENGINECONF.WEBChannel
            });
            req.req_parameters.p.Add(new p
            {
                k = "data_detail_input",
                v = json
            });
            var type = MCCTypeSS.Encrypt(); // model.labix_trade_thru_top == "T" ? ConstantPrm.SYSTEMTYPE.OTHER : ConstantPrm.SYSTEMTYPE.TOP }
            req.req_parameters.p.Add(new p { k = "current_action", v = CurrentAction });
            req.req_parameters.p.Add(new p { k = "next_status", v = NextState });
            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
            req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.HEDG_DEAL });
            req.req_parameters.p.Add(new p { k = "type", v = type });
            req.req_parameters.p.Add(new p { k = "note", v = string.IsNullOrEmpty(note) ? "-" : note });
            req.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            resData = service.CallService(req);

            return resData;
        }

        public ActionResult ApproveHEDG()
        {
            //ForTest
            //http://localhost:50131/CPAIMVC/CharterIn/ApproveCharterIn?token=89b7e01a8af24dc98102e6be1d3470a3

            string path = "~/web/login.aspx";
            if (Request.QueryString["token"] != null)
            {
                string TokenID = Request.QueryString["token"].ToString();
                path = CheckApprove(TokenID);
            }

            return Redirect(path);
        }

        public string CheckApprove(string sToken)
        {
            string urlPage = string.Empty;
            string strReqID = string.Empty;
            string strTransID = string.Empty;
            string strPurNo = string.Empty;
            string strSystem = string.Empty;
            string strScreenMode = string.Empty;
            string path = string.Empty;
            path = "~/web/login.aspx";

            UserPermissionDAL _cls = new UserPermissionDAL();
            List<userApprove> uApprove = _cls.CheckPermissionToken(sToken);

            if (uApprove != null)
            {
                if (uApprove.Count > 0)
                {
                    foreach (var item in uApprove)
                    {
                        MCCTypeSS = item.UserType.Encrypt();
                        FunctionTransactionDAL _func = new FunctionTransactionDAL();
                        List<FUNCTION_TRANSACTION> lstFunc = _func.findByTransactionId(item.TransactionID);
                        if (lstFunc != null)
                        {
                            foreach (var itemFunc in lstFunc)
                            {
                                strReqID = itemFunc.FTX_REQ_TRANS;
                                strPurNo = itemFunc.FTX_INDEX8;
                                strSystem = itemFunc.FTX_INDEX1;
                            }
                            strTransID = item.TransactionID;
                        }
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = item.UserName.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                        Const.User = _user;
                        strScreenMode = "DEAL";

                        path = "~/CPAIMVC/HedgingDeal/Edit?TranID=" + strTransID.Encrypt() + "&Tran_Req_ID=" + strReqID.Encrypt() + "&PURNO=" + strPurNo.Encrypt() + "&Type=" + MCCTypeSS + "&System=" + strSystem.Encrypt() + "&Screen_Type=" + strScreenMode.Encrypt();
                    }
                }
            }
            return path;
        }

        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }
        }
        #endregion

        public ActionResult GenerateContract(string dealNo)
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            model.dealNo = dealNo.Decrypt();
            HedgingDealServiceModel service = new HedgingDealServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = service.GetContact(ref model, model.dealNo);
            if (!rtn.Status)
            {
                var getContract = new ProjectCPAIEngine.DAL.DALHedg.HEDG_DEAL_CONTRACT_TMPLT_DAL(model.dealNo, CPAIConstantUtil.Hedg_Deal_ContrackType_LabixTrade).GetTemplate();
                model.htmlPDF = getContract.HCON_TMPLT_CONTENT;
            }
            return View("GenerateContract", model);
        }

        [HttpPost]
        public ActionResult GenerateContract(string htmlPdf, string deal_No, string contract_No)
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            model.contractPDFList = new List<HedgingDealContractPDF>();
            model.htmlPDF = htmlPdf;
            model.dealNo = deal_No.Decrypt();

            string last_seq = DAL.DALMaster.MasterData.GetSequences("HG_CONT_SEQ", "000");
            model.contractNo = String.IsNullOrEmpty(contract_No) ? last_seq + DateTime.Now.ToString("yy") : contract_No;  //+ GetCounterpartyCode(CounterID);

            model.htmlPDF = model.htmlPDF.Replace("#ContractNo", model.contractNo);
            GlobalProperties.LicenseKey = "SmF7anh/e2p+eWp7f2R6anl7ZHt4ZHNzc3M=";
            PdfDocument doc = new PdfDocument();
            SelectPdf.HtmlToPdf converter = new SelectPdf.HtmlToPdf();
            StringBuilder html = new StringBuilder();
            StringBuilder pdfData = new StringBuilder();
            int webPageWidth = 1024;
            int webPageHeight = 0;
            PdfPageSize pagesize = PdfPageSize.A4;
            PdfPageOrientation pdfOrientation = PdfPageOrientation.Portrait;
            pdfData = GetTemplate(model);
            html.Append(pdfData);
            converter.Options.PdfPageSize = pagesize;
            converter.Options.PdfPageOrientation = pdfOrientation;
            converter.Options.WebPageWidth = webPageWidth;
            converter.Options.WebPageHeight = webPageHeight;
            converter.Options.MarginTop = 30;
            converter.Options.MarginBottom = 30;
            doc = converter.ConvertHtmlString(html.ToString());
            var dir = new System.IO.DirectoryInfo(Server.MapPath("~/Web/FileUpload/ContractPDF/" + model.contractNo + "/"));
            if (!dir.Exists)
            {
                dir.Create();
            }
            System.IO.FileInfo[] fileNames = dir.GetFiles("*.*");
            var index = 0;
            index = fileNames != null ? fileNames.Count() + 1 : index + 1;
            string filenameurl = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload", "ContractPDF", model.contractNo, string.Format("{0}-{1}-{2}.pdf", "CONTRACT", model.contractNo, "V" + index));

            if (!System.IO.File.Exists(filenameurl))
            {
                System.IO.File.Delete(filenameurl);
            }
            doc.Save(filenameurl);
            doc.Close();

            //model.uploadFile = new List<string>();
            model.fileUrl = "Web/FileUpload/ContractPDF/" + model.contractNo + "/";
            var result = Path.GetFileName(filenameurl);
            model.fileContractPdf = result;
            //foreach (var item in fileNames)
            //{
            //    model.uploadFile.Add(item.Name);
            //    model.fileContractPdf = item.Name;
            //}
            var username = ViewBag.UserName as string;
            HedgingDealServiceModel service = new HedgingDealServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = service.SaveContact(model, username);
            ViewBag.saveStatus = rtn.Status;
            ViewBag.saveMessage = rtn.Message;
            return PartialView("_PDF_List", model);
        }

        public StringBuilder GetTemplate(HedgingDealViewModel model)
        {
            StringBuilder html = new StringBuilder();
            html.Append("<html>");
            html.Append("<style>");
            html.Append("img {display:inline-block; margin-left:auto; margin-right:auto;}");
            html.Append(@" </style>");
            html.Append(@"<body>" + model.htmlPDF + "</body>");
            html.Append(@"</html>");
            return html;
        }

        [HttpPost]
        public ActionResult GetTempMailContract(string filename, string pathfile, string dealNo)
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            model.dealNo = dealNo;
            var getContract = new ProjectCPAIEngine.DAL.DALHedg.HEDG_DEAL_CONTRACT_TMPLT_DAL(model.dealNo).GetTemplate();
            using (DAL.Entity.EntityCPAIEngine context = new DAL.Entity.EntityCPAIEngine())
            {
                var username = ViewBag.UserName as string;
                var objService = new UsersServiceModel();
                var user = objService.Get(username.ToLower());
                model.sendMailFrom = user.Email;//context.USERS.SingleOrDefault(a => a.USR_LOGIN.ToLower() == username.ToLower()).USR_EMAIL;
            }
            model.dealNo = dealNo;
            model.sendMailFile = filename;
            model.sendMailFileUrl = pathfile;
            model.sendMailTo = getContract.HCON_TMPLT_EMAIL;
            model.sendMailCC = getContract.HCON_TMPLT_EMAIL_CC;
            model.sendMailSubject = getContract.HCON_TMPLT_SUBJECT;
            return PartialView("_SendMailContract", model);
        }

        [HttpPost]
        public ActionResult SendMailContract(string deal_no,string sendMailFrom, string sendMailTo, string sendMailCC, string sendMailSubject, string sendMailFile, string sendMailFileUrl, string sendMailBody)
        {
            HedgingDealViewModel model = new HedgingDealViewModel();
            //System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-z0-9-]+(\.[a-z0-9-]+)*$");
            sendMailCC = sendMailCC.Replace(',',';');
            model.dealNo = deal_no;
            model.sendMailFrom = sendMailFrom;
            model.sendMailTo = sendMailTo;
            model.sendMailCC = sendMailCC;
            model.sendMailSubject = sendMailSubject;
            model.sendMailFile = sendMailFile;
            model.sendMailFileUrl = AppDomain.CurrentDomain.BaseDirectory+sendMailFileUrl+ sendMailFile;
            model.sendMailBody = sendMailBody;
            HedgingDealServiceModel service = new HedgingDealServiceModel();
            var username = ViewBag.UserName as string;
            service.SendEmail(model, username);
            return RedirectToAction("Search", "HedgingDeal");
        }


    }
}