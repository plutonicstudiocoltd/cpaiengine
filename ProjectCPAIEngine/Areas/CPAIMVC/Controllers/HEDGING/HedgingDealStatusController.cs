﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingDealStatusController : BaseController
    {
        #region Action
        public string MCCTypeSS
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : string.Empty; }
            set { Session["Type"] = value; }
        }

        [HttpGet]
        public ActionResult Search()
        {
            HedgingDealContactViewModel model = initialModel(MCCTypeSS.Decrypt());
            return View(model);
        }

        [HttpGet]
        public ActionResult TestLoadTemplate() {
            HedgingDealContactViewModel model = initialModel(MCCTypeSS.Decrypt());
            string tTemplate = new ProjectCPAIEngine.DAL.DALHedg.HEDG_DEAL_CONTRACT_TMPLT_DAL("DEAL-1707-0259").GetTemplate().HCON_TMPLT_CONTENT;
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingDealContactViewModel postModel)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingDealContactViewModel model = initialModel(postModel.systemType);
            HedgingDealContactViewModel_Search search = new HedgingDealContactViewModel_Search();
            HedgingDealContactServiceModel service = new HedgingDealContactServiceModel();

            search = postModel.Search;
            search.systemType = postModel.systemType;
            rtn = service.Search(ref search);
            model.Search = search;
            modifyField(ref model);

            TempData["ReponseStatus"] = rtn.Status;
            TempData["ReponseMessage"] = rtn.Message;
            return View(model);
        }
        #endregion

        #region Data
        private HedgingDealContactViewModel initialModel(string systemType)
        {
            HedgingDealContactViewModel model = new HedgingDealContactViewModel();
            model.Search = new HedgingDealContactViewModel_Search();
            model.Search.SearchData = new List<HedgingDealContactViewModel_SearchData>();

            model.systemType = systemType;
            model.ddlStatus = DropdownServiceModel.getStatus(false, string.Empty, DropdownServiceModel.HedgeStatus.Deal);
            model.ddlTool = DropdownServiceModel.getTool(false, string.Empty);
            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            model.ddlVS = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", false);
            model.ddlCompany = DropdownServiceModel.getCompanyForDeal(false, string.Empty, "HG_CP_TOP");
            model.ddlCounterParty = DropdownServiceModel.getCounterPartyForDeal(false, string.Empty, "HG_CP_OTH");

            return model;
        }

        private void modifyField(ref HedgingDealContactViewModel model)
        {
            if (model.Search.SearchData != null)
            {
                foreach (var item in model.Search.SearchData)
                {
                    var nameTool = model.ddlTool.SingleOrDefault(a => a.Value == item.dTool);
                    var nameUnderly = model.ddlUnderlying.SingleOrDefault(a => a.Value == item.dUnderlying);                    
                    var nameCompany = model.ddlCompany.SingleOrDefault(a => a.Value == item.dCompany);
                    var nameCounterparty = model.ddlCounterParty.SingleOrDefault(a => a.Value == item.dCounterparty);                    

                    if (nameTool != null)
                        item.dTool = nameTool.Text;
                    if (nameUnderly != null)
                        item.dUnderlying = nameUnderly.Text;                    
                    if (nameCompany != null)
                        item.dCompany = nameCompany.Text;
                    if (nameCounterparty != null)
                        item.dCounterparty = nameCounterparty.Text;                    
                }
            }
        }
        #endregion
    }
}