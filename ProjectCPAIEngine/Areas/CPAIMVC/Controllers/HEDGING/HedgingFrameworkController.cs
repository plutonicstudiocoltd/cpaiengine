﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingFrameworkController : BaseController
    {
        // GET: CPAIMVC/HedgingFramework

        public ActionResult Search()
        {
            HedgingFrameworkViewModel model = new HedgingFrameworkViewModel();
            model.hedgingFramework_Search = new HedgingFrameworkViewModel_Search();
            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
            model.ddlStatus = DropdownServiceModel.getStatus(false, string.Empty);
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchResult(FormCollection fm, HedgingFrameworkViewModel tempmodel)
        {
            HedgingFrameworkViewModel model = new HedgingFrameworkViewModel();
            HedgingFrameworkViewModel_Search searchmodel = new HedgingFrameworkViewModel_Search();
            model.hedgingFramework_Search = new HedgingFrameworkViewModel_Search();
            searchmodel = tempmodel.hedgingFramework_Search;
            if (searchmodel.sUnderlying != null)
            {
                string[] tempvalue = searchmodel.sUnderlying.Split('|');
                searchmodel.sUnderlying = tempvalue[0];
            }
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();


            service.Search(ref searchmodel);

            model.hedgingFramework_Search = searchmodel;
            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
            model.ddlStatus = DropdownServiceModel.getStatus(false, string.Empty);
            return View("Search", model);
        }

        public ActionResult Index()
        {
            HedgingFrameworkViewModel model = initialModel();
            model.pageMode = "NEW";
            model.hedgingFramework_Detail.Status = "NEW";
            model.hedgingFramework_Detail.IsChange = "0";
            model.hedgingFramework_Detail.DetailData = new List<HedgingFrameworkViewModel_DetailData>();
            model.hedgingFramework_Detail.DetailData.Add(new HedgingFrameworkViewModel_DetailData { Order = "1", ValueType = "FIX", HedgeType = "CROPPLAN", Type = "VOLUME" });
            return View(model);
        }

        public ActionResult ViewDetail(string OrderID,bool IsDup = false)
        {
            HedgingFrameworkViewModel model = initialModel();
            HedgingFrameworkServiceModel servicemodel = new HedgingFrameworkServiceModel();
            model.hedgingFramework_Detail = servicemodel.GetDetail(OrderID);
            if (IsDup)
            {
                model.hedgingFramework_Detail.Status = "COPY";
                model.hedgingFramework_Detail.OrderID = null;
                foreach (var list in model.hedgingFramework_Detail.DetailData)
                {
                    list.RowID = null;
                }
                model.pageMode = "NEW";
            }
            else
            {
                model.pageMode = "EDIT";
            }
            model.hedgingFramework_Detail.IsChange = "0";
            return View("index",model);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, HedgingFrameworkViewModel resultmodel)
        {
            HedgingFrameworkViewModel model = new HedgingFrameworkViewModel();
            HedgingFrameworkServiceModel servicemodel = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            HedgingFrameworkViewModel_Detail modelDetail = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkViewModel modelSet = initialModel();
            string oldstatus = modelDetail.Status;
            modelDetail = resultmodel.hedgingFramework_Detail;
            modelDetail.Status = ConstantPrm.ACTION.DRAFT;
            if (modelDetail.DetailData[0].Order == null)
            {
                modelDetail.DetailData[0].Order = "1";
                modelDetail.DetailData[0].ValueType = "FIX";
            }
            string[] tempvalue = resultmodel.hedgingFramework_Detail.Underlying.Split('|');
            modelDetail.Underlying = tempvalue[0];
            if (resultmodel.pageMode == "EDIT") {
                rtn = servicemodel.Edit(modelDetail, lbUserName);
            }
            else
            {
                rtn = servicemodel.Add(ref modelDetail, lbUserName);
                if (rtn.Status)
                {
                    model.pageMode = "EDIT";
                }
                else
                {
                    model.pageMode = "NEW";
                    model.hedgingFramework_Detail.Status = oldstatus;
                }
            }
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["OrderID"] = modelDetail.OrderID;
            modelSet.hedgingFramework_Detail = modelDetail;
            return View("Index", modelSet);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, HedgingFrameworkViewModel resultmodel)
        {
            HedgingFrameworkViewModel model = new HedgingFrameworkViewModel();
            HedgingFrameworkServiceModel servicemodel = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            HedgingFrameworkViewModel_Detail modelDetail = new HedgingFrameworkViewModel_Detail();
            HedgingFrameworkViewModel modelSet = initialModel();

            modelDetail = resultmodel.hedgingFramework_Detail;
            string prestatus = modelDetail.Status;
            modelDetail.Status = ConstantPrm.ACTION.SUBMIT;
            if (modelDetail.DetailData[0].Order == null)
            {
                modelDetail.DetailData[0].Order = "1";
                modelDetail.DetailData[0].ValueType = "FIX";
            }
            if(prestatus != ConstantPrm.ACTION.SUBMIT)
            {
                modelDetail.IsChange = "1";
                string[] tempvalue = resultmodel.hedgingFramework_Detail.Underlying.Split('|');
                modelDetail.Underlying = tempvalue[0];
            }

            if (resultmodel.pageMode == "EDIT")
            {
                rtn = servicemodel.Edit(modelDetail, lbUserName, prestatus);
                if (!rtn.Status)
                {
                    modelDetail.Status = prestatus;
                    modelSet.hedgingFramework_Detail = resultmodel.hedgingFramework_Detail;
                }
                else
                {
                    modelSet.hedgingFramework_Detail = modelDetail;
                }
            }
            else
            {
                modelDetail.IsChange = "1";
                rtn = servicemodel.Add(ref modelDetail, lbUserName);
                if (!rtn.Status)
                {
                    modelDetail.Status = prestatus;
                    modelSet.hedgingFramework_Detail = resultmodel.hedgingFramework_Detail;
                }
                else
                {
                    modelSet.hedgingFramework_Detail = modelDetail;
                }
            }

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
           
            TempData["OrderID"] = modelDetail.OrderID;
            modelSet.pageMode = "EDIT";
            return View("Index", modelSet);
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action",Argument = "Cancel")]
        public ActionResult Cancel (FormCollection form, HedgingFrameworkViewModel resultmodel)
        {
            HedgingFrameworkViewModel_Detail modelDetail = new HedgingFrameworkViewModel_Detail();
            string reason = resultmodel.hedgingFramework_Detail.CancelReamark;
            HedgingFrameworkServiceModel service = new HedgingFrameworkServiceModel();
            ReturnValue rtn = new ReturnValue();
            modelDetail = resultmodel.hedgingFramework_Detail;
            modelDetail.Status = ConstantPrm.ACTION.CANCEL;
            rtn = service.Cancel(modelDetail, lbUserName, reason);
            HedgingFrameworkViewModel modelSet = initialModel();
            modelSet.hedgingFramework_Detail = modelDetail;
            if (!rtn.Status)
            {
                modelSet.hedgingFramework_Detail.Status = ConstantPrm.ACTION.SUBMIT;
            }
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["OrderID"] = modelDetail.OrderID;
            return View("Index",modelSet);
        }

        public HedgingFrameworkViewModel initialModel()
        {
            HedgingFrameworkViewModel model = new HedgingFrameworkViewModel();
            model.hedgingFramework_Detail = new HedgingFrameworkViewModel_Detail();
            model.ddlUnderlying = new List<SelectListItem>();
            model.ddlHegingType = DropdownServiceModel.getHedgeType(false, string.Empty);
            model.ddlType = DropdownServiceModel.getType(false, string.Empty);
            model.ddlValueType = DropdownServiceModel.getValueType(false, string.Empty);
            model.ddlVolumeUnit = DropdownServiceModel.getUnitVolume(false, string.Empty);
            model.ddlUnderlying = DropdownServiceModel.getUnderlying(false, string.Empty, "HEDG", true);
            return model;
        }
    }
}