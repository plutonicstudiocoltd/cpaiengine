﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Model;
using com.pttict.engine.utility;


namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgingTicketController : BaseController
    {
        ShareFn _FN = new ShareFn();
        public string MCCTypeSS
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : string.Empty; }
            set { Session["Type"] = value; }
        }
         

        public ActionResult Search()
        { 
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            model.HedgingTicket_Search = new HedgingTicketViewModel_Search();      
            model.systemType = Session["Type"] as string; 
            TempData["ReponseStatus"] = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(HedgingTicketViewModel postModel)
        {
            ReturnValue rtn = new ReturnValue(); 
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            model.HedgingTicket_Search = new HedgingTicketViewModel_Search(); 
            HedgingTicketViewModel_Search search = new HedgingTicketViewModel_Search();
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            search = postModel.HedgingTicket_Search;
            search.systemType = postModel.systemType;
            rtn = service.Search(ref search);
            search.sSearchData = search.sSearchData == null ? new List<HedgingTicketViewModel_SearchData>() : search.sSearchData;
            model.HedgingTicket_Search = search;
            modifyField(ref model);
            model.systemType = postModel.systemType;
            if (rtn.Status || rtn.Message == "extra_xml is empty" || rtn.Message == "Data not found")
            {
                TempData["ReponseStatus"] = null;
            }
            else
            {
                TempData["ReponseStatus"] = rtn.Status;
            }
            TempData["ReponseMessage"] = rtn.Message;
            return View(model);
        } 

        public ActionResult Index(string Type)
        {
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            model.systemType = Type == ""? Request.QueryString["Type"].ToString() : Type;
            model.deal_detail.hedging_chart.hedged_vol = "0";
            model.deal_detail.hedging_chart.limit = "0";
            model.deal_detail.hedging_chart.target = "0";
            model.pageMode = "New";
            model.trading_ticket.revision = "0";
            model.trading_ticket.trader = ViewBag.UserName;
            model.trading_ticket.status = "New";
            model.buttonMode = "AUTO";
            ViewBag.record_status = "N";
            ViewBag.Type = Type.Decrypt();
            Session["pageMode"] = model.pageMode;
            Session["statusTicket"] = "New";
            Session["dealList_no"] = null;
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchAddDeal(string sdeal_type, string sUnderlying, string sTool, string shegingType,string systemType)
        {
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            if (sdeal_type != null)
            {
                ViewBag.SearchDeal = "TRUE";
                model.search_deal.sDeal_type = model.search_deal.deal_type.Single(a => a.Text == sdeal_type).Value;
            }
            else { ViewBag.SearchDeal = "FALSE"; }
            if (sUnderlying != null)
            {
                ViewBag.SearchDeal = "TRUE";
                model.search_deal.sUnderlying = model.search_deal.underlying.Single(a => a.Text == sUnderlying).Value;
            }
            else { ViewBag.SearchDeal = "FALSE"; }
            if (sTool != null)
            {
                ViewBag.SearchDeal = "TRUE";
                model.search_deal.sTool = model.search_deal.tool.Single(a => a.Text == sTool).Value;
            }
            else { ViewBag.SearchDeal = "FALSE"; }
            if (shegingType != null)
            {
                ViewBag.SearchDeal = "TRUE";
                model.search_deal.sHedgeType = model.search_deal.hegingType.Single(a => a.Text == shegingType).Value;
            }
            else { ViewBag.SearchDeal = "FALSE"; }
            model.pageMode = Session["pageMode"] as string;
            model.systemType = systemType;
            var deallist_no = Session["dealList_no"] as List<string>;
            return PartialView("_Search_Deal", model);
        }

        [HttpPost]
        public ActionResult SearchDeal(string deal_no, string deal_Date, string deal_Type, string tool, string underlying, string vS, string hegingType,string systemType)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            model.search_deal.deal_no = deal_no;
            model.search_deal.sDeal_type = deal_Type;
            model.search_deal.sTool = tool;
            model.search_deal.sUnderlying = underlying;
            model.search_deal.sVS = vS;
            model.search_deal.sHedgeType = hegingType; 
            model.systemType = systemType.Decrypt();
            if (!String.IsNullOrEmpty(deal_Date))
            {
                string[] dealDate = Regex.Split(deal_Date, @"to");
                model.search_deal.sDate_start = dealDate[0];
                model.search_deal.sDate_to = dealDate[1];
            }
            rtn = service.GetDetail(ref model, false);
            model.dealList = Session["dealList_no"] as List<string>;
            var newSearch_deal = new List<HedgingTicketViewModel.SearchDealList>();
            foreach (var item in model.search_deal.search_deal_list)
            {
                newSearch_deal.Add(item);
            }
            foreach (var item in newSearch_deal)
            {
                var oldtoolData = item.tool;
                var newtooldata = model.search_deal.tool.SingleOrDefault(a => a.Value == item.tool);
                item.tool = newtooldata == null ? oldtoolData : model.search_deal.tool.SingleOrDefault(a => a.Value == item.tool).Text;
                var oldhedgtypeData = item.hedging_type;
                var newhedgtypedata = model.search_deal.hegingType.SingleOrDefault(a => a.Value == item.hedging_type);
                item.hedging_type = newhedgtypedata == null ? oldhedgtypeData : model.search_deal.hegingType.Single(a => a.Value == item.hedging_type).Text;
            }
            model.search_deal.search_deal_list = newSearch_deal;
            model.systemType = model.systemType.Encrypt();
            TempData["ReponseStatus"] = rtn.Status;
            TempData["ReponseMessage"] = rtn.Message;
            return Json(model);
        }

        public ActionResult Edit(string TranID, string Tran_Req_ID, string Type, string System)
        {
            ReturnValue rtn = new ReturnValue();
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            try
            {
                if (TempData["ReponseMessage"] != null && TempData["ReponseMessage"] as string != "Successfully")
                {
                    ViewBag.ErrorPage = "TRUE";
                }
                TempData["ReponseStatus"] = TempData["ReponseStatus"] == null ? rtn.Status : TempData["ReponseStatus"];
                TempData["ReponseMessage"] = TempData["ReponseMessage"] == null ? rtn.Message : TempData["ReponseMessage"];
                rtn.Message = TempData["ReponseMessage"] == null ? rtn.Message : TempData["ReponseMessage"] as string;
                TempData["ReponseMessage"] = rtn.Status == true ? "Successfully" : rtn.Message;
                var type = Type.Decrypt();
                var system = System.Decrypt();
                rtn = LoadButton(TranID.Decrypt(), type, system, ref model);
                model.pageMode = "Edit";
                model.systemType = Type;
                model.system = System;
                if (ViewBag.ErrorPage == "TRUE")
                {
                    model.deal_detail = new HedgingTicketViewModel.DealDetail();
                    model.deal_detail.hedging_target = new List<HedgingTicketViewModel.HedgingTarget>();
                    model.deal_detail.hedging_execution = new HedgingTicketViewModel.HedgingExecution();
                    model.deal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
                    model.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
                    model.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                    model.deal_detail.hedging_chart = new HedgingTicketViewModel.HedgingChart();
                }
                else
                {
                    List<string> dealList_no = new List<string>();
                    foreach (var item in model.deal_detail.hedging_execution.hedging_execution_data)
                    {
                        dealList_no.Add(item.deal_no);
                    }
                    Session["dealList_no"] = dealList_no;
                    Session["statusTicket"] = model.trading_ticket.status;
                    Session["ticketNumber"] = model.trading_ticket.ticket_no;
                    Session["pageMode"] = model.pageMode;
                    foreach (var item in model.deal_detail.hedging_execution.hedging_execution_data)
                    {
                        item.record_status = "N";
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            return View("Index", model);
        }
         
        public HedgingTicketViewModel LoadXMLFromTest(string Path)
        {
            if (System.IO.File.Exists(Path))
            {
                StreamReader _sr = new StreamReader(Path);
                string Line = _sr.ReadToEnd();
                HedgingTicketViewModel model = new JavaScriptSerializer().Deserialize<HedgingTicketViewModel>(Line);
                return model;
            }
            return null;
        }

        [HttpPost]
        public ActionResult AddDeal(string[] deal_no, string sdeal_no, string sdeal_date, string sdeal_type, string sTool, string sUnderlying, string sVS, string hegingType, string pageMode,string systemType)
        {
            //////Test call Json file
            HedgingTicketViewModel vm = new HedgingTicketViewModel();
            string subPath = "~/Web/FileUpload";
            string fileName = "json_trading_ticketv2_201705516.txt";
            string path = Path.Combine(Server.MapPath(subPath), fileName);
            vm = LoadXMLFromTest(path);

            ReturnValue rtn = new ReturnValue();
            HedgingTicketViewModel model = new HedgingTicketViewModel(); 
            model.search_deal.deal_no = sdeal_no;
            model.search_deal.sDeal_type = sdeal_type;
            model.search_deal.sTool = sTool;
            model.search_deal.sUnderlying = sUnderlying;
            model.search_deal.sVS = sVS;
            model.search_deal.deal_date = sdeal_date;
            model.search_deal.sHedgeType = hegingType;
            model.systemType = systemType.Decrypt();
            if (!String.IsNullOrEmpty(sdeal_date))
            {
                string[] dealDate = Regex.Split(sdeal_date, @"to");
                model.search_deal.sDate_start = dealDate[0];
                model.search_deal.sDate_to = dealDate[1];
            }
            model.trading_ticket.status = model.trading_ticket.status == null ? Session["statusTicket"] as string : model.trading_ticket.status;
            model.trading_ticket.ticket_no = model.trading_ticket.ticket_no == null ? Session["ticketNumber"] as string : model.trading_ticket.ticket_no;
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            rtn = service.GetDetail(ref model, true);
            List<HedgingTicketViewModel.HedgingExecutionData> dealData = new List<HedgingTicketViewModel.HedgingExecutionData>();
            List<HedgingTicketViewModel.HedgingTarget> dealTarget = new List<HedgingTicketViewModel.HedgingTarget>();
            var deallist_no = Session["dealList_no"] as List<string>;
            var i = 1;
            if (deal_no != null)
            {
                foreach (var item in deal_no)
                {
                    var data = model.deal_detail.hedging_execution.hedging_execution_data.SingleOrDefault(a => a.deal_no == item);
                    if (data != null)
                    {
                        dealData.Add(data);
                        var target = vm.deal_detail.hedging_target.SingleOrDefault(a => a.order == i.ToString());
                        if (target != null)
                        {
                            dealTarget.Add(target);
                        }
                    }
                    i++;
                }
                foreach (var item in model.oldDeal_detail.hedging_execution.hedging_execution_data)
                {
                    dealData.Add(item);
                }
            }
            model.deal_detail.hedging_target = dealTarget;
            model.deal_detail.hedging_execution.hedging_execution_data = dealData;
            if (vm.deal_detail.hedging_execution.hedging_position == null)
            {
                vm.deal_detail.hedging_execution.hedging_position = new HedgingTicketViewModel.HedgingPosition();
            }
            model.deal_detail.hedging_chart = vm.deal_detail.hedging_chart;
            model.deal_detail.deal_type = model.search_deal.deal_type.SingleOrDefault(a => a.Value == model.deal_detail.deal_type).Text;
            model.deal_detail.underlying_name = model.search_deal.underlying.SingleOrDefault(a => a.Value == model.deal_detail.underlying_name).Text;
            model.deal_detail.tool_name = model.search_deal.tool.SingleOrDefault(a => a.Value == model.deal_detail.tool_name).Text;
            model.deal_detail.hedg_type_name = model.search_deal.hegingType.SingleOrDefault(a => a.Value == hegingType).Text; ;
            model.pageMode = pageMode;
            model.systemType = model.systemType.Encrypt();
            Session["pageMode"] = pageMode;
            List<string> dealList_no = new List<string>();
            foreach (var item in model.deal_detail.hedging_execution.hedging_execution_data)
            {
                dealList_no.Add(item.deal_no);
            }
            Session["dealList_no"] = dealList_no;
            return PartialView("_Deal_Detail", model);
        }

        [HttpPost]
        public ActionResult DeleteList(string[] deal_no, string deleteRow, string sdeal_no, string sdeal_date, string sdeal_type, string sTool, string sUnderlying, string sVS, string systemType)
        {
            //////Test call Json file
            HedgingTicketViewModel vm = new HedgingTicketViewModel();
            string subPath = "~/Web/FileUpload";
            string fileName = "json_trading_ticketv2_201705516.txt";
            string path = Path.Combine(Server.MapPath(subPath), fileName);
            vm = LoadXMLFromTest(path);

            ReturnValue rtn = new ReturnValue();
            HedgingTicketViewModel model = new HedgingTicketViewModel();
            model.search_deal.deal_no = sdeal_no;
            model.search_deal.sDeal_type = sdeal_type;
            model.search_deal.sTool = sTool;
            model.search_deal.sUnderlying = sUnderlying;
            model.search_deal.sVS = sVS;
            model.systemType = systemType.Decrypt();
            if (!String.IsNullOrEmpty(sdeal_date))
            {
                string[] dealDate = Regex.Split(sdeal_date, @"to");
                model.search_deal.sDate_start = dealDate[0];
                model.search_deal.sDate_to = dealDate[1];
            }
            model.trading_ticket.status = model.trading_ticket.status == null ? Session["statusTicket"] as string : model.trading_ticket.status;
            model.trading_ticket.ticket_no = model.trading_ticket.ticket_no == null ? Session["ticketNumber"] as string : model.trading_ticket.ticket_no;
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            rtn = service.GetDetail(ref model, true);
            List<HedgingTicketViewModel.HedgingExecutionData> dealData = new List<HedgingTicketViewModel.HedgingExecutionData>();
            List<HedgingTicketViewModel.HedgingExecutionData> newDealData = new List<HedgingTicketViewModel.HedgingExecutionData>();
            List<HedgingTicketViewModel.HedgingTarget> dealTarget = new List<HedgingTicketViewModel.HedgingTarget>();
            var i = 1;
            if (deal_no != null)
            {
                foreach (var item in deal_no)
                {
                    var data = model.deal_detail.hedging_execution.hedging_execution_data.SingleOrDefault(a => a.deal_no == item);
                    if (data != null)
                    {
                        dealData.Add(data);
                        newDealData.Add(data);
                        var target = vm.deal_detail.hedging_target.SingleOrDefault(a => a.order == i.ToString());
                        if (target != null)
                        {
                            dealTarget.Add(target);
                        }
                    }
                    i++;
                }
                foreach (var item in model.oldDeal_detail.hedging_execution.hedging_execution_data)
                {
                    dealData.Add(item);
                    newDealData.Add(item);
                }
            }
            foreach (var item in newDealData)
            {
                if (item.deal_no == deleteRow)
                {
                    if (item.record_status == "N")
                    {
                        dealData.Remove(item);
                    }
                    else
                    {
                        item.record_status = "D";
                    }
                }
            }
            model.deal_detail.hedging_target = dealTarget;
            model.deal_detail.hedging_execution.hedging_execution_data = dealData;
            model.deal_detail.hedging_execution.hedging_position.hedging_position_data = vm.deal_detail.hedging_execution.hedging_position.hedging_position_data;
            model.deal_detail.hedging_chart = vm.deal_detail.hedging_chart;
            if (model.deal_detail.hedging_execution.hedging_execution_data.Count == 0)
            {
                model.deal_detail.hedging_target = new List<HedgingTicketViewModel.HedgingTarget>();
                model.deal_detail.hedging_execution.hedging_position.hedging_position_data = new List<HedgingTicketViewModel.HedgingPositionData>();
                model.deal_detail.hedging_execution.hedging_execution_data = new List<HedgingTicketViewModel.HedgingExecutionData>();
            }
            model.pageMode = Session["pageMode"] as string;
            List<string> dealList_no = new List<string>();
            foreach (var item in model.deal_detail.hedging_execution.hedging_execution_data)
            {
                dealList_no.Add(item.deal_no);
            }
            model.systemType = model.systemType.Encrypt();
            Session["dealList_no"] = dealList_no;
            return PartialView("_Deal_Detail", model);
        }

        public ReturnValue LoadButton(string transactionID, string Type, string System, ref HedgingTicketViewModel model)
        {
            ReturnValue rtn = new ReturnValue();
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();

            try
            {
                req.Function_id = ConstantPrm.FUNCTION.F10000051;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = System });
                req.Req_parameters.P.Add(new P { K = "type", V = Type });
                req.Req_parameters.P.Add(new P { K = "transaction_id", V = transactionID });
                req.Extra_xml = "";

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (!resData.result_code.Trim().Equals("1"))
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                string _DataJson = resData.extra_xml;
                ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
                if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
                {
                    attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                    if (Att.attach_items.Count > 0)
                    {
                        foreach (string _item in Att.attach_items)
                        {
                            ViewBag.hdfFileUpload += _item + "|";
                        }
                    }
                }
                if (_model.data_detail != null)
                {
                    var modelRoot = new JavaScriptSerializer().Deserialize<TradingTicketRootObject>(_model.data_detail);
                    model.trading_ticket = modelRoot.trading_ticket;
                    model.deal_detail = modelRoot.deal_detail;

                }
                model.buttonMode = "MANUAL";
                if (_model.buttonDetail != null && _model.buttonDetail.Button.Count > 0)
                {
                    BindButtonPermission(_model.buttonDetail.Button, ref model);
                }
            }
            catch (Exception ex)
            {
                rtn.Status = false;
                rtn.Message = ex.Message;
            }

            return rtn;
        }

        public void BindButtonPermission(List<ButtonAction> Button, ref HedgingTicketViewModel model)
        {
            model.buttonMode = "MANUAL";
            if (Button.Where(x => x.name.ToUpper() == ConstantPrm.ACTION.SAVE_DRAFT).ToList().Count <= 0)
            {
                model.pageMode = "EDIT";
            }
            string _btn = "";
            foreach (ButtonAction _button in Button)
            {
                _btn = "<input type='submit' id='" + _button.name + "' value='" + _button.name + "' class='" + _FN.GetDefaultButtonCSS(_button.name) + "' onclick='OpenBoxBungerPrompt(\"" + _FN.GetMessageBTN(_button.name) + "\",\"" + _button.name + "\");return false;' />";
                model.buttonCode += _btn;
            }
            model.buttonCode += "<input type='submit' value='MANUAL' id='btnMANUAL' style='display: none;' name='action:MANUAL' />";
            ButtonListHedgingTicket = Button;
        }

        public List<ButtonAction> ButtonListHedgingTicket
        {
            get
            {
                if (Session["ButtonListHedgingTicket"] == null) return null;
                else return (List<ButtonAction>)Session["ButtonListHedgingTicket"];
            }
            set { Session["ButtonListHedgingTicket"] = value; }
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "MANUAL")]
        public ActionResult _btn_Click(FormCollection form, HedgingTicketViewModel model)
        {
            if (ButtonListHedgingTicket != null)
            {
                TempData["ReponseStatus"] = null;
                TempData["ReponseMessage"] = null;
                var _lstButton = ButtonListHedgingTicket.Where(x => x.name == form["hdfEventClick"]).ToList();
                if (_lstButton != null && _lstButton.Count > 0)
                {
                    string _evnt = _lstButton[0].name;
                    string _xml = _lstButton[0].call_xml;
                    string temp = form["hdfNoteAction"];
                    string note = string.IsNullOrEmpty(temp) ? "-" : temp;

                    List<ReplaceParam> _param = new List<ReplaceParam>();
                    string TranReqID = Request.QueryString["Tran_Req_ID"] == null ? model.reqID : Request.QueryString["Tran_Req_ID"].ToString();
                    string json;
                    TradingTicketRootObject rootObj = new TradingTicketRootObject();
                    rootObj.deal_detail = model.deal_detail;
                    rootObj.trading_ticket = model.trading_ticket;
                    json = new JavaScriptSerializer().Serialize(rootObj);
                    _param.Add(new ReplaceParam() { ParamName = "#req_txn_id", ParamVal = TranReqID.Decrypt() });
                    _param.Add(new ReplaceParam() { ParamName = "#note", ParamVal = string.IsNullOrEmpty(note) ? "-" : note });
                    _param.Add(new ReplaceParam() { ParamName = "#json_data", ParamVal = HttpUtility.HtmlEncode(json) });
                    _param.Add(new ReplaceParam() { ParamName = "#TranID", ParamVal = ConstantPrm.EnginGetEngineID() });
                    _param.Add(new ReplaceParam() { ParamName = "#attach_items", ParamVal = HttpUtility.HtmlEncode(getFileUploadJson(form["hdfFileUpload"])) });
                    _xml = _FN.RaplceParamXML(_param, _xml);
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                    RequestData _req = new RequestData();
                    ResponseData resData = new ResponseData();
                    _req = ShareFunction.DeserializeXMLFileToObject<RequestData>(_xml);
                    resData = service.CallService(_req);
                    TempData["res_message"] = string.IsNullOrEmpty(resData.response_message) ? resData.result_desc : resData.response_message;
                    if (resData != null)
                    {
                        if (resData.result_code == "1" || resData.result_code == "2" || resData.result_code == "10000020" || resData.result_code == "10000021" || resData.result_code == "10000022" || resData.result_code == "10000023")
                        {
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            var type = model.systemType;
                            var system = ConstantPrm.SYSTEM.HEDG_TCKT.Encrypt();
                            string path = string.Format("~/CPAIMVC/HedgingTicket/Edit?TranID={0}&Tran_Req_ID={1}&Type={2}&System={3}", tranID, reqID, type, system);
                            return Redirect(path);
                        }
                        else
                        {
                            TempData["ReponseStatus"] = false;
                            TempData["ReponseMessage"] = resData.response_message;
                            string tranID = resData.transaction_id.Encrypt();
                            string reqID = resData.req_transaction_id.Encrypt();
                            var type = model.systemType;
                            var system = ConstantPrm.SYSTEM.HEDG_TCKT.Encrypt();
                            string path = string.Format("~/CPAIMVC/HedgingTicket/Edit?TranID={0}&Tran_Req_ID={1}&Type={2}&System={3}", tranID, reqID, type, system);
                            return Redirect(path);
                        }
                    }
                }
            }
            return RedirectToAction("Search", "HedgingTicket");
        }

        private string getFileUploadJson(string hdfFileUpload)
        {
            string[] _split = hdfFileUpload.Split('|');
            if (_split.Length > 0)
            {
                attach_file _att = new attach_file();
                _att.attach_items = new List<string>();
                foreach (var item in _split)
                {
                    _att.attach_items.Add(item);
                }
                var json = new JavaScriptSerializer().Serialize(_att);
                return json;
            }
            else
            {
                return "";
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "SaveDraft")]
        public ActionResult SaveDraft(FormCollection form, HedgingTicketViewModel model)
        {
            model.action = "DRAFT";
            model.next_status = "DRAFT";
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            model.trading_ticket.note = model.trading_ticket.note == null ? "-" : model.trading_ticket.note;
            if (model.deal_detail.hedging_execution.hedging_execution_data != null)
            {
                for (var i = 0; i < model.deal_detail.hedging_execution.hedging_execution_data.Count; i++)
                {
                    model.deal_detail.hedging_execution.hedging_execution_data[i].order = i.ToString();
                }
            }
            ReturnValue rtn = new ReturnValue();
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            try
            {
                rtn = service.Save(ref model, json_uploadFile);
                TempData["ReponseStatus"] = rtn.Status;
                TempData["ReponseMessage"] = rtn.Status == true ? "Successfully" : rtn.Message;
            }
            catch (Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
            return RedirectToAction("Search", "HedgingTicket");
        }

        [HttpPost]
        [ValidateInput(false)]
        [MultipleButton(Name = "action", Argument = "Submit")]
        public ActionResult Submit(FormCollection form, HedgingTicketViewModel model)
        {
            model.action = "APPROVE_1";
            model.next_status = "WAITING VERIFY";
            string json_uploadFile = getFileUploadJson(form["hdfFileUpload"]);
            model.trading_ticket.reason = model.trading_ticket.reason == null ? "-" : model.trading_ticket.reason;
            model.systemType = model.systemType.Decrypt();
            for (var i = 0; i < model.deal_detail.hedging_execution.hedging_execution_data.Count; i++)
            {
                model.deal_detail.hedging_execution.hedging_execution_data[i].order = i.ToString();
            }
            ReturnValue rtn = new ReturnValue();
            HedgingTicketServiceModel service = new HedgingTicketServiceModel();
            try
            {
                rtn = service.Save(ref model, json_uploadFile);
                TempData["ReponseStatus"] = rtn.Status;
                TempData["ReponseMessage"] = rtn.Status == true ? "Successfully" : rtn.Message;
                if (!rtn.Status)
                {
                    model.trading_ticket.status = Session["statusTicket"] as string;
                    model.pageMode = "RENEW";
                    return View("Index", model);
                }
                string tranID = model.tranID.Encrypt();
                string reqID = model.reqID.Encrypt();
                var type = model.systemType.Encrypt();
                var system = ConstantPrm.SYSTEM.HEDG_TCKT.Encrypt();
                string path = string.Format("~/CPAIMVC/HedgingTicket/Edit?TranID={0}&Tran_Req_ID={1}&Type={2}&System={3}", tranID, reqID, type, system);
                return Redirect(path);
            }
            catch (Exception ex)
            {
                model.trading_ticket.status = Session["statusTicket"] as string;
                model.pageMode = "RENEW";
                ViewBag.Error = true;
                ViewBag.ErrorMessage = ex.Message;
                return View("Index", model);
            }
        } 

        private void modifyField(ref HedgingTicketViewModel model)
        {
            if (model.HedgingTicket_Search.sSearchData != null)
            {
                foreach (var item in model.HedgingTicket_Search.sSearchData)
                {
                    var nameTool = model.ddlTool.SingleOrDefault(a => a.Value == item.dTool);
                    var nameUnderly = model.ddlUnderlying.SingleOrDefault(a => a.Value == item.dUnderlying);

                    if (nameTool != null)
                        item.dTool = nameTool.Text;
                    if (nameUnderly != null)
                        item.dUnderlying = nameUnderly.Text;

                    foreach (var j in item.detail)
                    {
                        var nameCounterparty = model.ddlCounterParty.SingleOrDefault(a => a.Value == j.counterParty);
                        if (nameCounterparty != null)
                            j.counterParty = nameCounterparty.Text;
                    }
                }
            }
        }

        

    }
}