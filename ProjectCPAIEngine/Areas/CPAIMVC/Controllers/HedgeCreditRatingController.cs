﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class HedgeCreditRatingController : BaseController
    {
        // GET: CPAIMVC/HedgeCreditRating
        public ActionResult Index()
        {
            HedgeCreditRatingViewModel model = new HedgeCreditRatingViewModel();
            CreditRatingServiceModel serviceModel = new CreditRatingServiceModel();
            model = serviceModel.GetList();
            return View("Index", model);
        }

        [HttpPost]
        public ActionResult Index(HedgeCreditRatingViewModel model)
        {
            CreditRatingServiceModel serviceModel = new CreditRatingServiceModel();
            ReturnValue rtn = new ReturnValue();
            rtn = serviceModel.Edit(model, lbUserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            return View("Index", model);

        }
    }
}