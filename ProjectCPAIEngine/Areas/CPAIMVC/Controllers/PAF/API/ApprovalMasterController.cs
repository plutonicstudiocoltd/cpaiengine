﻿using Newtonsoft.Json;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API
{
    [AllowAnonymous]
    public class ApprovalMasterController : BaseApiController
    {
        
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            result.Add("CUSTOMERS", PAF_MASTER_DAL.GetCustomerList());
            result.Add("CUSTOMERS_DOM", PAF_MASTER_DAL.GetCustomerDomList());
            result.Add("CUSTOMERS_INTER", PAF_MASTER_DAL.GetCustomerInterList());
            result.Add("VENDOR", PAF_MASTER_DAL.GetVendors());
            result.Add("PRODUCTS_DOM", PAF_MASTER_DAL.GetProductDomList());
            result.Add("PRODUCTS_INTER", PAF_MASTER_DAL.GetProductInterList());
            result.Add("PRICE_UNITS", PAF_MASTER_DAL.GetPriceUnits());
            result.Add("QUANTITY_UNITS", PAF_MASTER_DAL.GetQuantityUnits());
            result.Add("PAF_MT_CREDIT_DETAIL", PAF_MASTER_DAL.GetCreditDetails());
            result.Add("CURRENCY_PER_VOLUMN_UNITS", PAF_MASTER_DAL.GetCurrencyPerVolumnUnit());
            result.Add("COMPANY", PAF_MASTER_DAL.GetCompany());
            result.Add("INCOTERMS", PAF_MASTER_DAL.GetIncoterms());
            result.Add("COUNTRY", PAF_MASTER_DAL.GetCountry());
            result.Add("BENCHMARK", PAF_MASTER_DAL.GetBenchMark());
            result.Add("MT_CUST_PAYMENT_TERM", PAF_MASTER_DAL.GetPaymentTerm());
            result.Add("PAF_QUANTITY_TOLERANCE_OPTION", PAF_MASTER_DAL.ToleranceOptions());
            result.Add("PRODUCTS_CMLA", PAF_MASTER_DAL.GetProductCMLAList());
            result.Add("QUANTITY_UNITS_CMLA", PAF_MASTER_DAL.GetQuantityUnitsCMLA());
            result.Add("CURRENCY_PER_VOLUMN_UNITS_CMLA", PAF_MASTER_DAL.GetCurrencyPerVolumnUnitCMLA());
            result.Add("FORMULA", PAF_MASTER_DAL.GetFormula());
            result.Add("PRODUCTS_CMLA_FORM_2", PAF_MASTER_DAL.GetProductCMLAForm2List());
            result.Add("PRODUCTS_CMLA_IMPORT", PAF_MASTER_DAL.GetProductCMLAImportList());
            result.Add("PAF_CMLA_PACKAGING", PAF_MASTER_DAL.GetPackaging());

            result.Add("CUSTOMERS_BITUMEN", PAF_MASTER_DAL.GetCustomerBitumenList());
            result.Add("PAF_CMLA_BITUMEN", PAF_MASTER_DAL.GetProductCMLABitumenList());
            result.Add("PAF_MT_BTM_TIER", PAF_MASTER_DAL.GetBTMTier());

            return SendJson(result);
        }

        public ActionResult Post_HSFO(HSFO_FX_CRITERIA criteria)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result.Add("HSFO", PAF_MASTER_DAL.GetHSFO(criteria));
            return SendJson(result);
        }

        public ActionResult Post_FX(HSFO_FX_CRITERIA criteria)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            result.Add("FX", PAF_MASTER_DAL.GetFX(criteria));
            return SendJson(result);
        }
    }
}