﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALMaster;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PCFPurchaseEntryController : BaseController
    {
        // GET: CPAIMVC/PCFPurchaseEntry
        public ActionResult Index()
        {
            return RedirectToAction("Search", "PCFPurchaseEntry");
        }

        [HttpGet]
        public ActionResult Search()
        {
            PCFPurchaseEntryViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(PCFPurchaseEntryViewModel tempModel)
        {
            try
            {                
                PCFPurchaseEntryViewModel model = initialModel();
                PCFPurchaseEntryServiceModel servicemodel = new PCFPurchaseEntryServiceModel();
                PCFPurchaseEntryViewModel_Search viewModelSearch = new PCFPurchaseEntryViewModel_Search();

                viewModelSearch.sSearchData = new List<PCFPurchaseEntryViewModel_SearchData>();
                viewModelSearch = tempModel.PCFPurchaseEntry_Search;
                servicemodel.Search(ref viewModelSearch);
                model.PCFPurchaseEntry_Search = viewModelSearch;

                return View(model);
            }
            catch(Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = "Unexpected error occurred, please contact the administrator.";
                ViewBag.DebugMessageShort = ex.Message;
                ViewBag.DebugMessageFull = ex.ToString();

                PCFPurchaseEntryViewModel model = initialModel();
                return View(model);
            }
            finally
            {
                //ถ้าจะเปิด Debug Mode ให้เข้าไปแก้ค่าใน PCF_PURCHASE_ENTRY_SEARCH_DEBUG ใน Global Config ให้เป็น "true"
                var debugMode = MasterData.GetJsonMasterSetting("PCF_PURCHASE_ENTRY_SEARCH_DEBUG");
                if (debugMode == "true")
                {
                    ViewBag.DebugMode = true;
                }
                else
                {
                    ViewBag.DebugMode = false;
                }
            }
        }

        public void GetShowSendToSap(PCFPurchaseEntryViewModel_SearchData model)
        {
            ViewBag.ShowSendToSap = false;
            if (model.sOutturn != null)
            {
                if (model.sOutturn.Count > 0)
                {
                    if (model.sOutturn.Where(p => p.UpdatedBy != "" && p.UpdatedBy != null).ToList().Count > 0)
                    {
                        ViewBag.ShowSendToSap = true;
                    }
                }
            }
        }

        [HttpGet]
        public ActionResult Change(PCFPurchaseEntryViewModel_SearchData model)
        {
            try
            {
                PCFPurchaseEntryServiceModel servicemodel = new PCFPurchaseEntryServiceModel();
                model.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
                ReturnValue rtn = servicemodel.GetOutturnData(model);                

                if (!rtn.Status)
                {
                    ViewBag.Error = true;
                    ViewBag.ErrorMessage = rtn.Message;
                }
                else
                {
                    ViewBag.Error = false;
                    Session["CurrentPCFModel"] = model;
                    GetShowSendToSap(model);
                }

                return View(model);
            }
            catch(Exception ex)
            {
                ViewBag.Error = true;
                ViewBag.ErrorMessage = "Unexpected error occurred, please contact the administrator.";
                ViewBag.DebugMessageShort = ex.Message;
                ViewBag.DebugMessageFull = ex.ToString();

                return View(model);
            }
            finally
            {
                var debugMode = Request["Debug"];
                if (debugMode == "true")
                {
                    ViewBag.DebugMode = true;
                }
                else
                {
                    ViewBag.DebugMode = false;
                }                
            }
        }

        [HttpPost]
        public ActionResult ChangeData(PCFPurchaseEntryViewModel_SearchData model)
        {
            PCFPurchaseEntryServiceModel servicemodel = new PCFPurchaseEntryServiceModel();
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            var blDate = Request["txtBlDate"];
            var simulateDataName = Request["SimulateDataName"];
            var simulateValue = Request["SimulateValue"];
            
            if (Session["CurrentPCFModel"] == null)
            {
                servicemodel.GetOutturnData(model);
            }
            else
            {
                model = (PCFPurchaseEntryViewModel_SearchData)Session["CurrentPCFModel"];
            }

            if (blDate != "" && simulateDataName != null)
            {
                blDate = blDate.Replace(" to ", "|");
                var fromDate = DateTime.ParseExact(blDate.Split('|')[0], "dd/MM/yyyy", cultureinfo);
                var toDate = DateTime.ParseExact(blDate.Split('|')[1], "dd/MM/yyyy", cultureinfo);

                decimal n;
                bool isSimulateValueNumeric = decimal.TryParse(simulateValue, out n);

                if (fromDate == null || toDate == null || !isSimulateValueNumeric)
                {
                    return View("Change", model);
                }

                foreach (var item in model.sOutturn)
                {
                    var runningDate = DateTime.ParseExact(item.RunningDate, "dd/MM/yyyy", cultureinfo);
                    if (item.Valid && (runningDate >= fromDate && runningDate <= toDate))
                    {
                        simulateValue = Math.Round(Convert.ToDecimal(simulateValue), 4).ToString("#,##0.0000");
                        if (simulateDataName == "FOB Price")
                        {
                            item.FOBPrice = simulateValue;
                        }
                        else if (simulateDataName == "Freight Price")
                        {
                            item.FreightPrice = simulateValue;
                        }
                        else if(simulateDataName == "C & F Price")
                        {
                            item.CnFPrice = simulateValue;
                        }
                        else
                        {
                            item.ROE = simulateValue;
                        }

                        #region Re-calculate Amount & VAT (now disabled)

                        //PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();

                        //decimal? fobPrice = serviceModel.GetFOBPrice(model.TripNo);
                        //decimal? freightPrice = serviceModel.GetFreightPrice(model.TripNo);
                        //decimal? cnfPrice = fobPrice + freightPrice;
                        //decimal? fobAmountUSD = 0;
                        //decimal? fobAmountTHB = 0;
                        //decimal? freightAmountUSD = 0;
                        //decimal? freightAmountTHB = 0;
                        //decimal? cnfAmountUSD = 0;
                        //decimal? cnfAmountTHB = 0;
                        //decimal? cnfVat7 = 0;
                        //decimal? cnfTotalAmountUSD = 0;
                        //decimal? cnfTotalAmountTHB = 0;
                        //decimal? roe = Convert.ToDecimal(item.ROE);

                        //if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "bbl")
                        //{
                        //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.BlQtyBbl) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.BlQtyBbl) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.BlQtyBbl) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.BlQtyBbl) * freightPrice;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.QtyBbl) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.QtyBbl) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.QtyBbl) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.QtyBbl) * freightPrice;
                        //        }
                        //    }
                        //}
                        //else if (String.IsNullOrEmpty(model.VolumeUnit) || model.VolumeUnit.ToLower() == "mt")
                        //{
                        //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.BlQtyMt) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.BlQtyMt) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.BlQtyMt) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.BlQtyMt) * freightPrice;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.QtyMt) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.QtyMt) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.QtyMt) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.QtyMt) * freightPrice;
                        //        }
                        //    }
                        //}
                        //else
                        //{
                        //    if (String.IsNullOrEmpty(model.InvoiceFigure) || model.InvoiceFigure.ToLower() == "bl")
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.BlQtyMl) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.BlQtyMl) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.BlQtyMl) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.BlQtyMl) * freightPrice;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (String.IsNullOrEmpty(model.PriceUnit) || model.PriceUnit.ToLower() == "thb")
                        //        {
                        //            fobAmountUSD = (Convert.ToDecimal(item.QtyMl) * fobPrice) / roe;
                        //            freightAmountUSD = (Convert.ToDecimal(item.QtyMl) * freightPrice) / roe;
                        //        }
                        //        else
                        //        {
                        //            fobAmountUSD = Convert.ToDecimal(item.QtyMl) * fobPrice;
                        //            freightAmountUSD = Convert.ToDecimal(item.QtyMl) * freightPrice;
                        //        }
                        //    }
                        //}

                        //roe = Math.Round(Convert.ToDecimal(roe), 4);
                        //fobAmountTHB = fobAmountUSD * Convert.ToDecimal(roe);
                        //freightAmountTHB = freightAmountUSD * Convert.ToDecimal(roe);
                        //fobAmountUSD = Math.Round(Convert.ToDecimal(fobAmountUSD), 4);
                        //fobAmountTHB = Math.Round(Convert.ToDecimal(fobAmountTHB), 4);
                        //freightAmountUSD = Math.Round(Convert.ToDecimal(freightAmountUSD), 4);
                        //freightAmountTHB = Math.Round(Convert.ToDecimal(freightAmountTHB), 4);
                        //cnfAmountUSD = Math.Round(Convert.ToDecimal(fobAmountUSD + freightAmountUSD), 4);
                        //cnfAmountTHB = Math.Round(Convert.ToDecimal(fobAmountTHB + freightAmountTHB), 4);
                        //cnfVat7 = cnfAmountTHB * Convert.ToDecimal(0.07);
                        //cnfTotalAmountUSD = cnfAmountUSD;
                        //cnfTotalAmountTHB = cnfAmountTHB + cnfVat7;

                        //cnfVat7 = Math.Round(Convert.ToDecimal(cnfVat7), 4);
                        //cnfTotalAmountUSD = Math.Round(Convert.ToDecimal(cnfTotalAmountUSD), 4);
                        //cnfTotalAmountTHB = Math.Round(Convert.ToDecimal(cnfTotalAmountTHB), 4);


                        #endregion

                    }
                }
            }
            else
            {
                ViewBag.ErrorMissingChangeData = true;
                return View("Change", model);
            }            

            Session["CurrentPCFModel"] = model;
            ModelState.Clear();
            return View("Change", model);
        }

        [HttpPost]
        public ActionResult ManagePE(PCFPurchaseEntryViewModel_SearchData model, string command)
        {
            //Un: ใช้ command ในการแยกประเภทของ action ที่จะทำ
            if (command == "Calculate")
            {
                PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Calculate(model);
                GetShowSendToSap(model);
                if (rtn.Status == false)
                {
                    //Un: ใช้สำหรับ Debug Mode เท่านั้นเพราะบน prod เรา debug ไม่ได้ จริงมะจ๊ะ
                    ViewBag.DebugMessageFull = rtn.Message;
                }

                ViewBag.FromCalculate = true;
            }
            else if (command == "Save")
            {
                PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();
                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Save(model);
                GetShowSendToSap(model);
                //Un: ใส่ข้อมูลลง ViewBag เพื่อจะโชว์ popup ตอน save เสร็จ
                ViewBag.FromCalculate = true;
                ViewBag.SaveCompleted = true;
                ViewBag.SaveCompletedMessage = rtn.Message;
            }
            else if (command == "Report")
            {
                GetShowSendToSap(model);
                return View("Change", model);
            }
            else if (command == "Send to SAP")
            {
                PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();
                ReturnValue rtn = new ReturnValue();
                var accrualType = Request["AccrualType"];
                var reverseDate = Request["txtReverseDate"];
                model.SupplierNo = Convert.ToInt32(model.SupplierNo).ToString();

                ConfigManagement configManagement = new ConfigManagement();
                string configFromDownStream = "";
                if (accrualType.ToUpper() == "A") //A กับ C ใช้ config คนละตัวกัน ถ้ามีแก้อย่าลืมตามไปแก้ทั้งคู่ล่ะ
                {
                    configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND_TYPE_A");
                }
                else // Type C
                {
                    configFromDownStream = configManagement.getDownstreamConfig("CIP_PE_SEND");
                }
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                rtn = serviceModel.SendToSAP(configFromDownStream, model, accrualType, reverseDate);
                if (accrualType.ToUpper() == "C")
                {
                    if(model.sOutturn.Count > 0)
                    {
                        foreach(var i in model.sOutturn)
                        {
                            i.Select = false;
                        }
                    }
                }
                GetShowSendToSap(model);
                //rtn = serviceModel.Save(model);
                ViewBag.SendToSapCompleted = true;
                ViewBag.SendToSapCompletedMessage = rtn.Message;
                if (TempData["CurrentPCFModel"] != null)
                {
                    PCFPurchaseEntryViewModel_SearchData modelTemp = (PCFPurchaseEntryViewModel_SearchData)TempData["CurrentPCFModel"];
                    foreach (var s in modelTemp.sOutturn)
                    {
                        var temp = model.sOutturn.Where(p => p.RunningDate == s.RunningDate).ToList();
                        if (temp.Count > 0)
                        {
                            if(!string.IsNullOrEmpty(temp[0].SAPFIDoc))
                            {
                                if(temp[0].SAPFIDoc.Length > 10)
                                {
                                    s.SAPFIDoc = temp[0].SAPFIDoc.Substring(0,10);
                                }
                                else
                                {
                                    s.SAPFIDoc = temp[0].SAPFIDoc;
                                }
                            }
                            else
                            {
                                s.SAPFIDoc = "";
                            }
                            
                            s.SAPFIDocStatus = temp[0].SAPFIDocStatus;
                        }
                    }
                    TempData["CurrentPCFModel"] = modelTemp;
                }
                else if (Session["CurrentPCFModel"] != null)
                {
                    PCFPurchaseEntryViewModel_SearchData modelTemp = (PCFPurchaseEntryViewModel_SearchData)Session["CurrentPCFModel"];
                    foreach (var s in modelTemp.sOutturn)
                    {
                        var temp = model.sOutturn.Where(p => p.RunningDate == s.RunningDate).ToList();
                        if(temp.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(temp[0].SAPFIDoc))
                            {
                                if (temp[0].SAPFIDoc.Length > 10)
                                {
                                    s.SAPFIDoc = temp[0].SAPFIDoc.Substring(0, 10);
                                }
                                else
                                {
                                    s.SAPFIDoc = temp[0].SAPFIDoc;
                                }
                            }
                            else
                            {
                                s.SAPFIDoc = "";
                            }

                            s.SAPFIDocStatus = temp[0].SAPFIDocStatus;
                        }
                    }
                    Session["CurrentPCFModel"] = modelTemp;
                }
                //serviceModel.GetOutturnData(model);
                //serviceModel.Calculate(model);
                ViewBag.FromCalculate = true;
            }
            else if (command == "Back")
            {
                return RedirectToAction("Search");
            }

            return View("Change", model);
        }

        [HttpGet]
        public ActionResult Report()
        {
            //Un: Logic ล่าสุดของ Report ที่ไม่อยู่ใน spec O_o!!
            //ต่อจากนี้เวลากด report จะดึงข้อมูลจาก db มาแสดงใน report อย่างเดียว
            //แต่ไม่สนค่าบนจอว่าจะแสดงอะไร สนแต่ใน db ล้วนๆจ้ะ
            //เลยต้องมีการเรียก  FilterOnlySavedRecord() เพื่อกรองเฉพาะข้อมูลที่มาจาก db ล้วนๆ
            PCFPurchaseEntryViewModel_SearchData model = null;
            //PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();
            //if (TempData["CurrentPCFModel"] != null)
            //{
            //    model = (PCFPurchaseEntryViewModel_SearchData)TempData["CurrentPCFModel"];
            //    serviceModel.GetPcfPurchaseEntryDaily(model);
            //    model = serviceModel.FilterOnlySavedRecord(model);
            //}
            //else if(Session["CurrentPCFModel"] != null)
            //{
            //    model = (PCFPurchaseEntryViewModel_SearchData)Session["CurrentPCFModel"];
            //    serviceModel.GetPcfPurchaseEntryDaily(model);
            //    model = serviceModel.FilterOnlySavedRecord(model);
            //}
            //else
            //{
            //    serviceModel.GetPcfPurchaseEntryDaily(model);
            //    model = serviceModel.FilterOnlySavedRecord(model);
            //}

            //return View("Report", model);
            string path = "~/Report/Report_Purchas_Entry.aspx";
            if (TempData["CurrentPCFModel"] != null)
            {
                model = (PCFPurchaseEntryViewModel_SearchData)TempData["CurrentPCFModel"];
                path += "?trip_no=" + model.TripNo;
            }
            else if (Session["CurrentPCFModel"] != null)
            {
                model = (PCFPurchaseEntryViewModel_SearchData)Session["CurrentPCFModel"];
                path += "?trip_no=" + model.TripNo;
            }

                return Redirect(path);

        }

        [HttpPost]
        public ActionResult ExportToExcel()
        {
            PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();
            PCFPurchaseEntryViewModel_SearchData model = new PCFPurchaseEntryViewModel_SearchData();

            if (TempData["CurrentPCFModel"] != null)
            {
                model = (PCFPurchaseEntryViewModel_SearchData)TempData["CurrentPCFModel"];
            }
            else if (Session["CurrentPCFModel"] != null)
            {
                model = (PCFPurchaseEntryViewModel_SearchData)Session["CurrentPCFModel"];
            }
            
            string result = serviceModel.ExportToExcel(model);

            if (!result.StartsWith("error"))
            {
                return File(result, "application /excel", System.IO.Path.GetFileName(result));
            }
            return null;
        }

        public PCFPurchaseEntryViewModel initialModel()
        {
            PCFPurchaseEntryServiceModel serviceModel = new PCFPurchaseEntryServiceModel();

            PCFPurchaseEntryViewModel model = new PCFPurchaseEntryViewModel();
            model.PCFPurchaseEntry_Search = new PCFPurchaseEntryViewModel_Search();
            model.PCFPurchaseEntry_Search.sSearchData = new List<PCFPurchaseEntryViewModel_SearchData>();

            model.ddl_Company = serviceModel.GetCompany();
            model.ddl_Product = serviceModel.GetProduct();
            model.ddl_Supplier = DropdownServiceModel.getVendorPIT();

            return model;
        }

        [HttpGet]
        public ActionResult BackToSearch()
        {
            return RedirectToAction("Search", true);
        }
    }
}