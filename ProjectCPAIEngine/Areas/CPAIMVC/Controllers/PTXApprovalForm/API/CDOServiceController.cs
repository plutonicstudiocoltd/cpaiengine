﻿using com.pttict.engine.utility;
using Newtonsoft.Json;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PAF.API;
using ProjectCPAIEngine.DAL.DALCDO;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers.PTXApprovalForm.API
{
    public class CDOServiceController : BaseApiController
    {
        // GET: CPAIMVC/CDOService
        public ActionResult Index()
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            return SendJson(result);
        }

        public ActionResult SaveDraft(CDO_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Draft;
            ResponseData result = new ResponseData();
            if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            {
                //List<CPAI_ACTION_BUTTON> button_list = PAF_DATA_DAL.GetButtons(lbUserID);
                result = SaveData(_status, md, "");
            }
            else
            {
                result = ButtonClickCDO("SAVE DRAFT", md);
                //result = _btn_Click("SAVE DRAFT", md);
                //md.PDA_FORM_ID = result.resp_parameters[0].v;
                //md.PDA_CREATED = md.PDA_CREATED ?? DateTime.Now;
                //md.PDA_CREATED_BY = lbUserID;
                //md.PDA_STATUS = ACTION.DRAFT;
                ////PAF_DATA_DAL.InsertOrUpdate(md);
            }
            return SendJson(result);
        }
        public ActionResult Submit(CDO_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            result = ButtonClickCDO("SUBMIT", md);
            return SendJson(result);
        }

        public ActionResult Verify(CDO_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            result = ButtonClickCDO("VERIFY", md);
            return SendJson(result);
        }
        public ActionResult Endorse(CDO_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Submit;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickCDO("ENDORSE", md);
            //}
            return SendJson(result);
        }
        public ActionResult Cancel(CDO_DATA md)
        {
            DocumentActionStatus _status = DocumentActionStatus.Cancel;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            result = ButtonClickCDO("CANCEL", md);
            //}
            return SendJson(result);
        }
        public ActionResult Reject(CDO_DATA md)
        {
            //DocumentActionStatus _status = DocumentActionStatus.Reject;
            ResponseData result = new ResponseData();
            //if (String.IsNullOrEmpty(md.REQ_TRAN_ID))
            //{
            //    result = SaveData(_status, md, "");
            //}
            //else
            //{
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            result = ButtonClickCDO("REJECT", md);
            //}
            return SendJson(result);
        }
        public ActionResult Approve(CDO_DATA md)
        {
            ResponseData result = new ResponseData();
            md.CDO_UPDATED = DateTime.Now;
            md.CDO_UPDATED_BY = lbUserID;
            //PAF_DATA_DAL.UpdateNote(md.PDA_NOTE, md.TRAN_ID);
            //PAF_DATA_DAL.UpdateBrief(md.PDA_BRIEF, md.TRAN_ID);
            result = ButtonClickCDO("APPROVE", md);
            return SendJson(result);
        }

        public ActionResult GetByTransactionId(CDO_DATA model)
        {
            return FindDafButtons(model);
        }
        public ActionResult FindDafButtons(CDO_DATA model)
        {
            CDO_DATA result = LoadCDOData(model.TRAN_ID);
            return SendJson(result);
        }

        //public ActionResult GenExcel(string id)
        //{
        //    CDOExcelExport exMan = new CDOExcelExport();
        //    CDO_DATA_DAL dafMan = new CDO_DATA_DAL();

        //    CDO_DATA data = dafMan.Get(id);
        //    string file_name = string.Format("Demurrage-{0}.xlsx", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
        //    string file_path = exMan.GenExcel(data, file_name);
        //    return SendJson(file_path);
        //}

        //public ActionResult GeneratePDF(string id)
        //{
        //    CDO_DATA_DAL dafMan = new CDO_DATA_DAL();
        //    CDO_DATA data = dafMan.Get(id);
        //    string pdf_file = CDOGenerator.genPDF(data);
        //    return SendJson(pdf_file);
        //}
    }
}
