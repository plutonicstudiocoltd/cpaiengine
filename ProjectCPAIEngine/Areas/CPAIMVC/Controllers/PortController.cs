﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class PortController : BaseController
    {
        // GET: CPAIMVC/Port
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Port");
        }

        [HttpGet]
        public ActionResult Search()
        {
            PortViewModel model = initialModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(PortViewModel portViewModel)
        {
            PortViewModel model = initialModel();
            PortServiceModel serviceModel = new PortServiceModel();
            PortViewModel_Search viewModelSearch = new PortViewModel_Search();

            viewModelSearch = portViewModel.port_Search;
            serviceModel.Search(ref viewModelSearch);
            model.port_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                PortViewModel model = initialModel();

                if (string.IsNullOrEmpty(model.port_Detail.CreateType))
                {
                    model.port_Detail.CreateType = "CPAI";
                }
                model.port_Detail.Control.Add(new PortViewModel_Control { RowID = "", Order = "1", System = "", Status = "ACTIVE" });

                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Port" }));
            }
        }

        [HttpPost]
        public ActionResult Create(PortViewModel portViewModel)
        {
            if (ViewBag.action_create)
            {
                PortServiceModel serviceModel = new PortServiceModel();
                ReturnValue rtn = new ReturnValue();
                PortViewModel_Detail pModel = new PortViewModel_Detail();
                pModel = portViewModel.port_Detail;
                rtn = serviceModel.Add(ref pModel, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                PortViewModel model = initialModel();
                model.port_Detail = pModel;
                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Port" }));
            }
        }

        [HttpGet]
        public ActionResult Edit(string portID)
        {
            if (ViewBag.action_edit)
            {
                PortServiceModel serviceModel = new PortServiceModel();

                PortViewModel model = initialModel();
                model.port_Detail = serviceModel.Get(portID);
                
                TempData["portID"] = portID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Port" }));
            }
        }

        [HttpPost]
        public ActionResult Edit(string portID, PortViewModel portViewModel)
        {
            if (ViewBag.action_edit)
            {
                PortServiceModel serviceModel = new PortServiceModel();
                ReturnValue rtn = new ReturnValue();

                rtn = serviceModel.Edit(portViewModel.port_Detail, lbUserName);

                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["portID"] = portID;

                PortViewModel model = initialModel();
                model.port_Detail = portViewModel.port_Detail;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Port" }));
            }
        }

        [HttpGet]
        public ActionResult ViewDetail(string portID)
        {
            if (ViewBag.action_view)
            {
                PortServiceModel serviceModel = new PortServiceModel();

                PortViewModel model = initialModel();
                model.port_Detail = serviceModel.Get(portID);
                if (model.port_Detail.Control.Count < 1)
                {
                    model.port_Detail.Control.Add(new PortViewModel_Control { RowID = "", Order = "1", System = "" });
                }

                TempData["vendorCode"] = portID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Port" }));
            }
        }

        public PortViewModel initialModel()
        {
            PortViewModel model = new PortViewModel();
            model.port_Detail = new PortViewModel_Detail();
            model.port_Detail.Control = new List<PortViewModel_Control>();

            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_System = DropdownServiceModel.getPortCtrlSystem();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();

            model.port_SystemJSON = PortServiceModel.GetSystemJSON();

            return model;
        }
    }
}