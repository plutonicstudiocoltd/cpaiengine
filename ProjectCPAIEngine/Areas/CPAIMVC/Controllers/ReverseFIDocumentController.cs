﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using com.pttict.sap.Interface.Service;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class ReverseFIDocumentController : BaseController
    {
        // GET: CPAIMVC/ReverseFIDocument
        public ActionResult Index()
        {
            ReverseFIDocumentServiceModel serviceModel = new ReverseFIDocumentServiceModel();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            model.ddl_Company = serviceModel.GetCompany();

            return View(model);
        }

        public ActionResult PCF()
        {
            ReverseFIDocumentServiceModel serviceModel = new ReverseFIDocumentServiceModel();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            model.ddl_Company = serviceModel.GetCompany();

            return View("Index_PCF", model);
        }

        public ActionResult Search(ReverseFIDocumentViewModel model)
        {
            ReverseFIDocumentServiceModel service = new ReverseFIDocumentServiceModel();
            PurchaseEntryViewModel_Search purchaseEntryViewModel = new PurchaseEntryViewModel_Search();
            var searchModel = service.Search(model.Year, model.Document.Trim());
            Session["FIModel"] = model;

            return View("Search", searchModel);
        }

        public ActionResult Search_PCF(ReverseFIDocumentViewModel model)
        {
            var type = Request["ddlType"];

            if(type == "PPT")
            {
                ReverseFIDocumentServiceModel service = new ReverseFIDocumentServiceModel();
                PCFPurchaseEntryViewModel_Search purchaseEntryViewModel = new PCFPurchaseEntryViewModel_Search();
                var searchModel = service.SearchPCFTrain(model.Year, model.Document);
                Session["FIModel"] = model;

                return View("Search_Train", searchModel);
            }
            else if(type == "PPV")
            {
                ReverseFIDocumentServiceModel service = new ReverseFIDocumentServiceModel();
                PCFPurchaseEntryViewModel_Search purchaseEntryViewModel = new PCFPurchaseEntryViewModel_Search();
                var searchModel = service.SearchPCFVessel(model.Year, model.Document);
                Session["FIModel"] = model;

                return View("Search_Vessel", searchModel);
            }

            return View("Index_PCF");
        }

        public ActionResult Send(PurchaseEntryViewModel_SearchData data)
        {
            ReverseFIDocumentSendResult returnResult = new ReverseFIDocumentSendResult();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            ReverseFIDocumentServiceModel serviceModel = new ReverseFIDocumentServiceModel();

            if (Session["FIModel"] != null)
            {
                model = (ReverseFIDocumentViewModel)Session["FIModel"];
                returnResult = serviceModel.Post(model.Year, model.ComCode, model.Document);
            }
            else
            {
                model.ddl_Company = serviceModel.GetCompany();
                return View("Index", model);
            }

            ReverseFIDocumentResult result = new ReverseFIDocumentResult();
            result.Pass = returnResult.Pass;
            result.PoNo = data.PoNo;
            result.SAPReverseDocNo = returnResult.ReturnDocumentNo;
            result.ResultMessage = returnResult.ResultText;
            result.CompanyCode = model.ComCode;

            if (result.Pass)
            {
                serviceModel.UpdateTables(data, model.Document, result.SAPReverseDocNo, result);
            }
            
            return View("Result", result);
        }

        public ActionResult SendPCFTrain(PCFPurchaseEntryViewModel_SearchData Temp)
        {
            System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo("en-US");
            string format = "dd/MM/yyyy";
            ReverseFIDocumentSendResult returnResult = new ReverseFIDocumentSendResult();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            ReverseFIDocumentServiceModel serviceModel = new ReverseFIDocumentServiceModel();
                
            if (Session["FIModel"] != null)
            {
                model = (ReverseFIDocumentViewModel)Session["FIModel"];
                returnResult = serviceModel.Post(model.Year, model.ComCode, model.Document);
            }
            else
            {
                model.ddl_Company = serviceModel.GetCompany();
                return View("Index_PCF", model);
            }

            ReverseFIDocumentResult result = new ReverseFIDocumentResult();
            result.Pass = returnResult.Pass;
            result.SAPReverseDocNo = returnResult.ReturnDocumentNo;
            result.ResultMessage = returnResult.ResultText;

            if (result.Pass)
            {
                string tripNo = Temp.TripNo;
                string MatItemNo = Temp.MatItemNo;
                string ItemNo = Temp.sOutturn[0].ItemNo;
                DateTime PODate = DateTime.ParseExact(Temp.sOutturn[0].BLDate,format,provider);
                serviceModel.UpdateTablesPCFTrain(tripNo, model.Document, result.SAPReverseDocNo, MatItemNo, ItemNo, PODate);
            }

            return View("Result_Train", result);
        }

        public ActionResult SendPCFVessel(PurchaseEntryVesselViewModel Temp)
        {
            ReverseFIDocumentSendResult returnResult = new ReverseFIDocumentSendResult();
            ReverseFIDocumentViewModel model = new ReverseFIDocumentViewModel();
            ReverseFIDocumentServiceModel serviceModel = new ReverseFIDocumentServiceModel();

            if (Session["FIModel"] != null)
            {
                model = (ReverseFIDocumentViewModel)Session["FIModel"];
                returnResult = serviceModel.Post(model.Year, model.ComCode, model.Document);
            }
            else
            {
                model.ddl_Company = serviceModel.GetCompany();
                return View("Index_PCF", model);
            }

            ReverseFIDocumentResult result = new ReverseFIDocumentResult();
            result.Pass = returnResult.Pass;
            result.SAPReverseDocNo = returnResult.ReturnDocumentNo;
            result.ResultMessage = returnResult.ResultText;

            if (result.Pass)
            {
                string tripNo = Temp.PriceList[0].sTripNo;
                string MatItemNo = Temp.PriceList[0].sMetNum;
                string ItemNo = Temp.PriceList[0].sItemNo;
                serviceModel.UpdateTablesPCFVessel(tripNo, Temp.PriceList[0].SAPDocumentNO, result.SAPReverseDocNo, MatItemNo, ItemNo);
            }
            return View("Result_Train", result);
        }
    }
}