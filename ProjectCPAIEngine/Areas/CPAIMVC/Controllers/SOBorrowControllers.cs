﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class SOBorrowControllers : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "BorrowSO");
        }

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Search")]
        //public ActionResult Search(SOBorrowViewModel pModel)
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();
        //    serviceModel.Search(ref pModel);
        //    SOBorrowViewModel model = initialModel();
        //    model.SOClearLine_Search = pModel.SOClearLine_Search;
        //    //TempData["tempClearLine"] = pModel;
        //    return View(model);
        //    //return RedirectToAction("Search",pModel);
        //}

        //[HttpGet]
        //public ActionResult Search()
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();
        //    SOBorrowViewModel model = new SOBorrowViewModel();
        //    model = initialModel();
        //    return View("Search", model);
        //}

        //[HttpGet]
        //public ActionResult Create(SOBorrowViewModel pModel, string pTripNo)
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();
        //    //SOBorrowViewModel model = new SOBorrowViewModel();

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Create")]
        //public ActionResult Create(FormCollection frm, SOBorrowViewModel pModel)
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();
        //    if (!string.IsNullOrEmpty(pModel.SOClearLine_Search.sTripNo_Select))
        //    {
        //        pModel.selectedTripNo = pModel.SOClearLine_Search.sTripNo_Select;
        //    }


        //    #region "For Use"

        //    #endregion

        //    //For Test
        //    serviceModel.CreateSO(ref pModel);

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Update")]
        //public ActionResult Update(FormCollection frm, SOBorrowViewModel pModel)
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();

        //    var selectedTripNo = frm["SelectedTripNo"].Split(',');
        //    for (var k = 0; k < selectedTripNo.Length - 1; k++)
        //    {
        //        if (!String.IsNullOrEmpty(selectedTripNo[k]))
        //        {
        //            pModel.selectedTripNo = selectedTripNo[k];
        //            break;
        //        }
        //    }

        //    #region "For Use"
        //    //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
        //    //{
        //    //    if (pModel.SOClearLine_Search.SearchData != null)
        //    //    {
        //    //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
        //    //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
        //    //        {
        //    //            serviceModel.UpdateSO(ref pModel);
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    //For Test
        //    serviceModel.UpdateSO(ref pModel);


        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "Cancel")]
        //public ActionResult Cancel(FormCollection frm, SOBorrowViewModel pModel)
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();

        //    var selectedTripNo = frm["SelectedTripNo"].Split(',');
        //    for (var k = 0; k < selectedTripNo.Length - 1; k++)
        //    {
        //        if (!String.IsNullOrEmpty(selectedTripNo[k]))
        //        {
        //            pModel.selectedTripNo = selectedTripNo[k];
        //            break;
        //        }
        //    }
        //    #region "For Use"
        //    //if (!String.IsNullOrEmpty(pModel.selectedTripNo))
        //    //{
        //    //    if (pModel.SOClearLine_Search.SearchData != null)
        //    //    {
        //    //        var qry = pModel.SOClearLine_Search.SearchData.Where(z => z.dTripNo.ToUpper().Equals(pModel.selectedTripNo.ToUpper()));
        //    //        if (qry != null && !String.IsNullOrEmpty(qry.ToList()[0].dSaleOrder))
        //    //        {
        //    //            serviceModel.CancelSO(ref pModel);
        //    //        }
        //    //    }
        //    //}
        //    #endregion

        //    //For Test
        //    serviceModel.CancelSO(ref pModel);

        //    serviceModel.setInitail_ddl(ref pModel);

        //    if (TempData["tempSOClearLine"] != null) { TempData.Remove("tempSOClearLine"); }
        //    TempData["tempSOClearLine"] = pModel;

        //    return View("Search", pModel);
        //}

        //public SOBorrowViewModel initialModel()
        //{
        //    SOClearLineServiceModel serviceModel = new SOClearLineServiceModel();
        //    SOBorrowViewModel model = new SOBorrowViewModel();

        //    model.SOClearLine_Search = new SOBorrowViewModel_Search();
        //    model.SOClearLine_Search.SearchData = new List<SOBorrowViewModel_SearchData>();

        //    model.ddl_Vessel = SOClearLineServiceModel.getVehicle("PRODUCT", true, "");
        //    model.json_Vessel = SOClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Vessel);
        //    model.ddl_Crude = SOClearLineServiceModel.getMaterial();
        //    model.json_Crude = SOClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Crude);
        //    model.ddl_Customer = SOClearLineServiceModel.getCustomer();
        //    model.json_Customer = SOClearLineServiceModel.GetDataToJSON_TextValue(model.ddl_Customer);

        //    return model;
        //}
    }
}