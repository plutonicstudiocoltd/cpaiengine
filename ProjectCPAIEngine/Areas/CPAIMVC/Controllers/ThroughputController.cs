﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using com.pttict.downstream.common.utilities;
using com.pttict.engine.utility;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class ThroughputController : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "Throughput");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Search")]
        public ActionResult Search(ThroughputViewModel pModel)
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            if (!String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryDate))
            {
                string[] tempDate = pModel.Throughput_Search.sDeliveryDate.Split(' ');
                pModel.Throughput_Search.sDeliveryDateFrom = tempDate[0];
                pModel.Throughput_Search.sDeliveryDateTo = tempDate[2];
            }
            serviceModel.GetSearchList(ref pModel); 
            serviceModel.setInitail_ddl(ref pModel);
            return View("Search", pModel);
        }

        [HttpGet]
        public ActionResult Search()
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            ThroughputViewModel model = new ThroughputViewModel();
            model = initialModel();
            serviceModel.setInitail_ddl(ref model);
            model.Throughput_Search.SearchData = null;
            return View("Search", model);
        }

        [HttpGet]
        public ActionResult AddEdit(string pTripNo)
        {
            ThroughputViewModel model = new ThroughputViewModel();
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            //model.ThroughputFeeTransaction.sTripNo = pTripNo.Decrypt();
            if (!String.IsNullOrEmpty(pTripNo))
            {
                serviceModel.GetData(ref model, pTripNo);
                model.pageMode = "EDIT";
            }
            else
            {
                model.Throughput_Detail.dShipTo = serviceModel.GenerateTripNo();
                model.pageMode = "NEW";
            }
            serviceModel.setInitail_ddl(ref model);
            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name ="action",Argument = "Calculate")]
        public ActionResult Calculate(ThroughputViewModel pModel)
        {
            ThroughputServiceModel service = new ThroughputServiceModel();
            service.CalculateData(ref pModel);
            service.setInitail_ddl(ref pModel);
            return View("AddEdit",pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Add")]
        public ActionResult AddEdit()
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            ThroughputViewModel model = initialModel();
            model.Throughput_Detail.dTripNo = serviceModel.GenerateTripNo();
            serviceModel.setInitail_ddl(ref model);
            model.pageMode = "NEW";
            model.Throughput_Detail.ItemList.Add( new ThroughputFeeViewModel_DetailList {
                iIndex = "1",
                iPriceStatus = "0",
                iFixRate = "0",
                iTotalAmountUSD = "0",
                iCurrency = "USD",
                iExchangeRate = "",
                iTotalAmountTHB = "0"
            });
            return View("AddEdit", model);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Save")]
        public ActionResult Save(FormCollection frm, ThroughputViewModel pModel)
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            if (pModel.pageMode == "NEW")
            {
                serviceModel.SaveData(ref pModel);
            }
            else
            {
                serviceModel.EditData(ref pModel);
            }
            serviceModel.setInitail_ddl(ref pModel);
            return View("AddEdit", pModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult CreateSO(FormCollection frm, ThroughputViewModel pModel)
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();

            if (pModel == null) { pModel = initialModel(); }
            serviceModel.setInitail_ddl(ref pModel);
             
            return RedirectToAction("Search", "Throughput");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Update")]
        public ActionResult UpdateSO(FormCollection frm, ThroughputViewModel pModel)
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            if (pModel == null) { pModel = initialModel(); }
            serviceModel.setInitail_ddl(ref pModel);
             
            return RedirectToAction("Search", "Throughput");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Cancel")]
        public ActionResult CancelSO(FormCollection frm, ThroughputViewModel pModel)
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            if (pModel == null) { pModel = initialModel(); }
            serviceModel.CreateSO(ref pModel);
            serviceModel.setInitail_ddl(ref pModel);
             
            return RedirectToAction("Search", "Throughput");
        }

        [HttpPost]
        public string AddPrice(string QTYBBL)
        {
            ThroughputFeeViewModel_DetailList model = new ThroughputFeeViewModel_DetailList();
            THROUGHPUT_FEE_CAL_ViewModel temp = new THROUGHPUT_FEE_CAL_ViewModel();
            temp.input_Volume_1_1 = Decimal.Parse(QTYBBL);
            temp.input_Exchange_BuyingRate = (decimal)33.0692;
            temp.input_Exchange_AverageRate = (decimal)33.2469;
            ThroughputServiceModel service = new ThroughputServiceModel();
            temp = service.calThroughPut(temp);
            model.iCurrency = "USD";
            model.iExchangeRate = "";
            model.iFixRate = temp.FixRate1_2.ToString();
            model.iPriceStatus = "1";
            model.iTotalAmountUSD = temp.ServiceCharge1_3.ToString();
            model.iTotalAmountTHB = temp.ProServiceFeeBase1_5_3.ToString();
            return new JavaScriptSerializer().Serialize(model);
        }

        public ThroughputViewModel initialModel()
        {
            ThroughputServiceModel serviceModel = new ThroughputServiceModel();
            ThroughputViewModel model = new ThroughputViewModel();
            model.Throughput_Search = new ThroughputViewModel_Search();
            model.Throughput_Search.SearchData = new List<ThroughputViewModel_SearchData>();
            model.Throughput_Detail = new ThroughoutFeeViewModel_Detail();
            model.Throughput_Detail.ItemList = new List<ThroughputFeeViewModel_DetailList>();


            model.ThroughputFeeTransaction = new ThroughputFeeTransactionViewModel();
            model.ThroughputFeeTransaction.ThroughputFeeTransactionList = new List<ThroughputFeeTransactionDetailViewModel>();
            model.ddl_Product = ThroughputServiceModel.getMaterial();
            model.json_Product = ThroughputServiceModel.GetDataToJSON_TextValue(model.ddl_Product);
            model.ddl_Customer = ThroughputServiceModel.getCustomer();
            model.ddl_Customer.Add(new SelectListItem
            {
                Value = "2",
                Text = "Esso (Thailand) Plc."
            });
            model.json_Customer = ThroughputServiceModel.GetDataToJSON_TextValue(model.ddl_Customer);
            model.ddl_Plant = new List<SelectListItem>();
            model.ddl_Plant = ThroughputServiceModel.getPlant("1100,1400");
            model.ddl_Unit = new List<SelectListItem>();
            model.ddl_Unit = ThroughputServiceModel.getUnit();
            model.ddl_Vessel = new List<SelectListItem>();
            model.ddl_Vessel = ThroughputServiceModel.getVessel();
            model.ddl_PriceStatus = new List<SelectListItem>();
            model.ddl_PriceStatus = ThroughputServiceModel.getPriceStatus();
            model.ddl_ExchangeRate_Type = new List<SelectListItem>();
            model.ddl_ExchangeRate_Type = ThroughputServiceModel.getExchangeRate();
            model.ddl_Hour = new List<SelectListItem>();
            model.ddl_Hour = ThroughputServiceModel.getHour();
            model.ddl_Min = new List<SelectListItem>();
            model.ddl_Min = ThroughputServiceModel.getMin();
            return model;
        }
    }
}