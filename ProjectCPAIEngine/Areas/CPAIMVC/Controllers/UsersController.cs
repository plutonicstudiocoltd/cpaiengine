﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class UsersController : BaseController
    {
        public ActionResult index()
        {
            return RedirectToAction("Search", "Users");
        }

        [HttpGet]
        public ActionResult Search()
        {
            UsersViewModel model = initialModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(UsersViewModel viewModel)
        {
            UsersViewModel model = initialModel();

            UsersServiceModel serviceModel = new UsersServiceModel();
            UsersViewModel_Search viewModelSearch = new UsersViewModel_Search();

            viewModelSearch = viewModel.users_Search;
            serviceModel.Search(ref viewModelSearch);
            model.users_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create()
        {

            UsersViewModel model = initialModel();
            model.users_UserADJSON = UsersServiceModel.GetUserADJSON();
            if (model.users_Detail.Control.Count < 1)
            {
                model.users_Detail.Control.Add(new UsersViewModel_Control { RowID = "", Order = "1", UserGroup = "", UserGroupSystem = "", UserAffectEndDate = "" });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, UsersViewModel viewModel)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            ReturnValue rtn = new ReturnValue();

            UsersViewModel model = initialModel();

            if (viewModel.users_Detail.Control != null)
            {
                for (int i = 0; i < viewModel.users_Detail.Control.Count; i++)
                {
                    viewModel.users_Detail.Control[i].UsedFlag = "Y";
                    viewModel.users_Detail.Control[i].Status = "ACTIVE";

                    string[] DateRange = viewModel.users_Detail.Control[i].UserAffectEndDate.SplitWord(" to ");
                    if (DateRange != null)
                    {
                        if (DateRange.Length >= 2)
                        {
                            viewModel.users_Detail.Control[i].UserAffectDate = DateRange[0];
                            viewModel.users_Detail.Control[i].UserEndDate = DateRange[1];
                        }
                    }
                }
            }

            rtn = serviceModel.Add(viewModel.users_Detail, lbUserName);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            model.users_UserADJSON = UsersServiceModel.GetUserADJSON();
            model.users_Detail = viewModel.users_Detail;
            if (model.users_Detail.Control == null)
                model.users_Detail.Control = new List<UsersViewModel_Control>();
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();

            UsersViewModel model = initialModel();
            model.users_Detail = serviceModel.Get(id);
            if (model.users_Detail.Control == null)
            {
                model.users_Detail.Control.Add(new UsersViewModel_Control { RowID = "", Order = "1", UserGroup = "", UserGroupSystem = "", UserAffectEndDate = "" });
            }
            if (!String.IsNullOrEmpty(model.users_Detail.Role))
            {
                var roleList = model.ddl_Role.Where(p => p.Value == model.users_Detail.Role);
                string role = "";
                if (roleList != null)
                {
                    foreach (var i in roleList)
                    {
                        role = i.Text;
                    }
                }
                model.users_UserRoleJSON = UsersServiceModel.GetMenu(role);
            }

            TempData["vendorCode"] = id;

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Edit(string id, FormCollection frm, UsersViewModel viewModel)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            ReturnValue rtn = new ReturnValue();

            UsersViewModel model = initialModel();

            if (viewModel.users_Detail.Control != null)
            {
                for (int i = 0; i < viewModel.users_Detail.Control.Count; i++)
                {
                    viewModel.users_Detail.Control[i].UsedFlag = "Y";
                    viewModel.users_Detail.Control[i].Status = "ACTIVE";

                    string[] DateRange = viewModel.users_Detail.Control[i].UserAffectEndDate.SplitWord(" to ");
                    if (DateRange != null)
                    {
                        if (DateRange.Length >= 2)
                        {
                            viewModel.users_Detail.Control[i].UserAffectDate = DateRange[0];
                            viewModel.users_Detail.Control[i].UserEndDate = DateRange[1];
                        }
                    }
                }
            }

            rtn = serviceModel.Edit(viewModel.users_Detail, lbUserName);
            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;
            TempData["UserID"] = id;

            model.users_Detail = viewModel.users_Detail;
            if (model.users_Detail.Control == null)
                model.users_Detail.Control = new List<UsersViewModel_Control>();
            return View("Create", model);
        }

        [HttpGet]
        public ActionResult GetUserGroup(string system)
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            string userID = serviceModel.Get(lbUserID).UsersID;
            var response = DropdownServiceModel.getDelegateGroup(userID, system);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Delegate()
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            UsersViewModel model = initialModel();
            List<SelectListItem> uList = new List<SelectListItem>();
            string userID = serviceModel.Get(lbUserID).UsersID;

            var msdata = MasterData.GetAllUserForDelegate(null, null).Where(x => x.USR_ROW_ID != userID);
            foreach (var a in msdata)
            {
                uList.Add(new SelectListItem { Value = a.USR_ROW_ID, Text = a.USR_LOGIN });
            }
            model.ddl_User = uList;

            
            model.ddl_UserGroupSystem = DropdownServiceModel.getDelegateSystem(userID);

            model.users_Detail.Control.Clear();
            if (model.users_Detail.Control.Count < 1)
            {
                model.users_Detail.Control.Add(new UsersViewModel_Control { RowID = "", Order = "1", UserGroup = "", UserGroupSystem = "", UserAffectEndDate = "" });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delegate(FormCollection frm, UsersViewModel viewModel)
        {
            List<SelectListItem> uList = new List<SelectListItem>();
            UsersServiceModel serviceviewModel = new UsersServiceModel();
            ReturnValue rtn = new ReturnValue();
            UsersViewModel model = initialModel();

            List<string> listUsersID = new List<string>();
            if (viewModel.sUser != null)
            {
                var UserID = viewModel.sUser.Split('|');
                for (int i = 0; i < UserID.Count(); i++)
                {
                    listUsersID.Add(UserID[i]);
                }
            }

            if (viewModel.users_Detail.Control != null)
            {
                for (int i = 0; i < viewModel.users_Detail.Control.Count; i++)
                {
                    viewModel.users_Detail.Control[i].UsedFlag = "Y";
                    viewModel.users_Detail.Control[i].Status = "ACTIVE";

                    string[] DateRange = viewModel.users_Detail.Control[i].UserAffectEndDate.SplitWord(" to ");
                    if (DateRange != null)
                    {
                        if (DateRange.Length >= 2)
                        {
                            viewModel.users_Detail.Control[i].UserAffectDate = DateRange[0];
                            viewModel.users_Detail.Control[i].UserEndDate = DateRange[1];
                        }
                    }
                }
            }

            rtn = serviceviewModel.Delegate(listUsersID, viewModel.users_Detail, lbUserID);

            TempData["ReponseMessage"] = rtn.Message;
            TempData["ReponseStatus"] = rtn.Status;

            return View("Create", model);
        }

        [HttpGet]
        public ActionResult ViewDetail(string id, UsersViewModel model)
        {
            if (ViewBag.action_view)
            {
                UsersServiceModel serviceModel = new UsersServiceModel();

                model = initialModel();
                model.users_Detail = serviceModel.Get(id);
                if (model.users_Detail.Control == null)
                {
                    model.users_Detail.Control.Add(new UsersViewModel_Control { RowID = "", Order = "1", UserGroup = "", UserGroupSystem = "" });
                }
                if (!String.IsNullOrEmpty(model.users_Detail.Role))
                {
                    var roleList = model.ddl_Role.Where(p => p.Value == model.users_Detail.Role);
                    string role = "";
                    if (roleList != null)
                    {
                        foreach (var i in roleList)
                        {
                            role = i.Text;
                        }
                    }
                    model.users_UserRoleJSON = UsersServiceModel.GetMenu(role);
                }

                TempData["vendorCode"] = id;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(new { f = "Users" }));
            }
        }

        public UsersViewModel initialModel()
        {
            UsersServiceModel serviceModel = new UsersServiceModel();
            UsersViewModel model = new UsersViewModel();

            model.users_Search = new UsersViewModel_Search();
            model.users_Search.sSearchData = new List<UsersViewModel_SearchData>();

            model.users_Detail = new UsersViewModel_Detail();
            model.users_Detail.Control = new List<UsersViewModel_Control>();

            model.ddl_Company = DropdownServiceModel.GetDDLFromJson("company");
            model.ddl_Status = DropdownServiceModel.GetDDLFromJson("status");
            model.ddl_ADLogin = DropdownServiceModel.GetDDLFromJson("ad_login_flag");
            model.ddl_TitleEN = DropdownServiceModel.GetDDLFromJson("title_eng");
            model.ddl_TitleTH = DropdownServiceModel.GetDDLFromJson("title_thai");
            model.ddl_System = DropdownServiceModel.GetDDLFromJson("system");
            model.ddl_UserGroup = DropdownServiceModel.GetDDLFromJson("user_group");
            model.ddl_UserGroupSystem = DropdownServiceModel.GetDDLFromJson("user_group_system");
            model.ddl_Role = DropdownServiceModel.getRoleName();


            //model.users_UsersGroupJSON = UsersServiceModel.GetUSystemJSON();
            //model.users_UsersGroupSystemJSON = VendorServiceModel.GetTypeJSON();

            return model;
        }

        public string GetUserDetail(FormCollection frm)
        {
            return UsersServiceModel.GetUserADJSON(frm["name"]);
        }

        public string GetRoleMenu(FormCollection frm)
        {
            return UsersServiceModel.GetMenu(frm["role_name"]);
        }

        [HttpGet]
        public string GetRoleMenu(string frm)
        {
            return UsersServiceModel.GetMenu(frm);
        }
    }
}