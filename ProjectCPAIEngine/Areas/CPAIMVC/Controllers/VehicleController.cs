﻿using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using ProjectCPAIEngine.Flow.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Controllers
{
    public class VehicleController : BaseController
    {
        // GET: CPAIMVC/Vehicle
        public ActionResult Index()
        {
            return RedirectToAction("Search", "Vehicle");
        }

        [HttpPost]
        public ActionResult Search(VehicleViewModel tempModel)
        {

            VehicleViewModel model = initialModel();

            VehicleServiceModel serviceModel = new VehicleServiceModel();
            VehicleViewModel_Search viewModelSearch = new VehicleViewModel_Search();

            viewModelSearch = tempModel.veh_Search;
            serviceModel.Search(ref viewModelSearch);
            model.veh_Search = viewModelSearch;

            return View(model);
        }

        [HttpGet]
        public ActionResult Search()
        {
            VehicleViewModel model = initialModel();
            return View(model);
        }

        public static VehicleViewModel SearchQuery(VehicleViewModel VehicleModel)
        {
            VehicleViewModel_Search viewModelSearch = new VehicleViewModel_Search();
            VehicleServiceModel serviceModel = new VehicleServiceModel();

            VehicleModel.ddl_CreateType = DropdownServiceModel.getCreateType();
            VehicleModel.ddl_CtrlSystem = DropdownServiceModel.getCustomerCtrlSystem();
            VehicleModel.ddl_CtrlType = DropdownServiceModel.getCustomerCtrlType();
            VehicleModel.ddl_Status = DropdownServiceModel.getMasterStatus();

            viewModelSearch = VehicleModel.veh_Search;
            serviceModel.Search(ref viewModelSearch);
            VehicleModel.veh_Search = viewModelSearch;

            return VehicleModel;
        }

        [HttpPost]
        public ActionResult Create(FormCollection frm, VehicleViewModel model)
        {
            if (ViewBag.action_create)
            {
                VehicleServiceModel serviceModel = new VehicleServiceModel();
                ReturnValue rtn = new ReturnValue();
                //model.veh_Detail.Status = model.veh_Detail.BoolStatus == true ? "ACTIVE" : "INACTIVE";

                VehicleViewModel_Detail pModel = new VehicleViewModel_Detail();
                pModel = model.veh_Detail;
                rtn = serviceModel.Add(ref pModel , lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;

                VehicleViewModel modelDropdownSet = initialModel();
                modelDropdownSet.veh_Detail = pModel;
                TempData["VehicleID"] = pModel.VEH_ID;

                return View(modelDropdownSet);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Vehicle" }));
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ViewBag.action_create)
            {
                VehicleViewModel model = initialModel();

                model.veh_Detail.VHE_CREATE_TYPE = "CPAI";
                if(model.veh_Detail.Control.Count == 0)
                    model.veh_Detail.Control.Add(new VehicleViewModel_Control { RowID="", System="", Type="", Status= CPAIConstantUtil.ACTIVE });


                return View(model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Vehicle" }));
            }

        }

        [HttpPost]
        public ActionResult Edit(string VehicleID, VehicleViewModel model)
        {
            if (ViewBag.action_edit)
            {
                VehicleServiceModel serviceModel = new VehicleServiceModel();
                ReturnValue rtn = new ReturnValue();
                rtn = serviceModel.Edit(model.veh_Detail, lbUserName);
                TempData["ReponseMessage"] = rtn.Message;
                TempData["ReponseStatus"] = rtn.Status;
                TempData["VehicleID"] = VehicleID;

                VehicleViewModel modelDropdownSet = initialModel();
                modelDropdownSet.veh_Detail = model.veh_Detail;
                return View("Create", modelDropdownSet);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Vehicle" }));
            }

        }

        [HttpGet]
        public ActionResult Edit(string VehicleID)
        {
            if (ViewBag.action_edit)
            {
                VehicleServiceModel serviceModel = new VehicleServiceModel();

                VehicleViewModel model = initialModel();
                model.veh_Detail = serviceModel.Get(VehicleID);
                TempData["VehicleID"] = VehicleID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Vehicle" }));
            }

        }

        [HttpGet]
        public ActionResult View(string VehicleID)
        {
            if (ViewBag.action_view)
            {
                VehicleServiceModel serviceModel = new VehicleServiceModel();

                VehicleViewModel model = initialModel();
                model.veh_Detail = serviceModel.Get(VehicleID);
                TempData["VehicleID"] = VehicleID;

                return View("Create", model);
            }
            else
            {
                return RedirectToAction("PermissionDenied", "Redirect", new RouteValueDictionary(
    new { f = "Vehicle" }));
            }

        }

        public VehicleViewModel initialModel()
        {
            VehicleServiceModel serviceModel = new VehicleServiceModel();
            VehicleViewModel model = new VehicleViewModel();

            model.veh_Detail = new VehicleViewModel_Detail();
            model.veh_Detail.Control = new List<VehicleViewModel_Control>();
            model.veh_Search = new VehicleViewModel_Search();
            model.veh_Search.sSearchData = new List<VehicleViewModel_SearchData>();
            
            model.veh_TypeJSON = VehicleServiceModel.GetVehTypeJSON();
            model.veh_GroupJSON = VehicleServiceModel.GetVehGroupJSON();
            model.veh_OwnerJSON = VehicleServiceModel.GetVehOwnerJSON();

            model.veh_CtrlSystemJSON = VehicleServiceModel.GetCtrlSystemJSON();
            model.veh_CtrlTypeJSON = VehicleServiceModel.GetCtrlTypeJSON();

            model.ddl_CreateType = DropdownServiceModel.getCreateType();
            model.ddl_Status = DropdownServiceModel.getMasterStatus();
            model.ddl_CtrlSystem = DropdownServiceModel.getVehicleCtrlSystem();
            model.ddl_CtrlType = DropdownServiceModel.getVehicleCtrlType();
            model.ddl_Language = DropdownServiceModel.getLanguage();

            return model;
        }
    }
}