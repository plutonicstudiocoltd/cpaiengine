﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ActionMenuServiceModel
    {
        //Sample
        //actionName = "create"
        //url = "cpaimvc/vendor"
        public bool getActionMenu(string actionName, string url)
        {
            bool actionMenu = Const.User.GetEventPermission(actionName, url);
            return actionMenu;
        }
        
    }
}