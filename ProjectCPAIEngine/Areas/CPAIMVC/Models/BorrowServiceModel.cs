﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class BorrowServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";
        //NumberStyles _NumStyle = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands;
        public void Edit(ref BorrowViewModel pModel)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //For query from DB
                    if ((!String.IsNullOrEmpty(pModel.pBorrowDetail.dTripNo)) || (!String.IsNullOrEmpty(pModel.pBorrowDetail.dTripNo)))
                    {
                        string sTripNos = pModel.sBorrowDetail.dTripNo;
                        string sTripNop = pModel.pBorrowDetail.dTripNo;


                        var qryBorrowItem = context.BORROW_CRUDE.Where(c => c.BRC_TRIP_NO.ToUpper().Equals(sTripNos.ToUpper())).ToList();
                        var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(sTripNos.ToUpper())).ToList();
                        if (qryBorrowItem.Count > 0)
                        {
                            pModel.TripNoM = qryBorrowItem[0].BRC_TRIP_NO;
                            if (qryBorrowItem.Count == 1)
                            {
                                int i = 0;
                                if (qryBorrowItem[i].BRC_TYPE == "S")
                                {

                                    if (pModel.sBorrowDetail == null)
                                    {
                                        pModel.sBorrowDetail = new BorrowDetailViewModel();
                                    }
                                    pModel.sBorrowDetail.dTripNo = qryBorrowItem[i].BRC_TRIP_NO;
                                    pModel.sBorrowDetail.dCustomer = qryBorrowItem[i].BRC_CUST_NUM;
                                    pModel.sBorrowDetail.dFIDoc = qryBorrowItem[i].BRC_FI_DOC;
                                    if (qryBorrowItem[i].BRC_DELIVERY_DATE != null)
                                    {
                                        pModel.sBorrowDetail.dDeliveryDate = (qryBorrowItem[i].BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                                    }
                                    pModel.sBorrowDetail.dCrude = qryBorrowItem[i].BRC_CRUDE_TYPE_ID;
                                    pModel.sBorrowDetail.dSaleOrg = qryBorrowItem[i].BRC_COMPANY;
                                    pModel.sBorrowDetail.dPlant = qryBorrowItem[i].BRC_PLANT;
                                    pModel.sBorrowDetail.dCalcuUnit = qryBorrowItem[i].BRC_UNIT_TOTAL;
                                    pModel.sBorrowDetail.dTank = qryBorrowItem[i].BRC_TANK;
                                    if (qryBorrowItem[i].BRC_DUE_DATE != null)
                                    {
                                        pModel.sBorrowDetail.dDueDate = (qryBorrowItem[i].BRC_DUE_DATE ?? DateTime.Now).ToString(format, provider);
                                    }
                                    pModel.sBorrowDetail.dVolumeBBL = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_BBL.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.sBorrowDetail.dVolumeLitres = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_LITE.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.sBorrowDetail.dVolumeMT = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_MT.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.sBorrowDetail.dPONo = qryBorrowItem[i].BRC_PO_NO;
                                    if (qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() == "" || qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() == null)
                                        pModel.sBorrowDetail.dExchangeRate = "0.0000";
                                    else
                                        pModel.sBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.0000");
                                    //pModel.sBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.00");
                                    if (pModel.sBorrowDetail.BorrowPriceList == null)
                                    {
                                        pModel.sBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();
                                    }
                                    int m = 1;
                                    pModel.sBorrowDetail.dnum = (qryBorrowPrice[i].BRP_NUM).ToString();
                                    //pModel.sBorrowDetail.dnum = (qryBorrowPrice[3].BRP_NUM).ToString();

                                    foreach (var q in qryBorrowPrice.OrderBy(p => p.BRP_NUM))
                                    {
                                        BorrowPriceViewModel temp = new BorrowPriceViewModel();

                                        temp.sTripNo = q.BRP_TRIP_NO;
                                        temp.sPriceType = q.BRP_NUM.ToString();
                                        if (temp.sPriceType == "1" || temp.sPriceType == "2")
                                        {
                                            if (q.BRP_DATE_PRICE != null)
                                            {
                                                temp.sDueDate = (q.BRP_DATE_PRICE ?? DateTime.Now).ToString(format, provider);
                                            }
                                            if (q.BRP_PRICE != null)
                                            {
                                                temp.sPrice = (q.BRP_PRICE ?? 0).ToString("#,##0.0000");
                                            }
                                            temp.sPriceUnit = q.BRP_UNIT_ID ?? "";
                                            if (q.BRP_INVOICE != null)
                                            {
                                                temp.sInvoice = (q.BRP_INVOICE ?? 0).ToString("#,##0.0000");
                                            }
                                            temp.sInvUnit = q.BRP_UNIT_INV ?? "";
                                            temp.sNum = q.BRP_NUM.ToString();
                                            pModel.sBorrowDetail.BorrowPriceList.Add(temp);
                                            m++;
                                        }
                                    }

                                    //add fake p

                                    pModel.pBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();

                                    BorrowPriceViewModel temp1 = new BorrowPriceViewModel();
                                    temp1.sPriceType = "3";
                                    pModel.pBorrowDetail.BorrowPriceList.Add(temp1);

                                    BorrowPriceViewModel temp2 = new BorrowPriceViewModel();
                                    temp2.sPriceType = "4";
                                    pModel.pBorrowDetail.BorrowPriceList.Add(temp2);

                                }

                                else if (qryBorrowItem[i].BRC_TYPE == "P")
                                {

                                    if (pModel.pBorrowDetail == null)
                                    {
                                        pModel.pBorrowDetail = new BorrowDetailViewModel();
                                    }
                                    pModel.pBorrowDetail.dTripNo = qryBorrowItem[i].BRC_TRIP_NO;
                                    pModel.pBorrowDetail.dCustomer = qryBorrowItem[i].BRC_CUST_NUM;
                                    pModel.sBorrowDetail.dFIDoc = qryBorrowItem[i].BRC_FI_DOC;
                                    if (qryBorrowItem[i].BRC_DELIVERY_DATE != null)
                                    {
                                        pModel.pBorrowDetail.dDeliveryDate = (qryBorrowItem[i].BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                                    }
                                    pModel.pBorrowDetail.dCrude = qryBorrowItem[i].BRC_CRUDE_TYPE_ID;
                                    pModel.pBorrowDetail.dSaleOrg = qryBorrowItem[i].BRC_COMPANY;
                                    pModel.pBorrowDetail.dPlant = qryBorrowItem[i].BRC_PLANT;
                                    pModel.pBorrowDetail.dCalcuUnit = qryBorrowItem[i].BRC_UNIT_TOTAL;
                                    pModel.pBorrowDetail.dTank = qryBorrowItem[i].BRC_TANK;
                                    if (qryBorrowItem[i].BRC_DUE_DATE != null)
                                    {
                                        pModel.pBorrowDetail.dDueDate = (qryBorrowItem[i].BRC_DUE_DATE ?? DateTime.Now).ToString(format, provider);
                                    }
                                    pModel.pBorrowDetail.dVolumeBBL = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_BBL.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.pBorrowDetail.dVolumeLitres = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_LITE.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.pBorrowDetail.dVolumeMT = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_MT.ToString() ?? "0").ToString("#,##0.0000");
                                    pModel.pBorrowDetail.dPONo = qryBorrowItem[i].BRC_PO_NO;
                                    pModel.pBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.0000"); //Comment on 06 Aug 2017
                                    if (pModel.pBorrowDetail.BorrowPriceList == null)
                                    {
                                        pModel.pBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();
                                    }

                                    // var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper())).ToList();
                                    int m = 3;
                                    pModel.pBorrowDetail.dnum = (qryBorrowPrice[i].BRP_NUM).ToString();
                                    foreach (var q in qryBorrowPrice.OrderBy(p => p.BRP_NUM))
                                    {
                                        if (q.BRP_NUM == 3 || q.BRP_NUM == 4)
                                        {
                                            BorrowPriceViewModel temp = new BorrowPriceViewModel();
                                            temp.sTripNo = q.BRP_TRIP_NO;
                                            temp.sPriceType = q.BRP_NUM.ToString();


                                            if (q.BRP_DATE_PRICE != null)
                                            {
                                                temp.sDueDate = (q.BRP_DATE_PRICE ?? DateTime.Now).ToString(format, provider);
                                            }
                                            if (q.BRP_PRICE != null)
                                            {
                                                temp.sPrice = (q.BRP_PRICE ?? 0).ToString("#,##0.0000");
                                            }
                                            temp.sPriceUnit = q.BRP_UNIT_ID ?? "";
                                            if (q.BRP_INVOICE != null)
                                            {
                                                temp.sInvoice = (q.BRP_INVOICE ?? 0).ToString("#,##0.0000");
                                            }
                                            temp.sInvUnit = q.BRP_UNIT_INV ?? "";
                                            temp.sNum = q.BRP_NUM.ToString();
                                            pModel.pBorrowDetail.BorrowPriceList.Add(temp);
                                            m++;
                                        }
                                    }
                                    pModel.sBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();
                                    BorrowPriceViewModel temp4 = new BorrowPriceViewModel();
                                    temp4.sPriceType = "1";
                                    pModel.pBorrowDetail.BorrowPriceList.Add(temp4);

                                    BorrowPriceViewModel temp5 = new BorrowPriceViewModel();
                                    temp5.sPriceType = "2";
                                    pModel.pBorrowDetail.BorrowPriceList.Add(temp5);
                                }

                            }
                            else if (qryBorrowItem.Count == 2)
                            {
                                for (int i = 0; i < qryBorrowItem.Count; i++)
                                {

                                    if (qryBorrowItem[i].BRC_TYPE == "S")
                                    {

                                        if (pModel.sBorrowDetail == null)
                                        {
                                            pModel.sBorrowDetail = new BorrowDetailViewModel();
                                        }
                                        pModel.sBorrowDetail.dTripNo = qryBorrowItem[i].BRC_TRIP_NO;
                                        pModel.sBorrowDetail.dCustomer = qryBorrowItem[i].BRC_CUST_NUM;
                                        pModel.sBorrowDetail.dFIDoc = qryBorrowItem[i].BRC_FI_DOC;
                                        if (qryBorrowItem[i].BRC_DELIVERY_DATE != null)
                                        {
                                            pModel.sBorrowDetail.dDeliveryDate = (qryBorrowItem[i].BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                                        }
                                        pModel.sBorrowDetail.dCrude = qryBorrowItem[i].BRC_CRUDE_TYPE_ID;
                                        pModel.sBorrowDetail.dSaleOrg = qryBorrowItem[i].BRC_COMPANY;
                                        pModel.sBorrowDetail.dPlant = qryBorrowItem[i].BRC_PLANT;
                                        pModel.sBorrowDetail.dCalcuUnit = qryBorrowItem[i].BRC_UNIT_TOTAL;
                                        pModel.sBorrowDetail.dTank = qryBorrowItem[i].BRC_TANK;
                                        if (qryBorrowItem[i].BRC_DUE_DATE != null)
                                        {
                                            pModel.sBorrowDetail.dDueDate = (qryBorrowItem[i].BRC_DUE_DATE ?? DateTime.Now).ToString(format, provider);
                                        }
                                        pModel.sBorrowDetail.dVolumeBBL = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_BBL.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.sBorrowDetail.dVolumeLitres = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_LITE.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.sBorrowDetail.dVolumeMT = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_MT.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.sBorrowDetail.dPONo = qryBorrowItem[i].BRC_PO_NO;
                                        if (qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() == "" || qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() == null)
                                            pModel.sBorrowDetail.dExchangeRate = "0.0000";
                                        else
                                            pModel.sBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.0000");
                                        //pModel.sBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.00");
                                        if (pModel.sBorrowDetail.BorrowPriceList == null)
                                        {
                                            pModel.sBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();
                                        }
                                        int m = 1;
                                        pModel.sBorrowDetail.dnum = (qryBorrowPrice[i].BRP_NUM).ToString();
                                        //pModel.sBorrowDetail.dnum = (qryBorrowPrice[3].BRP_NUM).ToString();

                                        foreach (var q in qryBorrowPrice.OrderBy(p => p.BRP_NUM))
                                        {
                                            BorrowPriceViewModel temp = new BorrowPriceViewModel();

                                            temp.sTripNo = q.BRP_TRIP_NO;
                                            temp.sPriceType = q.BRP_NUM.ToString();
                                            if (temp.sPriceType == "1" || temp.sPriceType == "2")
                                            {
                                                if (q.BRP_DATE_PRICE != null)
                                                {
                                                    temp.sDueDate = (q.BRP_DATE_PRICE ?? DateTime.Now).ToString(format, provider);
                                                }
                                                if (q.BRP_PRICE != null)
                                                {
                                                    temp.sPrice = (q.BRP_PRICE ?? 0).ToString("#,##0.0000");
                                                }
                                                temp.sPriceUnit = q.BRP_UNIT_ID ?? "";
                                                if (q.BRP_INVOICE != null)
                                                {
                                                    temp.sInvoice = (q.BRP_INVOICE ?? 0).ToString("#,##0.0000");
                                                }
                                                temp.sInvUnit = q.BRP_UNIT_INV ?? "";
                                                temp.sNum = q.BRP_NUM.ToString();
                                                pModel.sBorrowDetail.BorrowPriceList.Add(temp);
                                                m++;
                                            }
                                        }
                                    }

                                    else if (qryBorrowItem[i].BRC_TYPE == "P")
                                    {

                                        if (pModel.pBorrowDetail == null)
                                        {
                                            pModel.pBorrowDetail = new BorrowDetailViewModel();
                                        }
                                        pModel.pBorrowDetail.dTripNo = qryBorrowItem[i].BRC_TRIP_NO;
                                        pModel.pBorrowDetail.dCustomer = qryBorrowItem[i].BRC_CUST_NUM;
                                        pModel.sBorrowDetail.dFIDoc = qryBorrowItem[i].BRC_FI_DOC;
                                        if (qryBorrowItem[i].BRC_DELIVERY_DATE != null)
                                        {
                                            pModel.pBorrowDetail.dDeliveryDate = (qryBorrowItem[i].BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                                        }
                                        pModel.pBorrowDetail.dCrude = qryBorrowItem[i].BRC_CRUDE_TYPE_ID;
                                        pModel.pBorrowDetail.dSaleOrg = qryBorrowItem[i].BRC_COMPANY;
                                        pModel.pBorrowDetail.dPlant = qryBorrowItem[i].BRC_PLANT;
                                        pModel.pBorrowDetail.dCalcuUnit = qryBorrowItem[i].BRC_UNIT_TOTAL;
                                        pModel.pBorrowDetail.dTank = qryBorrowItem[i].BRC_TANK;
                                        if (qryBorrowItem[i].BRC_DUE_DATE != null)
                                        {
                                            pModel.pBorrowDetail.dDueDate = (qryBorrowItem[i].BRC_DUE_DATE ?? DateTime.Now).ToString(format, provider);
                                        }
                                        pModel.pBorrowDetail.dVolumeBBL = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_BBL.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.pBorrowDetail.dVolumeLitres = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_LITE.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.pBorrowDetail.dVolumeMT = Decimal.Parse(qryBorrowItem[i].BRC_VOLUME_MT.ToString() ?? "0").ToString("#,##0.0000");
                                        pModel.pBorrowDetail.dPONo = qryBorrowItem[i].BRC_PO_NO;
                                        pModel.pBorrowDetail.dExchangeRate = Decimal.Parse(qryBorrowItem[i].BRC_EXCHANGE_RATE.ToString() ?? "0").ToString("#,##0.0000"); //Comment on 06 Aug 2017
                                        if (pModel.pBorrowDetail.BorrowPriceList == null)
                                        {
                                            pModel.pBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>();
                                        }

                                        // var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper())).ToList();
                                        int m = 3;
                                        pModel.pBorrowDetail.dnum = (qryBorrowPrice[i].BRP_NUM).ToString();
                                        foreach (var q in qryBorrowPrice.OrderBy(p => p.BRP_NUM))
                                        {
                                            if (q.BRP_NUM == 3 || q.BRP_NUM == 4)
                                            {
                                                BorrowPriceViewModel temp = new BorrowPriceViewModel();
                                                temp.sTripNo = q.BRP_TRIP_NO;
                                                temp.sPriceType = q.BRP_NUM.ToString();


                                                if (q.BRP_DATE_PRICE != null)
                                                {
                                                    temp.sDueDate = (q.BRP_DATE_PRICE ?? DateTime.Now).ToString(format, provider);
                                                }
                                                if (q.BRP_PRICE != null)
                                                {
                                                    temp.sPrice = (q.BRP_PRICE ?? 0).ToString("#,##0.0000");
                                                }
                                                temp.sPriceUnit = q.BRP_UNIT_ID ?? "";
                                                if (q.BRP_INVOICE != null)
                                                {
                                                    temp.sInvoice = (q.BRP_INVOICE ?? 0).ToString("#,##0.0000");
                                                }
                                                temp.sInvUnit = q.BRP_UNIT_INV ?? "";
                                                temp.sNum = q.BRP_NUM.ToString();
                                                pModel.pBorrowDetail.BorrowPriceList.Add(temp);
                                                m++;
                                            }
                                        }
                                    }

                                    ////For Test
                                    //if (pModel.ClearLineDetail == null) { pModel.ClearLineDetail = new ClearLineDetailViewModel(); }
                                    ////pModel.ClearLineDetail.sTripNo = "SW-2017-06-001";
                                    //pModel.ClearLineDetail.sVesselName = "TATEYAMA";
                                    //pModel.ClearLineDetail.sCustomer = "PPT PLC.";
                                    //pModel.ClearLineDetail.sDeliveryDate = "17/05/2017";
                                    //pModel.ClearLineDetail.sCrude = "MURBAN";
                                    //pModel.ClearLineDetail.sPlant = "";
                                    //pModel.ClearLineDetail.sDueDate = "22/05/2017";
                                    //pModel.ClearLineDetail.sVolumeBBL = "26,022.0210";
                                    //pModel.ClearLineDetail.sVolumeLitres = "10,000,000";
                                    //pModel.ClearLineDetail.sVolumeMT = "32,000,000";
                                    //pModel.ClearLineDetail.sOutturnBBL = "26,022.0210";
                                    //pModel.ClearLineDetail.sOutturnLitres = "10,000,000";
                                    //pModel.ClearLineDetail.sOutturnMT = "32,000,000";
                                    //pModel.ClearLineDetail.sPONo = "664587";
                                    //pModel.ClearLineDetail.sExchangeRate = "35.08";
                                    //pModel.ClearLineDetail.sTotalAmount = "37,000,000.00";
                                    //pModel.ClearLineDetail.sTotalUnit = "230,000.0000";
                                    //pModel.ClearLineDetail.sRemark = "";
                                    //if (pModel.ClearLineDetail.PriceList == null) { pModel.ClearLineDetail.PriceList = new List<PriceViewModel>(); }
                                    //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                                    //{
                                    //    sPriceType = "P",
                                    //    sDueDate = "23/05/2017",
                                    //    sPrice = "32,000.00",
                                    //    sPriceUnit = "BBL",
                                    //    sInvoice = "23,000.00",
                                    //    sInvUnit = "BBL"
                                    //});
                                    //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                                    //{
                                    //    sPriceType = "F",
                                    //    sDueDate = "25/05/2017",
                                    //    sPrice = "31,000.00",
                                    //    sPriceUnit = "BBL",
                                    //    sInvoice = "21,000.00",
                                    //    sInvUnit = "BBL"
                                    //});
                                    //pModel.ClearLineDetail.PriceList.Add(new PriceViewModel
                                    //{
                                    //    sPriceType = "A",
                                    //    sDueDate = "25/05/2017",
                                    //    sPrice = "30,000.00",
                                    //    sPriceUnit = "BBL",
                                    //    sInvoice = "30,000.00",
                                    //    sInvUnit = "BBL"
                                    //});
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void Add(ref BorrowViewModel pModel)
        {
            try
            {
                if (pModel.pBorrowDetail == null) { pModel.pBorrowDetail = new BorrowDetailViewModel(); }
                pModel.pBorrowDetail.dTripNo = "";
                pModel.pBorrowDetail.dCustomer = "";
                pModel.pBorrowDetail.dDeliveryDate = "";
                pModel.pBorrowDetail.dCrude = "";
                pModel.pBorrowDetail.dSaleOrg = "";
                pModel.pBorrowDetail.dPlant = "";
                pModel.pBorrowDetail.dDueDate = "";
                pModel.pBorrowDetail.dCalcuUnit = "";
                pModel.pBorrowDetail.dExchangeRate = "";
                pModel.pBorrowDetail.dCrude = "";
                pModel.pBorrowDetail.dPONo = "";
                pModel.pBorrowDetail.dVolumeBBL = "";
                pModel.pBorrowDetail.dVolumeLitres = "";
                pModel.pBorrowDetail.dVolumeMT = "";
                pModel.pBorrowDetail.dExchangeRate = "";
                pModel.pBorrowDetail.dTank = "";
                pModel.pBorrowDetail.dType = "P"; //fix
                if (pModel.pBorrowDetail.BorrowPriceList == null) { pModel.pBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>(); }
                pModel.pBorrowDetail.BorrowPriceList.Add(new BorrowPriceViewModel
                {
                    //   sPriceType = "P",
                    sPriceType = "3",
                    sDueDate = "",
                    sPrice = "",
                    sPriceUnit = "",
                    sInvoice = "",
                    sInvUnit = ""
                });
                pModel.pBorrowDetail.BorrowPriceList.Add(new BorrowPriceViewModel
                {
                    sPriceType = "4",
                    sDueDate = "",
                    sPrice = "",
                    sPriceUnit = "",
                    sInvoice = "",
                    sInvUnit = ""
                });
            }
            catch (Exception ex)
            {
            }

            try
            {
                if (pModel.sBorrowDetail == null) { pModel.sBorrowDetail = new BorrowDetailViewModel(); }
                pModel.sBorrowDetail.dTripNo = "";
                pModel.sBorrowDetail.dCustomer = "";
                pModel.sBorrowDetail.dDeliveryDate = "";
                pModel.sBorrowDetail.dCrude = "";
                pModel.sBorrowDetail.dSaleOrg = "";
                pModel.sBorrowDetail.dPlant = "";
                pModel.sBorrowDetail.dDueDate = "";
                pModel.sBorrowDetail.dCalcuUnit = "";
                pModel.sBorrowDetail.dExchangeRate = "";
                pModel.sBorrowDetail.dCrude = "";
                pModel.sBorrowDetail.dPONo = "";
                pModel.sBorrowDetail.dVolumeBBL = "";
                pModel.sBorrowDetail.dVolumeLitres = "";
                pModel.sBorrowDetail.dVolumeMT = "";
                pModel.sBorrowDetail.dExchangeRate = "";
                pModel.sBorrowDetail.dTank = "";
                pModel.sBorrowDetail.dType = "S";
                if (pModel.sBorrowDetail.BorrowPriceList == null) { pModel.sBorrowDetail.BorrowPriceList = new List<BorrowPriceViewModel>(); }
                pModel.sBorrowDetail.BorrowPriceList.Add(new BorrowPriceViewModel
                {
                    sPriceType = "1",
                    sDueDate = "",
                    sPrice = "",
                    sPriceUnit = "",
                    sInvoice = "",
                    sInvUnit = ""
                });
                pModel.sBorrowDetail.BorrowPriceList.Add(new BorrowPriceViewModel
                {
                    sPriceType = "2",
                    sDueDate = "",
                    sPrice = "",
                    sPriceUnit = "",
                    sInvoice = "",
                    sInvUnit = ""
                });
            }
            catch (Exception ex)
            {
            }
        }

        // public void Search(ref BorrowViewModel pModel)
        public ReturnValue Search(ref BorrowViewModel pModel)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var sTripNo = pModel.Borrow_Search.sTripNo == null ? "" : pModel.Borrow_Search.sTripNo.ToUpper();
                    var sCustomer = pModel.Borrow_Search.sCustomer == null ? "" : pModel.Borrow_Search.sCustomer.ToUpper();
                    var sCrude = pModel.Borrow_Search.sCrude == null ? "" : pModel.Borrow_Search.sCrude.ToUpper();
                    var sDeliveryDate = pModel.Borrow_Search.sDeliveryDate;
                    var sDateFrom = String.IsNullOrEmpty(pModel.Borrow_Search.sDeliveryDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy") : pModel.Borrow_Search.sDeliveryDate.Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.Borrow_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy") : pModel.Borrow_Search.sDeliveryDate.Substring(14, 10);
                    var sComCodeFIDoc = pModel.Borrow_Search.sComCode ?? "";
                    var sYear = pModel.Borrow_Search.sYear ?? "";
                    var sDocument = pModel.Borrow_Search.sDocument ?? "";

                    
                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);

                    var query = (from bo in context.BORROW_CRUDE
                                 join mat in context.MT_MATERIALS on bo.BRC_CRUDE_TYPE_ID equals mat.MET_NUM
                                 join com in context.MT_COMPANY on bo.BRC_COMPANY equals com.MCO_COMPANY_CODE
                                 //join price in context.BORROW_PRICE on bo.BRC_TRIP_NO equals price.BRP_TRIP_NO
                                 join cus in context.MT_CUST_DETAIL on  new { ColA = bo.BRC_CUST_NUM, ColB = bo.BRC_COMPANY , ColC = "I" } equals new { ColA = cus.MCD_FK_CUS, ColB = cus.MCD_FK_COMPANY, ColC= cus.MCD_NATION }
                                 where (((bo.BRC_DELIVERY_DATE >= dDateFrom && bo.BRC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                 && ((bo.BRC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((bo.BRC_CRUDE_TYPE_ID.ToUpper().Equals(sCrude.ToUpper())) || (String.IsNullOrEmpty(sCrude)))
                                 && ((bo.BRC_CUST_NUM.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 && bo.BRC_TYPE == "S")                              
                                 //&& ((price.BRP_NUM == 1) || (price.BRP_NUM == 2))
                                 select new
                                 {
                                     bo,
                                     //price = "",
                                     MatName = mat.MET_MAT_DES_ENGLISH,
                                     CompanyName = com.MCO_SHORT_NAME,
                                     CustomerName = cus.MCD_NAME_1

                                 });
                   
                    if (query != null)
                    {
                        pModel.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>();
                        foreach (var item in query.ToList())
                        {
                            BorrowViewModel_SearchData temp = new BorrowViewModel_SearchData();
                            temp.aCrudeType = item.MatName;
                            temp.aCustomer = item.CustomerName;
                            if (item.bo.BRC_DELIVERY_DATE != null)
                            {
                                temp.aDeliveryDate = (item.bo.BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                            }
                            temp.aTripNo = item.bo.BRC_TRIP_NO;
                            temp.aSaleOrg = item.CompanyName;
                            temp.aVolumeBBL = (item.bo.BRC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.aVolumeMT = (item.bo.BRC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.aVolumeLitres = (item.bo.BRC_VOLUME_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");

                            var qryBorrowPrice = (from Pri in context.BORROW_PRICE
                                                  where (Pri.BRP_TRIP_NO.Contains(temp.aTripNo)
                                                  && ((Pri.BRP_NUM == 1) || (Pri.BRP_NUM == 2)))
                                                  select Pri).ToList();
                            //var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(temp.aTripNo.ToUpper())).ToList();
                            foreach (var PriceItem in qryBorrowPrice.ToList())
                            {
                                if ((PriceItem.BRP_NUM == 1) || (PriceItem.BRP_NUM == 2))
                                {
                                    if ((temp.aPrice == null) && PriceItem.BRP_NUM == 1)
                                    {
                                        temp.aPrice = (PriceItem.BRP_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                                    }
                                    if ((PriceItem.BRP_PRICE != null) && (PriceItem.BRP_NUM == 2))
                                    {
                                        temp.aPrice = (PriceItem.BRP_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                                    }

                                }

                            }
                            //temp.aPrice = (item.bo.BRC_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.aFX = (item.bo.BRC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.aPurchaseOrder = item.bo.BRC_PO_NO;
                            temp.aSaleOrder = item.bo.BRC_SALEORDER;
                            pModel.Borrow_Search.SearchData.Add(temp);
                        }

                        //    //For Test 
                        //    if (pModel.Borrow_Search == null) { pModel.Borrow_Search = new BorrowViewModel_Search(); }
                        //if (pModel.Borrow_Search.SearchData == null) { pModel.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>(); }

                        //pModel.Borrow_Search.SearchData.Add(new BorrowViewModel_SearchData
                        //{
                        //    aTripNo = "BO201706001",
                        //    aCustomer = "PTT PLC.",
                        //    aDeliveryDate = "17 May 2017",
                        //    aPlant = "SRI",
                        //    aCrudeType = "MURBAN",
                        //    aVolumeBBL = "198,637.0000",
                        //    aVolumeMT = "26,022.0210",
                        //    aVolumeLitres = "32,000.000",
                        //    aFX = "35.7898",
                        //    aPrice = "10.04",
                        //    aSaleOrder = "1110000123",
                        //    aPurchaseOrder = "1210000111"
                        //});

                        //pModel.Borrow_Search.SearchData.Add(new BorrowViewModel_SearchData
                        //{
                        //    aTripNo = "BO201706002",
                        //    aCustomer = "PTT PLC.",
                        //    aDeliveryDate = "17 May 2017",
                        //    aPlant = "SRI",
                        //    aCrudeType = "MURBAN",
                        //    aVolumeBBL = "98,637.0000",
                        //    aVolumeMT = "6,022.0210",
                        //    aVolumeLitres = "2,000.0000",
                        //    aFX = "35.7898",
                        //    aPrice = "10.04",
                        //    aSaleOrder = "1110000123",
                        //    aPurchaseOrder = "1210000111"

                        //});

                        //pModel.Borrow_Search.SearchData.Add(new BorrowViewModel_SearchData
                        //{
                        //    aTripNo = "BO201706003",
                        //    aCustomer = "PTT PLC.",
                        //    aDeliveryDate = "17 May 2017",
                        //    aPlant = "SRI",
                        //    aCrudeType = "MURBAN",
                        //    aVolumeBBL = "198,637.0000",
                        //    aVolumeMT = "26,022.0210",
                        //    aVolumeLitres = "32,000.0000",
                        //    aFX = "35.7898",
                        //    aPrice = "10.04",
                        //    aSaleOrder = "1110000123",
                        //    aPurchaseOrder = "1210000111"

                        //});
                        rtn = null;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }

            return rtn;
        }

        public ReturnValue SearchFIDoc(ref BorrowViewModel pModel)
        {

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var sTripNo = pModel.Borrow_Search.sTripNo == null ? "" : pModel.Borrow_Search.sTripNo.ToUpper();
                    var sCustomer = pModel.Borrow_Search.sCustomer == null ? "" : pModel.Borrow_Search.sCustomer.ToUpper();
                    var sCrude = pModel.Borrow_Search.sCrude == null ? "" : pModel.Borrow_Search.sCrude.ToUpper();
                    var sDeliveryDate = pModel.Borrow_Search.sDeliveryDate;                    
                    var sDateFrom = new System.DateTime().ToString("dd/MM/yyyy").Replace("0001", pModel.Borrow_Search.sYear);                    
                    var sDateTo = String.IsNullOrEmpty(pModel.Borrow_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy") : pModel.Borrow_Search.sDeliveryDate.Substring(14, 10);
                    var sComCodeFIDoc = pModel.Borrow_Search.sComCode ?? "";
                    var sYear = pModel.Borrow_Search.sYear ?? "";
                    var sDocument = pModel.Borrow_Search.sDocument ?? "";
                                        
                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);

                    var query = (from bo in context.BORROW_CRUDE
                                 join mat in context.MT_MATERIALS on bo.BRC_CRUDE_TYPE_ID equals mat.MET_NUM
                                 join com in context.MT_COMPANY on bo.BRC_COMPANY equals com.MCO_COMPANY_CODE
                                 join price in context.BORROW_PRICE on bo.BRC_TRIP_NO equals price.BRP_TRIP_NO
                                 join cus in context.MT_CUST_DETAIL on bo.BRC_CUST_NUM equals cus.MCD_ROW_ID
                                 where (((bo.BRC_DELIVERY_DATE >= dDateFrom && bo.BRC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                 && ((bo.BRC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((bo.BRC_CRUDE_TYPE_ID.ToUpper().Equals(sCrude.ToUpper())) || (String.IsNullOrEmpty(sCrude)))
                                 && ((bo.BRC_CUST_NUM.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 && bo.BRC_TYPE == "S")                             
                                 select new
                                 {
                                     bo,
                                     bo.BRC_TRIP_NO,
                                     price,
                                     MatName = mat.MET_MAT_DES_ENGLISH,
                                     CompanyName = com.MCO_SHORT_NAME,
                                     CustomerName = cus.MCD_NAME_1
                                 });
                    var s = query.ToList();
                    if (!string.IsNullOrEmpty(sComCodeFIDoc))
                    {
                        query = query.Where(p => p.bo.BRC_COMPANY.Contains(sComCodeFIDoc));
                    }
                    if (!string.IsNullOrEmpty(sDocument))
                    {
                        query = query.Where(p => p.bo.BRC_FI_DOC.Contains(sDocument));
                    }
                    query = query.OrderByDescending(c => c.price.BRP_NUM);
                    
                    var a = query.Count();
                    if (query != null)                    
                    {                        
                        if (pModel.Borrow_Search.SearchData == null) { pModel.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>(); }
                        foreach (var item in query.ToList())
                        {
                            BorrowViewModel_SearchData temp = new BorrowViewModel_SearchData();
                            temp.aCrudeType = item.MatName;
                            temp.aCustomer = item.CustomerName;                           
                            temp.aDeliveryDate = (item.bo.BRC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);                           
                            temp.aTripNo = item.bo.BRC_TRIP_NO;
                            temp.aSaleOrg = item.CompanyName;
                            temp.aVolumeBBL = (item.bo.BRC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.aVolumeMT = (item.bo.BRC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.aVolumeLitres = (item.bo.BRC_VOLUME_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.0000");

                            var qryBorrowPrice = (from Pri in context.BORROW_PRICE
                                                  where (Pri.BRP_TRIP_NO.Contains(temp.aTripNo))                                                
                                                  select Pri).ToList().OrderByDescending(c=>c.BRP_NUM);
                                                       
                            temp.aPrice_Num = item.price.BRP_NUM.ToString();
                            temp.aPrice = (item.price.BRP_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.aUnit = item.price.BRP_UNIT_ID;
                            temp.aInvoice = (item.price.BRP_INVOICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.aCurrency_invoice = item.price.BRP_UNIT_INV;   
                            temp.aFX = (item.bo.BRC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.aPurchaseOrder = item.bo.BRC_PO_NO;
                            temp.aSaleOrder = item.bo.BRC_SALEORDER;
                            pModel.Borrow_Search.SearchData.Add(temp);
                        }
                        var result = pModel.Borrow_Search.SearchData.Where(c=>c.aPrice != "0.00").FirstOrDefault();
                        pModel.Borrow_Search.SearchData = new List<BorrowViewModel_SearchData>();
                        pModel.Borrow_Search.SearchData.Add(result);
                      
                        rtn = null;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }

            return rtn;
        }

        public ReturnValue SaveData(ref BorrowViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();

            ResponseData resData = new ResponseData();
            try
            {
                if ((string.IsNullOrEmpty(pModel.pBorrowDetail.dTripNo)) || (string.IsNullOrEmpty(pModel.sBorrowDetail.dTripNo)))
                {
                    if ((!string.IsNullOrEmpty(pModel.pBorrowDetail.dSaleOrg)) || (!string.IsNullOrEmpty(pModel.sBorrowDetail.dSaleOrg)))
                    {
                        if ((!string.IsNullOrEmpty(pModel.pBorrowDetail.dSaleOrg)))
                        {
                            pModel.TripNoM = genTripNo(pModel.pBorrowDetail.dSaleOrg);
                        }
                        if ((!string.IsNullOrEmpty(pModel.sBorrowDetail.dSaleOrg)))
                        {
                            pModel.TripNoM = genTripNo(pModel.sBorrowDetail.dSaleOrg);
                        }
                    }
                    else
                    {
                        pModel.pBorrowDetail.dTripNo = "";
                        pModel.sBorrowDetail.dTripNo = "";
                    }

                }
                else
                {
                    pModel.TripNoM = pModel.pBorrowDetail.dTripNo;
                    pModel.TripNoM = pModel.sBorrowDetail.dTripNo;
                }




                Borrow obj = new Borrow();
                obj.BorrowDetailModelPO = new BorrowDetailModel();
                obj.BorrowDetailModelSale = new BorrowDetailModel();

                pModel.sBorrowDetail.dType = "S";
                pModel.sBorrowDetail.dTripNo = pModel.TripNoM;
                obj.sTripNo = pModel.sBorrowDetail.dTripNo;
                obj.BorrowDetailModelSale.sCustomer = pModel.sBorrowDetail.dCustomer;
                obj.BorrowDetailModelSale.sCrude = pModel.sBorrowDetail.dCrude;
                obj.BorrowDetailModelSale.sSaleOrg = pModel.sBorrowDetail.dSaleOrg;
                obj.BorrowDetailModelSale.sPlant = pModel.sBorrowDetail.dPlant;
                obj.BorrowDetailModelSale.sDeliveryDate = pModel.sBorrowDetail.dDeliveryDate;
                obj.BorrowDetailModelSale.sDueDate = pModel.sBorrowDetail.dDueDate;
                obj.BorrowDetailModelSale.sVolumeBBL = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeBBL);    //pModel.sBorrowDetail.dVolumeBBL;
                obj.BorrowDetailModelSale.sVolumeLitres = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeLitres); //pModel.sBorrowDetail.dVolumeLitres;
                obj.BorrowDetailModelSale.sVolumeMT = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeMT);  //pModel.sBorrowDetail.dVolumeMT;
                obj.BorrowDetailModelSale.sPONo = pModel.sBorrowDetail.dPONo;
                obj.BorrowDetailModelSale.sExchangeRate = String.Format("{0:0.####}", pModel.sBorrowDetail.dExchangeRate);  //pModel.sBorrowDetail.dExchangeRate;
                obj.BorrowDetailModelSale.sTotalAmount = String.Format("{0:0.####}", pModel.sBorrowDetail.dTotalAmount);  //pModel.sBorrowDetail.dTotalAmount;
                obj.BorrowDetailModelSale.sTotalUnit = pModel.sBorrowDetail.dCalcuUnit;
                obj.BorrowDetailModelSale.sRemark = pModel.sBorrowDetail.dRemark;
                obj.BorrowDetailModelSale.sTank = pModel.sBorrowDetail.dTank;
                obj.BorrowDetailModelSale.sPricePer = pModel.sBorrowDetail.dPricePer;
                obj.BorrowDetailModelSale.sPrice = String.Format("{0:0.####}", pModel.sBorrowDetail.dPrice);  //pModel.sBorrowDetail.dPrice;
                obj.BorrowDetailModelSale.sUnitID = pModel.sBorrowDetail.dUnitID;
                obj.BorrowDetailModelSale.sType = "S";
                obj.BorrowDetailModelSale.sStatus = pModel.sBorrowDetail.dStatus;
                obj.BorrowDetailModelSale.sRelease = pModel.sBorrowDetail.dRelease;
                obj.BorrowDetailModelSale.sTank = pModel.sBorrowDetail.dTank;
                obj.BorrowDetailModelSale.sSaleOrder = pModel.sBorrowDetail.dSaleOrder;
                obj.BorrowDetailModelSale.sPriceRelease = pModel.sBorrowDetail.dPriceRelease;
                obj.BorrowDetailModelSale.sDistriChann = pModel.sBorrowDetail.dDistriChann;
                obj.BorrowDetailModelSale.sTableName = pModel.sBorrowDetail.dTableName;
                obj.BorrowDetailModelSale.sMemo = pModel.sBorrowDetail.dMemo;
                obj.BorrowDetailModelSale.sDateSAP = pModel.sBorrowDetail.dDateSAP;
                obj.BorrowDetailModelSale.sFIDoc = pModel.sBorrowDetail.dFIDoc;
                obj.BorrowDetailModelSale.sDONo = pModel.sBorrowDetail.dDONo;
                obj.BorrowDetailModelSale.sStatusToSAP = pModel.sBorrowDetail.dStatusToSAP;
                obj.BorrowDetailModelSale.sFIDocReverse = pModel.sBorrowDetail.dFIDocReverse;
                obj.BorrowDetailModelSale.sIncoTermType = pModel.sBorrowDetail.dIncoTermType;
                obj.BorrowDetailModelSale.sTemp = pModel.sBorrowDetail.dTemp;
                obj.BorrowDetailModelSale.sDensity = pModel.sBorrowDetail.dDensity;
                obj.BorrowDetailModelSale.sStorageLocation = pModel.sBorrowDetail.dStorageLocation;
                obj.BorrowDetailModelSale.sUpdateDO = pModel.sBorrowDetail.dUpdateDO;
                obj.BorrowDetailModelSale.sUpdateGI = pModel.sBorrowDetail.dUpdateGI;

                obj.BorrowDetailModelSale.PriceList = new List<PriceModel>();
                int i = 0;
                foreach (var item in pModel.sBorrowDetail.BorrowPriceList.OrderBy(p => p.sPriceType))
                {
                    item.sTripNo = pModel.sBorrowDetail.dTripNo;
                    item.sumInvoice = 0;

                    if (pModel.sBorrowDetail.BorrowPriceList[i].sPrice != null)
                    {
                        if (obj.BorrowDetailModelSale.sTotalUnit == "BBL")
                        {
                            if (pModel.sBorrowDetail.BorrowPriceList[i].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelSale.sVolumeBBL)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice))) * (decimal.Parse(pModel.sBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelSale.sVolumeBBL)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice));
                            }
                        }
                        if (obj.BorrowDetailModelSale.sTotalUnit == "MT")
                        {
                            if (pModel.sBorrowDetail.BorrowPriceList[i].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelSale.sVolumeMT)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice))) * (decimal.Parse(pModel.sBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelSale.sVolumeMT)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice));
                            }
                        }
                        if (obj.BorrowDetailModelSale.sTotalUnit == "L30")
                        {
                            if (pModel.sBorrowDetail.BorrowPriceList[i].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelSale.sVolumeLitres)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice))) * (decimal.Parse(pModel.sBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelSale.sVolumeLitres)) * (decimal.Parse(pModel.sBorrowDetail.BorrowPriceList[i].sPrice));
                            }
                        }

                        item.sumInvoice = (decimal.Parse(String.Format("{0:0.####}", item.sumInvoice)));
                    }


                    obj.BorrowDetailModelSale.PriceList.Add(new PriceModel
                    {
                        sPriceType = item.sPriceType,
                        sDueDate = item.sDueDate,
                        sPrice = item.sPrice,
                        sPriceUnit = item.sPriceUnit,
                        sInvoice = item.sumInvoice.ToString(),
                        sInvUnit = item.sInvUnit,
                        sTripNo = item.sTripNo,
                        sNum = item.sPriceType,
                        sMemo = item.sMemo,
                        sRelease = item.sRelease

                    });
                    //k++;
                    i++;
                }

                ///PURCHASE////
                pModel.pBorrowDetail.dType = "P";
                pModel.pBorrowDetail.dTripNo = pModel.TripNoM;
                obj.sTripNo = pModel.pBorrowDetail.dTripNo;
                obj.BorrowDetailModelPO.sCustomer = pModel.pBorrowDetail.dCustomer;
                obj.BorrowDetailModelPO.sCrude = pModel.pBorrowDetail.dCrude;
                obj.BorrowDetailModelPO.sSaleOrg = pModel.sBorrowDetail.dSaleOrg;
                obj.BorrowDetailModelPO.sPlant = pModel.sBorrowDetail.dPlant;
                obj.BorrowDetailModelPO.sDeliveryDate = pModel.pBorrowDetail.dDeliveryDate;
                obj.BorrowDetailModelPO.sDueDate = pModel.pBorrowDetail.dDueDate;
                obj.BorrowDetailModelPO.sVolumeBBL = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeBBL); //pModel.pBorrowDetail.dVolumeBBL;
                obj.BorrowDetailModelPO.sVolumeLitres = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeLitres); //pModel.pBorrowDetail.dVolumeLitres;
                obj.BorrowDetailModelPO.sVolumeMT = String.Format("{0:0.####}", pModel.sBorrowDetail.dVolumeMT);  //pModel.pBorrowDetail.dVolumeMT;
                obj.BorrowDetailModelPO.sPONo = pModel.pBorrowDetail.dPONo;
                obj.BorrowDetailModelPO.sExchangeRate = String.Format("{0:0.####}", pModel.sBorrowDetail.dExchangeRate);  //pModel.pBorrowDetail.dExchangeRate;
                obj.BorrowDetailModelPO.sTotalAmount = String.Format("{0:0.####}", pModel.sBorrowDetail.dTotalAmount);  //pModel.pBorrowDetail.dTotalAmount;
                obj.BorrowDetailModelPO.sTotalUnit = pModel.pBorrowDetail.dCalcuUnit;
                obj.BorrowDetailModelPO.sTank = pModel.pBorrowDetail.dTank;
                obj.BorrowDetailModelPO.sRemark = pModel.pBorrowDetail.dRemark;
                obj.BorrowDetailModelPO.sPricePer = pModel.pBorrowDetail.dPricePer;
                obj.BorrowDetailModelPO.sPrice = String.Format("{0:0.####}", pModel.sBorrowDetail.dPrice); //pModel.pBorrowDetail.dPrice;              
                obj.BorrowDetailModelPO.sType = "P";
                obj.BorrowDetailModelPO.sStatus = pModel.pBorrowDetail.dStatus;
                obj.BorrowDetailModelPO.sRelease = pModel.pBorrowDetail.dRelease;
                obj.BorrowDetailModelPO.sTank = pModel.pBorrowDetail.dTank;
                obj.BorrowDetailModelPO.sSaleOrder = pModel.pBorrowDetail.dSaleOrder;
                obj.BorrowDetailModelPO.sPriceRelease = pModel.pBorrowDetail.dPriceRelease;
                obj.BorrowDetailModelPO.sDistriChann = pModel.pBorrowDetail.dDistriChann;
                obj.BorrowDetailModelPO.sTableName = pModel.pBorrowDetail.dTableName;
                obj.BorrowDetailModelPO.sMemo = pModel.pBorrowDetail.dMemo;
                obj.BorrowDetailModelPO.sDateSAP = pModel.pBorrowDetail.dDateSAP;
                obj.BorrowDetailModelPO.sFIDoc = pModel.pBorrowDetail.dFIDoc;
                obj.BorrowDetailModelPO.sDONo = pModel.pBorrowDetail.dDONo;
                obj.BorrowDetailModelPO.sStatusToSAP = pModel.pBorrowDetail.dStatusToSAP;
                obj.BorrowDetailModelPO.sFIDocReverse = pModel.pBorrowDetail.dFIDocReverse;
                obj.BorrowDetailModelPO.sIncoTermType = pModel.pBorrowDetail.dIncoTermType;
                obj.BorrowDetailModelPO.sTemp = pModel.pBorrowDetail.dTemp;
                obj.BorrowDetailModelPO.sDensity = pModel.pBorrowDetail.dDensity;
                obj.BorrowDetailModelPO.sStorageLocation = pModel.pBorrowDetail.dStorageLocation;
                obj.BorrowDetailModelPO.sUpdateDO = pModel.pBorrowDetail.dUpdateDO;
                obj.BorrowDetailModelPO.sUpdateGI = pModel.pBorrowDetail.dUpdateGI;
                obj.BorrowDetailModelPO.PriceList = new List<PriceModel>();

                int np = 3;
                int iP = 0;

                foreach (var item in pModel.pBorrowDetail.BorrowPriceList.OrderBy(p => p.sPriceType))
                {
                    item.sTripNo = pModel.pBorrowDetail.dTripNo;
                    item.sumInvoice = 0;
                    if (pModel.pBorrowDetail.BorrowPriceList[iP].sPrice != null)
                    {
                        if (obj.BorrowDetailModelPO.sTotalUnit == "BBL")
                        {
                            if (pModel.pBorrowDetail.BorrowPriceList[iP].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelPO.sVolumeBBL)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice))) * (decimal.Parse(pModel.pBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelPO.sVolumeBBL)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice));
                            }
                        }
                        if (obj.BorrowDetailModelPO.sTotalUnit == "MT")
                        {
                            if (pModel.pBorrowDetail.BorrowPriceList[iP].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelPO.sVolumeMT)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice))) * (decimal.Parse(pModel.pBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelPO.sVolumeMT)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice));
                            }
                        }
                        if (obj.BorrowDetailModelPO.sTotalUnit == "L30")
                        {
                            if (pModel.pBorrowDetail.BorrowPriceList[iP].sPriceUnit == "US4")
                            {
                                item.sumInvoice = ((decimal.Parse(obj.BorrowDetailModelPO.sVolumeLitres)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice))) * (decimal.Parse(pModel.pBorrowDetail.dExchangeRate));
                            }
                            else
                            {
                                item.sumInvoice = (decimal.Parse(obj.BorrowDetailModelPO.sVolumeLitres)) * (decimal.Parse(pModel.pBorrowDetail.BorrowPriceList[iP].sPrice));
                            }
                        }

                        item.sumInvoice = (decimal.Parse(String.Format("{0:0.####}", item.sumInvoice)));
                        //pModel.pBorrowDetail.BorrowPriceList[0].sPriceType = "3";
                        //pModel.pBorrowDetail.BorrowPriceList[1].sPriceType = "4";
                    }

                    obj.BorrowDetailModelPO.PriceList.Add(new PriceModel
                    {
                        sPriceType = item.sPriceType,
                        sDueDate = item.sDueDate,
                        sPrice = item.sPrice,
                        sPriceUnit = item.sPriceUnit,
                        sInvoice = item.sumInvoice.ToString(),
                        sInvUnit = item.sInvUnit,
                        sTripNo = item.sTripNo,
                        sNum = item.sPriceType,
                        sMemo = item.sMemo,
                        sRelease = item.sRelease

                    });
                    np++;
                    iP++;
                }


                var json = new JavaScriptSerializer().Serialize(obj);
                RequestData req = new RequestData();
                req.function_id = ConstantPrm.FUNCTION.F10000067;
                req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.state_name = "";
                req.req_parameters = new req_parameters();
                req.req_parameters.p = new List<p>();

                req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req.req_parameters.p.Add(new p { k = "system", v = "Borrow" });
                req.req_parameters.p.Add(new p { k = "trip_no", v = obj.sTripNo });
                req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                req.extra_xml = "";

                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                resData = service.CallService(req);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        private string genTripNo(string com_code)
        {
            string ret = "BO";
            com_code = com_code.Substring(0, 2);
            string yy = DateTime.Now.ToString("yyMM", new System.Globalization.CultureInfo("en-US"));

            string TripPrefix = ret + com_code + yy;
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.BORROW_CRUDE
                                 where v.BRC_TRIP_NO.Contains(TripPrefix)
                                 orderby v.BRC_TRIP_NO descending
                                 select v.BRC_TRIP_NO).ToList();


                    if (query != null && query.Count() > 0)
                    {
                        string lastTrip = query[0];
                        if (lastTrip != "")
                            lastTrip = lastTrip.Replace(TripPrefix, "");

                        string nextTrip = (int.Parse(lastTrip) + 1).ToString().PadLeft(2, '0');

                        ret = TripPrefix + nextTrip;
                    }
                    else
                    {
                        ret = TripPrefix + "01";
                    }
                }

            }
            catch (Exception ex) { }

            return ret;
        }

        public void setInitail_ddl(ref BorrowViewModel pModel)
        {

            if (pModel.ddl_Crude == null)
            {
                pModel.ddl_Crude = new List<SelectListItem>();
                pModel.ddl_Crude = getMaterial();
            }
            if (pModel.ddl_Customer == null)
            {
                pModel.ddl_Customer = new List<SelectListItem>();
                pModel.ddl_Customer = getCustomer();
            }

            if (pModel.ddl_SaleOrg == null)
            {
                pModel.ddl_SaleOrg = new List<SelectListItem>();
                pModel.ddl_SaleOrg = getCompany();
            }
            if (pModel.ddl_Plant == null)
            {
                pModel.ddl_Plant = new List<SelectListItem>();
                pModel.ddl_Plant = getPlant("1100,1400");
            }
            if (pModel.ddl_Unit == null)
            {
                pModel.ddl_Unit = new List<SelectListItem>();
                pModel.ddl_Unit = getUnit();
            }
            if (pModel.ddl_UnitPrice == null)
            {
                pModel.ddl_UnitPrice = new List<SelectListItem>();
                pModel.ddl_UnitPrice = getUnitPrice();
            }
            if (pModel.ddl_UnitTotal == null)
            {
                pModel.ddl_UnitTotal = new List<SelectListItem>();
                pModel.ddl_UnitTotal = getUnitTotal();
            }
            if (pModel.ddl_UnitINV == null)
            {
                pModel.ddl_UnitINV = new List<SelectListItem>();
                pModel.ddl_UnitINV = getUnitINV();
            }
            if (pModel.ddl_PriceStatus == null)
            {
                pModel.ddl_PriceStatus = new List<SelectListItem>();
                pModel.ddl_PriceStatus = getPriceStatus();
            }
        }

        // get Data Customer
        public static List<SelectListItem> getCustomer(string type = "PCF", bool isOptional = false, string message = "")
        {
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(PROJECT_NAME_SPACE, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_FK_CUS).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVendor(string system = "CPAI", string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VENDOR> mt_vendor = VendorDAL.GetVendor(system, type, ACTIVE).OrderBy(x => x.VND_NAME1).ToList();
            return insertSelectListValue(mt_vendor.Select(x => x.VND_NAME1).ToList(), mt_vendor.Select(x => x.VND_ACC_NUM_VENDOR).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getCompany(string pStatus = "ACTIVE")
        {
            List<SelectListItem> rtn = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_COMPANY
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCO_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCO_SHORT_NAME }
                                    select new { value = d.MCO_COMPANY_CODE, text = d.MCO_SHORT_NAME.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.text))
                        {
                            rtn.Add(new SelectListItem { Value = item.value, Text = item.text });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                //var LineNumber = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
                //Log.Info("Error LineNumber : " + LineNumber + " => " + ex.Message);
                //Log.Error(ex.Message);
                return rtn;

            }
        }

        public static List<SelectListItem> getPlant(string pCompanyCode)
        {
            List<SelectListItem> lstPlant = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sListComCode = pCompanyCode.Replace(",", "\",\"");
                    sListComCode = "\"" + sListComCode + "\"";
                    var tmpPlant = context.MT_PLANT.Where(z => z.MPL_STATUS == "ACTIVE" && (sListComCode.Contains(z.MPL_COMPANY_CODE) || String.IsNullOrEmpty(pCompanyCode))).OrderBy(o => o.MPL_PLANT);

                    foreach (var item in tmpPlant.ToList())
                    {
                        //lstPlant.Add(new SelectListItem { Value = item.MPL_PLANT, Text = item.MPL_PLANT });
                        //lstPlant.Add(new SelectListItem { Value = item.MPL_SHORT_NAME, Text = item.MPL_SHORT_NAME });
                        lstPlant.Add(new SelectListItem { Value = item.MPL_PLANT, Text = item.MPL_SHORT_NAME });
                    }

                }

            }
            catch (Exception ex)
            {
                lstPlant.Add(new SelectListItem { Value = "", Text = "" });
            }

            return lstPlant;
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
                //else
                //{
                //    selectList.Add(new SelectListItem { Text = "", Value = "" });
                //}
            }
            //selectList.Add(new SelectListItem { Text = "SPOT", Value = "SPOT" });
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });

                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<SelectListItem> getUnit()
        {

            string JsonD = MasterData.GetJsonMasterSetting("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
            GlobalConfigBorrow dataList = (GlobalConfigBorrow)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigBorrow));

            List<SelectListItem> dList = new List<SelectListItem>();
            try
            {
                foreach (var item in dataList.PCF_UNIT_BW)
                {
                    dList.Add(new SelectListItem { Value = item.Code, Text = item.Name });
                }
            }
            catch (Exception ex)
            {

            }
            return dList;
        }

        public static List<SelectListItem> getUnitPrice()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "US4", Text = "US4" });
            dList.Add(new SelectListItem { Value = "TH4", Text = "TH4" });

            return dList;
        }

        public static List<SelectListItem> getUnitTotal()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "BBL", Text = "BBL" });
            dList.Add(new SelectListItem { Value = "MT", Text = "MT" });
            dList.Add(new SelectListItem { Value = "L30", Text = "Litres" });

            return dList;
        }

        public static List<SelectListItem> getUnitINV()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "THB", Text = "THB" });

            return dList;
        }

        public static List<SelectListItem> getPriceStatus()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "1", Text = "1" });
            dList.Add(new SelectListItem { Value = "2", Text = "2" });
            dList.Add(new SelectListItem { Value = "3", Text = "3" });
            dList.Add(new SelectListItem { Value = "4", Text = "4" });

            return dList;
        }

        public void CreateUpateSO(ref BorrowViewModel pModel)
        {
            var selectedTripNo = pModel.Borrow_Search.sTripNo_Select;

            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var qryBorrowCrude = context.BORROW_CRUDE.Where(c => c.BRC_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper())).ToList();
                    var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper())).ToList();
                    string type_status = "CREATE";
                    if (qryBorrowCrude.Count > 0)
                    {
                        if(!string.IsNullOrEmpty( qryBorrowCrude[0].BRC_SALEORDER))
                        {
                            type_status = "CHANGE";
                        }
                        int n = 0;
                        int running_index = 0;
                        if (qryBorrowCrude[n].BRC_TYPE == "S")
                        {
                            //string BrpNum = qryBorrowPrice[n].BRP_NUM.ToString();

                            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();
                            obj.SalePrice = new List<SalesPriceCreateModel>();

                            #region Error
                            //string str = qryBorrowCrude.ToList()[n].BRC_DELIVERY_DATE.ToString();
                            //DateTime dt1 = new DateTime() ;
                            //var dt2 = new DateTime(); 
                            //try
                            //{
                            //    dt1 = (DateTime.ParseExact(str.Substring(0,str.IndexOf(" ")), format, provider));
                            //}
                            //catch (Exception ex)
                            //{
                            //obj.SalePrice.KBETR = Decimal.Parse(qryBorrowPrice[n].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000");
                            //if (BrpNum == "1")
                            //{
                            //    obj.SalePrice.KSCHL = "ZCR1";
                            //}
                            //else if (BrpNum == "2")
                            //{
                            //    obj.SalePrice.KSCHL = "ZCR2";
                            //}
                            //    }
                            //obj.SalePrice.KUNNR = qryBorrowCrude[n].BRC_CUST_NUM; //"0000000013";//Customer Code
                            //obj.SalePrice.MATNR = qryBorrowCrude[n].BRC_CRUDE_TYPE_ID; //"YMB";//Material
                            //obj.SalePrice.VKORG = qryBorrowCrude[n].BRC_COMPANY; //"1100";//Company Code
                            //obj.SalePrice.VTWEG = "31";//"31";//DC
                            //obj.SalePrice.WERKS = qryBorrowCrude[n].BRC_PLANT;//"1200";//Plant
                            //obj.SalePrice.ZZVSTEL = "12SV";//Shipping Point
                            //Borrow TOP = Z1I4, Borrow TLB = Z3I4
                            //string DocType = "";
                            //string DC = "";//Distribution Channel
                            //if (qryBorrowCrude[n].BRC_COMPANY == "1100")  //TOP 1100
                            //{
                            //    DocType = "Z1I4";
                            //    DC = "31";
                            //}
                            //else if (qryBorrowCrude[n].BRC_COMPANY == "1400") //TLB 1400
                            //{
                            //    DocType = "Z3I4";
                            //    DC = "12";
                            //}

                            //}
                            //if (dt1.Year > 2500)
                            //{
                            //    dt2 = new DateTime(dt1.Year - 1086, dt1.Month, dt1.Day);
                            //}
                            //string str1 = (dt2).ToString("yyyy-MM-dd");
                            //obj.SalePrice.DATAB = String.IsNullOrEmpty(qryBorrowCrude.ToList()[n].BRC_DELIVERY_DATE.ToString()) ? "" : str1; // "2017-06-01";
                            //obj.SalePrice.DATBI = String.IsNullOrEmpty(qryBorrowCrude.ToList()[n].BRC_DELIVERY_DATE.ToString()) ? "" : str1; // "2017-06-01";

                            //    int i = 0;
                            //    for (i = 0; i < pModel.Borrow_Search.SearchData.Count; i++)
                            //    {
                            //        if (pModel.Borrow_Search.SearchData[i].aTripNo == selectedTripNo)
                            //        {
                            //            obj.SalePrice.KBETR = Decimal.Parse(pModel.Borrow_Search.SearchData[i].aPrice.ToString() ?? "0").ToString("#,##0.0000");

                            //        }
                            //    }

                            //    obj.SalePrice.KMEIN = qryBorrowCrude[n].BRC_UNIT_TOTAL; //"BBL";//Sales Unit
                            //    obj.SalePrice.KONWA = qryBorrowPrice[n].BRP_UNIT_ID; //"TH4";//Currency
                            //    for (i = 0; i < qryBorrowPrice.Count; i++)
                            //    {
                            //        if (Decimal.Parse(qryBorrowPrice[i].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000") == obj.SalePrice.KBETR && qryBorrowPrice[i].BRP_NUM.ToString() == "2")
                            //        {
                            //            obj.SalePrice.KSCHL = "ZCR2";
                            //            i = qryBorrowPrice.Count + 1;
                            //        }
                            //        else {
                            //            obj.SalePrice.KSCHL = "ZCR1";
                            //        }
                            //    }

                            //obj.SalePrice.KUNNR = qryBorrowCrude[n].BRC_CUST_NUM; //"0000000013";//Customer Code
                            //obj.SalePrice.MATNR = qryBorrowCrude[n].BRC_CRUDE_TYPE_ID; //"YMB";//Material
                            //obj.SalePrice.VKORG = qryBorrowCrude[n].BRC_COMPANY; //"1100";//Company Code
                            //obj.SalePrice.VTWEG = "31";//"31";//DC
                            //obj.SalePrice.WERKS = qryBorrowCrude[n].BRC_PLANT;//"1200";//Plant
                            //obj.SalePrice.ZZVSTEL = "12SV";//Shipping Point



                            ////Borrow TOP = Z1I4, Borrow TLB = Z3I4
                            //string DocType = "";
                            //string DC = "";//Distribution Channel
                            //if (qryBorrowCrude[n].BRC_COMPANY == "1100")  //TOP 1100
                            //{
                            //    DocType = "Z1I4";
                            //    DC = "31";
                            //}
                            //else if (qryBorrowCrude[n].BRC_COMPANY == "1400") //TLB 1400
                            //{
                            //    DocType = "Z3I4";
                            //    DC = "12";
                            //}
                            //obj.SaleOrder = new SaleOrderModel();
                            //obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                            //obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                            //{
                            //    DOC_TYPE = DocType,
                            //    SALES_ORG = qryBorrowCrude[n].BRC_COMPANY,
                            //    DISTR_CHAN = DC,
                            //    REQ_DATE_H = String.IsNullOrEmpty(qryBorrowCrude.ToList()[n].BRC_DELIVERY_DATE.ToString()) ? "" : str1, // "2017-06-01";
                            //    PURCH_NO_C = qryBorrowCrude[n].BRC_PO_NO,//"TEST_CIP_CLEAR_LINE_01",
                            //    PURCH_NO_S = qryBorrowCrude[n].BRC_PO_NO,//"TEST_CIP_CLEAR_LINE_01",
                            //    CURRENCY = qryBorrowPrice[n].BRP_UNIT_ID//"THB"
                            //});

                            //obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                            //obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                            //{      
                            //    ITM_NUMBER = "000010",
                            //    MATERIAL  = qryBorrowCrude[n].BRC_CRUDE_TYPE_ID,
                            //    PLANT = qryBorrowCrude[n].BRC_PLANT,//"1200",
                            //    ITEM_CATEG = "ZTW1",
                            //    SALES_UNIT = qryBorrowCrude[n].BRC_UNIT_TOTAL//"BBL"
                            //});

                            //obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                            //obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                            //{
                            //    PARTN_NUMB = qryBorrowCrude.ToList()[n].BRC_CUST_NUM
                            //});

                            //obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                            //obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                            //{
                            //    REQ_DATE = String.IsNullOrEmpty(qryBorrowCrude.ToList()[n].BRC_DELIVERY_DATE.ToString()) ? "" : str1,
                            //    DATE_TYPE = "1",

                            //});

                            //obj.SaleOrder.SALESDOCUMENT_IN = "";
                            //obj.SaleOrder.Flag = "";

                            //var json = new JavaScriptSerializer().Serialize(obj);

                            //RequestData req = new RequestData();
                            //req.function_id = ConstantPrm.FUNCTION.F10000059;
                            //req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                            //req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                            //req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                            //req.state_name = "";
                            //req.req_parameters = new req_parameters();
                            //req.req_parameters.p = new List<p>();

                            //req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                            //req.req_parameters.p.Add(new p { k = "system", v = "tempBorrow" });
                            //req.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                            //req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                            //req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                            //req.extra_xml = "";

                            //ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                            //resData = service.CallService(req);
                            //if (resData.result_code == "1")
                            //{
                            //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            //    rtn.Status = true;
                            //}
                            //else
                            //{
                            //    rtn.Message = resData.result_desc;
                            //    rtn.Status = false;
                            //}
                            //if (resData.result_code == "1")
                            //{
                            //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            //    rtn.Status = true;
                            //}
                            //else
                            //{
                            //    rtn.Message = resData.result_desc;
                            //    rtn.Status = false;
                            //}
                            #endregion

                            #region "For Use"
                            //DateTime dt1 = new DateTime();
                            //var dt2 = new DateTime();
                            if (!string.IsNullOrEmpty(selectedTripNo))
                            {
                                var tempSelect = pModel.Borrow_Search.SearchData.Where(p => p.aTripNo == selectedTripNo).ToList();
                                if (tempSelect != null && tempSelect.Count > 0)
                                {
                                    //Create Sale Price
                                    obj.SalePrice = new List<SalesPriceCreateModel>();
                                    int m = 1;

                                    foreach (var qryBorrowC in qryBorrowCrude)
                                    {
                                        SalesPriceCreateModel temp = new SalesPriceCreateModel();
                                        if (qryBorrowC.BRC_DELIVERY_DATE != null)
                                        {
                                            DateTime datePrice = qryBorrowC.BRC_DELIVERY_DATE ?? DateTime.Now;
                                            string[] strDate = datePrice.ToString(format, provider).Replace('/', '-').Split('-');
                                            temp.DATAB = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                            temp.DATBI = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                        }

                                        temp.KBETR = Decimal.Parse(qryBorrowPrice[running_index].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000");
                                        temp.KMEIN = qryBorrowC.BRC_UNIT_TOTAL;
                                        temp.KONWA = qryBorrowPrice[running_index].BRP_UNIT_ID;
                                        temp.KSCHL = (Decimal.Parse(qryBorrowPrice[running_index].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000") == temp.KBETR && qryBorrowPrice[running_index].BRP_NUM.ToString() == "2") ? "ZCR2" : "ZCR1";
                                        temp.KUNNR = qryBorrowC.BRC_CUST_NUM;//"0000000013";//Customer Code
                                        temp.MATNR = qryBorrowC.BRC_CRUDE_TYPE_ID; ////"YMB";//Material
                                        temp.VKORG = qryBorrowC.BRC_COMPANY; //"1100";//ดึงจาก company
                                        temp.VTWEG = "31";//"31";//Config DISTRIBUTION CHANEL
                                        temp.WERKS = qryBorrowC.BRC_PLANT;
                                        temp.ZZVSTEL = "12SV";//"12PI";//Config sHIPPING POINT
                                        obj.SalePrice.Add(temp);
                                        m++;
                                    }
                                    //Create Sale Order
                                    //if (dt1.Year > 2500)
                                    //{
                                    //    dt2 = new DateTime(dt1.Year - 1086, dt1.Month, dt1.Day);
                                    //}
                                    string str1 = DateTime.Now.ToString("yyyy-MM-dd",provider);
                                    string DocType = "";
                                    string DC = "";//Distribution Channel
                                    if (qryBorrowCrude[n].BRC_COMPANY == "1100")  //TOP 1100
                                    {
                                        DocType = "Z1I4";
                                        DC = "31";
                                    }
                                    else if (qryBorrowCrude[n].BRC_COMPANY == "1400") //TLB 1400
                                    {
                                        DocType = "Z3I4";
                                        DC = "12";
                                    }
                                    obj.SaleOrder = new SaleOrderModel();
                                    obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                                    obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                                    {
                                        DOC_TYPE = DocType, //fix
                                        SALES_ORG = qryBorrowCrude[running_index].BRC_COMPANY,//ดึงจาก company
                                        DISTR_CHAN = DC,//"31",//Config DISTRIBUTION CHANEL
                                        REQ_DATE_H = String.IsNullOrEmpty(qryBorrowCrude.ToList()[running_index].BRC_DELIVERY_DATE.ToString()) ? "" : str1,
                                        PURCH_NO_C = qryBorrowCrude[running_index].BRC_PO_NO,
                                        PURCH_NO_S = qryBorrowCrude[running_index].BRC_PO_NO,
                                        CURRENCY = "THB"
                                    });

                                    obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                                    obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                                    {
                                        ITM_NUMBER = "000010",
                                        MATERIAL = qryBorrowCrude[running_index].BRC_CRUDE_TYPE_ID,
                                        PLANT = qryBorrowCrude[running_index].BRC_PLANT,//"1200",
                                        ITEM_CATEG = "ZTW1",
                                        SALES_UNIT = qryBorrowCrude[running_index].BRC_UNIT_TOTAL//"BBL"
                                    });

                                    obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                                    obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                                    {
                                        PARTN_NUMB = qryBorrowCrude.ToList()[running_index].BRC_CUST_NUM
                                    });

                                    obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                                    obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                                    {
                                        REQ_DATE = String.IsNullOrEmpty(qryBorrowCrude.ToList()[running_index].BRC_DELIVERY_DATE.ToString()) ? "" : str1,
                                        DATE_TYPE = "1",
                                    });

                                    obj.SaleOrder.SALESDOCUMENT_IN = "";
                                    obj.SaleOrder.Flag = "";

                                    var json = new JavaScriptSerializer().Serialize(obj);

                                    RequestData req1 = new RequestData();
                                    req1.function_id = ConstantPrm.FUNCTION.F10000059;
                                    req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                                    req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                                    req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                                    req1.state_name = "";
                                    req1.req_parameters = new req_parameters();
                                    req1.req_parameters.p = new List<p>();

                                    req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                                    req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                                    req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                                    req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                                    req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                                    req1.req_parameters.p.Add(new p { k = "status", v = type_status });
                                    req1.extra_xml = "";

                                    ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                                    resData = service1.CallService(req1);

                                    if (resData.result_code == "1")
                                    {
                                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                        rtn.Status = true;
                                    }
                                    else
                                    {
                                        rtn.Message = resData.result_desc;
                                        rtn.Status = false;
                                    }
                                }
                            }
                            #endregion

                            #region "For Test"
                            //////For Test
                            //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();
                            //obj1.SalePrice = new SalesPriceCreateModel();
                            //obj1.SalePrice.DATAB = "2017-06-27";
                            //obj1.SalePrice.DATBI = "2017-06-27";
                            //obj1.SalePrice.KBETR = "1609.140";
                            //obj1.SalePrice.KMEIN = "BBL";
                            //obj1.SalePrice.KONWA = "TH4";
                            //obj1.SalePrice.KSCHL = "ZCR1";
                            //obj1.SalePrice.KUNNR = "0000000023";
                            //obj1.SalePrice.MATNR = "YMURBAN";
                            //obj1.SalePrice.VKORG = "1100";
                            //obj1.SalePrice.VTWEG = "31";
                            //obj1.SalePrice.WERKS = "1200";
                            //obj1.SalePrice.ZZVSTEL = "12SV";//12SV

                            //obj1.SaleOrder = new SaleOrderModel();
                            //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                            //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                            //{
                            //    DOC_TYPE = "Z1I4", //Borrow TOP=Z1I4, Borrow TLB=Z3I4
                            //    SALES_ORG = "1100",
                            //    DISTR_CHAN = "31",
                            //    DIVISION = "00",
                            //    REQ_DATE_H = "2017-06-27",
                            //    DATE_TYPE = "1",
                            //    PURCH_NO_C = "TEST_CIP_CLEAR_LINE_01",
                            //    PURCH_NO_S = "TEST_CIP_CLEAR_LINE_01",
                            //    CURRENCY = "THB"
                            //});


                            //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                            //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                            //{
                            //    ITM_NUMBER = "000010",
                            //    MATERIAL = "YMURBAN",
                            //    PLANT = "1200",
                            //    ITEM_CATEG = "ZTW1",
                            //    SALES_UNIT = "BBL"
                            //});

                            //obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                            //obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                            //{
                            //    PARTN_NUMB = "0000000023"
                            //});

                            //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                            //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                            //{
                            //    REQ_DATE = "2017-06-27",
                            //    DATE_TYPE = "1",
                            //    REQ_QTY = 1609.140M
                            //});

                            //obj1.SaleOrder.SALESDOCUMENT_IN = "";
                            //obj1.SaleOrder.Flag = "";

                            //var json1 = new JavaScriptSerializer().Serialize(obj1);

                            //RequestData req1 = new RequestData();
                            //req1.function_id = ConstantPrm.FUNCTION.F10000059;
                            //req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                            //req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                            //req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                            //req1.state_name = "";
                            //req1.req_parameters = new req_parameters();
                            //req1.req_parameters.p = new List<p>();

                            //req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                            //req1.req_parameters.p.Add(new p { k = "system", v = "tempBorrow" });
                            //req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                            //req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                            //req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                            //req1.extra_xml = "";

                            //ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                            //resData = service1.CallService(req1);

                            //if (resData.result_code == "1")
                            //{
                            //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            //    rtn.Status = true;
                            //}
                            //else
                            //{
                            //    rtn.Message = resData.result_desc;
                            //    rtn.Status = false;
                            //}

                            #endregion
                            n++;
                            running_index++;

                        }
                        if (qryBorrowCrude[n].BRC_TYPE == "P") // Sap to Memo
                        {
                            n++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void CancelSO(ref BorrowViewModel pModel)
        {
            var selectedTripNo = pModel.Borrow_Search.sTripNo_Select;

            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var qryBorrowCrude = context.BORROW_CRUDE.Where(c => c.BRC_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper())).ToList();
                    var qryBorrowPrice = context.BORROW_PRICE.Where(z => z.BRP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper())).ToList();

                    if (qryBorrowCrude.Count > 0)
                    {
                        int n = 0;
                        int running_index = 0;
                        for (int c = 0; qryBorrowCrude.Count > c; c++)
                        {
                            if (qryBorrowCrude[n].BRC_TYPE == "S")
                            {
                                SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();
                                obj.SalePrice = new List<SalesPriceCreateModel>();
                                DateTime dt1 = new DateTime();
                                var dt2 = new DateTime();
                                if (!string.IsNullOrEmpty(selectedTripNo))
                                {
                                    var tempSelect = pModel.Borrow_Search.SearchData.Where(p => p.aTripNo == selectedTripNo).ToList();
                                    if (tempSelect != null && tempSelect.Count > 0)
                                    {
                                        //Create Sale Price
                                        obj.SalePrice = new List<SalesPriceCreateModel>();
                                        int m = 1;

                                        foreach (var qryBorrowC in qryBorrowCrude)
                                        {
                                            SalesPriceCreateModel temp = new SalesPriceCreateModel();
                                            if (qryBorrowC.BRC_DELIVERY_DATE != null)
                                            {
                                                DateTime datePrice = qryBorrowC.BRC_DELIVERY_DATE ?? DateTime.Now;
                                                string[] strDate = datePrice.ToString(format, provider).Replace('/', '-').Split('-');
                                                temp.DATAB = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                                temp.DATBI = strDate[2] + "-" + strDate[1] + "-" + strDate[0];// "2017-06-01";
                                            }

                                            temp.KBETR = Decimal.Parse(qryBorrowPrice[running_index].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000");
                                            temp.KMEIN = qryBorrowC.BRC_UNIT_TOTAL;
                                            temp.KONWA = qryBorrowPrice[running_index].BRP_UNIT_ID;
                                            temp.KSCHL = (Decimal.Parse(qryBorrowPrice[running_index].BRP_PRICE.ToString() ?? "0").ToString("#,##0.0000") == temp.KBETR && qryBorrowPrice[running_index].BRP_NUM.ToString() == "2") ? "ZCR2" : "ZCR1";
                                            temp.KUNNR = qryBorrowC.BRC_CUST_NUM;//"0000000013";//Customer Code
                                            temp.MATNR = qryBorrowC.BRC_CRUDE_TYPE_ID; ////"YMB";//Material
                                            temp.VKORG = qryBorrowC.BRC_COMPANY; //"1100";//ดึงจาก company
                                            temp.VTWEG = "31";//"31";//Config DISTRIBUTION CHANEL
                                            temp.WERKS = qryBorrowC.BRC_PLANT;
                                            temp.ZZVSTEL = "12SV";//"12PI";//Config sHIPPING POINT
                                            obj.SalePrice.Add(temp);
                                            m++;
                                        }
                                        //Create Sale Order
                                        if (dt1.Year > 2500)
                                        {
                                            dt2 = new DateTime(dt1.Year - 1086, dt1.Month, dt1.Day);
                                        }
                                        string str1 = (dt2).ToString("yyyy-MM-dd");
                                        string DocType = "";
                                        string DC = "";//Distribution Channel
                                        if (qryBorrowCrude[n].BRC_COMPANY == "1100")  //TOP 1100
                                        {
                                            DocType = "Z1I4";
                                            DC = "31";
                                        }
                                        else if (qryBorrowCrude[n].BRC_COMPANY == "1400") //TLB 1400
                                        {
                                            DocType = "Z3I4";
                                            DC = "12";
                                        }
                                        obj.SaleOrder = new SaleOrderModel();
                                        obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                                        obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                                        {
                                            DOC_TYPE = "Z1P3", //fix
                                            SALES_ORG = qryBorrowCrude[running_index].BRC_COMPANY,//ดึงจาก company
                                            DISTR_CHAN = DC,//"31",//Config DISTRIBUTION CHANEL
                                            REQ_DATE_H = String.IsNullOrEmpty(qryBorrowCrude.ToList()[running_index].BRC_DELIVERY_DATE.ToString()) ? "" : str1,
                                            PURCH_NO_C = qryBorrowCrude[running_index].BRC_PO_NO,
                                            PURCH_NO_S = qryBorrowCrude[running_index].BRC_PO_NO,
                                            CURRENCY = "THB"
                                        });

                                        obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                                        obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                                        {
                                            ITM_NUMBER = "000010",
                                            MATERIAL = qryBorrowCrude[running_index].BRC_CRUDE_TYPE_ID,
                                            PLANT = qryBorrowCrude[running_index].BRC_PLANT,//"1200",
                                            ITEM_CATEG = "ZTW1",
                                            SALES_UNIT = qryBorrowCrude[running_index].BRC_UNIT_TOTAL//"BBL"
                                        });

                                        obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                                        obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                                        {
                                            PARTN_NUMB = qryBorrowCrude.ToList()[running_index].BRC_CUST_NUM
                                        });

                                        obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                                        obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                                        {
                                            REQ_DATE = String.IsNullOrEmpty(qryBorrowCrude.ToList()[running_index].BRC_DELIVERY_DATE.ToString()) ? "" : str1,
                                            DATE_TYPE = "1",
                                        });

                                        obj.SaleOrder.SALESDOCUMENT_IN = "";
                                        obj.SaleOrder.Flag = "X";

                                        var json = new JavaScriptSerializer().Serialize(obj);

                                        RequestData req1 = new RequestData();
                                        req1.function_id = ConstantPrm.FUNCTION.F10000059;
                                        req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                                        req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                                        req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                                        req1.state_name = "";
                                        req1.req_parameters = new req_parameters();
                                        req1.req_parameters.p = new List<p>();

                                        req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                                        req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                                        req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                                        req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                                        req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                                        req1.extra_xml = "";

                                        ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                                        resData = service1.CallService(req1);

                                        if (resData.result_code == "1")
                                        {
                                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                            rtn.Status = true;
                                        }
                                        else
                                        {
                                            rtn.Message = resData.result_desc;
                                            rtn.Status = false;
                                        }
                                    }
                                }
                                n++;
                                running_index++;
                            }
                            if (qryBorrowCrude[n].BRC_TYPE == "P") // Sap to Memo
                            {
                                n++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void CreateUpdateMEMO(ref BorrowViewModel pModel)
        {

        }

        public void CancelMEMO(ref BorrowViewModel pModel)
        {

        }

        public void CreateUpdatePO(ref BorrowViewModel pModel)
        {

        }

        public void CancelPO(ref BorrowViewModel pModel)
        {

        }

    }
}