﻿using Common.Logging;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CamTemplateServiceModel
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ReturnValue Add(CamTemplateViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                if(pModel.Reason == null)
                {
                    rtn.Message = "Reason much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CAM_TEMP_DAL dal = new MT_CAM_TEMP_DAL();
                MT_CAM_TEMP ent = new MT_CAM_TEMP();

                DateTime now = DateTime.Now;
                try
                {
                    var Code = ShareFn.GenerateCodeByDate("CPAI");
                    ent.COCT_ROW_ID = Code;
                    ent.COCT_VERSION = (MT_CAM_TEMP_DAL.GetMaxVersion() + 1) + "";

                    foreach(var item in pModel.JSON.Spiral)
                    {
                        item.Component = ShareFn.EncodeString(item.Component);                        
                    }
           
                    ent.COCT_JSON = new JavaScriptSerializer().Serialize(pModel.JSON);
                    ent.COCT_REASON = pModel.Reason;
                    ent.COCT_CREATED_BY = pUser;
                    ent.COCT_CREATED = now;
                    ent.COCT_UPDATED_BY = pUser;
                    ent.COCT_UPDATED = now;

                    dal.Save(ent);

                    pModel.CamCode = Code;
                    rtn.newID = Code;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Search(ref CamTemplateViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCamCode = pModel.sCamCode == null ? "" : pModel.sCamCode.ToUpper();
                    var sVersion = pModel.sVersion == null ? "" : pModel.sVersion.ToUpper();
                    var pre_query = (from u in context.MT_CAM_TEMP
                        group u by u.COCT_VERSION into g
                        select new { date = g.Max(x => x.COCT_CREATED) });

                    var query = from p in pre_query
                                from c in context.MT_CAM_TEMP
                                where c.COCT_CREATED == p.date
                                select new CamTemplateViewModel_SearchData
                                {
                                    dVersion = c.COCT_VERSION,
                                    dCamCode = c.COCT_ROW_ID,
                                    dUpdatedDate = c.COCT_UPDATED,
                                    dUpdatedBy = c.COCT_UPDATED_BY,
                                    dReason = c.COCT_REASON                              
                                };

                    if (!string.IsNullOrEmpty(sCamCode))
                        query = query.Where(p => p.dCamCode.ToUpper().Contains(sCamCode));
                    if (!string.IsNullOrEmpty(sVersion))
                        query = query.Where(p => p.dVersion.ToUpper().Contains(sVersion));


                    if (query != null)
                    {
                        pModel.sSearchData = query.ToList();
                        pModel.sSearchData = new List<CamTemplateViewModel_SearchData>();
                        foreach (var item in query)
                        {
                            pModel.sSearchData.Add(new CamTemplateViewModel_SearchData
                            {
                                dCamCode = string.IsNullOrEmpty(item.dCamCode) ? "" : item.dCamCode,
                                dVersion = string.IsNullOrEmpty(item.dVersion) ? "" : item.dVersion,
                                dUpdatedDate = item.dUpdatedDate,
                                dUpdatedBy = item.dUpdatedBy,
                                dReason = string.IsNullOrEmpty(item.dReason) ? "" : item.dReason
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Edit(CamTemplateViewModel_Detail pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                if(pModel.Reason == null)
                {
                    rtn.Message = "Reason much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CAM_TEMP_DAL dal = new MT_CAM_TEMP_DAL();
                MT_CAM_TEMP ent = new MT_CAM_TEMP();

                DateTime now = DateTime.Now;

                try
                {
                    //var Code = ShareFn.GenerateCodeByDate("CPAI");
                    ent.COCT_ROW_ID = pModel.CamCode;
                    ent.COCT_VERSION = pModel.Version;
                    foreach (var item in pModel.JSON.Spiral)
                    {
                        item.Component = ShareFn.EncodeString(item.Component.ToString());
                    }
                    ent.COCT_JSON = new JavaScriptSerializer().Serialize(pModel.JSON);
                    ent.COCT_REASON = pModel.Reason;
                    ent.COCT_UPDATED_BY = pUser;
                    ent.COCT_UPDATED = now;

                    dal.Update(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }


            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public CamTemplateViewModel_Detail Get(string pCamCode)
        {
            CamTemplateViewModel_Detail model = new CamTemplateViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pCamCode) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        MT_CAM_TEMP query = context.MT_CAM_TEMP.Where(c => c.COCT_ROW_ID.ToUpper() == pCamCode.ToUpper()).FirstOrDefault();

                        if (query != null)
                        {
                            model.CamCode = query.COCT_ROW_ID;
                            model.Version = query.COCT_VERSION;                        
                            model.JSON = new JavaScriptSerializer().Deserialize<CamTemplateViewModel_JSON>(query.COCT_JSON);
                            foreach (var item in model.JSON.Spiral)
                            {
                                item.Component = ShareFn.DecodeString(item.Component);
                            }
                            model.Reason = query.COCT_REASON;
                        }

                    }               
                    
                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ReturnValue Delete(CamTemplateViewModel_Detail pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CAM_TEMP_DAL dal = new MT_CAM_TEMP_DAL();
                MT_CAM_TEMP ent = new MT_CAM_TEMP();

                DateTime now = DateTime.Now;

                try
                {
                    //var Code = ShareFn.GenerateCodeByDate("CPAI");
                    ent.COCT_ROW_ID = pModel.CamCode;
                    ent.COCT_VERSION = pModel.Version;
                    ent.COCT_JSON = new JavaScriptSerializer().Serialize(pModel.JSON);
                    ent.COCT_REASON = pModel.Reason;
                    ent.COCT_UPDATED_BY = pUser;
                    ent.COCT_UPDATED = now;
                    
                    dal.Delete(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }


        public bool IsUsed(string CamCode)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.COO_CAM
                        where c.COCA_FK_MT_CAM_TEMP == CamCode
                        select c).FirstOrDefault();

                    if (query == null)
                    {
                        return false;
                    } else
                    {
                        return true;
                    }
                    
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public List<CAMTemplate> getCAMTemplate(CamTemplateViewModel_Detail campTemp)
        {
            if (campTemp != null)
            {
                List<CAMTemplate> camTemplst = new List<CAMTemplate>();
                CamTemplateViewModel_Detail cam_detail = campTemp;
                if (cam_detail != null)
                {
                    if (cam_detail.JSON != null)
                    {
                        string spiral_header_col = cam_detail.JSON.spiral_header_column;
                        string spiral_data_col = cam_detail.JSON.spiral_data_column;
                        string compared_crude = cam_detail.JSON.compare_clude;
                        string test_method_col = cam_detail.JSON.test_method_column;
                        string source_col = cam_detail.JSON.source_column;
                        string constraint_col = cam_detail.JSON.constraint_column;
                        string remarks_col = cam_detail.JSON.remarks_column;
                        string component_header_column = cam_detail.JSON.component_header_column;

                        string g_border_style = cam_detail.JSON.g_border_style;
                        string g_font_color = cam_detail.JSON.g_font_color;
                        string g_font_size = cam_detail.JSON.g_font_size;
                        string g_freeze_row = cam_detail.JSON.g_freeze_row;
                        string g_freeze_col = cam_detail.JSON.g_freeze_col;
                        string g_format_number = cam_detail.JSON.g_number_format;

                        CAMTemplate camFreeze = new CAMTemplate();
                        camFreeze.freeze_row = cam_detail.JSON.g_freeze_row;
                        camFreeze.freeze_col = cam_detail.JSON.g_freeze_col;
                        camTemplst.Add(camFreeze);


                        foreach (var item in cam_detail.JSON.Spiral)
                        {
                            CAMTemplate camTemp = new CAMTemplate();
                            camTemp.border_style = g_border_style;
                            camTemp.font_color = g_font_color;
                            camTemp.font_size = g_font_size;
                            camTemp.number_format = g_format_number;
                            if (string.IsNullOrEmpty(item.Row_to))
                            {
                                camTemp.row = item.Row_from;
                            }
                            else
                            {
                                camTemp.row = item.Row_from + "|" + item.Row_to;
                                camTemp.merge = "Y";
                            }
                            camTemp.column = component_header_column;
                            camTemp.value = ShareFn.DecodeString(item.Component);
                            camTemplst.Add(camTemp);

                            if (item.ObjProperty != null && item.ObjProperty.Property.Count > 0)
                            {
                                foreach (var property in item.ObjProperty.Property)
                                {
                                    //SpecDetail specDetail;
                                    camTemp = new CAMTemplate(
                                        spiral_data_col,
                                        camTemp.bg_color,
                                        camTemp.alignment
                                        );
                                    if (string.IsNullOrEmpty(property.txtTo))
                                    {
                                        camTemp.row = property.txtFrom;
                                    }
                                    else
                                    {
                                        camTemp.row = property.txtFrom + "|" + property.txtTo;
                                    }

                                    if (!string.IsNullOrEmpty(property.txtUnit))
                                    {
                                        camTemp.column = spiral_header_col;
                                        camTemp.value = property.txtUnit;
                                        camTemplst.Add(camTemp);
                                    }
                                    else
                                    {
                                        camTemp.column = spiral_header_col;
                                        camTemp.value = property.txtProperty;
                                        camTemplst.Add(camTemp);
                                    }

                                    //camTemp = new CAMTemplate(
                                    //    camTemp.row,
                                    //    spiral_data_col,
                                    //    camTemp.bg_color,
                                    //    camTemp.alignment
                                    //    );



                                }
                            }
                        }

                        
                        foreach (var item in cam_detail.JSON.Data)
                        {
                            CAMTemplate camTemp = new CAMTemplate();
                            camTemp.border_style = g_border_style;
                            camTemp.font_color = g_font_color;
                            camTemp.font_size = g_font_size;
                            camTemp.number_format = g_format_number;
                            camTemp.row = item.row;
                            camTemp.column = spiral_header_col;
                            camTemp.alignment = "H";
                            camTemplst.Add(camTemp);
                        }
                        foreach (var item in cam_detail.JSON.Fix)
                        {
                            CAMTemplate camTemp = new CAMTemplate();
                            camTemp.border_style = g_border_style;
                            camTemp.font_color = g_font_color;
                            camTemp.font_size = g_font_size;
                            camTemp.number_format = g_format_number;
                            if (string.IsNullOrEmpty(item.row_to))
                            {
                                camTemp.row = item.row_from;
                            }
                            else
                            {
                                camTemp.row = item.row_from + "|" + item.row_to;
                            }

                            if (string.IsNullOrEmpty(item.col_to))
                            {
                                camTemp.column = item.col_from;
                            }
                            else
                            {
                                camTemp.column = item.col_from + "|" + item.col_to;
                            }
                            camTemp.value = item.value;
                            camTemplst.Add(camTemp);
                        }

                        if (cam_detail.JSON.Config != null)
                        {
                            foreach (var item in cam_detail.JSON.Config)
                            {
                                CAMTemplate camTemp = new CAMTemplate();
                                if (string.IsNullOrEmpty(item.row_to))
                                {
                                    camTemp.row = item.row_from;
                                }
                                else
                                {
                                    camTemp.row = item.row_from + "|" + item.row_to;
                                }

                                if (string.IsNullOrEmpty(item.col_to))
                                {
                                    camTemp.column = item.col_from;
                                }
                                else
                                {
                                    camTemp.column = item.col_from + "|" + item.col_to;
                                }
                                camTemp.bg_color = item.bg_color;
                                camTemp.alignment = item.alignment;
                                camTemp.hidden = item.hidden;
                                camTemp.merge = item.merge;
                                camTemp.font_bold = item.font_bold;
                                camTemp.font_color = item.font_color;
                                camTemp.font_size = item.font_size;
                                camTemp.number_format = item.number_format;
                                camTemp.indent = item.indent;
                                camTemp.center = item.center;

                                camTemp.border_style = item.border_style;
                                camTemp.border_color = item.border_color;

                                camTemp.font_italic = item.font_italic;
                                camTemp.font_underline = item.font_underline;
                                camTemp.font_textwrap = item.font_textwrap;
                                camTemp.font_horizontal_alignment = item.font_horizontal_alignment;
                                camTemp.font_vertical_alignment = item.font_vertical_alignment;
                                camTemp.border_top_style = item.border_top_style;
                                camTemp.border_top_color = item.border_top_color;
                                camTemp.border_bottom_style = item.border_bottom_style;
                                camTemp.border_bottom_color = item.border_bottom_color;
                                camTemp.border_left_style = item.border_left_style;
                                camTemp.border_left_color = item.border_left_color;
                                camTemp.border_right_style = item.border_right_style;
                                camTemp.border_right_color = item.border_right_color;

                                camTemplst.Add(camTemp);
                            }
                        }

                        if (cam_detail.JSON.ColumnWidth != null)
                        {
                            foreach (var item in cam_detail.JSON.ColumnWidth)
                            {
                                CAMTemplate camtemp = new CAMTemplate();
                                camtemp.column = item.column;
                                camtemp.width = item.width;
                                camTemplst.Add(camtemp);
                            }
                        }
                        return camTemplst;
                    }
                }
            }
            return null;
        }

    }
}