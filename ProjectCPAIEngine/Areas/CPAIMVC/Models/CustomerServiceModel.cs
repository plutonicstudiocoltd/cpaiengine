﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class CustomerServiceModel
    {
        public ReturnValue Add(ref CustomerViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CustName))
                {
                    rtn.Message = "Customer Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Company))
                {
                    rtn.Message = "Company should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Nation))
                {
                    rtn.Message = "Nation should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CountryKey))
                {
                    rtn.Message = "Country Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Color));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }

                }

                if (pModel.Broker != null && pModel.Broker.Any())
                {
                    var countDefault = pModel.Broker.Where(x => x.Default.Equals("Y")).Count();
                    if (countDefault != 1)
                    {
                        rtn.Message = "Broker default should be select one";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Broker.GroupBy(x => x.Vendor.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "Broker name is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CUST_DAL dal = new MT_CUST_DAL();
                MT_CUST ent = new MT_CUST();

                MT_CUST_DETAIL_DAL dalDetail = new MT_CUST_DETAIL_DAL();
                MT_CUST_DETAIL entDetail = new MT_CUST_DETAIL();

                MT_CUST_CONTROL_DAL dalCtr = new MT_CUST_CONTROL_DAL();
                MT_CUST_CONTROL entCtr = new MT_CUST_CONTROL();

                MT_CUST_PAYMENT_TERM_DAL dalPayment = new MT_CUST_PAYMENT_TERM_DAL();
                MT_CUST_PAYMENT_TERM entPayment = new MT_CUST_PAYMENT_TERM();

                MT_CUST_BROKER_DAL dalBroker = new MT_CUST_BROKER_DAL();
                MT_CUST_BROKER entBroker = new MT_CUST_BROKER();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var Code = ShareFn.GenerateCodeByDate("CPAI");

                            ent.MCT_CUST_NUM = Code;
                            ent.MCT_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;  // CPAI
                            ent.MCT_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MCT_CREATED_BY = pUser;
                            ent.MCT_CREATED_DATE = now;
                            ent.MCT_UPDATED_BY = pUser;
                            ent.MCT_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            var CtrlID = ConstantPrm.GetDynamicCtrlID();
                            // Detail
                            entDetail.MCD_ROW_ID = CtrlID;
                            entDetail.MCD_FK_CUS = Code;
                            entDetail.MCD_NAME_1 = pModel.CustName.Trim();
                            entDetail.MCD_COUNT_KEY = pModel.CountryKey;
                            entDetail.MCD_FK_COMPANY = pModel.Company;
                            entDetail.MCD_NATION = pModel.Nation;
                            entDetail.MCD_STATUS = CPAIConstantUtil.ACTIVE;
                            entDetail.MCD_CREATED_BY = pUser;
                            entDetail.MCD_CREATED_DATE = now;
                            entDetail.MCD_UPDATED_BY = pUser;
                            entDetail.MCD_UPDATED_DATE = now;

                            dalDetail.Save(entDetail, context);

                            // Control
                            foreach (var item in pModel.Control)
                            {
                                entCtr = new MT_CUST_CONTROL();
                                entCtr.MCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MCR_FK_CUST_CONTROL_ID = CtrlID;
                                entCtr.MCR_SYSTEM = item.System.Trim().ToUpper();
                                entCtr.MCR_TYPE = item.Type.Trim().ToUpper();
                                entCtr.MCR_COLOR_CODE = item.Color;
                                entCtr.MCR_STATUS = item.Status;
                                entCtr.MCR_CREATED_BY = pUser;
                                entCtr.MCR_CREATED_DATE = now;
                                entCtr.MCR_UPDATED_BY = pUser;
                                entCtr.MCR_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            //Payment Term
                            entPayment.MCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                            entPayment.MCP_FK_MT_CUST = Code;
                            entPayment.MCP_PAYMENT_TERM = pModel.PaymentTerm;
                            entPayment.MCP_ADDRESS_COMMISSION = pModel.AddressCommission;
                            entPayment.MCP_BROKER_NAME = pModel.BrokerName;
                            entPayment.MCP_BROKER_COMMISSION = pModel.BrokerCommission;
                            entPayment.MCP_WITHOLDING_TAX = pModel.WithodingTax;
                            entPayment.MCP_CREATED_BY = pUser;
                            entPayment.MCP_CREATED_DATE = now;
                            entPayment.MCP_UPDATED_BY = pUser;
                            entPayment.MCP_UPDATED_DATE = now;

                            dalPayment.Save(entPayment, context);


                            // Broker
                            if (pModel.Broker != null && pModel.Broker.Any())
                            {
                                int brokerOrder = 1;
                                foreach (var item in pModel.Broker)
                                {
                                    entBroker = new MT_CUST_BROKER();
                                    entBroker.MCB_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entBroker.MCB_FK_MT_CUST = Code;
                                    entBroker.MCB_FK_MT_VENDOR_BROKER = item.Vendor.Trim().ToUpper();
                                    entBroker.MCB_DEFAULT = item.Default.Trim().ToUpper();
                                    entBroker.MCB_ORDER = brokerOrder + "";
                                    entBroker.MCB_STATUS = item.Status;
                                    entBroker.MCB_CREATED_BY = pUser;
                                    entBroker.MCB_CREATED_DATE = now;
                                    entBroker.MCB_UPDATED_BY = pUser;
                                    entBroker.MCB_UPDATED_DATE = now;
                                    dalBroker.Save(entBroker, context);
                                    brokerOrder++;
                                }
                            }


                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;

                            pModel.CustCode = Code;
                            pModel.CustDetailID = CtrlID;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(CustomerViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CustCode))
                {
                    rtn.Message = "Customer Code should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CreateType))
                {
                    rtn.Message = "Create Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CustDetailID))
                {
                    rtn.Message = "Customer Detail ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CustName) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Customer Name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Company) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Company should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.Nation) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Nation should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.CountryKey) && pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                {
                    rtn.Message = "Country Key should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (pModel.Control == null)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count == 0)
                {
                    rtn.Message = "Please add system, type and color";
                    rtn.Status = false;
                    return rtn;
                }
                else if (pModel.Control.Count >= 0)
                {
                    var systemEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var typeEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Type));
                    if (typeEmpty == true)
                    {
                        rtn.Message = "Type should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var colorEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Color));
                    if (colorEmpty == true)
                    {
                        rtn.Message = "Color should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = pModel.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = pModel.Control.GroupBy(x => x.System.Trim().ToUpper() + x.Type.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System and Type is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }

                }

                if (pModel.Broker != null && pModel.Broker.Any())
                {
                    if (pModel.Broker.Count >= 0)
                    {
                        var countDefault = pModel.Broker.Where(x => x.Default.Equals("Y")).Count();
                        if (countDefault != 1)
                        {
                            rtn.Message = "Broker default should be select one";
                            rtn.Status = false;
                            return rtn;
                        }

                        var checkDuplicate = pModel.Broker.GroupBy(x => x.Vendor.Trim().ToUpper()).Any(g => g.Count() > 1);
                        if (checkDuplicate == true)
                        {
                            rtn.Message = "Broker name is duplicate";
                            rtn.Status = false;
                            return rtn;
                        }
                    }
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_CUST_DAL dal = new MT_CUST_DAL();
                MT_CUST ent = new MT_CUST();

                MT_CUST_DETAIL_DAL dalDetail = new MT_CUST_DETAIL_DAL();
                MT_CUST_DETAIL entDetail = new MT_CUST_DETAIL();

                MT_CUST_CONTROL_DAL dalCtr = new MT_CUST_CONTROL_DAL();
                MT_CUST_CONTROL entCtr = new MT_CUST_CONTROL();

                MT_CUST_PAYMENT_TERM_DAL dalPayment = new MT_CUST_PAYMENT_TERM_DAL();
                MT_CUST_PAYMENT_TERM entPayment = new MT_CUST_PAYMENT_TERM();

                MT_CUST_BROKER_DAL dalBroker = new MT_CUST_BROKER_DAL();
                MT_CUST_BROKER entBroker = new MT_CUST_BROKER();

                DateTime now = DateTime.Now;


                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            // Update data in MT_CUST
                            ent.MCT_CUST_NUM = pModel.CustCode;
                            ent.MCT_CREATE_TYPE = pModel.CreateType;
                            ent.MCT_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MCT_UPDATED_BY = pUser;
                            ent.MCT_UPDATED_DATE = now;

                            dal.Update(ent, context);

                            // Update Detail
                            if (pModel.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                            {
                                // Detail
                                entDetail.MCD_ROW_ID = pModel.CustDetailID;
                                entDetail.MCD_FK_CUS = pModel.CustCode;
                                entDetail.MCD_NAME_1 = pModel.CustName.Trim();
                                entDetail.MCD_FK_COMPANY = pModel.Company;
                                entDetail.MCD_NATION = pModel.Nation;
                                entDetail.MCD_COUNT_KEY = pModel.CountryKey;
                                entDetail.MCD_STATUS = CPAIConstantUtil.ACTIVE;
                                entDetail.MCD_UPDATED_BY = pUser;
                                entDetail.MCD_UPDATED_DATE = now;

                                dalDetail.Update(entDetail, context);
                            }


                            // Insert data to MT_VENDOR_CONTROL
                            foreach (var item in pModel.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_CUST_CONTROL();
                                    entCtr.MCR_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MCR_FK_CUST_CONTROL_ID = pModel.CustDetailID;
                                    entCtr.MCR_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MCR_TYPE = string.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MCR_COLOR_CODE = item.Color;
                                    entCtr.MCR_STATUS = item.Status;
                                    entCtr.MCR_CREATED_BY = pUser;
                                    entCtr.MCR_CREATED_DATE = now;
                                    entCtr.MCR_UPDATED_BY = pUser;
                                    entCtr.MCR_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_CUST_CONTROL();
                                    entCtr.MCR_ROW_ID = item.RowID;
                                    entCtr.MCR_FK_CUST_CONTROL_ID = pModel.CustDetailID;
                                    entCtr.MCR_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MCR_TYPE = string.IsNullOrEmpty(item.Type) ? "" : item.Type.Trim().ToUpper();
                                    entCtr.MCR_COLOR_CODE = item.Color;
                                    entCtr.MCR_STATUS = item.Status;
                                    entCtr.MCR_UPDATED_BY = pUser;
                                    entCtr.MCR_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }
                            }

                            //Payment Term

                            if (pModel.CustPaymentTermID != null)
                            {
                                entPayment.MCP_ROW_ID = pModel.CustPaymentTermID;
                            }
                            else
                            {
                                entPayment.MCP_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entPayment.MCP_CREATED_BY = pUser;
                                entPayment.MCP_CREATED_DATE = now;
                            }
                            entPayment.MCP_FK_MT_CUST = pModel.CustCode;
                            entPayment.MCP_PAYMENT_TERM = pModel.PaymentTerm;
                            entPayment.MCP_ADDRESS_COMMISSION = pModel.AddressCommission;
                            entPayment.MCP_BROKER_NAME = pModel.BrokerName;
                            entPayment.MCP_BROKER_COMMISSION = pModel.BrokerCommission;
                            entPayment.MCP_WITHOLDING_TAX = pModel.WithodingTax;
                            entPayment.MCP_UPDATED_BY = pUser;
                            entPayment.MCP_UPDATED_DATE = now;

                            dalPayment.Update(entPayment, context);


                            // Broker
                            dalBroker.DeleteByCode(pModel.CustCode, context);
                            if (pModel.Broker != null && pModel.Broker.Any())
                            {
                                int brokerOrder = 1;
                                foreach (var item in pModel.Broker)
                                {
                                    entBroker = new MT_CUST_BROKER();
                                    entBroker.MCB_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entBroker.MCB_FK_MT_CUST = pModel.CustCode;
                                    entBroker.MCB_FK_MT_VENDOR_BROKER = item.Vendor.Trim().ToUpper();
                                    entBroker.MCB_DEFAULT = item.Default.Trim().ToUpper();
                                    entBroker.MCB_ORDER = brokerOrder + "";
                                    entBroker.MCB_STATUS = item.Status;
                                    entBroker.MCB_CREATED_BY = pUser;
                                    entBroker.MCB_CREATED_DATE = now;
                                    entBroker.MCB_UPDATED_BY = pUser;
                                    entBroker.MCB_UPDATED_DATE = now;

                                    dalBroker.Save(entBroker, context);
                                    brokerOrder++;
                                }
                            }


                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref CustomerViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sCode = pModel.sCustCode == null ? "" : pModel.sCustCode.ToUpper();
                    var sName = pModel.sCustName == null ? "" : pModel.sCustName.ToLower();
                    var sCompany = pModel.sCompany == null ? "" : pModel.sCompany.ToUpper();
                    var sNation = pModel.sNation == null ? "" : pModel.sNation.ToUpper();
                    var sCountry = pModel.sCountry == null ? "" : pModel.sCountry.ToUpper();
                    var sSystem = pModel.sSystem == null ? "" : pModel.sSystem.ToUpper();
                    var sType = pModel.sType == null ? "" : pModel.sType.ToUpper();
                    var sCreateType = pModel.sCreateType == null ? "" : pModel.sCreateType.ToUpper();
                    var sStatus = pModel.sStatus == null ? "" : pModel.sStatus.ToUpper();


                    var query = (from v in context.MT_CUST
                                 join vd in context.MT_CUST_DETAIL on v.MCT_CUST_NUM equals vd.MCD_FK_CUS into viewD
                                 from vdl in viewD.DefaultIfEmpty()
                                 join c in context.MT_COMPANY on vdl.MCD_FK_COMPANY equals c.MCO_COMPANY_CODE into viewCom
                                 from vdcl in viewCom.DefaultIfEmpty()
                                 join vc in context.MT_CUST_CONTROL on vdl.MCD_ROW_ID equals vc.MCR_FK_CUST_CONTROL_ID into viewCust
                                 from vcl in viewCust.DefaultIfEmpty()
                                 select new CustomerViewModel_SeachData
                                 {
                                     dCustCtrlID = vdl.MCD_ROW_ID,
                                     dCustCode = v.MCT_CUST_NUM,
                                     dCreateType = string.IsNullOrEmpty(v.MCT_CREATE_TYPE) ? "SAP" : v.MCT_CREATE_TYPE,
                                     dCustName = vdl.MCD_NAME_1,
                                     dCompany = vdcl.MCO_COMPANY_CODE,
                                     dCompanyName = vdcl.MCO_SHORT_NAME,
                                     dNation = vdl.MCD_NATION,
                                     dCountry = vdl.MCD_COUNT_KEY,
                                     dSystem = vcl.MCR_SYSTEM,
                                     dType = vcl.MCR_TYPE,
                                     dStatus = vcl.MCR_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sCode))
                        query = query.Where(p => p.dCustCode.ToUpper().Contains(sCode));

                    if (!string.IsNullOrEmpty(sName))
                        query = query.Where(p => p.dCustName.ToLower().Contains(sName));

                    if (!string.IsNullOrEmpty(sCountry))
                        query = query.Where(p => p.dCountry.ToUpper().Equals(sCountry));

                    if (!string.IsNullOrEmpty(sType))
                        query = query.Where(p => p.dType.ToUpper().Equals(sType));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Equals(sSystem));

                    if (!string.IsNullOrEmpty(sCompany))
                        query = query.Where(p => p.dCompany.ToUpper().Equals(sCompany));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Equals(sCreateType));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));


                    if (query != null)
                    {

                        pModel.sSearchData = query.ToList();
                        pModel.sSearchData = new List<CustomerViewModel_SeachData>();
                        foreach (var item in query)
                        {
                            pModel.sSearchData.Add(new CustomerViewModel_SeachData
                            {
                                dCustCtrlID = string.IsNullOrEmpty(item.dCustCtrlID) ? "" : item.dCustCtrlID,
                                dCustCode = string.IsNullOrEmpty(item.dCustCode) ? "" : item.dCustCode,
                                dCreateType = string.IsNullOrEmpty(item.dCreateType) ? "" : item.dCreateType,
                                dCustName = string.IsNullOrEmpty(item.dCustName) ? "" : item.dCustName,
                                dCompany = string.IsNullOrEmpty(item.dCompanyName) ? "" : item.dCompanyName,
                                dNation = string.IsNullOrEmpty(item.dNation) ? "" : item.dNation,
                                dCountry = string.IsNullOrEmpty(item.dCountry) ? "" : item.dCountry,
                                dSystem = string.IsNullOrEmpty(item.dSystem) ? "" : item.dSystem,
                                dType = string.IsNullOrEmpty(item.dType) ? "" : item.dType,
                                dStatus = string.IsNullOrEmpty(item.dStatus) ? "" : item.dStatus,
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public CustomerViewModel_Detail Get(string pCustCtrlID)
        {
            CustomerViewModel_Detail model = new CustomerViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pCustCtrlID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_CUST_DETAIL
                                     where v.MCD_ROW_ID.Equals(pCustCtrlID)
                                     join vd in context.MT_CUST on v.MCD_FK_CUS equals vd.MCT_CUST_NUM into viewD
                                     from vdl in viewD.DefaultIfEmpty()
                                     join c in context.MT_COMPANY on v.MCD_FK_COMPANY equals c.MCO_COMPANY_CODE into viewCom
                                     from vdcl in viewCom.DefaultIfEmpty()
                                     join vc in context.MT_CUST_CONTROL on v.MCD_ROW_ID equals vc.MCR_FK_CUST_CONTROL_ID into viewCust
                                     from vcl in viewCust.DefaultIfEmpty()
                                     join vp in context.MT_CUST_PAYMENT_TERM on v.MCD_FK_CUS equals vp.MCP_FK_MT_CUST into viewPayment
                                     from vpl in viewPayment.DefaultIfEmpty()
                                     orderby new { vcl.MCR_SYSTEM, vcl.MCR_TYPE }
                                     select new
                                     {
                                         dCustCtrlID = v.MCD_ROW_ID
                                         ,
                                         dCustCode = vdl.MCT_CUST_NUM
                                         ,
                                         dCreateType = string.IsNullOrEmpty(vdl.MCT_CREATE_TYPE) ? "SAP" : vdl.MCT_CREATE_TYPE
                                         ,
                                         dCustName = v.MCD_NAME_1
                                         ,
                                         dCompanyCode = vdcl.MCO_COMPANY_CODE
                                         ,
                                         dCompany = vdcl.MCO_SHORT_NAME
                                         ,
                                         dNation = v.MCD_NATION
                                         ,
                                         dCountry = v.MCD_COUNT_KEY
                                         ,
                                         dRowID = vcl.MCR_ROW_ID
                                         ,
                                         dSystem = vcl.MCR_SYSTEM
                                         ,
                                         dType = vcl.MCR_TYPE
                                         ,
                                         dColor = vcl.MCR_COLOR_CODE
                                         ,
                                         dStatus = vcl.MCR_STATUS
                                         ,
                                         dCustPaymentTermID = vpl.MCP_ROW_ID
                                         ,
                                         dPaymentTerm = vpl.MCP_PAYMENT_TERM
                                         ,
                                         dAddressCommission = vpl.MCP_ADDRESS_COMMISSION
                                         ,
                                         dBrokerName = vpl.MCP_BROKER_NAME
                                         ,
                                         dBrokerCommission = vpl.MCP_BROKER_COMMISSION
                                         ,
                                         dWithodingTax = vpl.MCP_WITHOLDING_TAX
                                     });

                        if (query != null)
                        {
                            model.Control = new List<CustomerViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                if (i == 1)
                                {
                                    model.CustDetailID = g.dCustCtrlID;
                                    model.CustCode = g.dCustCode;
                                    model.CustName = g.dCustName;
                                    model.Company = g.dCompanyCode;
                                    model.Nation = g.dNation;
                                    model.CountryKey = g.dCountry;
                                    model.CreateType = g.dCreateType;
                                    model.CustPaymentTermID = g.dCustPaymentTermID;
                                    model.PaymentTerm = g.dPaymentTerm;
                                    model.AddressCommission = g.dAddressCommission;
                                    model.BrokerName = g.dBrokerName;
                                    model.BrokerCommission = g.dBrokerCommission;
                                    model.WithodingTax = g.dWithodingTax;
                                    model.Broker = new List<CustomerViewModel_Broker>();

                                    // Add Broker List
                                    var queryBroker = (from vb in context.MT_CUST_BROKER
                                                       where vb.MCB_FK_MT_CUST.Equals(g.dCustCode)
                                                       orderby new { vb.MCB_ORDER }
                                                       select vb);
                                    if (queryBroker != null)
                                    {
                                        foreach (var gb in queryBroker)
                                        {
                                            model.Broker.Add(new CustomerViewModel_Broker() { RowID = gb.MCB_ROW_ID, Order = gb.MCB_ORDER, Vendor = gb.MCB_FK_MT_VENDOR_BROKER, Default = gb.MCB_DEFAULT, Status = gb.MCB_STATUS });
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(g.dRowID))
                                {
                                    model.Control.Add(new CustomerViewModel_Control { RowID = g.dRowID, Order = i.ToString(), System = g.dSystem, Type = g.dType, Color = g.dColor, Status = g.dStatus });
                                }
                                i++;
                            }
                        }
                    }

                    if (model.Control.Count < 1)
                    {
                        model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "", System = "", Type = "", Color = CPAIConstantUtil.COLOR_DEFUALT, Status = CPAIConstantUtil.ACTIVE });
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetName(string pCustID, string pNation = "I")
        {

            try
            {
                if (String.IsNullOrEmpty(pCustID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = (from v in context.MT_CUST_DETAIL
                                     where v.MCD_ROW_ID.Equals(pCustID)
                                     && v.MCD_NATION.Equals(pNation)
                                     select v.MCD_NAME_1).FirstOrDefault();

                        return query == null ? "" : query;

                    }

                }
                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string GetSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_CUST_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCR_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCR_SYSTEM }
                                    select new { System = d.MCR_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetSystemForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_CUST_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCR_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCR_SYSTEM }
                                    select new { System = d.MCR_SYSTEM.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetTypeJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_CUST_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCR_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCR_TYPE }
                                    select new { Type = d.MCR_TYPE.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Type));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetTypeForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_CUST_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MCR_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MCR_TYPE }
                                    select new { Type = d.MCR_TYPE.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.Type))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.Type, Text = item.Type });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getCustDDLJson(string System, string Type)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from v in context.MT_CUST_DETAIL
                             join vd in context.MT_CUST on v.MCD_FK_CUS equals vd.MCT_CUST_NUM into viewD
                             from vdl in viewD.DefaultIfEmpty()
                             join c in context.MT_COMPANY on v.MCD_FK_COMPANY equals c.MCO_COMPANY_CODE into viewCom
                             from vdcl in viewCom.DefaultIfEmpty()
                             join vc in context.MT_CUST_CONTROL on v.MCD_ROW_ID equals vc.MCR_FK_CUST_CONTROL_ID into viewCust
                             from vcl in viewCust.DefaultIfEmpty()
                             join vp in context.MT_CUST_PAYMENT_TERM on v.MCD_FK_CUS equals vp.MCP_FK_MT_CUST into viewPayment
                             from vpl in viewPayment.DefaultIfEmpty()
                             where vcl.MCR_SYSTEM.ToUpper() == System.ToUpper()
                             && vcl.MCR_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                             select new
                             {
                                 dCustCtrlID = v.MCD_ROW_ID
                                 ,
                                 dCustCode = vdl.MCT_CUST_NUM
                                 ,
                                 dCreateType = vdl.MCT_CREATE_TYPE
                                 ,
                                 dCustName = v.MCD_NAME_1
                                 ,
                                 dCompanyCode = vdcl.MCO_COMPANY_CODE
                                 ,
                                 dCompany = vdcl.MCO_SHORT_NAME
                                 ,
                                 dNation = v.MCD_NATION
                                 ,
                                 dCountry = v.MCD_COUNT_KEY
                                 ,
                                 dRowID = vcl.MCR_ROW_ID
                                 ,
                                 dSystem = vcl.MCR_SYSTEM
                                 ,
                                 dType = vcl.MCR_TYPE
                                 ,
                                 dColor = vcl.MCR_COLOR_CODE
                                 ,
                                 dStatus = vcl.MCR_STATUS
                                 ,
                                 dCustPaymentTermID = vpl.MCP_ROW_ID
                                 ,
                                 dPaymentTerm = vpl.MCP_PAYMENT_TERM
                                 ,
                                 dAddressCommission = vpl.MCP_ADDRESS_COMMISSION
                                 ,
                                 dBrokerName = vpl.MCP_BROKER_NAME
                                 ,
                                 dBrokerCommission = vpl.MCP_BROKER_COMMISSION
                             });

                if (!string.IsNullOrEmpty(Type))
                {
                    string[] aryType = Type.ToUpper().Split('|');
                    query = query.Where(x => aryType.Contains(x.dType.ToUpper().Trim()));
                }


                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.dCustCode, text = p.dCustName }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }
    }
}