﻿using System;
using System.Linq;
using ProjectCPAIEngine.DAL.Entity;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.Collections.Generic;
using System.Collections;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALFreight;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class EstimateFreightServiceModel
    {
        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string getTransactionByID(string id)
        {
            FclRootObject rootObj = new FclRootObject();
            rootObj.fcl_vessel_particular = new FclVesselParticular();
            rootObj.fcl_payment_terms = new FclPaymentTerms();
            rootObj.fcl_time_for_operation = new FclTimeForOperation();
            rootObj.fcl_vessel_speed = new FclVesselSpeed();
            rootObj.fcl_latest_bunker_price = new FclLatestBunkerPrice();
            rootObj.fcl_bunker_consumption = new FclBunkerConsumption();
            rootObj.fcl_bunker_cost = new List<FclBunkerCost>();
            rootObj.fcl_other_cost = new FclOtherCost();
            rootObj.fcl_results = new FclResults();
            rootObj.fcl_payment_terms.Cargoes = new List<FclCargo>();

            FREIGHT_DATA_DAL dataDal = new FREIGHT_DATA_DAL();
            CPAI_FREIGHT_DATA data = dataDal.GetFCLDATAByID(id);
            if (data != null)
            {
                rootObj.fcl_vessel_particular.date_time = (data.FDA_DATE_FILL_IN == DateTime.MinValue || data.FDA_DATE_FILL_IN == null) ? "" : Convert.ToDateTime(data.FDA_DATE_FILL_IN).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.fcl_vessel_particular.vessel_name = data.FDA_FK_VESSEL;
                rootObj.fcl_vessel_particular.type = data.FDA_TYPE;
                rootObj.fcl_vessel_particular.built = data.FDA_BUILT;
                rootObj.fcl_vessel_particular.age = data.FDA_AGE;
                rootObj.fcl_vessel_particular.dwt = data.FDA_DWT;
                rootObj.fcl_vessel_particular.displacement = data.FDA_DISPLACEMENT;
                rootObj.fcl_vessel_particular.capacity = data.FDA_CAPACITY;
                rootObj.fcl_vessel_particular.max_draft = data.FDA_MAX_DRAFT;
                rootObj.fcl_vessel_particular.coating = data.FDA_COATING;

                rootObj.fcl_payment_terms.charterer = data.FDA_FK_CHARTERER;
                //rootObj.fcl_payment_terms.unit = data.FDA_UNIT;
                //rootObj.fcl_payment_terms.cargo = data.FDA_FK_CARGO;
                //rootObj.fcl_payment_terms.quantity = data.FDA_QUANTITY;
                rootObj.fcl_payment_terms.laycan_from = (data.FDA_LAYCAN_FROM == DateTime.MinValue || data.FDA_LAYCAN_FROM == null) ? "" : Convert.ToDateTime(data.FDA_LAYCAN_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.fcl_payment_terms.laycan_to = (data.FDA_LAYCAN_TO == DateTime.MinValue || data.FDA_LAYCAN_TO == null) ? "" : Convert.ToDateTime(data.FDA_LAYCAN_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.fcl_payment_terms.laytime = data.FDA_LAYTIME;
                rootObj.fcl_payment_terms.bss = data.FDA_BSS;
                rootObj.fcl_payment_terms.dem = data.FDA_DEM;
                rootObj.fcl_payment_terms.dem_unit = data.FDA_UNIT;
                rootObj.fcl_payment_terms.payment_term = data.FDA_PAYMENT_TERM;
                rootObj.fcl_payment_terms.transit_loss = data.FDA_TRANSIT_LOSS;

                //rootObj.fcl_payment_terms.broker = data.FDA_FK_BROKER;
                rootObj.fcl_payment_terms.broker = data.FDA_BROKER_NAME;
                rootObj.fcl_payment_terms.broker_comm = data.FDA_BROKER_COMM;
                rootObj.fcl_payment_terms.add_comm = data.FDA_ADD_COMM;
                rootObj.fcl_payment_terms.total_comm = data.FDA_TOTAL_COMM;
                rootObj.fcl_payment_terms.withold_tax = data.FDA_WITHOLD_TAX;
                rootObj.fcl_payment_terms.load_port = data.FDA_LOAD_PORT;
                rootObj.fcl_payment_terms.discharge_port = data.FDA_DISCHARGE_PORT;

                rootObj.fcl_time_for_operation.loading_port = data.FDA_TSP_LOAD_PORT;
                rootObj.fcl_time_for_operation.discharge_port = data.FDA_TSP_DIS_PORT;
                rootObj.fcl_time_for_operation.distance_ballast = data.DISTANCE_BALLAST;
                rootObj.fcl_time_for_operation.ballast = data.FDA_TSM_BALLAST;
                rootObj.fcl_time_for_operation.distance_laden = data.DISTANCE_LADEN;
                rootObj.fcl_time_for_operation.laden = data.FDA_TSM_LADEN;
                rootObj.fcl_time_for_operation.tank_cleaning = data.FDA_TANK_CLEANING;
                rootObj.fcl_time_for_operation.total_duration = data.FDA_TOTAL_DURATION;

                rootObj.fcl_vessel_speed.fcl_eco = new FclEco();
                rootObj.fcl_vessel_speed.fcl_full = new FclFull();
                if (data.FDA_VESSEL_SPEED_FLAG == "E")
                {
                    rootObj.fcl_vessel_speed.fcl_eco.flag = "Y";
                    rootObj.fcl_vessel_speed.fcl_full.flag = "";
                } else
                {
                    rootObj.fcl_vessel_speed.fcl_eco.flag = "";
                    rootObj.fcl_vessel_speed.fcl_full.flag = "Y";
                }
                rootObj.fcl_vessel_speed.fcl_eco.ballast = data.FDA_ECO_BALLAST;
                rootObj.fcl_vessel_speed.fcl_eco.laden = data.FDA_ECO_LADEN;
                rootObj.fcl_vessel_speed.fcl_full.ballast = data.FDA_FULL_BALLAST;
                rootObj.fcl_vessel_speed.fcl_full.laden = data.FDA_FULL_LADEN;

                rootObj.fcl_exchange_rate = data.FDA_EXCHANGE_RATE;
                rootObj.fcl_exchange_date = data.FDA_EXCHANGE_DATE == null ? "" : Convert.ToDateTime(data.FDA_EXCHANGE_DATE).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                rootObj.fcl_latest_bunker_price.fcl_grade_fo = new FclGradeFo();
                rootObj.fcl_latest_bunker_price.fcl_grade_mgo = new FclGradeMgo();
                rootObj.fcl_latest_bunker_price.fcl_grade_fo.date = (data.FDA_GRADE_FO_DATE_FROM == DateTime.MinValue || data.FDA_GRADE_FO_DATE_FROM == null) ? "" : Convert.ToDateTime(data.FDA_GRADE_FO_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.fcl_latest_bunker_price.fcl_grade_fo.place = data.FDA_GRADE_FO_PLACE;
                rootObj.fcl_latest_bunker_price.fcl_grade_fo.price = data.FDA_GRADE_FO_PRICE;
                rootObj.fcl_latest_bunker_price.fcl_grade_fo.bunker_id = data.FDA_GRADE_FO_BNK_ID;
                rootObj.fcl_latest_bunker_price.fcl_grade_mgo.date = (data.FDA_GRADE_MGO_DATE_FROM == DateTime.MinValue || data.FDA_GRADE_MGO_DATE_FROM == null) ? "" : Convert.ToDateTime(data.FDA_GRADE_MGO_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.fcl_latest_bunker_price.fcl_grade_mgo.place = data.FDA_GRADE_MGO_PLACE;
                rootObj.fcl_latest_bunker_price.fcl_grade_mgo.price = data.FDA_GRADE_MGO_PRICE;
                rootObj.fcl_latest_bunker_price.fcl_grade_mgo.bunker_id = data.FDA_GRADE_MGO_BNK_ID;

                rootObj.fcl_payment_terms.ControlPort = new List<Port_Control>();
                rootObj.fcl_payment_terms.ControlDischarge = new List<Discharge_Control>();
                FREIGHT_PORT_DAL portDal = new FREIGHT_PORT_DAL();
                List<MT_JETTY> jettylst = PortDAL.GetPortData();
                List<CPAI_FREIGHT_PORT> portlst = portDal.GetFDPDATAByFCLID(id);
                for (int i = 0; i < portlst.Count; i++)
                {
                    var temp = jettylst.Where(p => p.MTJ_ROW_ID.Trim().ToUpper() == portlst[i].FDP_FK_PORT).ToList();
                    if (portlst[i].FDP_PORT_TYPE.ToUpper() == "L")
                    {
                        Port_Control port = new Port_Control();
                        port.JettyPortKey = portlst[i].FDP_FK_PORT;
                        port.PortOrder = portlst[i].FDP_ORDER_PORT;
                        //port.PortType = portlst[i].FDP_PORT_TYPE;
                        port.PortOtherDesc = portlst[i].FDP_OTHER_DESC == null ? "" : portlst[i].FDP_OTHER_DESC;
                        port.PortValue = portlst[i].FDP_VALUE;
                        if (temp != null && temp.Count > 0)
                        {
                            port.PortFullName = temp[0].MTJ_JETTY_NAME + ", " + temp[0].MTJ_LOCATION + ", " + temp[0].MTJ_MT_COUNTRY;
                            port.PortName = temp[0].MTJ_JETTY_NAME;
                            port.PortType = temp[0].MTJ_PORT_TYPE;
                            port.PortCountry = temp[0].MTJ_MT_COUNTRY;
                            port.PortLocation = temp[0].MTJ_LOCATION;
                        }
                        rootObj.fcl_payment_terms.ControlPort.Add(port);
                    } else
                    {
                        Discharge_Control port = new Discharge_Control();
                        port.JettyPortKey = portlst[i].FDP_FK_PORT;
                        port.PortOrder = portlst[i].FDP_ORDER_PORT;
                        //port.PortType = portlst[i].FDP_PORT_TYPE;
                        port.PortOtherDesc = portlst[i].FDP_OTHER_DESC == null ? "" : portlst[i].FDP_OTHER_DESC;
                        port.DischargeValue = portlst[i].FDP_VALUE;
                        if (temp != null && temp.Count > 0)
                        {
                            port.DischargeFullName = temp[0].MTJ_JETTY_NAME + ", " + temp[0].MTJ_LOCATION + ", " + temp[0].MTJ_MT_COUNTRY;
                            port.DischargeName = temp[0].MTJ_JETTY_NAME;
                            port.PortType = temp[0].MTJ_PORT_TYPE;
                            port.PortCountry = temp[0].MTJ_MT_COUNTRY;
                            port.PortLocation = temp[0].MTJ_LOCATION;
                        }
                        rootObj.fcl_payment_terms.ControlDischarge.Add(port);
                    }
                }

                rootObj.total_port_charge = data.FDA_TOTAL_PORT_CHARGE;

                rootObj.fcl_bunker_consumption.fcl_load.fo = data.FDA_FO_LOAD;
                rootObj.fcl_bunker_consumption.fcl_load.mgo = data.FDA_MGO_LOAD;
                rootObj.fcl_bunker_consumption.fcl_discharge.fo = data.FDA_FO_DISCHARGE;
                rootObj.fcl_bunker_consumption.fcl_discharge.mgo = data.FDA_MGO_DISCHARGE;
                rootObj.fcl_bunker_consumption.fcl_steaming_ballast.fo = data.FDA_FO_STEAMING_BALLAST;
                rootObj.fcl_bunker_consumption.fcl_steaming_ballast.mgo = data.FDA_MGO_STEAMING_BALLAST;
                rootObj.fcl_bunker_consumption.fcl_steaming_laden.fo = data.FDA_FO_STEAMING_LADEN;
                rootObj.fcl_bunker_consumption.fcl_steaming_laden.mgo = data.FDA_MGO_STEAMING_LADEN;
                rootObj.fcl_bunker_consumption.fcl_idle.fo = data.FDA_FO_IDLE;
                rootObj.fcl_bunker_consumption.fcl_idle.mgo = data.FDA_MGO_IDLE;
                rootObj.fcl_bunker_consumption.fcl_tack_cleaning.fo = data.FDA_FO_TANK_CLEANING;
                rootObj.fcl_bunker_consumption.fcl_tack_cleaning.mgo = data.FDA_MGO_TANK_CLEANING;
                rootObj.fcl_bunker_consumption.fcl_heating.fo = data.FDA_FO_HEATING;
                rootObj.fcl_bunker_consumption.fcl_heating.mgo = data.FDA_MGO_HEATING;

                FREIGHT_BUNKER_DAL bunkerDal = new FREIGHT_BUNKER_DAL();
                CPAI_FREIGHT_BUNKER bunker = bunkerDal.GetFBCDATAByID(id);
                if (bunker != null)
                {
                    FclBunkerCost cost = new FclBunkerCost();
                    cost.fcl_price.fo = bunker.FBC_FCL_PRICE_FO;
                    cost.fcl_price.mgo = bunker.FBC_FCL_PRICE_MGO;
                    cost.loading = bunker.FBC_LOADING;
                    cost.discharge_port = bunker.FBC_DIS_PORT;
                    cost.steaming_ballast = bunker.FBC_STEAMING_BALLAST;
                    cost.steaming_laden = bunker.FBC_STEAMING_LADEN;
                    cost.tank_cleaning = bunker.FBC_TANK_CLEANING;
                    rootObj.fcl_bunker_cost.Add(cost);
                }
                rootObj.total_bunker_cost = data.FDA_TOT_BUNKER_COST;

                FREIGHT_CARGO_DAL cargoDal = new FREIGHT_CARGO_DAL();
                List <CPAI_FREIGHT_CARGO> cargoes = cargoDal.GetFBCDATAByID(id);
                if (cargoes != null)
                {
                    foreach(var cargo in cargoes)
                    {
                        FclCargo newcargo = new FclCargo();
                        newcargo.order = cargo.FDC_ORDER;
                        newcargo.cargo = cargo.FDC_FK_CARGO;
                        newcargo.quantity = cargo.FDC_QUANTITY;
                        newcargo.unit = cargo.FDC_UNIT;
                        newcargo.unit_other = cargo.FDC_OTHERS;
                        newcargo.tolerance = cargo.FDC_TOLERANCE;
                        rootObj.fcl_payment_terms.Cargoes.Add(newcargo);
                    }
                }

                rootObj.fcl_other_cost.fix_cost = data.FDA_FIX_COST_PER_DAY;
                rootObj.fcl_other_cost.total_fix_cost_per_day = data.FDA_TOT_FIX_COST_PER_DAY;
                rootObj.fcl_other_cost.total_commession = data.FDA_TOTAL_COMMISSION;
                rootObj.fcl_other_cost.withold_tax = data.FDA_WITHHOLDING_TAX;
                rootObj.fcl_other_cost.others = data.FDA_COST_OTHERS;
                rootObj.fcl_other_cost.total_expenses = data.FDA_TOTAL_EXPENSES;
                rootObj.fcl_other_cost.suggested_margin = data.FDA_SUGGESTED_MARGIN;
                rootObj.fcl_other_cost.suggested_margin_value = data.FDA_SUGGESTED_MARGIN_VALUE;

                rootObj.fcl_results.offer_freight.offer_unit = data.FDA_OFFER_UNIT;
                rootObj.fcl_results.offer_freight.offer_by = data.FDA_OFFER_BY;
                rootObj.fcl_results.offer_freight.usd = data.FDA_OFFER_FREIGHT;
                rootObj.fcl_results.offer_freight.baht = data.FDA_OFFER_FREIGHT_B;
                rootObj.fcl_results.offer_freight.mt = data.FDA_OFFER_FREIGHT_MT;
                rootObj.fcl_results.total_variable_cost.usd = data.FDA_TOT_VAR_COST;
                rootObj.fcl_results.total_variable_cost.baht = data.FDA_TOT_VAR_COST_B;
                rootObj.fcl_results.profit_over.usd = data.FDA_PROFIT_VAR;
                rootObj.fcl_results.profit_over.baht = data.FDA_PROFIT_VAR_B;
                rootObj.fcl_results.profit_over.percent = data.FDA_PROFIT_VAR_PERCENT;
                rootObj.fcl_results.total_fix_cost.usd = data.FDA_TOT_FIX_COST;
                rootObj.fcl_results.total_fix_cost.baht = data.FDA_TOT_FIX_COST_B;
                rootObj.fcl_results.total_cost.usd = data.FDA_TOT_COST;
                rootObj.fcl_results.total_cost.baht = data.FDA_TOT_COST_B;
                rootObj.fcl_results.profit_over_total.usd = data.FDA_PROFIT_TOT;
                rootObj.fcl_results.profit_over_total.baht = data.FDA_PROFIT_TOT_B;
                rootObj.fcl_results.profit_over_total.percent = data.FDA_PROFIT_TOT_PERCENT;
                rootObj.fcl_results.market_ref.usd = data.FDA_MARKET_FREIGHT;
                rootObj.fcl_results.market_ref.baht = data.FDA_MARKET_FREIGHT_B;

                rootObj.fcl_remark = data.FDA_REMARK;
                rootObj.fcl_pic_path = data.FDA_IMAGE_PATH;
                rootObj.fcl_excel_file_name = data.FDA_FILE_PATH;
            } else
            {
                rootObj.fcl_bunker_cost.Add(new FclBunkerCost());
            }

            return new JavaScriptSerializer().Serialize(rootObj);
        }

        public static string getAttFileByID(string id)
        {
            string strJSON = "{\"attach_items\":[{0}]}";

            string strFile = "";
            CHI_ATTACH_FILE_DAL fileDal = new CHI_ATTACH_FILE_DAL();
            List<CHI_ATTACH_FILE> extFile = fileDal.GetFileByDataID(id, "ATT");
            if (extFile.Count > 0)
            {
                for (int i = 0; i < extFile.Count; i++)
                {
                    strFile += "\"" + extFile[i].IAF_PATH + "\"";

                    if (i < extFile.Count - 1)
                    {
                        strFile += ",";
                    }
                }
            }
            else
            {
                strFile = "\"\"";
            }

            strJSON = strJSON.Replace("{0}", strFile);

            return strJSON;
        }

        public string GetExchangeRate(string date)
        {
            string exchangVal = "";
            try
            {

                if (date == "") return "";
                string[] temp = date.Split('/');
                if (temp.Length == 3 )
                {
                    date = temp[2] + temp[1] + temp[0];
                }
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var queryExchange = (from v in context.GLOBAL_CONFIG
                                         where v.GCG_KEY.ToUpper().Equals("FREIGHT_EXCHANGE_RATE".ToUpper())
                                         select new
                                         {
                                             dExchangKey = v.GCG_VALUE
                                         });

                    if (queryExchange != null)
                    {
                        string type = "";
                        foreach (var g in queryExchange)
                        {
                            if (g.dExchangKey == "B")
                            {
                                type = "B";
                            }
                            else type = "M";

                            var query = (from v in context.MKT_TRN_FX_B
                                         where v.T_FXB_VALDATE == date && v.T_FXB_CUR == "USD" && v.T_FXB_VALTYPE == type
                                         select new
                                         {
                                             dExchangVal = v.T_FXB_VALUE1
                                         });
                            if (query != null)
                            {
                                foreach (var t in query)
                                {
                                    exchangVal = t.dExchangVal.ToString();
                                }
                            }
                        }

                    }
                }

                return exchangVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FclLatestBunkerPrice GetLastBunkerByDate(string vessel_id, DateTime? date)
        {
            FclLatestBunkerPrice result = new FclLatestBunkerPrice();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sql = (from v in context.BNK_DATA
                               join o in context.BNK_OFFER_ITEMS on v.BDA_ROW_ID equals o.BOI_FK_BNK_DATA
                               join i in context.BNK_ROUND_ITEMS on o.BOI_ROW_ID equals i.BRI_FK_OFFER_ITEMS
                               join r in context.BNK_ROUND on i.BRI_ROW_ID equals r.BRD_FK_ROUND_ITEMS
                               where v.BDA_STATUS == "APPROVED" && v.BDA_TYPE == "VESSEL" && v.BDA_FK_VESSEL == vessel_id && (date.HasValue ? v.BDA_PURCHASE_DATE <= date : true)
                               && o.BOI_FINAL_FLAG == "Y"
                               select new
                               {
                                   ROW_ID = v.BDA_ROW_ID,
                                   PURCHASE_NO = v.BDA_PURCHASE_NO,
                                   PURCHASE_DATE = v.BDA_PURCHASE_DATE,
                                   SUPPLYING_LOCATION = v.BDA_SUPPLYING_LOCATION,
                                   ROUND_NO = i.BRI_ROUND_NO,
                                   ROUND_TYPE = r.BRD_ROUND_TYPE,
                                   ROUND_VALUE = r.BRD_ROUND_VALUE
                               }).ToList().OrderBy(o => o.ROUND_TYPE).ToList().OrderBy(o => o.PURCHASE_NO).ToList().OrderByDescending(o => o.ROUND_NO).ToList().OrderByDescending(o => o.PURCHASE_DATE).ToList();

                    result.fcl_grade_fo = new FclGradeFo();
                    result.fcl_grade_mgo = new FclGradeMgo();

                    for (int i = 0; i < sql.Count; i++)
                    {
                        if (sql[i].ROUND_TYPE.Contains("FO"))
                        {
                            result.fcl_grade_fo.bunker_id = sql[i].ROW_ID.Encrypt();
                            result.fcl_grade_fo.bunker_purno = sql[i].PURCHASE_NO.Encrypt();
                            result.fcl_grade_fo.date = ShareFn.ConvertDateTimeToDateStringFormat(sql[i].PURCHASE_DATE, "dd/MM/yyyy");
                            result.fcl_grade_fo.place = sql[i].SUPPLYING_LOCATION;
                            result.fcl_grade_fo.price = sql[i].ROUND_VALUE;
                            break;
                        }
                    }

                    for (int i = 0; i < sql.Count; i++)
                    {
                        if (sql[i].ROUND_TYPE.Contains("MGO"))
                        {
                            result.fcl_grade_mgo.bunker_id = sql[i].ROW_ID.Encrypt();
                            result.fcl_grade_mgo.bunker_purno = sql[i].PURCHASE_NO.Encrypt();
                            result.fcl_grade_mgo.date = ShareFn.ConvertDateTimeToDateStringFormat(sql[i].PURCHASE_DATE, "dd/MM/yyyy");
                            result.fcl_grade_mgo.place = sql[i].SUPPLYING_LOCATION;
                            result.fcl_grade_mgo.price = sql[i].ROUND_VALUE;
                            break;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public FclLatestBunkerPrice GetLastBunker(string vessel_id, DateTime? date)
        {
            FclLatestBunkerPrice val = new FclLatestBunkerPrice();
            ArrayList bnkData = new ArrayList();
            
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string[] bnkDataID = new string[0];
                    var queryDATA = (from r in context.BNK_DATA
                                 where r.BDA_STATUS == "APPROVED" && r.BDA_TYPE == "VESSEL" && r.BDA_FK_VESSEL == vessel_id && (date.HasValue ? r.BDA_PURCHASE_DATE >= date : true)
                                     select new {
                                         ROW_ID = r.BDA_ROW_ID,
                                         PURCHASE_DATE = r.BDA_PURCHASE_DATE,
                                         SUPPLYING_LOCATION = r.BDA_SUPPLYING_LOCATION,
                                         PURNO = r.BDA_PURCHASE_NO
                                     }).ToList().OrderBy(o => o.PURCHASE_DATE).ToList();
                    if (queryDATA != null)
                    {
                        bnkDataID = new string[queryDATA.Count()];
                        int j = 0;
                        foreach (var i in queryDATA)
                        {
                            bnkDataID[j] = i.ROW_ID;
                            j += 1;
                            string[] tmp = new string[4];
                            tmp[0] = i.ROW_ID;
                            tmp[1] = ShareFn.ConvertDateTimeToDateStringFormat(i.PURCHASE_DATE, "dd/MM/yyyy");
                            tmp[2] = i.SUPPLYING_LOCATION;
                            tmp[3] = "";
                            bnkData.Add(tmp);                            
                        }
                    }

                    string[] bnkOfficeID = new string[0];
                    if (bnkDataID.Count()>0)
                    {
                       var queryOFFICE = (from r in context.BNK_OFFER_ITEMS
                                       where bnkDataID.Contains(r.BOI_FK_BNK_DATA)
                                       && r.BOI_FINAL_FLAG == "Y"
                                       select r).ToList();

                        if(queryOFFICE != null)
                        {
                            bnkOfficeID = new string[queryOFFICE.Count()];
                            for (int k = 0; k < bnkOfficeID.Count(); k++)
                            {
                                bnkOfficeID[k] = queryOFFICE[k].BOI_ROW_ID;
                            }

                            #region FO
                            var queryItemFO = (from r in context.BNK_ROUND
                                             join i in context.BNK_ROUND_ITEMS on r.BRD_FK_ROUND_ITEMS equals i.BRI_ROW_ID
                                             where bnkOfficeID.Contains(i.BRI_FK_OFFER_ITEMS)
                                             && r.BRD_ROUND_TYPE.Contains("FO")
                                             orderby i.BRI_ROUND_NO descending, i.BRI_CREATED descending, r.BRD_ROUND_VALUE descending
                                               select new { OFFICE_ID = i.BRI_FK_OFFER_ITEMS,
                                                    ROUND_NO = i.BRI_ROUND_NO,
                                                    ROUND_TYPE = r.BRD_ROUND_TYPE,
                                                    ROUND_VALUE = r.BRD_ROUND_VALUE
                                               }).Take(1).ToList();
                            if(queryItemFO != null)
                            {
                                if(queryItemFO.Count()> 0)
                                {
                                    var tmpOffice = queryOFFICE.Where(p => p.BOI_ROW_ID == queryItemFO[0].OFFICE_ID).ToList();

                                    if (tmpOffice != null)
                                    {
                                        if (tmpOffice.Count() > 0)
                                        {
                                            var tmpData = queryDATA.Where(p => p.ROW_ID == tmpOffice[0].BOI_FK_BNK_DATA).ToList();
                                            if(tmpData != null)
                                            {
                                                if (tmpData.Count() > 0)
                                                {
                                                    val.fcl_grade_fo = new FclGradeFo();
                                                    val.fcl_grade_fo.date = ShareFn.ConvertDateTimeToDateStringFormat(tmpData[0].PURCHASE_DATE, "dd/MM/yyyy") ;
                                                    val.fcl_grade_fo.place = tmpData[0].SUPPLYING_LOCATION;
                                                    val.fcl_grade_fo.price = queryItemFO[0].ROUND_VALUE;
                                                    val.fcl_grade_fo.bunker_id = tmpData[0].ROW_ID.Encrypt();
                                                    val.fcl_grade_fo.bunker_purno = tmpData[0].PURNO.Encrypt();
                                                }
                                            }
                                        }                                        
                                    }
                                }                                
                            }
                            #endregion

                            #region MGO
                            var queryItemMGO = (from r in context.BNK_ROUND
                                               join i in context.BNK_ROUND_ITEMS on r.BRD_FK_ROUND_ITEMS equals i.BRI_ROW_ID
                                               where bnkOfficeID.Contains(i.BRI_FK_OFFER_ITEMS)
                                               && r.BRD_ROUND_TYPE.Contains("MGO")
                                               orderby i.BRI_ROUND_NO descending, i.BRI_CREATED descending
                                               , r.BRD_ROUND_VALUE descending
                                                select new
                                               {
                                                   OFFICE_ID = i.BRI_FK_OFFER_ITEMS,
                                                   ROUND_NO = i.BRI_ROUND_NO,
                                                   ROUND_TYPE = r.BRD_ROUND_TYPE,
                                                   ROUND_VALUE = r.BRD_ROUND_VALUE
                                               }).Take(1).ToList();
                            if (queryItemMGO != null)
                            {
                                if (queryItemMGO.Count() > 0)
                                {
                                    var tmpOffice = queryOFFICE.Where(p => p.BOI_ROW_ID == queryItemMGO[0].OFFICE_ID).ToList();

                                    if (tmpOffice != null)
                                    {
                                        if (tmpOffice.Count() > 0)
                                        {
                                            var tmpData = queryDATA.Where(p => p.ROW_ID == tmpOffice[0].BOI_FK_BNK_DATA).ToList();
                                            if (tmpData != null)
                                            {
                                                if (tmpData.Count() > 0)
                                                {
                                                    val.fcl_grade_mgo = new FclGradeMgo();
                                                    val.fcl_grade_mgo.date = ShareFn.ConvertDateTimeToDateStringFormat(tmpData[0].PURCHASE_DATE, "dd/MM/yyyy");
                                                    val.fcl_grade_mgo.place = tmpData[0].SUPPLYING_LOCATION;
                                                    val.fcl_grade_mgo.price = queryItemMGO[0].ROUND_VALUE;
                                                    val.fcl_grade_mgo.bunker_id = tmpData[0].ROW_ID.Encrypt();
                                                    val.fcl_grade_mgo.bunker_purno = tmpData[0].PURNO.Encrypt();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }                    
                }
                return val;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public MT_CUST_PAYMENT_TERM GetPaymentByBrokerName(string name)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_CUST_PAYMENT_TERM> results = context.MT_CUST_PAYMENT_TERM.Where(x => x.MCP_BROKER_NAME.ToUpper() == name.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_CUST_PAYMENT_TERM();
                }
            }
        }

        public MT_CUST_PAYMENT_TERM GetPaymentByCharterer(string name)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_CUST_PAYMENT_TERM> results = (from d in context.MT_CUST_DETAIL
                               join p in context.MT_CUST_PAYMENT_TERM
                               on d.MCD_FK_CUS equals p.MCP_FK_MT_CUST
                               where d.MCD_ROW_ID == name 
                               select p).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0];
                }
                else
                {
                    return new MT_CUST_PAYMENT_TERM();
                }
            }
        }

        public string GetPortCharge(string portId)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_JETTY_CHARGE> results = context.MT_JETTY_CHARGE.Where(x => x.MJC_FK_MT_JETTY.ToUpper() == portId.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0].MJC_PORT_CHARGE;
                }
                else
                {
                    return "0";
                }
            }
        }

        public string GetPortCharge(string portId, string vesselId)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                List<MT_JETTY_CHARGE> results = context.MT_JETTY_CHARGE.Where(x => x.MJC_FK_MT_JETTY.ToUpper() == portId.ToUpper() && x.MJC_FK_MT_VEHICLE.ToUpper() == vesselId.ToUpper()).ToList();
                if (results != null && results.Count() > 0)
                {
                    return results[0].MJC_PORT_CHARGE;
                }
                else
                {
                    return "0";
                }
            }
        }

        public static Jetty_Port getPortJettyById(string row_id)
        {
            MT_JETTY temp = PortDAL.GetPortJettyById(row_id).FirstOrDefault();

            Jetty_Port result = new Jetty_Port();
            result.PortType = temp.MTJ_PORT_TYPE;
            result.PortCountry = temp.MTJ_MT_COUNTRY;
            result.PortLocation = temp.MTJ_LOCATION;
            result.JettyPortName = temp.MTJ_JETTY_NAME;

            return result;
        }

        public static List<ListDrowDownKey> getPortJetty(string fromType = "", string portType = "", string country = "", string location = "", string name = "")
        {
            List<MT_JETTY> mt_port = PortDAL.GetPortData();
            if (fromType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_CREATE_TYPE.Trim().ToUpper() == fromType.Trim().ToUpper()).OrderBy(x => x.MTJ_PORT_TYPE).ToList();
            }
            if (portType != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_PORT_TYPE.Trim().ToUpper() == portType.Trim().ToUpper()).OrderBy(x => x.MTJ_MT_COUNTRY).ToList();
            }
            if (country != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_MT_COUNTRY.Trim().ToUpper() == country.Trim().ToUpper()).OrderBy(x => x.MTJ_LOCATION).ToList();
            }
            if (location != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_LOCATION.Trim().ToUpper() == location.Trim().ToUpper()).OrderBy(x => x.MTJ_JETTY_NAME).ToList();
            }
            if (name != "")
            {
                mt_port = mt_port.Where(p => p.MTJ_JETTY_NAME.ToUpper().Trim() == name.ToUpper().Trim()).OrderBy(x => x.MTJ_JETTY_NAME).ToList();
            }

            if (fromType != "" && portType != "" && country != "" && location != "" && name != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_ROW_ID).ToList(), mt_port.Select(x => x.MTJ_JETTY_NAME).ToList(), mt_port.Select(x => (x.MTJ_JETTY_NAME + ", " + x.MTJ_LOCATION + ", " + x.MTJ_MT_COUNTRY)).ToList());
            }
            else if (fromType != "" && portType != "" && country != "" && location != "" )
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_ROW_ID).ToList(), mt_port.Select(x => x.MTJ_JETTY_NAME).ToList(), mt_port.Select(x => (x.MTJ_JETTY_NAME + ", " + x.MTJ_LOCATION + ", " + x.MTJ_MT_COUNTRY)).ToList());
            }
            else if (fromType != "" && portType != "" && country != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList(), mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList(), mt_port.Select(x => x.MTJ_LOCATION).Distinct().ToList());
            }
            else if (fromType != "" && portType != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList(), mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList(), mt_port.Select(x => x.MTJ_MT_COUNTRY).Distinct().ToList());
            }
            else if (fromType != "")
            {
                return mapSelectListKey(mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList(), mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList(), mt_port.Select(x => x.MTJ_PORT_TYPE).Distinct().ToList());
            }
            else
                return new List<ListDrowDownKey>();
           
        }

        private static List<ListDrowDownKey> mapSelectListKey(List<string> key, List<string> text, List<string> value)
        {
            List<ListDrowDownKey> selectList = new List<ListDrowDownKey>();
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new ListDrowDownKey { Key = key[i], Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public string getBokerCommission(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from p in context.MT_VENDOR
                             where p.VND_ACC_NUM_VENDOR.ToUpper() == id.ToUpper()
                             select p).FirstOrDefault();
                return query.VND_BROKER_COMMISSION;
            }
                
        }

        public string getLastExchangeRate(string date)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var qurey = (from p in context.MKT_TRN_FX_B
                             where p.T_FXB_CUR.ToUpper() == "USD"
                             select p).ToList().OrderByDescending(x => x.T_FXB_VALDATE).Where(v => int.Parse(v.T_FXB_VALDATE) <= int.Parse(date)).FirstOrDefault();
                return qurey.T_FXB_VALUE1.ToString() + "|" + qurey.T_FXB_VALDATE;
            }
        }

        public FclBunkerConsumption getBunkerConsumptionSpeed(string id, string type)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                FclBunkerConsumption data = new FclBunkerConsumption();
                var query = (from p in context.MT_VEHICLE
                             where p.VEH_ID.ToUpper() == id.ToUpper()
                             select p).FirstOrDefault();

                if (type.ToUpper() == "FULL")
                {
                    data.fcl_load.fo = query.VHE_F_FO_LOAD;
                    data.fcl_load.mgo = query.VHE_F_MGO_LOAD;
                    data.fcl_discharge.fo = query.VHE_F_FO_DISCHARGE;
                    data.fcl_discharge.mgo = query.VHE_F_MGO_DISCHARGE;
                    data.fcl_steaming_ballast.fo = query.VHE_F_FO_STEAMING_BALLAST;
                    data.fcl_steaming_ballast.mgo = query.VHE_F_MGO_STEAMING_BALLAST;
                    data.fcl_steaming_laden.fo = query.VHE_F_FO_STEAMING_LADEN;
                    data.fcl_steaming_laden.mgo = query.VHE_F_MGO_STEAMING_LADEN;
                    data.fcl_idle.fo = query.VHE_F_FO_IDLE;
                    data.fcl_idle.mgo = query.VHE_F_MGO_IDLE;
                    data.fcl_tack_cleaning.fo = query.VHE_F_FO_TANK_CLEANING;
                    data.fcl_tack_cleaning.mgo = query.VHE_F_MGO_TANK_CLEANING;
                    data.fcl_heating.fo = query.VHE_F_FO_HEATING;
                    data.fcl_heating.mgo = query.VHE_F_MGO_HEATING;
                }
                else if (type.ToUpper() == "ECO")
                {
                    data.fcl_load.fo = query.VHE_FO_LOAD;
                    data.fcl_load.mgo = query.VHE_MGO_LOAD;
                    data.fcl_discharge.fo = query.VHE_FO_DISCHARGE;
                    data.fcl_discharge.mgo = query.VHE_MGO_DISCHARGE;
                    data.fcl_steaming_ballast.fo = query.VHE_FO_STEAMING_BALLAST;
                    data.fcl_steaming_ballast.mgo = query.VHE_MGO_STEAMING_BALLAST;
                    data.fcl_steaming_laden.fo = query.VHE_FO_STEAMING_LADEN;
                    data.fcl_steaming_laden.mgo = query.VHE_MGO_STEAMING_LADEN;
                    data.fcl_idle.fo = query.VHE_FO_IDLE;
                    data.fcl_idle.mgo = query.VHE_MGO_IDLE;
                    data.fcl_tack_cleaning.fo = query.VHE_FO_TANK_CLEANING;
                    data.fcl_tack_cleaning.mgo = query.VHE_MGO_TANK_CLEANING;
                    data.fcl_heating.fo = query.VHE_FO_HEATING;
                    data.fcl_heating.mgo = query.VHE_MGO_HEATING;
                }                
                return data;
            }
        }

        public static string getLastFreightNo(string month)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string result = "";
                string month_temp = "FRI-" + month;
                var query = (from f in context.CPAI_FREIGHT_DATA
                             where f.FDA_DOC_NO.Contains(month_temp)
                             orderby f.FDA_DOC_NO descending
                             select f).FirstOrDefault();

                if (query != null)
                {
                    result = month_temp + "-" + (int.Parse(query.FDA_DOC_NO.SplitWord("-")[2]) + 1).ToString().PadLeft(4, '0');
                }
                else
                {
                    result = month_temp + "-0001";
                }

                return result;
            }                
        }

        public static bool isUseFreightCharterOut(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from x in context.CHOT_DATA
                             //join f in context.CPAI_FREIGHT_DATA on x.OTDA_FK_FREIGHT equals f.FDA_ROW_ID
                             where x.OTDA_FK_FREIGHT == id && x.OTDA_STATUS != "CANCEL"
                             select x).ToList();

                if (query != null && query.Count != 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }               
        }

    }
}