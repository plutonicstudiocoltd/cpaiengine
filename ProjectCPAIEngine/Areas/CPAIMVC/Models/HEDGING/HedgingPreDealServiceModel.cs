﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Web.Script.Serialization;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;

using com.pttict.engine.utility;
using com.pttict.engine.dal.Entity;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgingPreDealServiceModel
    {
        private List<HedgingPreDealViewModel_Detail> getDealDetail(string preDealNo)
        {
            List<HedgingPreDealViewModel_Detail> detail = new List<HedgingPreDealViewModel_Detail>();
            FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
            List<FUNCTION_TRANSACTION> listTran = funcTran.GetDealByPreDeal(preDealNo.Trim());

            if (listTran != null && listTran.Count() > 0)
            {
                foreach (var i in listTran)
                {
                    detail.Add(new HedgingPreDealViewModel_Detail
                    {
                        hTransactionIdEncrypted = i.FTX_TRANS_ID.Encrypt(),
                        hTransactionRequestEncrypted = i.FTX_REQ_TRANS.Encrypt(),
                        hSystemEncrypted = i.FTX_INDEX1.Encrypt(),
                        hTypeEncrypted = i.FTX_INDEX2.Encrypt(),
                        hScreenTypeEncrypted = ("DEAL").Encrypt(),
                        hDealNoEncrypted = i.FTX_INDEX6.Encrypt(),

                        ref_pre_deal_no = preDealNo.Trim(),                        
                        status = i.FTX_INDEX4,
                        deal_no = i.FTX_INDEX6                        
                    });
                }
            }

            return detail;
        }
        
        public ReturnValue Search(ref HedgingPreDealViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();            
            RequestCPAI req = new RequestCPAI();
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService projService = new ServiceProvider.ProjService();
            List<Transaction> _transaction = new List<Flow.Model.Transaction>();
            List_TCEtrx _model;
            string[] lstStatus;
            string _strfrom, _strTo, sPre_Deal_no, sDeal_no, sStatus, sHedged_Type, sTrade_For, sTool, sSeller, sBuyer, sUnderlying, sUnderlyingVS, sCreate_by;

            try
            {
                try
                {
                    _strfrom = String.IsNullOrEmpty(pModel.sDeal_Date) ? string.Empty : pModel.sDeal_Date.Trim().Substring(0, 10);
                    _strTo = String.IsNullOrEmpty(pModel.sDeal_Date) ? string.Empty : pModel.sDeal_Date.Trim().Substring(14, 10);
                    _strfrom = _strfrom.Replace("-", "/");
                    _strTo = _strTo.Replace("-", "/");
                    sPre_Deal_no = String.IsNullOrEmpty(pModel.sPre_Deal_no) ? string.Empty : pModel.sPre_Deal_no.Trim().ToLower();
                    sDeal_no = String.IsNullOrEmpty(pModel.sDeal_no) ? string.Empty : pModel.sDeal_no.Trim().ToLower();
                    sStatus = String.IsNullOrEmpty(pModel.sStatus) ? string.Empty : pModel.sStatus.Trim().ToLower();
                    lstStatus = String.IsNullOrEmpty(pModel.sStatusSelected) ? lstStatus = new string[0] : pModel.sStatusSelected.Trim().ToLower().Split(char.Parse("|"));
                    sHedged_Type = String.IsNullOrEmpty(pModel.sHedged_Type) ? string.Empty : pModel.sHedged_Type.Trim().ToLower();
                    sTrade_For = String.IsNullOrEmpty(pModel.sTrade_For) ? string.Empty : pModel.sTrade_For.Trim().ToLower();
                    sTool = String.IsNullOrEmpty(pModel.sTool) ? string.Empty : pModel.sTool.Trim().ToLower();
                    sSeller = String.IsNullOrEmpty(pModel.sSeller) ? string.Empty : pModel.sSeller.Trim().ToLower();
                    sBuyer = String.IsNullOrEmpty(pModel.sBuyer) ? string.Empty : pModel.sBuyer.Trim().ToLower();
                    sUnderlying = String.IsNullOrEmpty(pModel.sUnderlying) ? string.Empty : pModel.sUnderlying.Trim().ToLower();
                    sUnderlyingVS = String.IsNullOrEmpty(pModel.sVS) ? string.Empty : pModel.sVS.Trim().ToLower();
                    sCreate_by = String.IsNullOrEmpty(pModel.sCreate_by) ? string.Empty : pModel.sCreate_by.Trim().ToLower();                    
                }
                catch
                {
                    rtn.Status = false;
                    rtn.Message = "Data invalid format";
                    return rtn;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                req.Function_id = ConstantPrm.FUNCTION.F10000054;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.HEDG_DEAL });
                req.Req_parameters.P.Add(new P { K = "type", V = pModel.systemType });
                req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
                req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
                req.Req_parameters.P.Add(new P { K = "status", V = sStatus });
                req.Req_parameters.P.Add(new P { K = "from_date", V = _strfrom });
                req.Req_parameters.P.Add(new P { K = "to_date", V = _strTo });
                //req.Req_parameters.P.Add(new P { K = "index_5", V = (_strfrom != "" && _strTo != "") ? _strfrom + "|" + _strTo : "" });
                req.Extra_xml = "";

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = projService.CallService(reqData);
                if (!resData.result_code.Trim().Equals("1"))
                {
                    rtn.Status = false;
                    rtn.Message = resData.result_desc;
                    return rtn;
                }
                _model = ShareFunction.DeserializeXMLFileToObject<List_TCEtrx>(resData.extra_xml);
                if (_model == null || _model.TCETransaction == null || _model.TCETransaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }                
                _transaction = _model.TCETransaction;
                if (_transaction == null || _transaction.Count < 1)
                {
                    rtn.Status = false;
                    rtn.Message = "Data not found";
                    return rtn;
                }
                _transaction = _transaction.OrderByDescending(a => a.pre_deal_no).ToList();
                if (!String.IsNullOrEmpty(sPre_Deal_no))
                    _transaction = _transaction.Where(x => x.pre_deal_no != null && x.pre_deal_no.Trim().ToLower().Contains(sPre_Deal_no)).ToList();                                
                if (!String.IsNullOrEmpty(sHedged_Type))
                    _transaction = _transaction.Where(x => x.hedg_type != null && x.hedg_type.Trim().ToLower().Equals(sHedged_Type)).ToList();
                if (!String.IsNullOrEmpty(sTrade_For))
                    _transaction = _transaction.Where(x => x.trade_for != null && x.trade_for.Trim().ToLower().Equals(sTrade_For)).ToList();
                if (!String.IsNullOrEmpty(sTool))
                    _transaction = _transaction.Where(x => x.tool != null && x.tool.Trim().ToLower().Equals(sTool)).ToList();
                if (!String.IsNullOrEmpty(sSeller))
                    _transaction = _transaction.Where(x => x.seller != null && x.seller.Trim().ToLower().Equals(sSeller)).ToList();
                if (!String.IsNullOrEmpty(sBuyer))
                    _transaction = _transaction.Where(x => x.buyer != null && x.buyer.Trim().ToLower().Equals(sBuyer)).ToList();
                if (!String.IsNullOrEmpty(sUnderlying))
                    _transaction = _transaction.Where(x => x.underlying != null && x.underlying.Trim().ToLower().Equals(sUnderlying)).ToList();
                if (!String.IsNullOrEmpty(sUnderlyingVS))
                    _transaction = _transaction.Where(x => x.underlying_vs != null && x.underlying_vs.Trim().ToLower().Equals(sUnderlyingVS)).ToList();
                if (!String.IsNullOrEmpty(sCreate_by))
                    _transaction = _transaction.Where(x => x.create_by != null && x.create_by.Trim().ToLower().Contains(sCreate_by)).ToList();                
                if (!string.IsNullOrEmpty(_strfrom) && !string.IsNullOrEmpty(_strTo))
                {
                    DateTime sDate = new DateTime();
                    DateTime eDate = new DateTime();
                    sDate = ShareFn.ConvertStrDateToDate(_strfrom);
                    eDate = ShareFn.ConvertStrDateToDate(_strTo).AddDays(1).AddSeconds(-1);
                    _transaction = _transaction.Where(x => x.deal_date != null && Convert.ToDateTime(x.deal_date) >= sDate && Convert.ToDateTime(x.deal_date) <= eDate).ToList();
                }
                if (lstStatus.Length > 0)
                    _transaction = _transaction.Where(x => x.status != null && lstStatus.Contains(x.status.Trim().ToLower())).ToList();
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////                             
                pModel.sSearchData = new List<HedgingPreDealViewModel_SearchData>();                
                foreach (var i in _transaction)
                {
                    pModel.sSearchData.Add(new HedgingPreDealViewModel_SearchData
                    {                        
                        hTransactionIdEncrypted = i.transaction_id.Encrypt(),
                        hTransactionRequestEncrypted  = i.req_transaction_id.Encrypt(),
                        hSystemEncrypted  = i.system.Encrypt(),
                        hTypeEncrypted  = i.type.Encrypt(),
                        hScreenTypeEncrypted  = ("PREDEAL").Encrypt(),
                        hPreDealEncrypted  = i.pre_deal_no.Encrypt(),

                        dPreDeal_Status = i.status,
                        dDate = i.deal_date,
                        dPre_Deal_no = i.pre_deal_no,
                        dHedged_Type = i.hedg_type,
                        dTrade_For = i.trade_for,
                        dTool = i.tool,
                        dUnderlying = i.underlying,
                        dToolVS = i.underlying_vs,
                        dSeller = i.seller,
                        dBuyer = i.buyer,
                        dCreate_by = i.create_by,
                        detail = getDealDetail(i.pre_deal_no)
                    });
                }
                if (!String.IsNullOrEmpty(sDeal_no))
                    pModel.sSearchData = pModel.sSearchData.Where(x => x.detail.Any(y => y.deal_no.Trim().ToLower().Contains(sDeal_no))).ToList();
                rtn.Status = true;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
            }
            catch (Exception ex)
            { 
                rtn.Status = false;
                rtn.Message = ex.Message;                
            }

            return rtn;
        }
    }
}
