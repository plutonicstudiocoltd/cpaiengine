﻿using OfficeOpenXml;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Services;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Services.CDSService;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeCDSImportServiceModel
    {
        public ReturnValue Search(ref HedgeCDSImportViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP_CDS_FILE
                                 orderby v.HMCF_IMPORTED_DATE descending
                                 select new HedgeCDSImport_SearchData
                                 {
                                     dID = v.HMCF_ROW_ID,
                                     dFileName = v.HMCF_FILE_NAME,
                                     dFilePath = v.HMCF_FILE_PATH,
                                     dResult = v.HMCF_RESULT,
                                     dNote = v.HMCF_NOTE,
                                     dImportDate = v.HMCF_IMPORTED_DATE,
                                     dImportBy = v.HMCF_IMPORTED_BY
                                 });

                    if (query != null)
                    {
                        pModel.sSearchData = new List<HedgeCDSImport_SearchData>();

                        if (pModel != null)
                            query = query.Take(pModel.sLimitRow);

                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new HedgeCDSImport_SearchData
                                                    {
                                                        dID = g.dID,
                                                        dFileName = g.dFileName,
                                                        dFilePath = g.dFilePath,
                                                        dResult = g.dResult,
                                                        dNote = g.dNote,
                                                        dImportDate = g.dImportDate,
                                                        dImportBy = g.dImportBy
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue UploadExcel(string pFileName, string pFilePath, string pResult, string pNote, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            List<HedgingCDSImportViewModel_Detail> hmc_detail = new List<HedgingCDSImportViewModel_Detail>();

            List<HedgingCDSImportViewModel_Detail> hmc_detailTmp = new List<HedgingCDSImportViewModel_Detail>();
            List<HedgingCDSImportViewModel_DetailCds> hmc_DetailCdsTmp = new List<HedgingCDSImportViewModel_DetailCds>();

            List<CounterPartyTemp> vendorMatchList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorUnMatchList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> vendorMatchGreatList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorMatchLessList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> vendorMatchSuccessList = new List<CounterPartyTemp>();
            List<CounterPartyTemp> vendorMatchFailedList = new List<CounterPartyTemp>();

            List<CounterPartyTemp> counterPartyList = new List<CounterPartyTemp>();

            List<MT_VENDOR> vendorResultList = new List<MT_VENDOR>();

            string fileNewID = "";

            try
            {
                rtn = SaveFileToDb(pFileName, pFilePath, pResult, pNote, pUser);
                fileNewID = rtn.newID;

                //List<VendorViewModel_SeachData> vendorControlList = new List<VendorViewModel_SeachData>();
                //vendorControlList = VendorServiceModel.getVendorControl("CPAI", "HEDG_MT_CP", "ACTIVE");
                var vendorControlList = getAllCounterParty().ToList();

                #region ReadExcelFile
                byte[] file = File.ReadAllBytes(pFilePath);
                using (MemoryStream ms = new MemoryStream(file))
                using (ExcelPackage package = new ExcelPackage(ms))
                {
                    ExcelWorksheet ws = package.Workbook.Worksheets.First();

                    for (int c = 1; c < ws.Dimension.End.Column; c++)
                    {

                        int vendorExist = vendorControlList.Count(x => (ws.Cells[1, c + 1].Value.ToString().Trim().Equals(x.MappingCdsCP)));
                        if (vendorExist != 0)
                        {
                            string vendorIDx = vendorControlList.Where(x => (ws.Cells[1, c + 1].Value.ToString().Trim().Equals(x.MappingCdsCP))).Select(x => x.CouterPartyID).FirstOrDefault();
                            for (int r = 2; r < ws.Dimension.End.Row; r++)
                            {
                                counterPartyList.Add(new CounterPartyTemp
                                {
                                    VendorID = vendorIDx,
                                    Name = ws.Cells[2, c].Value.ToString().Trim(),
                                    Rating = ws.Cells[2, c + 1].Value.ToString().Trim(),
                                    Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[r + 1, 1].Value).Date, "yyyyMMdd"),
                                    Value = ws.Cells[r + 1, c + 1].Value == null ? "" : ws.Cells[r + 1, c + 1].Value.ToString()
                                });
                            }
                            #region VendorMatch
                            vendorMatchList.Add(new CounterPartyTemp
                            {
                                VendorID = vendorIDx,
                                Name = ws.Cells[1, c + 1].Value.ToString().Trim(),
                                Rating = ws.Cells[2, c + 1].Value.ToString().Trim(),
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[ws.Dimension.End.Row, 1].Value).Date, "yyyyMMdd") + " to " +
                                       ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[2 + 1, 1].Value).Date, "yyyyMMdd"),
                                Count = (ws.Dimension.End.Row - 2).ToString()
                            });
                            #endregion
                        }
                        else
                        {
                            #region VendorUnMatch
                            vendorUnMatchList.Add(new CounterPartyTemp
                            {
                                VendorID = ws.Cells[1, c + 1].Value.ToString().Trim(),
                                Name = ws.Cells[1, c + 1].Value.ToString(),
                                Rating = ws.Cells[2, c + 1].Value.ToString(),
                                Date = ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[2 + 1, 1].Value).Date, "yyyyMMdd") + " to " +
                                             ShareFn.ConvertDateTimeToDateStringFormat(((System.DateTime)ws.Cells[ws.Dimension.End.Row, 1].Value).Date, "yyyyMMdd"),
                                Count = (ws.Dimension.End.Row - 2).ToString()
                            });
                            #endregion
                        }
                    }
                }
                #endregion

                #region MatchDataFillDate
                foreach (CounterPartyTemp counterParty in counterPartyList)
                {
                    string rCounterPartyID = counterParty.VendorID;
                    //string rName = counterParty.Name;
                    //string rRating = counterParty.Rating;
                    string rDate = counterParty.Date;
                    string rValue = counterParty.Value;

                    string maxCounterpartyDate = getCPDateControl().Where(x => x.VendorID == rCounterPartyID).Select(x => x.Date).FirstOrDefault();
                    if (Convert.ToInt32(rDate) > Convert.ToInt32(maxCounterpartyDate))
                    {
                        string newValue = null;
                        double rResult;

                        if (!double.TryParse(rValue, out rResult))
                        {
                            newValue = counterPartyList.AsEnumerable().Where(x => x.VendorID == rCounterPartyID
                            && Convert.ToInt32(x.Date) < Convert.ToInt32(rDate)
                            && !String.IsNullOrEmpty(x.Value)
                            ).OrderByDescending(x => x.Date).Select(x => x.Value).FirstOrDefault().ToString();
                        }
                        else
                        {
                            newValue = rValue;
                        }

                        hmc_DetailCdsTmp.Add(new HedgingCDSImportViewModel_DetailCds
                        {
                            HMCD_FK_HEDG_MT_CP = rCounterPartyID,
                            CdsDate = rDate,
                            CdsValue = newValue,
                            Status = "Add"
                        });
                    }
                    else
                    {
                        hmc_DetailCdsTmp.Add(new HedgingCDSImportViewModel_DetailCds
                        {
                            HMCD_FK_HEDG_MT_CP = rCounterPartyID,
                            CdsDate = rDate,
                            Status = "Exist"
                        });
                    }
                }
                #endregion

                #region FillDetailToHead
                foreach (var item in vendorMatchList)
                {
                    hmc_detail.Add(new HedgingCDSImportViewModel_Detail
                    {
                        HMCP_ROW_ID = null,
                        CouterPartyID = item.VendorID,
                        CreditRating = item.Rating,
                        CreditLimit = null,
                        LastCdsDate = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID).OrderByDescending(y => y.CdsDate).Select(z => z.CdsDate).FirstOrDefault(),
                        LastCdsValue = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID).OrderByDescending(y => y.CdsDate).Select(z => z.CdsValue).FirstOrDefault(),
                        Status = CPAIConstantUtil.NORMAL,
                        Reason = null,
                        DetailCds = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID).ToList()
                    });

                    string countAdd = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Add").Count().ToString();
                    string beginDateAdd = "0";
                    string endDateAdd = "0";
                    if (hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Add").OrderBy(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault() != null)
                    {
                        beginDateAdd = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Add").OrderBy(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault().ToString();
                        endDateAdd = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Add").OrderByDescending(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault().ToString();

                        vendorMatchGreatList.Add(new CounterPartyTemp
                        {
                            VendorID = item.VendorID,
                            Name = item.Name,
                            Date = beginDateAdd + " to " + endDateAdd,
                            Count = countAdd
                        });
                    }

                    string countExist = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Exist").Count().ToString();
                    string beginDateExist = "0";
                    string endDateExist = "0";
                    if (hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Exist").OrderBy(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault() != null)
                    {
                        beginDateExist = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Exist").OrderBy(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault().ToString();
                        endDateExist = hmc_DetailCdsTmp.Where(x => x.HMCD_FK_HEDG_MT_CP == item.VendorID && x.Status == "Exist").OrderByDescending(x => x.CdsDate).Select(x => x.CdsDate).FirstOrDefault().ToString();

                        vendorMatchLessList.Add(new CounterPartyTemp
                        {
                            VendorID = item.VendorID,
                            Name = item.Name,
                            Date = beginDateExist + " to " + endDateExist,
                            Count = countExist
                        });
                    }
                }
                #endregion

                #region Result
                foreach (var item in hmc_detail)
                {
                    rtn = SaveToDb(fileNewID, pUser, true, item);

                    var vendorMatchGreat = vendorMatchGreatList.Where(x => x.VendorID == item.CouterPartyID).FirstOrDefault();
                    var vendorMatchLess = vendorMatchLessList.Where(x => x.VendorID == item.CouterPartyID).FirstOrDefault();

                    if (rtn.Status == true)
                    {
                        if (vendorMatchGreat != null)
                        {
                            vendorMatchSuccessList.Add(new CounterPartyTemp
                            {
                                VendorID = vendorMatchGreat.VendorID,
                                Name = vendorMatchGreat.Name,
                                Date = null,
                                Count = null
                            });
                        }
                    }
                    else
                    {
                        vendorMatchFailedList.Add(new CounterPartyTemp
                        {
                            VendorID = vendorMatchGreat.VendorID,
                            Name = vendorMatchGreat.Name,
                            Date = null,
                            Count = null
                        });
                    }
                }

                string htmlText = "";
                if (vendorMatchLessList.Count() > 0)
                {
                    //htmlText += "Match " + vendorMatchList.Count() + "<br>";
                    //foreach (var item in vendorMatchList)
                    //{
                    //    htmlText += item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                    //}
                    //htmlText += "Success<br>";
                    //foreach (var item in vendorMatchSuccessList)
                    //{
                    //    htmlText += item.Name + " " + item.Date + " " + item.Count + "<br>";
                    //}
                    htmlText += "Does not update: [" + vendorMatchLessList.Count() + "]<br>";
                    foreach (var item in vendorMatchLessList)
                    {
                        htmlText += item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                    }
                }

                if (vendorMatchGreatList.Count() > 0)
                {
                    htmlText += "Addition Success : [" + vendorMatchGreatList.Count() + "]<br>";
                    foreach (var item in vendorMatchGreatList)
                    {
                        htmlText += item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                    }
                }

                if (vendorMatchFailedList.Count() > 0)
                {
                    htmlText += "Failed : [" + vendorMatchFailedList.Count() + "]<br>";
                    foreach (var item in vendorMatchFailedList)
                    {
                        htmlText += item.Name + "<br>";
                    }
                }

                if (vendorUnMatchList.Count() > 0)
                {
                    htmlText += "Unmatch [" + vendorUnMatchList.Count() + "]<br>";
                    foreach (var item in vendorUnMatchList)
                    {
                        //htmlText += item.Name + " date : " + item.Date + " record : " + item.Count + "<br>";
                        htmlText += item.Name + "<br>";
                    }
                }

                //foreach (var item in vendorMatchList)
                //{
                //    vendorResultList.Add(new MT_VENDOR
                //    {
                //        VND_ACC_NUM_VENDOR = item.VendorID,
                //        VND_NAME1 = item.Name,
                //    }
                //    );
                //    CDSService CDSService = new CDSService();
                //    ResultCheckCDS rCDS5 = CDSService.CheckCDS5(vendorResultList, pUser);
                //}

                rtn = UpdateResultFileToDb(fileNewID, rtn.Status, htmlText, pUser);

                #endregion
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
                UpdateResultFileToDb(fileNewID, rtn.Status, null, pUser);
                return rtn;
            }

            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
            rtn.Status = true;
            return rtn;
        }

        public ReturnValue SaveFileToDb(string pFileName, string pFilePath, string pResult, string pNote, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            if (!string.IsNullOrEmpty(pFileName) && !string.IsNullOrEmpty(pFilePath))
            {
                try
                {
                    HEDG_MT_CP_CDS_FILE_DAL fileDal = new HEDG_MT_CP_CDS_FILE_DAL();
                    HEDG_MT_CP_CDS_FILE fileEnt = new HEDG_MT_CP_CDS_FILE();

                    DateTime now = DateTime.Now;

                    using (var context = new EntityCPAIEngine())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                string fileCtrlID = ConstantPrm.GetDynamicCtrlID();
                                fileEnt.HMCF_ROW_ID = fileCtrlID;
                                fileEnt.HMCF_FILE_NAME = pFileName;
                                fileEnt.HMCF_FILE_PATH = pFilePath;
                                fileEnt.HMCF_RESULT = pResult;
                                fileEnt.HMCF_NOTE = pNote;
                                fileEnt.HMCF_IMPORTED_DATE = now;
                                fileEnt.HMCF_IMPORTED_BY = pUser;
                                fileEnt.HMCF_UPDATED_DATE = now;
                                fileEnt.HMCF_UPDATED_BY = pUser;

                                fileDal.Save(fileEnt, context);

                                dbContextTransaction.Commit();
                                rtn.newID = fileCtrlID;
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                rtn.Message = ex.Message;
                                rtn.Status = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }

            return rtn;
        }

        public ReturnValue UpdateResultFileToDb(string pfileCtrlID, bool pResult, string pNote, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            if (!string.IsNullOrEmpty(pfileCtrlID))
            {
                try
                {
                    HEDG_MT_CP_CDS_FILE_DAL fileDal = new HEDG_MT_CP_CDS_FILE_DAL();
                    HEDG_MT_CP_CDS_FILE fileEnt = new HEDG_MT_CP_CDS_FILE();

                    DateTime now = DateTime.Now;

                    using (var context = new EntityCPAIEngine())
                    {
                        using (var dbContextTransaction = context.Database.BeginTransaction())
                        {
                            try
                            {
                                fileEnt.HMCF_ROW_ID = pfileCtrlID;
                                fileEnt.HMCF_RESULT = (pResult == true) ? "SUCCESS" : "FAIL";
                                fileEnt.HMCF_NOTE = pNote;
                                fileEnt.HMCF_UPDATED_DATE = now;
                                fileEnt.HMCF_UPDATED_BY = pUser;

                                fileDal.Update(fileEnt, context);

                                dbContextTransaction.Commit();
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                rtn.Message = ex.Message;
                                rtn.Status = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }

            return rtn;
        }

        public ReturnValue SaveToDb(string fileCtrlID, string pUser, bool Excel, HedgingCDSImportViewModel_Detail hmc_detail)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                CreditRatingServiceModel creditRatingServiceModel = new CreditRatingServiceModel();

                HEDG_MT_CP_DAL cpDal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP cpEnt = new HEDG_MT_CP();

                HEDG_MT_CP_CDS_DAL cpCdsDal = new HEDG_MT_CP_CDS_DAL();
                HEDG_MT_CP_CDS cpCdsEnt = new HEDG_MT_CP_CDS();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string result = "Completed XXX ROW";
                            int lastLog = 1;
                            //1Update HEDG_MT_CP LasDate LastValue
                            //2Log HEDG_MT_CP_HISTORY_LOG
                            //3Insert HEDG_MT_CP_CDS
                            //4Log HEDG_MT_CP_HISTORY

                            var cpExist = getAllCounterParty().Where(x => x.CouterPartyID == hmc_detail.CouterPartyID).FirstOrDefault();
                            var creditDataOld = creditRatingServiceModel.GetList().creditRating.Where(x => x.ID == cpExist.CreditRating).FirstOrDefault();
                            var creditData = creditRatingServiceModel.GetList().creditRating.Where(x => x.Rating == hmc_detail.CreditRating).FirstOrDefault();

                            var itemDataList = hmc_detail.DetailCds.Where(x => x.HMCD_FK_HEDG_MT_CP == hmc_detail.CouterPartyID && x.Status == "Add");
                            int itemCount = itemDataList.Count();

                            if (itemCount != 0)
                            {
                                //1Update HEDG_MT_CP LasDate LastValue
                                string cpCtrlID = cpExist.HMCP_ROW_ID;
                                cpEnt.HMCP_ROW_ID = cpCtrlID;
                                cpEnt.HMCP_FK_VENDOR = hmc_detail.CouterPartyID;
                                cpEnt.HMCP_NAME = hmc_detail.CouterPartyName;
                                cpEnt.HMCP_FULL_NAME = hmc_detail.CouterPartyFullName;
                                cpEnt.HMCP_MAPPING_CDS_CP = hmc_detail.MappingCdsCP;
                                cpEnt.HMCP_LAST_CDS_DATE = ShareFn.ConvertStringDateFormatToDatetime(hmc_detail.LastCdsDate, "yyyyMMdd");
                                cpEnt.HMCP_LAST_CDS_VALUE = hmc_detail.LastCdsValue;
                                cpEnt.HMCP_CREDIT_RATING = creditData.ID;
                                cpEnt.HMCP_CREDIT_RATING_REF_FLAG = hmc_detail.CreditRatingRefFlag;
                                cpEnt.HMCP_CREDIT_LIMIT = creditData.Amount;
                                cpEnt.HMCP_CREDIT_LIMIT_UPDATE = cpExist.CreditLimitUpdate;
                                cpEnt.HMCP_PAYMENT_DAY = hmc_detail.PaymentDay;
                                cpEnt.HMCP_PAYMENT_DAY_TYPE = hmc_detail.PaymentDayType;
                                cpEnt.HMCP_UNIT_ROUND_DECIMAL = hmc_detail.RoundDecimal;
                                cpEnt.HMCP_UNIT_ROUND_TYPE = hmc_detail.RoundType;
                                cpEnt.HMCP_DESC = hmc_detail.Description;
                                cpEnt.HMCP_REASON = hmc_detail.Reason;
                                cpEnt.HMCP_STATUS = hmc_detail.Status;
                                cpEnt.HMCP_CREATED_DATE = ShareFn.ConvertStrDateToDate(cpExist.CreateDate, "yyyyMMdd HHmmss");
                                cpEnt.HMCP_CREATED_BY = cpExist.CreateBy;
                                cpEnt.HMCP_UPDATED_DATE = now;
                                cpEnt.HMCP_UPDATED_BY = pUser;
                                cpEnt.HMCP_STATUS_TRADE = hmc_detail.StatusTrade;
                                cpDal.UpdateFromExcel(cpEnt, context);

                                //2Log HEDG_MT_CP_HISTORY
                                string cpHisCtrlID = ConstantPrm.GetDynamicCtrlID();
                                saveToHis(cpHisCtrlID, cpExist.HMCP_ROW_ID, "UploadExcelFile", pUser, context);

                                //3Log HEDG_MT_CP_HISTORY_LOG CreditRating CreditLimit has been Changed
                                if (cpExist.CreditLimit != creditData.Amount)
                                {
                                    saveToHisLog(cpHisCtrlID, lastLog.ToString(), "Limit", cpExist.CreditLimit, creditData.Amount, null, pUser, context);
                                    lastLog++;
                                }
                                if (cpExist.CreditLimitUpdate == "N" && creditDataOld.Rating != hmc_detail.CreditRating)
                                {
                                    saveToHisLog(cpHisCtrlID, lastLog.ToString(), "Rating", creditDataOld.Rating, hmc_detail.CreditRating, null, pUser, context);
                                    lastLog++;
                                }

                                foreach (var itemData in itemDataList)
                                {
                                    //4Insert HEDG_MT_CP_CDS
                                    cpCdsEnt = new HEDG_MT_CP_CDS();
                                    string cpCdsCtrlID = ConstantPrm.GetDynamicCtrlID();
                                    cpCdsEnt.HMCD_ROW_ID = cpCdsCtrlID;
                                    cpCdsEnt.HMCD_FK_HEDG_MT_CP = cpCtrlID;
                                    cpCdsEnt.HMCD_FK_HEDG_MT_CDS_FILE = fileCtrlID;
                                    cpCdsEnt.HMCD_CDS_DATE = ShareFn.ConvertStringDateFormatToDatetime(itemData.CdsDate, "yyyyMMdd");
                                    cpCdsEnt.HMCD_CDS_VALUE = itemData.CdsValue;
                                    cpCdsEnt.HMCD_STATUS = CPAIConstantUtil.ACTIVE;
                                    cpCdsEnt.HMCD_CREATED_DATE = now;
                                    cpCdsEnt.HMCD_CREATED_BY = pUser;
                                    cpCdsEnt.HMCD_UPDATED_DATE = now;
                                    cpCdsEnt.HMCD_UPDATED_BY = pUser;
                                    cpCdsDal.Save(cpCdsEnt, context);
                                }

                                //XLog HEDG_MT_CP_HISTORY_LOG Add Item
                                saveToHisLog(cpHisCtrlID, lastLog.ToString(), "Cds", ShareFn.ConvertDateTimeToDateStringFormat(ShareFn.ConvertStringDateFormatToDatetime(hmc_detail.LastCdsDate, "yyyyMMdd"), "dd/MM/yyyy"), itemCount.ToString(), null, pUser, context);
                                lastLog++;

                                CDSService CDSService = new CDSService();
                                ResultCheckCDS rCDS = CDSService.CheckCDSConfigByID(cpCtrlID, cpHisCtrlID, pUser, context);
                                if (rCDS.result)
                                    saveToHisLog(cpHisCtrlID, lastLog.ToString(), "CheckCDS", rCDS.statusOld, rCDS.statusNew, rCDS.statusMessage, pUser, context);

                                result = "Completed | Add " + itemCount + " ROW";
                            }
                            else
                            {
                                result = "Completed | Old Data";
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public List<HedgeCDSImport_SearchData> getCdsLogList(string sLimit)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var query = (from v in context.HEDG_MT_CP_CDS_FILE
                                 orderby v.HMCF_IMPORTED_DATE descending
                                 select new HedgeCDSImport_SearchData
                                 {
                                     dID = v.HMCF_ROW_ID,
                                     dFileName = v.HMCF_FILE_NAME,
                                     dFilePath = v.HMCF_FILE_PATH,
                                     dResult = v.HMCF_RESULT,
                                     dNote = v.HMCF_NOTE,
                                     dImportDate = v.HMCF_IMPORTED_DATE,
                                     dImportBy = v.HMCF_IMPORTED_BY
                                 });
                    var result = query.ToList();

                    List<HedgeCDSImport_SearchData> arr_data = new List<HedgeCDSImport_SearchData>();

                    if (query != null)
                    {
                        if (!string.IsNullOrEmpty(sLimit))
                            query = query.Take(Convert.ToInt32(sLimit));

                        foreach (var g in query)
                        {
                            arr_data.Add(
                                                    new HedgeCDSImport_SearchData
                                                    {
                                                        dID = g.dID,
                                                        dFileName = g.dFileName,
                                                        dFilePath = g.dFilePath,
                                                        dResult = g.dResult,
                                                        dNote = g.dNote,
                                                        dImportDateString = ShareFn.ConvertDateTimeToDateStringFormat(g.dImportDate, "dd/MM/yyyy HH:mm"),
                                                        dImportBy = g.dImportBy
                                                    });
                        }
                    }
                    return arr_data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Class

        public static List<HedgingCDSImportViewModel_Detail> getAllCounterParty()
        {
            List<HedgingCDSImportViewModel_Detail> rtn = new List<HedgingCDSImportViewModel_Detail>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from c in context.HEDG_MT_CP
                            select new
                            {
                                HMCP_ROW_ID = c.HMCP_ROW_ID,
                                HMCP_FK_VENDOR = c.HMCP_FK_VENDOR,
                                HMCP_NAME = c.HMCP_NAME,
                                HMCP_FULL_NAME = c.HMCP_FULL_NAME,
                                HMCP_MAPPING_CDS_CP = c.HMCP_MAPPING_CDS_CP,
                                HMCP_LAST_CDS_DATE = c.HMCP_LAST_CDS_DATE,
                                HMCP_LAST_CDS_VALUE = c.HMCP_LAST_CDS_VALUE,
                                HMCP_CREDIT_RATING = c.HMCP_CREDIT_RATING,
                                HMCP_CREDIT_LIMIT = c.HMCP_CREDIT_LIMIT,
                                HMCP_CREDIT_LIMIT_UPDATE = c.HMCP_CREDIT_LIMIT_UPDATE,
                                HMCP_PAYMENT_DAY = c.HMCP_PAYMENT_DAY,
                                HMCP_PAYMENT_DAY_TYPE = c.HMCP_PAYMENT_DAY_TYPE,
                                HMCP_UNIT_ROUND_DECIMAL = c.HMCP_UNIT_ROUND_DECIMAL,
                                HMCP_UNIT_ROUND_TYPE = c.HMCP_UNIT_ROUND_TYPE,
                                HMCP_DESC = c.HMCP_DESC,
                                HMCP_REASON = c.HMCP_REASON,
                                HMCP_STATUS = c.HMCP_STATUS,
                                HMCP_CREATED_DATE = c.HMCP_CREATED_DATE,
                                HMCP_CREATED_BY = c.HMCP_CREATED_BY,
                                HMCP_UPDATED_DATE = c.HMCP_UPDATED_DATE,
                                HMCP_UPDATED_BY = c.HMCP_UPDATED_BY,
                                HMCP_STATUS_TRADE = c.HMCP_STATUS_TRADE
                            };

                if (query != null)
                {
                    foreach (var g in query)
                    {
                        var queryDetail = from c in context.HEDG_MT_CP_CDS
                                          where c.HMCD_FK_HEDG_MT_CP == g.HMCP_ROW_ID
                                          select new
                                          {
                                              HMCD_ROW_ID = c.HMCD_ROW_ID,
                                              HMCD_FK_HEDG_MT_CP = c.HMCD_FK_HEDG_MT_CP,
                                              HMCD_FK_HEDG_MT_CDS_FILE = c.HMCD_FK_HEDG_MT_CDS_FILE,
                                              HMCD_CDS_DATE = c.HMCD_CDS_DATE,
                                              HMCD_CDS_VALUE = c.HMCD_CDS_VALUE,
                                              HMCD_STATUS = c.HMCD_STATUS,
                                              HMCD_CREATED_DATE = c.HMCD_CREATED_DATE,
                                              HMCD_CREATED_BY = c.HMCD_CREATED_BY,
                                              HMCD_UPDATED_DATE = c.HMCD_UPDATED_DATE,
                                              HMCD_UPDATED_BY = c.HMCD_UPDATED_BY
                                          };

                        List<HedgingCDSImportViewModel_DetailCds> rtnData = new List<HedgingCDSImportViewModel_DetailCds>();
                        foreach (var item in queryDetail)
                        {
                            rtnData.Add(new HedgingCDSImportViewModel_DetailCds
                            {
                                HMCD_ROW_ID = item.HMCD_ROW_ID,
                                HMCD_FK_HEDG_MT_CP = item.HMCD_FK_HEDG_MT_CP,
                                FileID = item.HMCD_FK_HEDG_MT_CDS_FILE,
                                CdsDate = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_CDS_DATE, "yyyyMMdd HHmmss"),
                                CdsValue = item.HMCD_CDS_VALUE,
                                Status = item.HMCD_STATUS,
                                CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_CREATED_DATE, "yyyyMMdd HHmmss"),
                                CreateBy = item.HMCD_CREATED_BY,
                                UpdateDate = ShareFn.ConvertDateTimeToDateStringFormat(item.HMCD_UPDATED_DATE, "yyyyMMdd HHmmss"),
                                UpdateBy = item.HMCD_UPDATED_BY
                            });
                        }

                        rtn.Add(new HedgingCDSImportViewModel_Detail
                        {
                            HMCP_ROW_ID = g.HMCP_ROW_ID,
                            CouterPartyName = g.HMCP_NAME,
                            CouterPartyFullName = g.HMCP_FULL_NAME,
                            MappingCdsCP = g.HMCP_MAPPING_CDS_CP,
                            CouterPartyID = g.HMCP_FK_VENDOR,
                            CreditRating = g.HMCP_CREDIT_RATING,
                            CreditLimit = g.HMCP_CREDIT_LIMIT,
                            CreditLimitUpdate = g.HMCP_CREDIT_LIMIT_UPDATE,
                            Status = g.HMCP_STATUS_TRADE,
                            Reason = g.HMCP_REASON,
                            CreateDate = ShareFn.ConvertDateTimeToDateStringFormat(g.HMCP_CREATED_DATE, "yyyyMMdd HHmmss"),
                            CreateBy = g.HMCP_CREATED_BY,
                            UpdateDate = ShareFn.ConvertDateTimeToDateStringFormat(g.HMCP_UPDATED_DATE, "yyyyMMdd HHmmss"),
                            UpdateBy = g.HMCP_UPDATED_BY,
                            DetailCds = rtnData
                        });
                    }
                }
            }
            return rtn;
        }

        public static List<HedgingCDSImportViewModel_CdsMax> getCPDateControl()
        {
            List<HedgingCDSImportViewModel_CdsMax> rtn = new List<HedgingCDSImportViewModel_CdsMax>();

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.HEDG_MT_CP
                            select new
                            { VendorID = v.HMCP_FK_VENDOR, Date = v.HMCP_LAST_CDS_DATE };

                query = from p in query
                        group p by p.VendorID into g
                        select new { VendorID = g.Key, Date = g.Max(d => d.Date) };

                if (query.Count() != 0)
                {
                    foreach (var g in query)
                    {
                        rtn.Add(new HedgingCDSImportViewModel_CdsMax
                        {
                            VendorID = g.VendorID,
                            Date = g.Date != null ? g.Date.Value.ToString("yyyyMMdd") : "n/a"
                        });
                    }
                }
            }
            return rtn;
        }

        private DateTime parseInputDate(string date)
        {
            try
            {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Class UnUsed
        //public static List<DateTime?> getAllHoliday()
        //{
        //    List<DateTime?> rtn = new List<DateTime?>();

        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        var query = from c in context.MT_HOLIDAY
        //                    select new
        //                    {
        //                        MH_HOL_DATE = c.MH_HOL_DATE,
        //                    };

        //        foreach (var item in query.ToList())
        //        {
        //            rtn.Add(item.MH_HOL_DATE);
        //        }
        //    }
        //    return rtn;

        //}

        //public string genCPDateForAddControl(string pDate, string cpID)
        //{
        //    string rtn = "";
        //    DateTime now = DateTime.Now.Date;
        //    if (!string.IsNullOrEmpty(pDate))
        //        now = DateTime.ParseExact(pDate, "dd/MM/yyyy", null).AddDays(1);

        //    List<DateTime?> holiday = getAllHoliday();
        //    DateTime? cdsMax = ShareFn.ConvertStringDateFormatToDatetime(getCPDateForAddControl(cpID).Select(x => x.Date).FirstOrDefault(), "yyyyMMdd");

        //    int countHoliday = holiday.Where(x => x.Value == now).Count();
        //    while (countHoliday != 0 || now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday || cdsMax >= now)
        //    {
        //        now = now.AddDays(1);
        //        countHoliday = holiday.Where(x => x.Value == now).Count();
        //    }
        //    rtn = ShareFn.ConvertDateTimeToDateStringFormat(now, "dd/MM/yyyy");

        //    return rtn;
        //}

        //public static List<HedgingCDSImportViewModel_CdsMax> getCPDateForAddControl(string cpID)
        //{

        //    List<HedgingCDSImportViewModel_CdsMax> rtn = new List<HedgingCDSImportViewModel_CdsMax>();

        //    using (EntityCPAIEngine context = new EntityCPAIEngine())
        //    {
        //        var query = from v in context.HEDG_MT_CP
        //                    join vc in context.HEDG_MT_CP_CDS on v.HMCP_ROW_ID equals vc.HMCD_FK_HEDG_MT_CP
        //                    select new
        //                    { HMCP_ROW_ID = v.HMCP_ROW_ID, Date = vc.HMCD_CDS_DATE };

        //        if (!string.IsNullOrEmpty(cpID))
        //            query = query.Where(p => p.HMCP_ROW_ID.Equals(cpID));

        //        query = from p in query
        //                group p by p.HMCP_ROW_ID into g
        //                select new { HMCP_ROW_ID = g.Key, Date = g.Max(d => d.Date) };

        //        if (query.Count() != 0)
        //        {
        //            foreach (var g in query)
        //            {
        //                rtn.Add(new HedgingCDSImportViewModel_CdsMax
        //                {
        //                    VendorID = g.HMCP_ROW_ID,
        //                    Date = g.Date != null ? g.Date.Value.ToString("yyyyMMdd") : "n/a"
        //                });
        //            }
        //        }
        //    }
        //    return rtn;
        //}

        //public ReturnValue ValidateInput(ref HedgingCDSImportViewModel_Detail pModel, string pUser)
        //{
        //    ShareFn shareFn = new ShareFn();
        //    ReturnValue rtn = new ReturnValue();
        //    rtn.Status = true;
        //    rtn.Message = string.Empty;

        //    if (string.IsNullOrEmpty(pUser))
        //    {
        //        rtn.Message = "User should not be empty";
        //        rtn.Status = false;
        //    }
        //    else if (pModel == null)
        //    {
        //        rtn.Message = "Model is not null";
        //        rtn.Status = false;
        //    }
        //    else if (string.IsNullOrEmpty(pModel.Status))
        //    {
        //        rtn.Message = "Status should not be empty";
        //        rtn.Status = false;
        //    }
        //    else if (pModel.DetailCreditLimit != null)
        //    {
        //        bool check = false;
        //        for (int index = 0; index < pModel.DetailCreditLimit.Count(); index++)
        //        {
        //            string fromDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[index].dDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
        //            string toDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[index].dDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
        //            DateTime fDate = parseInputDate(fromDate);
        //            DateTime tDate = parseInputDate(toDate);
        //            for (int i = 0; i < pModel.DetailCreditLimit.Count(); i++)
        //            {
        //                fromDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[i].dDate.ToUpper().Substring(0, 10), true).Replace("-", "/");
        //                toDate = shareFn.ConvertDateFormat(pModel.DetailCreditLimit[i].dDate.ToUpper().Substring(14, 10), true).Replace("-", "/");
        //                DateTime cfDate = parseInputDate(fromDate);
        //                DateTime ctDate = parseInputDate(toDate);
        //                if (index != i)
        //                {
        //                    if (fDate >= cfDate && tDate <= ctDate || fDate <= cfDate && tDate >= ctDate)
        //                    {
        //                        rtn.Message = "Period Date is Duplicate";
        //                        rtn.Status = false;
        //                        check = true;
        //                        break;
        //                    }
        //                }
        //            }
        //            if (check) break;
        //        }
        //        if (!check)
        //        {
        //            foreach (var item in pModel.DetailCreditLimit)
        //            {
        //                if (string.IsNullOrEmpty(item.dDate))
        //                {
        //                    rtn.Message = "Period Date should not be empty";
        //                    rtn.Status = false;
        //                    break;
        //                }
        //                else if (string.IsNullOrEmpty(item.CreditLimit))
        //                {
        //                    rtn.Message = "Period CreditLimit should not be empty";
        //                    rtn.Status = false;
        //                    break;
        //                }
        //                else if (string.IsNullOrEmpty(item.Status))
        //                {
        //                    rtn.Message = "Period Status should not be empty";
        //                    rtn.Status = false;
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    else if (pModel.DetailCds != null)
        //    {
        //        foreach (var item in pModel.DetailCds)
        //        {
        //            if (string.IsNullOrEmpty(item.CdsDate))
        //            {
        //                rtn.Message = "CDS Date should not be empty";
        //                rtn.Status = false;
        //                break;
        //            }
        //            else if (string.IsNullOrEmpty(item.CdsValue))
        //            {
        //                rtn.Message = "CDS Value should not be empty";
        //                rtn.Status = false;
        //                break;
        //            }
        //        }
        //    }
        //    return rtn;
        //}

        //public static string getCounterAdditional(string cp)
        //{
        //    string result = "";
        //    if (string.IsNullOrEmpty(cp) == false)
        //    {
        //        using (EntityCPAIEngine context = new EntityCPAIEngine())
        //        {
        //            var query = from v in context.HEDG_MT_CP
        //                        where
        //                        v.HMCP_FK_VENDOR.Equals(cp)
        //                        select new
        //                        {
        //                            HMCP_ROW_ID = v.HMCP_ROW_ID,
        //                            HMCP_CREDIT_LIMIT = v.HMCP_CREDIT_LIMIT,
        //                            HMCP_CREDIT_RATING = v.HMCP_CREDIT_RATING,
        //                        };
        //            if (query.Count() != 0)
        //            {
        //                var query2 = query.ToList().FirstOrDefault();
        //                result = "Credit Rating: " + query2.HMCP_CREDIT_RATING + " Credit Remaining: " + query2.HMCP_CREDIT_LIMIT + " THB";
        //            }
        //        }
        //    }
        //    return result;
        //}
        #endregion
    }
}