﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeCounterpartyEntryServiceModel
    {
        public ReturnValue Add(HedgeCounterpartyEntryViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                HEDG_MT_CP_DAL dal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP ent = new HEDG_MT_CP();

                HEDG_MT_CP_LIMIT limit = new HEDG_MT_CP_LIMIT();
                HEDG_MT_CP_LIMIT_DAL limitDal = new HEDG_MT_CP_LIMIT_DAL();

                HEDG_MT_CP_HISTORY hist = new HEDG_MT_CP_HISTORY();
                HEDG_MT_CP_HISTORY_DAL histDal = new HEDG_MT_CP_HISTORY_DAL();

               

                EntityCPAIEngine context = new EntityCPAIEngine();

                if (pModel==null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.counterParty))
                {
                    rtn.Message = "Counter party should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if(String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyName))
                {
                    rtn.Message = "Counter party name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.statustrading))
                {
                    rtn.Message = "Status Trad should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyFullName))
                {
                    rtn.Message = "Counter party full name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.mappingCDS))
                {
                    rtn.Message = "Mapping CDS Counter party should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS))
                {
                    rtn.Message = "Lastest CDS should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS_value))
                {
                    rtn.Message = "Latest CDS Value should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.creditRating) && String.IsNullOrEmpty(pModel.cpe_Detail.refer_CreditRating))
                {
                    rtn.Message = "Credit Rating should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.creditLimit))
                {
                    rtn.Message = "Credit Limit should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.paymentDay))
                {
                    rtn.Message = "Payment Day should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.roundingDecimal))
                {
                    rtn.Message = "Rounding Decimal should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if(String.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                var hasId = dal.ChkByCountryParty(pModel.cpe_Detail.counterParty);
                if (hasId)
                {
                    rtn.Message = "Counter party is already";
                    rtn.Status = false;
                    return rtn;
                }

                var hasMappingCDS = dal.ChkMappingCDS(pModel.cpe_Detail.mappingCDS);
                if (hasMappingCDS)
                {
                    rtn.Message = "Mapping CDS Counterparty is already";
                    rtn.Status = false;
                    return rtn;
                }
                var now = DateTime.Now;
                try
                {
                    #region Save to CP and CP_Limit
                    var cpId = ConstantPrm.GetDynamicCtrlID();
                    ent.HMCP_ROW_ID = cpId;
                    ent.HMCP_FK_VENDOR = pModel.cpe_Detail.counterParty;
                    ent.HMCP_NAME = pModel.cpe_Detail.counterPartyName;
                    ent.HMCP_FULL_NAME = pModel.cpe_Detail.counterPartyFullName;
                    ent.HMCP_MAPPING_CDS_CP = pModel.cpe_Detail.mappingCDS;
                    ent.HMCP_STATUS_TRADE = pModel.cpe_Detail.statustrading;
                    ent.HMCP_LAST_CDS_DATE = DateTime.ParseExact(pModel.cpe_Detail.latestCDS, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    ent.HMCP_LAST_CDS_VALUE = pModel.cpe_Detail.latestCDS_value;
                    if (pModel.cpe_Detail.creditRating_NAFlag == "on")
                    {
                        pModel.cpe_Detail.creditRating = null;
                    }
                    ent.HMCP_CREDIT_RATING = pModel.cpe_Detail.creditRating;
                    ent.HMCP_CREDIT_RATING_REF_FLAG = pModel.cpe_Detail.creditRating_NAFlag == "on" ? "Y" : "N";
                    //ent.HMCP_FK_HEDG_MT_CP_REF = pModel.cpe_Detail.refer_CreditRating;
                    ent.HMCP_CREDIT_LIMIT = pModel.cpe_Detail.creditLimit;
                    ent.HMCP_CREDIT_LIMIT_UPDATE = pModel.cpe_Detail.creditLimit_UpdateFlag == "on" ? "Y" : "N";
                    ent.HMCP_PAYMENT_DAY = pModel.cpe_Detail.paymentDay;
                    ent.HMCP_PAYMENT_DAY_TYPE = pModel.cpe_Detail.countDate;
                    ent.HMCP_UNIT_ROUND_DECIMAL = pModel.cpe_Detail.roundingDecimal;
                    ent.HMCP_UNIT_ROUND_TYPE = pModel.cpe_Detail.status_roundingDecimal == "AC" ? "0" : "1";
                    ent.HMCP_DESC = pModel.cpe_Detail.description ?? "";
                    ent.HMCP_REASON = pModel.cpe_History.reason ?? "Create Counterparty Entry";
                    ent.HMCP_STATUS = pModel.cpe_Detail.status;
                    ent.HMCP_VERSION = "1";
                    ent.HMCP_CREATED_DATE = now;
                    ent.HMCP_CREATED_BY = pUser;
                    ent.HMCP_UPDATED_DATE = now;
                    ent.HMCP_UPDATED_BY = pUser;

                    dal.Save(ent);

                    if (pModel.cpe_Detail.creditLimitSpecifyPeriod != null && pModel.cpe_Detail.creditLimitSpecifyPeriod.Any())
                    {
                        foreach (var item in pModel.cpe_Detail.creditLimitSpecifyPeriod)
                        {
                            var limitId = ConstantPrm.GetDynamicCtrlID();
                            string[] period_date = item.period.Split(new[] { " to " }, StringSplitOptions.None);
                            var date_start = period_date[0];
                            var date_end = period_date[1];

                            limit = new HEDG_MT_CP_LIMIT();
                            limit.HMCL_ROW_ID = limitId;
                            limit.HMCL_FK_HEDG_MT_CP = cpId;
                            limit.HMCL_DATE_START = DateTime.ParseExact(date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            limit.HMCL_DATE_END = DateTime.ParseExact(date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            limit.HMCL_CREDIT_LIMIT_AMT = item.credit_amount;
                            limit.HMCL_NOTE = item.note;
                            limit.HMCL_STATUS = item.status;
                            limit.HMCL_CREATED_DATE = now;
                            limit.HMCL_CREATED_BY = pUser;
                            limit.HMCL_UPDATED_DATE = now;
                            limit.HMCL_UPDATED_BY = pUser;
                            limitDal.Save(limit);
                        }
                    }
                    #endregion

                    #region Save History and History Log
                    var histId = ConstantPrm.GetDynamicCtrlID();
                    hist = new HEDG_MT_CP_HISTORY();
                    hist.HMCH_ROW_ID = histId;
                    hist.HMCH_FK_HEDG_MT_CP = cpId;
                    hist.HMCH_REASON = pModel.cpe_History.reason ?? "Create Counterparty Entry";
                    hist.HMCH_CREATED_DATE = now;
                    hist.HMCH_CREATED_BY = pUser;
                    hist.HMCH_UPDATED_DATE = now;
                    hist.HMCH_UPDATED_BY = pUser;
                    histDal.Save(hist, context);

                    SaveLog(pModel, histId, pUser);
                    #endregion


                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {

                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgeCounterpartyEntry_Detail Get(string EntryId)
        {
            HedgeCounterpartyEntry_Detail model = new HedgeCounterpartyEntry_Detail();
            if (!String.IsNullOrEmpty(EntryId))
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = context.HEDG_MT_CP.Where(c => c.HMCP_ROW_ID == EntryId).SingleOrDefault();
                    if (query !=null)
                    {
                        var limit = (from l in context.HEDG_MT_CP_LIMIT
                                     where l.HMCL_FK_HEDG_MT_CP == query.HMCP_ROW_ID
                                     select l).ToList();

                        List<Credit_Limit_Specify_Period> limitList = new List<Credit_Limit_Specify_Period>();
                        if (limit!=null&& limit.Any())
                        {
                            foreach (var item in limit)
                            {
                                Credit_Limit_Specify_Period lim = new Credit_Limit_Specify_Period();
                                lim.credit_limit_code = item.HMCL_ROW_ID;
                                lim.period = item.HMCL_DATE_START.Value.ToString("dd/MM/yyyy") + " to " + item.HMCL_DATE_END.Value.ToString("dd/MM/yyyy");
                                lim.credit_amount = item.HMCL_CREDIT_LIMIT_AMT;
                                lim.note = item.HMCL_NOTE;
                                lim.updated_by = item.HMCL_UPDATED_BY;
                                lim.updated_date = item.HMCL_UPDATED_DATE.ToString("dd/MM/yyyy");
                                lim.status = item.HMCL_STATUS;
                                limitList.Add(lim);
                            }
                        }


                        var cds = context.HEDG_MT_CP_CDS.Where(c => c.HMCD_FK_HEDG_MT_CP == query.HMCP_ROW_ID).ToList();
                        List<CDS> cdsList = new List<CDS>();
                        if (cds!=null&&cds.Any())
                        {
                            foreach (var item in cds)
                            {
                                CDS c = new CDS();
                                c.date = item.HMCD_CDS_DATE.Value.ToString("dd/MM/yyyy");
                                c.value = item.HMCD_CDS_VALUE;
                                c.updated_date = item.HMCD_UPDATED_DATE.ToString("dd/MM/yyyy");
                                c.updated_by = item.HMCD_UPDATED_BY;
                                cdsList.Add(c);
                            }
                        }


                        var history = context.HEDG_MT_CP_HISTORY.Where(c => c.HMCH_FK_HEDG_MT_CP == query.HMCP_ROW_ID).ToList();
                        List<History> historyList = new List<History>();
                        if (history != null && history.Any())
                        {
                            foreach (var item in history)
                            {
                                History hist = new History();
                                hist.history_id = item.HMCH_ROW_ID;
                                hist.date = item.HMCH_UPDATED_DATE.ToString("dd/MM/yyyy");
                                hist.by = item.HMCH_UPDATED_BY;
                                hist.reason = item.HMCH_REASON;
                                historyList.Add(hist);
                            }
                        }

                        if (historyList != null && historyList.Any())
                        {
                            var version = Convert.ToInt32(query.HMCP_VERSION);
                            foreach (var item in historyList)
                            {
                                item.version = version.ToString();
                                version--;
                            }

                        }

                       


                        model.row_id = query.HMCP_ROW_ID;
                        model.version = query.HMCP_VERSION;
                        model.counterParty = query.HMCP_FK_VENDOR;
                        model.statustrading = query.HMCP_STATUS_TRADE;
                        model.counterPartyName = query.HMCP_NAME;
                        model.counterPartyFullName = query.HMCP_FULL_NAME;
                        model.mappingCDS = query.HMCP_MAPPING_CDS_CP;
                        model.latestCDS = query.HMCP_LAST_CDS_DATE.HasValue ? query.HMCP_LAST_CDS_DATE.Value.ToString("dd/MM/yyyy") : string.Empty;
                        model.latestCDS_value = query.HMCP_LAST_CDS_VALUE;
                        model.creditRating = query.HMCP_CREDIT_RATING != null ? query.HMCP_CREDIT_RATING : "";
                        model.creditRating_NAFlag = query.HMCP_CREDIT_RATING_REF_FLAG == "Y" ? "on" : "off";
                        model.creditRating_NAFlag_h = query.HMCP_CREDIT_RATING_REF_FLAG == "Y" ? "on" : "off";
                        //model.refer_CreditRating = query.HMCP_FK_HEDG_MT_CP_REF;
                        model.creditLimit = query.HMCP_CREDIT_LIMIT;
                        model.creditLimit_UpdateFlag = query.HMCP_CREDIT_LIMIT_UPDATE == "Y" ? "on" : "off";
                        model.paymentDay = query.HMCP_PAYMENT_DAY;
                        model.countDate = query.HMCP_PAYMENT_DAY_TYPE;
                        model.roundingDecimal = query.HMCP_UNIT_ROUND_DECIMAL;
                        model.status_roundingDecimal = query.HMCP_UNIT_ROUND_TYPE;
                        model.status = query.HMCP_STATUS;
                        model.description = query.HMCP_DESC;
                        model.creditLimitSpecifyPeriod = limitList;
                        model.history = historyList;
                        model.cds = cdsList;
                    }
                    return model;
                }
            }
            else
            {
                return null;
            }
        }

        public ReturnValue Edit(HedgeCounterpartyEntryViewModel pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                HEDG_MT_CP_DAL dal = new HEDG_MT_CP_DAL();
                HEDG_MT_CP ent = new HEDG_MT_CP();

                HEDG_MT_CP_LIMIT limit = new HEDG_MT_CP_LIMIT();
                HEDG_MT_CP_LIMIT_DAL limitDal = new HEDG_MT_CP_LIMIT_DAL();

                HEDG_MT_CP_HISTORY hist = new HEDG_MT_CP_HISTORY();
                HEDG_MT_CP_HISTORY_DAL histDal = new HEDG_MT_CP_HISTORY_DAL();

                EntityCPAIEngine context = new EntityCPAIEngine();
                #region check null
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.counterParty))
                {
                    rtn.Message = "Counter party should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyName))
                {
                    rtn.Message = "Counter party name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.statustrading))
                {
                    rtn.Message = "Status Trad should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyFullName))
                {
                    rtn.Message = "Counter party full name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.mappingCDS))
                {
                    rtn.Message = "Mapping CDS Counter party should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS))
                {
                    rtn.Message = "Lastest CDS should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS_value))
                {
                    rtn.Message = "Latest CDS Value should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.creditRating) && String.IsNullOrEmpty(pModel.cpe_Detail.refer_CreditRating))
                {
                    rtn.Message = "Credit Rating should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.creditLimit))
                {
                    rtn.Message = "Credit Limit should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.paymentDay))
                {
                    rtn.Message = "Payment Day should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pModel.cpe_Detail.roundingDecimal))
                {
                    rtn.Message = "Rounding Decimal should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (String.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                #endregion
                var now = DateTime.Now;

                try
                {
                    #region Save History and History Log
                    var histId = ConstantPrm.GetDynamicCtrlID();
                    hist = new HEDG_MT_CP_HISTORY();
                    hist.HMCH_ROW_ID = histId;
                    hist.HMCH_FK_HEDG_MT_CP = pModel.cpe_Detail.row_id;
                    hist.HMCH_REASON = pModel.cpe_History.reason ?? "";
                    hist.HMCH_CREATED_DATE = now;
                    hist.HMCH_CREATED_BY = pUser;
                    hist.HMCH_UPDATED_DATE = now;
                    hist.HMCH_UPDATED_BY = pUser;
                    histDal.Save(hist, context);

                    SaveLog(pModel, histId, pUser);
                    #endregion






                    #region Update to CP and CP_Limit
                   
                    var cpData = context.HEDG_MT_CP.Where(c => c.HMCP_ROW_ID == pModel.cpe_Detail.row_id).SingleOrDefault();
                    var version = Convert.ToInt32(cpData.HMCP_VERSION) + 1;
                    ent.HMCP_ROW_ID = pModel.cpe_Detail.row_id;
                    ent.HMCP_FK_VENDOR = String.IsNullOrEmpty(pModel.cpe_Detail.counterParty) ? cpData.HMCP_FK_VENDOR : pModel.cpe_Detail.counterParty;
                    ent.HMCP_NAME = String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyName) ? cpData.HMCP_NAME : pModel.cpe_Detail.counterPartyName;
                    ent.HMCP_FULL_NAME = String.IsNullOrEmpty(pModel.cpe_Detail.counterPartyFullName) ? cpData.HMCP_FULL_NAME : pModel.cpe_Detail.counterPartyFullName;
                    ent.HMCP_MAPPING_CDS_CP = String.IsNullOrEmpty(pModel.cpe_Detail.mappingCDS) ? cpData.HMCP_MAPPING_CDS_CP : pModel.cpe_Detail.mappingCDS;
                    ent.HMCP_STATUS_TRADE = String.IsNullOrEmpty(pModel.cpe_Detail.statustrading) ? cpData.HMCP_STATUS_TRADE : pModel.cpe_Detail.statustrading;
                    ent.HMCP_LAST_CDS_DATE = String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS) ? cpData.HMCP_LAST_CDS_DATE : DateTime.ParseExact(pModel.cpe_Detail.latestCDS, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    ent.HMCP_LAST_CDS_VALUE = String.IsNullOrEmpty(pModel.cpe_Detail.latestCDS_value) ? cpData.HMCP_LAST_CDS_VALUE : pModel.cpe_Detail.latestCDS_value;
                    ent.HMCP_CREDIT_RATING = pModel.cpe_Detail.creditRating;
                    if (pModel.cpe_Detail.creditRating_NAFlag==null)
                    {
                        if (pModel.cpe_Detail.creditRating_NAFlag_h!=null)
                        {
                            ent.HMCP_CREDIT_RATING_REF_FLAG = pModel.cpe_Detail.creditRating_NAFlag_h == "on" ? "Y" : "N";
                        }
                        else
                        {
                            ent.HMCP_CREDIT_RATING_REF_FLAG = cpData.HMCP_CREDIT_RATING_REF_FLAG;
                        }                    
                    }
                    else
                    {
                        ent.HMCP_CREDIT_RATING_REF_FLAG = pModel.cpe_Detail.creditRating_NAFlag == "on" ? "Y" : "N";
                    }
                    //ent.HMCP_FK_HEDG_MT_CP_REF = pModel.cpe_Detail.refer_CreditRating;
                    ent.HMCP_CREDIT_LIMIT = String.IsNullOrEmpty(pModel.cpe_Detail.creditLimit) ? cpData.HMCP_CREDIT_LIMIT : pModel.cpe_Detail.creditLimit;
                    ent.HMCP_CREDIT_LIMIT_UPDATE = String.IsNullOrEmpty(pModel.cpe_Detail.creditLimit_UpdateFlag) ? cpData.HMCP_CREDIT_LIMIT_UPDATE : pModel.cpe_Detail.creditLimit_UpdateFlag == "on" ? "Y" : "N";
                    ent.HMCP_PAYMENT_DAY = String.IsNullOrEmpty(pModel.cpe_Detail.paymentDay) ? cpData.HMCP_PAYMENT_DAY : pModel.cpe_Detail.paymentDay;
                    ent.HMCP_PAYMENT_DAY_TYPE = String.IsNullOrEmpty(pModel.cpe_Detail.countDate) ? cpData.HMCP_PAYMENT_DAY_TYPE : pModel.cpe_Detail.countDate;
                    ent.HMCP_UNIT_ROUND_DECIMAL = String.IsNullOrEmpty(pModel.cpe_Detail.roundingDecimal) ? cpData.HMCP_UNIT_ROUND_DECIMAL : pModel.cpe_Detail.roundingDecimal;
                    ent.HMCP_UNIT_ROUND_TYPE = String.IsNullOrEmpty(pModel.cpe_Detail.status_roundingDecimal) ? cpData.HMCP_UNIT_ROUND_TYPE : pModel.cpe_Detail.status_roundingDecimal == "AC" ? "0" : "1";
                    ent.HMCP_DESC = String.IsNullOrEmpty(pModel.cpe_Detail.description) ? cpData.HMCP_DESC : pModel.cpe_Detail.description ?? "";
                    ent.HMCP_REASON = String.IsNullOrEmpty(pModel.cpe_History.reason) ? cpData.HMCP_REASON : pModel.cpe_History.reason;
                    ent.HMCP_VERSION = version.ToString();
                    ent.HMCP_STATUS = String.IsNullOrEmpty(pModel.cpe_Detail.status) ? cpData.HMCP_STATUS : pModel.cpe_Detail.status;
                    ent.HMCP_UPDATED_DATE = now;
                    ent.HMCP_UPDATED_BY = pUser;
                    dal.Update(ent, context);


                    if (pModel.cpe_Detail.creditLimitSpecifyPeriod != null && pModel.cpe_Detail.creditLimitSpecifyPeriod.Any())
                    {
                        foreach (var item in pModel.cpe_Detail.creditLimitSpecifyPeriod)
                        {
                            if (item.credit_limit_code == null)
                            {
                                var limitId = ConstantPrm.GetDynamicCtrlID();
                                string[] period_date = item.period.Split(new[] { " to " }, StringSplitOptions.None);
                                var date_start = period_date[0];
                                var date_end = period_date[1];

                                limit = new HEDG_MT_CP_LIMIT();
                                limit.HMCL_ROW_ID = limitId;
                                limit.HMCL_FK_HEDG_MT_CP = pModel.cpe_Detail.row_id;
                                limit.HMCL_DATE_START = DateTime.ParseExact(date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                limit.HMCL_DATE_END = DateTime.ParseExact(date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                limit.HMCL_CREDIT_LIMIT_AMT = item.credit_amount;
                                limit.HMCL_NOTE = item.note;
                                limit.HMCL_STATUS = item.status;
                                limit.HMCL_CREATED_DATE = now;
                                limit.HMCL_CREATED_BY = pUser;
                                limit.HMCL_UPDATED_DATE = now;
                                limit.HMCL_UPDATED_BY = pUser;
                                limitDal.Save(limit); //Change to Update
                            }
                            else
                            {
                                var creditLimit = context.HEDG_MT_CP_LIMIT.Where(c => c.HMCL_ROW_ID == item.credit_limit_code).SingleOrDefault();

                                string[] period_date = item.period.Split(new[] { " to " }, StringSplitOptions.None);
                                var date_start = period_date[0];
                                var date_end = period_date[1];
                                limit = new HEDG_MT_CP_LIMIT();
                                limit.HMCL_ROW_ID = item.credit_limit_code;
                                limit.HMCL_DATE_START = DateTime.ParseExact(date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                limit.HMCL_DATE_END = DateTime.ParseExact(date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                limit.HMCL_CREDIT_LIMIT_AMT = !String.IsNullOrEmpty(item.credit_amount)?item.credit_amount:creditLimit.HMCL_CREDIT_LIMIT_AMT;
                                limit.HMCL_NOTE = !String.IsNullOrEmpty(item.note) ? item.note : creditLimit.HMCL_NOTE;
                                limit.HMCL_STATUS = !String.IsNullOrEmpty(item.status) ? item.status : creditLimit.HMCL_STATUS;                             
                                limit.HMCL_UPDATED_DATE = now;
                                limit.HMCL_UPDATED_BY = pUser;
                                limitDal.Update(limit); //Change to Update
                            }
                            
                        }
                    }
                    #endregion

                

                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {

                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        private void SaveLog(HedgeCounterpartyEntryViewModel pModel, string historyId, string pUser)
        {           
            HEDG_MT_CP_DAL dal = new HEDG_MT_CP_DAL();
            HEDG_MT_CP ent = new HEDG_MT_CP();

            HEDG_MT_CP_LIMIT limit = new HEDG_MT_CP_LIMIT();
            HEDG_MT_CP_LIMIT_DAL limitDal = new HEDG_MT_CP_LIMIT_DAL();

            HEDG_MT_CP_HISTORY_LOG histLog = new HEDG_MT_CP_HISTORY_LOG();
            HEDG_MT_CP_HISTORY_LOG_DAL histLogDal = new HEDG_MT_CP_HISTORY_LOG_DAL();

            EntityCPAIEngine context = new EntityCPAIEngine();
            List<History_Log> LogList = new List<History_Log>();
           
            if (pModel.cpe_Detail.row_id == null)
            {
                History_Log log = new History_Log();
                log.order = string.Empty;
                log.title = "Create Counterparty Entry";
                log.action = "ADD";
                log.history_note = "Create Counterparty Entry";
                LogList.Add(log);
            }
            else
            {
                HEDG_MT_CP lastData = dal.GetMtById(pModel.cpe_Detail.row_id);
                var lastCDSDate = DateTime.ParseExact(pModel.cpe_Detail.latestCDS, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (lastData.HMCP_NAME != pModel.cpe_Detail.counterPartyName)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Counterparty Name";
                    log.action = "EDIT";
                    log.history_note = "Change Counterparty Name from " + lastData.HMCP_NAME + " to " + pModel.cpe_Detail.counterPartyName + "";
                    LogList.Add(log);
                }
                if(lastData.HMCP_STATUS_TRADE != pModel.cpe_Detail.statustrading)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Status Trad";
                    log.action = "EDIT";
                    log.history_note = "Change Status Trad from " + lastData.HMCP_STATUS_TRADE + " to " + pModel.cpe_Detail.statustrading + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_STATUS != pModel.cpe_Detail.status)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Status";
                    log.action = "EDIT";
                    log.history_note = "Change Status from " + lastData.HMCP_STATUS + " to " + pModel.cpe_Detail.status + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_FULL_NAME!=pModel.cpe_Detail.counterPartyFullName)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Counterparty Entry Full Name";
                    log.action = "EDIT";
                    log.history_note = "Change Full Name from " + lastData.HMCP_FULL_NAME + " to " + pModel.cpe_Detail.counterPartyFullName + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_MAPPING_CDS_CP != pModel.cpe_Detail.mappingCDS)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Mapping CDS Counterparty";
                    log.action = "EDIT";
                    log.history_note = "Change Mapping CDS Counterparty from " + lastData.HMCP_MAPPING_CDS_CP + " to " + pModel.cpe_Detail.mappingCDS + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_CREDIT_RATING != pModel.cpe_Detail.creditRating)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Credit Rating";
                    log.action = "EDIT";
                    log.history_note = "Change Credit Rating from " + lastData.HMCP_CREDIT_RATING + " to " + pModel.cpe_Detail.creditRating + "";
                    LogList.Add(log);
                }
                //else if (lastData.HMCP_CREDIT_RATING_NA != pModel.cpe_Detail.creditRating_NAFlag)
                //{
                //    History_Log log = new History_Log();
                //    log.order = string.Empty;
                //    log.title = "Refer Credit Rating";
                //    log.action = "EDIT";
                //    log.history_note = "Change Refer Credit Rating from " + lastData.HMCP_CREDIT_RATING_NA + " to " + pModel.cpe_Detail.creditRating_NAFlag + "";
                //    LogList.Add(log);
                //}
                if (lastData.HMCP_CREDIT_LIMIT != pModel.cpe_Detail.creditLimit)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Credit Limit";
                    log.action = "EDIT";
                    log.history_note = "Change Credit Limit from " + lastData.HMCP_CREDIT_LIMIT + " usd to " + pModel.cpe_Detail.creditLimit + " usd";
                    LogList.Add(log);
                }
                if (lastData.HMCP_CREDIT_LIMIT_UPDATE != pModel.cpe_Detail.creditLimit_UpdateFlag)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Credit Limit";
                    log.action = "EDIT";
                    log.history_note = "Change Credit Limit from " + lastData.HMCP_CREDIT_LIMIT + " usd to " + pModel.cpe_Detail.creditLimit + " usd and update from import.";
                    LogList.Add(log);
                }
                if (lastData.HMCP_PAYMENT_DAY != pModel.cpe_Detail.paymentDay)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Payment Day";
                    log.action = "EDIT";
                    log.history_note = "Change Payment Day from " + lastData.HMCP_PAYMENT_DAY + " to " + pModel.cpe_Detail.paymentDay + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_PAYMENT_DAY_TYPE != pModel.cpe_Detail.countDate)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Payment Day";
                    log.action = "EDIT";
                    log.history_note = "Change Type from " + lastData.HMCP_PAYMENT_DAY_TYPE + " to " + pModel.cpe_Detail.countDate + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_LAST_CDS_DATE != lastCDSDate)
                {
                    var oldDate = lastData.HMCP_LAST_CDS_DATE.ToString();
                    var newDate = lastCDSDate.ToString();
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Latest CDS";
                    log.action = "EDIT";
                    log.history_note = "Change Latest CDS from " + oldDate + " to " + newDate + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_LAST_CDS_VALUE != pModel.cpe_Detail.latestCDS_value)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Latest CDS";
                    log.action = "EDIT";
                    log.history_note = "Change Latest CDS from " + lastData.HMCP_LAST_CDS_VALUE + " to " + pModel.cpe_Detail.latestCDS_value + "";
                    LogList.Add(log);
                }
                if (lastData.HMCP_DESC != pModel.cpe_Detail.description)
                {
                    History_Log log = new History_Log();
                    log.order = string.Empty;
                    log.title = "Description";
                    log.action = "EDIT";
                    log.history_note = "Change Description from " + lastData.HMCP_DESC + " to " + pModel.cpe_Detail.description + "";
                    LogList.Add(log);
                }
            }


            if (LogList != null && LogList.Any())
            {
                var logCount = 1;
                foreach (var item in LogList)
                {
                    item.order = logCount.ToString();
                    logCount++;
                }
                
            }


            if (LogList != null && LogList.Any())
            {
                foreach (var item in LogList)
                {
                    histLog = new HEDG_MT_CP_HISTORY_LOG();
                    histLog.HMHL_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                    histLog.HMHL_FK_CP_HISTORY = historyId;
                    histLog.HMHL_ORDER = item.order;
                    histLog.HMHL_TITLE = item.title;
                    histLog.HMHL_ACTION = item.action;
                    histLog.HMHL_NOTE = item.history_note;
                    histLog.HMHL_CREATED_BY = pUser;
                    histLog.HMHL_CREATED_DATE = DateTime.Now;
                    histLog.HMHL_UPDATED_BY = pUser;
                    histLog.HMHL_UPDATED_DATE = DateTime.Now;
                    histLogDal.Save(histLog, context);
                }
            }



        }

        public ReturnValue Search(ref HedgeCounterpartyEntry_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //Filter condition
                    var sCounterparty = pModel.scpeCounterparty == null ? "" : pModel.scpeCounterparty.ToUpper();
                    var sCreditRating = pModel.scpeCreditRating == null ? "" : pModel.scpeCreditRating.ToUpper();
                    var sCreditLimit = pModel.scpeCreditLimit == null ? "" : pModel.scpeCreditLimit.ToUpper();
                    var sCDS = pModel.scpeCDS == null ? "" : pModel.scpeCDS.ToUpper();
                    var sStatus = pModel.scpeStatus == null ? "" : pModel.scpeStatus.ToUpper();


                    //Query
                    var query = (from c in context.HEDG_MT_CP
                                 join v in context.MT_VENDOR on c.HMCP_FK_VENDOR equals v.VND_ACC_NUM_VENDOR
                                 // join r in context.MT_CREDIT_RATING on c.HMCP_CREDIT_RATING equals r.MCR_ROW_ID                                 
                                 select new HedgeCounterpartyEntry_SearchData
                                 {
                                     dEntryCode = c.HMCP_ROW_ID,
                                     dCounterparty = v.VND_NAME1,
                                     dCreditRatingId = c.HMCP_CREDIT_RATING,
                                     //dCreditRating = r.MCR_RATING,
                                     dCreditLimit = c.HMCP_CREDIT_LIMIT,
                                     dCDS = c.HMCP_LAST_CDS_VALUE,
                                     dStatusTrad = c.HMCP_STATUS_TRADE, //HMCP_STATUS_TRAD
                                     dStatus = c.HMCP_STATUS,
                                     dReason = c.HMCP_REASON,
                                     dUpdatedBy = c.HMCP_UPDATED_BY,
                                     dUpdatedDate = c.HMCP_UPDATED_DATE
                                 });


                   // var query = context.HEDG_MT_CP.ToList();



                    //query by filter
                    if (!string.IsNullOrEmpty(sCounterparty))
                        query = query.Where(p => p.dCounterparty.ToUpper().Contains(sCounterparty));

                    if (!string.IsNullOrEmpty(sCreditRating))
                        query = query.Where(p => p.dCreditRatingId.ToUpper().Contains(sCreditRating));

                    if (!string.IsNullOrEmpty(sCreditLimit))
                        query = query.Where(p => p.dCreditLimit.ToUpper().Contains(sCreditLimit));

                    if (!string.IsNullOrEmpty(sCDS))
                    {
                        query = query.Where(p => p.dCDS.ToUpper().Contains(sCDS));
                    }

                    if (!string.IsNullOrEmpty(sStatus))
                    {
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));
                    }



                    //fill data to model and return
                    if (query != null)
                    {
                        var a = query.ToList();
                       
                        //pModel.sSearchData = query.ToList();
                        pModel.searchData = new List<HedgeCounterpartyEntry_SearchData>();
                        foreach (var item in query.OrderBy(c => c.dCounterparty).ToList())
                        {
                            pModel.searchData.Add(new HedgeCounterpartyEntry_SearchData
                            {
                                dEntryCode = string.IsNullOrEmpty(item.dEntryCode) ? "" : item.dEntryCode,
                                dCounterparty = string.IsNullOrEmpty(item.dCounterparty) ? "" : item.dCounterparty,
                                dCreditRating = string.IsNullOrEmpty(item.dCreditRating) ? GetCreditRating(item.dCreditRatingId) : item.dCreditRating,
                                dCreditLimit = string.IsNullOrEmpty(item.dCreditLimit) ? "" : item.dCreditLimit,
                                dCDS = string.IsNullOrEmpty(item.dCDS) ? "" : item.dCDS,
                                dStatusTrad = string.IsNullOrEmpty(item.dStatusTrad) ? "" : item.dStatusTrad,
                                dStatus = string.IsNullOrEmpty(item.dStatus) ? "" : item.dStatus,
                                dReason = string.IsNullOrEmpty(item.dReason) ? "" : item.dReason,
                                dUpdatedBy = string.IsNullOrEmpty(item.dUpdatedBy) ? "" : item.dUpdatedBy,
                                dUpdatedDate = item.dUpdatedDate ?? null
                            });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public string GetCreditRating(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string Cr;
                if (!String.IsNullOrEmpty(id))
                {
                    Cr = context.MT_CREDIT_RATING.Where(c => c.MCR_ROW_ID == id).SingleOrDefault().MCR_RATING;                   
                }else
                {
                    Cr = "-";
                }
                return Cr;
            }
        }

        public List<History_Log> GetHistoryLog(string id)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var data = context.HEDG_MT_CP_HISTORY_LOG.Where(c => c.HMHL_FK_CP_HISTORY == id).ToList();
                List<History_Log> loglist = new List<History_Log>(); 
                if (data!=null)
                {           
                    foreach (var item in data)
                    {
                        History_Log log = new History_Log();
                        log.title = item.HMHL_TITLE;
                        log.action = item.HMHL_ACTION;
                        log.history_note = item.HMHL_NOTE;
                        log.order = item.HMHL_ORDER;
                        loglist.Add(log);
                    }
                }

                loglist.OrderBy(c => c.order).ToList();
                return loglist;
            }
        }

        public string GetRefCreditLimit(string refcreditRating)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                string data = context.HEDG_MT_CP.Where(c => c.HMCP_FK_VENDOR == refcreditRating).SingleOrDefault().HMCP_CREDIT_LIMIT;
                return data;
            }
        }

        public List<CDS> GetCDS(string date,string entryId)
        {
            CultureInfo provider = new CultureInfo("en-US");
            string[] period_date = date.Split(new[] { " to " }, StringSplitOptions.None);
            var date_start = string.Empty;
            var date_end = string.Empty;
            DateTime? dateS = null;
            DateTime? dateE = null;

            if (period_date.Count() > 1)
            {
                date_start = period_date[0];
                date_end = period_date[1];

                dateS = DateTime.ParseExact(date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateE = DateTime.ParseExact(date_end, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = context.HEDG_MT_CP.Where(c => c.HMCP_ROW_ID == entryId).SingleOrDefault();
                    var cds = context.HEDG_MT_CP_CDS.Where(c => c.HMCD_FK_HEDG_MT_CP == query.HMCP_ROW_ID
                    && c.HMCD_CDS_DATE >= dateS && c.HMCD_CDS_DATE <= dateE).ToList();
                    List<CDS> cdsList = new List<CDS>();
                    if (cds != null && cds.Any())
                    {
                        foreach (var item in cds)
                        {
                            CDS c = new CDS();
                            c.date = item.HMCD_CDS_DATE.Value.ToString("dd/MM/yyyy");
                            c.value = item.HMCD_CDS_VALUE;
                            c.updated_date = item.HMCD_UPDATED_DATE.ToString("dd/MM/yyyy");
                            c.updated_by = item.HMCD_UPDATED_BY;
                            cdsList.Add(c);
                        }
                    }
                    return cdsList;
                }
            }
            else
            {
                date_start = period_date[0];
                dateS = DateTime.ParseExact(date_start, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = context.HEDG_MT_CP.Where(c => c.HMCP_ROW_ID == entryId).SingleOrDefault();
                    var cds = context.HEDG_MT_CP_CDS.Where(c => c.HMCD_FK_HEDG_MT_CP == query.HMCP_ROW_ID
                    && c.HMCD_CDS_DATE == dateS).ToList();
                    List<CDS> cdsList = new List<CDS>();
                    if (cds != null && cds.Any())
                    {
                        foreach (var item in cds)
                        {
                            CDS c = new CDS();
                            c.date = item.HMCD_CDS_DATE.Value.ToString("dd/MM/yyyy");
                            c.value = item.HMCD_CDS_VALUE;
                            c.updated_date = item.HMCD_UPDATED_DATE.ToString("dd/MM/yyyy");
                            c.updated_by = item.HMCD_UPDATED_BY;
                            cdsList.Add(c);
                        }
                    }
                    return cdsList;
                }
            }
            


           
        }
    }
}