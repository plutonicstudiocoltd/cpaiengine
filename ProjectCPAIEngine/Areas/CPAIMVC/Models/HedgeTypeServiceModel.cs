﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HedgeTypeServiceModel
    {
        public ReturnValue Search(ref HedgeTypeViewModel_Seach pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sCode = String.IsNullOrEmpty(pModel.sHedgeTypeCode) ? "" : pModel.sHedgeTypeCode.ToUpper();
                    var sName = String.IsNullOrEmpty(pModel.sHedgeTypeName) ? "" : pModel.sHedgeTypeName.ToUpper();
                    var sDesc = String.IsNullOrEmpty(pModel.sHedgeTypeDesc) ? "" : pModel.sHedgeTypeDesc;
                    var sInventory = String.IsNullOrEmpty(pModel.sHedgeTypeInventory) ? "" : pModel.sHedgeTypeInventory.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sHedgeTypeStatus) ? "" : pModel.sHedgeTypeStatus.ToUpper();
                    var sDateFrom = String.IsNullOrEmpty(pModel.sHedgeTypeUpdatedDate) ? "" : pModel.sHedgeTypeUpdatedDate.ToUpper().Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.sHedgeTypeUpdatedDate) ? "" : pModel.sHedgeTypeUpdatedDate.ToUpper().Substring(14, 10);
                    var sUpdatedBy = String.IsNullOrEmpty(pModel.sHedgeTypeUpdatedBy) ? "" : pModel.sHedgeTypeUpdatedBy.ToUpper();


                    var query = context.HEDG_MT_HEDGE_TYPE.ToList().OrderBy(n => n.HMHT_NAME).ToList();

                    //var query = (from v in context.HEDG_MT_HEDGE_TYPE
                    //             orderby new { v.HMHT_NAME }
                    //             where v.HMHT_ROW_ID.ToUpper().Contains(sCode) && v.HMHT_NAME.ToUpper().Contains(sName)
                    //             select v);

                   // query.ToList();
                    if (!String.IsNullOrEmpty(sDesc))
                    {
                        
                        query = query.Where(c => c.HMHT_DESC == sDesc).ToList();
                        var a = query.ToList();
                    }

                    if (!String.IsNullOrEmpty(sInventory))
                    {
                        sInventory = sInventory == "YES" ? "Y" : "N";
                        query = query.Where(c => c.HMHT_INVENTORY_FLAG == sInventory).ToList();
                    }

                    if (!String.IsNullOrEmpty(sStatus))
                    {
                        query = query.Where(c => c.HMHT_STATUS == sStatus).ToList();
                    }

                    if (!string.IsNullOrEmpty(sDateFrom) && !string.IsNullOrEmpty(sDateTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDateFrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDateTo).AddDays(1).AddSeconds(-1);

                        query = query.Where(c => c.HMHT_UPDATED_DATE >= sDate && c.HMHT_UPDATED_DATE <= eDate).ToList();
                    }

                    if (!String.IsNullOrEmpty(sUpdatedBy))
                    {
                        query = query.Where(u => u.HMHT_UPDATED_BY == sUpdatedBy.ToUpper()).ToList();
                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<HedgeTypeViewModel_SeachData>();
                        foreach (var q in query)
                        {
                            pModel.sSearchData.Add(
                                new HedgeTypeViewModel_SeachData
                                {
                                    dHedgeTypeCode = q.HMHT_ROW_ID,
                                    dHedgeTypeName = q.HMHT_NAME,
                                    dHedgeTypeDesc = q.HMHT_DESC,
                                    dHedgeTypeInventory = q.HMHT_INVENTORY_FLAG,
                                    dHedgeTypeStatus = q.HMHT_STATUS,
                                    dHedgeTypeUpdatedBy = q.HMHT_UPDATED_BY,
                                    dHedgeTypeUpdatedDate = q.HMHT_UPDATED_DATE.ToString("dd/MM/yyyy")
                                });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch(Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Add(HedgeTypeViewModel_Detail pModel,string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HedgeTypeName))
                {
                    rtn.Message = "Hedge Type name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HedgeTypeStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_HEDGE_TYPE_DAL dal = new HEDG_MT_HEDGE_TYPE_DAL();
                HEDG_MT_HEDGE_TYPE ent = new HEDG_MT_HEDGE_TYPE();

                DateTime now = DateTime.Now;
                try
                {
                    var last = dal.getLastID();

                    string ID;
                    if (!String.IsNullOrEmpty(last))
                    {
                        var a = last.ToUpper().Substring(5, 3);
                        int b = Convert.ToInt32(a) + 1;
                        ID = "HEGT-" + b.ToString("000");                        
                    }
                    else
                    {
                        ID = "HEGT-000";
                    }
                    ent.HMHT_ROW_ID = ID;
                    ent.HMHT_NAME = pModel.HedgeTypeName;
                    ent.HMHT_DESC = pModel.description;
                    ent.HMHT_INVENTORY_FLAG = pModel.HedgeTypeInventory;
                    ent.HMHT_STATUS = pModel.HedgeTypeStatus;
                    ent.HMHT_CREATED_DATE = now;
                    ent.HMHT_CREATED_BY = pUser.ToUpper();
                    ent.HMHT_UPDATED_BY = pUser.ToUpper();
                    ent.HMHT_UPDATED_DATE = now;
                    dal.Save(ent);

                    pModel.HedgeTypeCode = ID;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HedgeTypeViewModel_Detail Get(string typeId)
        {
            HedgeTypeViewModel_Detail model = new HedgeTypeViewModel_Detail();
            try
            {
                if (!String.IsNullOrEmpty(typeId))
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var query = context.HEDG_MT_HEDGE_TYPE.Where(c => c.HMHT_ROW_ID == typeId);

                        if (query!=null)
                        {
                            foreach (var d in query)
                            {
                                model.HedgeTypeCode = d.HMHT_ROW_ID;
                                model.HedgeTypeName = d.HMHT_NAME;
                                model.description = d.HMHT_DESC;
                                model.HedgeTypeInventory = d.HMHT_INVENTORY_FLAG;
                                model.HedgeTypeStatus = d.HMHT_STATUS;
                                model.BoolInventory = d.HMHT_INVENTORY_FLAG == "Y" ? true : false;
                                model.BoolStatus = d.HMHT_STATUS == "ACTIVE" ? true : false;
                            }
                        }
                    }
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue Edit(HedgeTypeViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HedgeTypeName))
                {
                    rtn.Message = "Hedge Type name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HedgeTypeStatus))
                {
                    rtn.Message = "Status should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                HEDG_MT_HEDGE_TYPE_DAL dal = new HEDG_MT_HEDGE_TYPE_DAL();
                HEDG_MT_HEDGE_TYPE ent = new HEDG_MT_HEDGE_TYPE();

                DateTime now = DateTime.Now;
                try
                {                   
                    ent.HMHT_ROW_ID = pModel.HedgeTypeCode;
                    ent.HMHT_NAME = pModel.HedgeTypeName;
                    ent.HMHT_DESC = pModel.description;
                    ent.HMHT_INVENTORY_FLAG = pModel.HedgeTypeInventory;
                    ent.HMHT_STATUS = pModel.HedgeTypeStatus;
                    ent.HMHT_CREATED_DATE = now;
                    ent.HMHT_CREATED_BY = pUser.ToUpper();
                    ent.HMHT_UPDATED_BY = pUser.ToUpper();
                    ent.HMHT_UPDATED_DATE = now;
                    dal.Update(ent);

                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }
    }
}