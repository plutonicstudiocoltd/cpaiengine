﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Globalization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class HolidayServiceModel
    {
        /// <summary>
        /// Check Holiday
        /// </summary>
        /// <param name="pDateString">dd/MM/yyyy</param>
        /// <param name="pHolidayType">UK,TH</param>
        /// <returns>True : is holity, False : is not holiday</returns>
        public static bool CheckHoliday(string pDateString, string pHolidayType)
        {
            if (string.IsNullOrEmpty(pDateString))
            {
                return false;
            }
            else if (ShareFn.IsValidDateFormat(pDateString) == false)
            {
                return false;
            }
            else
            {
                try
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        DateTime date = ShareFn.ConvertStrDateToDate(pDateString);
                        var _search = from p in context.MT_HOLIDAY
                                      where p.MH_HOL_DATE.Value.Equals(date)
                                      select p;

                        if (!string.IsNullOrEmpty(pHolidayType))
                            _search = _search.Where(p => p.MH_HOL_TYPE.ToUpper().Equals(pHolidayType.ToUpper()));

                        if (_search.ToList().Count != 0)
                        {
                            return true;
                        }
                        else if (date.DayOfWeek == DayOfWeek.Sunday)
                        {
                            return true;    // is holiday
                        }
                        else if (date.DayOfWeek == DayOfWeek.Saturday)
                        {
                            return true;    // is holiday
                        }
                        else
                        {
                            return false;
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        public ReturnValue Add(ref HolidayViewModel_Detail pModel, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HolidayDesc))
                {
                    rtn.Message = "Holiday Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HolidayDate))
                {
                    rtn.Message = "Holiday Date should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (ShareFn.IsValidDateFormat(pModel.HolidayDate) == false)
                {
                    rtn.Message = "Holiday Date is not valid format";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(pModel.HolidayType))
                {
                    rtn.Message = "Holiday Type should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (CheckHoliday(pModel.HolidayDate, pModel.HolidayType))
                {
                    rtn.Message = "Holiday Date is duplicate";
                    rtn.Status = false;
                    return rtn;
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_HOLIDAY_DAL dal = new MT_HOLIDAY_DAL();
                MT_HOLIDAY ent = new MT_HOLIDAY();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            
                            ent.MH_HOL_DESC = pModel.HolidayDesc.Trim();
                            ent.MH_HOL_DATE = ShareFn.ConvertStringDateFormatToDatetime(pModel.HolidayDate, "dd/MM/yyyy");
                            ent.MH_HOL_TYPE = pModel.HolidayType;

                            dal.Save(ent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                            
                            var holID = getMaxID();
                            if (string.IsNullOrEmpty(holID))
                            {
                                rtn.Message = "Cannot generate Holiday ID";
                                rtn.Status = false;
                                return rtn;
                            }
                            pModel.HolidayID = holID;    // Return ID

                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(HolidayViewModel_Detail model, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                if (model == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.HolidayID))
                {
                    rtn.Message = "Holiday ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.HolidayDesc))
                {
                    rtn.Message = "Holiday Description should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                //else if (string.IsNullOrEmpty(model.HolidayDate))
                //{
                //    rtn.Message = "Holiday Date should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                //else if (ShareFn.IsValidDateFormat(model.HolidayDate))
                //{
                //    rtn.Message = "Holiday Date is not valid format";
                //    rtn.Status = false;
                //    return rtn;
                //}
                //else if (string.IsNullOrEmpty(model.HolidayType))
                //{
                //    rtn.Message = "Holiday Type should not be empty";
                //    rtn.Status = false;
                //    return rtn;
                //}
                

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_HOLIDAY_DAL dal = new MT_HOLIDAY_DAL();
                MT_HOLIDAY ent = new MT_HOLIDAY();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                         
                            ent.MH_HOL_ID = Convert.ToDecimal(model.HolidayID);
                            ent.MH_HOL_DESC = model.HolidayDesc.Trim();
                            //ent.MH_HOL_DATE = ShareFn.ConvertStringDateFormatToDatetime(model.HolidayDate, "dd/MM/yyyy");
                            //ent.MH_HOL_TYPE = model.HolidayType.Trim();

                            dal.Update(ent, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref HolidayViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sHolidayID = String.IsNullOrEmpty(pModel.sHolidayID) ? "" : pModel.sHolidayID;
                    var sHolidayDesc = String.IsNullOrEmpty(pModel.sHolidayDesc) ? "" : pModel.sHolidayDesc.ToLower().Trim();
                    var sHolidayType = String.IsNullOrEmpty(pModel.sHolidayType) ? "" : pModel.sHolidayType.ToUpper();
                    var sHolidayDate = String.IsNullOrEmpty(pModel.sHolidayDate) == true  ? "" : pModel.sHolidayDate;

                    var query = (from v in context.MT_HOLIDAY
                                 select v);

                    if (!string.IsNullOrEmpty(sHolidayID) && query != null)
                        query = query.Where(p => p.MH_HOL_ID.ToString().Contains(sHolidayID));


                    if (!string.IsNullOrEmpty(sHolidayDesc) && query != null)
                        query = query.Where(p => p.MH_HOL_DESC.ToLower().Contains(sHolidayDesc));

                    if (!string.IsNullOrEmpty(sHolidayType) && query != null)
                        query = query.Where(p => p.MH_HOL_TYPE.ToUpper().Equals(sHolidayType));


                    if (!string.IsNullOrEmpty(sHolidayDate) && query != null)
                    {
                        ShareFn _FN = new ShareFn();
                        var startDate = "";
                        var endDate = "";
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        if (sHolidayDate != "")
                        {
                            string[] date = sHolidayDate.Split(' ');
                            startDate = date[0];
                            sDate = ShareFn.ConvertStrDateToDate(startDate);
                            endDate = date[2];
                            eDate = ShareFn.ConvertStrDateToDate(endDate);
                        }
                        query = query.Where(q => q.MH_HOL_DATE >= sDate && q.MH_HOL_DATE <= eDate);

                    }

                    if (query != null)
                    {
                        pModel.sSearchData = new List<HolidayViewModel_SearchData>();
                        foreach (var item in query)
                        {
                            pModel.sSearchData.Add(new HolidayViewModel_SearchData
                            {
                                dHolidayID = Convert.ToString(item.MH_HOL_ID)
                                 ,
                                dHolidayType = string.IsNullOrEmpty(item.MH_HOL_TYPE) ? "" : item.MH_HOL_TYPE
                                         ,
                                dHolidayDate = ShareFn.ConvertDateTimeToDateStringFormat(item.MH_HOL_DATE, "dd/MM/yyyy")
                                         ,
                                dHolidayDesc = string.IsNullOrEmpty(item.MH_HOL_DESC) ? "" : item.MH_HOL_DESC
                                ,
                                dHolidayDateOrder = item.MH_HOL_TYPE +"_"+ShareFn.ConvertDateTimeToDateStringFormat(item.MH_HOL_DATE, "yyyyMMdd")
                            });
                        }
                        pModel.sSearchData = pModel.sSearchData.OrderBy(d => d.dHolidayDateOrder).ToList();
                                   
                        
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data not found";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public HolidayViewModel_Detail Get(string pHolidayID)
        {
            HolidayViewModel_Detail model = new HolidayViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pHolidayID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        decimal hID = Convert.ToDecimal(pHolidayID);
                        var query = context.MT_HOLIDAY.Where(p => p.MH_HOL_ID == hID).ToList();
                                    

                        if (query != null)
                        {
                            
                            foreach (var item in query)
                            {
                                model.HolidayDate = ShareFn.ConvertDateTimeToDateStringFormat(item.MH_HOL_DATE,"dd/MM/yyyy");
                                model.HolidayID = Convert.ToString(item.MH_HOL_ID);
                                model.HolidayDesc = item.MH_HOL_DESC;
                                model.HolidayType = item.MH_HOL_TYPE;
                            }
                            return model;
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ReturnValue Delete(string pHolidayID)
        {
            ReturnValue rtn = new ReturnValue();
          
                if (String.IsNullOrEmpty(pHolidayID))
                {
                    rtn.Message = "Holiday ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_HOLIDAY_DAL dal = new MT_HOLIDAY_DAL();
                MT_HOLIDAY ent = new MT_HOLIDAY();

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            dal.Delete(pHolidayID, context);

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            return rtn;
        }

        public static string getMaxID()
        {
            string strMaxID = "";
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_HOLIDAY
                                 select v.MH_HOL_ID);

                    if (query != null)
                    {
                        strMaxID = (query.Max()).ToString();
                    }

                }

                return strMaxID;

            }
            catch (Exception ex)
            {
                //throw ex;
                return "";
            }
        }
    }
}