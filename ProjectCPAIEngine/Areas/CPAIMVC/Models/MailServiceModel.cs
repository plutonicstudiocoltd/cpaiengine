﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using log4net;

using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.engine.downstream;
using DSMail.service;


namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class MailServiceModel
    {
        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private MailMappingService mms = new MailMappingService();

        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage,ref MailDetail detail)
        {
            List<USERS> userResult;
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER))
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                userResult = userGMan.findUserMailByGroupSystem(userGroup, detail.system);
                if (userResult.Count == 0)
                {
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, userResult,ref detail);
                }
            }
            else
            {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1)
                {
                    log.Info("user not found");
                }
                else
                {
                    prepareSendMail(stateModel, userResult,ref detail);
                }
            }
            return currentCode;
        }

        public void prepareSendMail(StateModel stateModel, List<USERS> userMail,ref MailDetail detail)
        {
            List<USERS> userResult = userMan.findByLogin(detail.user);

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(detail.user, detail.system); //do action

            if (userResult.Count == 0)
            {
                log.Info("login user not found");
            }
            if (detail.subject.Contains("#user"))
            {
                detail.subject = detail.subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (detail.body.Contains("#user"))
            {
                detail.body = detail.body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (detail.subject.Contains("#group_user"))
            {
                detail.subject = detail.subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (detail.body.Contains("#group_user"))
            {
                detail.body = detail.body.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            //get create by
            string tem = detail.createby;
            List<USERS> create_by = userMan.findByLogin(tem);
            string create_by_v = "";
            if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            {
                create_by_v = create_by[0].USR_FIRST_NAME_EN;
            }
            if (detail.body.Contains("#create_by"))
            {
                detail.body = detail.body.Replace("#create_by", create_by_v);
            }

            if (detail.body.Contains("#token"))
            {
                //for loop send mail
                string temp_body = detail.body;
                foreach (USERS um in userMail)
                {
                    temp_body = detail.body;

                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = detail.type;
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = detail.system;
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = detail.user;
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = detail.user;
                    at.TOK_UPDATED_DATE = now;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, detail.subject, out_body);
                }
            }
            else
            {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++)
                {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, detail.subject, detail.body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body)
        {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++)
            {
                if (mailTo.Length > 0)
                {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }
    }
}