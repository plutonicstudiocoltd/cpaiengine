﻿using Common.Logging;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Text;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class OperatParamServiceModel
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        EntityCPAIEngine context = new EntityCPAIEngine();


        MT_OPER_DATA_DAL dalData = new MT_OPER_DATA_DAL();
        MT_OPER_PARAM_DAL dalParam = new MT_OPER_PARAM_DAL();
        MT_OPER_UNIT_DAL dalUnit = new MT_OPER_UNIT_DAL();
        MT_OPER_CONDITION_DAL dalCondition = new MT_OPER_CONDITION_DAL();

        MT_OPER_DATA entData = new MT_OPER_DATA();
        MT_OPER_PARAM entParam = new MT_OPER_PARAM();
        MT_OPER_UNIT entUnit = new MT_OPER_UNIT();
        MT_OPER_CONDITION entCondition = new MT_OPER_CONDITION();




        public ReturnValue Add(OperatParamViewModel_Detail pModel, string pUser)
        {
            int countUnit = 0;
            int countParam = 0;
            int countData = 0;
            int countCondition = 0;
            ReturnValue rtn = new ReturnValue();
            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.Unit == null)
            {
                rtn.Message = "Unit is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.Param == null)
            {
                rtn.Message = "Detail is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.DataCondition == null)
            {
                rtn.Message = "Condition is not null";
                rtn.Status = false;
                return rtn;
            }
            try
            {
                entUnit = new MT_OPER_UNIT();
                var unitRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countUnit.ToString("000"));
                entUnit.MOU_ROW_ID = unitRowId;
                entUnit.MOU_UNIT = pModel.Unit;
                entUnit.MOU_CREATED_BY = pUser;
                entUnit.MOU_CREATED_DATE = DateTime.Now;
                entUnit.MOU_UPDATED_BY = pUser;
                entUnit.MOU_UPDATED_DATE = DateTime.Now;
                dalUnit.Save(entUnit);
                countUnit++;
                #region
                if (pModel.Param != null && pModel.Param.Any())
                {
                    //int indexParam = 1;
                    foreach (var param in pModel.Param)
                    {
                        entParam = new MT_OPER_PARAM();
                        var paramRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countParam.ToString("000"));
                        entParam.MOP_ROW_ID = paramRowId;
                        entParam.MOP_ORDER = param.Order;
                        entParam.MOP_FK_MT_OPER_UNIT = unitRowId;
                        entParam.MOP_PARAMETER = param.Parameter;
                        entParam.MOP_WEIGHT_FACTOR = param.Factor;
                        entParam.MOP_CREATED_BY = pUser;
                        entParam.MOP_CREATED_DATE = DateTime.Now;
                        entParam.MOP_UPDATED_BY = pUser;
                        entParam.MOP_UPDATED_DATE = DateTime.Now;
                        dalParam.Save(entParam);
                        countParam++;

                        if (param.DataKeyValue != null && param.DataKeyValue.Any())
                        {
                            foreach (var keyValue in param.DataKeyValue)
                            {
                                foreach (var key in keyValue.KeyValue)
                                {
                                    entData = new MT_OPER_DATA();
                                    var dataRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countData.ToString("000"));
                                    entData.MOD_ROW_ID = dataRowId;
                                    entData.MOD_FK_MT_OPER_PARAM = paramRowId;
                                    entData.MOD_SCORE = keyValue.Score == null ? "" : keyValue.Score;
                                    entData.MOD_KEY = key.Key != null ? ShareFn.EncodeString(key.Key) : null;
                                    entData.MOD_CREATED_BY = pUser;
                                    entData.MOD_CREATED_DATE = DateTime.Now;
                                    entData.MOD_UPDATED_BY = pUser;
                                    entData.MOD_UPDATED_DATE = DateTime.Now;
                                    dalData.Save(entData);
                                    countData++;
                                }
                            }
                        }
                    }
                }
                #endregion

                if (pModel.DataCondition != null && pModel.DataCondition.Any())
                {
                    foreach (var con in pModel.DataCondition)
                    {
                        entCondition = new MT_OPER_CONDITION();
                        var conRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countCondition.ToString("000"));
                        entCondition.MOC_ROW_ID = conRowId;
                        entCondition.MOC_FK_MT_OPER_UNIT = unitRowId;
                        entCondition.MOC_KEY = con.Key != null ? ShareFn.EncodeString(con.Key) : null;
                        entCondition.MOC_ROW = con.Row;
                        entCondition.MOC_COLUMN = con.Column;
                        entCondition.MOC_CONDITION = new JavaScriptSerializer().Serialize(con.ObjCondition);
                        dalCondition.Save(entCondition);
                        countCondition++;
                    }
                }

                pModel.RowId = unitRowId;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Edit(OperatParamViewModel_Detail pModel, string pUser)
        {
            int countUnit = 0;
            int countParam = 0;
            int countData = 0;
            int countCondition = 0;
            ReturnValue rtn = new ReturnValue();
            if (pModel == null)
            {
                rtn.Message = "Model is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.Unit == null)
            {
                rtn.Message = "Unit is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.Param == null)
            {
                rtn.Message = "Detail is not null";
                rtn.Status = false;
                return rtn;
            }
            else if (pModel.DataCondition == null)
            {
                rtn.Message = "Condition is not null";
                rtn.Status = false;
                return rtn;
            }
            try
            {
                var unit = context.MT_OPER_UNIT.Where(c => c.MOU_ROW_ID == pModel.RowId);
                var paramdata = context.MT_OPER_PARAM.Where(c => c.MOP_FK_MT_OPER_UNIT == pModel.RowId).FirstOrDefault();
                if (paramdata != null)
                {
                    var paramId = paramdata.MOP_ROW_ID;
                    var data = context.MT_OPER_DATA.Where(c => c.MOD_FK_MT_OPER_PARAM == paramId);
                    if (data.Count() > 0)
                    {
                        dalData.Delete(paramId);
                    }

                    if (paramdata != null)
                    {
                        dalParam.Delete(pModel.RowId);
                    }
                }


                var dataCondition = context.MT_OPER_CONDITION.Where(c => c.MOC_FK_MT_OPER_UNIT == pModel.RowId);

                if (dataCondition.Count() > 0)
                {
                    dalCondition.Delete(pModel.RowId);
                }






                entUnit.MOU_ROW_ID = pModel.RowId;
                entUnit.MOU_UNIT = pModel.Unit;
                entUnit.MOU_UPDATED_BY = pUser;
                entUnit.MOU_UPDATED_DATE = DateTime.Now;
                dalUnit.Update(entUnit);
                countUnit++;

                if (pModel.Param != null && pModel.Param.Any())
                {
                    foreach (var param in pModel.Param)
                    {
                        entParam = new MT_OPER_PARAM();
                        var paramRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countParam.ToString("000"));
                        entParam.MOP_ROW_ID = paramRowId;
                        entParam.MOP_ORDER = param.Order;
                        entParam.MOP_FK_MT_OPER_UNIT = pModel.RowId;
                        entParam.MOP_PARAMETER = param.Parameter;
                        entParam.MOP_WEIGHT_FACTOR = param.Factor;
                        entParam.MOP_CREATED_BY = pUser;
                        entParam.MOP_CREATED_DATE = DateTime.Now;
                        entParam.MOP_UPDATED_BY = pUser;
                        entParam.MOP_UPDATED_DATE = DateTime.Now;
                        dalParam.Save(entParam);
                        countParam++;

                        if (param.DataKeyValue != null && param.DataKeyValue.Any())
                        {
                            foreach (var keyValue in param.DataKeyValue)
                            {
                                foreach (var key in keyValue.KeyValue)
                                {
                                    entData = new MT_OPER_DATA();
                                    var dataRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countData.ToString("000"));
                                    entData.MOD_ROW_ID = dataRowId;
                                    entData.MOD_FK_MT_OPER_PARAM = paramRowId;
                                    entData.MOD_SCORE = keyValue.Score == null ? "" : keyValue.Score;
                                    entData.MOD_KEY = key.Key != null ? ShareFn.EncodeString(key.Key) : null;
                                    entData.MOD_CREATED_BY = pUser;
                                    entData.MOD_CREATED_DATE = DateTime.Now;
                                    entData.MOD_UPDATED_BY = pUser;
                                    entData.MOD_UPDATED_DATE = DateTime.Now;
                                    dalData.Save(entData);
                                    countData++;
                                }
                            }
                        }
                    }
                }

                if (pModel.DataCondition != null && pModel.DataCondition.Any())
                {
                    foreach (var con in pModel.DataCondition)
                    {
                        entCondition = new MT_OPER_CONDITION();
                        var conRowId = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countCondition.ToString("000"));
                        entCondition.MOC_ROW_ID = conRowId;
                        entCondition.MOC_FK_MT_OPER_UNIT = pModel.RowId;
                        entCondition.MOC_KEY = con.Key != null ? ShareFn.EncodeString(con.Key) : null;
                        entCondition.MOC_ROW = con.Row;
                        entCondition.MOC_COLUMN = con.Column;
                        entCondition.MOC_CONDITION = new JavaScriptSerializer().Serialize(con.ObjCondition);
                        dalCondition.Save(entCondition);
                        countCondition++;
                    }
                }


                //pModel.RowId = unitRowId;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                rtn.Message = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }        



        public ReturnValue Search(ref OperatParamViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                var sOPCode = pModel.sOperaParamCode == null ? "" : pModel.sOperaParamCode.ToUpper();
                var sOPUnit = pModel.sOperaParamUnit == null ? "" : pModel.sOperaParamUnit.ToUpper();

                var query = (from u in context.MT_OPER_UNIT
                             join mt in context.MT_UNIT on u.MOU_UNIT equals mt.MUN_ROW_ID
                             select new OperatParamViewModel_SearchData
                             {
                                 dOperaParamCode = u.MOU_ROW_ID,
                                 dOperaParamUnitId = mt.MUN_ROW_ID,
                                 dOperaParamUnit = mt.MUN_NAME,
                                 dOperaParamDate = u.MOU_CREATED_DATE
                             });

                if (!string.IsNullOrEmpty(sOPCode))
                    query = query.Where(p => p.dOperaParamCode.ToUpper().Contains(sOPCode)).OrderBy(c => c.dOperaParamDate);

                if (!string.IsNullOrEmpty(sOPUnit))
                    query = query.Where(p => p.dOperaParamUnitId.ToUpper().Contains(sOPUnit)).OrderBy(c => c.dOperaParamDate);



                if (query != null)
                {

                    pModel.sSearchData = query.ToList();
                    pModel.sSearchData = new List<OperatParamViewModel_SearchData>();
                    foreach (var item in query)
                    {
                        pModel.sSearchData.Add(new OperatParamViewModel_SearchData
                        {
                            dOperaParamCode = string.IsNullOrEmpty(item.dOperaParamCode) ? "" : item.dOperaParamCode,
                            dOperaParamUnit = string.IsNullOrEmpty(item.dOperaParamUnit) ? "" : item.dOperaParamUnit,
                            dOperaParamDate = item.dOperaParamDate
                        });
                    }
                    rtn.Status = true;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data is null";
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }


        public OperatParamViewModel_Detail GetOperParamDetail(string rowId, bool isUnitKey = false)
        {
            OperatParamViewModel_Detail model = new OperatParamViewModel_Detail();

            try
            {
                MT_OPER_UNIT oper_unit = new MT_OPER_UNIT();
                if (!isUnitKey)
                {
                    oper_unit = context.MT_OPER_UNIT.Where(c => c.MOU_ROW_ID == rowId).OrderByDescending(x => x.MOU_CREATED_DATE).FirstOrDefault();
                }
                else
                {
                    oper_unit = context.MT_OPER_UNIT.Where(c => c.MOU_UNIT == rowId).OrderByDescending(x => x.MOU_CREATED_DATE).FirstOrDefault();
                    if (oper_unit != null)
                    {
                        rowId = oper_unit.MOU_ROW_ID;
                    }
                }

                if (oper_unit != null)
                {
                    var oper_param = (from a in context.MT_OPER_PARAM
                                      where a.MOP_FK_MT_OPER_UNIT == rowId
                                      select new OperatParamViewModel_Param
                                      {
                                          Row_ID = a.MOP_ROW_ID,
                                          Order = a.MOP_ORDER,
                                          Parameter = a.MOP_PARAMETER,
                                          Factor = a.MOP_WEIGHT_FACTOR,
                                          //DataKeyValue = getOper_data(a.MOP_ROW_ID)
                                      }).OrderBy(c => c.Order).ToList();




                    if (oper_param != null)
                    {
                        for (int i = 0; i < oper_param.Count(); i++)
                        {
                            oper_param[i].DataKeyValue = getOper_data(oper_param[i].Row_ID);
                        }
                    }


                    var oper_Condition_detail = (from a in context.MT_OPER_CONDITION
                                                 where a.MOC_FK_MT_OPER_UNIT == rowId
                                                 select new
                                                 {
                                                     Row_Id = a.MOC_ROW_ID,
                                                     Key = a.MOC_KEY,
                                                     Row = a.MOC_ROW,
                                                     Column = a.MOC_COLUMN,
                                                     Condition = a.MOC_CONDITION,
                                                     MT_Oper_Unit = a.MOC_FK_MT_OPER_UNIT                                                  
                                                 }).AsEnumerable().OrderBy(c => c.Key).ToList();
                    var oper_Condition = oper_Condition_detail.Select(i => new OperatParamViewModel_Condition()
                    {
                        Row_Id = i.Row_Id,
                        Key = i.Key != null ? ShareFn.DecodeString(i.Key) : null,
                        //Key=i.Key,
                        Row = i.Row,
                        Column = i.Column,
                        MT_Oper_Unit = i.MT_Oper_Unit,
                        ObjCondition = new JavaScriptSerializer().Deserialize<objCondition>(i.Condition)
                    });

                    model.RowId = oper_unit.MOU_ROW_ID;
                    model.Unit = oper_unit.MOU_UNIT;
                    model.Param = oper_param;
                    model.DataCondition = oper_Condition.OrderBy(c=>c.Row_Id).ToList();

                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        





        public List<OperatParamViewModel_Data> getOper_data(string paramId)
        {
            List<OperatParamViewModel_Data> oper_data = new List<OperatParamViewModel_Data>();
            if (paramId != null)
            {
                try
                {
                    var oper_data_detail = (from d in context.MT_OPER_DATA
                                            where d.MOD_FK_MT_OPER_PARAM == paramId
                                            select new
                                            {
                                                Row_Id = d.MOD_ROW_ID,
                                                MT_Oper_Param = d.MOD_FK_MT_OPER_PARAM,
                                                Score = d.MOD_SCORE,
                                                Key = d.MOD_KEY
                                            }).OrderBy(c => c.Score).ThenBy(n => n.Key).ToList();

                    if (oper_data_detail != null && oper_data_detail.Any())
                    {
                        foreach (var oper_data_detail_item in oper_data_detail)
                        {
                            if (!oper_data.Any(i => i.Score == oper_data_detail_item.Score))
                            {
                                oper_data.Add(new OperatParamViewModel_Data()
                                {
                                    MT_Oper_Param = oper_data_detail_item.MT_Oper_Param,
                                    Row_Id = oper_data_detail_item.Row_Id,
                                    Score = oper_data_detail_item.Score,
                                    KeyValue = new List<KeyValue>()
                                });
                            }
                            oper_data.Where(i => i.Score == oper_data_detail_item.Score).First().KeyValue.Add(new KeyValue() { Key = oper_data_detail_item.Key != null ? ShareFn.DecodeString(oper_data_detail_item.Key) : null });
                        }


                    }

                    return oper_data;

                }

                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else
            {
                return null;
            }
        }

        public List<KeyValue> getKey(string paramId)
        {
            var data = context.MT_OPER_DATA.Where(c => c.MOD_FK_MT_OPER_PARAM == paramId).ToList();


            var key_data = (from d in data

                            select new KeyValue
                            {
                                //Key = d.Key
                            }).ToList();
            return key_data;
        }


        private void DeleteParam(string unitId)
        {

            var param = context.MT_OPER_PARAM.Where(c => c.MOP_FK_MT_OPER_UNIT == unitId).ToList();
            var data = context.MT_OPER_DATA.Where(c => c.MOD_FK_MT_OPER_PARAM == param.SingleOrDefault().MOP_ROW_ID);

            if (data != null)
            {
                try
                {
                    dalData.Delete(param[0].MOP_ROW_ID);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                dalParam.Delete(unitId);
            }
        }


      
    }
}