﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class PlantServiceModel
    {
        public ReturnValue Add(PlantViewModel_Detail model, string pUser)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {

                if (model == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.CompanyCode))
                {
                    rtn.Message = "Company should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.PlantID))
                {
                    rtn.Message = "Plant ID should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (CheckHaveData(model.PlantID))
                {
                    rtn.Message = "Plant ID is duplicate";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.PlantShortName))
                {
                    rtn.Message = "Short name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
               

                if (model.Control == null)
                {
                    rtn.Message = "Please add system and status";
                    rtn.Status = false;
                    return rtn;
                }
                else if (model.Control.Count == 0)
                {
                    rtn.Message = "Please add system and status";
                    rtn.Status = false;
                    return rtn;
                }
                else if (model.Control.Count >= 0)
                {
                    var systemEmpty = model.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = model.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = model.Control.GroupBy(x => x.System.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }


                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_PLANT_DAL dal = new MT_PLANT_DAL();
                MT_PLANT ent = new MT_PLANT();

                MT_PLANT_CONTROL_DAL dalCtr = new MT_PLANT_CONTROL_DAL();
                MT_PLANT_CONTROL entCtr = new MT_PLANT_CONTROL();

                DateTime now = DateTime.Now;

                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //var portID = Convert.ToDecimal(ShareFn.GenerateCodeByDate(""));
                            ent.MPL_COMPANY_CODE = model.CompanyCode;
                            ent.MPL_PLANT = model.PlantID;
                            ent.MPL_SHORT_NAME = model.PlantShortName.Trim();
                            ent.MPL_DESC = model.PlantDesc;
                            ent.MPL_CREATE_TYPE = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            ent.MPL_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MPL_CREATED_BY = pUser;
                            ent.MPL_CREATED_DATE = now;
                            ent.MPL_UPDATED_BY = pUser;
                            ent.MPL_UPDATED_DATE = now;

                            dal.Save(ent, context);

                            foreach (var item in model.Control)
                            {
                                entCtr = new MT_PLANT_CONTROL();
                                entCtr.MPC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                entCtr.MPC_FK_PLANT = model.PlantID;
                                entCtr.MPC_SYSTEM = String.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                entCtr.MPC_STATUS = item.Status;
                                entCtr.MPC_CREATED_BY = pUser;
                                entCtr.MPC_CREATED_DATE = now;
                                entCtr.MPC_UPDATED_BY = pUser;
                                entCtr.MPC_UPDATED_DATE = now;

                                dalCtr.Save(entCtr, context);
                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Edit(PlantViewModel_Detail model, string pUser)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {

                if (model == null)
                {
                    rtn.Message = "Model is not null";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.CompanyCode))
                {
                    rtn.Message = "Company should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.PlantID))
                {
                    rtn.Message = "Plant id should not be empty";
                    rtn.Status = false;
                    return rtn;
                }
                else if (string.IsNullOrEmpty(model.PlantShortName))
                {
                    rtn.Message = "Short name should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                if (model.Control == null)
                {
                    rtn.Message = "Please add system and status";
                    rtn.Status = false;
                    return rtn;
                }
                else if (model.Control.Count == 0)
                {
                    rtn.Message = "Please add system and status";
                    rtn.Status = false;
                    return rtn;
                }
                else if (model.Control.Count >= 0)
                {
                    var systemEmpty = model.Control.Any(x => string.IsNullOrWhiteSpace(x.System));
                    if (systemEmpty == true)
                    {
                        rtn.Message = "System should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var statusEmpty = model.Control.Any(x => string.IsNullOrWhiteSpace(x.Status));
                    if (statusEmpty == true)
                    {
                        rtn.Message = "Status should not be empty";
                        rtn.Status = false;
                        return rtn;
                    }

                    var checkDuplicate = model.Control.GroupBy(x => x.System.Trim().ToUpper()).Any(g => g.Count() > 1);
                    if (checkDuplicate == true)
                    {
                        rtn.Message = "System is duplicate";
                        rtn.Status = false;
                        return rtn;
                    }
                }

                if (string.IsNullOrEmpty(pUser))
                {
                    rtn.Message = "User should not be empty";
                    rtn.Status = false;
                    return rtn;
                }

                MT_PLANT_DAL dal = new MT_PLANT_DAL();
                MT_PLANT ent = new MT_PLANT();

                MT_PLANT_CONTROL_DAL dalCtr = new MT_PLANT_CONTROL_DAL();
                MT_PLANT_CONTROL entCtr = new MT_PLANT_CONTROL();

                DateTime now = DateTime.Now;
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {

                            // Update data in MT_PLANT
                            ent.MPL_PLANT = model.PlantID;

                            ent.MPL_COMPANY_CODE = model.CompanyCode;
                            ent.MPL_SHORT_NAME = model.PlantShortName;
                            ent.MPL_DESC = model.PlantDesc;
                            ent.MPL_CREATE_TYPE = model.CreateType;
                            ent.MPL_STATUS = CPAIConstantUtil.ACTIVE;
                            ent.MPL_UPDATED_BY = pUser;
                            ent.MPL_UPDATED_BY = now.ToString();

                            if (model.CreateType.ToUpper() == CPAIConstantUtil.PROJECT_NAME_SPACE)
                                dal.Update(ent, context);                   // CPAI : Update all
                            else
                                dal.UpdateStatus(ent, context);             // SAP : Update status

                            // Insert data to MT_PLANT_CONTROL
                            foreach (var item in model.Control)
                            {
                                if (string.IsNullOrEmpty(item.RowID))
                                {
                                    // Add
                                    entCtr = new MT_PLANT_CONTROL();
                                    entCtr.MPC_ROW_ID = ConstantPrm.GetDynamicCtrlID();
                                    entCtr.MPC_FK_PLANT = model.PlantID;
                                    entCtr.MPC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MPC_STATUS = item.Status;
                                    entCtr.MPC_CREATED_BY = pUser;
                                    entCtr.MPC_CREATED_DATE = now;
                                    entCtr.MPC_UPDATED_BY = pUser;
                                    entCtr.MPC_UPDATED_DATE = now;

                                    dalCtr.Save(entCtr, context);
                                }
                                else
                                {
                                    // Update
                                    entCtr = new MT_PLANT_CONTROL();
                                    entCtr.MPC_ROW_ID = item.RowID;
                                    entCtr.MPC_FK_PLANT = model.PlantID;
                                    entCtr.MPC_SYSTEM = string.IsNullOrEmpty(item.System) ? "" : item.System.Trim().ToUpper();
                                    entCtr.MPC_STATUS = item.Status;
                                    entCtr.MPC_UPDATED_BY = pUser;
                                    entCtr.MPC_UPDATED_DATE = now;

                                    dalCtr.Update(entCtr, context);
                                }

                            }

                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            rtn.Message = ex.Message;
                            rtn.Status = false;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public ReturnValue Search(ref PlantViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {

                    var sComCode = String.IsNullOrEmpty(pModel.sCompanyCode) ? "" : pModel.sCompanyCode.ToUpper();
                    var sPlantId = String.IsNullOrEmpty(pModel.sPlantID) ? "" : pModel.sPlantID.ToUpper();
                    var sPlantShortName = String.IsNullOrEmpty(pModel.sPlantShortName) ? "" : pModel.sPlantShortName.ToUpper();
                    var sCreateType = String.IsNullOrEmpty(pModel.sCreateType) ? "" : pModel.sCreateType.ToUpper();
                    var sSystem = String.IsNullOrEmpty(pModel.sSystem) ? "" : pModel.sSystem.ToUpper();
                    var sStatus = String.IsNullOrEmpty(pModel.sStatus) ? "" : pModel.sStatus.ToUpper();

                    var query = (from v in context.MT_PLANT
                                 join vc in context.MT_PLANT_CONTROL on v.MPL_PLANT equals vc.MPC_FK_PLANT into view
                                 from vc in view.DefaultIfEmpty()
                                 orderby new { v.MPL_PLANT, vc.MPC_SYSTEM }
                                 select new PlantViewModel_SearchData
                                 {
                                     dCompanyCode = v.MPL_COMPANY_CODE
                                     ,
                                     dCompanyName = v.MT_COMPANY.MCO_SHORT_NAME
                                     ,
                                     dPlantID = v.MPL_PLANT
                                     ,
                                     dPlantShortName = v.MPL_SHORT_NAME
                                     ,
                                     dPlantDesc = v.MPL_DESC
                                     ,
                                     dCreateType = String.IsNullOrEmpty( v.MPL_CREATE_TYPE) ? "SAP" : v.MPL_CREATE_TYPE
                                     ,
                                     dSystem = vc.MPC_SYSTEM
                                     ,
                                     dStatus = vc.MPC_STATUS
                                 });

                    if (!string.IsNullOrEmpty(sComCode))
                        query = query.Where(p => p.dCompanyCode.ToUpper().Contains(sComCode.Trim()));


                    if (!string.IsNullOrEmpty(sPlantId))
                        query = query.Where(p => p.dPlantID.ToUpper().Contains(sPlantId.Trim()));

                    if (!string.IsNullOrEmpty(sPlantShortName))
                        query = query.Where(p => p.dPlantShortName.ToUpper().Contains(sPlantShortName.Trim()));

                    if (!string.IsNullOrEmpty(sCreateType))
                        query = query.Where(p => p.dCreateType.ToUpper().Contains(sCreateType));

                    if (!string.IsNullOrEmpty(sSystem))
                        query = query.Where(p => p.dSystem.ToUpper().Contains(sSystem));

                    if (!string.IsNullOrEmpty(sStatus))
                        query = query.Where(p => p.dStatus.ToUpper().Equals(sStatus));


                    if (query != null)
                    {
                        pModel.sSearchData = new List<PlantViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            pModel.sSearchData.Add(
                                                    new PlantViewModel_SearchData
                                                    {
                                                        dCompanyCode = g.dCompanyCode,
                                                        dCompanyName = g.dCompanyName,
                                                        dPlantID = g.dPlantID,
                                                        dPlantShortName = g.dPlantShortName,
                                                        dPlantDesc = g.dPlantDesc,
                                                        dCreateType = g.dCreateType,
                                                        dSystem = g.dSystem,
                                                        dStatus = g.dStatus
                                                    });
                        }
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public PlantViewModel_Detail Get(string pPlantID)
        {
            PlantViewModel_Detail model = new PlantViewModel_Detail();
            try
            {
                if (String.IsNullOrEmpty(pPlantID) == false)
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        
                        var query = (from v in context.MT_PLANT
                                     where v.MPL_PLANT == pPlantID
                                     join vc in context.MT_PLANT_CONTROL on v.MPL_PLANT equals vc.MPC_FK_PLANT into view
                                     from vc in view.DefaultIfEmpty()
                                     orderby new { vc.MPC_SYSTEM }
                                     select new
                                     {
                                         dCompanyCode = v.MPL_COMPANY_CODE
                                         ,
                                         dPlantID = v.MPL_PLANT
                                         ,
                                         dPlantShortName = v.MPL_SHORT_NAME
                                         ,
                                         dCreateType = String.IsNullOrEmpty(v.MPL_CREATE_TYPE) ? "SAP" : v.MPL_CREATE_TYPE
                                         ,
                                         dPlantDesc = v.MPL_DESC
                                         ,
                                         dStatus = v.MPL_STATUS
                                         ,
                                         dCtrlRowID = vc.MPC_ROW_ID
                                         ,
                                         dCtrlSystem = vc.MPC_SYSTEM
                                         ,
                                         dCtrlStatus = vc.MPC_STATUS
                                     });

                        if (query != null)
                        {
                            model.Control = new List<PlantViewModel_Control>();
                            int i = 1;
                            foreach (var g in query)
                            {
                                model.CreateType = g.dCreateType;
                                model.CompanyCode = g.dCompanyCode;
                                model.PlantID = g.dPlantID;
                                model.PlantShortName = g.dPlantShortName;
                                model.PlantDesc = g.dPlantDesc;
                                if(!string.IsNullOrEmpty(g.dCtrlRowID))
                                {
                                    model.Control.Add(new PlantViewModel_Control { RowID = g.dCtrlRowID, Order = i.ToString(), System = g.dCtrlSystem, Status = g.dCtrlStatus });
                                }
                                else
                                {
                                    model.Control.Add(new PlantViewModel_Control { RowID = "", Order = i.ToString(), System = "", Status = "ACTIVE" });

                                }

                                i++;
                            }
                        }
                    }

                    return model;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetSystemJSON(string pStatus = "")
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PLANT_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MPC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MPC_SYSTEM }
                                    select new { System = d.MPC_SYSTEM.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.System));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetPlantIdJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PLANT
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MPL_PLANT }
                                    select new { Value = d.MPL_PLANT.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static string GetPlantNameJSON()
        {
            string json = "";
            try
            {

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PLANT
                                 select v);

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MPL_SHORT_NAME }
                                    select new { Value = d.MPL_SHORT_NAME.ToUpper() }).Distinct().ToArray();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        json = js.Serialize(qDis.OrderBy(p => p.Value));
                    }

                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public static List<DropdownServiceModel_Data> GetSystemForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.MT_PLANT_CONTROL
                                 select v);

                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MPC_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MPC_SYSTEM }
                                    select new { System = d.MPC_SYSTEM.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.System))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.System, Text = item.System });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<DropdownServiceModel_Data> GetPlantCompanyForDDL(string pStatus = "ACTIVE")
        {

            List<DropdownServiceModel_Data> rtn = new List<DropdownServiceModel_Data>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    //var query = (from v in context.MT_PLANT
                    //             join c in context.MT_COMPANY
                    //             on v.MPL_COMPANY_CODE equals c.MCO_COMPANY_CODE
                    //             select new { plant = v, company_name = c.MCO_SHORT_NAME});

                    //if (string.IsNullOrEmpty(pStatus) == false)
                    //    query = query.Where(q => q.plant.MPL_STATUS.Equals(pStatus));

                    //if (query != null)
                    //{
                    //    var qDis = (from d in query
                    //                orderby new { d.company_name }
                    //                select new { CompanyCode = d.plant.MPL_COMPANY_CODE, CompanyName = d.company_name.ToUpper() }).Distinct();

                    //    foreach (var item in qDis.OrderBy(p => p.CompanyName))
                    //    {
                    //        rtn.Add(new DropdownServiceModel_Data { Value = item.CompanyCode, Text = item.CompanyName });
                    //    }
                    //}

                    var query = (from v in context.MT_PLANT
                                 select v);
                    if (string.IsNullOrEmpty(pStatus) == false)
                        query = query.Where(q => q.MPL_STATUS.Equals(pStatus));

                    if (query != null)
                    {
                        var qDis = (from d in query
                                    orderby new { d.MT_COMPANY.MCO_SHORT_NAME }
                                    select new { CompanyCode = d.MPL_COMPANY_CODE, CompanyName = d.MT_COMPANY.MCO_SHORT_NAME.ToUpper() }).Distinct();

                        foreach (var item in qDis.OrderBy(p => p.CompanyName))
                        {
                            rtn.Add(new DropdownServiceModel_Data { Value = item.CompanyCode, Text = item.CompanyName });
                        }
                    }
                }

                return rtn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string getPlantDDLJson(string System)
        {
            string json = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = from v in context.MT_PLANT
                            join vc in context.MT_PLANT_CONTROL on v.MPL_PLANT equals vc.MPC_FK_PLANT
                            where vc.MPC_SYSTEM.ToUpper() == System.ToUpper()
                            && vc.MPC_STATUS.ToUpper() == CPAIConstantUtil.ACTIVE
                            select new
                            {
                                Id = v.MPL_PLANT
                            ,
                                Name = v.MPL_SHORT_NAME
                            ,
                                System = vc.MPC_SYSTEM
                            ,
                                Status = vc.MPC_STATUS
                            };

                if (query != null)
                {
                    var data = (from p in query
                                select new { value = p.Id, text = p.Name }).Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    json = js.Serialize(data.OrderBy(p => p.text));
                    if (!string.IsNullOrEmpty(json))
                    {
                        json = "{ \"data\":" + json + "}";
                    }
                }

            }

            return json;
        }

        public static bool CheckHaveData(string pPlantID)
        {
            if (string.IsNullOrEmpty(pPlantID))
            {
                return false;
            }
            else
            {
                try
                {
                    using (EntityCPAIEngine context = new EntityCPAIEngine())
                    {
                        var _search = context.MT_PLANT.Find(pPlantID);
                        return _search != null ? true : false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }
    }
}