﻿using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ProductBunkerServiceModel
    {
        public static List<string> GetAllProductNames()
        {
            List<string> lstName = new List<string>();
            try {
                lstName = BNK_PRODUCT_ITEMS_DAL.GetProductItems().Select(p => p.BPI_PRODUCT_NAME).Distinct().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lstName;
        }

        public static string GetAllProductNamesJson()
        {
            var lstName = GetAllProductNames().Select(p => new { text = p, value = p }).OrderBy(p => p.text);
            string json = "";
            if (lstName != null)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                json = js.Serialize(lstName);
                if (!string.IsNullOrEmpty(json))
                {
                    json = "{\"data\":" + json + "}";
                }
            }

            return json;
        }
    }
}