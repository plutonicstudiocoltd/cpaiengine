﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.Utilities;
using com.pttict.sap.Interface.sap.reversefidocument.post;
using com.pttict.sap.Interface.Service;
using System.Web.Mvc;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using com.pttict.engine.utility;
using com.pttict.sap.Interface.Model;
using ProjectCPAIEngine.DAL.DALPCF;
using System.Web.Script.Serialization;
using System.Globalization;
using com.pttict.downstream.common.utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ReverseFIDocumentServiceModel : BasicBean
    {
        public ReverseFIDocumentSendResult Post(string year, string company, string documentNumber)
        {
            try
            {
                ReverseFIDocumentService service = new ReverseFIDocumentService();
                ReverseFIDocumentSendResult returnResult = new ReverseFIDocumentSendResult();

                ConfigManagement configManagement = new ConfigManagement();
                string configFromDownStream = configManagement.getDownstreamConfig("CIP_REVERSE_FI_DOC_REVERSE");
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var myConfig = javaScriptSerializer.Deserialize<ConfigModel>(configFromDownStream);
                var config = javaScriptSerializer.Serialize(myConfig);

                returnResult = service.Connect(config, year, company, documentNumber);

                return returnResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PurchaseEntryViewModel_SearchData Search(string year, string documentNumber)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            PurchaseEntryViewModel_SearchData model = new PurchaseEntryViewModel_SearchData();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    #region Purchase Entry
                    var query = (from c in context.PIT_PURCHASE_ENTRY
                                 where c.SAP_FI_DOC_NO == documentNumber
                                 select new
                                 {
                                     dBLDate = c.BL_DATE,
                                     dBLVolumeBBL = c.BL_VOLUME_BBL,
                                     dBLVolumeMT = c.BL_VOLUME_MT,
                                     dBLVolumnL30 = c.BL_VOLUME_L30,
                                     dGRDate = c.GR_DATE,
                                     dGRVolumeBBL = c.GR_VOLUME_BBL,
                                     dGRVolumnMT = c.GR_VOLUME_MT,
                                     dGRVolumnL30 = c.GR_VOLUME_L30,
                                     dPrice = c.PRICE,
                                     dROE = c.ROE,
                                     dAmountUSD = c.AMOUNT_USD,
                                     dAmountTHB = c.AMOUNT_THB,
                                     dVAT = c.VAT,
                                     dTotalAmountUSD = c.TOTAL_USD,
                                     dTotalAmountTHB = c.TOTAL_THB,
                                     dSAPFIDoc = c.SAP_FI_DOC_NO,
                                     dSAPFIDocStatus = c.SAP_FI_DOC_STATUS,
                                     dLastUpdated = c.UPDATED_DATE,
                                     dUpdatedBy = c.UPDATED_BY,
                                     dPoNo = c.PO_NO,
                                     dPoItem = c.PO_ITEM,
                                     dItemNo = c.ITEM_NO
                                 });
                    if (query != null && query.ToList().Count() > 0)
                    {
                        var item = query.ToList()[0];
                        model.sOutturn = new List<PurchaseEntryViewModel_Outtern>();
                        PurchaseEntryViewModel_Outtern outturn = new PurchaseEntryViewModel_Outtern();
                        outturn.BLDate = item.dBLDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.BlQtyBbl = item.dBLVolumeBBL.Value.ToString("#,##0.0000");
                        outturn.BlQtyMt = item.dBLVolumeMT.Value.ToString("#,##0.0000");
                        outturn.BlQtyMl = item.dBLVolumnL30.Value.ToString("#,##0.0000");
                        outturn.PostingDate = item.dGRDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.QtyBbl = item.dGRVolumeBBL.Value.ToString("#,##0.0000");
                        outturn.QtyMt = item.dGRVolumnMT.Value.ToString("#,##0.0000");
                        outturn.QtyMl = item.dGRVolumnL30.Value.ToString("#,##0.0000");
                        outturn.Price = item.dPrice.Value.ToString("#,##0.0000");
                        outturn.ROE = item.dROE.Value.ToString("#,##0.0000");
                        outturn.AmountUSD = item.dAmountUSD.Value.ToString("#,##0.0000");
                        outturn.AmountTHB = item.dAmountTHB.Value.ToString("#,##0.0000");
                        outturn.Vat7 = item.dVAT.Value.ToString("#,##0.0000");
                        outturn.TotalAmountUSD = item.dTotalAmountUSD.Value.ToString("#,##0.0000");
                        outturn.TotalAmountTHB = item.dTotalAmountTHB.Value.ToString("#,##0.0000");
                        outturn.SAPFIDoc = item.dSAPFIDoc;
                        outturn.SAPFIDocStatus = item.dSAPFIDocStatus;
                        outturn.UpdatedDate = (item.dLastUpdated == null) ? "" : item.dLastUpdated.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.UpdatedBy = (item.dUpdatedBy == null) ? "" : item.dUpdatedBy;
                        outturn.ItemNo = item.dItemNo.ToString();
                        model.sOutturn.Add(outturn);
                        #endregion

                        #region Purchase Order                        

                        var query2 = (from d in context.PIT_PO_HEADER
                                      where d.PO_NO == item.dPoNo
                                      join i in context.PIT_PO_ITEM on d.PO_NO equals i.PO_NO into viewD
                                      from vnd in viewD.DefaultIfEmpty()
                                          //join j in context.MT_VENDOR on Convert.ToDecimal(vnd.ACC_NUM_VENDOR) equals Convert.ToDecimal(j.VND_ACC_NUM_VENDOR) into viewS
                                          //from vds in viewS.DefaultIfEmpty()
                                      select new
                                      {
                                          dTripNo = vnd.TRIP_NO,
                                          dPoDate = d.PO_DATE,
                                          dProduct = vnd.MET_NUM,
                                          dSupplier = vnd.ACC_NUM_VENDOR,
                                          dIncoterm = d.INCOTERMS,
                                          dVolume = vnd.VOLUME,
                                          dVolumeUnit = vnd.VOLUME_UNIT,
                                          dPrice = vnd.UNIT_PRICE,
                                          dTotal = vnd.VOLUME * vnd.UNIT_PRICE,
                                          dCurrency = d.CURRENCY
                                      });
                        if (query2 != null && query2.ToList().Count() > 0)
                        {
                            var item2 = query2.ToList()[0];
                            model.TripNo = item2.dTripNo;
                            model.PoNo = item.dPoNo;
                            model.PoItem = item.dPoItem.ToString();
                            model.PoDate = item2.dPoDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                            model.Product = item2.dProduct;
                            model.Supplier = GetSupplier(item2.dSupplier);
                            model.Incoterms = item2.dIncoterm;
                            model.Volume = item2.dVolume.Value.ToString("#,##0.0000");
                            model.VolumeUnit = item2.dVolumeUnit;
                            model.Price = item2.dPrice.Value.ToString("#,##0.0000");
                            model.Total = item2.dTotal.Value.ToString("#,##0.0000");
                            model.Currency = item2.dCurrency;
                        }
                        #endregion
                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetSupplier(string supplierId)
        {
            supplierId = Convert.ToDecimal(supplierId).ToString();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = (from q in context.MT_VENDOR
                             where q.VND_ACC_NUM_VENDOR == supplierId
                             select new
                             {
                                 dSupplierName = q.VND_NAME1
                             });
                if (query != null && query.ToList().Count() > 0)
                {
                    return query.ToList()[0].dSupplierName;
                }
            }

            return "";
        }

        public PCFPurchaseEntryViewModel_SearchData SearchPCFTrain(string year, string documentNumber)
        {
            System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            PCFPurchaseEntryViewModel_SearchData model = new PCFPurchaseEntryViewModel_SearchData();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.PCF_PURCHASE_ENTRY_DAILY
                                 where c.SAP_FI_DOC_NO == documentNumber
                                 select new
                                 {
                                     dItemNo = c.ITEM_NO,
                                     dBLDate = c.BL_DATE,
                                     dBLVolumeBBL = c.BL_VOLUME_BBL,
                                     dBLVolumeMT = c.BL_VOLUME_MT,
                                     dBLVolumnL30 = c.BL_VOLUME_L30,
                                     dGRDate = c.GR_DATE,
                                     dGRVolumeBBL = c.GR_VOLUME_BBL,
                                     dGRVolumnMT = c.GR_VOLUME_MT,
                                     dGRVolumnL30 = c.GR_VOLUME_L30,
                                     dFOBPrice = c.FOB_PRICE,
                                     dFreightPrice = c.FRT_PRICE,
                                     dCnFPrice = c.CF_PRICE,
                                     dROE = c.ROE,
                                     dFOBAmountUSD = c.FOB_AMOUNT_USD,
                                     dFOBAmountTHB = c.FOB_AMOUNT_THB,
                                     dFreightAmountUSD = c.FRT_AMOUNT_USD,
                                     dFreightAmountTHB = c.FRT_AMOUNT_THB,
                                     dCnFAmountUSD = c.CF_AMOUNT_USD,
                                     dCnFAmountTHB = c.CF_AMOUNT_THB,
                                     dVAT = c.CF_VAT,
                                     dTotalAmountUSD = c.CF_TOTAL_USD,
                                     dTotalAmountTHB = c.CF_TOTAL_THB,
                                     dSAPFIDoc = c.SAP_FI_DOC_NO,
                                     dSAPFIDocStatus = c.SAP_FI_DOC_STATUS,
                                     dLastUpdated = c.UPDATED_DATE,
                                     dUpdatedBy = c.UPDATED_BY,
                                     dTripNo = c.TRIP_NO
                                 });

                    if (query != null && query.ToList().Count() > 0)
                    {
                        var item = query.ToList()[0];
                        model.sOutturn = new List<PCFPurchaseEntryViewModel_Outtern>();
                        PCFPurchaseEntryViewModel_Outtern outturn = new PCFPurchaseEntryViewModel_Outtern();
                        outturn.ItemNo = item.dItemNo.ToString();
                        outturn.BLDate = item.dBLDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.BlQtyBbl = (item.dBLVolumeBBL ?? 0).ToString("#,##0.0000");
                        outturn.BlQtyMt = (item.dBLVolumeMT ?? 0).ToString("#,##0.0000");
                        outturn.BlQtyMl = (item.dBLVolumnL30 ?? 0).ToString("#,##0.0000");
                        outturn.PostingDate = item.dGRDate.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.QtyBbl = (item.dGRVolumeBBL ?? 0).ToString("#,##0.0000");
                        outturn.QtyMt = (item.dGRVolumnMT ?? 0).ToString("#,##0.0000");
                        outturn.QtyMl = (item.dGRVolumnL30 ?? 0).ToString("#,##0.0000");
                        outturn.FOBPrice = (item.dFOBPrice ?? 0).ToString("#,##0.0000");
                        outturn.FreightPrice = (item.dFreightPrice ?? 0).ToString("#,##0.0000");
                        outturn.CnFPrice = (item.dCnFPrice ?? 0).ToString("#,##0.0000");
                        outturn.ROE = (item.dROE ?? 0).ToString("#,##0.0000");
                        outturn.FOBAmountUSD = (item.dFOBAmountUSD ?? 0).ToString("#,##0.0000");
                        outturn.FOBAmountTHB = (item.dFOBAmountTHB ?? 0).ToString("#,##0.0000");
                        outturn.FreightAmountUSD = (item.dFreightAmountUSD ?? 0).ToString("#,##0.0000");
                        outturn.FreightAmountTHB = (item.dFreightAmountTHB ?? 0).ToString("#,##0.0000");
                        outturn.CnFAmountUSD = (item.dCnFAmountUSD ?? 0).ToString("#,##0.0000");
                        outturn.CnFAmountTHB = (item.dCnFAmountTHB ?? 0).ToString("#,##0.0000");
                        outturn.CnFVat7 = item.dVAT.ToString("#,##0.0000");
                        outturn.CnFTotalAmountUSD = (item.dTotalAmountUSD ?? 0).ToString("#,##0.0000");
                        outturn.CnFTotalAmountTHB = (item.dTotalAmountTHB ?? 0).ToString("#,##0.0000");
                        outturn.SAPFIDoc = item.dSAPFIDoc;
                        outturn.SAPFIDocStatus = item.dSAPFIDocStatus;
                        outturn.UpdatedDate = (item.dLastUpdated == null) ? "" : item.dLastUpdated.Value.ToString("dd/MM/yyyy", cultureinfo);
                        outturn.UpdatedBy = (item.dUpdatedBy == null) ? "" : item.dUpdatedBy;
                        model.sOutturn.Add(outturn);

                        var query1_2 = (from h in context.PCF_HEADER
                                        where h.PHE_BOOKING_RELEASE == "Y" && (h.PHE_CHANNEL == "VESSEL" || h.PHE_CHANNEL == "TRAIN") && h.PHE_STATUS == "ACTIVE"
                                        && h.PHE_TRIP_NO == item.dTripNo
                                        join m in context.PCF_MATERIAL on h.PHE_TRIP_NO equals m.PMA_TRIP_NO into viewA
                                        from va in viewA.DefaultIfEmpty()
                                        join o in context.OUTTURN_FROM_SAP on h.PHE_TRIP_NO equals o.TRIP_NO into viewB
                                        from vb in viewB.DefaultIfEmpty()
                                        join v in context.MT_VEHICLE on h.PHE_VESSEL equals v.VEH_ID into viewC
                                        from vc in viewC.DefaultIfEmpty()
                                        join mt in context.MT_MATERIALS on va.PMA_MET_NUM equals mt.MET_NUM into viewD
                                        from vd in viewD.DefaultIfEmpty()
                                        join s in context.MT_VENDOR on va.PMA_SUPPLIER equals s.VND_ACC_NUM_VENDOR into viewE
                                        from ve in viewE.DefaultIfEmpty()
                                        join t in context.PCF_MATERIAL_PRICE on h.PHE_TRIP_NO equals t.PMP_TRIP_NO into viewF
                                        from vf in viewF.DefaultIfEmpty()
                                        select new
                                        {
                                            dTripNo = h.PHE_TRIP_NO,
                                            dMetNum = va.PMA_MET_NUM,
                                            dVessel = h.PHE_CHANNEL,
                                            dVesselType = vc.VEH_TYPE,
                                            dPoNo = va.PMA_PO_NO,
                                            dProduct = vd.MET_MAT_DES_ENGLISH,
                                            dSupplier = ve.VND_NAME1,
                                            dSupplierNo = ve.VND_ACC_NUM_VENDOR,
                                            dVolumeBBL = va.PMA_VOLUME_BBL,
                                            dVolumeMT = va.PMA_VOLUME_MT,
                                            dPurchaseType = va.PMA_PURCHASE_TYPE,
                                            dCompanyCode = h.PHE_COMPANY_CODE,
                                            dCurrency = vf.PMP_CURRENCY,
                                            dMatItemNo = va.PMA_ITEM_NO
                                        });


                        //if (!string.IsNullOrEmpty(sPoNo))
                        //{
                        //    query = query.Where(p => p.dPoNo.Contains(sPoNo));
                        //}

                        //var query2 = (from h in context.PCF_HEADER
                        //              where h.PHE_TRIP_NO == item.dTripNo
                        //              join m in context.PCF_MATERIAL on item.dTripNo equals m.PMA_TRIP_NO into viewM
                        //              from vnm in viewM.DefaultIfEmpty()
                        //              select new
                        //              {
                        //                  dVessel = h.PHE_VESSEL,
                        //                  dCrude = vnm.PMA_MET_NUM,
                        //                  dQtyBBL = vnm.PMA_VOLUME_BBL,
                        //                  dQtyKMT = vnm.PMA_VOLUME_MT,
                        //                  dPurchaseType = vnm.PMA_PURCHASE_TYPE
                        //              });
                        if (query1_2 != null && query1_2.ToList().Count() > 0)
                        {
                            query1_2 = query1_2.Distinct();
                            var query1 = query1_2.ToList();
                            var query2 = query1.GroupBy(x => new { x.dTripNo, x.dMetNum }).Select(grp => grp.First()).ToList();

                            var item2 = query2.ToList()[0];
                            model.TripNo = item2.dTripNo;
                            model.PoNo = item2.dPoNo;
                            model.Vessel = item2.dVessel;
                            model.Crude = item2.dProduct;
                            model.Supplier = item2.dSupplier;
                            model.VolumeBBL = item2.dVolumeBBL.Value.ToString("#,##0.0000");
                            model.VolumeMT = item2.dVolumeMT.Value.ToString("#,##0.0000");
                            model.PurchaseType = GetPurchaseType(item2.dPurchaseType.ToString());
                            model.FIDoc = item.dSAPFIDoc;
                            model.MatItemNo = item2.dMatItemNo.ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {

            }

            return model;
        }

        public string GetPurchaseType(string code)
        {
            try
            {

                var _CIPConfig = CrudeImportPlanServiceModel.getCIPConfig("CIP_IMPORT_PLAN");
                if (_CIPConfig != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var purchaseType = js.Serialize(_CIPConfig.PCF_MT_PURCHASE_TYPE);
                    var dataList = (List<PCF_MT_PURCHASE_TYPE>)Newtonsoft.Json.JsonConvert.DeserializeObject(purchaseType, typeof(List<PCF_MT_PURCHASE_TYPE>));
                    var result = dataList.Where(x => x.CODE == code);

                    if (result != null && result.ToList().Count() != 0)
                    {
                        return result.ToList()[0].NAME;
                    }
                    else
                    {
                        return "N/A";
                    }
                }
                else
                {
                    return "N/A";
                }
            }
            catch (Exception ex)
            {
                return "N/A";
            }
        }

        public PurchaseEntryVesselViewModel SearchPCFVessel(string year, string documentNumber)
        {
            PurchaseEntryVesselViewModel pModel = new PurchaseEntryVesselViewModel();
            
            //PurchaseEntryVesselServiceModel pModel = new PurchaseEntryVesselServiceModel();
            
            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";
            string TripNo = "";
            string MetNum = "";
            #region Checking requred conditions
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                try
                {
                    var query_HEADER = (from pe in context.PCF_PURCHASE_ENTRY
                                        where pe.PPE_SAP_FI_DOC_NO == documentNumber 
                                        //&& (pe.PPE_BL_DATE ?? DateTime.Now).ToString("yyyy", provider) == year
                                        join phe in context.PCF_HEADER on pe.PPE_TRIP_NO equals phe.PHE_TRIP_NO
                                        join otr in context.OUTTURN_FROM_SAP on phe.PHE_TRIP_NO equals otr.TRIP_NO
                                        join mtm in context.MT_MATERIALS on otr.MAT_NUM equals mtm.MET_NUM
                                        join mtv in context.MT_VEHICLE on phe.PHE_VESSEL equals mtv.VEH_ID
                                        where phe.PHE_TRIP_NO.ToUpper().Equals(pe.PPE_TRIP_NO.ToUpper()) &&
                                            otr.MAT_NUM == pe.PPE_MET_NUM
                                        select new
                                        {
                                            TripNo = pe.PPE_TRIP_NO,
                                            Vessel = phe.PHE_VESSEL,
                                            MetNum = pe.PPE_MET_NUM,
                                            txtVeh = mtv != null ? mtv.VEH_VEH_TEXT : phe.PHE_VESSEL,
                                            txtMet = mtm != null ? mtm.MET_MAT_DES_ENGLISH : MetNum,
                                            PostingDate = pe.PPE_GR_DATE,
                                            BLQtyML = pe.PPE_BL_VOLUME_ML,
                                            BLQtyMT = pe.PPE_BL_VOLUME_MT,
                                            BLQtyBBL = pe.PPE_BL_VOLUME_BBL,
                                            QtyML = pe.PPE_GR_VOLUME_ML,
                                            QtyMT = pe.PPE_GR_VOLUME_MT,
                                            QtyBBL = pe.PPE_GR_VOLUME_BBL,
                                            CompanyCode = phe.PHE_COMPANY_CODE,
                                            ItemNo = pe.PPE_ITEM_NO,
                                            BLDate = pe.PPE_BL_DATE,
                                            InvoiceFigure = pe.PPE_CAL_INVOICE_FIGURE,
                                            VolumnUnits = pe.PPE_CAL_VOLUME_UNIT,
                                            FOBPriceUSD = pe.PPE_FOB_PRICE_USD,
                                            FOBTotalUSD = pe.PPE_FOB_TOTAL_USD,
                                            FOBROE = pe.PPE_FOB_ROE,
                                            FOBTotalTHB = pe.PPE_FOB_TOTAL_THB,
                                            FrtTotalUSD = pe.PPE_FRT_TOTAL_USD,
                                            FrtTotalTHB = pe.PPE_FRT_TOTAL_THB,
                                            FobFrtTotalUSD = pe.PPE_FOB_FRT_TOTAL_USD,
                                            FobFrtTotalTHB = pe.PPE_FOB_FRT_TOTAL_THB,
                                            InsPriceUSD = pe.PPE_INS_PRICE_USD,
                                            FOBSupplier = pe.PPE_FOB_SUPPLIER,
                                            FRTPriceUSD = pe.PPE_FRT_PRICE_USD,
                                            FRTROE = pe.PPE_FRT_ROE,
                                            INSTotalUSD = pe.PPE_INS_TOTAL_USD,
                                            InsuranceRate = pe.PPE_INS_RATE,
                                            StampDutyTotalUSD = pe.PPE_STAMP_DUTY_TOTAL_USD,
                                            InsStampDutyRate = pe.PPE_INS_STAMP_DUTY_RATE,
                                            LossGain = pe.PPE_LOSS_GAIN,
                                            MRCInsTotalUSD = pe.PPE_MRC_INS_TOTAL_USD,
                                            MRCInsROE = pe.PPE_MRC_INS_ROE,
                                            MRCInsTotalTHB = pe.PPE_MRC_INS_TOTAL_THB,
                                            TotalCIFPriceUSD = pe.PPE_TOTAL_CIF_PRICE_USD,
                                            TotalCIFTotalUSD = pe.PPE_TOTAL_CIF_TOTAL_USD,
                                            TotalCIFTotalTHB = pe.PPE_TOTAL_CIF_TOTAL_THB,
                                            RemarkFOBUSD = pe.PPE_REMARK_FOB_USD,
                                            RemarkFOBTHB = pe.PPE_REMARK_FOB_THB,
                                            RemarkFRTUSD = pe.PPE_REMARK_FRT_USD,
                                            RemarkFRTTHB = pe.PPE_REMARK_FRT_THB,
                                            RemarkINSUSD = pe.PPE_REMARK_INS_USD,
                                            RemarkINSTHB = pe.PPE_REMARK_INS_THB,
                                            RemarkImportDutyUSD = pe.PPE_REMARK_IMPORT_DUTY_USD,
                                            RemarkImportDutyROE = pe.PPE_REMARK_IMPORT_DUTY_ROE,
                                            RemarkImportDutyTHB = pe.PPE_REMARK_IMPORT_DUTY_THB,
                                            RemarkTotalUSD = pe.PPE_REMARK_TOTAL_USD,
                                            RemarkTotalTHB = pe.PPE_REMARK_TOTAL_THB,
                                            
                                        }).ToList();
                    
                    if (query_HEADER.ToList().Count > 0)
                    {
                        TripNo = query_HEADER.ToList().FirstOrDefault().TripNo;
                        MetNum = query_HEADER.ToList().FirstOrDefault().MetNum;

                        #region Material
                        var query_MATERIAL = (from pma in context.PCF_MATERIAL
                                              join pmp in context.PCF_MATERIAL_PRICE on pma.PMA_TRIP_NO equals pmp.PMP_TRIP_NO
                                              join ctv in context.PCF_CUSTOMS_VAT on pma.PMA_TRIP_NO equals ctv.TRIP_NO
                                              join ven in context.MT_VENDOR on pma.PMA_SUPPLIER equals ven.VND_ACC_NUM_VENDOR
                                              where pma.PMA_TRIP_NO.ToUpper().Equals(TripNo.ToUpper()) &&
                                              pma.PMA_MET_NUM == MetNum &&
                                              pma.PMA_ITEM_NO == pmp.PMP_MAT_ITEM_NO &&
                                              pma.PMA_ITEM_NO == ctv.MAT_ITEM_NO
                                              select new
                                              {
                                                  BLDate = pma.PMA_BL_DATE,
                                                  DueDate = pma.PMA_DUE_DATE,
                                                  Supplier = pma.PMA_SUPPLIER,
                                                  TxtSupplier = ven.VND_NAME1,
                                                  VolumeBBL = pma.PMA_VOLUME_BBL,
                                                  VolumeML = pma.PMA_VOLUME_ML,
                                                  VolumeMT = pma.PMA_VOLUME_MT,
                                                  MatItemNo = pma.PMA_ITEM_NO,
                                                  SupplierNo = pma.PMA_SUPPLIER,
                                                  Currency = pmp.PMP_CURRENCY,
                                                  PriceUnitUSD = pmp.PMP_BASE_PRICE,
                                                  FxAgreement = pmp.PMP_FX_AGREEMENT,
                                                  ItemNo = pmp.PMP_ITEM_NO,
                                                  PriceStatus = pmp.PMP_PRICE_STATUS,
                                                  TotalPrice = pmp.PMP_TOTAL_PRICE,
                                                  INS_RATE = ctv.INSURANCE_RATE,
                                                  InvoiceNo = pmp.PMP_INVOICE == null ? "No" : pmp.PMP_INVOICE,
                                                  ROE = ctv.ROE,
                                                  VatImportDuty = ctv.IMPORT_DUTY
                                              }).OrderByDescending(p => p.MatItemNo).OrderByDescending(a => a.PriceStatus == "A").OrderByDescending(a => a.PriceStatus == "P").ToList();
                        #endregion
                        #region Freight
                        var query_FREIGHT = (from fed in context.PCF_FREIGHT
                                             join fedt in context.PCF_MT_FREIGHT_TYPE on fed.PFR_FREIGHT_SUB_TYPE equals fedt.PMF_CODE
                                             join van in context.MT_VENDOR on fed.PFR_SUPPLIER equals van.VND_ACC_NUM_VENDOR into vanj
                                             from van in vanj.DefaultIfEmpty()
                                             where fed.PFR_TRIP_NO == TripNo
                                             select new
                                             {
                                                 fed,
                                                 fedname = fedt.PMF_NAME,
                                                 vanname = van == null ? "Unknow" : van.VND_NAME1
                                             }).ToList().OrderBy(z => z.fed.PFR_ITEM_NO);
                        #endregion

                        string JsonMaster_PTT = MasterData.GetJsonMasterSetting("PCF_PTT_AGREEMENT");
                        PCF_PTT_AGREEMENT dataList = (PCF_PTT_AGREEMENT)Newtonsoft.Json.JsonConvert.DeserializeObject(JsonMaster_PTT.ToString(), typeof(PCF_PTT_AGREEMENT));

                        decimal total_All = Convert.ToDecimal(context.PCF_MATERIAL.Where(a => a.PMA_TRIP_NO == TripNo).Select(a => a.PMA_VOLUME_MT).Sum());
                        decimal total_Met = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeMT).Sum());
                        decimal total_BL_MT = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyMT).Sum());
                        decimal total_BL_ML = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyML).Sum());
                        decimal total_BL_BBL = Convert.ToDecimal(query_HEADER.Select(a => a.BLQtyBBL).Sum());
                        decimal total_OT_MT = Convert.ToDecimal(query_HEADER.Select(a => a.QtyMT).Sum());
                        decimal total_OT_ML = Convert.ToDecimal(query_HEADER.Select(a => a.QtyML).Sum());
                        decimal total_OT_BBL = Convert.ToDecimal(query_HEADER.Select(a => a.QtyBBL).Sum());
                        decimal total_VOL_MT = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeMT).Sum());
                        decimal total_VOL_ML = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeML).Sum());
                        decimal total_VOL_BBL = Convert.ToDecimal(query_MATERIAL.Select(a => a.VolumeBBL).Sum());
                        double total_IMPORT_DUTY = Convert.ToDouble(query_MATERIAL.Select(a => a.VatImportDuty).Sum());

                        var SumNetTotalVolumeBBL = 0.0;
                        var SumNetTotalPriceBBLUSD = 0.0;
                        var SumNetTotalPriceBBLTHB = 0.0;
                        var SumNetTotalVolumeML = 0.0;
                        var SumNetTotalPriceMLUSD = 0.0;
                        var SumNetTotalPriceMLTHB = 0.0;
                        var SumNetTotalVolumeMT = 0.0;
                        var SumNetTotalPriceMTUSD = 0.0;
                        var SumNetTotalPriceMTTHB = 0.0;

                        //        if (query_HEADER.Count() != 0)
                        //        {
                        string INS_RATE = query_MATERIAL[0].INS_RATE.ToString();
                        string Currency = query_MATERIAL[0].Currency;
                        string SupplierNo = query_MATERIAL[0].SupplierNo;
                        string BLDate = Convert.ToDateTime((query_MATERIAL.OrderBy(p => p.BLDate).ToList()[0].BLDate).ToString()).ToString(format, provider);
                        string DueDate = Convert.ToDateTime((query_MATERIAL.OrderBy(p => p.DueDate).ToList()[0].DueDate).ToString()).ToString(format, provider);
                        string FrtROE = GetRateExchange(DateTime.ParseExact(BLDate, format, provider), context);
                        decimal DDutyROE = Convert.ToDecimal(query_MATERIAL.Sum(a => a.ROE)) / query_MATERIAL.Count();
                        string SDutyROE = String.IsNullOrEmpty(DDutyROE.ToString()) ? "1" : Double.Parse(DDutyROE.ToString()).ToString("#,##0.0000");
                        int ItemNo = 10;
                        int index = 0;
                        foreach (var item in query_HEADER)
                        {
                            string FOBName = "";
                            if (index == 0)
                            {
                                pModel.PriceList = new List<PEV_Price>();
                                pModel.PriceList.Add(new PEV_Price
                                {
                                    SAPDocumentNO = documentNumber,
                                    sTripNo = item.TripNo,//----success
                                    sItemNo = item.ItemNo.ToString(),//----success
                                    sBLDate = (item.BLDate ?? DateTime.Now).ToString(format, provider),//----success
                                    sMetNum = item.MetNum,//----success
                                    sTxtMet = item.txtMet,//----success
                                    sDueDate = DueDate,
                                    Currency = Currency,
                                    sVesselName = item.Vessel,//----success
                                    sTxtVesselName = item.txtVeh,//----success
                                    sPostingDate = (item.PostingDate ?? DateTime.Now).ToString(format, provider),//----success
                                    sCalVolumeUnit = item.VolumnUnits,//----success
                                    sCalInvFigure = item.InvoiceFigure,//----success
                                    CompanyCode = item.CompanyCode,//----success
                                    SupplierNo = SupplierNo,
                                    INS_DF = INS_RATE,
                                    FOBFRTTotalUSD = item.FobFrtTotalUSD.ToString(),//----success
                                    FOBFRTTotalTHB = item.FobFrtTotalTHB.ToString(),//----success
                                    FRTTotalTHB = item.FrtTotalTHB.ToString(),//----success
                                    FRTTotalUSD = item.FrtTotalUSD.ToString(),//----success
                                    FOBTotalUSD = item.FOBTotalUSD.ToString(),//----success
                                    FOBROE = item.FOBROE.ToString(),//----success
                                    FOBTotalTHB = item.FOBTotalTHB.ToString(),//----success
                                    FOBPriceUSD = item.FOBPriceUSD.ToString(),//----success
                                    FRTPriceUSD = item.FRTPriceUSD.ToString(),
                                    FOBSupplier = item.FOBSupplier,
                                    INSPriceUSD = item.InsPriceUSD.ToString(),//----success

                                    FRTROE = item.FRTROE.ToString(),
                                    INSTotalUSD = item.INSTotalUSD.ToString(),
                                    InsuranceRate = item.InsuranceRate.ToString(),
                                    StampDutyTotalUSD = item.StampDutyTotalUSD.ToString(),
                                    InsStampDutyRate = item.InsStampDutyRate.ToString(),
                                    LossGain = item.LossGain.ToString(),
                                    MRCInsTotalUSD = item.MRCInsTotalUSD.ToString(),
                                    MRCInsROE = item.MRCInsROE.ToString(),
                                    MRCInsTotalTHB = item.MRCInsTotalTHB.ToString(),
                                    TotalCIFPriceUSD = item.TotalCIFPriceUSD.ToString(),
                                    TotalCIFTotalUSD = item.TotalCIFTotalUSD.ToString(),
                                    TotalCIFTotalTHB = item.TotalCIFTotalTHB.ToString(),
                                    RemarkFOBUSD = item.RemarkFOBUSD.ToString(),
                                    RemarkFOBTHB = item.RemarkFOBTHB.ToString(),
                                    RemarkFRTUSD = item.RemarkFRTUSD.ToString(),
                                    RemarkFRTTHB = item.RemarkFRTTHB.ToString(),
                                    RemarkINSUSD = item.RemarkINSUSD.ToString(),
                                    RemarkINSTHB = item.RemarkINSTHB.ToString(),
                                    RemarkImportDutyUSD = item.RemarkImportDutyUSD.ToString(),
                                    RemarkImportDutyROE = item.RemarkImportDutyROE.ToString(),
                                    RemarkImportDutyTHB = item.RemarkImportDutyTHB.ToString(),
                                    RemarkTotalUSD = item.RemarkTotalUSD.ToString(),
                                    RemarkTotalTHB = item.RemarkTotalTHB.ToString(),
                                    ddl_ItemNo = new List<SelectListItem>(),
                                    Fed_Calculate = new FED_Calculate(),
                                    QuantityList = new List<PEV_Quantity>(),
                                    FreightList = new List<PEV_Freight>(),
                                    PriceSupplierList = new List<PEV_PriceSupplier>()
                                });
                                pModel.PriceList[index].QuantityList = new List<PEV_Quantity>();

                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "ML",
                                    QtyBL = String.IsNullOrEmpty(total_BL_ML.ToString()) ? "0" : Double.Parse(total_BL_ML.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_ML.ToString()) ? "0" : Double.Parse(total_OT_ML.ToString()).ToString("#,##0.0000")
                                });
                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "MT",
                                    QtyBL = String.IsNullOrEmpty(total_BL_MT.ToString()) ? "0" : Double.Parse(total_BL_MT.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_MT.ToString()) ? "0" : Double.Parse(total_OT_MT.ToString()).ToString("#,##0.0000")
                                });
                                pModel.PriceList[index].QuantityList.Add(new PEV_Quantity
                                {
                                    QuantityUnit = "BBL",
                                    QtyBL = String.IsNullOrEmpty(total_BL_BBL.ToString()) ? "0" : Double.Parse(total_BL_BBL.ToString()).ToString("#,##0.0000"),
                                    QtyOutTurn = String.IsNullOrEmpty(total_OT_BBL.ToString()) ? "0" : Double.Parse(total_OT_BBL.ToString()).ToString("#,##0.0000")
                                });

                                pModel.PriceList[index].FreightList = new List<PEV_Freight>();
                                foreach (var item_frt in query_FREIGHT)
                                {
                                    decimal result_price = Math.Round((Convert.ToDecimal(item_frt.fed.PFR_AMOUNT) * total_Met) / total_All, 2, MidpointRounding.AwayFromZero);
                                    pModel.PriceList[index].FreightList.Add(new PEV_Freight
                                    {
                                        FRTSupplier = item_frt.fed.PFR_SUPPLIER,
                                        FRTSubItem = item_frt.fed.PFR_FREIGHT_SUB_TYPE.ToString(),
                                        FRTSupplierTxt = item_frt.fedname + " (" + item_frt.vanname + ")",
                                        FRTItemNo = item_frt.fed.PFR_ITEM_NO.ToString(),
                                        FRTTotalUSD = result_price.ToString(),
                                        FRTROE = FrtROE,
                                        FRTTotalTHB = " "
                                    });
                                }

                                SumNetTotalVolumeBBL = 0.0;
                                SumNetTotalPriceBBLUSD = 0.0;
                                SumNetTotalPriceBBLTHB = 0.0;
                                SumNetTotalVolumeML = 0.0;
                                SumNetTotalPriceMLUSD = 0.0;
                                SumNetTotalPriceMLTHB = 0.0;
                                SumNetTotalVolumeMT = 0.0;
                                SumNetTotalPriceMTUSD = 0.0;
                                SumNetTotalPriceMTTHB = 0.0;

                                pModel.PriceList[index].PriceSupplierList = new List<PEV_PriceSupplier>();
                                foreach (var item_supplier in query_MATERIAL)
                                {
                                    FOBName = FOBName + item_supplier.TxtSupplier + " ,";
                                    string Sup_BLDate = Convert.ToDateTime(item_supplier.BLDate.ToString()).ToString(format, provider);
                                    string Sup_ROE = GetRateExchange(DateTime.ParseExact(BLDate, format, provider), context);
                                    Sup_ROE = item_supplier.SupplierNo == dataList.PTT_VENDER.ToString() ? String.IsNullOrEmpty(item_supplier.FxAgreement.ToString()) ? Double.Parse(Sup_ROE).ToString("#,##0.0000") : Double.Parse((item_supplier.FxAgreement).ToString()).ToString("#,##0.0000") : Double.Parse(Sup_ROE).ToString("#,##0.0000");
                                    decimal VolBBL = String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeBBL);
                                    decimal VolML = String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeML);
                                    decimal VolMT = String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? 0 : Convert.ToDecimal(item_supplier.VolumeMT);
                                    VolBBL = total_VOL_BBL != 0 ? total_VOL_BBL == Convert.ToDecimal(item.BLQtyBBL) ? VolBBL : (Convert.ToDecimal(item.BLQtyBBL) * VolBBL) / total_VOL_BBL : VolBBL;
                                    VolBBL = total_VOL_ML != 0 ? total_VOL_ML == Convert.ToDecimal(item.BLQtyML) ? VolML : (Convert.ToDecimal(item.BLQtyML) * VolML) / total_VOL_ML : VolML;
                                    VolBBL = total_VOL_MT != 0 ? total_VOL_MT == Convert.ToDecimal(item.BLQtyMT) ? VolMT : (Convert.ToDecimal(item.BLQtyMT) * VolMT) / total_VOL_MT : VolMT;
                                    string PriceUsd = Double.Parse(((item_supplier.VolumeBBL == null ? 0 : item_supplier.VolumeBBL) * (item_supplier.PriceUnitUSD == null ? 0 : item_supplier.PriceUnitUSD)).ToString()).ToString("#,##0.0000");
                                    string PriceThb = (Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd) * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE)).ToString("#,##0.0000");
                                    string UnitUsd = Double.Parse(String.IsNullOrEmpty(item_supplier.TotalPrice.ToString()) ? "0" : item_supplier.TotalPrice.ToString()).ToString("#,##0.0000");
                                    string Vol_BBL = Double.Parse(VolBBL.ToString()).ToString("#,##0.0000");
                                    string Vol_Ml = Double.Parse(VolML.ToString()).ToString("#,##0.0000");
                                    string Vol_Mt = Double.Parse(VolMT.ToString()).ToString("#,##0.0000");

                                    SumNetTotalVolumeBBL += Double.Parse(String.IsNullOrEmpty(Vol_BBL) ? "0" : Vol_BBL);
                                    SumNetTotalPriceBBLUSD += Double.Parse(String.IsNullOrEmpty(PriceUsd) ? "0" : PriceUsd);
                                    SumNetTotalPriceBBLTHB += Double.Parse(String.IsNullOrEmpty(PriceThb) ? "0" : PriceThb);
                                    SumNetTotalVolumeML += Double.Parse(String.IsNullOrEmpty(Vol_Ml) ? "0" : Vol_Ml);
                                    SumNetTotalPriceMLUSD += SumNetTotalVolumeML * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                    SumNetTotalPriceMLTHB += SumNetTotalPriceMLUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);
                                    SumNetTotalVolumeMT += Double.Parse(String.IsNullOrEmpty(Vol_Mt) ? "0" : Vol_Mt);
                                    SumNetTotalPriceMTUSD += SumNetTotalVolumeMT * Double.Parse(String.IsNullOrEmpty(UnitUsd) ? "0" : UnitUsd);
                                    SumNetTotalPriceMTTHB += SumNetTotalPriceMTUSD * Double.Parse(String.IsNullOrEmpty(Sup_ROE) ? "1" : Sup_ROE);

                                    pModel.PriceList[index].PriceSupplierList.Add(
                                        new PEV_PriceSupplier
                                        {
                                            txtSupplierName = item_supplier.TxtSupplier,
                                            SupplierName = item_supplier.Supplier,
                                            VolumeBBL = Vol_BBL,
                                            OriginalVolumeBBL = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeBBL.ToString()) ? "0" : item_supplier.VolumeBBL.ToString()).ToString("#,##0.0000"),
                                            VolumeML = Vol_Ml,
                                            OriginalVolumeML = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeML.ToString()) ? "0" : item_supplier.VolumeML.ToString()).ToString("#,##0.0000"),
                                            VolumeMT = Vol_Mt,
                                            OriginalVolumeMT = Double.Parse(String.IsNullOrEmpty(item_supplier.VolumeMT.ToString()) ? "0" : item_supplier.VolumeMT.ToString()).ToString("#,##0.0000"),
                                            PriceUnitUSD = UnitUsd,
                                            PriceTotalUSD = PriceUsd,
                                            ROE = Sup_ROE,
                                            PriceTotalTHB = PriceThb,
                                            NetTotalVolumeBBL = "",
                                            NetTotalPriceBBLUSD = "",
                                            NetTotalPriceBBLTHB = "",
                                            NetTotalVolumeML = "",
                                            NetTotalPriceMLUSD = "",
                                            NetTotalPriceMLTHB = "",
                                            NetTotalVolumeMT = "",
                                            NetTotalPriceMTUSD = "",
                                            NetTotalPriceMTTHB = "",
                                            MatItemNo = item_supplier.MatItemNo.ToString(),
                                            PriceStatus = item_supplier.PriceStatus,
                                            InvoiceNo = item_supplier.InvoiceNo,
                                            HVolumeBBL = Vol_BBL,
                                            HVolumeML = Vol_Ml,
                                            HVolumeMT = Vol_Mt
                                        });
                                }
                                FOBName = FOBName.Remove(FOBName.Length - 1);
                                pModel.PriceList[index].FOBSupplier = FOBName;
                                pModel.PriceList[index].NetTotalVolumeBBL = SumNetTotalVolumeBBL.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceBBLUSD = SumNetTotalPriceBBLUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceBBLTHB = SumNetTotalPriceBBLTHB.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalVolumeML = SumNetTotalVolumeML.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMLUSD = SumNetTotalPriceMLUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMLTHB = SumNetTotalPriceMLTHB.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalVolumeMT = SumNetTotalVolumeMT.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMTUSD = SumNetTotalPriceMTUSD.ToString("#,##0.0000");
                                pModel.PriceList[index].NetTotalPriceMTTHB = SumNetTotalPriceMTTHB.ToString("#,##0.0000");
                                if (query_HEADER.Count() == 1)
                                {
                                    pModel.PriceList.Add(pModel.PriceList[0]);
                                    pModel.PriceList.Add(pModel.PriceList[0]);
                                    break;
                                }
                            }
                            FOBName = "";

                           
                        }
                        pModel.bPartly = false;
                        pModel.bPartlyComplete = false;
                        pModel.sCalInvFigure = "";
                        pModel.sCalVolumeUnit = "";
                        pModel.SAPDocumentDate = DateTime.Now.ToString("dd/MM/yyyy");
                        pModel.SAPPostingDate = DateTime.Now.ToString("dd/MM/yyyy");
                        pModel.SAPReverseDateForTypeA = "01/" + DateTime.Now.AddMonths(1).ToString("MM") + "/" + DateTime.Now.AddMonths(1).ToString("yyyy");
                       

                    }
                    else
                    {
                        pModel.PriceList = new List<PEV_Price>();
                    }

                    if (pModel.ddl_InsuranceRate == null)
                    {
                        pModel.ddl_InsuranceRate = new List<SelectListItem>();
                        if(pModel.PriceList.Count > 0)
                        {
                            pModel.ddl_InsuranceRate = PurchaseEntryVesselServiceModel.getCIPConfigInsuranceRate(pModel);
                        }
                        
                    }
                }
                catch (Exception ex)
                {

                }
            }
            
            #endregion
            
            return pModel;
            #region Old Data
            //System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
            //PurchaseEntryVesselViewModel model = new PurchaseEntryVesselViewModel();
            //model.PriceList = new List<PEV_Price>();
            //try
            //{
            //    using (EntityCPAIEngine context = new EntityCPAIEngine())
            //    {
            //        var query = (from c in context.PCF_PURCHASE_ENTRY
            //                     where c.PPE_SAP_FI_DOC_NO == documentNumber
            //                     join d in context.PCF_MATERIAL on c.PPE_TRIP_NO equals d.PMA_TRIP_NO into viewD
            //                     from vd in viewD.DefaultIfEmpty()
            //                     join e in context.PCF_MATERIAL_PRICE on vd.PMA_ITEM_NO equals e.PMP_ITEM_NO into viewE
            //                     from ve in viewE.DefaultIfEmpty()
            //                     join f in context.PCF_HEADER on c.PPE_TRIP_NO equals f.PHE_TRIP_NO into viewF
            //                     from vf in viewF.DefaultIfEmpty()
            //                     select new
            //                     {
            //                         dTripNo = c.PPE_TRIP_NO,
            //                         dBLDate = c.PPE_BL_DATE,
            //                         dMetNum = c.PPE_MET_NUM,
            //                         dDueDate = ve.PMP_DUE_DATE,
            //                         dVessel = vf.PHE_VESSEL,
            //                         dGRDate = c.PPE_GR_DATE,
            //                         dBLVolumeML = c.PPE_BL_VOLUME_ML,
            //                         dBLVolumeMT = c.PPE_BL_VOLUME_MT, 
            //                         dBLVolumeBBL = c.PPE_BL_VOLUME_BBL,
            //                         dGRVolumeML = c.PPE_GR_VOLUME_ML,
            //                         dGRVolumeMT = c.PPE_GR_VOLUME_MT,
            //                         dGRVolumeBBL = c.PPE_GR_VOLUME_BBL,
            //                         dFOBTotalUSD = c.PPE_FOB_TOTAL_USD,
            //                         dFRTTotalUSD = c.PPE_FRT_TOTAL_USD,
            //                         dMRCIncTotalUSD = c.PPE_MRC_INS_TOTAL_USD,
            //                         dTotalCIFTotalUSD = c.PPE_TOTAL_CIF_TOTAL_USD,
            //                         dFOBTotalTHB = c.PPE_FOB_TOTAL_THB,
            //                         dFRTTotalTHB = c.PPE_FRT_TOTAL_THB,
            //                         dMRCIncTotalTHB = c.PPE_MRC_INS_TOTAL_THB,
            //                         dTotalCIFTotalTHB = c.PPE_TOTAL_CIF_TOTAL_THB
            //                     }).Distinct();

            //        if (query != null && query.ToList().Count() > 0)
            //        {
            //            model.PriceSupplierList = new List<PEV_PriceSupplier>();
            //            model.PriceList = new List<PEV_Price>() ;
            //            for(int i = 0; i < 3; i ++)
            //            {

            //                PEV_Price priceItem = new PEV_Price();
            //                var item = query.ToList()[0];
            //                model.sTripNo = item.dTripNo;
            //                priceItem.sTripNo = item.dTripNo;
            //                priceItem.sBLDate = (item.dBLDate != null) ? item.dBLDate.Value.ToString("dd/MM/yyyy", cultureinfo) : "N/A";
            //                priceItem.sMetNum = item.dMetNum;
            //                priceItem.sDueDate = (item.dDueDate != null) ? item.dDueDate.Value.ToString("dd/MM/yyyy", cultureinfo) : "N/A";
            //                priceItem.sVesselName = item.dVessel;
            //                priceItem.sPostingDate = (item.dGRDate != null) ? item.dGRDate.Value.ToString("dd/MM/yyyy", cultureinfo) : "N/A";
            //                priceItem.PriceSupplierList = new List<PEV_PriceSupplier>();
            //                priceItem.PriceSupplierList.Add(new PEV_PriceSupplier
            //                {
            //                    VolumeBBL = item.dBLVolumeBBL.Value.ToString("#,000.0000"),
            //                    OriginalVolumeBBL = item.dGRVolumeBBL.Value.ToString("#,000.0000"),
            //                    VolumeMT = item.dBLVolumeMT.Value.ToString("#,000.0000"),
            //                    OriginalVolumeMT = item.dGRVolumeMT.Value.ToString("#,000.0000"),
            //                    VolumeML = item.dBLVolumeML.Value.ToString("#,000.0000"),
            //                    OriginalVolumeML = item.dBLVolumeML.Value.ToString("#,000.0000"),
            //                });
            //                priceItem.FOBTotalUSD = item.dFOBTotalUSD.Value.ToString("#,000.0000");
            //                priceItem.FRTTotalUSD = item.dFRTTotalUSD.Value.ToString("#,000.0000");
            //                priceItem.MRCInsTotalUSD = item.dMRCIncTotalUSD.Value.ToString("#,000.0000");
            //                priceItem.TotalCIFTotalUSD = item.dTotalCIFTotalUSD.Value.ToString("#,000.0000");
            //                priceItem.FOBTotalTHB = item.dFOBTotalTHB.Value.ToString("#,000.0000");
            //                priceItem.FRTTotalTHB = item.dFRTTotalTHB.Value.ToString("#,000.0000");
            //                priceItem.MRCInsTotalTHB = item.dMRCIncTotalTHB.Value.ToString("#,000.0000");
            //                priceItem.TotalCIFTotalTHB = item.dTotalCIFTotalTHB.Value.ToString("#,000.0000");
            //                model.PriceList.Add(priceItem);
            //            }


            //            return model;
            //        }
            //    }

            //    return null;
            //}
            //catch(Exception ex)
            //{
            //    return null;
            //}
            #endregion


        }
        public string GetRateExchange(DateTime date, EntityCPAIEngine context)
        {
            CultureInfo provider = new CultureInfo("en-US");
            var tempFx = (from h in context.MKT_TRN_FX_H
                          join b in context.MKT_TRN_FX_B on h.T_FXH_ID equals b.T_FXB_FXHID_FK
                          where (h.T_FXH_BNKCODE == "BOT" &&
                          b.T_FXB_ENABLED == "T" &&
                          b.T_FXB_CUR == "USD" &&
                          b.T_FXB_VALTYPE == "M")
                          select b).OrderByDescending(p => p.T_FXB_VALDATE);

            var queryFx = from d in tempFx.AsEnumerable()
                          where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(date.ToString("yyyyMMdd", provider))
                          select d;
            var test = queryFx.ToList()[0];
            string ROE = queryFx.ToList()[0].T_FXB_VALUE1.ToString();
            return ROE;
        }

        public List<SelectListItem> GetCompany()
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.MT_COMPANY
                                 where (c.MCO_COMPANY_CODE == "1100") || (c.MCO_COMPANY_CODE == "1400")
                                 select new
                                 {
                                     dCompanyCode = c.MCO_COMPANY_CODE,
                                     dCompanyName = c.MCO_SHORT_NAME
                                 }).Distinct();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            selectList.Add(new SelectListItem { Text = item.dCompanyName, Value = item.dCompanyCode });
                        }
                    }
                }
                return selectList;
            }
            catch
            {
                return null;
            }
        }

        public void UpdateTables(PurchaseEntryViewModel_SearchData data, string sapFIDoc, string sapReverseDocNo, ReverseFIDocumentResult result)
        {
            try
            {
                System.Globalization.CultureInfo cultureinfo = new System.Globalization.CultureInfo("en-US");
                string userName = Const.User.Name;
                PIT_PURCHASE_ENTRY ppe = new PIT_PURCHASE_ENTRY();
                PIT_REVERSED_FI_DOC prd = new PIT_REVERSED_FI_DOC();

                ppe.UPDATED_BY = userName;
                ppe.PO_NO = data.PoNo;
                ppe.PO_ITEM = Convert.ToDecimal(data.PoItem);
                ppe.ITEM_NO = Convert.ToDecimal(data.sOutturn[0].ItemNo);
                ppe.SAP_FI_DOC_NO = sapFIDoc;

                prd.COMPANY_CODE = result.CompanyCode;
                prd.PO_NO = data.PoNo;
                prd.PO_DATE = DateTime.ParseExact(data.PoDate, "dd/MM/yyyy", cultureinfo);
                prd.ITEM_NO = Convert.ToDecimal(data.sOutturn[0].ItemNo);
                prd.SAP_FI_DOC_NO = sapFIDoc;
                prd.SAP_REVERSED_DOC_NO = sapReverseDocNo;
                prd.REVERSAL_REASON = "01";
                prd.CREATED_DATE = DateTime.Now;
                prd.CREATED_BY = userName;

                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    PIT_PURCHASE_ENTRY_DAL dal = new PIT_PURCHASE_ENTRY_DAL();
                    dal.Update(ppe, context);

                    PIT_REVERSED_FI_DOC_DAL dal2 = new PIT_REVERSED_FI_DOC_DAL();
                    dal2.Save(prd);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateTablesPCFTrain(string tripNo, string sapFIDoc, string sapReverseDocNo, string matNo, string ItemNo, DateTime poDate)
        {
            string userName = Const.User.Name;
            PCF_DAILY_REVERSED_FI_DOC ppe = new PCF_DAILY_REVERSED_FI_DOC();
            ppe.TRIP_NO = tripNo;
            ppe.MAT_ITEM_NO = decimal.Parse(matNo);
            ppe.ITEM_NO = decimal.Parse(ItemNo);
            ppe.CREATED_BY = userName;
            ppe.CREATED_DATE = DateTime.Now;
            ppe.UPDATED_BY = userName;
            ppe.UPDATED_DATE = DateTime.Now;
            ppe.SAP_FI_DOC_NO = sapFIDoc;
            ppe.SAP_REVERSED_DOC_NO = sapReverseDocNo;
            ppe.PO_DATE = poDate;


            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                PCF_PURCHASE_ENTRY_DAILY_DAL dal = new PCF_PURCHASE_ENTRY_DAILY_DAL();
                dal.SaveReversedFiDoc(ppe);

            }
        }

        public void UpdateTablesPCFVessel(string tripNo, string sapFIDoc, string sapReverseDocNo, string matNo, string ItemNo)
        {
            string userName = Const.User.Name;
            PCF_REVERSED_FI_DOC ppe = new PCF_REVERSED_FI_DOC();
            ppe.CREATED_BY = userName;
            ppe.CREATED_DATE = DateTime.Now;
            ppe.UPDATED_BY = userName;
            ppe.UPDATED_DATE = DateTime.Now;
            ppe.ITEM_NO = decimal.Parse( ItemNo);
            ppe.MET_NUM = matNo;
            ppe.TRIP_NO = tripNo;
            ppe.SAP_FI_DOC_NO = sapFIDoc;
            ppe.SAP_REVERSED_DOC_NO = sapReverseDocNo;
            ppe.REVERSAL_REASON = "";

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                PURCHASE_ENTRY_VESSEL_DAL dal = new PURCHASE_ENTRY_VESSEL_DAL();
                dal.SaveReversedFiDoc(ppe);
            }
        }
    }
}