﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class SOClearLineServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";

        public void Search(ref SOClearLineViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();

            try
            {
                pModel.SOClearLine_Search.SearchData = new List<SOClearLineViewModel_SearchData>();
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var sTripNo = pModel.SOClearLine_Search.sTripNo == null ? "" : pModel.SOClearLine_Search.sTripNo.ToUpper();
                    var sVesselID = pModel.SOClearLine_Search.sVesselName == null ? "" : pModel.SOClearLine_Search.sVesselName.ToUpper();
                    var sCrude = pModel.SOClearLine_Search.sCrude == null ? "" : pModel.SOClearLine_Search.sCrude.ToUpper();
                    var sCustomer = pModel.SOClearLine_Search.sCustomer == null ? "" : pModel.SOClearLine_Search.sCustomer.ToUpper();
                    var sDeliveryDate = pModel.SOClearLine_Search.sDeliveryDate;
                    var sDateFrom = String.IsNullOrEmpty(pModel.SOClearLine_Search.sDeliveryDate) ? DateTime.Now.AddYears(-1).ToString("dd/MM/yyyy") : pModel.SOClearLine_Search.sDeliveryDate.Substring(0, 10);
                    var sDateTo = String.IsNullOrEmpty(pModel.SOClearLine_Search.sDeliveryDate) ? DateTime.Now.ToString("dd/MM/yyyy") : pModel.SOClearLine_Search.sDeliveryDate.Substring(14, 10);

                    DateTime dDateFrom = DateTime.ParseExact(sDateFrom, format, provider);
                    DateTime dDateTo = DateTime.ParseExact(sDateTo, format, provider);

                    var query = (from cl in context.CLEAR_LINE_CRUDE
                                 join mat in context.MT_MATERIALS on cl.CLC_CRUDE_TYPE_ID equals mat.MET_NUM
                                 join veh in context.MT_VEHICLE on cl.CLC_VESSEL_ID equals veh.VEH_ID
                                 join cus in context.MT_CUST_DETAIL on cl.CLC_CUST_NUM equals cus.MCD_ROW_ID
                                 where ((cl.CLC_DELIVERY_DATE >= dDateFrom && cl.CLC_DELIVERY_DATE <= dDateTo) || (String.IsNullOrEmpty(sDeliveryDate)))
                                 && ((cl.CLC_TRIP_NO.ToUpper().Contains(sTripNo.ToUpper())) || String.IsNullOrEmpty(sTripNo))
                                 && ((cl.CLC_VESSEL_ID.ToUpper().Equals(sVesselID.ToUpper())) || (String.IsNullOrEmpty(sVesselID)))
                                 && ((cl.CLC_CRUDE_TYPE_ID.ToUpper().Equals(sCrude.ToUpper())) || (String.IsNullOrEmpty(sCrude)))
                                 && ((cl.CLC_CUST_NUM.ToUpper().Equals(sCustomer.ToUpper())) || (String.IsNullOrEmpty(sCustomer)))
                                 select new
                                 {
                                     cl,
                                     MatName = mat.MET_MAT_DES_ENGLISH,
                                     VesselName = veh.VEH_VEHICLE,
                                     CustomerName = cus.MCD_NAME_1
                                 });

                    if (query != null)
                    {
                        if (pModel.SOClearLine_Search.SearchData == null) { pModel.SOClearLine_Search.SearchData = new List<SOClearLineViewModel_SearchData>(); }

                        foreach (var item in query.ToList())
                        {
                            SOClearLineViewModel_SearchData temp = new SOClearLineViewModel_SearchData();
                            temp.dTripNo = item.cl.CLC_TRIP_NO;
                            temp.dCustomer = item.CustomerName;
                            if (item.cl.CLC_DELIVERY_DATE != null)
                            {
                                temp.dDeliveryDate = (item.cl.CLC_DELIVERY_DATE ?? DateTime.Now).ToString(format, provider);
                            }
                            temp.dPlant = item.cl.CLC_PLANT;
                            temp.dProduct = item.MatName;
                            temp.dVolumeBBL = (item.cl.CLC_VOLUME_BBL ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dVolumeMT = (item.cl.CLC_VOLUME_MT ?? Convert.ToDecimal(0)).ToString("#,##0.0000");
                            temp.dPrice = (item.cl.CLC_PRICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dVolumeLitres = (item.cl.CLC_OUTTURN_LITE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dPONo = item.cl.CLC_PO_NO ?? "";

                            var cp_query = (from cp in context.CLEAR_LINE_PRICE
                                         where (cp.CLP_TRIP_NO.ToUpper().Contains(item.cl.CLC_TRIP_NO)) 
                                         select cp).OrderByDescending(x => x.CLP_DATE_PRICE); ;
                            if(cp_query != null && cp_query.ToList().Count > 0)
                            {
                                
                                temp.dAmount = (cp_query.ToList()[0].CLP_INVOICE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                                temp.dUS4 = cp_query.ToList()[0].CLP_UNIT_ID ?? "";
                            }
                            
                            temp.dExchangeRate = (item.cl.CLC_EXCHANGE_RATE ?? Convert.ToDecimal(0)).ToString("#,##0.00");
                            temp.dSaleOrder = item.cl.CLC_SALEORDER ?? "";
                            temp.dMEMONo = item.cl.CLC_MEMO ?? "";
                            temp.dDO = item.cl.CLC_DO_NO ?? "";
                            pModel.SOClearLine_Search.SearchData.Add(temp);
                        }
                    }
                }
                //pModel.SOClearLine_Search.SearchData.Add(new SOClearLineViewModel_SearchData
                //{
                //    dTripNo = "CL-2017-06-001",
                //    dCustomer = "Esso (Thailand) PLC",
                //    dDeliveryDate = "13/03/2016",
                //    dPlant = "1200",
                //    dProduct = "Murban",
                //    dVolumeBBL = "138.19",
                //    dVolumeMT = "179.535",
                //    dVolumeLitres = "22,255.59",
                //    dPrice = "1,190.1015",
                //    dAmount = "30,000",
                //    dUS4 = "US4",
                //    dPONo = "XXXXX",
                //    dExchangeRate = "35.19",
                //    dSaleOrder = "121xxxxxx",
                //    dMEMONo = "xxxxxx",
                //    dDO = "xxxxxxx"
                //});

            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
        }

        public void CreateSO(ref SOClearLineViewModel pModel)
        {
            var selectedTripNo = pModel.selectedTripNo;

            try
            {
                ReturnValue rtn = new ReturnValue();

                ResponseData resData = new ResponseData();

                #region "For Use"
                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{
                //    var qryClearLineCrude = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));

                //    if (qryClearLineCrude != null)
                //    {
                //        var qryClearLinePrice = context.CLEAR_LINE_PRICE.Where(x => x.CLP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));
                //        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryClearLineCrude.ToList()[0].CLC_PLANT.ToUpper()));
                //        string sCompanyCode = "";
                //        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                //        if (qryClearLinePrice != null)
                //        {
                //            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();
                //            obj.SalePrice = new SalesPriceCreateModel();
                //            obj.SalePrice.DATAB = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd");
                //            obj.SalePrice.DATBI = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd");
                //            obj.SalePrice.KBETR = qryClearLinePrice.ToList()[0].CLP_PRICE.ToString();
                //            obj.SalePrice.KMEIN = "BBL"; // qryClearLineCrude.ToList()[0].CLC_UNIT_TOTAL
                //            obj.SalePrice.KONWA = qryClearLinePrice.ToList()[0].CLP_UNIT_ID;
                //            obj.SalePrice.KSCHL = "ZCR1";
                //            obj.SalePrice.KUNNR = qryClearLineCrude.ToList()[0].CLC_CUST_NUM;
                //            obj.SalePrice.MATNR = qryClearLineCrude.ToList()[0].CLC_CRUDE_TYPE_ID;
                //            obj.SalePrice.VKORG = sCompanyCode;
                //            obj.SalePrice.VTWEG = "31";
                //            obj.SalePrice.WERKS = qryClearLineCrude.ToList()[0].CLC_PLANT;
                //            obj.SalePrice.ZZVSTEL = "12PI";

                //            obj.SaleOrder = new SaleOrderModel();
                //            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //            {
                //                DOC_TYPE = "Z1P3",
                //                SALES_ORG = sCompanyCode,
                //                DISTR_CHAN = "31",
                //                DIVISION = "00",
                //                REQ_DATE_H = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                DATE_TYPE = "1",
                //                PURCH_NO_C = "CIP_CLEAR_LINE_01",
                //                PURCH_NO_S = "CIP_CLEAR_LINE_01",
                //                CURRENCY = "THB"
                //            });

                //            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //            {
                //                ITM_NUMBER = "000010",
                //                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()) ? "0" : qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()),
                //                CURRENCY = qryClearLineCrude.ToList()[0].CLC_UNIT_ID
                //            });

                //            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //            {
                //                ITM_NUMBER = "000010",
                //                MATERIAL = qryClearLineCrude.ToList()[0].CLC_CRUDE_TYPE_ID,
                //                PLANT = qryClearLineCrude.ToList()[0].CLC_PLANT,
                //                //ITEM_CATEG = "ZTW1",
                //                //SALES_UNIT = qryClearLineCrude.ToList()[0].CLC_PRICE_PER
                //                SALES_UNIT = "BBL"
                //            });

                //            obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //            obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //            {
                //                PARTN_NUMB = qryClearLineCrude.ToList()[0].CLC_CUST_NUM
                //            });

                //            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //            {
                //                ITM_NUMBER = "000010",
                //                REQ_DATE = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                DATE_TYPE = "1",
                //                REQ_QTY = Decimal.Parse(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString() ?? "0")
                //            });

                //            obj.SaleOrder.SALESDOCUMENT_IN = "";
                //            obj.SaleOrder.Flag = "";

                //            var json = new JavaScriptSerializer().Serialize(obj);

                //            RequestData req = new RequestData();
                //            req.function_id = ConstantPrm.FUNCTION.F10000059;
                //            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //            req.state_name = "";
                //            req.req_parameters = new req_parameters();
                //            req.req_parameters.p = new List<p>();

                //            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //            req.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                //            req.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                //            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //            req.extra_xml = "";

                //            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                //            resData = service.CallService(req);

                //            if (resData.result_code == "1")
                //            {
                //                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //                rtn.Status = true;
                //            }
                //            else
                //            {
                //                rtn.Message = resData.result_desc;
                //                rtn.Status = false;
                //            }
                //        }

                //    }

                //}

                //if(!string.IsNullOrEmpty(selectedTripNo))
                //{
                //    var tempSelect = pModel.SOClearLine_Search.SearchData.Where(p => p.dTripNo == selectedTripNo).ToList();
                //    if(tempSelect != null && tempSelect.Count > 0)
                //    {
                //        SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();
                //        obj.SalePrice = new SalesPriceCreateModel();
                //        obj.SalePrice.DATAB = tempSelect[0].dDeliveryDate.Replace('/','-');
                //        obj.SalePrice.DATBI = tempSelect[0].dDeliveryDate.Replace('/', '-');
                //        obj.SalePrice.KBETR = tempSelect[0].dPrice;
                //        obj.SalePrice.KMEIN = tempSelect[0].dSaleUnitType;
                //        obj.SalePrice.KONWA = tempSelect[0].dUS4;
                //        obj.SalePrice.KSCHL = "ZCR1"; // ดึงจาก table clear line price rowindex = sNum
                //        obj.SalePrice.KUNNR = tempSelect[0].dCustomer;
                //        obj.SalePrice.MATNR = tempSelect[0].dProduct;
                //        obj.SalePrice.VKORG = "1100";//ดึงจาก company
                //        obj.SalePrice.VTWEG = "31";//Config DISTRIBUTION CHANEL
                //        obj.SalePrice.WERKS = tempSelect[0].dPlant;
                //        obj.SalePrice.ZZVSTEL = "12PI";//Config sHIPPING POINT

                //        obj.SaleOrder = new SaleOrderModel();
                //        obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //        obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //        {
                //            DOC_TYPE = "Z1P3", //fix
                //            SALES_ORG = "1100",//ดึงจาก company
                //            DISTR_CHAN = "31",//Config DISTRIBUTION CHANEL
                //            DIVISION = "00",//Config DIVISION
                //            REQ_DATE_H = tempSelect[0].dDeliveryDate.Replace('/', '-'),
                //            DATE_TYPE = "1",//Config DATE_TYPE
                //            PURCH_NO_C = tempSelect[0].dPONo,
                //            PURCH_NO_S = tempSelect[0].dPONo,
                //            CURRENCY = "THB"
                //        });

                //        decimal conVal = 0;
                //        if(tempSelect[0].dSaleUnitType == "BBL")
                //        {
                //            if(!string.IsNullOrEmpty(tempSelect[0].dVolumeBBL))
                //            {
                //                conVal = Convert.ToDecimal(tempSelect[0].dVolumeBBL);
                //            }
                //        }
                //        else if (tempSelect[0].dSaleUnitType == "L30")
                //        {
                //            if (!string.IsNullOrEmpty(tempSelect[0].dVolumeLitres))
                //            {
                //                conVal = Convert.ToDecimal(tempSelect[0].dVolumeLitres);
                //            }
                //        }
                //        else if (tempSelect[0].dSaleUnitType == "MT")
                //        {
                //            if (!string.IsNullOrEmpty(tempSelect[0].dVolumeMT))
                //            {
                //                conVal = Convert.ToDecimal(tempSelect[0].dVolumeMT);
                //            }
                //        }

                //        obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //        obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //        {
                //            ITM_NUMBER = "000010",//Config ITM_NUMBER
                //            COND_VALUE = conVal,
                //            CURRENCY = "TH5"
                //        });

                //        obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //        obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //        {
                //            ITM_NUMBER = "000010",//Config ITM_NUMBER
                //            MATERIAL = tempSelect[0].dProduct,
                //            PLANT = tempSelect[0].dPlant,
                //            SALES_UNIT = tempSelect[0].dSaleUnitType
                //        });

                //        obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //        obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //        {
                //            PARTN_NUMB = tempSelect[0].dCustomer
                //    });

                //        obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //        obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //        {
                //            ITM_NUMBER = "000010",
                //            REQ_DATE = tempSelect[0].dDeliveryDate.Replace('/', '-'),
                //            DATE_TYPE = "1",
                //            REQ_QTY = 1609.140M //Volume by type
                //        });

                //        obj.SaleOrder.SALESDOCUMENT_IN = "";
                //        obj.SaleOrder.Flag = "";

                //        var json = new JavaScriptSerializer().Serialize(obj);

                //        RequestData req1 = new RequestData();
                //        req1.function_id = ConstantPrm.FUNCTION.F10000059;
                //        req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //        req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //        req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //        req1.state_name = "";
                //        req1.req_parameters = new req_parameters();
                //        req1.req_parameters.p = new List<p>();

                //        req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //        req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                //        req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                //        req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //        req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //        req1.extra_xml = "";

                //        ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                //        resData = service1.CallService(req1);

                //        if (resData.result_code == "1")
                //        {
                //            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //            rtn.Status = true;
                //        }
                //        else
                //        {
                //            rtn.Message = resData.result_desc;
                //            rtn.Status = false;
                //        }
                //    }
                //}
                #endregion

                #region "For Test"
                ////For Test
                //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();
                //obj1.SalePrice = new SalesPriceCreateModel();
                //obj1.SalePrice.DATAB = "2017-06-27";
                //obj1.SalePrice.DATBI = "2017-06-27";
                //obj1.SalePrice.KBETR = "1609.140";
                //obj1.SalePrice.KMEIN = "BBL";
                //obj1.SalePrice.KONWA = "TH4";
                //obj1.SalePrice.KSCHL = "ZCR1";
                //obj1.SalePrice.KUNNR = "0000000023";
                //obj1.SalePrice.MATNR = "YMURBAN";
                //obj1.SalePrice.VKORG = "1100";
                //obj1.SalePrice.VTWEG = "31";
                //obj1.SalePrice.WERKS = "1200";
                //obj1.SalePrice.ZZVSTEL = "12PI";

                //obj1.SaleOrder = new SaleOrderModel();
                //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //{
                //    DOC_TYPE = "Z1P3",
                //    SALES_ORG = "1100",
                //    DISTR_CHAN = "31",
                //    DIVISION = "00",
                //    REQ_DATE_H = "2017-06-27",
                //    DATE_TYPE = "1",
                //    PURCH_NO_C = "CIP_CLEAR_LINE_01",
                //    PURCH_NO_S = "CIP_CLEAR_LINE_01",
                //    CURRENCY = "THB"
                //});

                //obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //{
                //    ITM_NUMBER = "000010",
                //    COND_VALUE = 33.3M,
                //    CURRENCY = "TH5"
                //});

                //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //{
                //    ITM_NUMBER = "000010",
                //    MATERIAL = "YMURBAN",
                //    PLANT = "1200",
                //    //ITEM_CATEG = "ZTW1",
                //    SALES_UNIT = "BBL"
                //});

                //obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //{
                //    PARTN_NUMB = "0000000023"
                //});

                //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //{
                //    ITM_NUMBER = "000010",
                //    REQ_DATE = "2017-06-27",
                //    DATE_TYPE = "1",
                //    REQ_QTY = 1609.140M
                //});

                //obj1.SaleOrder.SALESDOCUMENT_IN = "";
                //obj1.SaleOrder.Flag = "";

                //var json1 = new JavaScriptSerializer().Serialize(obj);

                //RequestData req1 = new RequestData();
                //req1.function_id = ConstantPrm.FUNCTION.F10000059;
                //req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //req1.state_name = "";
                //req1.req_parameters = new req_parameters();
                //req1.req_parameters.p = new List<p>();

                //req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                //req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                //req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                //req1.extra_xml = "";

                //ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                //resData = service1.CallService(req1);

                //if (resData.result_code == "1")
                //{
                //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //    rtn.Status = true;
                //}
                //else
                //{
                //    rtn.Message = resData.result_desc;
                //    rtn.Status = false;
                //}
                #endregion

            }
            catch (Exception ex)
            {

            }

        }

        public void UpdateSO(ref SOClearLineViewModel pModel)
        {
            var selectedTripNo = pModel.selectedTripNo;

            try
            {
                ReturnValue rtn = new ReturnValue();

                ResponseData resData = new ResponseData();

                #region "For Use"
                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{
                //    var qryClearLineCrude = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));
                //    if (qryClearLineCrude != null)
                //    {
                //        var qryClearLinePrice = context.CLEAR_LINE_PRICE.Where(x => x.CLP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));
                //        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryClearLineCrude.ToList()[0].CLC_PLANT.ToUpper()));
                //        string sCompanyCode = "";
                //        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                //        if (qryClearLinePrice != null)
                //        {
                //            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();

                //            obj.SaleOrder = new SaleOrderModel();
                //            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //            {
                //                DOC_TYPE = "Z1P3",
                //                REQ_DATE_H = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd")
                //            });

                //            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //            {
                //                ITM_NUMBER = "000010",
                //                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()) ? "0" : qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()),
                //                CURRENCY = qryClearLineCrude.ToList()[0].CLC_UNIT_ID
                //            });

                //            obj.SaleOrder.SALESDOCUMENT_IN = qryClearLineCrude.ToList()[0].CLC_SALEORDER;
                //            obj.SaleOrder.Flag = "";

                //            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //            {
                //                ITM_NUMBER = "000010",
                //                MATERIAL = qryClearLineCrude.ToList()[0].CLC_CRUDE_TYPE_ID,
                //                PLANT = qryClearLineCrude.ToList()[0].CLC_PLANT,
                //                //ITEM_CATEG = "ZTW1",
                //                PURCH_DATE = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                //SALES_UNIT = qryClearLineCrude.ToList()[0].CLC_PRICE_PER
                //                SALES_UNIT = "BBL"
                //            });

                //            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //            {
                //                ITM_NUMBER = "000010",
                //                REQ_DATE = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                REQ_QTY = Decimal.Parse(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString() ?? "0")
                //            });

                //            var json = new JavaScriptSerializer().Serialize(obj);

                //            RequestData req = new RequestData();
                //            req.function_id = ConstantPrm.FUNCTION.F10000059;
                //            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //            req.state_name = "";
                //            req.req_parameters = new req_parameters();
                //            req.req_parameters.p = new List<p>();

                //            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //            req.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                //            req.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo.ToUpper() });
                //            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //            req.extra_xml = "";

                //            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                //            resData = service.CallService(req);

                //            if (resData.result_code == "1")
                //            {
                //                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //                rtn.Status = true;
                //            }
                //            else
                //            {
                //                rtn.Message = resData.result_desc;
                //                rtn.Status = false;
                //            }
                //        }
                //    }
                //}
                #endregion

                #region "For Test"
                //For Test
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1P3",
                    REQ_DATE_H = "2017-06-27"

                });

                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = "000010",
                    COND_VALUE = 33.3M,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = "000010",
                    MATERIAL = "YMURBAN",
                    PLANT = "1200",
                    //ITEM_CATEG = "ZTW1",
                    SALES_UNIT = "BBL"
                });

                obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                {
                    PARTN_NUMB = "0000000023"
                });

                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = "000010",
                    REQ_DATE = "2017-06-27",
                    DATE_TYPE = "1",
                    REQ_QTY = 1609.140M
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = "1130002543";
                obj1.SaleOrder.Flag = "";

                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }
                #endregion

            }
            catch (Exception ex)
            {

            }
        }

        public void CancelSO(ref SOClearLineViewModel pModel)
        {
            var selectedTripNo = pModel.selectedTripNo;

            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();

                #region "For Use"
                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{
                //    var qryClearLineCrude = context.CLEAR_LINE_CRUDE.Where(z => z.CLC_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));
                //    if (qryClearLineCrude != null)
                //    {
                //        var qryClearLinePrice = context.CLEAR_LINE_PRICE.Where(x => x.CLP_TRIP_NO.ToUpper().Equals(selectedTripNo.ToUpper()));
                //        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryClearLineCrude.ToList()[0].CLC_PLANT.ToUpper()));
                //        string sCompanyCode = "";
                //        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                //        if (qryClearLinePrice != null)
                //        {
                //            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();

                //            obj.SaleOrder = new SaleOrderModel();
                //            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //            {
                //                DOC_TYPE = "Z1P3",
                //                REQ_DATE_H = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd")
                //            });

                //            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //            {
                //                ITM_NUMBER = "000010",
                //                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()) ? "0" : qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString()),
                //                CURRENCY = qryClearLineCrude.ToList()[0].CLC_UNIT_ID
                //            });

                //            obj.SaleOrder.SALESDOCUMENT_IN = qryClearLineCrude.ToList()[0].CLC_SALEORDER;
                //            obj.SaleOrder.Flag = "X";

                //            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //            {
                //                ITM_NUMBER = "000010",
                //                MATERIAL = qryClearLineCrude.ToList()[0].CLC_CRUDE_TYPE_ID,
                //                PLANT = qryClearLineCrude.ToList()[0].CLC_PLANT,
                //                //ITEM_CATEG = "ZTW1",
                //                PURCH_DATE = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                //SALES_UNIT = qryClearLineCrude.ToList()[0].CLC_PRICE_PER
                //                SALES_UNIT = "BBL"
                //            });

                //            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //            {
                //                ITM_NUMBER = "000010",
                //                REQ_DATE = String.IsNullOrEmpty(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryClearLineCrude.ToList()[0].CLC_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                REQ_QTY = Decimal.Parse(qryClearLineCrude.ToList()[0].CLC_TOTAL.ToString() ?? "0")
                //            });

                //            var json = new JavaScriptSerializer().Serialize(obj);

                //            RequestData req = new RequestData();
                //            req.function_id = ConstantPrm.FUNCTION.F10000059;
                //            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //            req.state_name = "";
                //            req.req_parameters = new req_parameters();
                //            req.req_parameters.p = new List<p>();

                //            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //            req.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                //            req.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo.ToUpper() });
                //            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //            req.extra_xml = "";

                //            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                //            resData = service.CallService(req);

                //            if (resData.result_code == "1")
                //            {
                //                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //                rtn.Status = true;
                //            }
                //            else
                //            {
                //                rtn.Message = resData.result_desc;
                //                rtn.Status = false;
                //            }
                //        }
                //    }
                //}
                #endregion

                #region "For Test"
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1P3",
                    REQ_DATE_H = "2017-06-30"
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = "1130002544";
                obj1.SaleOrder.Flag = "X";

                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = "000010",
                    COND_VALUE = 33.3M,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = "000010",
                    MATERIAL = "YMURBAN",
                    PLANT = "1200",
                    //ITEM_CATEG = "ZTW1",
                    //PURCH_DATE = "2017-06-27",
                    SALES_UNIT = "BBL"
                });

                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = "000010",
                    SCHED_LINE = "0001",
                    REQ_DATE = "2017-06-30",
                    REQ_QTY = 1609.140M
                });

                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOClearLine" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = selectedTripNo.ToUpper() });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }
                #endregion
            }
            catch (Exception ex)
            {

            }
        }

        public void setInitail_ddl(ref SOClearLineViewModel pModel)
        {
            if (pModel.ddl_Crude == null)
            {
                pModel.ddl_Crude = new List<SelectListItem>();
                pModel.ddl_Crude = getMaterial();
            }
            if (pModel.ddl_Customer == null)
            {
                pModel.ddl_Customer = new List<SelectListItem>();
                pModel.ddl_Customer = getCustomer();
            }
            if (pModel.ddl_Vessel == null)
            {
                pModel.ddl_Vessel = new List<SelectListItem>();
                pModel.ddl_Vessel = getVehicle("PRODUCT", true, "");
            }
        }

        // get Data Customer
        public static List<SelectListItem> getCustomer(string type = "CUS", bool isOptional = false, string message = "")
        {
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(PROJECT_NAME_SPACE, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_ROW_ID).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getVehicle(string type = "CPAI", bool isOptional = false, string message = "")
        {
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(PROJECT_NAME_SPACE, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
                //else
                //{
                //    selectList.Add(new SelectListItem { Text = "", Value = "" });
                //}
            }
            //selectList.Add(new SelectListItem { Text = "SPOT", Value = "SPOT" });
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });

                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

    }
}