﻿using Common.Logging;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class SpecServiceModel
    {
        public EntityCPAIEngine context = new EntityCPAIEngine();
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ReturnValue Add(SpecViewModel_Detail pModel, string pUser)
        {

            int countSpec = 0;
            int countSpecData = 0;
            int countSpecNote = 0;

            ReturnValue rtn = new ReturnValue();
            try
            {


                MT_SPEC_DAL dalSpec = new MT_SPEC_DAL();
                MT_SPEC_DATA_DAL dalSpecData = new MT_SPEC_DATA_DAL();
                MT_SPEC_NOTE_DAL dalSpecNote = new MT_SPEC_NOTE_DAL();

                MT_SPEC entSpec = new MT_SPEC();
                MT_SPEC_DATA entSpecData = new MT_SPEC_DATA();
                MT_SPEC_NOTE entSpecNote = new MT_SPEC_NOTE();

                var rowVersion = (MT_SPEC_DAL.GetMaxVersion() + 1) + "";
                var id = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpec.ToString("000"));
                //var specRowId = ShareFn.GenerateCodeByDate("CPAI");
                entSpec.MSP_ROW_ID = id;
                entSpec.MSP_VERSION = rowVersion;
                entSpec.MSP_CREATED_DATE = DateTime.Now;
                entSpec.MSP_CREATED_BY = pUser;
                entSpec.MSP_UPDATED_DATE = DateTime.Now;
                entSpec.MSP_UPDATED_BY = pUser;
                entSpec.MSP_REASON = pModel.Reason;
                dalSpec.Save(entSpec);
                countSpec++;

                if (pModel.SpecDetail != null && pModel.SpecDetail.Any())
                {
                    foreach (var item in pModel.SpecDetail)
                    {
                        var SpecDataid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecData.ToString("000"));
                        //var detailRowId = ShareFn.GenerateCodeByDate("CPAI");
                        entSpecData = new MT_SPEC_DATA();
                        entSpecData.MSPD_ROW_ID = SpecDataid;
                        entSpecData.MSPD_FK_MT_SPEC = id;
                        /*entSpecData.MSPD_HEADER = item.Component; */
                        entSpecData.MSPD_HEADER = item.Component != null ? ShareFn.EncodeString(item.Component) : null;
                        entSpecData.MSPD_KEY = item.Property;
                        entSpecData.MSPD_KEY_CHILD = item.Unit;
                        entSpecData.MSPD_TEST_METHOD = item.TestMethod;
                        entSpecData.MSPD_SOURCE = item.Source;
                        entSpecData.MSPD_CONSTRAINT = item.Constraint != null ? ShareFn.EncodeString(item.Constraint) : null;
                        entSpecData.MSPD_REMARKS = item.Remarks != null ? ShareFn.EncodeString(item.Remarks) : null;
                        entSpecData.MSPD_ORDER = item.Order;
                        entSpecData.MSPD_TEST_METHOD = item.TestMethod;
                        entSpecData.MSPD_CREATED_DATE = DateTime.Now;
                        entSpecData.MSPD_CREATED_BY = pUser;
                        entSpecData.MSPD_UPDATED_DATE = DateTime.Now;
                        entSpecData.MSPD_UPDATED_BY = pUser;

                        entSpecData.MSPD_MIN_1 = item.Min1;
                        entSpecData.MSPD_MIN_2 = item.Min2;
                        entSpecData.MSPD_MIN_3 = item.Min3;
                        entSpecData.MSPD_MIN_4 = item.Min4;
                        entSpecData.MSPD_MIN_5 = item.Min5;
                        entSpecData.MSPD_MIN_6 = item.Min6;
                        entSpecData.MSPD_MIN_7 = item.Min7;
                        entSpecData.MSPD_MIN_8 = item.Min8;
                        entSpecData.MSPD_MIN_9 = item.Min9;
                        entSpecData.MSPD_MIN_10 = item.Min10;

                        entSpecData.MSPD_MAX_1 = item.Max1;
                        entSpecData.MSPD_MAX_2 = item.Max2;
                        entSpecData.MSPD_MAX_3 = item.Max3;
                        entSpecData.MSPD_MAX_4 = item.Max4;
                        entSpecData.MSPD_MAX_5 = item.Max5;
                        entSpecData.MSPD_MAX_6 = item.Max6;
                        entSpecData.MSPD_MAX_7 = item.Max7;
                        entSpecData.MSPD_MAX_8 = item.Max8;
                        entSpecData.MSPD_MAX_9 = item.Max9;
                        entSpecData.MSPD_MAX_10 = item.Max10;
                        dalSpecData.Save(entSpecData);
                        countSpecData++;
                    }
                }       
                
                if(pModel.SpecNote != null && pModel.SpecNote.Any())
                {
                    foreach(var item in pModel.SpecNote)
                    {
                        var SpecNoteid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecNote.ToString("000"));
                        //var noteRowId = ShareFn.GenerateCodeByDate("CPAI");
                        entSpecNote = new MT_SPEC_NOTE();
                        entSpecNote.MSPN_ROW_ID = SpecNoteid;
                        entSpecNote.MSPN_FK_MT_SPEC = id;
                        entSpecNote.MSPN_NOTE = item.Note;
                        entSpecNote.MSPN_EXPLANATION = item.Explanation;
                        entSpecNote.MSPN_IMAGE_PATH = item.Image;
                        entSpecNote.MSPN_ORDER = item.Order;

                        entSpecNote.MSPN_CREATED_BY = pUser;
                        entSpecNote.MSPN_CREATED_DATE = DateTime.Now;
                        entSpecNote.MSPN_UPDATED_BY = pUser;
                        entSpecNote.MSPN_UPDATED_DATE = DateTime.Now;
                        dalSpecNote.Save(entSpecNote);
                        countSpecNote++;
                    }                  
                }    

                pModel.RowId = id;
                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                rtn.Status = true;
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Edit(SpecViewModel_Detail pModel, string pUser)
        {

            int countSpec = 0;
            int countSpecData = 0;
            int countSpecNote = 0;
            ReturnValue rtn = new ReturnValue();
            try
            {                              

                MT_SPEC_DAL dalSpec = new MT_SPEC_DAL();
                MT_SPEC_DATA_DAL dalSpecData = new MT_SPEC_DATA_DAL();
                MT_SPEC_NOTE_DAL dalSpecNote = new MT_SPEC_NOTE_DAL();

                MT_SPEC entSpec = new MT_SPEC();
                MT_SPEC_DATA entSpecData = new MT_SPEC_DATA();
                MT_SPEC_NOTE entSpecNote = new MT_SPEC_NOTE();
                try
                {
                    var rowVersion = (MT_SPEC_DAL.GetMaxVersion() + 1) + "";
                    entSpec.MSP_ROW_ID = pModel.RowId;

                    var useSpec = context.COO_CAM.Where(c => c.COCA_FK_MT_SPEC == pModel.RowId).Any();

                    if(useSpec == true)
                    {
                        entSpec.MSP_VERSION = rowVersion;
                    }
                    else
                    {
                        entSpec.MSP_VERSION = pModel.RowVersion != null ? pModel.RowVersion : rowVersion;
                    }
                    
                    entSpec.MSP_UPDATED_DATE = DateTime.Now;
                    entSpec.MSP_UPDATED_BY = pUser;
                    entSpec.MSP_REASON = pModel.Reason;
                    dalSpec.Update(entSpec);
                    countSpec++;

                    #region Spec Data
                    if (pModel.SpecDetail != null && pModel.SpecDetail.Any())
                    {

                        var oldDetail = dalSpecData.getSpecDataBySpec(pModel.RowId).Count();


                        if (oldDetail != pModel.SpecDetail.Count())
                        {
                            try
                            {
                                dalSpecData.Delete(pModel.RowId);
                                foreach (var item in pModel.SpecDetail)
                                {
                                    var SpecDataid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecData.ToString("000"));
                                    //var detailRowId = ShareFn.GenerateCodeByDate("CPAI");
                                    entSpecData = new MT_SPEC_DATA();
                                    entSpecData.MSPD_ROW_ID = SpecDataid;
                                    entSpecData.MSPD_FK_MT_SPEC = pModel.RowId;
                                    entSpecData.MSPD_HEADER = item.Component != null ? ShareFn.EncodeString(item.Component) : null;
                                    entSpecData.MSPD_KEY = item.Property;
                                    entSpecData.MSPD_KEY_CHILD = item.Unit;
                                    entSpecData.MSPD_TEST_METHOD = item.TestMethod;
                                    entSpecData.MSPD_SOURCE = item.Source;
                                    entSpecData.MSPD_CONSTRAINT = item.Constraint != null ? ShareFn.EncodeString(item.Constraint) : null;
                                    entSpecData.MSPD_REMARKS = item.Remarks != null ? ShareFn.EncodeString(item.Remarks) : null;
                                    entSpecData.MSPD_ORDER = item.Order;

                                    entSpecData.MSPD_MIN_1 = item.Min1;
                                    entSpecData.MSPD_MIN_2 = item.Min2;
                                    entSpecData.MSPD_MIN_3 = item.Min3;
                                    entSpecData.MSPD_MIN_4 = item.Min4;
                                    entSpecData.MSPD_MIN_5 = item.Min5;
                                    entSpecData.MSPD_MIN_6 = item.Min6;
                                    entSpecData.MSPD_MIN_7 = item.Min7;
                                    entSpecData.MSPD_MIN_8 = item.Min8;
                                    entSpecData.MSPD_MIN_9 = item.Min9;
                                    entSpecData.MSPD_MIN_10 = item.Min10;
                                    entSpecData.MSPD_MAX_1 = item.Max1;
                                    entSpecData.MSPD_MAX_2 = item.Max2;
                                    entSpecData.MSPD_MAX_3 = item.Max3;
                                    entSpecData.MSPD_MAX_4 = item.Max4;
                                    entSpecData.MSPD_MAX_5 = item.Max5;
                                    entSpecData.MSPD_MAX_6 = item.Max6;
                                    entSpecData.MSPD_MAX_7 = item.Max7;
                                    entSpecData.MSPD_MAX_8 = item.Max8;
                                    entSpecData.MSPD_MAX_9 = item.Max9;
                                    entSpecData.MSPD_MAX_10 = item.Max10;
                                    entSpecData.MSPD_CREATED_BY = pUser;
                                    entSpecData.MSPD_CREATED_DATE = DateTime.Now;
                                    entSpecData.MSPD_UPDATED_DATE = DateTime.Now;
                                    entSpecData.MSPD_UPDATED_BY = pUser;
                                    dalSpecData.Save(entSpecData);
                                    countSpecData++;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        else
                        {
                            foreach (var item in pModel.SpecDetail)
                            {
                                var SpecDataid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecData.ToString("000"));
                                //var detailRowId = ShareFn.GenerateCodeByDate("CPAI");
                                entSpecData = new MT_SPEC_DATA();
                                entSpecData.MSPD_ROW_ID = item.RowId != null ? item.RowId : SpecDataid;
                                entSpecData.MSPD_FK_MT_SPEC = pModel.RowId;
                                entSpecData.MSPD_HEADER = item.Component != null ? ShareFn.EncodeString(item.Component) : null;
                                entSpecData.MSPD_KEY = item.Property;
                                entSpecData.MSPD_KEY_CHILD = item.Unit;
                                entSpecData.MSPD_TEST_METHOD = item.TestMethod;
                                entSpecData.MSPD_SOURCE = item.Source;
                                entSpecData.MSPD_CONSTRAINT = item.Constraint != null ? ShareFn.EncodeString(item.Constraint) : null;
                                entSpecData.MSPD_REMARKS = item.Remarks != null ? ShareFn.EncodeString(item.Remarks) : null;
                                entSpecData.MSPD_ORDER = item.Order;

                                entSpecData.MSPD_MIN_1 = item.Min1;
                                entSpecData.MSPD_MIN_2 = item.Min2;
                                entSpecData.MSPD_MIN_3 = item.Min3;
                                entSpecData.MSPD_MIN_4 = item.Min4;
                                entSpecData.MSPD_MIN_5 = item.Min5;
                                entSpecData.MSPD_MIN_6 = item.Min6;
                                entSpecData.MSPD_MIN_7 = item.Min7;
                                entSpecData.MSPD_MIN_8 = item.Min8;
                                entSpecData.MSPD_MIN_9 = item.Min9;
                                entSpecData.MSPD_MIN_10 = item.Min10;
                                entSpecData.MSPD_MAX_1 = item.Max1;
                                entSpecData.MSPD_MAX_2 = item.Max2;
                                entSpecData.MSPD_MAX_3 = item.Max3;
                                entSpecData.MSPD_MAX_4 = item.Max4;
                                entSpecData.MSPD_MAX_5 = item.Max5;
                                entSpecData.MSPD_MAX_6 = item.Max6;
                                entSpecData.MSPD_MAX_7 = item.Max7;
                                entSpecData.MSPD_MAX_8 = item.Max8;
                                entSpecData.MSPD_MAX_9 = item.Max9;
                                entSpecData.MSPD_MAX_10 = item.Max10;
                                entSpecData.MSPD_CREATED_BY = pUser;
                                entSpecData.MSPD_CREATED_DATE = DateTime.Now;
                                entSpecData.MSPD_UPDATED_DATE = DateTime.Now;
                                entSpecData.MSPD_UPDATED_BY = pUser;
                                dalSpecData.Update(entSpecData);
                                countSpecData++;
                            }
                        }                    
                        
                    }
                    #endregion

                    #region Spec Note
                    if (pModel.SpecNote != null && pModel.SpecNote.Any())
                    {

                        var oldSpecNote = dalSpecNote.getSpecNoteBySpec(pModel.RowId).Count();


                        if (oldSpecNote != pModel.SpecNote.Count())
                        {
                            try
                            {
                                dalSpecNote.Delete(pModel.RowId);
                                foreach (var item in pModel.SpecNote)
                                {
                                    var SpecNoteid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecNote.ToString("000"));
                                    //var noteRowId = ShareFn.GenerateCodeByDate("CPAI");
                                    entSpecNote = new MT_SPEC_NOTE();
                                    entSpecNote.MSPN_ROW_ID = SpecNoteid;
                                    entSpecNote.MSPN_FK_MT_SPEC = pModel.RowId;
                                    entSpecNote.MSPN_NOTE = item.Note;
                                    entSpecNote.MSPN_EXPLANATION = item.Explanation;
                                    entSpecNote.MSPN_IMAGE_PATH = item.Image;
                                    entSpecNote.MSPN_ORDER = item.Order;
                                    entSpecNote.MSPN_CREATED_BY = pUser;
                                    entSpecNote.MSPN_CREATED_DATE = DateTime.Now;
                                    entSpecNote.MSPN_UPDATED_BY = pUser;
                                    entSpecNote.MSPN_UPDATED_DATE = DateTime.Now;
                                    dalSpecNote.Save(entSpecNote);
                                    countSpecNote++;
                                }
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }else
                        {
                            foreach (var item in pModel.SpecNote)
                            {
                                var SpecNoteid = string.Format("{0}{1}", ShareFn.GenerateCodeByDate("CPAI"), countSpecNote.ToString("000"));
                                //var noteRowId = ShareFn.GenerateCodeByDate("CPAI");
                                entSpecNote = new MT_SPEC_NOTE();
                                entSpecNote.MSPN_ROW_ID = item.RowId != null ? item.RowId : SpecNoteid;
                                entSpecNote.MSPN_FK_MT_SPEC = pModel.RowId;
                                entSpecNote.MSPN_NOTE = item.Note;
                                entSpecNote.MSPN_EXPLANATION = item.Explanation;
                                entSpecNote.MSPN_IMAGE_PATH = item.Image;
                                entSpecNote.MSPN_ORDER = item.Order;
                                entSpecNote.MSPN_CREATED_BY = pUser;
                                entSpecNote.MSPN_CREATED_DATE = DateTime.Now;
                                entSpecNote.MSPN_UPDATED_BY = pUser;
                                entSpecNote.MSPN_UPDATED_DATE = DateTime.Now;
                                dalSpecNote.Update(entSpecNote);
                                countSpecNote++;
                            }
                        }
                        
                    }
                    #endregion


                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            
            return rtn;
        }

        public ReturnValue Delete(SpecViewModel_Detail pModel, string pUser)
        {

            ReturnValue rtn = new ReturnValue();
            try
            {

                if (pModel == null)
                {
                    rtn.Message = "Model much not be null.";
                    rtn.Status = false;
                    return rtn;
                }

                MT_SPEC_DAL dal = new MT_SPEC_DAL();
                MT_SPEC ent = new MT_SPEC();

                DateTime now = DateTime.Now;

                try
                {
                    //var Code = ShareFn.GenerateCodeByDate("CPAI");
                    ent.MSP_ROW_ID = pModel.RowId;
                    dal.Delete(ent);

                    //dbContextTransaction.Commit();
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                catch (Exception ex)
                {
                    rtn.Message = ex.Message;
                    rtn.Status = false;
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
            return rtn;
        }

        public ReturnValue Search(ref SpecViewModel_Search pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                var sSpecRowId = pModel.sSpecRowId == null ? "" : pModel.sSpecRowId.ToUpper();

                var query = (from c in context.MT_SPEC
                             select new SpecViewModel_SearchData
                             {
                                 dSpecRowId = c.MSP_ROW_ID,
                                 dVersion = c.MSP_VERSION,
                                 dReason = c.MSP_REASON,
                                 dUpdatedDate = c.MSP_UPDATED_DATE,
                                 dUpdatedBy = c.MSP_UPDATED_BY
                             });

                if (!string.IsNullOrEmpty(sSpecRowId))
                    query = query.Where(p => p.dSpecRowId.ToUpper().Contains(sSpecRowId));


                if(query != null)
                {
                    pModel.sSearchData = query.OrderByDescending(c => c.dUpdatedDate).ToList();
                    pModel.sSearchData = new List<SpecViewModel_SearchData>();
                    foreach(var item in query)
                    {
                        pModel.sSearchData.Add(new SpecViewModel_SearchData
                        {
                            dSpecRowId = string.IsNullOrEmpty(item.dSpecRowId) ? "" : item.dSpecRowId,
                            dVersion = string.IsNullOrEmpty(item.dVersion) ? "" : item.dVersion,
                            dReason = string.IsNullOrEmpty(item.dReason) ? "" : item.dReason,
                            dUpdatedDate = item.dUpdatedDate,
                            dUpdatedBy = string.IsNullOrEmpty(item.dUpdatedBy)?"":item.dUpdatedBy
                        });
                    }
                    rtn.Status = true;
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                }
                else
                {
                    rtn.Status = false;
                    rtn.Message = "Data is null";
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }

            return rtn;
        }

        public SpecViewModel_Detail GetSpecDetail(string rowId)
        {
            SpecViewModel_Detail model = new SpecViewModel_Detail();
            try
            {
                if(String.IsNullOrEmpty(rowId) == false)
                {
                    var query = context.MT_SPEC.Where(c => c.MSP_ROW_ID == rowId).SingleOrDefault();

                    #region Spec Detail Data
                    var detail = context.MT_SPEC_DATA.Where(c => c.MSPD_FK_MT_SPEC == rowId).ToList();
                    var specDetailData = (from a in detail
                                          select new SpecDetail
                                          {
                                              RowId = a.MSPD_ROW_ID,
                                              IntOrder = Convert.ToInt32(a.MSPD_ORDER),
                                              Component = a.MSPD_HEADER != null ? ShareFn.DecodeString(a.MSPD_HEADER) : null,
                                              Property = a.MSPD_KEY,
                                              Unit = a.MSPD_KEY_CHILD,
                                              TestMethod = a.MSPD_TEST_METHOD,
                                              Min1 = a.MSPD_MIN_1,
                                              Min2 = a.MSPD_MIN_2,
                                              Min3 = a.MSPD_MIN_3,
                                              Min4 = a.MSPD_MIN_4,
                                              Min5 = a.MSPD_MIN_5,
                                              Min6 = a.MSPD_MIN_6,
                                              Min7 = a.MSPD_MIN_7,
                                              Min8 = a.MSPD_MIN_8,
                                              Min9 = a.MSPD_MIN_9,
                                              Min10 = a.MSPD_MIN_10,
                                              Max1 = a.MSPD_MAX_1,
                                              Max2 = a.MSPD_MAX_2,
                                              Max3 = a.MSPD_MAX_3,
                                              Max4 = a.MSPD_MAX_4,
                                              Max5 = a.MSPD_MAX_5,
                                              Max6 = a.MSPD_MAX_6,
                                              Max7 = a.MSPD_MAX_7,
                                              Max8 = a.MSPD_MAX_8,
                                              Max9 = a.MSPD_MAX_9,
                                              Max10 = a.MSPD_MAX_10,
                                              Source = a.MSPD_SOURCE,
                                              Constraint = a.MSPD_CONSTRAINT != null ? ShareFn.DecodeString(a.MSPD_CONSTRAINT) : null,
                                              Remarks = a.MSPD_REMARKS != null ? ShareFn.DecodeString(a.MSPD_REMARKS) : null
                                          }).OrderBy(c => c.IntOrder).ToList();

                    #endregion

                    #region Spec Detail Note
                    var note = context.MT_SPEC_NOTE.Where(c => c.MSPN_FK_MT_SPEC == rowId).ToList();
                    var specDetailNote = (from n in note                                        
                                          select new Spec_Note
                                          {
                                              RowId = n.MSPN_ROW_ID,
                                              SpecId = n.MSPN_FK_MT_SPEC,
                                              Note = n.MSPN_NOTE,
                                              Explanation = n.MSPN_EXPLANATION,
                                              Image = n.MSPN_IMAGE_PATH,
                                              Order = n.MSPN_ORDER,
                                              IntOrder = Convert.ToInt32(n.MSPN_ORDER)
                                          }).OrderBy(c => c.IntOrder).ToList();
                    #endregion
                              

                    model.RowId = query.MSP_ROW_ID;                   
                    model.RowVersion = query.MSP_VERSION;
                    model.SpecDetail = specDetailData;
                    model.SpecNote = specDetailNote;
                    model.Reason = query.MSP_REASON;                   
                    model.UpdatedDate = query.MSP_UPDATED_DATE;
                    return model;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SpecViewModel_Detail_Compared GetLastSpecDetail()
        {
            SpecViewModel_Detail_Compared model = new SpecViewModel_Detail_Compared();
            try
            {
                var query = context.MT_SPEC.OrderByDescending(c=>c.MSP_UPDATED_DATE).FirstOrDefault();

                #region Spec Detail Data
                var detail = context.MT_SPEC_DATA.Where(c => c.MSPD_FK_MT_SPEC == query.MSP_ROW_ID).ToList();
                var specDetailData = (from a in detail
                                      select new ComparedSpecDetail
                                      {
                                          RowId = a.MSPD_ROW_ID,
                                          IntOrder = Convert.ToInt32(a.MSPD_ORDER),
                                          Component = a.MSPD_HEADER != null ? ShareFn.DecodeString(a.MSPD_HEADER) : null,
                                          Property = a.MSPD_KEY,
                                          Unit = a.MSPD_KEY_CHILD,
                                          TestMethod = a.MSPD_TEST_METHOD,
                                          Min1 = a.MSPD_MIN_1,
                                          Min2 = a.MSPD_MIN_2,
                                          Min3 = a.MSPD_MIN_3,
                                          Min4 = a.MSPD_MIN_4,
                                          Min5 = a.MSPD_MIN_5,
                                          Min6 = a.MSPD_MIN_6,
                                          Min7 = a.MSPD_MIN_7,
                                          Min8 = a.MSPD_MIN_8,
                                          Min9 = a.MSPD_MIN_9,
                                          Min10 = a.MSPD_MIN_10,
                                          Max1 = a.MSPD_MAX_1,
                                          Max2 = a.MSPD_MAX_2,
                                          Max3 = a.MSPD_MAX_3,
                                          Max4 = a.MSPD_MAX_4,
                                          Max5 = a.MSPD_MAX_5,
                                          Max6 = a.MSPD_MAX_6,
                                          Max7 = a.MSPD_MAX_7,
                                          Max8 = a.MSPD_MAX_8,
                                          Max9 = a.MSPD_MAX_9,
                                          Max10 = a.MSPD_MAX_10,
                                          Source = a.MSPD_SOURCE,
                                          Constraint = a.MSPD_CONSTRAINT != null ? ShareFn.DecodeString(a.MSPD_CONSTRAINT) : null,
                                          Remarks = a.MSPD_REMARKS != null ? ShareFn.DecodeString(a.MSPD_REMARKS) : null
                                      }).OrderBy(c => c.IntOrder).ToList();            
            #endregion

                model.RowId = query.MSP_ROW_ID;
                model.RowVersion = query.MSP_VERSION;
                model.ComparedSpecDetail = specDetailData;
                model.Reason = query.MSP_REASON;

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public string GetLastSpecVersion()
        {
            DateTime query = context.MT_SPEC.OrderByDescending(c => c.MSP_UPDATED_DATE).FirstOrDefault().MSP_UPDATED_DATE;
            var returnData = query.ToString("dd-MMM-yyyy");
            return returnData;
        }

        public SpecViewModel_Detail GetLastSpecDetailComparision()
        {
            SpecViewModel_Detail model = new SpecViewModel_Detail();
            try
            {
               
                var query = context.MT_SPEC.OrderByDescending(c => c.MSP_UPDATED_DATE).FirstOrDefault();

                #region Spec Detail Data
                var detail = context.MT_SPEC_DATA.Where(c => c.MSPD_FK_MT_SPEC == query.MSP_ROW_ID).ToList();
                var specDetailData = (from a in detail
                                      select new SpecDetail
                                      {
                                          RowId = a.MSPD_ROW_ID,
                                          IntOrder = Convert.ToInt32(a.MSPD_ORDER),
                                          Component = a.MSPD_HEADER != null ? ShareFn.DecodeString(a.MSPD_HEADER) : null,
                                          Property = a.MSPD_KEY,
                                          Unit = a.MSPD_KEY_CHILD,
                                          TestMethod = a.MSPD_TEST_METHOD,
                                          Min1 = a.MSPD_MIN_1,
                                          Min2 = a.MSPD_MIN_2,
                                          Min3 = a.MSPD_MIN_3,
                                          Min4 = a.MSPD_MIN_4,
                                          Min5 = a.MSPD_MIN_5,
                                          Min6 = a.MSPD_MIN_6,
                                          Min7 = a.MSPD_MIN_7,
                                          Min8 = a.MSPD_MIN_8,
                                          Min9 = a.MSPD_MIN_9,
                                          Min10 = a.MSPD_MIN_10,
                                          Max1 = a.MSPD_MAX_1,
                                          Max2 = a.MSPD_MAX_2,
                                          Max3 = a.MSPD_MAX_3,
                                          Max4 = a.MSPD_MAX_4,
                                          Max5 = a.MSPD_MAX_5,
                                          Max6 = a.MSPD_MAX_6,
                                          Max7 = a.MSPD_MAX_7,
                                          Max8 = a.MSPD_MAX_8,
                                          Max9 = a.MSPD_MAX_9,
                                          Max10 = a.MSPD_MAX_10,
                                          Source = a.MSPD_SOURCE,
                                          Constraint = a.MSPD_CONSTRAINT != null ? ShareFn.DecodeString(a.MSPD_CONSTRAINT) : null,
                                          Remarks = a.MSPD_REMARKS != null ? ShareFn.DecodeString(a.MSPD_REMARKS) : null
                                      }).OrderBy(c => c.IntOrder).ToList();
                #endregion

                #region Spec Detail Note
                var note = context.MT_SPEC_NOTE.Where(c => c.MSPN_FK_MT_SPEC == query.MSP_ROW_ID).ToList();
                var specDetailNote = (from n in note
                                      select new Spec_Note
                                      {
                                          RowId = n.MSPN_ROW_ID,
                                          SpecId = n.MSPN_FK_MT_SPEC,
                                          Note = n.MSPN_NOTE,
                                          Explanation = n.MSPN_EXPLANATION,
                                          Image = n.MSPN_IMAGE_PATH,
                                          Order = n.MSPN_ORDER,
                                          IntOrder = Convert.ToInt32(n.MSPN_ORDER)
                                      }).OrderBy(c => c.IntOrder).ToList();
                #endregion

                model.RowId = query.MSP_ROW_ID;
                model.RowVersion = query.MSP_VERSION;
                model.SpecDetail = specDetailData;
                model.SpecNote = specDetailNote;
                model.Reason = query.MSP_REASON;
                model.UpdatedDate = query.MSP_UPDATED_DATE;

                return model;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsUsed(string specCode, bool isFinal = false)
        {
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from c in context.COO_CAM join d in context.COO_DATA on c.COCA_FK_COO_DATA equals d.CODA_ROW_ID
                                 where c.COCA_FK_MT_SPEC == specCode 
                                    && d.CODA_STATUS == (isFinal ? "APPROVED" : d.CODA_STATUS)
                                 select c).FirstOrDefault();

                    if (query == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool GenerateSpecComparison()
        {
            COO_DATA_DAL dal = new COO_DATA_DAL();
            List<COO_DATA> list_data = dal.GetByStatus("APPROVED", "migrate").OrderBy(i => i.CODA_UPDATED).ToList();
            //List<COO_DATA> list_data = dal.GetByStatus().OrderBy(i => i.CODA_UPDATED).ToList();
            //var unit = 50;
            //var total_page = (list_data.Count / unit);
            //var list_all = list_data.Skip(unit * 0).Take(unit).Select(data => new { Page = (0 + 1), Data = data }).ToList();
            //for (int page = 1; page < total_page; page++)
            //{
            //    var list_page = list_data.Skip(unit * page).Take(unit).Select(data => new { Page = (page + 1), Data = data }).ToList();
            //    list_all.AddRange(list_page);
            //}

            //while (true)
            //{

            //}

            foreach (COO_DATA data in list_data)
                {
                    if (data != null)
                    {
                        #region SetValue ViewModel
                        CoolViewModel viewModel = new CoolViewModel() { data = new Model.CooData() };
                        viewModel.taskID = data.CODA_ROW_ID;
                        viewModel.purno = data.CODA_PURCHASE_NO;
                        viewModel.data.date_purchase = (data.CODA_REQUESTED_DATE == DateTime.MinValue || data.CODA_REQUESTED_DATE == null) ? "" : Convert.ToDateTime(data.CODA_REQUESTED_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                        viewModel.data.approval_date = (data.CODA_COMPLETE_DATE == DateTime.MinValue || data.CODA_COMPLETE_DATE == null) ? "" : Convert.ToDateTime(data.CODA_COMPLETE_DATE).ToString("dd-MMM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture);
                        viewModel.data.area_unit = data.CODA_AREA_UNIT;
                        viewModel.data.assay_date = (data.CODA_ASSAY_DATE == DateTime.MinValue || data.CODA_ASSAY_DATE == null) ? "" : Convert.ToDateTime(data.CODA_ASSAY_DATE).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        viewModel.data.assay_from = data.CODA_ASSAY_TYPE;
                        viewModel.data.assay_ref = data.CODA_ASSAY_REF_NO;
                        viewModel.data.country = data.CODA_COUNTRY;
                        viewModel.data.origin = data.CODA_FK_COUNTRY;
                        viewModel.data.crude_categories = data.CODA_CRUDE_CATEGORIES;
                        viewModel.data.crude_characteristic = data.CODA_CRUDE_CHARACTERISTIC;
                        viewModel.data.crude_kerogen = data.CODA_CRUDE_KEROGEN_TYPE;
                        viewModel.data.crude_maturity = data.CODA_CRUDE_MATURITY;
                        viewModel.data.crude_name = data.CODA_CRUDE_NAME;
                        viewModel.data.crude_id = data.CODA_FK_MATERIALS;
                        viewModel.data.type_of_cam = data.CODA_TYPE_OF_CAM;
                        viewModel.data.update_from_crude = data.CODA_CRUDE_REFERENCE;
                        viewModel.data.workflow_priority = data.CODA_PRIORITY;
                        viewModel.data.workflow_status = data.CODA_STATUS;
                        viewModel.data.name = data.CODA_REQUESTED_BY;
                        viewModel.data.fouling_possibility = data.CODA_FOULING_POSSIBILITY;
                        viewModel.data.draft_cam_file = data.CODA_DRAFT_CAM_PATH;
                        viewModel.data.final_cam_file = data.CODA_FINAL_CAM_PATH;
                        viewModel.data.created_by = data.CODA_CREATED_BY;
                        viewModel.data.created_date = (data.CODA_CREATED == DateTime.MinValue || data.CODA_CREATED == null) ? "" : Convert.ToDateTime(data.CODA_CREATED).ToString("dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        viewModel.note = data.CODA_NOTE;
                        #endregion

                        #region Generate and Update Path
                        CoolController ctrl = new CoolController();
                        string final_cam_path = ctrl.GenerateExcel(viewModel, false, true);
                        if (!string.IsNullOrEmpty(final_cam_path))
                        {
                            data.CODA_FINAL_CAM_PATH = final_cam_path;
                            dal.UpdatePath(data);
                        }
                        #endregion
                    }
                }
                return true;
           
        }
    }
}