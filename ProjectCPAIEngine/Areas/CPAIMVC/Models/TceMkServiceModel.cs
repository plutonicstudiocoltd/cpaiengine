﻿using System;
using ProjectCPAIEngine.DAL.DALTce;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class TceMkServiceModel
    {
        public static string getTransactionByID(string id)
        {
            TceMkRootObject rootObj = new TceMkRootObject();

            rootObj.tce_mk_detail = new TceMkDetail();

            CPAI_TCE_MK_DAL dataDal = new CPAI_TCE_MK_DAL();
            CPAI_TCE_MK data = dataDal.GetByID(id);
            if (data != null)
            {
                rootObj.tce_mk_detail.date_from = (data.CTM_DATE_FROM == null) ? "" : Convert.ToDateTime(data.CTM_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_mk_detail.date_to = (data.CTM_DATE_TO == null) ? "" : Convert.ToDateTime(data.CTM_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_mk_detail.loadingdate_from = (data.CTM_LOADING_DATE_FROM == null) ? "" : Convert.ToDateTime(data.CTM_LOADING_DATE_FROM).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_mk_detail.loadingdate_to = (data.CTM_LOADING_DATE_TO == null) ? "" : Convert.ToDateTime(data.CTM_LOADING_DATE_TO).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                rootObj.tce_mk_detail.td = data.CTM_TD;
                rootObj.tce_mk_detail.vessel = data.CTM_FK_MT_VEHICLE;
                rootObj.tce_mk_detail.voy_no = data.CTM_TASK_NO;
                rootObj.tce_mk_detail.month = data.CTM_MONTH;
                rootObj.tce_mk_detail.tc_rate = data.CTM_TC_RATE;
                rootObj.tce_mk_detail.operating_days = data.CTM_OPERATING_DAYS;
                rootObj.tce_mk_detail.bunker_cost_fo = data.CTM_BUNKER_FO;
                rootObj.tce_mk_detail.bunker_cost_go = data.CTM_BUNKER_GO;
                rootObj.tce_mk_detail.other_cost = data.CTM_OTHER_COST;
                rootObj.tce_mk_detail.flat_rate = data.CTM_FLAT_RATE;
                rootObj.tce_mk_detail.minload = data.CTM_MINLOAD;
                rootObj.tce_mk_detail.theoritical_days = data.CTM_THEORITICAL_DAYS;
                rootObj.tce_mk_detail.fo_mt = data.CTM_FO_MT;
                rootObj.tce_mk_detail.go_mt = data.CTM_GO_MT;
                rootObj.tce_mk_detail.demurrage = data.CTM_DEMURRAGE;

                rootObj.tce_mk_detail.tc_num = data.CTM_TC_NUM;
                rootObj.tce_mk_detail.fo_unit_price = data.CTM_FO_UNIT_PRICE;
                rootObj.tce_mk_detail.go_unit_price = data.CTM_GO_UNIT_PRICE;
                
                CPAI_TCE_MK_PORT_DAL portDal = new CPAI_TCE_MK_PORT_DAL();
                List<CPAI_TCE_MK_PORT> lstPort = portDal.GetByID(id);

                // Load port
                var loadport = lstPort.Where(x => x.TMP_PORT_TYPE.Equals("L")).ToList();
                rootObj.tce_mk_detail.load_ports = new List<TceMkLoadPort>();
                foreach (var items in loadport)
                {
                    TceMkLoadPort item = new TceMkLoadPort();
                    item.port = items.TMP_FK_PORT.ToString();
                    item.port_order = items.TMP_ORDER_PORT;
                    item.port_value = items.TMP_POST_COST;
                    
                    rootObj.tce_mk_detail.load_ports.Add(item);
                }

                // Dis port
                var disport = lstPort.Where(x => x.TMP_PORT_TYPE.Equals("D")).ToList();
                rootObj.tce_mk_detail.dis_ports = new List<TceMkDisPort>();
                foreach (var items in disport)
                {
                    TceMkDisPort item = new TceMkDisPort();
                    item.port = items.TMP_FK_PORT.ToString();
                    item.port_order = items.TMP_ORDER_PORT;
                    item.port_value = items.TMP_POST_COST;

                    rootObj.tce_mk_detail.dis_ports.Add(item);
                }
               
             }

            return new JavaScriptSerializer().Serialize(rootObj);
        }
    }
}
