﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;
using System.Data.Objects.SqlClient;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using log4net;
using log4net.Appender;
using System.Web.Mvc;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Globalization;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALPCF;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models
{
    public class ThroughputServiceModel
    {
        CultureInfo provider = new CultureInfo("en-US");
        string format = "dd/MM/yyyy";
        public void SearchData(ref ThroughputViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                string sDelifrom = String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryDateFrom) ? string.Empty : pModel.Throughput_Search.sDeliveryDateFrom;
                string sDeliTo = String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryDateTo) ? string.Empty : pModel.Throughput_Search.sDeliveryDateTo;
                string sTripNo = String.IsNullOrEmpty(pModel.Throughput_Search.sTripNo) ? string.Empty : pModel.Throughput_Search.sTripNo;
                string sCustomer = String.IsNullOrEmpty(pModel.Throughput_Search.sCustomer) ? string.Empty : pModel.Throughput_Search.sCustomer;
                string sPlant = String.IsNullOrEmpty(pModel.Throughput_Search.sPlant) ? string.Empty : pModel.Throughput_Search.sPlant;
                //string sDeliveryDate = String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryDate) ? string.Empty : pModel.Throughput_Search.sDeliveryDate;
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    var query = (from v in context.THROUGHPUT_FEE
                                 join a in context.THROUGHPUT_DETAIL on v.TPF_TRIP_NO equals a.TPD_TRIP_NO into viewD
                                 from a in viewD.DefaultIfEmpty()
                                 select new
                                 {
                                     dTripNo = v.TPF_TRIP_NO,
                                     dCustomer = v.TPF_CUST_NUM,
                                     dDeliveryDate = v.TPF_DELIVERY_DATE,
                                     dPlant = v.TPF_PLANT,
                                     dVolume = a.TPD_OUTTURN_BBL,
                                     dTotal = a.TPD_TOTAL,
                                     dLast = a.TPD_LAST_MODIFY_DATE,
                                 }).ToList().Distinct();
                    if (!string.IsNullOrEmpty(sTripNo))
                        query = query.Where(p => p.dTripNo != null && p.dTripNo.ToUpper().Contains(sTripNo)).ToList();
                    if (!string.IsNullOrEmpty(sCustomer))
                        query = query.Where(p => p.dCustomer != null && p.dCustomer.ToUpper().Contains(sCustomer)).ToList();
                    if (!string.IsNullOrEmpty(sPlant))
                        query = query.Where(p => p.dPlant != null && p.dPlant.ToUpper().Contains(sPlant)).ToList();
                    //if (!string.IsNullOrEmpty(sDeliveryDate))
                    //    query = query.Where(p => p.dDeliveryDate != null && p.dDeliveryDate.Value.ToString(format).Equals(sDeliveryDate)).ToList();
                    if (!string.IsNullOrEmpty(sDelifrom) && !string.IsNullOrEmpty(sDeliTo))
                    {
                        DateTime sDate = new DateTime();
                        DateTime eDate = new DateTime();
                        sDate = ShareFn.ConvertStrDateToDate(sDelifrom);
                        eDate = ShareFn.ConvertStrDateToDate(sDeliTo).AddDays(1).AddSeconds(-1);
                        query = query.Where(p => p.dDeliveryDate != null && (p.dDeliveryDate >= sDate && p.dDeliveryDate <= eDate)).ToList();
                    }
                    if (query != null)
                    {
                        pModel.Throughput_Search.SearchData = new List<ThroughputViewModel_SearchData>();
                        var newList = new List<ThroughputViewModel_SearchData>();
                        foreach (var g in query)
                        {
                            newList.Add(new ThroughputViewModel_SearchData
                            {
                                dTripNo = g.dTripNo,
                                dCustomer = g.dCustomer,
                                dDeliveryDate = g.dDeliveryDate.Value.ToString(format),
                                dPlant = g.dPlant,
                                dVolumeBBL = g.dVolume.ToString(),
                                dTotalAmount = g.dTotal.ToString(),
                                dLast = g.dLast.ToString()
                            });
                        }
                        pModel.Throughput_Search.SearchData = newList.GroupBy(c => c.dTripNo).Select(g => g.OrderByDescending(c => c.dLast).First()).ToList();
                        rtn.Status = true;
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    }
                    else
                    {
                        rtn.Status = false;
                        rtn.Message = "Data is null";
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
        }

        public string GenerateTripNo()
        {
            string TripNo = "TP11";
            DateTime Date = DateTime.Now;
            TripNo += Date.Year.ToString().Substring(2);
            TripNo += Date.Month.ToString();
            string count = "00";
            using (EntityCPAIEngine context = new EntityCPAIEngine()) {
                int query = context.TRP_HEADER.Count();
                query++;
                count = query.ToString().PadLeft(2, '0');
            }
            TripNo += count;
                return TripNo;
        }

        public void CalculateData(ref ThroughputViewModel pModel)
        {
            string Customer = pModel.Throughput_Detail.dCustomer;
            double RangeDate = 30;
            string Hour = "00";
            bool Holiday = false;
            string IfMonday = "+1";
            var Setting = GetThroughtputSetting();
            var Config = Setting.THROUGHT_PUT_COMPANY_CONFIG.DATE_CONFIG.Where(q => q.COMP_CODE == Customer).SingleOrDefault();
            if (Config != null)
            {
                RangeDate = Double.Parse(Config.COMP_DATE);
                Holiday = Config.GET_HOLIDAY == "1" ? true : false;
                IfMonday = Config.IF_MONDAY;
            }
            Hour = pModel.Throughput_Detail.dHour;
            DateTime CompleteDate = DateTime.ParseExact(pModel.Throughput_Detail.dCompleteDate, format, provider);
            pModel.Throughput_Detail.dDueDate = Convert.ToDateTime(CheckHoliday(CompleteDate, RangeDate,Holiday)).ToString(format, provider);
            foreach(var item in pModel.Throughput_Detail.ItemList)
            {
                if(item.iExchangeRatePeriod != null)
                {
                    string Date = item.iExchangeRatePeriod;
                    string Average = GetExchangeRateAverage(Date);
                    string Buying = GetExchangeRateBuy(Date);
                    string Selling = GetExchangeRateSell(Date);
                    THROUGHPUT_FEE_CAL_ViewModel cal = new THROUGHPUT_FEE_CAL_ViewModel();
                    string Volume = pModel.Throughput_Detail.dSalesUnit == "BBL" ? item.iQtyBBL : pModel.Throughput_Detail.dNetOutturnQTYML == "ML" ? item.iQtyML : item.iQtyMT;
                    cal.input_Volume_1_1 = Decimal.Parse(Volume);
                    if(item.iExchangeRate != null)
                    {
                        if (item.iExchangeRateType == "Buying")
                        {
                            Buying = item.iExchangeRate;
                        }
                        if (item.iExchangeRateType == "Average")
                        {
                            Average = item.iExchangeRate;
                        }
                    }
                    cal.input_Exchange_BuyingRate = Decimal.Parse(Buying);
                    cal.input_Exchange_AverageRate = Decimal.Parse(Average);
                    cal = calThroughPut(cal);
                    item.iExchangeRate = item.iExchangeRateType == "Buying" ? Buying : item.iExchangeRateType == "Selling" ? Selling : Average;
                    item.iFixRate = cal.FixRate1_2.ToString();
                    item.iTotalAmountUSD = cal.ServiceCharge1_3.ToString();
                    item.iTotalAmountTHB = cal.ProServiceFeeBase1_5_3.ToString();
                }
                else
                {
                    string Date = ExchangeRate(CompleteDate, Hour, IfMonday);
                    item.iExchangeRatePeriod = Date;
                    string Average = GetExchangeRateAverage(Date);
                    string Buying = GetExchangeRateBuy(Date);
                    string Selling = GetExchangeRateSell(Date);
                    THROUGHPUT_FEE_CAL_ViewModel cal = new THROUGHPUT_FEE_CAL_ViewModel();
                    string Volume = pModel.Throughput_Detail.dSalesUnit == "BBL" ? item.iQtyBBL : pModel.Throughput_Detail.dNetOutturnQTYML == "ML" ? item.iQtyML : item.iQtyMT;
                    cal.input_Volume_1_1 = Decimal.Parse(Volume);
                    cal.input_Exchange_BuyingRate = Decimal.Parse(Buying);
                    cal.input_Exchange_AverageRate = Decimal.Parse(Average);
                    cal = calThroughPut(cal);
                    item.iExchangeRate = item.iExchangeRateType == "Buying" ? Buying : item.iExchangeRateType == "Selling" ? Selling : Average;
                    item.iFixRate = cal.FixRate1_2.ToString();
                    item.iTotalAmountUSD = cal.ServiceCharge1_3.ToString();
                    item.iTotalAmountTHB = cal.ProServiceFeeBase1_5_3.ToString();
                }
            }
        }

        public DateTime CheckHoliday(DateTime Date ,double RangeDate, bool IsHoliday)
        {
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var Weekends = GetDaysBetween(Date, Date.AddDays(RangeDate)).Where(d => d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday);
                var Holidays = context.MT_HOLIDAY.Where(h => h.MH_HOL_TYPE == "Thai").ToList();
                var Holidays_temp = Holidays.Where(h => h.MH_HOL_DATE >= Date && h.MH_HOL_DATE <= Date.AddDays(RangeDate));
                int Holi_day = Holidays_temp.Count() != 0 ? Holidays_temp.Count() : 0;
                int Weekend_day = Weekends.Count();
                double RangeDate_temp = RangeDate + (Weekend_day + Holi_day);
                if (IsHoliday)
                {
                    bool Check = true;
                    do
                    {
                        RangeDate_temp = Date.AddDays(RangeDate_temp).DayOfWeek == DayOfWeek.Saturday ? RangeDate_temp + 2 : Date.AddDays(RangeDate_temp).DayOfWeek == DayOfWeek.Sunday ? RangeDate_temp + 1 : RangeDate_temp;
                        var IsHolidays = Holidays.Where(h => h.MH_HOL_DATE == Date.AddDays(RangeDate));
                        if (IsHolidays.Count() != 0)
                        {
                            RangeDate_temp += 1;
                        }
                        else
                        {
                            Check = false;
                        }
                    }
                    while (Check);
                    RangeDate = RangeDate_temp;
                }
            }
            return Date.AddDays(RangeDate);
        }

        public string ExchangeRate(DateTime Date,string Hour,string IfMonday)
        {
            string DateFromTo = "";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                bool Check = true;
                var Holidays = context.MT_HOLIDAY.Where(h => h.MH_HOL_TYPE == "Thai").ToList();
                if (Decimal.Parse(Hour) < 18)
                {
                    DateTime DateROE = Date.AddDays(-1);
                    do
                    {
                        DateROE = DateROE.DayOfWeek == DayOfWeek.Saturday ? DateROE.AddDays(-1) : DateROE.DayOfWeek == DayOfWeek.Sunday ? DateROE.AddDays(-1) : DateROE;
                        var IsHolidays = Holidays.Where(h => h.MH_HOL_DATE == DateROE);
                        if (IsHolidays.Count() != 0)
                        {
                            if(DateROE.DayOfWeek == DayOfWeek.Monday)
                            {
                                DateROE = DateROE.AddDays(Int64.Parse(IfMonday));
                            }
                            else
                            {
                                DateROE = DateROE.AddDays(-1);
                            }
                        }
                        else
                        {
                            Check = false;
                        }
                    }
                    while (Check);
                    DateFromTo = Convert.ToDateTime(DateROE).ToString(format, provider) + " to " + Convert.ToDateTime(DateROE).ToString(format, provider);
                }
                else
                {
                    do
                    {
                        Date = Date.DayOfWeek == DayOfWeek.Saturday ? Date.AddDays(-1) : Date.DayOfWeek == DayOfWeek.Sunday ? Date.AddDays(-1) : Date;
                        var IsHolidays = Holidays.Where(h => h.MH_HOL_DATE == Date);
                        if (IsHolidays.Count() != 0)
                        {
                            if (Date.DayOfWeek == DayOfWeek.Monday)
                            {
                                Date = Date.AddDays(Int64.Parse(IfMonday));
                            }
                            else
                            {
                                Date = Date.AddDays(-1);
                            }
                        }
                        else
                        {
                            Check = false;
                        }
                    }
                    while (Check);
                    DateFromTo = Convert.ToDateTime(Date).ToString(format, provider) + " to " + Convert.ToDateTime(Date).ToString(format, provider);
                }
            }

            return DateFromTo;
        }

        IEnumerable<DateTime> GetDaysBetween(DateTime start, DateTime end)
        {
            for (DateTime i = start; i < end; i = i.AddDays(1))
            {
                yield return i;
            }
        }

        public ThroughtputSetting GetThroughtputSetting()
        {
            string JsonD = MasterData.GetJsonMasterSetting("THROUGHT_PUT_COMPANY_CONFIG");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);

            ThroughtputSetting myDeserializedObjList = (ThroughtputSetting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(ThroughtputSetting));
            return myDeserializedObjList;
        }

        public void GetSearchList(ref ThroughputViewModel pModel)
        {
            List<ThroughputViewModel_SearchData> data = new List<ThroughputViewModel_SearchData>();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var query = context.TRP_HEADER.ToList();
                if (query != null)
                {
                    foreach(var item in query)
                    {
                        var querydetail = context.TRP_PRICE.Where(h => h.TRIP_NO == item.TRIP_NO);
                        string customer_name = context.MT_CUST_DETAIL.Where(c => c.MCD_FK_CUS == item.CUSTOMER).Select(c => c.MCD_NAME_1).FirstOrDefault();
                        string vessel_name = context.MT_VEHICLE.Where(v => v.VEH_ID == item.VESSEL).Select(v => v.VEH_VEH_TEXT).FirstOrDefault();
                        DateTime Date = Convert.ToDateTime(item.COMPLETED_DATE_TIME, provider);
                        string Compdate = Date.ToString("MMM", provider) + "/" + Date.Year.ToString();
                        data.Add(new ThroughputViewModel_SearchData
                        {
                            dCompleted = Compdate,
                            dCompD = item.COMPLETED_DATE_TIME,
                            dCustomer = customer_name,
                            dVessel = vessel_name,
                            dTripNo = item.TRIP_NO,
                            dQTYBBL = querydetail.Select(a => a.QTY_BBL).Sum().ToString(),
                            dQTYMT = querydetail.Select(a => a.QTY_MT).Sum().ToString(),
                            dSalesOrder = item.SO_NO
                        });
                    }
                    string Customer = String.IsNullOrEmpty(pModel.Throughput_Search.sCustomer) ? string.Empty : pModel.Throughput_Search.sCustomer; 
                    if (!string.IsNullOrEmpty(Customer))
                        data = data.Where(p => p.dCustomer != null && p.dCustomer.ToUpper().Contains(Customer)).ToList();
                    string Month = String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryMonth) ? string.Empty : pModel.Throughput_Search.sDeliveryMonth;
                    string Year = String.IsNullOrEmpty(pModel.Throughput_Search.sDeliveryYear) ? string.Empty : pModel.Throughput_Search.sDeliveryYear;
                    if (!string.IsNullOrEmpty(Month) && !string.IsNullOrEmpty(Year))
                    {
                        DateTime RangeFrom = DateTime.ParseExact("01/" + Month + "/" + Year, format, provider);
                        DateTime RangeTo = DateTime.ParseExact(DateTime.DaysInMonth(Int32.Parse(Year),Int32.Parse(Month))+ "/" + Month + "/" + Year, format, provider);
                        data = data.Where(p => p.dCompD >= RangeFrom && p.dCompD <= RangeTo).ToList();
                    }
                    pModel.Throughput_Search.SearchData = data;
                }
            }
        }

        public void GetData(ref ThroughputViewModel pModel,string pTripNo)
        {
            ReturnValue rtn = new ReturnValue();
            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    TRP_HEADER entTPF = new TRP_HEADER();
                    TRP_PRICE entTPP = new TRP_PRICE();
                    entTPF = context.TRP_HEADER.SingleOrDefault(h => h.TRIP_NO == pTripNo);
                    if(entTPF != null)
                    {
                        string date = entTPF.COMPLETED_DATE_TIME.ToString();
                        DateTime Date = Convert.ToDateTime(date,provider);
                        pModel.Throughput_Detail = new ThroughoutFeeViewModel_Detail();
                        pModel.Throughput_Detail.dTripNo = entTPF.TRIP_NO;
                        pModel.Throughput_Detail.dCustomer = entTPF.CUSTOMER;
                        pModel.Throughput_Detail.dCompleteDate = Convert.ToDateTime(entTPF.COMPLETED_DATE_TIME).ToString(format, provider);
                        pModel.Throughput_Detail.dDueDate = Convert.ToDateTime(entTPF.DUE_DATE).ToString(format, provider);
                        pModel.Throughput_Detail.dHour = Date.Hour.ToString();
                        pModel.Throughput_Detail.dMin = Date.Minute.ToString().PadRight(2, '0');
                        pModel.Throughput_Detail.dSalesUnit = entTPF.SALES_UNIT;
                        pModel.Throughput_Detail.dShipTo = entTPF.SHIP_TO;
                        pModel.Throughput_Detail.dVesselID = entTPF.VESSEL;

                        var queryitem = context.TRP_PRICE.Where(d => d.TRIP_NO == pTripNo);
                        if(queryitem.Count() != 0)
                        {
                            pModel.Throughput_Detail.ItemList = new List<ThroughputFeeViewModel_DetailList>();
                            foreach(var item in queryitem)
                            {
                                pModel.Throughput_Detail.ItemList.Add(new ThroughputFeeViewModel_DetailList
                                {
                                    iExchangeRatePeriod = Convert.ToDateTime(item.EXCHANGE_RATE_FROM).ToString(format, provider) + " to " + Convert.ToDateTime(item.EXCHANGE_RATE_TO).ToString(format, provider),
                                    iExchangeRateType = item.EXCHANGE_RATE_TYPE,
                                    iExchangeRate = item.EXCHANGE_RATE.ToString(),
                                    iFixRate = item.FIX_RATE.ToString(),
                                    iIndex = item.ITEM_NO.ToString(),
                                    iPriceStatus = item.PRICE_STATUS,
                                    iQtyBBL = item.QTY_BBL.ToString(),
                                    iQtyML = item.QTY_ML.ToString(),
                                    iQtyMT = item.QTY_MT.ToString(),
                                    iTotalAmountTHB = item.TOTAL_AMOUNT_THB.ToString(),
                                    iTotalAmountUSD = item.TOTAL_AMOUNT_USD.ToString()
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                rtn.Message = ex.Message;
                rtn.Status = false;
            }
        }

        public ReturnValue SaveData(ref ThroughputViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                TRP_HEADER entTPH = new TRP_HEADER();
                THROUGHPUT_DAL DAL = new THROUGHPUT_DAL();

                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        int Hour = Int32.Parse(pModel.Throughput_Detail.dHour);
                        int Min = Int32.Parse(pModel.Throughput_Detail.dMin);
                        TimeSpan Time = new TimeSpan(Hour, Min, 00);
                        if (!String.IsNullOrEmpty(pModel.Throughput_Detail.dTripNo))
                        {
                            string sTripNo = pModel.Throughput_Detail.dTripNo;
                            entTPH.TRIP_NO = sTripNo;
                            entTPH.VESSEL = pModel.Throughput_Detail.dVesselID;
                            entTPH.SALES_ORG = "1100";
                            entTPH.COMPLETED_DATE_TIME = String.IsNullOrEmpty(pModel.Throughput_Detail.dCompleteDate) ? (DateTime?)null : DateTime.ParseExact(pModel.Throughput_Detail.dCompleteDate, format, provider) + Time;
                            entTPH.DUE_DATE = String.IsNullOrEmpty(pModel.Throughput_Detail.dDueDate) ? (DateTime?)null : DateTime.ParseExact(pModel.Throughput_Detail.dDueDate, format, provider);
                            entTPH.SHIP_TO = pModel.Throughput_Detail.dShipTo;
                            entTPH.CUSTOMER = pModel.Throughput_Detail.dCustomer;
                            entTPH.MET_NUM = "700158";
                            entTPH.SALES_UNIT = pModel.Throughput_Detail.dSalesUnit;
                            entTPH.DISTRIBUTION_CHANNEL = "";
                            entTPH.PLANT = "1200";
                            entTPH.SO_NO = null;
                            entTPH.SO_CREATED_DATE = null;
                            entTPH.SO_CREATED_BY = null;
                            entTPH.SO_UPDATED_BY = null;
                            entTPH.SO_UPDATED_DATE = null;
                            entTPH.UPDATED_BY = null;
                            entTPH.UPDATED_DATE = null;
                            entTPH.CREATED_DATE = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), format, provider);
                            entTPH.CREATED_BY = Const.User.UserName;
                            DAL.Save(entTPH);

                            foreach (var item in pModel.Throughput_Detail.ItemList)
                            {
                                TRP_PRICE entTPP = new TRP_PRICE();
                                string[] tempDate = item.iExchangeRatePeriod.Split(' ');
                                DateTime DateFrom = DateTime.ParseExact(tempDate[0], format, provider);
                                DateTime DateTo = DateTime.ParseExact(tempDate[2], format, provider);
                                entTPP.TRIP_NO = sTripNo;
                                entTPP.ITEM_NO = Decimal.Parse(item.iIndex);
                                entTPP.PRICE_STATUS = item.iPriceStatus;
                                entTPP.QTY_BBL = Decimal.Parse(item.iQtyBBL);
                                entTPP.QTY_ML = Decimal.Parse(item.iQtyML);
                                entTPP.QTY_MT = Decimal.Parse(item.iQtyMT);
                                entTPP.FIX_RATE = Decimal.Parse(item.iFixRate);
                                entTPP.TOTAL_AMOUNT_USD = Decimal.Parse(item.iTotalAmountUSD);
                                entTPP.TOTAL_AMOUNT_THB = Decimal.Parse(item.iTotalAmountTHB);
                                entTPP.EXCHANGE_RATE_FROM = DateFrom;
                                entTPP.EXCHANGE_RATE_TO = DateTo;
                                entTPP.EXCHANGE_RATE_TYPE = item.iExchangeRateType;
                                entTPP.CREATED_BY = Const.User.UserName;
                                entTPP.CREATED_DATE = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), format, provider);
                                entTPP.UPDATED_BY = null;
                                entTPP.UPDATED_DATE = null;
                                DAL.SaveDetail(entTPP);
                            }
                            dbContextTransaction.Commit();
                            rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                            rtn.Status = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        rtn.Message = ex.Message;
                        rtn.Status = false;
                    }
                }
            }
            return rtn;
        }

        public void EditData(ref ThroughputViewModel pModel)
        {
            ReturnValue rtn = new ReturnValue();
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                THROUGHPUT_DAL DAL = new THROUGHPUT_DAL();
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        TRP_HEADER entTPH = new TRP_HEADER();
                        string sTripNo = pModel.Throughput_Detail.dTripNo;
                        entTPH = context.TRP_HEADER.SingleOrDefault(a => a.TRIP_NO == sTripNo);
                        if (entTPH != null)
                        {
                            entTPH.COMPLETED_DATE_TIME = String.IsNullOrEmpty(pModel.Throughput_Detail.dCompleteDate) ? (DateTime?)null : DateTime.ParseExact(pModel.Throughput_Detail.dCompleteDate, format, provider);
                            entTPH.DUE_DATE = String.IsNullOrEmpty(pModel.Throughput_Detail.dDueDate) ? (DateTime?)null : DateTime.ParseExact(pModel.Throughput_Detail.dDueDate, format, provider);
                            entTPH.SHIP_TO = pModel.Throughput_Detail.dShipTo;
                            entTPH.SALES_UNIT = pModel.Throughput_Detail.dSalesUnit;
                            entTPH.PLANT = "1200";
                            entTPH.UPDATED_BY = Const.User.UserName;
                            entTPH.UPDATED_DATE = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), format, provider);
                            DAL.Update(entTPH);
                        }

                        foreach (var item in pModel.Throughput_Detail.ItemList)
                        {
                            TRP_PRICE entTPP = new TRP_PRICE();
                            decimal index = Decimal.Parse(item.iIndex);
                            entTPP = context.TRP_PRICE.SingleOrDefault(a => a.TRIP_NO == sTripNo && a.ITEM_NO == index);
                            if (entTPP != null)
                            {
                                string[] tempDate = item.iExchangeRatePeriod.Split(' ');
                                DateTime DateFrom = DateTime.ParseExact(tempDate[0], format, provider);
                                DateTime DateTo = DateTime.ParseExact(tempDate[2], format, provider);
                                entTPP.TRIP_NO = sTripNo;
                                entTPP.ITEM_NO = Decimal.Parse(item.iIndex);
                                entTPP.PRICE_STATUS = item.iPriceStatus;
                                entTPP.QTY_BBL = Decimal.Parse(item.iQtyBBL);
                                entTPP.QTY_ML = Decimal.Parse(item.iQtyML);
                                entTPP.QTY_MT = Decimal.Parse(item.iQtyMT);
                                entTPP.FIX_RATE = Decimal.Parse(item.iFixRate);
                                entTPP.TOTAL_AMOUNT_USD = Decimal.Parse(item.iTotalAmountUSD);
                                entTPP.TOTAL_AMOUNT_THB = Decimal.Parse(item.iTotalAmountTHB);
                                entTPP.EXCHANGE_RATE_FROM = DateFrom;
                                entTPP.EXCHANGE_RATE_TO = DateTo;
                                entTPP.EXCHANGE_RATE_TYPE = item.iExchangeRateType;
                                entTPP.UPDATED_BY = Const.User.UserName;
                                entTPP.UPDATED_DATE = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), format, provider);
                                DAL.UpdateDetail(entTPP);
                            }
                            else
                            {
                                string[] tempDate = item.iExchangeRatePeriod.Split(' ');
                                DateTime DateFrom = DateTime.ParseExact(tempDate[0], format, provider);
                                DateTime DateTo = DateTime.ParseExact(tempDate[2], format, provider);
                                entTPP.TRIP_NO = sTripNo;
                                entTPP.ITEM_NO = Decimal.Parse(item.iIndex);
                                entTPP.PRICE_STATUS = item.iPriceStatus;
                                entTPP.QTY_BBL = Decimal.Parse(item.iQtyBBL);
                                entTPP.QTY_ML = Decimal.Parse(item.iQtyML);
                                entTPP.QTY_MT = Decimal.Parse(item.iQtyMT);
                                entTPP.FIX_RATE = Decimal.Parse(item.iFixRate);
                                entTPP.TOTAL_AMOUNT_USD = Decimal.Parse(item.iTotalAmountUSD);
                                entTPP.TOTAL_AMOUNT_THB = Decimal.Parse(item.iTotalAmountTHB);
                                entTPP.EXCHANGE_RATE_FROM = DateFrom;
                                entTPP.EXCHANGE_RATE_TO = DateTo;
                                entTPP.EXCHANGE_RATE_TYPE = item.iExchangeRateType;
                                entTPP.CREATED_BY = Const.User.UserName;
                                entTPP.CREATED_DATE = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), format, provider);
                                entTPP.UPDATED_BY = null;
                                entTPP.UPDATED_DATE = null;
                                DAL.SaveDetail(entTPP);
                            }
                        }
                        dbContextTransaction.Commit();
                        rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                        rtn.Status = true;
                    }
                    catch (Exception ex)
                    {
                        dbContextTransaction.Rollback();
                        rtn.Message = ex.Message;
                        rtn.Status = false;
                    }
                }
            }
        }

        public void CreateSO(ref ThroughputViewModel pModel)
        {
            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();
                #region "For Use"
                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{
                //    string sTripNo = pModel.Throughput_Search.sTripNo;
                //    string sCustNum = pModel.Throughput_Search.sCustomer;
                //    //////////////TODO/////////////
                //    string sCrudeTypeId = "ABCD";

                //    var qryThroughputFee = context.THROUGHPUT_FEE.Where(z => z.TPF_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && z.TPF_CUST_NUM.ToUpper().Equals(sCustNum.ToUpper()) && z.TPF_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));

                //    if (qryThroughputFee != null)
                //    {
                //        var qryThroughputDetail = context.THROUGHPUT_DETAIL.Where(x => x.TPD_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && x.TPD_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));
                //        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryThroughputFee.ToList()[0].TPF_PLANT.ToUpper()));
                //        string sCompanyCode = "";
                //        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                //        if (qryThroughputDetail != null)
                //        {
                //            string sPricePer;
                //            switch (qryThroughputDetail.ToList()[0].TPD_PRICE_PER.ToUpper())
                //            {
                //                case "NBL":
                //                case "OBL":
                //                    sPricePer = "BBL"; break;
                //                case "NMT":
                //                case "OMT":
                //                    sPricePer = "MT"; break;
                //                case "NML":
                //                case "OML":
                //                    sPricePer = "ML"; break;
                //                default: sPricePer = ""; break;
                //            }

                //            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();
                //            obj.SalePrice = new SalesPriceCreateModel();
                //            obj.SalePrice.DATAB = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd");
                //            obj.SalePrice.DATBI = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd");
                //            obj.SalePrice.KBETR = qryThroughputDetail.ToList()[0].TPD_PRICE.ToString();
                //            obj.SalePrice.KMEIN = sPricePer;
                //            obj.SalePrice.KONWA = qryThroughputDetail.ToList()[0].TPD_UNIT_ID;
                //            obj.SalePrice.KSCHL = "ZCR1";
                //            obj.SalePrice.KUNNR = qryThroughputFee.ToList()[0].TPF_CUST_NUM;
                //            obj.SalePrice.MATNR = qryThroughputFee.ToList()[0].TPF_CRUDE_TYPE_ID;
                //            obj.SalePrice.VKORG = sCompanyCode;
                //            obj.SalePrice.VTWEG = "31";
                //            obj.SalePrice.WERKS = qryThroughputFee.ToList()[0].TPF_PLANT;
                //            obj.SalePrice.ZZVSTEL = "12SV";

                //            obj.SaleOrder = new SaleOrderModel();
                //            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //            {
                //                DOC_TYPE = "Z1V1",
                //                SALES_ORG = sCompanyCode,
                //                DISTR_CHAN = "31",
                //                DIVISION = "00",
                //                REQ_DATE_H = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                DATE_TYPE = "1",
                //                PURCH_NO_C = "CIP_THROUGHPUTFEE_01",
                //                PURCH_NO_S = "CIP_THROUGHPUTFEE_01",
                //                CURRENCY = "THB"
                //            });

                //            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //            {
                //                ITM_NUMBER = "000010",
                //                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()),
                //                CURRENCY = qryThroughputDetail.ToList()[0].TPD_UNIT_ID
                //            });

                //            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //            {
                //                ITM_NUMBER = "000010",
                //                MATERIAL = qryThroughputFee.ToList()[0].TPF_CRUDE_TYPE_ID,
                //                PLANT = qryThroughputFee.ToList()[0].TPF_PLANT,
                //                //ITEM_CATEG = "ZTW1",                                
                //                SALES_UNIT = sPricePer
                //            });

                //            obj.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //            obj.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //            {
                //                PARTN_NUMB = qryThroughputFee.ToList()[0].TPF_CUST_NUM
                //            });

                //            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //            {
                //                ITM_NUMBER = "000010",
                //                REQ_DATE = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                DATE_TYPE = "1",
                //                REQ_QTY = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString())
                //            });

                //            obj.SaleOrder.SALESDOCUMENT_IN = "";
                //            obj.SaleOrder.Flag = "";

                //            var json = new JavaScriptSerializer().Serialize(obj);

                //            RequestData req = new RequestData();
                //            req.function_id = ConstantPrm.FUNCTION.F10000059;
                //            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //            req.state_name = "";
                //            req.req_parameters = new req_parameters();
                //            req.req_parameters.p = new List<p>();

                //            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //            req.req_parameters.p.Add(new p { k = "system", v = "SOThroughputFee" });
                //            req.req_parameters.p.Add(new p { k = "trip_no", v = sTripNo });
                //            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //            req.extra_xml = "";

                //            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                //            resData = service.CallService(req);

                //            if (resData.result_code == "1")
                //            {
                //                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //                rtn.Status = true;
                //            }
                //            else
                //            {
                //                rtn.Message = resData.result_desc;
                //                rtn.Status = false;
                //            }
                //        }

                //    }

                //}
                #endregion

                #region "For Test"
                //For Test
                //SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();
                //obj1.SalePrice = new SalesPriceCreateModel();
                //obj1.SalePrice.DATAB = "2017-06-27";
                //obj1.SalePrice.DATBI = "2017-06-27";
                //obj1.SalePrice.KBETR = "1609.140";
                //obj1.SalePrice.KMEIN = "BBL";
                //obj1.SalePrice.KONWA = "TH4";
                //obj1.SalePrice.KSCHL = "ZCR1";
                //obj1.SalePrice.KUNNR = "0000000023";
                //obj1.SalePrice.MATNR = "YMURBAN";
                //obj1.SalePrice.VKORG = "1100";
                //obj1.SalePrice.VTWEG = "31";
                //obj1.SalePrice.WERKS = "1200";
                //obj1.SalePrice.ZZVSTEL = "12SV";

                //obj1.SaleOrder = new SaleOrderModel();
                //obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //{
                //    DOC_TYPE = "Z1V1",
                //    SALES_ORG = "1100",
                //    DISTR_CHAN = "31",
                //    DIVISION = "00",
                //    REQ_DATE_H = "2017-06-27",
                //    DATE_TYPE = "1",
                //    PURCH_NO_C = "CIP_THROUGHPUTFEE_01",
                //    PURCH_NO_S = "CIP_THROUGHPUTFEE_01",
                //    CURRENCY = "THB"
                //});

                //obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //{
                //    ITM_NUMBER = "000010",
                //    COND_VALUE = 33.3M,
                //    CURRENCY = "TH5"
                //});

                //obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //{
                //    ITM_NUMBER = "000010",
                //    MATERIAL = "YMURBAN",
                //    PLANT = "1200",
                //    //ITEM_CATEG = "ZTW1",
                //    SALES_UNIT = "BBL"
                //});

                //obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                //obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                //{
                //    PARTN_NUMB = "0000000023"
                //});

                //obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //{
                //    ITM_NUMBER = "000010",
                //    REQ_DATE = "2017-06-27",
                //    DATE_TYPE = "1",
                //    REQ_QTY = 1609.140M
                //});

                //obj1.SaleOrder.SALESDOCUMENT_IN = "";
                //obj1.SaleOrder.Flag = "";

                //var json1 = new JavaScriptSerializer().Serialize(obj1);

                //RequestData req1 = new RequestData();
                //req1.function_id = ConstantPrm.FUNCTION.F10000059;
                //req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //req1.state_name = "";
                //req1.req_parameters = new req_parameters();
                //req1.req_parameters.p = new List<p>();

                //req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //req1.req_parameters.p.Add(new p { k = "system", v = "SOThroughputFee" });
                //req1.req_parameters.p.Add(new p { k = "trip_no", v = "TP-2017-07-001" });
                //req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                //req1.extra_xml = "";

                //ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                //resData = service1.CallService(req1);

                //if (resData.result_code == "1")
                //{
                //    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //    rtn.Status = true;
                //}
                //else
                //{
                //    rtn.Message = resData.result_desc;
                //    rtn.Status = false;
                //}
                #endregion

            }
            catch (Exception ex)
            {

            }

        }

        public void UpdateSO(ref ThroughputViewModel pModel)
        {
            try
            {
                ReturnValue rtn = new ReturnValue();

                ResponseData resData = new ResponseData();

                #region "For Use"
                //using (EntityCPAIEngine context = new EntityCPAIEngine())
                //{
                //    string sTripNo = pModel.Throughput_Search.SearchData[Index].dTripNo;
                //    string sCustNum = pModel.Throughput_Search.SearchData[Index].dCustomer;
                //    string sCrudeTypeId = pModel.Throughput_Search.SearchData[Index].dProduct;

                //    var qryThroughputFee = context.THROUGHPUT_FEE.Where(z => z.TPF_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && z.TPF_CUST_NUM.ToUpper().Equals(sCustNum.ToUpper()) && z.TPF_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));
                //    if (qryThroughputFee != null)
                //    {
                //        var qryThroughputDetail = context.THROUGHPUT_DETAIL.Where(x => x.TPD_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && x.TPD_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));
                //        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryThroughputFee.ToList()[0].TPF_PLANT.ToUpper()));
                //        string sCompanyCode = "";
                //        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                //        if (qryThroughputDetail != null)
                //        {
                //            string sPricePer;
                //            switch (qryThroughputDetail.ToList()[0].TPD_PRICE_PER.ToUpper())
                //            {
                //                case "NBL":
                //                case "OBL":
                //                    sPricePer = "BBL"; break;
                //                case "NMT":
                //                case "OMT":
                //                    sPricePer = "MT"; break;
                //                case "NML":
                //                case "OML":
                //                    sPricePer = "ML"; break;
                //                default: sPricePer = ""; break;
                //            }

                //            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();

                //            obj.SaleOrder = new SaleOrderModel();
                //            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                //            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                //            {
                //                DOC_TYPE = "Z1V1",
                //                REQ_DATE_H = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd")
                //            });

                //            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                //            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                //            {
                //                ITM_NUMBER = "000010",
                //                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()),
                //                CURRENCY = qryThroughputDetail.ToList()[0].TPD_UNIT_ID
                //            });

                //            obj.SaleOrder.SALESDOCUMENT_IN = qryThroughputFee.ToList()[0].TPF_SALEORDER;
                //            obj.SaleOrder.Flag = "";

                //            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                //            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                //            {
                //                ITM_NUMBER = "000010",
                //                MATERIAL = qryThroughputFee.ToList()[0].TPF_CRUDE_TYPE_ID,
                //                PLANT = qryThroughputFee.ToList()[0].TPF_PLANT,
                //                //ITEM_CATEG = "ZTW1",
                //                PURCH_DATE = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),                                
                //                SALES_UNIT = sPricePer
                //            });

                //            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                //            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                //            {
                //                ITM_NUMBER = "000010",
                //                REQ_DATE = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                //                REQ_QTY = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString())
                //            });

                //            var json = new JavaScriptSerializer().Serialize(obj);

                //            RequestData req = new RequestData();
                //            req.function_id = ConstantPrm.FUNCTION.F10000059;
                //            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                //            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                //            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                //            req.state_name = "";
                //            req.req_parameters = new req_parameters();
                //            req.req_parameters.p = new List<p>();

                //            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                //            req.req_parameters.p.Add(new p { k = "system", v = "SOThroughputFee" });
                //            req.req_parameters.p.Add(new p { k = "trip_no", v = sTripNo.ToUpper() });
                //            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                //            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                //            req.extra_xml = "";

                //            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                //            resData = service.CallService(req);

                //            if (resData.result_code == "1")
                //            {
                //                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                //                rtn.Status = true;
                //            }
                //            else
                //            {
                //                rtn.Message = resData.result_desc;
                //                rtn.Status = false;
                //            }
                //        }
                //    }
                //}
                #endregion

                #region "For Test"
                //For Test
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1V1",
                    REQ_DATE_H = "2017-06-27"

                });

                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = "000010",
                    COND_VALUE = 33.3M,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = "000010",
                    MATERIAL = "YMURBAN",
                    PLANT = "1200",
                    //ITEM_CATEG = "ZTW1",
                    SALES_UNIT = "BBL"
                });

                obj1.SaleOrder.mORDER_PARTNERS = new List<SaleOrderModel.BAPIPARNR>();
                obj1.SaleOrder.mORDER_PARTNERS.Add(new SaleOrderModel.BAPIPARNR
                {
                    PARTN_NUMB = "0000000023"
                });

                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = "000010",
                    REQ_DATE = "2017-06-27",
                    DATE_TYPE = "1",
                    REQ_QTY = 1609.140M
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = "1130002543";
                obj1.SaleOrder.Flag = "";

                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOThroughputFee" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = "TP-2017-07-001" });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }
                #endregion

            }
            catch (Exception ex)
            {

            }
        }

        public void CancelSO(ref ThroughputViewModel pModel, int Index)
        {
            try
            {
                ReturnValue rtn = new ReturnValue();
                ResponseData resData = new ResponseData();

                #region "For Use"
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sTripNo = pModel.Throughput_Search.SearchData[Index].dTripNo;
                    string sCustNum = pModel.Throughput_Search.SearchData[Index].dCustomer;
                    string sCrudeTypeId = pModel.Throughput_Search.SearchData[Index].dProduct;

                    var qryThroughputFee = context.THROUGHPUT_FEE.Where(z => z.TPF_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && z.TPF_CUST_NUM.ToUpper().Equals(sCustNum.ToUpper()) && z.TPF_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));
                    if (qryThroughputFee != null)
                    {
                        var qryThroughputDetail = context.THROUGHPUT_DETAIL.Where(x => x.TPD_TRIP_NO.ToUpper().Equals(sTripNo.ToUpper()) && x.TPD_CRUDE_TYPE_ID.ToUpper().Equals(sCrudeTypeId.ToUpper()));
                        var qryCompany = context.MT_PLANT.Where(c => c.MPL_PLANT.ToUpper().Equals(qryThroughputFee.ToList()[0].TPF_PLANT.ToUpper()));
                        string sCompanyCode = "";
                        if (qryCompany == null) { sCompanyCode = ""; } else { sCompanyCode = qryCompany.ToList()[0].MPL_COMPANY_CODE; }

                        if (qryThroughputDetail != null)
                        {
                            string sPricePer;
                            switch (qryThroughputDetail.ToList()[0].TPD_PRICE_PER.ToUpper())
                            {
                                case "NBL":
                                case "OBL":
                                    sPricePer = "BBL"; break;
                                case "NMT":
                                case "OMT":
                                    sPricePer = "MT"; break;
                                case "NML":
                                case "OML":
                                    sPricePer = "ML"; break;
                                default: sPricePer = ""; break;
                            }

                            SaleOrderSalePriceModel obj = new SaleOrderSalePriceModel();

                            obj.SaleOrder = new SaleOrderModel();
                            obj.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                            obj.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                            {
                                DOC_TYPE = "Z1P3",
                                REQ_DATE_H = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd")
                            });

                            obj.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                            obj.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                            {
                                ITM_NUMBER = "000010",
                                COND_VALUE = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()),
                                CURRENCY = qryThroughputDetail.ToList()[0].TPD_UNIT_ID
                            });

                            obj.SaleOrder.SALESDOCUMENT_IN = qryThroughputFee.ToList()[0].TPF_SALEORDER;
                            obj.SaleOrder.Flag = "X";

                            obj.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                            obj.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                            {
                                ITM_NUMBER = "000010",
                                MATERIAL = qryThroughputFee.ToList()[0].TPF_CRUDE_TYPE_ID,
                                PLANT = qryThroughputFee.ToList()[0].TPF_PLANT,
                                //ITEM_CATEG = "ZTW1",
                                PURCH_DATE = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                                SALES_UNIT = sPricePer
                            });

                            obj.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                            obj.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                            {
                                ITM_NUMBER = "000010",
                                REQ_DATE = String.IsNullOrEmpty(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString()) ? "" : DateTime.ParseExact(qryThroughputFee.ToList()[0].TPF_DELIVERY_DATE.ToString(), format, provider).ToString("yyyy-MM-dd"),
                                REQ_QTY = Decimal.Parse(String.IsNullOrEmpty(qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString()) ? "0" : qryThroughputDetail.ToList()[0].TPD_TOTAL.ToString())
                            });

                            var json = new JavaScriptSerializer().Serialize(obj);

                            RequestData req = new RequestData();
                            req.function_id = ConstantPrm.FUNCTION.F10000059;
                            req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                            req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                            req.req_transaction_id = ConstantPrm.EnginGetEngineID();
                            req.state_name = "";
                            req.req_parameters = new req_parameters();
                            req.req_parameters.p = new List<p>();

                            req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                            req.req_parameters.p.Add(new p { k = "system", v = "SOThroughput" });
                            req.req_parameters.p.Add(new p { k = "trip_no", v = sTripNo.ToUpper() });
                            req.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                            req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                            req.extra_xml = "";

                            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                            resData = service.CallService(req);

                            if (resData.result_code == "1")
                            {
                                rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                                rtn.Status = true;
                            }
                            else
                            {
                                rtn.Message = resData.result_desc;
                                rtn.Status = false;
                            }
                        }
                    }
                }
                #endregion

                #region "For Test"
                SaleOrderSalePriceModel obj1 = new SaleOrderSalePriceModel();

                obj1.SaleOrder = new SaleOrderModel();
                obj1.SaleOrder.mORDER_HEADER_IN = new List<SaleOrderModel.BAPISDHD1>();
                obj1.SaleOrder.mORDER_HEADER_IN.Add(new SaleOrderModel.BAPISDHD1
                {
                    DOC_TYPE = "Z1P3",
                    REQ_DATE_H = "2017-06-30"
                });

                obj1.SaleOrder.SALESDOCUMENT_IN = "1130002544";
                obj1.SaleOrder.Flag = "X";

                obj1.SaleOrder.mORDER_CONDITIONS_IN = new List<SaleOrderModel.BAPICOND>();
                obj1.SaleOrder.mORDER_CONDITIONS_IN.Add(new SaleOrderModel.BAPICOND
                {
                    ITM_NUMBER = "000010",
                    COND_VALUE = 33.3M,
                    CURRENCY = "TH5"
                });

                obj1.SaleOrder.mORDER_ITEMS_IN = new List<SaleOrderModel.BAPISDITM>();
                obj1.SaleOrder.mORDER_ITEMS_IN.Add(new SaleOrderModel.BAPISDITM
                {
                    ITM_NUMBER = "000010",
                    MATERIAL = "YMURBAN",
                    PLANT = "1200",
                    //ITEM_CATEG = "ZTW1",
                    //PURCH_DATE = "2017-06-27",
                    SALES_UNIT = "BBL"
                });

                obj1.SaleOrder.mORDER_SCHEDULES_IN = new List<SaleOrderModel.BAPISCHDL>();
                obj1.SaleOrder.mORDER_SCHEDULES_IN.Add(new SaleOrderModel.BAPISCHDL
                {
                    ITM_NUMBER = "000010",
                    SCHED_LINE = "0001",
                    REQ_DATE = "2017-06-30",
                    REQ_QTY = 1609.140M
                });

                var json1 = new JavaScriptSerializer().Serialize(obj1);

                RequestData req1 = new RequestData();
                req1.function_id = ConstantPrm.FUNCTION.F10000059;
                req1.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                req1.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req1.req_transaction_id = ConstantPrm.EnginGetEngineID();
                req1.state_name = "";
                req1.req_parameters = new req_parameters();
                req1.req_parameters.p = new List<p>();

                req1.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                req1.req_parameters.p.Add(new p { k = "system", v = "SOThroughputFee" });
                req1.req_parameters.p.Add(new p { k = "trip_no", v = "TP-2017-07-001" });
                req1.req_parameters.p.Add(new p { k = "channel", v = "WEB" });
                req1.req_parameters.p.Add(new p { k = "data_detail_input", v = json1 });
                req1.extra_xml = "";

                ServiceProvider.ProjService service1 = new ServiceProvider.ProjService();

                resData = service1.CallService(req1);

                if (resData.result_code == "1")
                {
                    rtn.Message = ConstantPrm.ServiceModelMessageSuccess;
                    rtn.Status = true;
                }
                else
                {
                    rtn.Message = resData.result_desc;
                    rtn.Status = false;
                }
                #endregion
            }
            catch (Exception ex)
            {

            }
        }


        public void setInitail_ddl(ref ThroughputViewModel pModel)
        {
            if (pModel.ddl_Product.Count == 0)
            {
                pModel.ddl_Product = new List<SelectListItem>();
                pModel.ddl_Product = getMaterial();
            }
            if (pModel.ddl_Customer == null)
            {
                pModel.ddl_Customer = new List<SelectListItem>();
                pModel.ddl_Customer = getCustomer();
            }
            else if(pModel.ddl_Customer.Count == 0)
            {
                pModel.ddl_Customer = new List<SelectListItem>();
                pModel.ddl_Customer = getCustomer();
            }

            if (pModel.ddl_Plant == null)
            {
                pModel.ddl_Plant = new List<SelectListItem>();
                pModel.ddl_Plant = getPlant("1100,1400");
            }
            if (pModel.ddl_Plant == null)
            {
                pModel.ddl_Plant = new List<SelectListItem>();
                pModel.ddl_Plant = getPlant("1100,1400");
            }
            else if (pModel.ddl_Unit.Count == 0)
            {
                pModel.ddl_Unit = new List<SelectListItem>();
                pModel.ddl_Unit = getUnit();
            }

            if (pModel.ddl_Vessel == null)
            {
                pModel.ddl_Vessel = new List<SelectListItem>();
                pModel.ddl_Vessel = getVessel();
            }
            else if (pModel.ddl_Vessel.Count == 0)
            {
                pModel.ddl_Vessel = new List<SelectListItem>();
                pModel.ddl_Vessel = getVessel();
            }

            if (pModel.ddl_PriceStatus == null)
            {
                pModel.ddl_PriceStatus = new List<SelectListItem>();
                pModel.ddl_PriceStatus = getPriceStatus();
            }
            else if (pModel.ddl_PriceStatus.Count == 0)
            {
                pModel.ddl_PriceStatus = new List<SelectListItem>();
                pModel.ddl_PriceStatus = getPriceStatus();
            }

            if (pModel.ddl_ExchangeRate_Type == null)
            {
                pModel.ddl_ExchangeRate_Type = new List<SelectListItem>();
                pModel.ddl_ExchangeRate_Type = getExchangeRate();
            }
             else if (pModel.ddl_ExchangeRate_Type.Count == 0)
            {
                pModel.ddl_ExchangeRate_Type = new List<SelectListItem>();
                pModel.ddl_ExchangeRate_Type = getExchangeRate();
            }

            if (pModel.ddl_Hour == null)
            {
                pModel.ddl_Hour = new List<SelectListItem>();
                pModel.ddl_Hour = getHour();
            }
            else if (pModel.ddl_Hour.Count == 0)
            {
                pModel.ddl_Hour = new List<SelectListItem>();
                pModel.ddl_Hour = getHour();
            }

            if (pModel.ddl_Min == null)
            {
                pModel.ddl_Min = new List<SelectListItem>();
                pModel.ddl_Min = getMin();
            }
            else if (pModel.ddl_Min.Count == 0)
            {
                pModel.ddl_Min = new List<SelectListItem>();
                pModel.ddl_Min = getMin();
            }

        }

        #region DropDown

        public static List<SelectListItem> getHour()
        {
            List<SelectListItem> lstHour = new List<SelectListItem>();
            for (int hour = 0; hour <= 23; hour++)
            {
                lstHour.Add(new SelectListItem { Value = hour.ToString().PadLeft(2, '0'), Text = hour.ToString().PadLeft(2, '0') });
            }
            return lstHour;
        }

        public static List<SelectListItem> getMin()
        {
            List<SelectListItem> lstMin = new List<SelectListItem>();
            for (int min = 0; min <= 60; min++)
            {
                lstMin.Add(new SelectListItem { Value = min.ToString().PadLeft(2, '0'), Text = min.ToString().PadLeft(2, '0') });
            }
            return lstMin;
        }

        public static List<SelectListItem> getUnit()
        {
            List<SelectListItem> lstUnit = new List<SelectListItem>();

            lstUnit.Add(new SelectListItem { Value = "BBL", Text = "BBL" });
            lstUnit.Add(new SelectListItem { Value = "MT", Text = "MT" });
            lstUnit.Add(new SelectListItem { Value = "ML", Text = "ML" });

            return lstUnit;
        }

        public static List<SelectListItem> getVessel(string type = "VESSEL", bool isOptional = false, string message = "")
        {
            string system = "PCF";
            List<MT_VEHICLE> mt_vehicle = VehicleDAL.GetVehicle(system, ACTIVE, type).OrderBy(x => x.VEH_VEH_TEXT).ToList();
            return insertSelectListValue(mt_vehicle.Select(x => x.VEH_VEH_TEXT).ToList(), mt_vehicle.Select(x => x.VEH_ID).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getPriceStatus()
        {
            List<SelectListItem> dList = new List<SelectListItem>();

            dList.Add(new SelectListItem { Value = "1", Text = "Provisional QTY (FX M9)" });
            dList.Add(new SelectListItem { Value = "2", Text = "Adjust FX Agreement" });
            dList.Add(new SelectListItem { Value = "3", Text = "Actual QTY (FX M9)" });
            dList.Add(new SelectListItem { Value = "4", Text = "Final Adjust FX Agreement" });

            return dList;
        }

        public static List<SelectListItem> getExchangeRate()
        {
            List<SelectListItem> List = new List<SelectListItem>();
            List.Add(new SelectListItem { Value = "Buying", Text = "Buying" });
            List.Add(new SelectListItem { Value = "Selling", Text = "Selling" });
            List.Add(new SelectListItem { Value = "Average", Text = "Average" });
            return List;
        }

        public static List<SelectListItem> getPlant(string pCompanyCode)
        {
            List<SelectListItem> lstPlant = new List<SelectListItem>();

            try
            {
                using (EntityCPAIEngine context = new EntityCPAIEngine())
                {
                    string sListComCode = pCompanyCode.Replace(",", "\",\"");
                    sListComCode = "\"" + sListComCode + "\"";
                    var tmpPlant = context.MT_PLANT.Where(z => z.MPL_STATUS == "ACTIVE" && (sListComCode.Contains(z.MPL_COMPANY_CODE) || String.IsNullOrEmpty(pCompanyCode))).OrderBy(o => o.MPL_PLANT);

                    foreach (var item in tmpPlant.ToList())
                    {
                        lstPlant.Add(new SelectListItem { Value = item.MPL_PLANT, Text = item.MPL_PLANT });
                    }

                }

            }
            catch (Exception ex)
            {
                lstPlant.Add(new SelectListItem { Value = "", Text = "" });
            }

            return lstPlant;
        }

        public static List<SelectListItem> getCustomer(string type = "THROUGHPUT", bool isOptional = false, string message = "")
        {
            string system = "PCF";
            List<MT_CUST_DETAIL> mt_cust_detail = CustDetailDAL.GetCustDetail(system, ACTIVE, type);
            return insertSelectListValue(mt_cust_detail.Select(x => x.MCD_NAME_1).ToList(), mt_cust_detail.Select(x => x.MCD_FK_CUS).ToList(), isOptional, message);
        }

        public static List<SelectListItem> getMaterial(bool isOptional = false, string message = "", string type = "CPAI")
        {
            List<MT_MATERIALS> mt_materials = MaterialsDAL.GetMaterials(type, ACTIVE).OrderBy(x => x.MET_MAT_DES_ENGLISH).ToList();
            return insertSelectListValue(mt_materials.Select(x => x.MET_MAT_DES_ENGLISH).ToList(), mt_materials.Select(x => x.MET_NUM).ToList(), isOptional, message);
        }
        #endregion

        private static List<SelectListItem> insertSelectListValue(List<string> text, List<string> value, bool isOptional = false, string message = "Please select")
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (isOptional)
            {
                if (!string.IsNullOrEmpty(message))
                {
                    selectList.Add(new SelectListItem { Text = message, Value = "" });
                }
            }
            for (int i = 0; i < text.Count; i++)
            {
                selectList.Add(new SelectListItem { Text = text[i], Value = value[i] });
            }
            return selectList;
        }

        public static string GetDataToJSON_TextValue(List<SelectListItem> dataForJson)
        {
            string json = "";
            try
            {
                List<SelectListItem> data = dataForJson;
                var query = (from v in data
                             select new { value = v.Value, text = v.Text });

                if (query != null)
                {
                    var qDis = query.Distinct().ToArray();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    //json = js.Serialize(qDis.OrderBy(p => p.value));

                    json = js.Serialize(qDis);
                }

                return json;
            }
            catch (Exception)
            {
                return json;
            }
        }

        public List<PCF_MT_STORAGELOCATION> getStorageLocation()
        {
            string strJSON = "{{ \"COMPANY_CODE\":\"\",\"CODE\":\"\",\"NAME\":\"\"}}";
            string JsonD = MasterData.GetJsonGlobalConfig("CIP_IMPORT_PLAN");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigStorageLocation dataList = (GlobalConfigStorageLocation)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigStorageLocation));

            return dataList.PCF_MT_STORAGE_LOCATION;
        }

        public static Decimal getTierValue(Decimal value, FormulaItemList conItem)
        {
            Decimal returnVal = 0;
            try
            {
                bool getResult = false;
                foreach (FormulaTier i in conItem.FormulaTier)
                {
                    if (i.Compare_with.Count > 0 && getResult == false)
                    {
                        Decimal vCompare = value;

                        //ตรวจสอบตามเงื่อนไข
                        foreach (TierCondition j in i.Compare_with)
                        {
                            switch (j.Compare_Symbol.Trim())
                            {
                                case "<":
                                    if (vCompare < Decimal.Parse(j.Compare_Value.Trim())) { getResult = true; }
                                    break;
                                case "<=":
                                    if (vCompare <= Decimal.Parse(j.Compare_Value.Trim())) { getResult = true; }
                                    break;
                                case ">":
                                    if (vCompare > Decimal.Parse(j.Compare_Value.Trim())) { getResult = true; }
                                    break;
                                case ">=":
                                    if (vCompare >= Decimal.Parse(j.Compare_Value.Trim())) { getResult = true; }
                                    break;
                                default:

                                    break;
                            }
                        }

                        //กรณีเข้าเงื่อนไข
                        if (getResult)
                        {
                            return Decimal.Parse(i.Return_Value);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                returnVal = 0;
                throw ex;
            }
            return returnVal;
        }

        public static Decimal getVariableValue(string valName, THROUGHPUT_FEE_CAL_ViewModel conItem)
        {
            Decimal rtnVal = 0;
            switch (valName)
            {
                case "input_Volume_1_1": rtnVal = conItem.input_Volume_1_1; break;
                case "input_Exchange_BuyingRate": rtnVal = conItem.input_Exchange_BuyingRate; break;
                case "input_Exchange_AverageRate": rtnVal = conItem.input_Exchange_AverageRate; break; 
                case "FixRate1_2": rtnVal = conItem.FixRate1_2; break;
                case "ServiceCharge1_3": rtnVal = conItem.ServiceCharge1_3; break;
                case "BuyingRate1_4": rtnVal = conItem.BuyingRate1_4; break;
                case "ProServiceFeeBase1_5_3": rtnVal = conItem.ProServiceFeeBase1_5_3; break;
                case "ProServiceFeeRate1_5_5": rtnVal = conItem.ProServiceFeeRate1_5_5; break;
                case "FinalServiceFeeRate1_5_6": rtnVal = conItem.FinalServiceFeeRate1_5_6; break;
                case "ServiceFee2_3": rtnVal = conItem.ServiceFee2_3; break;
                case "Vat2_4": rtnVal = conItem.Vat2_4; break;
                case "Total2_5": rtnVal = conItem.Total2_5; break;
                case "ServiceFeeRate3_3": rtnVal = conItem.ServiceFeeRate3_3; break;
                case "ServiceFeeContactAgeement3_4": rtnVal = conItem.ServiceFeeContactAgeement3_4; break;
                case "ServiceFeeChargedInTax3_5": rtnVal = conItem.ServiceFeeContactAgeement3_4; break;
                case "DebitNote3_6": rtnVal = conItem.DebitNote3_6; break;
                default: rtnVal = Decimal.Parse(valName); break;
            }

            return rtnVal;
        }

        public static THROUGHPUT_FEE_CAL_ViewModel setVariableValue(string valName, Decimal value, THROUGHPUT_FEE_CAL_ViewModel conItem)
        {
            Decimal rtnVal = value;
            switch (valName)
            {
                case "ServiceCharge1_3": conItem.ServiceCharge1_3 = rtnVal; break;
                case "BuyingRate1_4": conItem.BuyingRate1_4 = rtnVal; break;
                case "ProServiceFeeBase1_5_3": conItem.ProServiceFeeBase1_5_3 = rtnVal; break;
                case "ProServiceFeeRate1_5_5": conItem.ProServiceFeeRate1_5_5 = rtnVal; break;
                case "FinalServiceFeeRate1_5_6": conItem.FinalServiceFeeRate1_5_6 = rtnVal; break;
                case "ServiceFee2_3": conItem.ServiceFee2_3 = rtnVal; break;
                case "Vat2_4": conItem.Vat2_4 = rtnVal; break;
                case "Total2_5": conItem.Total2_5 = rtnVal; break;
                case "ServiceFeeRate3_3": conItem.ServiceFeeRate3_3 = rtnVal; break;
                case "ServiceFeeContactAgeement3_4": conItem.ServiceFeeContactAgeement3_4 = rtnVal; break;
                case "ServiceFeeChargedInTax3_5": conItem.ServiceFeeContactAgeement3_4 = rtnVal; break;
                case "DebitNote3_6": conItem.DebitNote3_6 = rtnVal; break;
                default: break;
            }

            return conItem;
        }

        public static Int32 stringToInt(string value)
        {
            Int32 temp_value = 0;
            Int32 number;
            if (Int32.TryParse(value, out number))
            {
                temp_value = number;
            }
            return temp_value;
        }

        public static Decimal symbolCalculate(Decimal input1, string symbol, Decimal input2)
        {
            Decimal returnVal = 0;
            try
            {
                switch (symbol.Trim().ToUpper())
                {
                    case "+":
                        returnVal = input1 + input2;
                        break;
                    case "-":
                        returnVal = input1 - input2;
                        break;
                    case "*":
                        returnVal = input1 * input2;
                        break;
                    case "/":
                        returnVal = input1 / input2;
                        break;
                    case "MIN":
                        if (input1 > input2)
                        {
                            returnVal = input2;
                        }
                        else
                        {
                            returnVal = input1;
                        }
                        break;
                    case "MAX":
                        if (input1 > input2)
                        {
                            returnVal = input1;
                        }
                        else
                        {
                            returnVal = input2;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                returnVal = 0;
                throw ex;
            }
            return returnVal;
        }

        public THROUGHPUT_FEE_CAL_ViewModel calThroughPut(THROUGHPUT_FEE_CAL_ViewModel temp)
        {
            string JsonD = MasterData.GetJsonMasterSetting("THROUGHT_PUT_FEE");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? "" : JsonD);
            FormulaItemList dataList = (FormulaItemList)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(FormulaItemList));
            temp.FixRate1_2 = getTierValue(temp.input_Volume_1_1, dataList);
            temp.BuyingRate1_4 = temp.input_Exchange_BuyingRate;
            if (dataList.ConditionItem != null)
            {
                if (dataList.ConditionItem.Count > 0)
                {
                    foreach (var i in dataList.ConditionItem)
                    {
                        if (i.Symbol != "")
                        {
                            var val1 = getVariableValue(i.Variable1, temp);
                            if (!string.IsNullOrEmpty(i.Round1))
                            {
                                if (i.Round1.Trim() != "")
                                {
                                    val1 = (Decimal)Math.Round((decimal)val1, stringToInt(i.Round1), MidpointRounding.AwayFromZero);
                                }
                            }
                            var val2 = getVariableValue(i.Variable2, temp);

                            if (!string.IsNullOrEmpty(i.Round2))
                            {
                                if (i.Round2.Trim() != "")
                                {
                                    val2 = (Decimal)Math.Round((decimal)val2, stringToInt(i.Round2), MidpointRounding.AwayFromZero);
                                }
                            }

                            Decimal valTemp = 0;
                            if (!string.IsNullOrEmpty(i.Symbol) && val2 != 0)
                            {
                                valTemp = symbolCalculate(val1, i.Symbol, val2);
                            }
                            else
                            {
                                valTemp = val1;
                            }
                            //Rounding ผลลัพธ์
                            if (!string.IsNullOrEmpty(i.ResultOut_Round))
                            {
                                if (i.ResultOut_Round.Trim() != "")
                                {
                                    valTemp = (Decimal)Math.Round((decimal)valTemp, stringToInt(i.ResultOut_Round), MidpointRounding.AwayFromZero);
                                }
                            }

                            temp = setVariableValue(i.ResultOut, valTemp, temp);

                        }
                        else
                        {
                            var val1 = getVariableValue(i.Variable1, temp);
                            if (!string.IsNullOrEmpty(i.Round1))
                            {
                                if (i.Round1.Trim() != "")
                                {
                                    val1 = (Decimal)Math.Round((decimal)val1, stringToInt(i.Round1), MidpointRounding.AwayFromZero);
                                }
                            }

                            Decimal valTemp = 0;
                           
                                valTemp = val1;
                            
                            //Rounding ผลลัพธ์
                            if (!string.IsNullOrEmpty(i.ResultOut_Round))
                            {
                                if (i.ResultOut_Round.Trim() != "")
                                {
                                    valTemp = (Decimal)Math.Round((decimal)valTemp, stringToInt(i.ResultOut_Round), MidpointRounding.AwayFromZero);
                                }
                            }

                            temp = setVariableValue(i.ResultOut, valTemp, temp);
                        }
                    }

                }
            }


            return temp;
        }

        public string GetExchangeRateSell(string Date)
        {
            string[] tempDate = Date.Split(' ');
            decimal a_SELLING = 0;
            string v_SELLING = "";
            string selling = "";
            int count = 0;
            string sDateTo = DateTime.ParseExact(tempDate[0], format, provider).ToString("yyyyMMdd", provider);
            string sDateFrom = DateTime.ParseExact(tempDate[2], format, provider).ToString("yyyyMMdd", provider);
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var tempFx = (from v in context.MKT_TRN_FX_B
                              where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                              select v).OrderByDescending(p => p.T_FXB_VALDATE);

                var queryFx = from d in tempFx.AsEnumerable()
                              where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo)
                              && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom)
                              select d;

                var list_SELLING = queryFx.Where(s => (Int32.Parse(s.T_FXB_VALDATE) == Int32.Parse(sDateFrom)
                                            && s.T_FXB_VALTYPE == "M" && s.T_FXB_ENABLED == "T")
                                            && s.T_FXB_CUR == "USD");
                if (list_SELLING.Count() > 0)
                {
                    v_SELLING = list_SELLING.FirstOrDefault().T_FXB_VALUE1.ToString();
                    a_SELLING += Convert.ToDecimal(v_SELLING);
                    count++;
                }
                if(a_SELLING != 0)
                {
                    string[] selling_t = Math.Round((a_SELLING / count), 4, MidpointRounding.AwayFromZero).ToString().Split('.');//ToString("0.####")
                    if (selling_t.Length == 2)
                    {
                        selling = selling_t[0] + "." + selling_t[1].PadRight(4, '0');
                    }
                    else
                    {
                        selling = selling_t[0] + ".0000";
                    }
                }
                else
                {
                    selling = "0.0000";
                }

            }
            return selling;
        }

        public string GetExchangeRateBuy(string Date)
        {
            string[] tempDate = Date.Split(' ');
            decimal a_BUYING = 0;
            string v_BUYING = "";
            string buying = "";
            int count = 0;
            string sDateTo = DateTime.ParseExact(tempDate[0], format, provider).ToString("yyyyMMdd", provider);
            string sDateFrom = DateTime.ParseExact(tempDate[2], format, provider).ToString("yyyyMMdd", provider);
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var tempFx = (from v in context.MKT_TRN_FX_B
                              where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                              select v).OrderByDescending(p => p.T_FXB_VALDATE);

                var queryFx = from d in tempFx.AsEnumerable()
                              where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo)
                              && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom)
                              select d;

                var list_BUYING = queryFx.Where(s => (Int32.Parse(s.T_FXB_VALDATE) == Int32.Parse(sDateFrom)
                                            && s.T_FXB_VALTYPE == "B" && s.T_FXB_ENABLED == "T")
                                            && s.T_FXB_CUR == "USD");

                if (list_BUYING.Count() > 0)
                {
                    v_BUYING = list_BUYING.FirstOrDefault().T_FXB_VALUE1.ToString();
                    a_BUYING += Convert.ToDecimal(v_BUYING);
                    count++;
                }
                if(a_BUYING != 0)
                {
                    string[] buying_t = Math.Round((a_BUYING / count), 4, MidpointRounding.AwayFromZero).ToString().Split('.');//ToString("0.####")
                    if (buying_t.Length == 2)
                    {
                        buying = buying_t[0] + "." + buying_t[1].PadRight(4, '0');
                    }
                    else
                    {
                        buying = buying_t[0] + ".0000";
                    }
                }
                else
                {
                    buying = "0.0000";
                }
            }
            return buying;
        }

        public string GetExchangeRateAverage(string Date)
        {
            string[] tempDate = Date.Split(' ');
            
            string sDateTo = DateTime.ParseExact(tempDate[0], format, provider).ToString("yyyyMMdd", provider);
            string sDateFrom = DateTime.ParseExact(tempDate[2], format, provider).ToString("yyyyMMdd", provider);
            string ROE = "0";
            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var tempFx = (from v in context.MKT_TRN_FX_B
                              where v.T_FXB_ENABLED == "T" && v.T_FXB_CUR == "USD"
                              select v).OrderByDescending(p => p.T_FXB_VALDATE);

                var queryFx = from d in tempFx.AsEnumerable()
                              where Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) <= Int32.Parse(sDateTo)
                              && Convert.ToInt32(string.IsNullOrEmpty(d.T_FXB_VALDATE) ? "0" : d.T_FXB_VALDATE) >= Int32.Parse(sDateFrom)
                              select d;
                if(queryFx.Count() != 0)
                {
                    ROE = queryFx.Select(h => h.T_FXB_VALUE1).LastOrDefault().ToString();
                }
                else
                {
                    ROE = "0.0000";
                }
            }
            return ROE;
        }
    }
}