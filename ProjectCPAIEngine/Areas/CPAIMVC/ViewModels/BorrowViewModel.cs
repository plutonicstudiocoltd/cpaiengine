﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class BorrowViewModel
    {
        //public SOBorrowViewModel_Search SOBorrow_Search { get; set; }
        public BorrowViewModel_Search Borrow_Search { get; set; }
        public BorrowDetailViewModel pBorrowDetail { get; set; }
        public BorrowDetailViewModel sBorrowDetail { get; set; } //fix
        public string TripNoM { get; set; }
        public List<SelectListItem> ddl_Crude { get; set; }
        public string json_Crude { get; set; }
        public List<SelectListItem> ddl_Customer { get; set; }
        public string json_Customer { get; set; }
        public List<SelectListItem> ddl_Plant { get; set; }
        public string json_Plant { get; set; }
        public List<SelectListItem> ddl_SaleOrg { get; set; }
        public string json_SaleOrg { get; set; }
        public List<SelectListItem> ddl_Unit { get; set; }
        public string json_Unit { get; set; }
        public List<SelectListItem> ddl_UnitPrice { get; set; }
        public string json_UnitPrice { get; set; }
        public List<SelectListItem> ddl_UnitINV { get; set; }
        public string json_UnitINV { get; set; }
        public List<SelectListItem> ddl_UnitTotal { get; set; }
        public string json_UnitTotal { get; set; }
        public List<SelectListItem> ddl_PriceStatus { get; set; }
        public string json_PriceStatus { get; set; }
        public List<SelectListItem> ddl_Vender { get; set; }
        public string json_Vendor { get; set; }
        //public string selectedTripNo { get; set; }


    }

    public class BorrowDetailViewModel
    {
        public string dPlant { get; set; }
        public string dSaleOrg { get; set; }
        public string dTripNo { get; set; }
        public string dCustomer { get; set; }
        public string dDeliveryDate { get; set; }
        public string dDueDate { get; set; }
        public string dCalcuUnit { get; set; }
        public string dExchangeRate { get; set; }
        public string dCrude { get; set; }
        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }
        public string dVolumeLitres { get; set; }
        public string dPONo { get; set; }
        public string dTank { get; set; }
        public string dRemark { get; set; }
        public List<BorrowPriceViewModel> BorrowPriceList { get; set; }
        public string dTotalAmount { get; set; }
        public string dTotalUnit { get; set; }
        public string dnum { get; set; }
        public string dPricePer { get; set; }
        public string dPrice { get; set; }
        public string dUnitID { get; set; }
        public string dType { get; set; }
        public string dStatus { get; set; }
        public string dRelease { get; set; }
        public string dSaleOrder { get; set; }
        public string dPriceRelease { get; set; }
        public string dDistriChann { get; set; }
        public string dTableName { get; set; }
        public string dMemo { get; set; }
        public string dDateSAP { get; set; }
        public string dFIDoc { get; set; }
        public string dDONo { get; set; }
        public string dStatusToSAP { get; set; }
        public string dFIDocReverse { get; set; }
        public string dIncoTermType { get; set; }
        public string dTemp { get; set; }
        public string dDensity { get; set; }
        public string dStorageLocation { get; set; }
        public string dUpdateDO { get; set; }
        public string dUpdateGI { get; set; }
        public string dCreateDate { get; set; }
        public string dCreateBy { get; set; }
        public string dLastModifyDate { get; set; }
        public string dLastModifyBy { get; set; }
        public string dCreateTime { get; set; }
        public string dLastModifyTime { get; set; }
        public string dPriceStatus { get; set; }
        public string dSAPPONo { get; set; }
    }

    

    public class BorrowPriceViewModel
    {
        ///PURCHASE
        public string sPriceType { get; set; }  // P=Provisional , A=Actual
        public string sDueDate { get; set; }
        public string sPrice { get; set; }
        public string sPriceUnit { get; set; }
        public string sInvoice { get; set; }
        public string sInvUnit { get; set; }
        public string sTripNo { get; set; }
        public string sNum { get; set; }
        public string sRelease { get; set; }
        public string sMemo { get; set; }
        public decimal sumInvoice { get; set; }
       
    }

    public class BorrowViewModel_Search
    {
        public string sTripNo { get; set; }
        public string sDeliveryDate { get; set; }
        public string sCustomer { get; set; }
        public string sCrude { get; set; }
        public string sTripNo_Select { get; set; }
        public List<BorrowViewModel_SearchData> SearchData { get; set; }

        public string sDocument { get; set; }
        public string sComCode { get; set; }
        public string sYear { get; set; }



    }

    public class BorrowViewModel_SearchData
    {
        public string aTripNo { get; set; }
        public string aCustomer { get; set; }
        public string aDeliveryDate { get; set; }
        public string aSaleOrg { get; set; }
        public string aPlant { get; set; }
        public string aCrudeType { get; set; }
        public string aVolumeBBL { get; set; }
        public string aVolumeMT { get; set; }
        public string aVolumeLitres { get; set; }
        public string aFX { get; set; }      
        public string aPrice { get; set; }        
        public string aSaleOrder { get; set; }
        public string aPurchaseOrder { get; set; }
        //use in SO create
        public string aUnit { get; set; }
        public string aMemoNo { get; set; }
        public string aDONo { get; set; }
        public string aSaleUnitType { get; set; }
        public string aInvoiceFigureType { get; set; }

        //search reverse FI doc for Borow Crude
        public string aPrice_Num { get; set; }
        public string aInvoice { get; set; }
        public string aCurrency_invoice { get; set; }


    }


    public class GlobalConfigBorrow
    {
        public List<UnitCalBW> PCF_UNIT_BW { get; set; }
    }

    public class UnitCalBW
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}