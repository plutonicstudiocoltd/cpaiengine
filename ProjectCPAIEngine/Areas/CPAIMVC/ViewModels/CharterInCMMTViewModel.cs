﻿using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CharterInCMMTViewModel
    {
        public ChitEvaluating chit_evaluating { get; set; }
        public ChitCharteringMethod chit_chartering_method { get; set; }
        public ChitNegotiationSummary chit_negotiation_summary { get; set; }
        public ChitProposedForApprove chit_proposed_for_approve { get; set; }
        public string chit_reason { get; set; }
        public string chit_note { get; set; }

        public string laycan_full { get; set; }
        public string loading_month_full { get; set; }
        public List<SelectListItem> freight_type_list { get; set; }
        public List<SelectListItem> product_list { get; set; }
        public List<SelectListItem> broker_list { get; set; }
        public List<SelectListItem> vessel_list { get; set; }
        public List<SelectListItem> pricesTerm_list { get; set; }
        public List<SelectListItem> quantity_list { get; set; }
        public List<SelectListItem> type_list { get; set; }
    }
}