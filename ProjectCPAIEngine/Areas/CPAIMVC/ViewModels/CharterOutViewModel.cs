﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CharterOutViewModel
    {
        public ChosEvaluating chos_evaluating { get; set; }
        public ChosCharteringMethod chos_chartering_method { get; set; }
        public ChosNoCharterOutCaseA chos_no_charter_out_case_a { get; set; }
        public ChosCharterOutCaseB chos_charter_out_case_b { get; set; }
        public ChosProposedForApprove chos_proposed_for_approve { get; set; }
        public ChosBenefit chos_benefit { get; set; }
        public string chos_reason { get; set; } = "";
        public string chos_note { get; set; } = "";
        public List<ApproveItem> approve_items { get; set; }
        public string explanationAttach { get; set; }

        public ChotEvaluating chot_evaluating { get; set; }
        public ChotDetail chot_detail { get; set; }
        public ChotProposedForApprove chot_proposed_for_approve { get; set; }
        public ChotCharteringMethod chot_chartering_method { get; set; }
        public ChotNegotiationSummary chot_negotiation_summary { get; set; }
        public string chot_reason_explanation { get; set; }
        public string chot_reason { get; set; }
        public string chot_note { get; set; }

        public string chot_modal_date { get; set; }
        public string chot_modal_vessel { get; set; }
        public string chot_modal_charterer { get; set; }
        public string chot_modal_laycan { get; set; }
        public string chot_modal_laytime { get; set; }
        public string chot_modal_cargo { get; set; }

        public string chot_vessel { get; set; }
        public string chot_fcl_no { get; set; }
        public string chot_date { get; set; }
        public string chot_reason_new { get; set; }
        public ChotNoCharterOutCaseA chot_no_charter_out_case_a { get; set; }
        public ChotCharterOutCaseB chot_charter_out_case_b { get; set; }
        public ChotSummary chot_summary { get; set; }
        public ChotResult chot_result { get; set; }
        public string chot_note_new { get; set; }

        public List<SelectListItem> vessel { get; set; }
        public List<SelectListItem> pricesTerm { get; set; }
        public List<SelectListItem> freight { get; set; }
        public List<SelectListItem> customer { get; set; }
        public string ev_tempLaycan { get; set; }
        public string pa_tempLaycan { get; set; }
        public List<VesselSize> vesselSize { get; set; }

        public List<SelectListItem> product_list { get; set; }
        public List<SelectListItem> vessel_list { get; set; }
        public List<SelectListItem> broker_list { get; set; }
        public List<SelectListItem> charterer_broker_list { get; set; }        
        public List<SelectListItem> owner_broker_list { get; set; }
        public List<SelectListItem> bss_list { get; set; }
        public List<SelectListItem> quantity_list { get; set; }
        public List<SelectListItem> loadport_list { get; set; }
        public List<SelectListItem> dischargeport_list { get; set; }
        public List<SelectListItem> freight_list { get; set; }
        public List<SelectListItem> year_list { get; set; }
        public List<SelectListItem> deduct_action_list { get; set; }

        public List<SelectListItem> charterer_list { get; set; }
        public List<SelectListItem> cargo_list { get; set; }
        public List<SelectListItem> port_load_list { get; set; }
        public List<SelectListItem> port_dis_list { get; set; }

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string pur_no { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }

        public string vessel_id { get; set; }
        public string vessel_name { get; set; }
        public string freight_id { get; set; }
        public string freight_no { get; set; }
    }
    
    public class ChotData
    {

    }

    public class ChotFreightModal
    {
        public string chot_fcl_row_id { get; set; }
        public string chot_fcl_date { get; set; }
        public string chot_fcl_no { get; set; }
        public string chot_fcl_vessel { get; set; }
        public string chot_fcl_laycan { get; set; }
        public string chot_fcl_charterer { get; set; }
        public string chot_fcl_create_by { get; set; }
        public string chot_fcl_update_date { get; set; }
    }
}