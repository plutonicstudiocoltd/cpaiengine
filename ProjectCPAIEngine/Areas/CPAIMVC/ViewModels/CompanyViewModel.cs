﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CompanyViewModel
    {

        public CompanyViewModel_Seach cmp_Search { get; set; }
        public CompanyViewModel_Detail cmp_Detail { get; set; }

        public List<SelectListItem> ddl_CompanyCode { get; set; }
        public List<SelectListItem> ddl_ShortName { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_CreateType { get; set; }

        public string companyCodeJSON { get; set; }
        public string companyNameJSON { get; set; }
    }

    public class CompanyViewModel_Seach
    {
        public string sCompanyCode { get; set; }
        public string sCompanyName { get; set; }
        public string sStatus { get; set; }
        public string sCreatedType { get; set; }
        public string sDateFrom { get; set; }
        public List<CompanyViewModel_SeachData> sSearchData { get; set; }
    }

    public class CompanyViewModel_SeachData
    {
        public string dCompanyCode { get; set; }
        public string dCompanyName { get; set; }
        public string dStatus { get; set; }
        public string dCreatedBy { get; set; }
        public string dCreatedDate { get; set; }
        public string dUpdatedBy { get; set; }
        public string dUpdatedDate { get; set; }
        public string dCreatedType { get; set; }

    }

    public class CompanyViewModel_Detail
    {
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public bool BoolStatus { get; set; }
        public string CreateType { get; set; }

        public string fullName { get; set; }
        public string description { get; set; }
        public string accountPay { get; set; }
        public string accountReceive { get; set; }
        public List<CompanyViewModel_Auth> Company_auth { get; set; }

    }

    public class CompanyViewModel_Auth
    {
        public string RowID { get; set; }
        public string Order { get; set; }
        public string Trading_book { get; set; }
        public bool Trading_for_flag { get; set; }
        public string Fee_bbl { get; set; }
        public string Fee_mt { get; set; }
    }
    //public class CompanyViewModel_Control
    //{
    //    public string RowID { get; set; }
    //    public string Order { get; set; }
    //    public string System { get; set; }
    //    public string Type { get; set; }
    //    public string Color { get; set; }
    //    public string Status { get; set; }
    //}
}