﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class CoolTrackingViewModel
    {
        public List<CoolTrackingEncrypt> CoolTrackingTransaction { get; set; }
        public List<USERS> createdByUser { get; set; }
        public List<string> areaList { get; set; }
        public List<string> unitList { get; set; }
        public List<SelectListItem> country { get; set; }
        public List<SelectListItem> crudeCategories { get; set; }
        public List<SelectListItem> crudeKerogen { get; set; }
        public List<SelectListItem> crudeCharacteristic { get; set; }
        public List<SelectListItem> crudeMaturity { get; set; }
        public List<SelectListItem> area { get; set; }
        public List<SelectListItem> unit { get; set; }
        public List<SelectListItem> crude { get; set; }

        public List<string> crudeList { get; set; }
        public List<string> countryList { get; set; }

        public string sPurchaseNo { get; set; }
        public string sAssayRef { get; set; }
        public string sCountry { get; set; }
        public string sCrudeName { get; set; }
        public string sCrudeCategories { get; set; }
        public string sCrudeKerogen { get; set; }
        public string sCrudeCharacteristic { get; set; }
        public string sCrudeMaturity { get; set; }
        public string sArea { get; set; }
        public string sUnit { get; set; }
        

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string purno { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
    }
}