﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class DisChannelViewModel
    {
        public DisChannelViewModel_Search disChannel_Search { get; set; }
        public DisChannelViewModel_SearchData disChannel_SearchData { get; set; }
        public DisChannelViewModel_Detail disChannel_Detail { get; set; }

        public List<SelectListItem> ddl_Status { get; set; }
        public string disChannelIDJSON { get; set; }
        public string disChannelZoneJSON { get; set; }
    }
    public class DisChannelViewModel_Search
    {
        public string sMDCDC { get; set; }
        public string sMDCZone { get; set; }
        public string sMDCStatus { get; set; }
        public string sMDCDateFromTo { get; set; }

        public List<DisChannelViewModel_SearchData> sSearchData { get; set; }
    }

    public class DisChannelViewModel_SearchData
    {
        public string dMDCDC { get; set; }
        public string dMDCZone { get; set; }
        public string dDescription { get; set; }
        public string dMDCStatus { get; set; }
        public string dMDCCreatedDate { get; set; }
        public string dMDCCreatedBy { get; set; }
    }

    public class DisChannelViewModel_Detail
    {
        public string MDCDC { get; set; }
        public string MDCZone { get; set; }
        public string MDCDescription { get; set; }
        public string MDCStatus { get; set; }
        public string MDCCreatedDate { get; set; }
        public bool boolStatus { get; set; }
    }

}