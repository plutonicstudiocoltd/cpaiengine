﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class DocumentManagemantViewModel
    {
        public string crudeName { get; set; }
        public string country { get; set; }
        public string filePath { get; set; }
        public List<SelectListItem> ddlCrudeName { get; set; }
        public List<SelectListItem> ddlContry { get; set; }
        public List<SelectListItem> ddlFileType { get; set; }
    }

    public class SearchDocumentManagemantViewModel
    {
        public string d_crude_name { get; set; }
        public string s_crude_name { get; set; }
        public string cpu_name { get; set; }
        public string file_name { get; set; }
        public string file_type { get; set; }
        public string d_country { get; set; }
        public string s_country { get; set; }
        public string CPU_Path { get; set; }
        public List<Abbreviation> abbr_name { get; set; }
        public List<SupportingDocument_SearchData> search_data { get; set; }
        public List<Document_SearchData> search_doc { get; set; }
        public List<SelectListItem> ddlFileSupportingDocumentCountry { get; set; }
        public List<SelectListItem> ddlCrudeName { get; set; }
    }

    public class Abbreviation
    {
        public string unit { get; set; }
        public string crude { get; set; }
        public string abbreviation { get; set; }
    }

    public class SupportingDocument_SearchData
    {
        public string s_crude_name { get; set; }
        public string s_country { get; set; }
    }

    public class Document_SearchData
    {
        public string d_crude_name { get; set; }
        public string d_country { get; set; }
    }

    public class SupportingDocumentViewModel
    {
        public string crude_name { get; set; }
        public string country { get; set; }
        public string file_path { get; set; }
        public string tranID { get; set; }
        public List<SelectListItem> ddlFileSupportingDocumentType { get; set; }
    }

    public class DocumentManagemantViewDetailModel
    {
        public string crudeName { get; set; }
        public string country { get; set; }
        public string filePath { get; set; }
        public List<SelectListItem> ddlFileType { get; set; }
    }

    public class CPUViewModel
    {
        public string cdu1 { get; set; }
        public string cdu2 { get; set; }
        public string cdu3 { get; set; }
        public string processingDate_cdu1 { get; set; }
        public string processingDate_cdu2 { get; set; }
        public string processingDate_cdu3 { get; set; }

    }
}