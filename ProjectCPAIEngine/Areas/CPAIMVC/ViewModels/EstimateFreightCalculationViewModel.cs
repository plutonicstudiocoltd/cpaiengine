﻿using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class EstimateFreightCalculationViewModel
    {
        public string fcl_excel_file_name { get; set; } = "";
        public FclVesselParticular fcl_vessel_particular { get; set; } = new FclVesselParticular();
        public FclPaymentTerms fcl_payment_terms { get; set; } = new FclPaymentTerms();
        public FclTimeForOperation fcl_time_for_operation { get; set; } = new FclTimeForOperation();
        public string total_port_charge { get; set; } = "";
        public string total_bunker_cost { get; set; } = "";
        public FclVesselSpeed fcl_vessel_speed { get; set; } = new FclVesselSpeed();
        public string fcl_exchange_rate { get; set; } = "";
        public string fcl_exchange_date { get; set; } = "";
        public FclLatestBunkerPrice fcl_latest_bunker_price { get; set; } = new FclLatestBunkerPrice();
        public FclBunkerConsumption fcl_bunker_consumption { get; set; } = new FclBunkerConsumption();
        public string fcl_pic_path { get; set; } = "";
        public string fcl_remark { get; set; } = "";
        public List<FclBunkerCost> fcl_bunker_cost { get; set; }
        public FclOtherCost fcl_other_cost { get; set; } = new FclOtherCost();
        public FclResults fcl_results { get; set; } = new FclResults();
        public string explanationAttach { get; set; }

        public List<SelectListItem> broker { get; set; }
        public List<SelectListItem> vehicle { get; set; }
        public List<SelectListItem> portName { get; set; }
        public List<SelectListItem> portLocation { get; set; }
        public List<SelectListItem> portCountry { get; set; }
        public List<ListDrowDownKey> portTypeLoad { get; set; }
        public List<ListDrowDownKey> portTypeDisCharge { get; set; }
        public List<MT_JETTY> portFullData { get; set; }
        public List<SelectListItem> customer { get; set; }
        public List<SelectListItem> cargo { get; set; }
        public List<SelectListItem> status { get; set; }
        public List<SelectListItem> custPaymentTerms { get; set; }
        public List<SelectListItem> type { get; set; }
        public List<SelectListItem> built { get; set; }
        public List<SelectListItem> bss { get; set; }
        public List<SelectListItem> unit { get; set; }
        public List<SelectListItem> unit_offer { get; set; }
        public List<SelectListItem> by_offer { get; set; }
        public List<SelectListItem> unit_demurrage { get; set; }
        public List<USERS> createByUser { get; set; }
        public List<string> userList { get; set; }
        public string date { get; set; }
        public string laycan { get; set; }
        public List<FreightTransaction> FreightTransaction { get; set; }
        public string loadPort;
        public string dischargePort;
        public string vessel_id { get; set; }
        public string mode { get; set; } = "";

        public string taskID { get; set; }
        public string reqID { get; set; }
        public string pur_no { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public string buttonEvent { get; set; }

        public string search_date { get; set; }
        public string search_laycan { get; set; }
        public string search_laytime { get; set; }
        public string search_vessel { get; set; }
        public string search_cargo { get; set; }
        public string search_customer { get; set; }
        public string search_status { get; set; }

        
        public string FreightOfferTHB { get; set; }
        public string FreightOfferUSD { get; set; }
        public string FreightOfferTHBprice { get; set; }
        public string FreightOfferUSDprice { get; set; }
    }
}