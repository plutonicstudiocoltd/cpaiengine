﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class FeedstockBookingPriceViewModel
    {
        public FeedStockBookingPriceSearch data_Search { get; set; }
        public FeedStockBookingPriceDetail data_Detail { get; set; }
        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_Supplier { get; set; }
        public List<SelectListItem> ddl_Feedstock { get; set; }
        public List<SelectListItem> ddl_Invoice { get; set; }
        //public List<SelectListItem> ddl_CreateType { get; set; }
        public List<SelectListItem> quatityType { get; set; }

        public List<EXCHANGE_RATE> exchangeRate { get; set; }
        public List<PRODUCT_CODE> productCode { get; set; }
        public CONSTANT_PRODUCT_PRICE consProductPrice { get; set; }
        public CONSTANT_PRODUCT_BASE_DETAIL consProductDetail { get; set; }

        //public string interComCodeJSON { get; set; }
        public string companyNameJSON { get; set; }
        public string supplierJSON { get; set; }
        public string feedStockJSON { get; set; }
    }

    [Serializable]
    public class CONFIG_INTERCOMPANY
    {
        public List<EXCHANGE_RATE> EXCHANGE_RATE { get; set; }
        public List<PRODUCT_CODE> PRODUCT_CODE { get; set; }
        public CONSTANT_PRODUCT_PRICE CONSTANT_PRODUCT_PRICE { get; set; }

        public CONSTANT_PRODUCT_BASE_DETAIL CONSTANT_PRODUCT_BASE_DETAIL { get; set; }

        public List<NaphTha> NaphTha { get; set; }
        public List<BlendedKerosene> BlendedKerosene { get; set; }
        public List<StripperOVHDLiquid> StripperOVHDLiquid { get; set; }
        public List<PEPAromatics> PEPAromatics { get; set; }
        public List<DrainHydrocarbons> DrainHydrocarbons { get; set; }
        public List<A9_MX_C9> A9_MX_C9 { get; set; }
        public List<C4_C7> C4_C7 { get; set; }
        public List<TOLUENE> TOLUENE { get; set; }
        public List<C10_HEAVIES> C10_HEAVIES { get; set; }
        public List<C5_RAFFINATE> C5_RAFFINATE { get; set; }
    }

    public class NaphTha
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }

    public class BlendedKerosene
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }

    public class StripperOVHDLiquid
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }

    public class PEPAromatics
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    public class DrainHydrocarbons
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    public class A9_MX_C9
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    public class C4_C7
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    public class TOLUENE
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    public class C10_HEAVIES
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }

    public class C5_RAFFINATE
    {
        public string CODE { get; set; }
        public string VALUE { get; set; }
    }
    

    public class PRODUCT_CODE
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
    }

    public class CONSTANT_PRODUCT_PRICE
    {
        public string ULG95 { get; set; }
        public string KERO { get; set; }
        public string GASOIL { get; set; }
    }

    public class CONSTANT_PRODUCT_BASE_DETAIL
    {
        public string TEMPERATURE_DEG_C { get; set; }
        public string MIN { get; set; }
        public string MAX { get; set; }
        public List<PRODUCT_DETAIL> PRODUCT_DETAIL { get; set; }
        public List<INVERSE_DATA> INVERSE_DATA { get; set; }

    }
    public class INVERSE_DATA
    {
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string RHS { get; set; }
        public string INV_X { get; set; }
        public string INV_Y { get; set; }
        public string INV_Z { get; set; }
    }
    public class PRODUCT_DETAIL
    {
        public string TYPE { get; set; }
        public string DEN15 { get; set; }
        public string VIS_CST { get; set; }
        public string TEMP_C { get; set; }
        public string SUL_W { get; set; }
        public string FV { get; set; }
        public string FREIGHT { get; set; }
    }

    public class EXCHANGE_RATE
    {
        public string CODE { get; set; }
        public string BUYING { get; set; }
        public string SELLING { get; set; }
        public string AVERAGE { get; set; }
    }
    /// <summary>
    /// Model for Search Detail
    /// </summary>
    /// 
    public class FeedStockBookingPriceSearch
    {
        public string vSearchPeriod { get; set; }
        public string vUpdatePeriod { get; set; }
        public string vChangeDate { get; set; }

        public bool vDen15_select { get; set; }
        public string vDen15 { get; set; }
        public bool vVisc_select { get; set; }
        public string vVisc { get; set; }
        public bool vSulfer_select { get; set; }
        public string vSulfer { get; set; }
        public bool vUSDOld_select { get; set; }
        public string vUSDOld { get; set; }
        public bool vExchangeOld_select { get; set; }
        public string vExchangeOld { get; set; }
        public bool vTHBOld_select { get; set; }
        public string vTHBOld { get; set; }
        public bool vInvoice_select { get; set; }
        public string vInvoice { get; set; }
        public bool vCopyFinalPrice { get; set; }

        public bool vAC_1ST_select { get; set; }
        public bool vAC_2SD_select { get; set; }

        public string vVolumnPEPAromatics { get; set; }
        public string vVolumnHAB { get; set; }
        public string vVolumnUnionFining { get; set; }
        public string vVolumnPacol { get; set; }

        public string vFocus { get; set; }
        public UpdatePriceDetail sPriceDetail { get; set; }
        public List<TrnMOBSearchDetail> sMobDetail { get; set; }
        public List<TrnFXSearchDetail> sFXDetail { get; set; }
        public string jsonExchageRate { get; set; }
        public CrudePhetProductDetail sCrudePhet { get; set; }
    }


    public class CrudePhetProductDetail
    {
        public List<SelectListItem> Daily  { get; set; }
        public List<SelectListItem> Monthly { get; set; }
        public List<CrudePhetItemList> Item { get; set; }
    }

    public class CrudePhetItemList
    {
        public string Date { get; set; }
        public string DailyCode1 { get; set; }
        public string dValue1 { get; set; }
        public string DailyCode2 { get; set; }
        public string dValue2 { get; set; }
        public string DailyCode3 { get; set; }
        public string dValue3 { get; set; }
        public string DailyCode4 { get; set; }
        public string dValue4 { get; set; }
        public string DailyCode5 { get; set; }
        public string dValue5 { get; set; }
        public string DailyCode6 { get; set; }
        public string dValue6 { get; set; }
        public string DailyCode7 { get; set; }
        public string dValue7 { get; set; }
        public string MonthlyCode1 { get; set; }
        public string mValue1 { get; set; }
        public string MonthlyCode2 { get; set; }
        public string mValue2 { get; set; }
        public string MonthlyCode3 { get; set; }
        public string mValue3 { get; set; }
        public string MonthlyCode4 { get; set; }
        public string mValue4 { get; set; }
        public string MonthlyCode5 { get; set; }
        public string mValue5 { get; set; }
        public string MonthlyCode6 { get; set; }
        public string mValue6 { get; set; }
        public string MonthlyCode7 { get; set; }
        public string mValue7 { get; set; }
    }

    public class UpdatePriceDetail
    {
        public string USD_ULG92 { get; set; }
        public string USD_ULG95 { get; set; }
        public string USD_KERO { get; set; }
        public string USD_GASOIL { get; set; }
        public string USD_LSFO { get; set; }
        public string USD_HSFO { get; set; }
        public string USD_H2 { get; set; }
        public string USD_LPG { get; set; }
        public string USD_PROPANE { get; set; }
        public string USD_BUTANE { get; set; }
        public string USD_NAPHTA { get; set; }

        public string USD_LABIX_KERO { get; set; }
        public string USD_LABIX_GASOIL { get; set; }
        public string USD_LABIX_FO380 { get; set; }
        public string USD_LABIX_PROPANE { get; set; }
        public string USD_LABIX_BUTANE { get; set; }
        public string USD_LABIX_LPG { get; set; }
        public string USD_LABIX_NAPHTA { get; set; }
        public string USD_LABIX_ULG92 { get; set; }

        public string USD_TPX_ULG95 { get; set; }
        public string USD_TPX_KERO { get; set; }
        public string USD_TPX_GASOIL { get; set; }
        public string USD_TPX_NAPHTA { get; set; }

        public string BBL_ULG95 { get; set; }//Constant Get from Global Config
        public string BBL_KERO { get; set; }//Constant Get from Global Config
        public string BBL_GASOIL { get; set; }//Constant Get from Global Config
        public string EXC_BUYING { get; set; }
        [DisplayFormat(DataFormatString = "{0:n1}", ApplyFormatInEditMode = true), Display(Name = "Commission")]
        public string EXC_SELLING { get; set; }
        [DisplayFormat(DataFormatString = "{0:n1}", ApplyFormatInEditMode = true), Display(Name = "Commission")]
        public string EXC_AVERAGE { get; set; }
        //Calculate Update Price
        public string Company { get; set; }
        public string Supplier { get; set; }
        public string FeedStock { get; set; }
        public string PONO { get; set; }
        //public List<SelectListItem> PONO_LIST { get; set; }
        public string TripNO { get; set; }
        public string Incoterms { get; set; }
        public string Volumn { get; set; }
        public string VolumnUnit { get; set; }
        public List<ProductDetail> dProductList { get; set; }
        public List<QualityProductDetail> dQualityList { get; set; }
    }

    public class ProductDetail
    {
        public bool Selected { get; set; }
        public string PO_NO { get; set; }
        public string Trip_No { get; set; }
        public string Company { get; set; }
        public string Supplier { get; set; }
        public string Feedstock { get; set; }
        public string Feedstock_CodeName { get; set; }
        public string Incoterms { get; set; }
        public string Volume { get; set; }
        public string VolumeUnit { get; set; }
        public string CargoNo { get; set; }

        public string PO_Date { get; set; }
        public int IsFinal_1ST_Half { get; set; }
        public string Date_1ST_Half { get; set; }
        public int IsFinal_2ND_Half { get; set; }
        public string Date_2ND_Half { get; set; }
    }

    public class QualityProductDetail
    {
        public string NO { get; set; }
        public string PO_ITEM { get; set; }
        public string PO_NO { get; set; }
        public string PODate { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceNoStatus { get; set; }
        public string IsThaiLube { get; set; }
        public string TripNo { get; set; }
        public string CargoNo { get; set; }
        public string Product { get; set; }
        public string Product_Code { get; set; }
        public string VolumnMT { get; set; }
        public string Den_15 { get; set; }
        public string Visc_cst_50 { get; set; }
        public string Sulfur { get; set; }
        public string UnitPriceUSDNew { get; set; }
        public string UnitPriceUSDOld { get; set; }
        public string AdjustPrice { get; set; }
        public string ExchangeRateNew { get; set; }
        public string ExchangeRateOld { get; set; }
        public string UnitPriceTHBNew { get; set; }
        public string UnitPriceTHBOld { get; set; }
        public string Price { get; set; }
        public string Vat { get; set; }
        public string Total { get; set; }
        public string UnitPrice { get; set; }

        public string final_Price { get; set; }
        public string final_Vat { get; set; }
        public string final_Total { get; set; }

        public string price_include_vat { get; set; }
        public string final_minus_pro { get; set; }
        public string total_exc_vat { get; set; }
        public string total_vat { get; set; }
        public string total_inc_vat { get; set; }
        public string invoice_amount { get; set; }
    }

    public class TrnFXSearchDetail
    {
        public string d_Date { get; set; }
        public string d_Buying_value { get; set; }
        public string d_Selling_value { get; set; }

    }

    public class MobDetail
    {
        public DateTime date { get; set; }
        public string ProductName { get; set; }
        public decimal ProductValue { get; set; }
    }

    public class TrnMOBSearchDetail
    {
        public string d_Date { get; set; }
        public string d_Date_num { get; set; }
        public string d_ULG92 { get; set; }
        public string d_ULG95 { get; set; }
        public string d_KERO { get; set; }
        public string d_GASOIL { get; set; }
        public string d_LSFO { get; set; }
        public string d_HSFO { get; set; }
        public string d_H2 { get; set; }
        public string d_LPG { get; set; }
        public string d_PROPANE { get; set; }
        public string d_BUTANE { get; set; }
        public string d_NAPHTA { get; set; }
        public string d_LABIX_FO380 { get; set; }
        public string d_LABIX_PROPANE { get; set; }
        public string d_LABIX_BUTANE { get; set; }
        public string d_LABIX_GASOIL { get; set; }

        public string d_TPX_NAPTHA { get; set; }
    }

    /// <summary>
    /// Model for Add Edit Delete
    /// </summary>
    public class FeedStockBookingPriceDetail
    {
        public string volume { get; set; }
        public string totalPrice { get; set; }
        public string sumtotalPrice { get; set; }

        public string byProduct { get; set; }
        public string sixtyVGO { get; set; }
        public string Naphtha { get; set; }
        public string D_N { get; set; }
        public string sumtotalProduct { get; set; }

        public List<FeedStockDetailViewModel> feedstockDetail { get; set; }
        public List<NaphThaProViewModel> naphThaPro { get; set; }

        public List<PricePeriodControl> pricePeriod { get; set; }
        public List<ExchangeRateControl> exchangeRate { get; set; }
        public CONSTANT_PRODUCT_BASE_DETAIL consProduct { get; set; }
        public List<BlendedKeroseneControl> Blend { get; set; }
        public List<PEPAromaticsAndHABControl> PepAromatic { get; set; }
        public List<StripperOVHDLiquidControl> OVHD { get; set; }
        public List<DrainHydrocarbon> Hydrocarbon { get; set; }
    }
    public class NaphThaProViewModel
    {
        public string Date { get; set; }
        public string InvoiceNo { get; set; }
        public string TripNo { get; set; }
        public string Product { get; set; }
        public string Volume { get; set; }
        public string Den15 { get; set; }
        public string AvgBongkot { get; set; }
        public string Price { get; set; }
    }
    public class NaphThaFinalViewModel
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string DebitNoteNo { get; set; }
        public string Product { get; set; }
        public string Volume { get; set; }
        public string Den15Pro { get; set; }
        public string Den15Final { get; set; }
        public string FxPro { get; set; }
        public string FxFinal { get; set; }
        public string AvgBongkot { get; set; }
        public string UnitPriceProMT { get; set; }
        public string UnitPriceProBahtTMT { get; set; }
        public string UnitPriceForActual1 { get; set; }
        public string UnitPriceForActual2 { get; set; }
        public string ProvisionalUSD { get; set; }
        public string ProvisionalBAHT { get; set; }
        public string AmountExcVat { get; set; }
        public string AmountIncVat { get; set; }
    }
    public class FeedStockDetailViewModel
    {
        public string Date { get; set; }
        public string InvoiceNo { get; set; }
        public string TripNo { get; set; }
        public string Product { get; set; }
        public string Volume { get; set; }

        public string Quality_Den15 { get; set; }
        public string Quality_cst50 { get; set; }
        public string Quality_Sulfur { get; set; }
        public string UnitPriceNewMT { get; set; }
        public string UnitPriceOldMT { get; set; }

        public string AdjustPrice { get; set; }
        public string ExchangeRateNew { get; set; }
        public string ExchangeRateOld { get; set; }
        public string UnitPriceNewBathMT { get; set; }
        public string UnitPriceOldBathMT { get; set; }

        public string ProvisionalPrice { get; set; }
        public string ProvisionalVat { get; set; }
        public string ProvisionalTotal { get; set; }

        public string FinalPrice { get; set; }
        public string FinalVat { get; set; }
        public string FinalTotal { get; set; }

        public string PriceIncludeVat { get; set; }
        public string Final_Pro { get; set; }
        public string AmountExcVat { get; set; }
        public string Vat { get; set; }
        public string TotalIncVat { get; set; }

        public string Comparison_Key { get; set; }
        public string Comparison_FinalPrice_USD_TON { get; set; }

        public string PhysicalValueMin { get; set; }
        public string PhysicalValueMax { get; set; }

        public List<ThaiOilPaymentControl> PaymentList { get; set; }
        public List<PhysicalPropertiesBasedControl> PhysicalBasedList { get; set; }
        public List<INVERSE_DATA> InverseData { get; set; }
        //public List<ComparisonTable> ComparisonList { get; set; }
        public string CT_SulphurContent { get; set; }
        public string CT_ViscosityCST { get; set; }
        public string CT_TemperatureDeg { get; set; }
        public string CT_ViscosityBlend { get; set; }
        public string CT_X { get; set; }
        public string CT_Y { get; set; }
        public string CT_Z { get; set; }
        public string CT_Formula { get; set; }
        public string CT_FormulaValue { get; set; }
        public string CT_MOPS_GO { get; set; }
        public string CT_FBV { get; set; }
        public string CT_HCU { get; set; }
        public string CT_FCC { get; set; }
        public string CT_FinalPrice { get; set; }

        public HCUFormulaControl HCU { get; set; }

    }
    public class BlendedKeroseneControl
    {
        public string MOPSKero { get; set; }
        public string MOPSKeroMinus1 { get; set; }
        public string MOPSKeroMinus1_Sum { get; set; }
        public string MOPSKeroMinus2 { get; set; }
        public string BlendedKerosene_US { get; set; }
        public string FX { get; set; }
        public string BlendedKerosene_Baht { get; set; }
    }
    public class PEPAromaticsAndHABControl
    {
        public string MOPSGO { get; set; }
        public string MOPSGOMinus { get; set; }
        public string PEPAromatics { get; set; }
        public string MOPSFO380 { get; set; }
        public string MOPSFO380_PLUS { get; set; }
        public string HAB { get; set; }
        public string HAB_Ton { get; set; }
        public string HAB_BBL_DIV { get; set; }
        public string HAB_BBL { get; set; }

        public string Pro_PEPAromatics_Price { get; set; }
        public string Pro_PEPAromatics_Volumn { get; set; }
        public string Pro_HAB_Price { get; set; }
        public string Pro_HAB_Volumn { get; set; }
        public string Pro_PEPAromaticsHAB_US { get; set; }
        public string Pro_FX { get; set; }
        public string Pro_PEPAromaticsHAB_Baht { get; set; }

        public string Final_PEPAromatics_Price { get; set; }
        public string Final_PEPAromatics_Volumn { get; set; }
        public string Final_HAB_Price { get; set; }
        public string Final_HAB_Volumn { get; set; }
        public string Final_PEPAromaticsHAB_US { get; set; }
        public string Final_FX { get; set; }
        public string Final_PEPAromaticsHAB_Baht { get; set; }

    }
    public class StripperOVHDLiquidControl
    {
        public string Union_MOPSNaph { get; set; }
        public string Union_MOPSNaphMinus { get; set; }
        public string Union_MOPSNaphMinus_Sum { get; set; }
        public string Union_MOPSNaphMultiply { get; set; }
        public string Union_firstPortion { get; set; }

        public string Union_MOPSKero { get; set; }
        public string Union_MOPSKeroMinus { get; set; }
        public string Union_MOPSKeroMinus_Sum { get; set; }
        public string Union_MOPSKero_Multiply { get; set; }
        public string Union_SecondPortion { get; set; }

        public string Union_First_Second { get; set; }

        public string Union_MinusProcessingCostTon { get; set; }
        public string Union_MinusProcessingCostBBL { get; set; }
        public string Union_UnionfiningPrice { get; set; }

        public string Pacol_LPG { get; set; }
        public string Pacol_LPG_Multiply { get; set; }
        public string Pacol_LPG_Multiply_Multiple { get; set; }
        public string Pacol_LPG_Multiply_Plus { get; set; }
        public string Pacol_LPG_Multiply_Sum1 { get; set; }
        public string Pacol_LPG_Portion { get; set; }
        public string Pacol_LPG_Multiply2 { get; set; }
        public string Pacol_LPG_firstPortionTon { get; set; }
        public string Pacol_LPG_firstPortionBBL { get; set; }

        public string Pacol_Naph { get; set; }
        public string Pacol_Naph_Minus { get; set; }
        public string Pacol_Naph_Minus_Sum1 { get; set; }
        public string Pacol_Naph_Minus_Sum2 { get; set; }
        public string Pacol_Naph_Multiply { get; set; }
        public string Pacol_Naph_SecondPortion { get; set; }

        public string Pacol_ULG92 { get; set; }
        public string Pacol_ULG92_Minus { get; set; }
        public string Pacol_ULG92_Minus_Sum { get; set; }
        public string Pacol_ULG92_Multiply { get; set; }
        public string Pacol_ULG92_ThirdPortion { get; set; }

        public string Plus_First_Second_Third { get; set; }

        public string MinusProcessingCostTon { get; set; }
        public string MinusProcessingCostBBL { get; set; }
        public string PacolPrice { get; set; }

        public string Pro_UnionFining { get; set; }
        public string Pro_Pacol { get; set; }
        public string Pro_StripperOVHD_US { get; set; }
        public string Pro_FX { get; set; }
        public string Pro_StripperOVHD_Baht { get; set; }

        public string Final_UnionFining { get; set; }
        public string Final_Pacol { get; set; }
        public string Final_StripperOVHD_US { get; set; }
        public string Final_FX { get; set; }
        public string Final_StripperOVHD_Baht { get; set; }
    }
    public class DrainHydrocarbon
    {
        public string MOPSKero { get; set; }
        public string Minus1 { get; set; }
        public string Minus2 { get; set; }
        public string MinusProcessionCost { get; set; }
        public string DrainHCPrice_US { get; set; }
        public string FX { get; set; }
        public string DrainHCPrice_Baht { get; set; }

    }
    public class PricePeriodControl
    {
        public string RowID { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string Type { get; set; }
        public string ULG95 { get; set; }
        public string Kero { get; set; }
        public string Gasoil { get; set; }
        public string LSFO { get; set; }
        public string HSFO { get; set; }
        public string H2 { get; set; }
        public string H2_Date { get; set; }
        public string LPG { get; set; }

        public string ULG92 { get; set; }
        public string NAPHTA { get; set; }

    }
    public class ExchangeRateControl
    {
        public string RowID { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string Buying { get; set; }
        public string Selling { get; set; }
        public string Average { get; set; }
    }

    public class HCUFormulaControl
    {
        public double Kero { get; set; }
        public double Gasoil { get; set; }
        public double ULG95 { get; set; }
        public double HSFO { get; set; }
        public double H2 { get; set; }
        public double LPG { get; set; }
        public double HCU_Benefit { get; set; }
        public double FCC_Benefit { get; set; }
    }

    public class ThaiOilPaymentControl
    {
        public string Baseproduct { get; set; }
        public string Percent_S { get; set; }
        public string Viscosity40 { get; set; }
        public string Viscosity50 { get; set; }
        public string ViscosityBlend { get; set; }
        public string FOBSPorePrice { get; set; }
        public string Freight { get; set; }
        public string Loss { get; set; }
        public string ExportParity { get; set; }
        public string ImportParity { get; set; }
    }

    public class PhysicalPropertiesBasedControl
    {
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }

        public string Product { get; set; }
        public string DEN15 { get; set; }
        public string VIS_CST { get; set; }
        public string TEMP_C { get; set; }
        public string SUL_W { get; set; }
        public string fv1 { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        public string RHS { get; set; }
        public string X_Sum { get; set; }
        public string Y_Sum { get; set; }
        public string Z_Sum { get; set; }
        public string RHS_Sum { get; set; }
    }



}