﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class GIClearLineViewModel
    {
        public GIClearLineViewModel_Search GIClearLine_Search { get; set; }

        public List<SelectListItem> ddl_StorageLoc { get; set; }
        public List<SelectListItem> ddl_Type { get; set; }


    }

    public class GIClearLineViewModel_Search
    {
        public string sType { get; set; }
        public string sTripNo { get; set; }
        public string sDeliveryDate { get; set; }
        public string sMessage { get; set; }
        public string sCount { get; set; }
        public List<GIClearLineViewModel_SearchData> SearchData { get; set; }

    }

    public class GIClearLineViewModel_SearchData
    {

        public bool isSelected { get; set; }
        public string dType { get; set; }
        public string dTripNo { get; set; }
        public string dDONo { get; set; }
        public string dDeliveryDate { get; set; }
        public string dPlant { get; set; }
        public string dProduct { get; set; }
        public string dVolumeBBL { get; set; }
        public string dVolumeMT { get; set; }
        public string dVolumeLitres { get; set; }
        public string dPrice { get; set; }
        public string dAmount { get; set; }
        public string dUS4 { get; set; }
        public string dStorageLoc { get; set; }
        public string dTemp { get; set; }
        public string dDensity { get; set; }
        public string dSaleOrder { get; set; }
        public string dPONo { get; set; }
        public string dDO_NO { get; set; }
    }

    [Serializable]
    public class GlobalConfigStorageLocGIClearLine
    {
        public List<PCF_MT_STORAGELOC_GIClearLine> PCF_MT_STORAGE_LOCATION { get; set; }
    }

    public class PCF_MT_STORAGELOC_GIClearLine
    {
        public string COMPANY_CODE { get; set; }
        public string CODE { get; set; }
        public string NAME { get; set; }
    }
}



