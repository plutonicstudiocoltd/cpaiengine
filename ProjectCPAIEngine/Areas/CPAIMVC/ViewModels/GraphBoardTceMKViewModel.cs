﻿using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ProjectCPAIEngine.Model.TceMkDetail;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class GraphBoardTceMKViewModel
    {
        public List<GraphBoardFromDate> GraphDate { get; set; }
        public List<SelectListItem> lstVoyNo { get; set; }
        public List<SelectListItem> lstTD { get; set; }
        public List<string> Xdesx { get; set; }
        public List<TCEMKData> TDData { get; set; }
        public string ActionMSG { get; set; }
    }
    


    
    public class TCEMKViewModel
    {
        public bool PageMode { get; set; }
        public bool GenButton { get; set; }
        public List<SelectListItem> lstTD { get; set; }
        public List<SelectListItem> lstShipName { get; set; }
        public List<SelectListItem> lstPort { get; set; }
        public TceMkDetail TceMKDetail { get; set; }
        public string LoadingMonth { get; set; }
        public string LoadingYear { get; set; }

    }
    
}