﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System.ComponentModel.DataAnnotations;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingDealContactViewModel
    {
        public bool searchTicket { get; set; }
        public string systemType { get; set; }

        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> ddlCompany { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlVS { get; set; }
        public List<SelectListItem> ddlTool { get; set; }

        public HedgingDealContactViewModel_Search Search { get; set; }
    }

    public class HedgingDealContactViewModel_Search
    {
        public string systemType { get; set; }

        public string sStatus { get; set; }
        public string sStatusSelected { get; set; }
        public string sDealNo { get; set; }
        public string sTicketNo { get; set; }
        public string sContactNo { get; set; }
        public string sDealDate { get; set; }
        public string sCompany { get; set; }
        public string sDealType { get; set; }
        public bool sDealType_Sell { get; set; }
        public bool sDealType_Buy { get; set; }
        public string sCounterparty { get; set; }
        public string sUnderlying { get; set; }
        public string sVS { get; set; }
        public string sTool { get; set; }
        public string sVolumeMonth_From { get; set; }
        public string sVolumeMonth_To { get; set; }
        public string sPrice_From { get; set; }
        public string sPrice_To { get; set; }

        public List<HedgingDealContactViewModel_SearchData> SearchData { get; set; }
    }

    public class HedgingDealContactViewModel_SearchData
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; }
        public string hScreenTypeEncrypted { get; set; }

        public string dStatus { get; set; }
        public string dStatusTicket { get; set; }
        public string dDealNo { get; set; }
        public string dTicketNo { get; set; }
        public string dContactNo { get; set; }
        public string dDealDate { get; set; }
        public string dCompany { get; set; }
        public string dDealType { get; set; }
        public string dCounterparty { get; set; }
        public string dUnderlying { get; set; }
        public string dTool { get; set; }
        public string dVolumeMonth { get; set; }
        public string dPrice { get; set; }
    }
}
