﻿using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgingDealViewModel
    {
        public HedgingDealViewModel_Search HedgingDeal_Search { get; set; }
        public HedgingDealViewModel_Detail hedgingDeal_Detail { get; set; }

        public List<SelectListItem> ddlCompany { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }
        public List<SelectListItem> ddlUnitFee { get; set; }
        public List<SelectListItem> ddlUnitVolume { get; set; }
        public List<SelectListItem> ddlUnitPrice { get; set; }
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlUnderlyingName { get; set; }
        public List<SelectListItem> ddlHedged_Type { get; set; }
        public List<SelectListItem> ddlTool { get; set; }
        public List<SelectListItem> ddlObpCounterParty { get; set; }
        public List<SelectListItem> ddlUnitPayment { get; set; }
        

        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> hgdlSellbuy { get; set; }
        public List<SelectListItem> ddlTrade_For { get; set; }
        public List<SelectListItem> ddlCreateBy { get; set; }

        public List<SelectListItem> month_list { get; set; }
        public List<SelectListItem> year_list { get; set; }
        public List<SelectListItem> tool_list { get; set; }
        public List<SelectListItem> underlying_list { get; set; }
        public List<SelectListItem> unitPrice_list { get; set; }
        public List<SelectListItem> unitVolume_list { get; set; }
        public List<SelectListItem> counter_parties_list { get; set; }
        public List<SelectListItem> sbpt_list { get; set; }
        public List<SelectListItem> sbpt_dynamic_list { get; set; }
        public List<SelectListItem> hedge_type_list { get; set; }
        public List<SelectListItem> formula_list { get; set; }
        public List<SelectListItem> formula_operand_list { get; set; }
        public List<SelectListItem> option_list { get; set; }
        public List<SelectListItem> option_operand_list { get; set; }
        public List<SelectListItem> option_operand_premium_list { get; set; }

        public List<SelectListItem> sTool { get; set; }
        public List<SelectListItem> sBuyer { get; set; }
        public List<SelectListItem> sSeller { get; set; }
        public List<SelectListItem> sUnderlying { get; set; }
        public List<SelectListItem> sUnderlying_VS { get; set; }

        public string systemType { get; set; }
        public string pageMode { get; set; }
        public string buttonMode { get; set; }
        public string buttonCode { get; set; }
        public string screenMode { get; set; }
        public string hedgingDeal_reason { get; set; }
        public string reqID { get; set; }
        public string deal_date { get; set; }
        public string deal_time { get; set; }

        public string htmlPDF { get; set; }
        public string dealNo { get; set; }
        public string contractNo { get; set; }
        public string fileUrl { get; set; }
        public string fileContractPdf { get; set; }  
        public List<HedgingDealContractPDF> contractPDFList { get; set; }
        public string sendMailFrom { get; set; }
        public string sendMailTo { get; set; }
        public string sendMailCC { get; set; }
        public string sendMailSubject { get; set; }
        public string sendMailFile { get; set; }
        public string sendMailFileUrl { get; set; }
        public string sendMailBody { get; set; }
        public HttpPostedFileBase sendMailAttachment { get; set; }
    }

    public class HedgingDealContractPDF
    { 
        public DateTime createDate { get; set; }
        public string createBy { get; set; } 
        public string fileUrl { get; set; }
        public string fileContractPdf { get; set; } 
    }

    public class HedgingPreDealViewModel
    {
        public string systemType { get; set; }

        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> ddlHedged_Type { get; set; }
        public List<SelectListItem> ddlTrade_For { get; set; }
        public List<SelectListItem> ddlTool { get; set; }
        public List<SelectListItem> ddlUnderlying { get; set; }
        public List<SelectListItem> ddlVS { get; set; }
        public List<SelectListItem> ddlCompany { get; set; }
        public List<SelectListItem> ddlCounterParty { get; set; }

        public HedgingPreDealViewModel_Search HedgingPreDeal_Search { get; set; }
    }

    public class HedgingDealViewModel_Search
    {
        public string systemType { get; set; }
        public string sDeal_no { get; set; }
        public string sTicket_no { get; set; }
        public string sContactNo { get; set; }
        public string sTrade_For { get; set; }
        public string sDate { get; set; }
        public string sTenor { get; set; }
        public string sCreateBy { get; set; }
        public string sCreateBySelected { get; set; }
        public string sStatusSelected { get; set; }
        public string sVolumeMonth_From { get; set; }
        public string sVolumeMonth_To { get; set; }
        public string sPrice_From { get; set; }
        public string sPrice_To { get; set; }

        public string strStatus { get; set; }
        public string strHedgedType { get; set; }
        public string strTool { get; set; }
        public string strUnderlying { get; set; }
        public string strVS { get; set; }
        public string strSeller { get; set; }
        public string strBuyer { get; set; }

        public List<HedgingDealViewModel_SearchData> sSearchData { get; set; }        
    }

    public class HedgingDealViewModel_SearchData
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; }
        public string hScreenTypeEncrypted { get; set; }
        public string hDealEncrypted { get; set; }
        
        public string dDate { get; set; }
        public string dDealNo { get; set; }
        public string dTicketNo { get; set; }
        public string dContactNo { get; set; }
        public string dHedged_Type { get; set; }
        public string dStatus { get; set; }
        public string dTrade_For { get; set; }
        public string dTool { get; set; }
        public string dUnderlying { get; set; }
        public string dSeller { get; set; }
        public string dBuyer { get; set; }
        public string dCreatedBy { get; set; }
        public string dCopy { get; set; } 
    }    

    public class HedgingPreDealViewModel_Search
    {
        public string systemType { get; set; }

        public string sDeal_no { get; set; }
        public string sTicket_no { get; set; }
        public string sPre_Deal_no { get; set; }        
        public string sDeal_Date { get; set; }
        public string sHedged_Type { get; set; }
        public string sStatus { get; set; }
        public string sTrade_For { get; set; }
        public string sTool { get; set; }
        public string sSeller { get; set; }
        public string sBuyer { get; set; }
        public string sUnderlying { get; set; }
        public string sVS { get; set; }
        public string sCreate_by { get; set; }
        public string sStatusSelected { get; set; }

        public List<HedgingPreDealViewModel_SearchData> sSearchData { get; set; }        
    }

    public class HedgingPreDealViewModel_SearchData
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; }
        public string hScreenTypeEncrypted { get; set; }
        public string hPreDealEncrypted { get; set; }

        public string dDate { get; set; }
        public string dPreDeal_Status { get; set; }
        public string dPre_Deal_no { get; set; }
        public string dHedged_Type { get; set; }
        public string dTool { get; set; }
        public string dToolVS { get; set; }
        public string dUnderlying { get; set; }
        public string dSeller { get; set; }
        public string dBuyer { get; set; }
        public string dCreate_by { get; set; }
        public string dTrade_For { get; set; }
        public string dDeal_no { get; set; }
        public string dDeal_Status { get; set; }

        public List<HedgingPreDealViewModel_Detail> detail { get; set; }
    }

    public class HedgingPreDealViewModel_Detail
    {
        public string hTransactionIdEncrypted { get; set; }
        public string hTransactionRequestEncrypted { get; set; }
        public string hSystemEncrypted { get; set; }
        public string hTypeEncrypted { get; set; }
        public string hScreenTypeEncrypted { get; set; }
        public string hDealNoEncrypted { get; set; }

        public string ref_pre_deal_no { get; set; }
        public string deal_no { get; set; }
        public string status { get; set; }
    }

    #region HedgingDealViewModel_Detail

    public class HedgingDealViewModel_Detail
    {
        public string deal_no { get; set; }
        public string ref_pre_deal_no { get; set; }
        public string ref_ticket_no { get; set; }
        public string ref_deal_no { get; set; }
        public string unwide_status { get; set; }
        public string status { get; set; }
        public string status_tracking { get; set; }
        public string revision { get; set; }
        public string create_by { get; set; }
        public string create_date { get; set; }
        public string update_by { get; set; }
        public string update_date { get; set; }
        public string suggestion { get; set; }
        public string deal_date { get; set; }
        public string deal_type { get; set; }
        public string m1m_status { get; set; }
        public string company { get; set; }
        public string counter_parties { get; set; }
        public string counter_additional { get; set; }
        public string labix_trade_thru_top { get; set; }
        public string labix_fee { get; set; }
        public string labix_fee_unit { get; set; }
        public string underlying { get; set; }
        public string underlying_cf_opr { get; set; }
        public string underlying_cf_val { get; set; }
        public string underlying_weight_percent { get; set; }
        public string ref_underlying { get; set; }
        public string underlying_vs { get; set; }
        public string underlying_vs_cf_opr { get; set; }
        public string underlying_vs_cf_val { get; set; }
        public string underlying_vs_weight_percent { get; set; }
        public string ref_underlying_vs { get; set; }
        public string fw_annual_id { get; set; }
        public string fw_hedged_type { get; set; }
        public Tenor tenor { get; set; }
        public string volume_month { get; set; }
        public string volume_month_unit { get; set; }
        public string volume_month_total { get; set; }
        public string volume_month_total_unit { get; set; }
        public string price { get; set; }
        public string unit_price { get; set; }
        public string tool { get; set; }
        public string payment_day { get; set; }
        public string payment_day_unit { get; set; }
        public List<OtherBidPrice> other_bid_price { get; set; }
        public Verify verify { get; set; }
        public string note { get; set; }
        public List<Contract> contract { get; set; }

        public string reason { get; set; }
    }

    public class Tenor
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string start_end_date { get; set; }
        public string result { get; set; }
        public string excerise_date { get; set; }
        public string extend_date { get; set; }
        public string result_extend { get; set; }
    }

    public class OtherBidPrice
    {
        public string order { get; set; }
        public string counter_parties { get; set; }
        public string price { get; set; }
    }

    public class Verify
    {
        public string credit_rating { get; set; }
        public string credit_rating_reason { get; set; }
        public string credit_rating_file { get; set; }
        public string credit_rateing_remark { get; set; }
        public string cds { get; set; }
        public string cds_reason { get; set; }
        public string cds_file { get; set; }
        public string cds_remark { get; set; }
        public string credit_limit { get; set; }
        public string credit_limit_reason { get; set; }
        public string credit_limit_file { get; set; }
        public string credit_limit_remark { get; set; }
        public string annual_framework { get; set; }
        public string annual_framework_reason { get; set; }
        public string annual_framework_file { get; set; }
        public string annual_framework_remark { get; set; }
        public string committee_framework { get; set; }
        public string committee_framework_reason { get; set; }
        public string committee_framework_file { get; set; }
        public string committee_framework_remark { get; set; }
        public string other_bypass { get; set; }
        public string other_reason { get; set; }
        public string other_file { get; set; }
        public string other_remark { get; set; }
    }

    public class Contract
    {
        public string order { get; set; }
        public string contract_no { get; set; }
        public string contract_file { get; set; }
    }

    #endregion

}