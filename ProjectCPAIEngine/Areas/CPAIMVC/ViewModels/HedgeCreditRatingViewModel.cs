﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeCreditRatingViewModel
    {
        public List<CreditRating> creditRating { get; set; }
    }

    public class CreditRating
    {
        public string ID { get; set; }
        public string Order { get; set; }
        public string Rating { get; set; }
        public string Amount { get; set; }
        public string Desc { get; set; }
        public string Status { get; set; }
        public string CreateDate { get; set; }
        public string CreateBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

}