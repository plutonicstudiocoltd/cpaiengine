﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using static ProjectCPAIEngine.Areas.CPAIMVC.ViewModels.HedgeDealViewModel;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public static class HedgeDealViewModel
    {
        public const string FORMAT_DATE = "dd-MMM-yyyy";
        public const string FORMAT_DATETIME = "dd-MMM-yyyy HH:mm";

        public enum DealType
        {
            [Description("SELL")] Sell,
            [Description("BUY")] Buy,
        }

        public enum DealFlagType
        {
            [Description("Swaption")] Swaption,
            [Description("Extendible")] Extendible,
            [Description("Unwind")] Unwind,
            [Description("M-1 / M")] M1M,
            [Description("Time Spread")] TimeSpread,
            [Description("Terminate")] Terminate,
            [Description("Re-Structure")] ReStructure,
            [Description("Revoke")] Revoke,
        }
        public enum DealTimeSpreadType
        {
            [Description("Consecutive")] Consecutive,
            [Description("Non-Consecutive")] NonConsecutive
        }

        public enum ProductExtendType
        {
            None = 0,
            Normal,
            Subtract,
            Divide,
        }

        public enum KnockType
        {
            [Description("None")] None = 0,
            [Description("By Package")] ByPackage,
            [Description("By Market")] ByMarket,
        }

        internal static IEnumerable<DateTime> MonthsBetween(DateTime d0, DateTime d1)
        {
            return Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                             .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m));
        }
    }

    public class DealTypeModel
    {
        public DealType DealType { get; set; }
        public bool IsSelected { get; set; }
    }
    public class DealFlagTypeModel
    {
        public DealFlagType DealFlagType { get; set; }
        public bool IsSelected { get; set; }
    }
    public class DealTimeSpreadTypeModel
    {
        public DealTimeSpreadType DealTimeSpreadType { get; set; }
        public bool IsSelected { get; set; }
    }

    public class HedgeDealSummaryCriteria
    {
        public string PreDealNo { get; set; }
        public string DealNo { get; set; }
        public string TicketNo { get; set; }
        public string ContractNo { get; set; }
        public string Underlying { get; set; }
        public string CreatedBy { get; set; }

        public string TradeDates { get; set; }
        public string SubmitDates { get; set; }
        public string Tenors { get; set; }

        public string TotalVolumnFrom { get; set; }
        public string TotalVolumnTo { get; set; }

        public string Company { get; set; }
        public string TradingBook { get; set; }
        public string Counterparty { get; set; }
        public string Status { get; set; }
        public string RefCommittee { get; set; }
        public string Tool { get; set; }

        public List<DealTypeModel> DealTypes { get; set; }
        public List<DealFlagTypeModel> DealFlagTypes { get; set; }

        public HedgeDealSummaryCriteria()
        {
            DealTypes = new List<DealTypeModel>
            {
                new DealTypeModel { DealType = DealType.Sell, IsSelected = false },
                new DealTypeModel { DealType = DealType.Buy, IsSelected = false }
            };
            DealFlagTypes = new List<DealFlagTypeModel>
            {
                new DealFlagTypeModel { DealFlagType = DealFlagType.Swaption, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.Terminate, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.Extendible, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.ReStructure, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.Unwind, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.Revoke, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.M1M, IsSelected = false },
                new DealFlagTypeModel { DealFlagType = DealFlagType.TimeSpread, IsSelected = false },
            };
        }

        internal HedgeDealSummaryCriteria Clone()
        {
            var criteria = (HedgeDealSummaryCriteria)MemberwiseClone();
            return criteria;
        }
    }

    public class HedgeDealSummaryResult
    {
    }

    public class HedgeDealSummaryViewModel
    {
        public List<SelectListItem> Companies { get; set; }
        public List<SelectListItem> TradingBooks { get; set; }
        public List<SelectListItem> Counterparties { get; set; }
        public List<SelectListItem> Status { get; set; }
        public List<SelectListItem> RefCommittees { get; set; }
        public List<SelectListItem> Tools { get; set; }

        public HedgeDealSummaryCriteria Criteria { get; set; }
        public List<HedgeDealSummaryResult> HedgeDealSummaryResults { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;

        public HedgeDealSummaryViewModel()
        {
            Companies = new List<SelectListItem>();
            TradingBooks = new List<SelectListItem>();
            Counterparties = new List<SelectListItem>();
            Status = new List<SelectListItem>();
            RefCommittees = new List<SelectListItem>();
            Tools = new List<SelectListItem>();

            Criteria = new HedgeDealSummaryCriteria();
            HedgeDealSummaryResults = new List<HedgeDealSummaryResult>();
        }
    }

    public class HedgeDealKeyinHeaderViewModel
    {
        public string Company { get; set; }
        public string TradingBook { get; set; }
        public string Counterparty { get; set; }
        public string FeeValue { get; set; }
        public string UnitPrice { get; set; }

        public string TradeDates { get; set; }
        public bool IsTradeFor { get; set; }
        public bool IsUnwind { get; set; }

        public List<SelectListItem> Companies { get; set; }
        public List<SelectListItem> TradingBooks { get; set; }
        public List<SelectListItem> Counterparties { get; set; }
        public List<SelectListItem> UnitPrices { get; set; }

        public HedgeDealKeyinHeaderViewModel()
        {
            Companies = new List<SelectListItem>();
            TradingBooks = new List<SelectListItem>();
            Counterparties = new List<SelectListItem>();
            UnitPrices = new List<SelectListItem>();
        }
    }

    public class HedgeDealKeyinSearchCommitteeFrameworkCriteria
    {
        public string ReferenceNo { get; set; }
        public string TenorDates { get; set; }
        public string TotalVolumn { get; set; }
        public string Underlying { get; set; }
        public string DealType { get; set; }
        public string ToolName { get; set; }
        public string HedgingType { get; set; }
        public DealFlagTypeModel DealM1M { get; set; }
        public DealFlagTypeModel DealTimeSpread { get; set; }

        public HedgeDealKeyinSearchCommitteeFrameworkCriteria()
        {
            DealM1M = new DealFlagTypeModel { DealFlagType = DealFlagType.M1M, IsSelected = false };
            DealTimeSpread = new DealFlagTypeModel { DealFlagType = DealFlagType.TimeSpread, IsSelected = false };
        }
    }
    public class CommitteeFrameworkResult
    {
        public string Id { get; set; }
        public bool IsSelected { get; set; }
        public string TradingBook { get; set; }
        public string ReferenceNo { get; set; }
        public string HedgingType { get; set; }
        public string Tenor { get; set; }
        public string TotalVolumn { get; set; }
        public string Underlying { get; set; }
        public string ToolName { get; set; }
    }
    public class HedgeDealKeyinSearchCommitteeFrameworkViewModel
    {
        public List<SelectListItem> DealTypes { get; set; }
        public List<SelectListItem> ToolNames { get; set; }
        public List<SelectListItem> HedgingTypes { get; set; }

        public HedgeDealKeyinSearchCommitteeFrameworkCriteria Criteria { get; set; }
        public List<CommitteeFrameworkResult> Result { get; set; }

        public HedgeDealKeyinSearchCommitteeFrameworkViewModel()
        {
            DealTypes = new List<SelectListItem>();
            ToolNames = new List<SelectListItem>();
            HedgingTypes = new List<SelectListItem>();
            Criteria = new HedgeDealKeyinSearchCommitteeFrameworkCriteria();
            Result = new List<CommitteeFrameworkResult>();
        }
    }

    public class HedgeDealKeyinCommitteeFrameworkViewModel
    {
        public string RefNo { get; set; }
        public DealTypeModel DealTypeSell { get; set; }
        public DealTypeModel DealTypeBuy { get; set; }
        public string HedgeType { get; set; }
        public string Tenor { get; set; }
        public string TotalVolumn { get; set; }
        public string Unit { get; set; }
        public DealFlagTypeModel DealM1M { get; set; }
        public DealFlagTypeModel DealTimeSpread { get; set; }

        public DealTimeSpreadTypeModel DealTimeSpreadConsecutive { get; set; }
        public DealTimeSpreadTypeModel DealTimeSpreadNonConsecutive { get; set; }
        
        public HedgeDealKeyinCommitteeFrameworkViewModel()
        {
            DealTypeSell = new DealTypeModel { DealType = DealType.Sell, IsSelected = false };
            DealTypeBuy = new DealTypeModel { DealType = DealType.Buy, IsSelected = false };
            DealM1M = new DealFlagTypeModel { DealFlagType = DealFlagType.M1M, IsSelected = false };
            DealTimeSpread = new DealFlagTypeModel { DealFlagType = DealFlagType.TimeSpread, IsSelected = false };
            DealTimeSpreadConsecutive = new DealTimeSpreadTypeModel { DealTimeSpreadType = DealTimeSpreadType.Consecutive, IsSelected = false };
            DealTimeSpreadNonConsecutive = new DealTimeSpreadTypeModel { DealTimeSpreadType = DealTimeSpreadType.NonConsecutive, IsSelected = false };
        }
    }

    public class HedgeDealKeyinProductViewModel
    {
        public string AFFrame { get; set; }
        public string ProductName { get; set; }
        public string CFDefault { get; set; }
        public string CFDiff { get; set; }
        public string ProductUnit { get; set; }
        public string MainUnit { get; set; }
        public string Weight { get; set; }
        public ProductExtendType Type { get; set; }
    }

    public class HedgeUnderlyingPackageViewModel
    {
        public bool IsSelected { get; set; }

        public string PackageId
        {
            get
            {
                return string.Format("package_{0}", GetHashCode().ToString());
            }
        }

        public string PacketName { get; set; }
        public string ExternalCategory { get; set; }
        public List<HedgeDealKeyinProductViewModel> Products { get; set; }

        public bool HasProduct
        {
            get
            {
                return Products != null && Products.Count > 0;
            }
        }

        public HedgeUnderlyingPackageViewModel()
        {
            Products = new List<HedgeDealKeyinProductViewModel>();
        }
    }

    public class HedgeUnderlyingViewModel
    {
        public string MainVolumn { get; set; }
        public string MainUnit { get; set; }
        public List<HedgeUnderlyingPackageViewModel> Packages { get; set; }

        public bool HasPackage
        {
            get
            {
                return Packages != null && Packages.Count > 0;
            }
        }

        public HedgeUnderlyingViewModel()
        {
            Packages = new List<HedgeUnderlyingPackageViewModel>();
        }
    }

    public class HedgeDealToolOption
    {
        public string Order { get; set; }
        public string OptionName { get; set; }
        public string NetTargetPrice { get; set; }
        public string Return { get; set; }
        public string Premium { get; set; }
    }

    public class HedgeDealToolExtendible
    {
        public string Order { get; set; }
        public string ExtendPeriod { get; set; }
        public string ExerciseDate { get; set; }
        public string ExtendVolumn { get; set; }
    }

    public class HedgeDealToolKnockItem
    {
        public string Order { get; set; }
        public string KnockOption { get; set; }
        public string Condition { get; set; }
        public string HedgePrice { get; set; }
    }

    public class HedgeDealToolKnock
    {
        public KnockType KnockType { get; set; }
        public string KnockProductName { get; set; }
        public List<HedgeDealToolKnockItem> KnockItems { get; set; }

        public HedgeDealToolKnock()
        {
            KnockItems = new List<HedgeDealToolKnockItem>();
        }
    }

    public class HedgeDealTool
    {
        public bool IsSelected { get; set; }
        public string ToolId { get; set; }
        public string ToolName { get; set; }
        public string MainUnit { get; set; }

        public bool IsNetPremium { get; set; }
        public string NetPremiumValue { get; set; }
        public string NetPremiumUnit { get; set; }

        public bool IsUseFormula { get; set; }
        public string Formula { get; set; }

        public string SwaptionExerciseDate { get; set; }

        public string TargetRedemptionGain { get; set; }
        public string TargetRedemptionLoss { get; set; }
        public string TargetRedemptionGainActual { get; set; }
        public string TargetRedemptionLossActual { get; set; }

        public string CappedGain { get; set; }
        public string CappedGainActual { get; set; }

        public List<HedgeDealToolOption> Options { get; set; }
        public List<HedgeDealToolExtendible> Extendibles { get; set; }
        public HedgeDealToolKnock Knock { get; set; }

        public bool HasOption
        {
            get
            {
                return Options != null && Options.Count > 0;
            }
        }

        public HedgeDealTool()
        {
            Options = new List<HedgeDealToolOption>();
            Extendibles = new List<HedgeDealToolExtendible>();
            Knock = new HedgeDealToolKnock();
        }
    }

    public class HedgeDealToolViewModel
    {
        public bool HasTool
        {
            get
            {
                return Tools != null && Tools.Count > 0;
            }
        }

        public List<HedgeDealTool> Tools { get; set; }
    }

    public class HedgeDealDetailValue
    {
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
        public string PremiumValue { get; set; }
        public bool IsNetPremium { get; set; }
    }
    public class HedgeDealDetailItem
    {
        public string Order { get; set; }
        public string Tenor { get; set; }
        public string MainVolumn { get; set; }
        public string Extend { get; set; }
        public string Settlement { get; set; }
        public string TenorStatus { get; set; }
        public List<HedgeDealDetailValue> Values { get; set; }

        public HedgeDealDetailItem()
        {
            Values = new List<HedgeDealDetailValue>();
        }
    }
    public class HedgeDealOtherBidPriceItem
    {
        public string Order { get; set; }
        public string Counterparty { get; set; }
        public string Price { get; set; }
        public string Note { get; set; }
    }
    public class HedgeDealDetailViewModel
    {
        public string DealTenorDates { get; set; }
        public string VolumnPerMonth { get; set; }
        public string TotalVolumn { get; set; }
        public bool IsNetPremium { get; set; }
        public string NetPremiumVolumn { get; set; }
        public string TotalDealVolumn { get; set; }
        public string MainUnit { get; set; }
        public List<HedgeDealDetailItem> TenorItems { get; set; }
        public List<HedgeDealOtherBidPriceItem> OtherBidPriceItems { get; set; }

        public bool HasItem
        {
            get
            {
                return TenorItems != null && TenorItems.Count > 0;
            }
        }

        public HedgeDealDetailViewModel()
        {
            TenorItems = new List<HedgeDealDetailItem>();
            OtherBidPriceItems = new List<HedgeDealOtherBidPriceItem>();
        }

        internal static List<HedgeDealDetailItem> GenerateDealDetailItems(HedgeDealTool tool)
        {
            var dealTenorDates = "15-JAN-2017 to 31-MAR-2018";
            var cultureInfo = new CultureInfo("en-US");
            var datePair = Models.VCoolServiceModel.GetDatePair(dealTenorDates, cultureInfo);
            var monthList = MonthsBetween(datePair.Item1, datePair.Item2).Select(c => c.ToString("MMM-yyyy", cultureInfo)).ToList();

            var isNetPremium = tool.IsNetPremium;
            var values = tool.Options.Select(c =>
            {
                return new HedgeDealDetailValue
                {
                    IsNetPremium = isNetPremium,
                    OptionName = c.OptionName,
                    OptionValue = "",
                    PremiumValue = "",
                };
            });

            var order = 0;
            var items = monthList.Select(month =>
            {
                order++;
                var item = new HedgeDealDetailItem
                {
                    Order = order.ToString(),
                    Tenor = month,
                    MainVolumn = "1000",
                    Extend = "",
                    Settlement = "Settled",
                    TenorStatus = "ACTIVE",
                    Values = values.ToList(),
                };
                return item;
            }).ToList();
            return items;
        }
    }

    public class HedgeDealKeyinMainViewModel
    {
        #region Deal Information

        public string PreDealNo { get; set; }
        public string DealNo { get; set; }
        public string TicketNo { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string Status { get; set; }
        public string Revision { get; set; }

        #endregion Deal Information

        #region Sections

        public HedgeDealKeyinHeaderViewModel Header { get; set; }
        public HedgeDealKeyinCommitteeFrameworkViewModel CommitteeFramework { get; set; }
        public HedgeUnderlyingViewModel Underlying { get; set; }
        public HedgeUnderlyingViewModel ExtendUnderlying { get; set; }
        public HedgeDealToolViewModel Tool { get; set; }
        public HedgeDealDetailViewModel DealDetail { get; set; }

        public HedgeDealKeyinSearchCommitteeFrameworkViewModel CommitteeFrameworkCriteria { get; set; }

        #endregion Sections

        public string ErrorMessage { get; set; } = string.Empty;

        public HedgeDealKeyinMainViewModel()
        {
            Header = new HedgeDealKeyinHeaderViewModel();
            CommitteeFramework = new HedgeDealKeyinCommitteeFrameworkViewModel();
            Underlying = new HedgeUnderlyingViewModel();
            ExtendUnderlying = new HedgeUnderlyingViewModel();
            Tool = new HedgeDealToolViewModel();
            DealDetail = new HedgeDealDetailViewModel();
            CommitteeFrameworkCriteria = new HedgeDealKeyinSearchCommitteeFrameworkViewModel();
        }

        internal static HedgeDealKeyinMainViewModel GetMockup()
        {
            var tool = new HedgeDealToolViewModel
            {
                Tools = new List<HedgeDealTool>
                {
                        new HedgeDealTool
                        {
                            ToolId = "Swap001",
                            ToolName = "Swap",

                            CappedGain = "500",
                            CappedGainActual = "300",
                            Formula = "(Sell Swap - Sell Put) - Sell Call",
                            IsNetPremium = false,
                            IsSelected = false,
                            IsUseFormula = true,
                            MainUnit = "BBL",
                            NetPremiumUnit = "BBL",
                            NetPremiumValue = "",
                            SwaptionExerciseDate = "31 DEC 2016",
                            TargetRedemptionGain = "500",
                            TargetRedemptionGainActual = "300",
                            TargetRedemptionLoss = "500",
                            TargetRedemptionLossActual = "300",
                            Options = new List<HedgeDealToolOption>
                            {
                                new HedgeDealToolOption {Order = "1", OptionName = "Sell Call", NetTargetPrice = "6.15", Return = "1.00", Premium = "2.00" },
                                new HedgeDealToolOption {Order = "2", OptionName = "Buy Put", NetTargetPrice = "4.10", Return = "1.00", Premium = "2.00" },
                                new HedgeDealToolOption {Order = "3", OptionName = "Sell Put", NetTargetPrice = "4.15", Return = "1.00", Premium = "2.00" },
                            },
                            Extendibles = new List<HedgeDealToolExtendible>
                            {
                                new HedgeDealToolExtendible { Order = "1", ExtendPeriod = "JAN 17 to JUN 17", ExerciseDate = "31 DEC 16", ExtendVolumn = "300" },
                                new HedgeDealToolExtendible { Order = "2", ExtendPeriod = "JUL 17 to DEC 17", ExerciseDate = "31 JUN 16", ExtendVolumn = "300" },
                            },
                            Knock = new HedgeDealToolKnock
                            {
                                KnockProductName = "GO0.1 (BBL)",
                                KnockType = KnockType.ByPackage,
                                KnockItems = new List<HedgeDealToolKnockItem>
                                {
                                    new HedgeDealToolKnockItem {  Order = "1", KnockOption = "Knock Out", Condition = "MK >=", HedgePrice ="20" },
                                }
                            },
                        },
                        new HedgeDealTool
                        {
                            ToolId = "Swap3WayExt001",
                            ToolName = "3 Way Ext",

                            CappedGain = "500",
                            CappedGainActual = "300",
                            Formula = "(Sell Swap - Sell Put) - Sell Call",
                            IsNetPremium = false,
                            IsSelected = false,
                            IsUseFormula = true,
                            MainUnit = "BBL",
                            NetPremiumUnit = "BBL",
                            NetPremiumValue = "",
                            SwaptionExerciseDate = "31 DEC 2016",
                            TargetRedemptionGain = "500",
                            TargetRedemptionGainActual = "300",
                            TargetRedemptionLoss = "500",
                            TargetRedemptionLossActual = "300",
                            Options = new List<HedgeDealToolOption>
                            {
                                new HedgeDealToolOption {Order = "1", OptionName = "Sell Call", NetTargetPrice = "6.15", Return = "1.00", Premium = "2.00" },
                                new HedgeDealToolOption {Order = "2", OptionName = "Buy Put", NetTargetPrice = "4.10", Return = "1.00", Premium = "2.00" },
                                new HedgeDealToolOption {Order = "3", OptionName = "Sell Put", NetTargetPrice = "4.15", Return = "1.00", Premium = "2.00" },
                            },
                            Extendibles = new List<HedgeDealToolExtendible>
                            {
                                new HedgeDealToolExtendible { Order = "1", ExtendPeriod = "JAN 17 to JUN 17", ExerciseDate = "31 DEC 16", ExtendVolumn = "300" },
                                new HedgeDealToolExtendible { Order = "2", ExtendPeriod = "JUL 17 to DEC 17", ExerciseDate = "31 JUN 16", ExtendVolumn = "300" },
                            },
                            Knock = new HedgeDealToolKnock
                            {
                                KnockProductName = "GO0.1 (BBL)",
                                KnockType = KnockType.ByPackage,
                                KnockItems = new List<HedgeDealToolKnockItem>
                                {
                                    new HedgeDealToolKnockItem { KnockOption = "Knock Out", Condition = "MK >=", HedgePrice ="20" },
                                }
                            },
                        },
                    }
            };

            return new HedgeDealKeyinMainViewModel
            {
                PreDealNo = "PDEAL-1708-001",
                DealNo = "DEL-1708-001",
                TicketNo = "",
                Status = "New",
                Revision = "0.0.0.001",
                CreatedBy = "Admin",
                CreatedDate = DateTime.Now.ToString(HedgeDealViewModel.FORMAT_DATETIME, new CultureInfo("en-US")),
                UpdatedBy = "Admin",
                UpdatedDate = DateTime.Now.ToString(HedgeDealViewModel.FORMAT_DATETIME, new CultureInfo("en-US")),

                Underlying = new HedgeUnderlyingViewModel
                {
                    MainUnit = "BBL",
                    MainVolumn = "2000",
                    Packages = new List<HedgeUnderlyingPackageViewModel>
                    {
                        new HedgeUnderlyingPackageViewModel
                        {
                            IsSelected = true, PacketName = "GO-DB1", ExternalCategory = "Crude Spread", Products= new List<HedgeDealKeyinProductViewModel>
                            {
                                new HedgeDealKeyinProductViewModel{ AFFrame = "GO-DB1", ProductName  ="GO0.1 (BBL)", CFDefault = "",ProductUnit = "DB (MTON)", CFDiff = "0.6", Weight = "60",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "GO-DB1", ProductName  ="GO0.2 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "DB1", ProductName  ="DB1 (BBL)", CFDefault = "",ProductUnit = "", CFDiff = "", Weight = "100",Type = ProductExtendType.Subtract,MainUnit = "BBL"},
                            }
                        },
                        new HedgeUnderlyingPackageViewModel
                        {
                            IsSelected = false, PacketName = "GO-DB2", ExternalCategory = "Crude Spread", Products= new List<HedgeDealKeyinProductViewModel>
                            {
                                new HedgeDealKeyinProductViewModel{ AFFrame = "GO-DB2", ProductName  ="GO0.1 (BBL)", CFDefault = "",ProductUnit = "DB (MTON)", CFDiff = "0.6", Weight = "60",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "GO-DB2", ProductName  ="GO0.2 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "GO-DB2", ProductName  ="GO0.3 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "DB2", ProductName  ="DB2 (BBL)", CFDefault = "",ProductUnit = "", CFDiff = "", Weight = "100",Type = ProductExtendType.Subtract,MainUnit = "BBL"},
                            }
                        },
                    }
                },
                ExtendUnderlying = new HedgeUnderlyingViewModel
                {
                    MainUnit = "BBL",
                    MainVolumn = "2000",
                    Packages = new List<HedgeUnderlyingPackageViewModel>
                    {
                        new HedgeUnderlyingPackageViewModel
                        {
                            IsSelected = true, PacketName = "EX-DB1", ExternalCategory = "Crude Spread", Products= new List<HedgeDealKeyinProductViewModel>
                            {
                                new HedgeDealKeyinProductViewModel{ AFFrame = "EX-DB1", ProductName  ="GO0.1 (BBL)", CFDefault = "",ProductUnit = "DB (MTON)", CFDiff = "0.6", Weight = "60",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "EX-DB1", ProductName  ="GO0.2 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "DB1", ProductName  ="DB1 (BBL)", CFDefault = "",ProductUnit = "", CFDiff = "", Weight = "100",Type = ProductExtendType.Subtract,MainUnit = "BBL"},
                            }
                        },
                        new HedgeUnderlyingPackageViewModel
                        {
                            IsSelected = false, PacketName = "EX-DB2", ExternalCategory = "Crude Spread", Products= new List<HedgeDealKeyinProductViewModel>
                            {
                                new HedgeDealKeyinProductViewModel{ AFFrame = "EX-DB2", ProductName  ="GO0.1 (BBL)", CFDefault = "",ProductUnit = "DB (MTON)", CFDiff = "0.6", Weight = "60",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "EX-DB2", ProductName  ="GO0.2 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "EX-DB2", ProductName  ="GO0.3 (BBL)", CFDefault = "",ProductUnit = "DB (BBL)", CFDiff = "", Weight = "50",Type = ProductExtendType.Normal,MainUnit = "BBL"},
                                new HedgeDealKeyinProductViewModel{ AFFrame = "DB2", ProductName  ="DB2 (BBL)", CFDefault = "",ProductUnit = "", CFDiff = "", Weight = "100",Type = ProductExtendType.Subtract,MainUnit = "BBL"},
                            }
                        },
                    }
                },
                Tool = tool,
                DealDetail = new HedgeDealDetailViewModel
                {
                    DealTenorDates = "15-JAN-2017 to 31-MAR-2018",
                    IsNetPremium = false,
                    MainUnit = "BBL",
                    NetPremiumVolumn = "",
                    TotalDealVolumn = "1000",
                    TotalVolumn = "",
                    VolumnPerMonth = "",
                    TenorItems = HedgeDealDetailViewModel.GenerateDealDetailItems(tool.Tools.FirstOrDefault()),
                },
            };
        }
    }
}