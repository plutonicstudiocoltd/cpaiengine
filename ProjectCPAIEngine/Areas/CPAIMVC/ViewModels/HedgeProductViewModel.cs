﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeProductViewModel
    {
        public List<SearchProduct> SearchProductList { get; set; }
        public string deleteRowId { get; set; }
        public string rowId { get; set; }
        public string pageMode { get; set; }
        public string sCode { get; set; }
        public string sName { get; set; }
        public string sFullName { get; set; }
        public string sDescription { get; set; }
        public string sStatus { get; set; }
        public string sUpdateDate { get; set; }
        public string sUpdateBy { get; set; }
        public string sSelectUpdateBy { get; set; }
        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> ddlUser { get; set; }
        public List<MKT_MST_MOPS_PRODUCT_List> MMMPRODUCT_List { get; set; }
        public List<SearchMarketData> SearchMarketList { get; set; }
        public List<string> oldSelectList { get; set; }

        public string code { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string fullName { get; set; }
        public string description { get; set; }
        public List<MappingMarketPrice> MappingMarketPriceList { get; set; }

        public string MarketId { get; set; }
        public string MarketName { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string SelectedProduct { get; set; }
        public List<SelectListItem> ddlMarketName { get; set; }
        public List<SelectListItem> ddlProductUnit { get; set; }
        public List<SelectListItem> ddlSecondprice { get; set; }

        public HedgeProductViewModel()
        {
            oldSelectList = new List<string>();
            MMMPRODUCT_List = new List<MKT_MST_MOPS_PRODUCT_List>(); 
        }

    }

    public class SearchProduct
    {
        public string CodeEncrypt { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }

    public class SearchMarketData
    {
        public string MarketId { get; set; }
        public string MarketName { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; }
        public string Select { get; set; } 
    }

    public class MappingMarketPrice
    {        
        public string RowId { get; set; }
        public string Number { get; set; }
        public string ProductId_F { get; set; }
        public string FirstPrice { get; set; }
        public string FirstPriceUnit { get; set; }
        public string ProductId_S { get; set; }
        public string SecondPrice { get; set; }
        public string SecondPriceUnit { get; set; }
        public List<MappingForward> MappingForwardList { get;set;}
        public string ProductDisplayName { get; set; }
        public string ProductUnit { get; set; }
        public string CFBBL { get; set; }
        public bool Show { get; set; }
        public string Delete { get; set; }
        public MappingMarketPrice()
        {
            MappingForwardList = new List<MappingForward>();
        }
    }

    public class MappingForward
    {
        public string RowId { get; set; }
        public string FKId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Unit { get; set; } 
    }

    public class MKT_MST_MOPS_PRODUCT_List
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string MKT_MST_MOPS_MARKET_ID { get; set; } 
        public string Unit { get; set; }
    }

}