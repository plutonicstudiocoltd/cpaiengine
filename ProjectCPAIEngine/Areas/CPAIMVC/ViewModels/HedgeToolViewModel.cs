﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeToolViewModel
    {
        public HedgeToolViewModel_Search tool_Search { get; set; }
        public HedgeToolViewModel_Detail tool_Detail { get; set; }

        public List<SelectListItem> ddlStatus { get; set; }
        public List<SelectListItem> ddlUser { get; set; }

        public List<SelectListItem> ddlToolSwitch { get; set; }
        public List<SelectListItem> ddlToolOption { get; set; }
        public List<SelectListItem> ddlToolConditionOperand { get; set; }
        public List<SelectListItem> ddlToolConditionOperator { get; set; }
        public List<SelectListItem> ddlToolFormular { get; set; }

        public string pageMode { get; set; }
    }

    public class HedgeToolViewModel_Search
    {
        public string sCode { get; set; }
        public string sName { get; set; }
        public string sDesc { get; set; }
        public string sStatus { get; set; }
        public string sOption { get; set; }
        public string sUpdateDate { get; set; }
        public string sUpdateBy { get; set; }
        public string sSelectUpdateBy { get; set; }

        public List<HedgeToolViewModel_SearchData> sSearchData { get; set; }
    }

    public class HedgeToolViewModel_SearchData
    {
        public string dHedgeToolID { get; set; }
        public string dCode { get; set; }
        public string dName { get; set; }
        public string dDesc { get; set; }
        public string dOption { get; set; }
        public string dStatus { get; set; }
        public string dVersion { get; set; }
        public string dUpdateDate { get; set; }
        public string dUpdateBy { get; set; }

        public string dFeatureTargetFlag { get; set; }
        public string dFeatureSwapFlag { get; set; }
        public string dFeatureExtendFlag { get; set; }
        public string dFeatureCappedFlag { get; set; }
        public string dFeatureKnockInOutFlag { get; set; }
    }

    public class HedgeToolViewModel_Detail
    {
        public string HMT_ROW_ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FeatureTargetFlag { get; set; }
        public string FeatureSwapFlag { get; set; }
        public string FeatureExtendFlag { get; set; }
        public string FeatureCappedFlag { get; set; }
        public string FeatureKnockInOutFlag { get; set; }
        public string Version { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public List<ToolSwitch> ToolSwitch { get; set; }
        public List<ToolOption> ToolOption { get; set; }
    }

    public class ToolSwitch
    {
        public string HMTS_ROW_ID { get; set; }
        public string ToolID { get; set; }
        public string Order { get; set; }
        public string Option { get; set; }
    }

    public class ToolOption
    {
        public string HMTO_ROW_ID { get; set; }
        public string ToolID { get; set; }
        public string Order { get; set; }
        public string Option { get; set; }
        public List<ToolCondition> ToolCondition { get; set; }
    }

    public class ToolCondition
    {
        public string HMTC_ROW_ID { get; set; }
        public string ToolOptionID { get; set; }
        public string Order { get; set; }
        public string Value_A { get; set; }
        public string Value_B { get; set; }
        public string Operator { get; set; }
        public string Formula { get; set; }
        public string Expression { get; set; }
        public string ReturnFlag { get; set; }
        public string PremiumFlag { get; set; }
    }

}