﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class HedgeTypeViewModel
    {
        public HedgeTypeViewModel_Seach ht_Search { get; set; }
        public HedgeTypeViewModel_Detail ht_Detail { get; set; }

        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_Inventory { get; set; }
    }

    public class HedgeTypeViewModel_Detail
    {
        public string HedgeTypeCode { get; set; }
        public string HedgeTypeName { get; set; }
        public string HedgeTypeStatus { get; set; }
        public bool BoolStatus { get; set; }
        //public string CreateType { get; set; }
        public string HedgeTypeInventory { get; set; }
        public bool BoolInventory { get; set; }
        public string description { get; set; }
    }

    public class HedgeTypeViewModel_Seach
    {
        public string sHedgeTypeCode { get; set; }
        public string sHedgeTypeName { get; set; }
        public string sHedgeTypeStatus { get; set; }
        public string sHedgeTypeInventory { get; set; }
        public string sHedgeTypeUpdatedDate { get; set; }
        public string sHedgeTypeUpdatedBy { get; set; }
        public string sHedgeTypeDesc { get; set; }

        public List<HedgeTypeViewModel_SeachData> sSearchData { get; set; }
    }

    public class HedgeTypeViewModel_SeachData
    {
        public string dHedgeTypeCode { get; set; }
        public string dHedgeTypeName { get; set; }
        public string dHedgeTypeStatus { get; set; }
        public string dHedgeTypeInventory { get; set; }
        public string dHedgeTypeDesc { get; set; }
        public string dHedgeTypeUpdatedDate { get; set; }
        public string dHedgeTypeUpdatedBy { get; set; }       
    }
}