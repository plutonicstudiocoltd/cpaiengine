﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class MainBoardsModel
    {
        [JsonProperty("menu")]
        public List<Tabs> menu { get; set; }
    }
       
    public class Tabs
    {
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("tab_name")]
        public string tab_name { get; set; }
        [JsonProperty("tab")]
        public string tab { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
    }
}