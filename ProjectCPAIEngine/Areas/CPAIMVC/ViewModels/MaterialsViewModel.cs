﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class MaterialsViewModel
    {
        public MaterialsViewModel_Search met_Search { get; set; }
        public MaterialsViewModel_Detail met_Detail { get; set; }

        public List<SelectListItem> ddl_CreateType { get; set; }    // DropdownServiceModel.getCreateType
        public List<SelectListItem> ddl_Status { get; set; }        // DropdownServiceModel.getMasterStatus
        public List<SelectListItem> ddl_CtrlSystem { get; set; }    // DropdownServiceModel.getMaterialsCtrlSystem
        public List<SelectListItem> ddl_CtrlUnitPrice { get; set; }

        public string met_DivisionJSON { get; set; }
        public string met_MatTypeJSON { get; set; }
        public string met_FlaMatDeletionJSON { get; set; }

        public string met_CtrlSystemJSON { get; set; }
        public string met_CtrlUnitVolumeJSON { get; set; }
        public string met_CtrlUnitPriceJSON { get; set; }
    }

    public class MaterialsViewModel_Search
    {
        public string sNUM { get; set; }
        public string sDesEN { get; set; }
        public string sDivision { get; set; }
        public string sNamePerCre { get; set; }
        public string sMatType { get; set; }
        public string sMatGroup { get; set; }
        public string sFlaMatDeletion { get; set; }
        public string sCreateType { get; set; }
        public string sSystem { get; set; }
        public string sStatus { get; set; }
        public string sCreateDate { get; set; }
        public List<MaterialsViewModel_SearchData> sSearchData { get; set; }
    }

    public class MaterialsViewModel_SearchData
    {
        public string dCreateDate { get; set; }
        public string dNUM { get; set; }
        public string dDesEN { get; set; }
        public string dDivision { get; set; }
        public string dNamePerCre { get; set; }
        public string dMatType { get; set; }
        public string dMatGroup { get; set; }
        public string dFlaMatDeletion { get; set; }
        public string dCreateType { get; set; }
        public string dSystem { get; set; }
        public string dStatus { get; set; }
    }

    public class MaterialsViewModel_Detail
    {
        public string MET_CREATE_TYPE { get; set; }
        // Require
        public string MET_NUM { get; set; }             // Auto running
        public string MET_DIVISION { get; set; }
        public string MET_NAME_PER_CRE { get; set; }
        public string MET_MAT_TYPE { get; set; }
        public string MET_MAT_GROUP { get; set; }
        // Show
        public string MET_OLD_MAT_NUM { get; set; }
        public string MET_BASE_MEA { get; set; }
        public string MET_MAT_DES_THAI { get; set; }
        public string MET_MAT_DES_ENGLISH { get; set; }
        public string MET_CRUDE_TYPE { get; set; }
        public string MET_CRUDE_SRC { get; set; }
        public string MET_LOAD_PORT { get; set; }
        public string MET_ORIG_CRTY { get; set; }
        // Optional
        public string MET_CRE_DATE { get; set; }
        public string MET_DATE_LAST { get; set; }
        public string MET_LAB_DESIGN_OFFICE { get; set; }
        public string MET_FLA_MAT_DELETION { get; set; }
        public string MET_GRP_CODE { get; set; }
        public string MET_PRD_TYPE { get; set; }
        public string MET_BL_SRC { get; set; }
        public string MET_FK_PRD { get; set; }
        public string MET_DEN_CONV { get; set; }
        public string MET_VFC_CONV { get; set; }
        public string MET_IO { get; set; }
        public string MET_MAT_ADD { get; set; }
        public string MET_LORRY { get; set; }
        public string MET_SHIP { get; set; }
        public string MET_THAPP { get; set; }
        public string MET_PIPE { get; set; }
        public string MET_MAT_GROP_TYPE { get; set; }
        public string MET_ZONE { get; set; }
        public string MET_PORT { get; set; }
        public string MET_IO_TLB { get; set; }
        public string MET_IO_TPX { get; set; }
        public string MET_DOEB_CODE { get; set; }
        public string MET_IO_LABIX { get; set; }
        public string MET_EXCISE_CODE { get; set; }
        public string MET_EXCISE_UOM { get; set; }
        public string MET_PRODUCT_DENSITY { get; set; }
        public string MET_GL_ACCOUNT { get; set; }

        // Not show
        public string MET_CREATED_DATE { get; set; }
        public string MET_CREATED_BY { get; set; }
        public string MET_UPDATED_DATE { get; set; }
        public string MET_UPDATED_BY { get; set; }

        public List<MaterialsViewModel_Control> Control { get; set; }
    }

    public class MaterialsViewModel_Control
    {
        public string RowID { get; set; }
        public string System { get; set; }
        public string Name { get; set; }
        public string UnitVolume { get; set; }
        public string UnitPrice { get; set; }
        public string Status { get; set; }
    }
}