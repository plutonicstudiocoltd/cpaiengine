﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class MenuViewModel
    {
        public MenuViewModel_Detail menu_Detail { get; set; }

        public List<SelectListItem> ddl_ControlType { get; set; }
        public List<SelectListItem> ddl_Status { get; set; }
        public List<SelectListItem> ddl_ListNo { get; set; }

        public string menuJSON { set; get; }
    }

    public class MenuViewModel_Search
    {
        public string dMenuRowID { get; set; }
        public string dMenuGroupMenu { get; set; }
        public string dMenuParentID { get; set; }
        public string dDescription { get; set; }
        public string dMenuURL { get; set; }
        public string dMenuIMG { get; set; }
        public string dMenuLevel { get; set; }
        public string dMenuListNo { get; set; }
        public string dMenuControlType { get; set; }
        public string dActive { get; set; }
        public string dUrlDirect { get; set; }
        public string dParentDetail { get; set; }

        public List<MenuViewModel_SearchData> sSearchData { get; set; }
    }

    public class MenuViewModel_SearchData
    {
        public string dMenuRowID { get; set; }
        public string dMenuGroupMenu { get; set; }
        public string dMenuParentID { get; set; }
        public string dDescription { get; set; }
        public string dMenuURL { get; set; }
        public string dMenuIMG { get; set; }
        public string dMenuLevel { get; set; }
        public string dMenuListNo { get; set; }
        public string dMenuControlType { get; set; }
        public string dActive { get; set; }
        public string dUrlDirect { get; set; }
        public string dParentDetail { get; set; }

    }

    public class MenuViewModel_Detail
    {
        public string MEU_ROW_ID { get; set; }
        public string MEU_GROUP_MENU { get; set; }
        public string MEU_PARENT_ID { get; set; }
        public string LNG_DESCRIPTION { get; set; }
        public string MEU_URL { get; set; }
        public string MEU_IMG { get; set; }
        public string MEU_LEVEL { get; set; }
        public string MEU_LIST_NO { get; set; }
        public string MEU_CONTROL_TYPE { get; set; }
        public string MEU_ACTIVE { get; set; }
        public string MEU_URL_DIRECT { get; set; }
        public string Menu_ParentDetail { get; set; }

        public string MEU_PARENT_ID_CURRENT { get; set; }
        public string MEU_PARENT_DETAIL_CURRENT { get; set; }

        public List<MenuViewModel_Control> Control { get; set; }
    }

    public class MenuViewModel_Control
    {
        public string RowID { get; set; }
        public string Action { get; set; }

        public string GroupMenu { get; set; }
        public string ParentID { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Img { get; set; }
        public string Level { get; set; }
        public string ListNo { get; set; }
        public string ControlType { get; set; }
        public string Active { get; set; }
        public string UrlDirect { get; set; }
        public string ParentDetail { get; set; }

    }
}