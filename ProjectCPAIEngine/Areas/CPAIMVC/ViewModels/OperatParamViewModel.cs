﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class OperatParamViewModel
    {
        public OperatParamViewModel_Search OperaParam_Search { get; set; }
        public OperatParamViewModel_Detail OperatParam_Detail { get; set; }
        public List<SelectListItem> ddl_Unit { get; set; } //SelectList need for Dropdown
        public List<SelectListItem> ddl_Data_Operation { get; set; }
        public List<SelectListItem> ddl_Data_AndOr { get; set; }
    }

    public class OperatParamViewModel_Detail //Unit
    {
        public string RowId { get; set; }
        public string Unit { get; set; }
        public List<OperatParamViewModel_Param> Param { get; set; }
        public List<OperatParamViewModel_Condition> DataCondition { get; set; }


    }


    public class OperatParamViewModel_Search
    {
        public string sOperaParamCode { get; set; }
        public string sOperaParamUnit { get; set; }
        public List<OperatParamViewModel_SearchData> sSearchData { get; set; }
    }


    public class OperatParamViewModel_SearchData
    {
        public string dOperaParamCode { get; set; }
        public string dOperaParamUnit { get; set; }
        public string dOperaParamUnitId { get; set; }
        public DateTime? dOperaParamDate { get; set; }
    }

    public class OperatParamViewModel_Param //Param
    {
        public string Row_ID { get; set; }
        [Required]
        public string Order { get; set; }
        [Required]
        public string Parameter { get; set; }
        [Required]
        public string Factor { get; set; }
        public List<OperatParamViewModel_Data> DataKeyValue { get; set; }

    }

    public class OperatParamViewModel_Unit
    {
        public string Code { get; set; }
        public string Order { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public bool BoolStatus { get; set; }
        public string CreateType { get; set; }
        public string UserGroup { get; set; }
    }

    public class OperatParamViewModel_Data //Data
    {
        public string Row_Id { get; set; }
        public string MT_Oper_Param { get; set; }
        public string Score { get; set; }
        public List<KeyValue> KeyValue { get; set; }
        //public string Key { get; set; }
    }


    public class KeyValue
    {
        public string Key { get; set; }
    }

    public class OperatParamViewModel_Condition //Condition
    {
        public string Row_Id { get; set; }
        public string MT_Oper_Unit { get; set; }
        public string Key { get; set; }
        public string Row { get; set; }
        public string Column { get; set; }
        public objCondition ObjCondition { get; set; }
    }
    public class objCondition
    {
        public List<Condition> Condition { get; set; }
    }

    public class Condition
    {
        public string and_or { get; set; }
        public string operater_value { get; set; }
        public string value { get; set; }
    }
    }