﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class PurchaseEntryViewModel
    {
        public PurchaseEntryViewModel_Search PurchaseEntry_Search { get; set; }

        public List<SelectListItem> ddl_Company { get; set; }
        public List<SelectListItem> ddl_PoDate { get; set; }
        public List<SelectListItem> ddl_Product { get; set; }
        public List<SelectListItem> ddl_Supplier { get; set; }
    }

    public class PurchaseEntryViewModel_Search
    {
        public string sCompany { get; set; }
        public string sTripNo { get; set; }
        public string sPoDate { get; set; }
        public string sProduct { get; set; }
        public string sPoNo { get; set; }
        public string sGrDate { get; set; }
        public string sSupplier { get; set; }
        public List<PurchaseEntryViewModel_SearchData> sSearchData { get; set; }
    }
    
    public class PurchaseEntryViewModel_SearchData
    {
        public string TripNo { get; set; }
        public string PoNo { get; set; }
        public string PoItem { get; set; }
        public string PoDate { get; set; }
        public string Product { get; set; }
        public string Supplier { get; set; }
        public string SupplierNo { get; set; }
        public string Incoterms { get; set; }
        public string Volume { get; set; }
        public string VolumeUnit { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string Currency { get; set; }
        public string InvoiceFigure { get; set; }
        public string PriceUnit { get; set; }
        public bool SimulateROE { get; set; }
        public string SimulateDataName { get; set; }
        public string SimulateValue { get; set; }
        public string ExceptionMessage { get; set; }
        public string MetNum { get; set; }
        public string txtBlDate { get; set; }
        public string UpdatePrice { get; set; }
        public string LastUpdate { get; set; }
        public PurchaseEntryViewModel_PitPurchaseEntry PitPurchaseEntry { get; set; }
        public List<PurchaseEntryViewModel_Outtern> sOutturn { get; set; }
    }

    public class PurchaseEntryViewModel_Outtern
    {
        public string BLDate { get; set; }
        public string BlQtyBbl { get; set; }
        public string BlQtyMt { get; set; }
        public string BlQtyMl { get; set; }
        public string PostingDate { get; set; }
        public string ItemNo { get; set; }
        public string RunningDate { get; set; }
        public string QtyBbl { get; set; }
        public string QtyMt { get; set; }
        public string QtyMl { get; set; }
        public string Price { get; set; }
        public string ROE { get; set; }
        public string AmountUSD { get; set; }
        public string AmountTHB { get; set; }
        public string Vat7 { get; set; }
        public string TotalAmountUSD { get; set; }
        public string TotalAmountTHB { get; set; }
        public string SAPFIDoc { get; set; }
        public string SAPFIDocStatus { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public bool Select { get; set; }
        public bool Valid { get; set; }
    }

    public class PurchaseEntryViewModel_PitPurchaseEntry
    {
        public string CalVolumeUnit { get; set; }
        public string CalInvoiceFigure { get; set; }
        public string CalPriceUnit { get; set; }
    }

    public class PurchaseEntryViewModel_Test
    {
        public string PoNo { get; set; }
        public string PoDate { get; set; }
        public string BLDate { get; set; }
        public string BLVolumeBBL { get; set; }
    }
}