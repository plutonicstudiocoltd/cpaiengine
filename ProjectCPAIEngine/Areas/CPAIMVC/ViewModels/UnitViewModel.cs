﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectCPAIEngine.Areas.CPAIMVC.ViewModels
{
    public class UnitViewModel
    {
        public UnitViewModel_Search unit_Search { get; set; }
        public UnitViewModel_Detail unit_Detail { get; set; }

        public List<SelectListItem> ddl_UnitStatus { get; set; }
        public List<SelectListItem> ddl_UserGRoup { get; set; }
    }

    public class UnitViewModel_Search
    {
        public string sUnitCode { get; set; }
        public string sUnitName { get; set; }
        public string sUnitStatus { get; set; }
        
        public List<UnitViewModel_SeachData> sSearchData { get; set; }
    }

    public class UnitViewModel_Detail
    {
        public string UnitCode { get; set; }
        public string Order { get; set; }
        public string UnitName { get; set; }
        public string UnitStatus { get; set; }
        public string UnitProcessConfirm { get; set; }
        public bool BoolStatus { get; set; }
        public bool BoolProcessConfrim { get; set; }
        public string CreateType { get; set; }
        public string UserGroup { get; set; }
        public string AreaName { get; set; }

       
    }

    public class UnitViewModel_SeachData
    {
        public string dUnitCode { get; set; }
        public string dUnitName { get; set; }
        public string dUnitStatus { get; set; }
    }

   

}