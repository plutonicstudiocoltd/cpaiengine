﻿using com.pttict.engine.utility;
using ProjectCPAIEngine.Areas.CPAIMVC.Controllers;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Batches
{
    public class CrudePurchaseBondRequestStatusUpdate: IJob
    {
        void IJob.Execute(IJobExecutionContext context)
        {
            this.run();
        }

        private void run2() {
            System.Diagnostics.Debug.Print("Schedule run!");
        }

        private ResponseData LoadDataCrudePurchase(string TransactionID, ref CrudePurchaseViewModel model)
        {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000005;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            if (Const.User.UserGroup != null)
            {
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            }
            else
            {
                //create user job
                req.Req_parameters.P.Add(new P { K = "user", V = "ANAWAT" });
            }
            req.Req_parameters.P.Add(new P { K = "transaction_id", V = TransactionID });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
            req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
            req.Extra_xml = "";

            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;

            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
            if (_model.attachitems != "[\"\"]" && _model.attachitems != null)
            {
                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(_model.attachitems);
                if (Att.attach_items.Count > 0)
                {
                    foreach (string _item in Att.attach_items)
                    {
                        //ViewBag.hdfFileUpload += _item + "|";
                    }
                }
            }
            if (_model.data_detail != null)
            {
                model = new JavaScriptSerializer().Deserialize<CrudePurchaseViewModel>(_model.data_detail);
                if (!string.IsNullOrEmpty(model.contract_period.date_from) && !string.IsNullOrEmpty(model.contract_period.date_to))
                {
                    model.contract = model.contract_period.date_from + " to " + model.contract_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.loading_period.date_from) && !string.IsNullOrEmpty(model.loading_period.date_to))
                {
                    model.loading = model.loading_period.date_from + " to " + model.loading_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.discharging_period.date_from) && !string.IsNullOrEmpty(model.discharging_period.date_to))
                {
                    model.discharging = model.discharging_period.date_from + " to " + model.discharging_period.date_to;
                }
                if (!string.IsNullOrEmpty(model.proposal.price_period_from) && !string.IsNullOrEmpty(model.proposal.price_period_to))
                {
                    model.price_period = model.proposal.price_period_from + " to " + model.proposal.price_period_to;
                }
                if (!string.IsNullOrEmpty(model.payment_terms.sub_payment_term))
                {
                    model.nrdoDay = model.payment_terms.sub_payment_term;
                }
                if (!string.IsNullOrEmpty(model.requested_by))
                {
                    model.requested_by = model.requested_by;
                }
            }

            return resData;
        }

        private void run()
        {
            using (var context = new EntityCPAIEngine()) {
                DateTime tToday = DateTime.Today;
                List<VW_CRUDE_TRANS> crudepurchases = context.VW_CRUDE_TRANS.Where(a=> a.STATUS.Equals(CPAIConstantUtil.STATUS_WAITING_BOND_DOC)
                    ).ToList<VW_CRUDE_TRANS>();
                //crudepurchases = crudepurchases.Where(a => Convert.ToDateTime(a.BL_DATE) < tToday.Date).ToList(); // <= REAL CODE
                crudepurchases = crudepurchases.Where(a => Convert.ToDateTime(a.PURCHASE_DATE) < tToday.Date).ToList(); // <= FOR-DEV CODE
                foreach (VW_CRUDE_TRANS cp in crudepurchases) {

                    CrudePurchaseViewModel model = new CrudePurchaseViewModel();
                    ResponseData rspModelData = LoadDataCrudePurchase(cp.CDA_ROW_ID, ref model);

                    CdpRootObject obj = new CdpRootObject();
                    obj.approve_items = model.approve_items;
                    obj.compet_offers_items = model.compet_offers_items;
                    obj.contract_period = model.contract_period;
                    obj.crude_id = model.crude_id;
                    obj.crude_name = model.crude_name;
                    obj.crude_name_others = model.crude_name_others;
                    obj.date_purchase = model.date_purchase;
                    obj.plan_month = model.plan_month;
                    obj.plan_year = model.plan_year;
                    obj.discharging_period = model.discharging_period;
                    obj.explanation = model.explanation;
                    obj.explanationAttach = model.explanationAttach;
                    obj.feedstock = model.feedstock;
                    obj.feedstock_others = model.feedstock_others;
                    obj.for_feedStock = model.for_feedstock;
                    obj.loading_period = model.loading_period;
                    obj.notes = model.notes;
                    obj.offers_items = model.offers_items;
                    obj.origin = model.origin;
                    obj.origin_id = model.origin_id;
                    obj.other_condition = model.other_condition;
                    obj.payment_terms = model.payment_terms;
                    obj.proposal = model.proposal;
                    obj.purchase = model.purchase;
                    obj.reason = model.reason;
                    obj.term = model.term;
                    obj.term_others = model.term_others;
                    obj.gtc = model.gtc;
                    obj.bond_documents = model.bond_documents;
                    obj.performance_bond = model.performance_bond;

                    var json = new JavaScriptSerializer().Serialize(obj);

                    string tReqTranId = new FunctionTransactionDAL().GetReqTransIDByTransactionId(cp.CDA_ROW_ID);

                    RequestData req = new RequestData();
                    req.function_id = ConstantPrm.FUNCTION.F10000023;
                    req.app_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.req_transaction_id = tReqTranId;

                    req.state_name = "CPAIVerifyCDPHRequireInputContinueState";
                    req.req_parameters = new req_parameters();
                    req.req_parameters.p = new List<p>();
                    req.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
                    req.req_parameters.p.Add(new p { k = "data_detail_input", v = json });
                    req.req_parameters.p.Add(new p { k = "current_action", v = "WAIVE" });
                    req.req_parameters.p.Add(new p { k = "next_status", v = "WAIVED" });
                    //req.req_parameters.p.Add(new p { k = "user", v = "ANAWAT" });
                    if (Const.User.UserGroup != null)
                    {
                        req.req_parameters.p.Add(new p { k = "user", v = Const.User.UserName });
                    }
                    else
                    {
                        //create user job
                        req.req_parameters.p.Add(new p { k = "user", v = "ANAWAT" });
                    }
                    req.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CRUDE_PURCHASE });
                    req.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
                    req.extra_xml = "";

                    ResponseData resData = new ResponseData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    resData = service.CallService(req);
                    //System.Diagnostics.Debug.Print(cp.CDA_ROW_ID);
                }
            }
        }
    }

}