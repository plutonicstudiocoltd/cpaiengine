﻿
//Re-Create for on page postbacks


function initValidate() {

    $('.validate-message-alert').each(function () {
        var parent = $(this).parent();
        var icon = parent.find('i.icon-error');
        var pWidth = parent.width();
        var iPosition = icon.position();
        //console.log(pWidth);
        //console.log(iPosition.left);

        $(this).css('left', (iPosition.left - $(this).width() - 13) + 'px');
        $(this).css('margin-top', '-' + ($(this).height() + 55) + 'px');
    });


    $('i.icon-error').mouseover(function () {
        var parent = $(this).parent();
        var alert = parent.find('.validate-message-alert');
        alert.show();
    });
    $('i.icon-error').mouseout(function () {
        var parent = $(this).parent();
        var alert = parent.find('.validate-message-alert');
        alert.hide();
    });

}

$(document).mouseup(function (e) {
    var container = $(".validate-message-alert");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
});

function autoValidateRequiredField() {

    $('.require').focusout(function () {
        var inputVal = $(this).val();
        if (inputVal === '') {
            addValidMessageError($(this), 'Input is required')
        } else {
            addValidMessageSuccess($(this));
        }
    });

}

function addValidMessageError(element, text) {
    var parent = element.parent();
    parent.find('.icon-error').remove();
    parent.find('.icon-success').remove();
    parent.find('.validate-message-alert').remove();
    //var html = generateValidateErrorAlert(text);
    //element.after(html);
    initValidate();
    element.removeClass('input-error');
    element.addClass('input-error');
}
function generateValidateErrorAlert(text) {
    var html = '';

    html += '<i class="fa fa-exclamation-circle icon-error"></i>';
    html += '<div class="validate-message-alert validate-message-error" style="z-index:9999;">';
    html += '   <i class="fa fa-sort-down"></i>';
    html += '       ' + text;
    html += '</div>';

    return html;
}


function addValidMessageSuccess(element) {
    var parent = element.parent();
    parent.find('.icon-error').remove();
    parent.find('.icon-success').remove();
    parent.find('.validate-message-alert').remove();
    element.removeClass('input-error');
    element.after(generateValidateSuccess());
}
function generateValidateSuccess() {
    return '<i class="fa fa-check icon-success"></i>';
}


function validateBeforeSave(e, btnSave, className) {
    var flag = true;
    var _parent = $(e).closest("." + className)[0].id;
    $("#" + _parent + " .require").each(function () { $(this).trigger('focusout'); });
    $("#" + _parent + " .input-error").each(function () { flag = false; });


    return flag;
}

$(document).ready(function () {
    initRequiredField();
    autoValidateRequiredField();


    //$('.inputInteger').number(true, 0);
    //$('.inputNumeric2Digit').number(true, 2);

    $(".numeric").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

});

function initRequiredField() {
    var td = $('.td-required');
    td.append(generateRequiredFieldHtmlString());
}
function generateRequiredFieldHtmlString() {
    var html = '';

    html += '<label class="required">*</label>';

    return html;
}

function CheckLength(Ctrl, lengthCtrl) {
    $('#' + Ctrl).keypress(function () {
        if ($(this).val().length >= lengthCtrl) {
            return false;
        }
        else {
            return true;
        }
    });
    $('#' + Ctrl).focusout(function () {
        if ($(this).val().length > lengthCtrl) {
            $(this).val($(this).val().substring(0, lengthCtrl))
        }
    });
}

function CheckDateFormat(Ctrl) {
    $('#' + Ctrl).focusout(function () {
        debugger;
        if (moment($(this).val(), 'DD/MM/YYYY', true).isValid() == false) {
            $(this).val("");
        }
    });
}

function CheckDateFormatDT(Ctrl) {
    $('#' + Ctrl).focusout(function () {
        debugger;
        if (moment($(this).val(), 'DD/MM/YYYY HH:mm', true).isValid() == false) {
            $(this).val("");
        }
    });
}

function CheckDateFormatFT(Ctrl) {
    $('#' + Ctrl).focusout(function () {
        if (moment($(this).val(), 'DD/MM/YYYY to DD/MM/YYYY', true).isValid() == false) {
            $(this).val("");
        }
    });
}