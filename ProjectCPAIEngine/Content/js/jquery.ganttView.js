/*
jQuery.ganttView v.0.8.8
Copyright (c) 2010 JC Grubbs - jc.grubbs@devmynd.com
MIT License Applies
*/

/*
Options
-----------------
showWeekends: boolean
data: object
cellWidth: number
cellHeight: number
start: date
slideWidth: number
dataUrl: string
behavior: {
	clickable: boolean,
	draggable: boolean,
	resizable: boolean,
	hover: boolean,
	onClick: function,
	onDrag: function,
	onResize: function,
	onHover: function,
	onLeave: function
}
*/

(function (jQuery) {
	
    jQuery.fn.ganttView = function () {
    	
    	var args = Array.prototype.slice.call(arguments);
    	
    	if (args.length == 1 && typeof(args[0]) == "object") {
        	build.call(this, args[0]);
    	}
    	
    	if (args.length == 2 && typeof(args[0]) == "string") {
    		handleMethod.call(this, args[0], args[1]);
    	}
    };
    
    function build(options) {
    	
    	var els = this;
        var defaults = {
            showWeekends: true,
            cellWidth: 33,
            cellHeight: 58,
            slideWidth: 880,
            vHeaderWidth: 100,
            behavior: {
            	clickable: true,
            	draggable: true,
            	resizable: true,
				hover: true
            }
        };
		
        var opts = jQuery.extend(true, defaults, options);

		if (opts.data) {
			build();
		} else if (opts.dataUrl) {
			jQuery.getJSON(opts.dataUrl, function (data) { opts.data = data; build(); });
		}

		function build() {

		    // Edit by aekkasit : 30/11/2016 : set end date
		    //var minDays = Math.floor((opts.slideWidth / opts.cellWidth) + 30);
		    //var minDays = Math.floor((opts.slideWidth / opts.cellWidth));
	        //var startEnd = DateUtils.getBoundaryDatesFromData(opts.data, minDays);
	        //if (!opts.start) { opts.start = startEnd[0]; }

	        //var startYYYYMM = opts.start.getYear().toString() + opts.start.getMonth().toString();
	        //var endYYYYMM = opts.end.getYear().toString() + opts.end.getMonth().toString();
	        //console.log(startYYYYMM);
		    //console.log(endYYYYMM);

		    //console.log("opts.start" + opts.start);
		    //console.log("opts.end" + opts.end);

		    var sDate = new Date(opts.start);
		    //console.log("sDate" + sDate);
		    var eDate = new Date(opts.end);
		    //console.log("eDate" + eDate);

		    var startYYYYMM = moment(sDate).format('YYYYMM');
		    //console.log("startYYYYMM" + startYYYYMM);
		    var endYYYYMM = moment(eDate).format('YYYYMM');
		    //console.log("endYYYYMM" + endYYYYMM);

	        if (startYYYYMM == endYYYYMM)
	        {
	            var y = sDate.getFullYear(), m = sDate.getMonth();
	            var firstDay = new Date(y, m, 1);
	            var lastDay = new Date(y, m + 1, 0);

	            opts.start = firstDay
	            //console.log("opts.start" + opts.start);
	            opts.end = lastDay
	            //console.log("opts.end" + opts.end);
	        }
	       

	        els.each(function () {

	            var container = jQuery(this);
	            var div = jQuery("<div>", { "class": "ganttview" });
	            new Chart(div, opts).render();
				container.append(div);
				
	            // Edit by aekkasit : 03/02/2017 : set scroll to current month
				var gridWidth = jQuery("div.ganttview-grid", container).outerWidth();
				var scrollCenter = 0;
				var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
	            	jQuery("div.ganttview-slide-container", container).animate({ 'scrollLeft': scrollCenter }, 2000).outerWidth();

				//console.log("outerWidth vtheader : " + jQuery("div.ganttview-vtheader", container).outerWidth());
				//console.log("outerWidth container : " + jQuery("div.ganttview-slide-container", container).outerWidth());
				//console.log("outerWidth grid : " + jQuery("div.ganttview-grid", container).outerWidth());
				
				//console.log("w : " + w);

                // Edit by aekkasit : 30/11/2016 : set scroll to left
				//var w = jQuery("div.ganttview-vtheader", container).outerWidth() +
				//	jQuery("div.ganttview-slide-container", container).animate({ 'scrollLeft': 0 }, 0).outerWidth();
	            container.css("width", (w + 2) + "px");
	            
	            new Behavior(container, opts).apply();
	        });
		}
    }

	function handleMethod(method, value) {
		
		if (method == "setSlideWidth") {
			var div = $("div.ganttview", this);
			div.each(function () {
				var vtWidth = $("div.ganttview-vtheader", div).outerWidth();
				$(div).width(vtWidth + value + 1);
				//$("div.ganttview-slide-container", this).width(value);
			});
		}
	}

	var Chart = function(div, opts) {
	    console.log(div, opts);
		function render() {
			addVtHeader(div, opts.data, opts.cellHeight);

		    // Edit by aekkasit 01/12/2016 : solve Scrollbar right Set width gantt 
            var slideDiv = jQuery("<div>", {
                "class": "ganttview-slide-container",
                "css": { "width": opts.slideWidth + "px" }
		    });

			//var slideDiv = jQuery("<div>", {
			//    "class": "ganttview-slide-container",
			//    "css": { "width": "880" + "px" }        // Fix width becuase right scroll bar hide
			//});

			
            dates = getDates(opts.start, opts.end);
            addHzHeader(slideDiv, dates, opts.cellWidth);
            addGrid(slideDiv, opts.data, dates, opts.cellWidth, opts.showWeekends);
            addBlockContainers(slideDiv, opts.data);
            addBlocks(slideDiv, opts.data, opts.cellWidth, opts.start);
            div.append(slideDiv);
            applyLastClass(div);
		}
		
		var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

		// Creates a 3 dimensional array [year][month][day] of every day 
		// between the given start and end dates
        function getDates(start, end) {
            var dates = [];
			dates[start.getFullYear()] = [];
			dates[start.getFullYear()][start.getMonth()] = [start]
			var last = start;
			while (last.compareTo(end) == -1) {
				var next = last.clone().addDays(1);
				if (!dates[next.getFullYear()]) { dates[next.getFullYear()] = []; }
				if (!dates[next.getFullYear()][next.getMonth()]) { 
					dates[next.getFullYear()][next.getMonth()] = []; 
				}
				dates[next.getFullYear()][next.getMonth()].push(next);
				last = next;
			}
			return dates;
        }

        function addVtHeader(div, data, cellHeight) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-vtheader" });
            for (var i = 0; i < data.length; i++) {
                var itemDiv = jQuery("<div>", { "class": "ganttview-vtheader-item" });				
                itemDiv.append(jQuery("<div>", {
				    "id" : "ganttview-header-"+ data[i].id,
                    "class": "ganttview-vtheader-item-name",
                    "css": { "min-height": (data[i].series.length * cellHeight) + "px" }
                }).append(data[i].name));
                var seriesDiv = jQuery("<div>", { "class": "ganttview-vtheader-series" });
                for (var j = 0; j < data[i].series.length; j++) {	
					//for (var k = 0; k < data[i].series[j].activities.length; k++) {					
						seriesDiv.append(jQuery("<div>", 
						{ "class": "ganttview-vtheader-series-name",
						  "css": { "min-height": (data[i].series[j].activities.length * cellHeight) + "px" }
						  }
						)
							.append(data[i].series[j].name));
					//}
                }
                itemDiv.append(seriesDiv);
                headerDiv.append(itemDiv);
            }
            div.append(headerDiv);
        }

        function addHzHeader(div, dates, cellWidth) {
            var headerDiv = jQuery("<div>", { "class": "ganttview-hzheader" });
            var monthsDiv = jQuery("<div>", { "class": "ganttview-hzheader-months" });
            var daysDiv = jQuery("<div>", { "class": "ganttview-hzheader-days" });
            var totalW = 0;
			for (var y in dates) {
				for (var m in dates[y]) {
					var w = dates[y][m].length * cellWidth;
					totalW = totalW + w;
					monthsDiv.append(jQuery("<div>", {
						"class": "ganttview-hzheader-month",
						"id": monthNames[m],
						"css": { "width": (w - 1) + "px", "font-size": 24 + "px" }
					}).append(monthNames[m] + " " + y));
					for (var d in dates[y][m]) {
						daysDiv.append(jQuery("<div>", { "class": "ganttview-hzheader-day", "id": monthNames[m] + dates[y][m][d].getDate(), })
							.append(dates[y][m][d].getDate()));
					}
				}
			}
            monthsDiv.css("width", totalW + "px");
            daysDiv.css("width", totalW + "px");
            headerDiv.append(monthsDiv).append(daysDiv);
            div.append(headerDiv);
        }

        function addGrid(div, data, dates, cellWidth, showWeekends) {
            var gridDiv = jQuery("<div>", { "class": "ganttview-grid" });
            var rowDiv = jQuery("<div>", { "class": "ganttview-grid-row" });
			for (var y in dates) {
				for (var m in dates[y]) {
					for (var d in dates[y][m]) {
						var cellDiv = jQuery("<div>", { "class": "ganttview-grid-row-cell" });
						if (DateUtils.isWeekend(dates[y][m][d]) && showWeekends) { 
							cellDiv.addClass("ganttview-weekend"); 
						}
						rowDiv.append(cellDiv);
					}
				}
			}
            var w = jQuery("div.ganttview-grid-row-cell", rowDiv).length * cellWidth;
            rowDiv.css("width", w + "px");
            gridDiv.css("width", w + "px");
            //for (var i = 0; i < data.length; i++) {
            //    for (var j = 0; j < data[i].series.length; j++) {
			//		for (var k = 0; k < data[i].series[j].activities.length; k++) {
            //           gridDiv.append(rowDiv.clone());
			//		}
            //    }
            //}
            for (var k = 0; k < 6; k++) {
                gridDiv.append(rowDiv.clone());
            }
            div.append(gridDiv);
        }

        // Draw Schedule Timeline
        function addBlockContainers(div, data) {
            var blocksDiv = jQuery("<div>", { "class": "ganttview-blocks" });
            for (var i = 0; i < data.length; i++) {
                for (var j = 0; j < data[i].series.length; j++) {
                    //var seriesBlock = jQuery("<div>", { "class": "ganttview-series-container" }).appendTo(blocksDiv);
                    //for (var k = 0; k < data[i].series[j].activities.length; k++) {
                        // Edit by aekkasit : 15/11/2016 : Edit Schedule of vessel disply wrong
                        var seriesBlock = jQuery("<div>", { "class": "ganttview-series-container" }).appendTo(blocksDiv);
						$(seriesBlock).append(jQuery("<div>", { "class": "ganttview-block-container" }));
					//}
                }
            }
            div.append(blocksDiv);
        }

        function addBlocks(div, data, cellWidth, start) {
            var rows = jQuery("div.ganttview-blocks div.ganttview-block-container", div);
            var rowIdx = 0;
            // For Loop Vessel
            for (var i = 0; i < data.length; i++) {
                // For Loop series
                for (var j = 0; j < data[i].series.length; j++) {

                    var series = data[i].series[j];

                    // For Loop Activities
                    for (var k = 0; k < series.activities.length; k++) {
                        
                        var activity = data[i].series[j].activities[k];

                        if (activity.start != "" && activity.end != "" && Date.parse(activity.end) >= start) {

                       
                            var series_start = Date.parse(activity.start);
                            var series_end = Date.parse(activity.end);

                            // Edit by aekkasit 01/12/2016 : if select startdate frist column display wrong
                            //var size = DateUtils.daysBetween(activity.start, activity.end) + 1;
                            //if (series_start >= start ) {
                            //  var offset = DateUtils.daysBetween(start, series_start);
                            //} else {
                            //  var offset = -(DateUtils.daysBetween(series_start, start));
                            //}

                            //console.log("series_start : " + series_start);
                            //console.log("start : " + start.toDateString());
                            //console.log("series_end : " + series_end);
                            //console.log("end : " + opts.end.toDateString());

                            if (series_start.toDateString() == start.toDateString()) {
                                if (series_end.toDateString() == opts.end.toDateString())
                                {
                                    var size = DateUtils.daysBetween(activity.start, activity.end) + 1;
                                }
                                else if (series_end.toDateString() < opts.end.toDateString()) {
                                    var size = DateUtils.daysBetween(activity.start, activity.end) + 1;
                                }
                                else {
                                    var size = DateUtils.daysBetween(activity.start, activity.end) + 2;
                                }
                                
                                //console.log( "2" );
                            } else {
                                var size = DateUtils.daysBetween(activity.start, activity.end) + 1;
                                //console.log("1");
                            }
                            
                            if (series_start.toDateString() == start.toDateString()) {
                                var offset = DateUtils.daysBetween(start, series_start);
                            }else if (series_start >= start) {
                                var offset = DateUtils.daysBetween(start, series_start);
                            } else {
                                var offset = -(DateUtils.daysBetween(series_start, start));
                            }

						
                            var strTitle = activity.title.split('<p style=\"display: none;\">')[1].split('</p>')[0];
						  
						  var block = jQuery("<div>", {
							  "class": "tooltip ganttview-block",
							  "id": "ganttview-blocksss",
						      //"title": activity.tooltip + ", " + size + " days",
							  "title": strTitle,
							  "css": {
								  "width": ((size * cellWidth) - 9) + "px",
								  "margin-left": ((offset * cellWidth) + 2) + "px",
								  "margin-top": 30 + "px"           //"margin-top": 20 + "px"
							  }
						  });
						  
						  if (activity.color)
						     block.css("background-color", activity.color);
							 
						  addBlockData(block, data[i], series, activity);
						  if (data[i].series[j].color) {
							  block.css("background-color", data[i].series[j].color);
						  }
											 
						  if (activity.title)
							  block.append(jQuery("<div>", { "class": "ganttview-block-text" }).html(activity.title));
						  else
						      block.append(jQuery("<div>", { "class": "ganttview-block-text" }).text(size));
							  
						  jQuery(rows[rowIdx]).append(block);
                        }
                    }
                    rowIdx += 1;
                }
            }
        }
        
        function addBlockData(block, data, series, activity) {
        	// This allows custom attributes to be added to the series data objects
        	// and makes them available to the 'data' argument of click, resize, and drag handlers
        	var blockData = { id: data.id, name: data.name };
        	jQuery.extend(blockData, series);
			jQuery.extend(blockData, activity);
        	block.data("block-data", blockData);
        }

        function applyLastClass(div) {
            jQuery("div.ganttview-grid-row div.ganttview-grid-row-cell:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-days div.ganttview-hzheader-day:last-child", div).addClass("last");
            jQuery("div.ganttview-hzheader-months div.ganttview-hzheader-month:last-child", div).addClass("last");
        }
		
		return {
			render: render
		};
	}

	var Behavior = function (div, opts) {
		
		function apply() {
			
			if (opts.behavior.clickable) { 
            	bindBlockClick(div, opts.behavior.onClick); 
        	}
        	
            if (opts.behavior.resizable) { 
            	bindBlockResize(div, opts.cellWidth, opts.start, opts.behavior.onResize); 
        	}
            
            if (opts.behavior.draggable) { 
            	bindBlockDrag(div, opts.cellWidth, opts.start, opts.behavior.onDrag); 
        	}
			
			 if (opts.behavior.hover) { 
            	bindHover(div, opts.behavior.onHover, opts.behavior.onLeave); 
        	}
		}

        function bindBlockClick(div, callback) {
            jQuery("div.ganttview-block", div).on("click", function () {
                if (callback) { callback(jQuery(this).data("block-data")); }
            });
        }
		
		function bindHover(div, callbackHover, callbackLeave) {
            jQuery("div.ganttview-block", div).on("mouseover mouseout", function(event) {
			  if ( event.type == "mouseover" ) {
					if (callbackHover) { callbackHover(jQuery(this), jQuery(this).data("block-data")); }
			  } else {
					if (callbackLeave) { callbackLeave(jQuery(this), jQuery(this).data("block-data")); }
			  }
			});
        }
        
        function bindBlockResize(div, cellWidth, startDate, callback) {
        	jQuery("div.ganttview-block", div).resizable({
        		grid: cellWidth, 
        		handles: "e,w",
        		stop: function () {
        			var block = jQuery(this);
        			updateDataAndPosition(div, block, cellWidth, startDate);
        			if (callback) { callback(block.data("block-data")); }
        		}
        	});
        }
        
        function bindBlockDrag(div, cellWidth, startDate, callback) {
        	jQuery("div.ganttview-block", div).draggable({
        		axis: "x", 
        		grid: [cellWidth, cellWidth],
        		stop: function () {
        			var block = jQuery(this);
        			updateDataAndPosition(div, block, cellWidth, startDate);
        			if (callback) { callback(block.data("block-data")); }
        		}
        	});
        }
        
        function updateDataAndPosition(div, block, cellWidth, startDate) {
        	var container = jQuery("div.ganttview-slide-container", div);
        	var scroll = container.scrollLeft(0);
            var offset = block.offset().left - container.offset().left - 1 + scroll;
        	
			// Set new start date
			var daysFromStart = Math.round(offset / cellWidth);
			var newStart = startDate.clone().addDays(daysFromStart);
			block.data("block-data").start = newStart;

			// Set new end date
        	var width = block.outerWidth();
			var numberOfDays = Math.round(width / cellWidth) - 1;
			block.data("block-data").end = newStart.clone().addDays(numberOfDays);
			jQuery("div.ganttview-block-text", block).text(numberOfDays + 1);
			
			// Remove top and left properties to avoid incorrect block positioning,
        	// set position to relative to keep blocks relative to scrollbar when scrolling
			block.css("top", "").css("left", "")
				//.css("position", "relative")
				.css("margin-left", offset + "px");
        }
        
        return {
        	apply: apply	
        };
	}

    var ArrayUtils = {
	
        contains: function (arr, obj) {
            var has = false;
            for (var i = 0; i < arr.length; i++) { if (arr[i] == obj) { has = true; } }
            return has;
        }
    };

    var DateUtils = {
    	
        daysBetween: function (start, end) {
            if (!start || !end) { return 0; }
            start = Date.parse(start); end = Date.parse(end);
            if (start.getYear() == 1901 || end.getYear() == 8099) { return 0; }
            var count = 0, date = start.clone();
            while (date.compareTo(end) == -1) { count = count + 1; date.addDays(1); }
            return count;
        },
        
        isWeekend: function (date) {
            return date.getDay() % 6 == 0;
        },

		getBoundaryDatesFromData: function (data, minDays) {
			var minStart = new Date(); maxEnd = new Date();
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < data[i].series.length; j++) {
					for (var k = 0; k < data[i].series[j].activities.length; k++) {								
						if (data[i].series[j].activities[k].start != "" && data[i].series[j].activities[k].end != "") {
							var start = Date.parse(data[i].series[j].activities[k].start);
							var end = Date.parse(data[i].series[j].activities[k].end);
							if (i == 0 && j == 0) { minStart = start; maxEnd = end; }
							if (minStart.compareTo(start) == 1) { minStart = start; }
							if (maxEnd.compareTo(end) == -1) { maxEnd = end; }
						}
					}
				}
			}
			
			// Insure that the width of the chart is at least the slide width to avoid empty
			// whitespace to the right of the grid
			if (DateUtils.daysBetween(minStart, maxEnd) < minDays) {
				maxEnd = minStart.clone().addDays(minDays);
			}
			//console.log([minStart, maxEnd]);
			return [minStart, maxEnd];
		},
		
		doesOverlap: function(e1, e2) {
			  var e1start = e1.start.getTime();
			  var e1end = e1.end.getTime();
			  var e2start = e2.start.getTime();
			  var e2end = e2.end.getTime();

			  return (e1start > e2start && e1start < e2end || 
				  e2start > e1start && e2start < e1end)
		},
		
		getOverlaps: function(dates)
		{
		    var overlaps = 0;
        
			// sort the events by their start time
			 dates.sort(function(eventOne, eventTwo){
			      return eventOne.start - eventTwo.start;
			 });
			
			for (var i = dates.length - 1; i >= 0; i--){
					
				var dateone = dates[i];
				var datetwo = dates[i+1]
					
				if (doesOverlap(dateone, datetwo))
       				overlaps++;
			};
			
			return overlaps;
		}
    };

})(jQuery);