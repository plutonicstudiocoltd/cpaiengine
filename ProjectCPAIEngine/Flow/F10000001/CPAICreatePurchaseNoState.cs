﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000001
{
    public class CPACreatePurchaseNoState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPACreatePurchaseNoState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string seq = MasterData.GetSeqBunker();
                string dateString = DateTime.Now.ToString("yyMM");
                string pn = "BNK-" + dateString + "-" + seq;

                ExtendValue extPurchaseNumber = new ExtendValue();
                extPurchaseNumber.value = pn;
                //purchase no.
                etxValue.Add(CPAIConstantUtil.PurchaseNumber, extPurchaseNumber);
                etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PurchaseNumber, extPurchaseNumber);
                //create by
                ExtendValue extUser = new ExtendValue();
                extUser.value = etxValue.GetValue(CPAIConstantUtil.User);
                etxValue.Add(CPAIConstantUtil.CreateBy, extUser);


                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPACreatePurchaseNoState # :: Code >>> " + currentCode);
              
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPACreatePurchaseNoState # :: Exception >>> " + ex);
                log.Error("CPACreatePurchaseNoState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
