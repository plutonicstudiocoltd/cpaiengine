﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using System.DirectoryServices;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000003
{
    public class CPAIVerifyLoginRequireInputState : BasicBean,StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyLoginRequireInputState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserId)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_ID_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserPassword)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_PASSWORD_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserSystem)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_SYSTEM_RESP_CODE;
                //optional input comment by poo
                //else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserOS)))
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_OS_RESP_CODE;
                //else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserNotificationId)))
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_NOTI_ID_RESP_CODE;
                else
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //get ip address
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyLoginRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVerifyLoginRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyLoginRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
