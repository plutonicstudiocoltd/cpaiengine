﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.DAL.DALTce;

namespace ProjectCPAIEngine.Flow.F10000006
{
    public class CPAICheckTCEWSCancelState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICheckTCEWSCancelState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                //string p_no = stateModel.EngineModel.searchIndex8;
                //set tce_action of send email
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //string p_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                string chi_txn_id = stateModel.EngineModel.ftxTransId;
                string tce_action = CPAIConstantUtil.ACTION_FAIL_2;
                string tce_reason = "System Error";
                ExtendValue extTemp = new ExtendValue { value = tce_action };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                }
                ExtendValue extReason = new ExtendValue { value = tce_reason };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                }


                //check tce ws is exist
                if (CPAI_TCE_WS_DAL.isDuplicate(chi_txn_id, CPAIConstantUtil.ACTION_SUBMIT))
                {
                    tce_reason = "";
                    tce_action = CPAIConstantUtil.ACTION_CANCEL;
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    tce_reason = "";
                    tce_action = CPAIConstantUtil.ACTION_FAIL_2;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_TCE_WS_RESP_CODE;
                }

                extTemp = new ExtendValue { value = tce_action };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Action, extTemp);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Action] = extTemp;
                }
                extReason = new ExtendValue { value = tce_reason };
                try
                {
                    etxValue.Add(CPAIConstantUtil.TCE_Reason, extReason);
                }
                catch (ArgumentException)
                {
                    etxValue[CPAIConstantUtil.TCE_Reason] = extReason;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICheckTCEWSCancelState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICheckTCEWSCancelState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}