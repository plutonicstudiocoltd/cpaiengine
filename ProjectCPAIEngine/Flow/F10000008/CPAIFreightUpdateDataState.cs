﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALFreight;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000008
{
    public class CPAIFreightUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIFreightUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    FclRootObject dataDetail = JSonConvertUtil.jsonToModel<FclRootObject>(item);
                  
                    //if (nextStatus.Equals(CPAIConstantUtil.STATUS_DRAFT) || 
                    //    (dataDetail.fcl_vessel_particular.vessel_name != null && 
                    //    dataDetail.fcl_vessel_particular.date_time != null && 
                    //    dataDetail.fcl_payment_terms.charterer != null && 
                    //    dataDetail.fcl_payment_terms.cargo != null && 
                    //    dataDetail.fcl_payment_terms.broker != null))
                    //{
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

                        #region add data history
                        //add data history
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Insert Update DB
                        bool isRunDB = false;
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
                        {
                            //DRAFT or SUBMIT ==> DELETE , INSERT
                            isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail);
                        }
                        else
                        {
                            //OTHER ==> UPDATE
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
                        }
                        #endregion

                        #region INSERT JSON
                        if (isRunDB)
                        {
                            //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                            if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals("-"))
                            {
                                string dataDetailString = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                                string sVessel = dataDetail.fcl_vessel_particular.vessel_name;
                                string sDateTime = dataDetail.fcl_vessel_particular.date_time;
                                string sCharterer = dataDetail.fcl_payment_terms.charterer;
                                //string sCargo = dataDetail.fcl_payment_terms.cargo;
                                string sBroker = dataDetail.fcl_payment_terms.broker;
                                string laycan_from = dataDetail.fcl_payment_terms.laycan_from != null ? dataDetail.fcl_payment_terms.laycan_from : "";
                                string laycan_to = dataDetail.fcl_payment_terms.laycan_to != null ? dataDetail.fcl_payment_terms.laycan_to : "";
                                string laytime = dataDetail.fcl_payment_terms.laytime != null ? dataDetail.fcl_payment_terms.laytime : "";
                                string load_port = dataDetail.fcl_payment_terms.load_port != null ? dataDetail.fcl_payment_terms.load_port : "";
                                string dis_port = dataDetail.fcl_payment_terms.discharge_port != null ? dataDetail.fcl_payment_terms.discharge_port : "";

                                //set data
                                etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Date_Time, new ExtendValue { value = sDateTime, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Charterer, new ExtendValue { value = sCharterer, encryptFlag = "N" });
                                //etxValue.SetValue(CPAIConstantUtil.Cargo, new ExtendValue { value = sCargo, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Broker, new ExtendValue { value = sBroker, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.LayCan_From, new ExtendValue { value = laycan_from, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.LayCan_To, new ExtendValue { value = laycan_to, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Load_port, new ExtendValue { value = load_port, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Discharge_port, new ExtendValue { value = dis_port, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Laytime, new ExtendValue { value = laytime, encryptFlag = "N" });
                            }


                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            //set action
                            //string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                        #endregion
                    //}
                    //else
                    //{
                    //    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                    //}
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIFreightUpdateDataState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIFreightUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIFreightUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, FclRootObject dataDetail)
        {
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            FREIGHT_DATA_DAL dataDAL = new FREIGHT_DATA_DAL();
                            dataDAL.Delete(TransID, context);

                            #region CPAI_FREIGHT_DATA
                            CPAI_FREIGHT_DATA data = new CPAI_FREIGHT_DATA();
                            data.FDA_ROW_ID = TransID;
                            data.FDA_DOC_NO = etxValue.GetValue(CPAIConstantUtil.DocNo);
                            if (!string.IsNullOrEmpty(dataDetail.fcl_vessel_particular.date_time))
                                data.FDA_DATE_FILL_IN = DateTime.ParseExact(dataDetail.fcl_vessel_particular.date_time, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            data.FDA_FK_VESSEL = dataDetail.fcl_vessel_particular.vessel_name;
                            data.FDA_TYPE = dataDetail.fcl_vessel_particular.type;
                            data.FDA_BUILT = dataDetail.fcl_vessel_particular.built;
                            data.FDA_AGE = dataDetail.fcl_vessel_particular.age;
                            data.FDA_DWT = dataDetail.fcl_vessel_particular.dwt;
                            data.FDA_CAPACITY = dataDetail.fcl_vessel_particular.capacity;
                            data.FDA_MAX_DRAFT = dataDetail.fcl_vessel_particular.max_draft;
                            data.FDA_COATING = dataDetail.fcl_vessel_particular.coating;
                            data.FDA_DISPLACEMENT = dataDetail.fcl_vessel_particular.displacement;

                            //data.FDA_FK_LOAD_PORT = Convert.ToDecimal(dataDetail.fcl_payment_terms.load_port);
                            //data.FDA_FK_DIS_PORT = Convert.ToDecimal(dataDetail.fcl_payment_terms.discharge_port);
                            //data.FDA_FK_BROKER = dataDetail.fcl_payment_terms.broker;
                            data.FDA_BROKER_NAME = dataDetail.fcl_payment_terms.broker;
                            data.FDA_FK_CHARTERER = dataDetail.fcl_payment_terms.charterer;
                            //data.FDA_UNIT = dataDetail.fcl_payment_terms.unit;
                            //data.FDA_FK_CARGO = dataDetail.fcl_payment_terms.cargo;
                            data.FDA_BSS = dataDetail.fcl_payment_terms.bss;
                            if (!string.IsNullOrEmpty(dataDetail.fcl_payment_terms.laycan_from))
                                data.FDA_LAYCAN_FROM = DateTime.ParseExact(dataDetail.fcl_payment_terms.laycan_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.fcl_payment_terms.laycan_to))
                                data.FDA_LAYCAN_TO = DateTime.ParseExact(dataDetail.fcl_payment_terms.laycan_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            data.FDA_LAYTIME = dataDetail.fcl_payment_terms.laytime;
                            data.FDA_DEM = dataDetail.fcl_payment_terms.dem;
                            data.FDA_UNIT = dataDetail.fcl_payment_terms.dem_unit;
                            data.FDA_ADD_COMM = dataDetail.fcl_payment_terms.add_comm;
                            data.FDA_BROKER_COMM = dataDetail.fcl_payment_terms.broker_comm;
                            data.FDA_WITHOLD_TAX = dataDetail.fcl_payment_terms.withold_tax;
                            data.FDA_TRANSIT_LOSS = dataDetail.fcl_payment_terms.transit_loss;
                            data.FDA_PAYMENT_TERM = dataDetail.fcl_payment_terms.payment_term;
                            data.FDA_LOAD_PORT = dataDetail.fcl_payment_terms.load_port; 
                            data.FDA_DISCHARGE_PORT = dataDetail.fcl_payment_terms.discharge_port;
                            //data.FDA_QUANTITY = dataDetail.fcl_payment_terms.quantity;
                            data.FDA_TOTAL_COMM = dataDetail.fcl_payment_terms.total_comm;

                            data.FDA_TSP_LOAD_PORT = dataDetail.fcl_time_for_operation.loading_port;
                            data.FDA_TSP_DIS_PORT = dataDetail.fcl_time_for_operation.discharge_port;
                            data.DISTANCE_BALLAST = dataDetail.fcl_time_for_operation.distance_ballast;                            
                            data.FDA_TSM_BALLAST = dataDetail.fcl_time_for_operation.ballast;
                            data.DISTANCE_LADEN = dataDetail.fcl_time_for_operation.distance_laden;
                            data.FDA_TSM_LADEN = dataDetail.fcl_time_for_operation.laden;
                            data.FDA_TANK_CLEANING = dataDetail.fcl_time_for_operation.tank_cleaning;
                            data.FDA_TOTAL_DURATION = dataDetail.fcl_time_for_operation.total_duration;
                            data.FDA_TOTAL_PORT_CHARGE = dataDetail.total_port_charge;

                            data.FDA_OFFER_UNIT = dataDetail.fcl_results.offer_freight.offer_unit;
                            data.FDA_OFFER_BY = dataDetail.fcl_results.offer_freight.offer_by;
                            data.FDA_OFFER_FREIGHT = dataDetail.fcl_results.offer_freight.usd;
                            data.FDA_TOT_VAR_COST = dataDetail.fcl_results.total_variable_cost.usd;
                            data.FDA_PROFIT_VAR = dataDetail.fcl_results.profit_over.usd;
                            data.FDA_PROFIT_VAR_PERCENT = dataDetail.fcl_results.profit_over.percent;
                            data.FDA_TOT_FIX_COST = dataDetail.fcl_results.total_fix_cost.usd;
                            data.FDA_TOT_COST = dataDetail.fcl_results.total_cost.usd;
                            data.FDA_PROFIT_TOT = dataDetail.fcl_results.profit_over_total.usd;
                            data.FDA_PROFIT_TOT_PERCENT = dataDetail.fcl_results.profit_over_total.percent;

                            if (string.IsNullOrEmpty(dataDetail.fcl_results.market_ref.usd))
                            {
                                data.FDA_MARKET_FREIGHT = "N/A";
                            }
                            else
                            {
                                data.FDA_MARKET_FREIGHT = dataDetail.fcl_results.market_ref.usd;
                            }                            

                            data.FDA_OFFER_FREIGHT_B = dataDetail.fcl_results.offer_freight.baht;
                            data.FDA_TOT_VAR_COST_B = dataDetail.fcl_results.total_variable_cost.baht;
                            data.FDA_PROFIT_VAR_B = dataDetail.fcl_results.profit_over.baht;
                            data.FDA_TOT_FIX_COST_B = dataDetail.fcl_results.total_fix_cost.baht;
                            data.FDA_TOT_COST_B = dataDetail.fcl_results.total_cost.baht;
                            data.FDA_PROFIT_TOT_B = dataDetail.fcl_results.profit_over_total.baht;

                            if (string.IsNullOrEmpty(dataDetail.fcl_results.market_ref.baht))
                            {
                                data.FDA_MARKET_FREIGHT_B = "N/A";
                            }
                            else
                            {
                                data.FDA_MARKET_FREIGHT_B = dataDetail.fcl_results.market_ref.baht;
                            }                            
                            data.FDA_OFFER_FREIGHT_MT = dataDetail.fcl_results.offer_freight.mt;
                            data.FDA_EXCHANGE_RATE = dataDetail.fcl_exchange_rate;

                            if (!string.IsNullOrEmpty(dataDetail.fcl_exchange_date))
                            {
                                data.FDA_EXCHANGE_DATE = DateTime.ParseExact(dataDetail.fcl_exchange_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            } else
                            {
                                data.FDA_EXCHANGE_DATE = null;
                            }                                

                            data.FDA_TOT_BUNKER_COST = dataDetail.total_bunker_cost;

                            if (dataDetail.fcl_vessel_speed.fcl_full.flag == "Y")
                            {
                                data.FDA_VESSEL_SPEED_FLAG = "F"; //F=Full, E=Eco
                            }
                            else
                            {
                                data.FDA_VESSEL_SPEED_FLAG = "E"; //F=Full, E=Eco
                            }

                            data.FDA_FULL_BALLAST = dataDetail.fcl_vessel_speed.fcl_full.ballast;
                            data.FDA_FULL_LADEN = dataDetail.fcl_vessel_speed.fcl_full.laden;
                            data.FDA_ECO_BALLAST = dataDetail.fcl_vessel_speed.fcl_eco.ballast;
                            data.FDA_ECO_LADEN = dataDetail.fcl_vessel_speed.fcl_eco.laden;

                            //from to
                            if (!string.IsNullOrEmpty(dataDetail.fcl_latest_bunker_price.fcl_grade_fo.date))
                                data.FDA_GRADE_FO_DATE_FROM = DateTime.ParseExact(dataDetail.fcl_latest_bunker_price.fcl_grade_fo.date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            data.FDA_GRADE_FO_PLACE = dataDetail.fcl_latest_bunker_price.fcl_grade_fo.place;
                            data.FDA_GRADE_FO_PRICE = dataDetail.fcl_latest_bunker_price.fcl_grade_fo.price;
                            data.FDA_GRADE_FO_BNK_ID = dataDetail.fcl_latest_bunker_price.fcl_grade_fo.bunker_id;

                            //from to
                            if (!string.IsNullOrEmpty(dataDetail.fcl_latest_bunker_price.fcl_grade_mgo.date))
                                data.FDA_GRADE_MGO_DATE_FROM = DateTime.ParseExact(dataDetail.fcl_latest_bunker_price.fcl_grade_mgo.date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            data.FDA_GRADE_MGO_PLACE = dataDetail.fcl_latest_bunker_price.fcl_grade_mgo.place;
                            data.FDA_GRADE_MGO_PRICE = dataDetail.fcl_latest_bunker_price.fcl_grade_mgo.price;
                            data.FDA_GRADE_MGO_BNK_ID = dataDetail.fcl_latest_bunker_price.fcl_grade_mgo.bunker_id;

                            data.FDA_FO_LOAD = dataDetail.fcl_bunker_consumption.fcl_load.fo;
                            data.FDA_FO_DISCHARGE = dataDetail.fcl_bunker_consumption.fcl_discharge.fo;
                            data.FDA_FO_STEAMING_BALLAST = dataDetail.fcl_bunker_consumption.fcl_steaming_ballast.fo;
                            data.FDA_FO_STEAMING_LADEN = dataDetail.fcl_bunker_consumption.fcl_steaming_laden.fo;
                            data.FDA_FO_IDLE = dataDetail.fcl_bunker_consumption.fcl_idle.fo;
                            data.FDA_FO_TANK_CLEANING = dataDetail.fcl_bunker_consumption.fcl_tack_cleaning.fo;
                            data.FDA_FO_HEATING = dataDetail.fcl_bunker_consumption.fcl_heating.fo;

                            data.FDA_MGO_LOAD = dataDetail.fcl_bunker_consumption.fcl_load.mgo;
                            data.FDA_MGO_DISCHARGE = dataDetail.fcl_bunker_consumption.fcl_discharge.mgo;
                            data.FDA_MGO_STEAMING_BALLAST = dataDetail.fcl_bunker_consumption.fcl_steaming_ballast.mgo;
                            data.FDA_MGO_STEAMING_LADEN = dataDetail.fcl_bunker_consumption.fcl_steaming_laden.mgo;
                            data.FDA_MGO_IDLE = dataDetail.fcl_bunker_consumption.fcl_idle.mgo;
                            data.FDA_MGO_TANK_CLEANING = dataDetail.fcl_bunker_consumption.fcl_tack_cleaning.mgo;
                            data.FDA_MGO_HEATING = dataDetail.fcl_bunker_consumption.fcl_heating.mgo;

                            data.FDA_FIX_COST_PER_DAY = dataDetail.fcl_other_cost.fix_cost;
                            data.FDA_TOT_FIX_COST_PER_DAY = dataDetail.fcl_other_cost.total_fix_cost_per_day;
                            data.FDA_TOTAL_COMMISSION = dataDetail.fcl_other_cost.total_commession;
                            data.FDA_WITHHOLDING_TAX = dataDetail.fcl_other_cost.withold_tax;
                            data.FDA_COST_OTHERS = dataDetail.fcl_other_cost.others;
                            data.FDA_TOTAL_EXPENSES = dataDetail.fcl_other_cost.total_expenses;
                            data.FDA_SUGGESTED_MARGIN = dataDetail.fcl_other_cost.suggested_margin;
                            data.FDA_SUGGESTED_MARGIN_VALUE = dataDetail.fcl_other_cost.suggested_margin_value;
                            data.FDA_IMAGE_PATH = dataDetail.fcl_pic_path;
                            data.FDA_FILE_PATH = dataDetail.fcl_excel_file_name;
                            data.FDA_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);

                            data.FDA_REQUESTED_DATE = dtNow;
                            data.FDA_REQUESTED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.FDA_REMARK = dataDetail.fcl_remark;
                            data.FDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            data.FDA_CREATED = dtNow;
                            data.FDA_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            data.FDA_UPDATED = dtNow;
                            data.FDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Save(data, context);

                            FREIGHT_BUNKER_DAL dataFreightDAL = new FREIGHT_BUNKER_DAL();
                            CPAI_FREIGHT_BUNKER dataFreight;
                            foreach (var itemCost in dataDetail.fcl_bunker_cost)
                            {
                                dataFreight = new CPAI_FREIGHT_BUNKER();
                                dataFreight.FBC_ROW_ID = Guid.NewGuid().ToString("N");
                                dataFreight.FBC_FK_FREIGHT_DATA = TransID;
                                dataFreight.FBC_FCL_PRICE_FO = itemCost.fcl_price.fo;
                                dataFreight.FBC_FCL_PRICE_MGO = itemCost.fcl_price.mgo;
                                dataFreight.FBC_LOADING = itemCost.loading;
                                dataFreight.FBC_DIS_PORT = itemCost.discharge_port;
                                dataFreight.FBC_STEAMING_BALLAST = itemCost.steaming_ballast;
                                dataFreight.FBC_STEAMING_LADEN = itemCost.steaming_laden;
                                dataFreight.FBC_TANK_CLEANING = itemCost.tank_cleaning;
                                dataFreight.FBC_CREATED = dtNow;
                                dataFreight.FBC_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataFreight.FBC_UPDATED = dtNow;
                                dataFreight.FBC_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                dataFreightDAL.Save(dataFreight, context);
                            }

                            FREIGHT_CARGO_DAL dataCargoDAL = new FREIGHT_CARGO_DAL();
                            CPAI_FREIGHT_CARGO dataCargo;
                            if (dataDetail.fcl_payment_terms.Cargoes != null && dataDetail.fcl_payment_terms.Cargoes.Count() > 0)
                            {
                                foreach (var cargo in dataDetail.fcl_payment_terms.Cargoes)
                                {
                                    dataCargo = new CPAI_FREIGHT_CARGO();
                                    dataCargo.FDC_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataCargo.FDC_FK_FREIGHT_DATA = TransID;
                                    dataCargo.FDC_FK_CARGO = cargo.cargo;
                                    dataCargo.FDC_QUANTITY = cargo.quantity;
                                    dataCargo.FDC_ORDER = cargo.order;
                                    dataCargo.FDC_UNIT = cargo.unit;
                                    dataCargo.FDC_OTHERS = cargo.unit_other;
                                    dataCargo.FDC_TOLERANCE = cargo.tolerance;
                                    dataCargo.FDC_CREATED = dtNow;
                                    dataCargo.FDC_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCargo.FDC_UPDATED = dtNow;
                                    dataCargo.FDC_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataCargoDAL.Save(dataCargo, context);
                                }
                            }

                            FREIGHT_PORT_DAL dataLoadPortDAL = new FREIGHT_PORT_DAL();
                            CPAI_FREIGHT_PORT dataPort;
                            if (dataDetail.fcl_payment_terms.ControlPort != null && dataDetail.fcl_payment_terms.ControlPort.Count() > 0)
                            {
                                foreach (var itemLoadPort in dataDetail.fcl_payment_terms.ControlPort.Select((value, i) => new { i, value }))
                                {
                                    dataPort = new CPAI_FREIGHT_PORT();
                                    dataPort.FDP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataPort.FDP_FK_FREIGHT_DATA = TransID;
                                    dataPort.FDP_FK_PORT = itemLoadPort.value.JettyPortKey;
                                    dataPort.FDP_ORDER_PORT = itemLoadPort.value.PortOrder;
                                    dataPort.FDP_PORT_TYPE = itemLoadPort.value.PortType;
                                    dataPort.FDP_VALUE = itemLoadPort.value.PortValue;
                                    dataPort.FDP_OTHER_DESC = itemLoadPort.value.PortOtherDesc;
                                    dataPort.FDP_CREATED = dtNow;
                                    dataPort.FDP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataPort.FDP_UPDATED = dtNow;
                                    dataPort.FDP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataLoadPortDAL.Save(dataPort, context);
                                }
                            }
                            if (dataDetail.fcl_payment_terms.ControlDischarge != null && dataDetail.fcl_payment_terms.ControlDischarge.Count() > 0)
                            {
                                foreach (var itemLoadPort in dataDetail.fcl_payment_terms.ControlDischarge.Select((value, i) => new { i, value }))
                                {
                                    dataPort = new CPAI_FREIGHT_PORT();
                                    dataPort.FDP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataPort.FDP_FK_FREIGHT_DATA = TransID;
                                    dataPort.FDP_FK_PORT = itemLoadPort.value.JettyPortKey;
                                    dataPort.FDP_ORDER_PORT = itemLoadPort.value.PortOrder;
                                    dataPort.FDP_PORT_TYPE = itemLoadPort.value.PortType;
                                    dataPort.FDP_VALUE = itemLoadPort.value.DischargeValue;
                                    dataPort.FDP_OTHER_DESC = itemLoadPort.value.PortOtherDesc;
                                    dataPort.FDP_CREATED = dtNow;
                                    dataPort.FDP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataPort.FDP_UPDATED = dtNow;
                                    dataPort.FDP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataLoadPortDAL.Save(dataPort, context);
                                }
                            }

                            FREIGHT_ATTACH_FILE_DAL dataAttachFileDAL = new FREIGHT_ATTACH_FILE_DAL();
                            CPAI_FREIGHT_ATTACH_FILE dataFile;
                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            if (!string.IsNullOrEmpty(attach_items))
                            {
                                attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                                foreach (var item in Att.attach_items)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        dataFile = new CPAI_FREIGHT_ATTACH_FILE();
                                        dataFile.FDAF_ROW_ID = Guid.NewGuid().ToString("N");
                                        dataFile.FDAF_FK_FREIGHT_DATA = TransID;
                                        dataFile.FDAF_PATH = item;                                        
                                        dataFile.FDAF_TYPE = "ATT";
                                        dataFile.FDAF_CREATED = dtNow;
                                        dataFile.FDAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataFile.FDAF_UPDATED = dtNow;
                                        dataFile.FDAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        dataAttachFileDAL.Save(dataFile, context);
                                    }
                                }
                            }

                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            String innerMessage = (ex.InnerException != null)
                              ? ex.InnerException.Message
                              : "";
                            string res = ex.Message;
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("Exception : " + innerMessage + " : " + res);
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIFreightUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            FREIGHT_DATA_DAL dataDAL = new FREIGHT_DATA_DAL();
                            CPAI_FREIGHT_DATA dataF = new CPAI_FREIGHT_DATA();
                            dataF.FDA_ROW_ID = TransID;
                            dataF.FDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataF.FDA_UPDATED = DateTime.Now;
                            dataF.FDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.Update(dataF, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIFreightUpdateDataState # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIFreightUpdateDataState >> UpdateDB #");
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIFreightUpdateDataState # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}