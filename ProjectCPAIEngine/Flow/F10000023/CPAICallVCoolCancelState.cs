﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAICallVCoolCancelState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICallVCoolCancelState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = "", userGroupDlg = "";
                UserGroupDAL userDal = new UserGroupDAL();
                CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                if (r != null && r.USG_USER_GROUP != null)
                {
                    userGroup = r.USG_USER_GROUP;
                }
                CPAI_USER_GROUP dlg = userDal.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                if (dlg != null && dlg.USG_USER_GROUP != null)
                {
                    userGroupDlg = dlg.USG_USER_GROUP;
                }

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                CdpRootObject dataDetail = JSonConvertUtil.jsonToModel<CdpRootObject>(item);
                VCO_DATA_DAL dataDal = new VCO_DATA_DAL();
                VCO_DATA data = dataDal.GetByCrudePurchaseID(stateModel.EngineModel.ftxTransId);

                if (data != null)
                {
                    FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
                    FUNCTION_TRANSACTION func = ftxMan.findByTransactionIdSystemType(data.VCDA_ROW_ID, ConstantPrm.SYSTEM.VCOOL, ConstantPrm.SYSTEMTYPE.CRUDE).FirstOrDefault();

                    if (func != null)
                    {
                        VcoRootObject vco = new VcoRootObject();

                        vco.comment = new VcoComment();
                        vco.comment.evpc = dataDetail.reason;

                        var json = new JavaScriptSerializer().Serialize(vco);

                        //call function F10000063 : CANCEL
                        RequestCPAI req = new RequestCPAI();
                        req.Function_id = ConstantPrm.FUNCTION.F10000063;
                        req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.Req_transaction_id = func.FTX_REQ_TRANS;
                        req.State_name = "CPAIVerifyRequireInputContinueState";
                        req.Req_parameters = new Req_parameters();
                        req.Req_parameters.P = new List<P>();
                        req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                        req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_CANCEL });
                        req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.ACTION_CANCEL });
                        req.Req_parameters.P.Add(new P { K = "user", V = user });
                        req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                        req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                        req.Req_parameters.P.Add(new P { K = "note", V = stateModel.EngineModel.ftxTransId });
                        req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                        req.Extra_xml = "";

                        ResponseData resData = new ResponseData();
                        RequestData reqData = new RequestData();
                        resData = new ResponseData();
                        reqData = new RequestData();
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                        var xml = ShareFunction.XMLSerialize(req);
                        reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                        resData = service.CallService(reqData);
                        log.Debug(" resp function F10000063 >> " + resData.result_code);

                        currentCode = resData.result_code;
                    }
                }

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICallVCoolCancelState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICallVCoolCancelState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}