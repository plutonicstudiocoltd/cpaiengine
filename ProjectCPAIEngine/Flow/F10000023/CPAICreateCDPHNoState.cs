﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000023
{
    public class CPAICreateCDPHNoState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICreateCDPHNoState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string LastSEQ = MasterData.GetSeqCDPH();
                string purchaseNo = "CDPH-" + DateTime.Now.ToString("yyMM") + "-" + LastSEQ;

                ExtendValue extPurchaseNumber = new ExtendValue();
                extPurchaseNumber.value = purchaseNo;
                //purchase no.
                etxValue.Add(CPAIConstantUtil.PurchaseNumber, extPurchaseNumber);
                etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PurchaseNumber, extPurchaseNumber);
                //create by
                ExtendValue extUser = new ExtendValue();
                extUser.value = etxValue.GetValue(CPAIConstantUtil.User);
                etxValue.Add(CPAIConstantUtil.CreateBy, extUser);
                
                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICreateCDPHNoState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIUpdateDataState >> UpdateDB # :: Exception >>>  " + tem);
                log.Info("# Error CPAICreateCDPHNoState # :: Exception >>> " + ex);
                log.Error("CPAICreateCDPHNoState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }

        }
    }
}