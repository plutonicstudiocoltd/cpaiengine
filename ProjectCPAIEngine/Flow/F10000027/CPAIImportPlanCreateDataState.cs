﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPCF;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000027
{
    public class CPAIImportPlanCreateDataState : BasicBean, StateFlowAction
    {

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIImportPlanCreateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                bool isRunDB = false;
                String tripNo = "";
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

                CrudeImportPlanViewModel dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<CrudeImportPlanViewModel>(item);
                }

                isRunDB = InsertDB("100001", etxValue, dataDetail, 10, ref tripNo);

                if (isRunDB)
                {
                    currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                }

                ExtendValue extRowsPerPage = new ExtendValue
                { value = tripNo, encryptFlag = ConstantDBUtil.NO_FLAG };

                etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.TripNo, extRowsPerPage);


                //stateModel.BusinessModel.etxValue = etxValue;
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIImportPlanCreateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIImportPlanCreateDataState # :: Exception >>> " + ex);
                log.Error("CPAIImportPlanCreateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        //public void doAction(StateModel stateModel)
        //{
        //    log.Info("# Start State CPAIImportPlanCreateDataState #  ");
        //    RespCodeManagement respCodeManagement = new RespCodeManagement();

        //    string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        //    string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        //    stateModel.BusinessModel.currentNameSpace = currentNameSpace;
        //    ShareFn _FN = new ShareFn();
        //    try
        //    {
        //        String tripNo = "";
        //        Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
        //        //set index
        //        var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);

        //        TceMkRootObject dataDetail = null;
        //        if (item != null)
        //        {
        //            dataDetail = JSonConvertUtil.jsonToModel<TceMkRootObject>(item);
        //            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
        //            string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
        //            string User = etxValue.GetValue(CPAIConstantUtil.User);
        //            if (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) ||
        //                currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)))

        //            {
        //                #region add data history
        //                //add data history
        //                DataHistoryDAL dthMan = new DataHistoryDAL();
        //                DateTime now = DateTime.Now;
        //                CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
        //                dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
        //                dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
        //                dataHistory.DTH_ACTION_DATE = now;
        //                dataHistory.DTH_ACTION_BY = User;
        //                dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
        //                dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
        //                dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
        //                dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
        //                dataHistory.DTH_CREATED_BY = User;
        //                dataHistory.DTH_CREATED_DATE = now;
        //                dataHistory.DTH_UPDATED_BY = User;
        //                dataHistory.DTH_UPDATED_DATE = now;
        //                dthMan.Save(dataHistory);
        //                #endregion

        //                #region Set detail to Value
        //                string dataDetailString = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
        //                string month = dataDetail.tce_mk_detail.month;
        //                string date_from = dataDetail.tce_mk_detail.date_from;
        //                string date_to = dataDetail.tce_mk_detail.date_to;
        //                string sVessel = dataDetail.tce_mk_detail.vessel != null ? dataDetail.tce_mk_detail.vessel : "";

        //                string sStatus = etxValue.GetValue(CPAIConstantUtil.Status);
        //                #endregion
        //                //check charter in voy duplicate
        //                Boolean b = CPAI_TCE_MK_DAL.isDuplicate(dataDetail.tce_mk_detail.vessel, dataDetail.tce_mk_detail.month, CPAIConstantUtil.ACTION_SUBMIT);
        //                //check data 
        //                if (!b
        //                    || currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT)
        //                   || (b && currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
        //                   || currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL)
        //                   )
        //                {
        //                    #region Insert Update DB
        //                    bool isRunDB = false;
        //                    if (currentAction.Equals(CPAIConstantUtil.ACTION_CANCEL))
        //                    {
        //                        //OTHER ==> UPDATE
        //                        isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue);
        //                    }
        //                    else
        //                    {
        //                        double? avgWS = 0;
        //                        if (currentAction.Equals(CPAIConstantUtil.ACTION_EDIT) || currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT))
        //                        {
        //                            CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
        //                            var TD = dataDetail.tce_mk_detail.td;

        //                            // set start date from month
        //                            //var dt = DateTime.ParseExact(dataDetail.tce_mk_detail.month, "MM/yyyy", CultureInfo.InvariantCulture);

        //                            // Change to start date
        //                            var dt = DateTime.ParseExact(dataDetail.tce_mk_detail.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);

        //                            var ty = "UK";
        //                            avgWS = dataDAL.CalEverageWS(dt, TD, ty);

        //                            if (avgWS == null)
        //                            {
        //                                currentCode = CPAIConstantRespCodeUtil.NOT_CAL_AVG_WS_BITR_TD2_RESP_CODE;
        //                                goto EndState;
        //                            }
        //                        }

        //                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, Convert.ToDouble(avgWS));
        //                    }
        //                    #endregion
        //                    //check db by next status
        //                    #region INSERT JSON
        //                    if (isRunDB)
        //                    {
        //                        //set data detail (json when action = "-" or DRAFT or SUBMIT only)
        //                        #region Set dataDetail
        //                        //set status
        //                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
        //                        //set action
        //                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
        //                        //set data_detail_sch
        //                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetailString, encryptFlag = "N" });

        //                        //set data
        //                        etxValue.SetValue(CPAIConstantUtil.Month, new ExtendValue { value = month, encryptFlag = "N" });
        //                        etxValue.SetValue(CPAIConstantUtil.Date_from, new ExtendValue { value = date_from, encryptFlag = "N" });
        //                        etxValue.SetValue(CPAIConstantUtil.Date_to, new ExtendValue { value = date_to, encryptFlag = "N" });
        //                        etxValue.SetValue(CPAIConstantUtil.Vessel, new ExtendValue { value = sVessel, encryptFlag = "N" });

        //                        #endregion

        //                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
        //                    }
        //                    else
        //                    {
        //                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
        //                    }
        //                    #endregion
        //                }
        //                else
        //                {
        //                    currentCode = CPAIConstantRespCodeUtil.DUP_DATA_RESP_CODE;
        //                }

        //            }
        //            else
        //            {
        //                currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
        //            }
        //        }
        //        else
        //        {
        //            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
        //        }



        //        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

        //        ExtendValue extTripNo = new ExtendValue();
        //        extTripNo.value = tripNo;
        //        //Trip no.
        //        etxValue.Add(CPAIConstantUtil.PurchaseNumber, extTripNo);

        //        //map response code to response description
        //        stateModel.BusinessModel.currentCode = currentCode;
        //        respCodeManagement.setCurrentCodeMapping(stateModel);
        //        log.Info("# End State CPAIImportPlanCreateDataState # :: Code >>> " + currentCode);

        //    }
        //    catch (Exception ex)
        //    {
        //        log.Info("# Error CPAIImportPlanCreateDataState # :: Exception >>> " + ex);
        //        log.Error("CPAIImportPlanCreateDataState::Exception >>> ", ex);
        //        stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
        //        //map response code to response description
        //        respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
        //        stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
        //    }
        //}

        private string genTripNo(string com_code, EntityCPAIEngine context)
        {
            string ret = "CF";
            com_code = com_code.Substring(0, 2);
            string yy = DateTime.Now.ToString("yyMM", new System.Globalization.CultureInfo("en-US"));

            ret = ret + com_code + yy;
            try
            {
                CRUDE_IMPORT_PLAN_DAL dataDAL = new CRUDE_IMPORT_PLAN_DAL();
                string lastTrip = dataDAL.getLastTripNo(ret, context);
                if (lastTrip == "")
                {
                    ret += "01";
                }
                else
                {
                    string nextTrip = "00" + (int.Parse(lastTrip) + 1).ToString();
                    nextTrip = nextTrip.Substring(nextTrip.Length - 2, 2);

                    ret += nextTrip;
                }
            }
            catch (Exception ex) { }

            return ret;
        }

        private Boolean checkNumeric(object objValue)
        {
            Boolean ret = true;
            if (objValue == null || objValue.ToString() == "" || objValue.ToString() == "undefined" || objValue.ToString() == "null" || objValue.ToString() == "NaN")
            {
                ret = false;
            }
            //ret = (objValue == null || objValue.ToString() == "") ? false : true;
            return ret;
        }

        private void SplitDateFromTo(string sDate, ref string sDateFrom, ref string sDateTo)
        {
            sDateFrom = "";
            sDateTo = "";
            if (!string.IsNullOrEmpty(sDate))
            {
                sDate = sDate.Replace("to", "|");
                string[] arrDate = sDate.Split('|');
                if (arrDate.Length == 2)
                {
                    sDateFrom = arrDate[0].Trim();
                    sDateTo = arrDate[1].Trim();
                }
            }
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, CrudeImportPlanViewModel dataDetail, double AvgWS, ref string tripNo)
        {
            log.Info("# Start State CPAIImportPlanCreateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string HeadStatus = "";
                            HeaderViewModel itemHead = dataDetail.crude_approve_detail.dDetail_Header[0];
                            HeadStatus = (string.IsNullOrEmpty(itemHead.PHE_ACTION) || itemHead.PHE_ACTION.Equals("INSERT")) ? "ACTIVE" : itemHead.PHE_ACTION;

                            Boolean UpdatePriceSTS = true;

                            string User = etxValue.GetValue(CPAIConstantUtil.User);
                            string user_group = etxValue.GetValue(CPAIConstantUtil.UserGroup);
                            string ref_trip_no = etxValue.GetValue(CPAIConstantUtil.TripNo);

                            CRUDE_IMPORT_PLAN_DAL dataDAL = new CRUDE_IMPORT_PLAN_DAL();

                            if (ref_trip_no != "X")
                            {
                                //if (HeadStatus != "DELETE")
                                //    dataDAL.UpdatePrice0Only(ref_trip_no);

                                dataDAL.Delete(ref_trip_no, context);

                                tripNo = ref_trip_no;

                                UpdatePriceSTS = dataDAL.CheckUpdatePricing(ref_trip_no, user_group);

                            }
                            else
                            {
                                var comcode = dataDetail.crude_approve_detail.dDetail_Header[0].PHE_COMPANY_CODE;
                                tripNo = genTripNo(comcode, context);
                            }

                            Nullable<System.DateTime> DateTemp = null;
                            string loadingMonth = "";
                            string fDate = "";
                            string tDate = "";
                            if (dataDetail.crude_approve_detail.dDetail_Material != null && HeadStatus != "DELETE") {
                                foreach (var itemMat in dataDetail.crude_approve_detail.dDetail_Material) {
                                    SplitDateFromTo(itemMat.PMA_LOADING_DATE_FROM, ref fDate, ref tDate);
                                    if (tDate == "")
                                        continue;

                                    DateTime cDate = DateTime.ParseExact(tDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    if (DateTemp == null)
                                        DateTemp = cDate;
                                    else {
                                        if (cDate > DateTemp) 
                                            DateTemp = cDate; 

                                    }
                                }
                            }

                            if (DateTemp != null)
                            {
                                loadingMonth = Convert.ToDateTime(DateTemp).ToString("yyyyMM", new System.Globalization.CultureInfo("en-US"));
                            }

                            if (string.IsNullOrEmpty(loadingMonth))
                            {
                                loadingMonth = itemHead.PHE_ARRIVAL_YEAR ?? "";
                                if (checkNumeric(itemHead.PHE_ARRIVAL_MONTH)) {
                                    if (Convert.ToDecimal(itemHead.PHE_ARRIVAL_MONTH) > 9)
                                        loadingMonth += itemHead.PHE_ARRIVAL_MONTH;
                                    else
                                        loadingMonth += "0" + itemHead.PHE_ARRIVAL_MONTH;
                                }
                            }


                            #region CPAI_PCF_HEADER

                            PCF_HEADER dataHead = new PCF_HEADER();
                                                       
                            string ActualETAFrom = "";
                            string ActualETATo = "";
                            SplitDateFromTo(itemHead.PHE_ACTUAL_ETA_FROM, ref ActualETAFrom, ref ActualETATo);

                            dataHead.PHE_TRIP_NO = tripNo; // item.h_TripNo;
                            dataHead.PHE_COMPANY_CODE = itemHead.PHE_COMPANY_CODE;
                            dataHead.PHE_ARRIVAL_YEAR = checkNumeric(itemHead.PHE_ARRIVAL_YEAR) ? Convert.ToDecimal(itemHead.PHE_ARRIVAL_YEAR) : dataHead.PHE_ARRIVAL_YEAR;
                            dataHead.PHE_ARRIVAL_MONTH = checkNumeric(itemHead.PHE_ARRIVAL_MONTH) ? Convert.ToDecimal(itemHead.PHE_ARRIVAL_MONTH) : dataHead.PHE_ARRIVAL_MONTH;
                            dataHead.PHE_ACTUAL_ETA_FROM = string.IsNullOrEmpty(ActualETAFrom) ? dataHead.PHE_ACTUAL_ETA_FROM : DateTime.ParseExact(ActualETAFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            dataHead.PHE_ACTUAL_ETA_TO = string.IsNullOrEmpty(ActualETATo) ? dataHead.PHE_ACTUAL_ETA_TO : DateTime.ParseExact(ActualETATo, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                            dataHead.PHE_VESSEL = itemHead.PHE_VESSEL;
                            dataHead.PHE_CREATED_DATE = dtNow;
                            dataHead.PHE_CREATED_BY = User;
                            dataHead.PHE_UPDATED_DATE = dtNow;
                            dataHead.PHE_UPDATED_BY = User;
                            dataHead.PHE_CDA_ROW_ID = itemHead.PHE_CDA_ROW_ID;
                            dataHead.PHE_PLANNING_RELEASE = itemHead.PHE_PLANNING_RELEASE;
                            dataHead.PHE_BOOKING_RELEASE = itemHead.PHE_BOOKING_RELEASE;
                            dataHead.PHE_STATUS = HeadStatus;
                            dataHead.PHE_CHANNEL = itemHead.PHE_CHANNEL;
                            dataHead.PHE_SPOT_NAME = itemHead.PHE_SPOT_NAME;
                            dataHead.PHE_LOADING_MONTH = loadingMonth;
                            dataHead.PHE_MAT_TYPE = itemHead.PHE_MAT_TYPE;
                            dataHead.PHE_CHARTERING_NO = itemHead.PHE_CHARTERING_NO ?? "";
                            dataDAL.Save(dataHead, context);
                            //context.SaveChanges();

                            #endregion

                            String PurchaseType = "";

                            #region CPAI_PCF_MATERIAL
                            DateTime dtLaycanLoadFrom = DateTime.MinValue;
                            if (dataDetail.crude_approve_detail.dDetail_Material != null && HeadStatus != "DELETE")
                            {
                                foreach (MaterialViewModel itemMat in dataDetail.crude_approve_detail.dDetail_Material)
                                {
                                    PCF_MATERIAL dataMat = new PCF_MATERIAL();
                                    dataMat.PMA_TRIP_NO = tripNo;
                                    dataMat.PMA_ITEM_NO = Convert.ToDecimal(itemMat.PMA_ITEM_NO);
                                    dataMat.PMA_MET_NUM = itemMat.PMA_MET_NUM;
                                    dataMat.PMA_SUPPLIER = itemMat.PMA_SUPPLIER; // item.i_SupplierCode;// item.i_SupplierCode; // 4000010
                                    dataMat.PMA_FORMULA = itemMat.PMA_FORMULA;// item.h_FormulaPirce;
                                    dataMat.PMA_ORIGIN_COUNTRY = itemMat.PMA_ORIGIN_COUNTRY;// "10000171";
                                    dataMat.PMA_CONTRACT_TYPE = itemMat.PMA_CONTRACT_TYPE;// "CNT";
                                    dataMat.PMA_INCOTERMS = itemMat.PMA_INCOTERMS;// item.h_Incoterm;
                                    dataMat.PMA_GT_C = itemMat.PMA_GT_C;
                                    // "GT_C";// item.h_GT_C;

                                    PurchaseType = string.IsNullOrEmpty(itemMat.PMA_PURCHASE_TYPE) ? "" : itemMat.PMA_PURCHASE_TYPE;

                                    dataMat.PMA_PURCHASE_TYPE = (checkNumeric(itemMat.PMA_PURCHASE_TYPE)) ? Convert.ToDecimal(itemMat.PMA_PURCHASE_TYPE) : dataMat.PMA_PURCHASE_TYPE;
                                    dataMat.PMA_STORAGE_LOCATION = itemMat.PMA_STORAGE_LOCATION;
                                    dataMat.PMA_VOLUME_BBL = (checkNumeric(itemMat.PMA_VOLUME_BBL)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_BBL.Replace(",", "")) : dataMat.PMA_VOLUME_BBL;
                                    dataMat.PMA_VOLUME_MT = (checkNumeric(itemMat.PMA_VOLUME_MT)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_MT.Replace(",", "")) : dataMat.PMA_VOLUME_MT;
                                    dataMat.PMA_VOLUME_ML = (checkNumeric(itemMat.PMA_VOLUME_ML)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_ML.Replace(",", "")) : dataMat.PMA_VOLUME_ML;
                                    dataMat.PMA_VOLUME_L15 = (checkNumeric(itemMat.PMA_VOLUME_L15)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_L15.Replace(",", "")) : dataMat.PMA_VOLUME_L15;
                                    dataMat.PMA_VOLUME_LTON = (checkNumeric(itemMat.PMA_VOLUME_LTON)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_LTON.Replace(",", "")) : dataMat.PMA_VOLUME_LTON;
                                    dataMat.PMA_VOLUME_KG = (checkNumeric(itemMat.PMA_VOLUME_KG)) ? Convert.ToDecimal(itemMat.PMA_VOLUME_KG.Replace(",", "")) : dataMat.PMA_VOLUME_KG;
                                    dataMat.PMA_PURCHASE_UNIT = itemMat.PMA_PURCHASE_UNIT; // "BBL,MT,ML";
                                    dataMat.PMA_TOLERANCE = itemMat.PMA_TOLERANCE; // "TL";// item.h_Tolerance;

                                    
                                    string sNominatedFrom = "";
                                    string sNominatedTo = "";
                                    SplitDateFromTo(itemMat.PMA_NOMINATED_FROM, ref sNominatedFrom, ref sNominatedTo);

                                    string sReceivedFrom = "";
                                    string sReceivedTo = "";
                                    SplitDateFromTo(itemMat.PMA_RECEIVED_FROM, ref sReceivedFrom, ref sReceivedTo);

                                    dataMat.PMA_NOMINATED_FROM = string.IsNullOrEmpty(sNominatedFrom) ? dataMat.PMA_NOMINATED_FROM : DateTime.ParseExact(sNominatedFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_NOMINATED_TO = string.IsNullOrEmpty(sNominatedTo) ? dataMat.PMA_NOMINATED_TO : DateTime.ParseExact(sNominatedTo, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                                    dataMat.PMA_RECEIVED_FROM = string.IsNullOrEmpty(sReceivedFrom) ? dataMat.PMA_RECEIVED_FROM : DateTime.ParseExact(sReceivedFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_RECEIVED_TO = string.IsNullOrEmpty(sReceivedTo) ? dataMat.PMA_RECEIVED_TO : DateTime.ParseExact(sReceivedTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_BL_DATE = (string.IsNullOrEmpty(itemMat.PMA_BL_DATE)) ? dataMat.PMA_BL_DATE : DateTime.ParseExact(itemMat.PMA_BL_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_COMPLETE_TIME = null;
                                    dataMat.PMA_DUE_DATE = (string.IsNullOrEmpty(itemMat.PMA_DUE_DATE)) ? dataMat.PMA_DUE_DATE : DateTime.ParseExact(itemMat.PMA_DUE_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_BSW = (checkNumeric(itemMat.PMA_BSW)) ? Convert.ToDecimal(itemMat.PMA_BSW.Replace(",", "")) : dataMat.PMA_BSW;
                                    dataMat.PMA_API = (checkNumeric(itemMat.PMA_API)) ? Convert.ToDecimal(itemMat.PMA_API.Replace(",", "")) : dataMat.PMA_API;
                                    dataMat.PMA_DENSITY = (checkNumeric(itemMat.PMA_DENSITY)) ? Convert.ToDecimal(itemMat.PMA_DENSITY.Replace(",", "")) : dataMat.PMA_DENSITY;
                                    dataMat.PMA_TEMP = (checkNumeric(itemMat.PMA_TEMP)) ? Convert.ToDecimal(itemMat.PMA_TEMP.Replace(",", "")) : dataMat.PMA_TEMP;
                                    dataMat.PMA_REMARK = itemMat.PMA_REMARK;
                                    dataMat.PMA_MEMO_NO = itemMat.PMA_MEMO_NO;
                                    dataMat.PMA_PO_NO = itemMat.PMA_PO_NO;
                                    dataMat.PMA_LOADING_PORT = string.IsNullOrEmpty(itemMat.PMA_LOADING_PORT) ? 0 : Convert.ToDecimal(itemMat.PMA_LOADING_PORT);
                                    dataMat.PMA_LOADING_PORT2 = string.IsNullOrEmpty(itemMat.PMA_LOADING_PORT2) ? 0 : Convert.ToDecimal(itemMat.PMA_LOADING_PORT2);

                                    string sLoadDateFrom = "";
                                    string sLoadDateTo = "";
                                    SplitDateFromTo(itemMat.PMA_LOADING_DATE_FROM, ref sLoadDateFrom, ref sLoadDateTo);
                                    dataMat.PMA_LOADING_DATE_FROM = string.IsNullOrEmpty(sLoadDateFrom) ? dataMat.PMA_LOADING_DATE_FROM : DateTime.ParseExact(sLoadDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_LOADING_DATE_TO = string.IsNullOrEmpty(sLoadDateTo) ? dataMat.PMA_LOADING_DATE_TO : DateTime.ParseExact(sLoadDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMat.PMA_PAYMENTTERM = (checkNumeric(itemMat.PMA_PAYMENTTERM)) ? Convert.ToDecimal(itemMat.PMA_PAYMENTTERM.Replace(",", "")) : dataMat.PMA_PAYMENTTERM;
                                    dataMat.PMA_HOL_TYPE = itemMat.PMA_HOLIDAY;
                                    //dataMat.PMA_BECHMARK = itemMat.PMA_BECHMARK;

                                    string sDischargeFrom = "";
                                    string sDischargeTo = "";
                                    SplitDateFromTo(itemMat.PMA_DISCHARGE_LAYCAN_FROM, ref sDischargeFrom, ref sDischargeTo);
                                    dataMat.PMA_DISCHARGE_LAYCAN_FROM = string.IsNullOrEmpty(sDischargeFrom) ? dataMat.PMA_DISCHARGE_LAYCAN_FROM : DateTime.ParseExact(sDischargeFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMat.PMA_DISCHARGE_LAYCAN_TO = string.IsNullOrEmpty(sDischargeTo) ? dataMat.PMA_DISCHARGE_LAYCAN_TO : DateTime.ParseExact(sDischargeTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMat.PMA_CREATED_DATE = dtNow;
                                    dataMat.PMA_CREATED_BY = User;
                                    dataMat.PMA_UPDATED_DATE = dtNow;
                                    dataMat.PMA_UPDATED_BY = User;
                                    dataMat.PMA_GIT_NO = itemMat.PMA_GIT_NO;
                                    dataMat.PMA_BL_STATUS = itemMat.PMA_BL_STATUS;
                                    dataMat.PMA_BECHMARK = itemMat.PMA_BECHMARK;

                                    dataMat.PMA_GROSS_QTY_BBL = (checkNumeric(itemMat.GROSS_QTY_BBL)) ? Convert.ToDecimal(itemMat.GROSS_QTY_BBL.Replace(",", "")) : dataMat.PMA_GROSS_QTY_BBL;
                                    dataMat.PMA_GROSS_QTY_MT = (checkNumeric(itemMat.GROSS_QTY_MT)) ? Convert.ToDecimal(itemMat.GROSS_QTY_MT.Replace(",", "")) : dataMat.PMA_GROSS_QTY_MT;
                                    dataMat.PMA_GROSS_QTY_ML = (checkNumeric(itemMat.GROSS_QTY_ML)) ? Convert.ToDecimal(itemMat.GROSS_QTY_ML.Replace(",", "")) : dataMat.PMA_GROSS_QTY_ML;

                                    dataMat.PMA_FREIGHT_PRICE = (checkNumeric(itemMat.FREIGHT_PRICE)) ? Convert.ToDecimal(itemMat.FREIGHT_PRICE.Replace(",", "")) : dataMat.PMA_FREIGHT_PRICE;
                                    dataMat.PMA_INSURANCE_PRICE = (checkNumeric(itemMat.INSURANCE_PRICE)) ? Convert.ToDecimal(itemMat.INSURANCE_PRICE.Replace(",", "")) : dataMat.PMA_INSURANCE_PRICE;
                                    dataMat.PMA_IMPORTDUTY_PRICE = (checkNumeric(itemMat.IMPORTDUTY_PRICE)) ? Convert.ToDecimal(itemMat.IMPORTDUTY_PRICE.Replace(",", "")) : dataMat.PMA_IMPORTDUTY_PRICE;
                                    dataMat.PMA_REMARK = itemMat.PO_CURRENCY;

                                    dataMat.PMA_PO_MAT_ITEM = (checkNumeric(itemMat.PMA_PO_MAT_ITEM)) ? Convert.ToDecimal(itemMat.PMA_PO_MAT_ITEM.Replace(",", "")) : 0;
                                    dataMat.PMA_PO_FREIGHT_ITEM = (checkNumeric(itemMat.PMA_PO_FREIGHT_ITEM)) ? Convert.ToDecimal(itemMat.PMA_PO_FREIGHT_ITEM.Replace(",", "")) : 0;
                                    dataMat.PMA_PO_INSURANCE_ITEM = (checkNumeric(itemMat.PMA_PO_INSURANCE_ITEM)) ? Convert.ToDecimal(itemMat.PMA_PO_INSURANCE_ITEM.Replace(",", "")) : 0;
                                    dataMat.PMA_PO_CUSTOMS_ITEM = (checkNumeric(itemMat.PMA_PO_CUSTOMS_ITEM)) ? Convert.ToDecimal(itemMat.PMA_PO_CUSTOMS_ITEM.Replace(",", "")) : 0;

                                    dataMat.PMA_QTY_UNIT = string.IsNullOrEmpty(itemMat.CST_UNIT) ? "" : itemMat.CST_UNIT;

                                    dataMat.PMA_CDA_ROW_ID = string.IsNullOrEmpty(itemMat.PMA_CDA_ROW_ID) ? "" : itemMat.PMA_CDA_ROW_ID;
                                    
                                    dataDAL.Save(dataMat, context);
                                    //context.SaveChanges();
                                }
                            }

                            if (dataDetail.crude_approve_detail.dDetail_MaterialFile != null && HeadStatus != "DELETE")
                            {
                                int no = 0;
                                foreach (FileViewModel itemFF in dataDetail.crude_approve_detail.dDetail_MaterialFile)
                                {
                                    string PAF_STATUS = itemFF.PAF_STATUS ?? "";
                                    if (PAF_STATUS.Equals("D"))
                                        continue;

                                    no++;

                                    PCF_ATTACH_FILE dataAttFile = new PCF_ATTACH_FILE();
                                    dataAttFile.PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                    dataAttFile.PAF_ITEM_NO = checkNumeric(itemFF.PAF_ITEM_NO) ? Convert.ToDecimal(itemFF.PAF_ITEM_NO) : 0 ;
                                    dataAttFile.PAF_NO = no;
                                    dataAttFile.PAF_FILE_FOR = itemFF.PAF_FILE_FOR ?? "";
                                    dataAttFile.PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                    dataAttFile.PAF_PATH = itemFF.PAF_PATH ?? "";
                                    dataAttFile.PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                    dataDAL.Save(dataAttFile, context);


                                    //string PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                    //string PAF_ITEM_NO = itemFF.PAF_ITEM_NO ?? "";
                                    //string PAF_NO = no.ToString();
                                    //string PAF_FILE_FOR = "MAT";
                                    //string PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                    //string PAF_PATH = itemFF.PAF_PATH ?? "";
                                    //string PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                    //string PAF_UPLOAD_DATE = "sysdate";

                                    //string sql = "insert into PCF_ATTACH_FILE (PAF_TRIP_NO, PAF_FILE_FOR, PAF_ITEM_NO, PAF_FILE_NAME, PAF_PATH, PAF_NO, PAF_DESCRIPTION, PAF_UPLOAD_DATE) ";
                                    //sql += " values ('" + PAF_TRIP_NO + "', '" + PAF_FILE_FOR + "', '" + PAF_ITEM_NO + "', '" + PAF_FILE_NAME + "', '" + PAF_PATH + "', '" + PAF_NO + "', '" + PAF_DESCRIPTION + "', " + PAF_UPLOAD_DATE + " ) ";
                                    //dataDAL.SaveAttachFile(sql, context);

                                    //dataDAL.Save(DataAT, context);
                                }
                            }

                            if (dataDetail.crude_approve_detail.dDetail_DailyPrice != null && HeadStatus != "DELETE") {

                                foreach (var itemDaily in dataDetail.crude_approve_detail.dDetail_DailyPrice) {
                                    PCF_MATERIAL_PRICE_DAILY dataDailyPrice = new PCF_MATERIAL_PRICE_DAILY();
                                    dataDailyPrice.TRIP_NO = itemDaily.TRIP_NO;
                                    dataDailyPrice.MAT_NUM = itemDaily.MAT_NUM;

                                    dataDailyPrice.INVOICE_NO = itemDaily.INVOICE_NO;
                                    dataDailyPrice.BL_DATE = (string.IsNullOrEmpty(itemDaily.BL_DATE)) ? dataDailyPrice.BL_DATE : DateTime.ParseExact(itemDaily.BL_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataDailyPrice.PRO_PRICE =  itemDaily.PRO_PRICE;
                                    dataDailyPrice.FINAL_PRICE = itemDaily.FINAL_PRICE;
                                    dataDailyPrice.DIFF_PRICE = itemDaily.DIFF_PRICE;
                                    dataDailyPrice.OUTTURN_QTY = itemDaily.OUTTURN_QTY;
                                    dataDailyPrice.P1_FX_RATE = itemDaily.P1_FX_RATE;
                                    dataDailyPrice.P1_UNIT_PRICE = itemDaily.P1_UNIT_PRICE;
                                    dataDailyPrice.P1_TOTAL_BF_VAT = itemDaily.P1_TOTAL_BF_VAT;
                                    dataDailyPrice.P1_VAT = itemDaily.P1_VAT;
                                    dataDailyPrice.P1_TOTAL_AF_VAT = itemDaily.P1_TOTAL_AF_VAT;
                                    dataDailyPrice.P2_FX_RATE = itemDaily.P2_FX_RATE;
                                    dataDailyPrice.P2_UNIT_PRICE = itemDaily.P2_UNIT_PRICE;
                                    dataDailyPrice.P2_DIFF_UNITPRICE = itemDaily.P2_DIFF_UNITPRICE;
                                    dataDailyPrice.P2_ADJUST_PRICE = itemDaily.P2_ADJUST_PRICE;
                                    dataDailyPrice.P2_TOTAL_DIFF = itemDaily.P2_TOTAL_DIFF;
                                    dataDailyPrice.P2_DIFF_DOCNO = itemDaily.P2_DIFF_DOCNO;
                                    dataDailyPrice.F1_FX_RATE = itemDaily.F1_FX_RATE;

                                    dataDailyPrice.F1_UNIT_PRICE = itemDaily.F1_UNIT_PRICE;
                                    dataDailyPrice.F1_TOTAL_BF_VAT = itemDaily.F1_TOTAL_BF_VAT;
                                    dataDailyPrice.F1_VAT = itemDaily.F1_VAT;
                                    dataDailyPrice.F1_TOTAL_AF_VAT = itemDaily.F1_TOTAL_AF_VAT;
                                    dataDailyPrice.F2_FX_RATE = itemDaily.F2_FX_RATE;
                                    dataDailyPrice.F2_TOTAL_DIFF1 = itemDaily.F2_TOTAL_DIFF1;
                                    dataDailyPrice.F2_TOTAL_DIFF2 = itemDaily.F2_TOTAL_DIFF2;
                                    dataDailyPrice.F2_TOTAL_DIFF3 = itemDaily.F2_TOTAL_DIFF3;

                                    dataDAL.Save(dataDailyPrice, context);
                                }

                            }

                            #endregion

                            #region CPAI_PCF_MATERIAL_PRICE

                            if (dataDetail.crude_approve_detail.dDetail != null && HeadStatus != "DELETE")
                            {

                                foreach (var itemPrice in dataDetail.crude_approve_detail.dDetail)
                                {
                                    PCF_MATERIAL_PRICE dataMatPrice = new PCF_MATERIAL_PRICE();
                                    dataMatPrice.PMP_TRIP_NO = tripNo;
                                    if (itemPrice.MatItemNo != "")
                                        dataMatPrice.PMP_MAT_ITEM_NO = Convert.ToDecimal(itemPrice.MatItemNo);

                                    if (itemPrice.MatPriceItemNo != "")
                                        dataMatPrice.PMP_ITEM_NO = Convert.ToDecimal(itemPrice.MatPriceItemNo);

                                    dataMatPrice.PMP_PRICE_STATUS = itemPrice.i_PriceStatus;
                                    dataMatPrice.PMP_FORMULA = itemPrice.h_FormulaPirce;

                                    // item.h_FormulaPirce;
                                    dataMatPrice.PMP_BASE_PRICE = (checkNumeric(itemPrice.i_BasePrice)) ? Convert.ToDecimal(itemPrice.i_BasePrice.Replace(",", "")) : dataMatPrice.PMP_BASE_PRICE;
                                    dataMatPrice.PMP_OSP_PREMIUM_DISCOUNT = (checkNumeric(itemPrice.i_OSP_PremiumDiscount)) ? Convert.ToDecimal(itemPrice.i_OSP_PremiumDiscount.Replace(",", "")) : dataMatPrice.PMP_OSP_PREMIUM_DISCOUNT;
                                    dataMatPrice.PMP_TRADING_PREMIUM_DISCOUNT = (checkNumeric(itemPrice.i_TradingPremiumDiscount)) ? Convert.ToDecimal(itemPrice.i_TradingPremiumDiscount.Replace(",", "")) : dataMatPrice.PMP_TRADING_PREMIUM_DISCOUNT;
                                    dataMatPrice.PMP_OTHER_COST = (checkNumeric(itemPrice.i_OTHER_COST)) ? Convert.ToDecimal(itemPrice.i_OTHER_COST.Replace(",", "")) : dataMatPrice.PMP_OTHER_COST;
                                    dataMatPrice.PMP_TOTAL_PRICE = (checkNumeric(itemPrice.ProActual)) ? Convert.ToDecimal(itemPrice.ProActual.Replace(",", "")) : dataMatPrice.PMP_TOTAL_PRICE;

                                    dataMatPrice.PMP_TOTAL_AMOUNT = (checkNumeric(itemPrice.i_TOTAL_AMOUNT)) ? Convert.ToDecimal(itemPrice.i_TOTAL_AMOUNT.Replace(",", "")) : dataMatPrice.PMP_TOTAL_AMOUNT;
                                    dataMatPrice.PMP_CURRENCY = itemPrice.i_CURRENCY;

                                    string sPricePeriodFrom = "";
                                    string sPricePeriodTo = "";
                                    SplitDateFromTo(itemPrice.i_PricePeriod, ref sPricePeriodFrom, ref sPricePeriodTo);
                                    dataMatPrice.PMP_PRICE_PERIOD_FROM = string.IsNullOrEmpty(sPricePeriodFrom) ? dataMatPrice.PMP_PRICE_PERIOD_FROM : DateTime.ParseExact(sPricePeriodFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMatPrice.PMP_PRICE_PERIOD_TO = string.IsNullOrEmpty(sPricePeriodTo) ? dataMatPrice.PMP_PRICE_PERIOD_TO : DateTime.ParseExact(sPricePeriodTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMatPrice.PMP_FX_AGREEMENT = (checkNumeric(itemPrice.i_FX_AGREEMENT)) ? Convert.ToDecimal(itemPrice.i_FX_AGREEMENT.Replace(",", "")) : dataMatPrice.PMP_FX_AGREEMENT;
                                    dataMatPrice.PMP_FX_BOT = (checkNumeric(itemPrice.i_FX_BOT)) ? Convert.ToDecimal(itemPrice.i_FX_BOT.Replace(",", "")) : dataMatPrice.PMP_FX_BOT;
                                    dataMatPrice.PMP_TOTAL_AMOUNT_THB = (checkNumeric(itemPrice.i_TOTAL_AMOUNT_THB)) ? Convert.ToDecimal(itemPrice.i_TOTAL_AMOUNT_THB.Replace(",", "")) : dataMatPrice.PMP_TOTAL_AMOUNT_THB;
                                    dataMatPrice.PMP_CERTIFIED_DATE = string.IsNullOrEmpty(itemPrice.i_CERTIFIED_DATE) ? dataMatPrice.PMP_CERTIFIED_DATE : DateTime.ParseExact(itemPrice.i_CERTIFIED_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMatPrice.PMP_APPROVED_DATE = string.IsNullOrEmpty(itemPrice.i_APPROVED_DATE) ? dataMatPrice.PMP_APPROVED_DATE : DateTime.ParseExact(itemPrice.i_APPROVED_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMatPrice.PMP_DUE_DATE = string.IsNullOrEmpty(itemPrice.h_DueDate) ? dataMatPrice.PMP_DUE_DATE : DateTime.ParseExact(itemPrice.h_DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMatPrice.PMP_INVOICE_NO = string.IsNullOrEmpty(itemPrice.INVOICE_NO) ? "" : itemPrice.INVOICE_NO;
                                    dataMatPrice.PMP_TOTALPRICE_DIGIT = string.IsNullOrEmpty(itemPrice.UnitPriceDigit) ? dataMatPrice.PMP_TOTALPRICE_DIGIT : Convert.ToDecimal(itemPrice.UnitPriceDigit);

                                    dataMatPrice.PMP_PREPAYMENT = (checkNumeric(itemPrice.i_PREPAYMENT)) ? Convert.ToDecimal(itemPrice.i_PREPAYMENT.Replace(",", "")) : dataMatPrice.PMP_PREPAYMENT;
                                    dataMatPrice.PMP_PREPAYMENT_CURRENCY = itemPrice.i_PREPAYMENT_CURRENCY;
                                    dataMatPrice.PMP_INVOICE = itemPrice.i_INVOICE;
                                    dataMatPrice.PMP_CREATED_DATE = dtNow;
                                    dataMatPrice.PMP_CREATED_BY = User;
                                    dataMatPrice.PMP_UPDATED_DATE = dtNow;
                                    dataMatPrice.PMP_UPDATED_BY = User;

                                    string sDateFrom = "";
                                    string sDateTo = "";
                                    SplitDateFromTo(itemPrice.FXPeriodDate, ref sDateFrom, ref sDateTo);
                                    dataMatPrice.PMP_FX_DATE_FROM = string.IsNullOrEmpty(sDateFrom) ? dataMatPrice.PMP_FX_DATE_FROM : DateTime.ParseExact(sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataMatPrice.PMP_FX_DATE_TO = string.IsNullOrEmpty(sDateTo) ? dataMatPrice.PMP_FX_DATE_TO : DateTime.ParseExact(sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataMatPrice.PMP_PHET_VOL = (checkNumeric(itemPrice.PMP_PHET_VOL)) ? Convert.ToDecimal(itemPrice.PMP_PHET_VOL.Replace(",", "")) : dataMatPrice.PMP_PHET_VOL;

                                    dataMatPrice.PMP_CAL_TOTAL_PRICE = (checkNumeric(itemPrice.CalTotalPrice)) ? Convert.ToDecimal(itemPrice.CalTotalPrice.Replace(",", "")) : dataMatPrice.PMP_CAL_TOTAL_PRICE;
                                    dataMatPrice.PMP_OSP_MONTH = itemPrice.OSP_MONTH_PRICE ?? "";

                                    if (!string.IsNullOrEmpty(itemPrice.PriceUpdateDate))
                                        dataMatPrice.PMP_PRICE_UPDATE = dtNow;
                                     

                                    dataDAL.Save(dataMatPrice, context);
                                    //context.SaveChanges();
                                }
                            }
                            #endregion

                            #region "PCF_FREIGHT"

                            if (dataDetail.crude_approve_detail.dDetail_Freight != null && HeadStatus != "DELETE")
                            {
                                foreach (FreightViewModel itemFreight in dataDetail.crude_approve_detail.dDetail_Freight)
                                {
                                    PCF_FREIGHT dataFreight = new PCF_FREIGHT();
                                    dataFreight.PFR_TRIP_NO = tripNo;
                                    dataFreight.PFR_ITEM_NO = Convert.ToDecimal(itemFreight.FR_ItemNo);
                                    dataFreight.PFR_CHARTERING_TYPE = itemFreight.FR_CharteringType;// "1";
                                    dataFreight.PFR_FREIGHT_TYPE = (checkNumeric(itemFreight.FR_FreightType)) ? Convert.ToDecimal(itemFreight.FR_FreightType.Replace(",", "")) : dataFreight.PFR_FREIGHT_TYPE;
                                    dataFreight.PFR_VESSEL = "1000000307";
                                    dataFreight.PFR_SUPPLIER = itemFreight.FR_Owner;
                                    dataFreight.PFR_BROKER = itemFreight.FR_Broker; // "1000014";
                                    dataFreight.PFR_LAYTIME = itemFreight.FR_LayTime;

                                    dataFreight.PFR_WORLD_SCALE = (checkNumeric(itemFreight.FR_WorldScale)) ? Convert.ToDecimal(itemFreight.FR_WorldScale.Replace(",", "")) : dataFreight.PFR_WORLD_SCALE;
                                    dataFreight.PFR_MINIMUM_LOADABLE = (checkNumeric(itemFreight.FR_MinLoadable)) ? Convert.ToDecimal(itemFreight.FR_MinLoadable.Replace(",", "")) : dataFreight.PFR_MINIMUM_LOADABLE;
                                    dataFreight.PFR_DEMURRAGE = (checkNumeric(itemFreight.FR_Demurrange)) ? Convert.ToDecimal(itemFreight.FR_Demurrange) : dataFreight.PFR_DEMURRAGE;
                                    dataFreight.PFR_WAR_RISK_PREMIUM = (checkNumeric(itemFreight.FR_WarRiskPremium)) ? Convert.ToDecimal(itemFreight.FR_WarRiskPremium.Replace(",", "")) : dataFreight.PFR_WAR_RISK_PREMIUM;
                                    dataFreight.PFR_PRICE_PER_TON = (checkNumeric(itemFreight.FR_Price)) ? Convert.ToDecimal(itemFreight.FR_Price.Replace(",", "")) : dataFreight.PFR_PRICE_PER_TON;
                                    dataFreight.PFR_AMOUNT = (checkNumeric(itemFreight.FR_Amt)) ? Convert.ToDecimal(itemFreight.FR_Amt.Replace(",", "")) : dataFreight.PFR_AMOUNT;

                                    dataFreight.PFR_CURRENCY = itemFreight.FR_AmtUnit;

                                    dataFreight.PFR_CF_CURRENCY = itemFreight.FR_CoAmtUnit;
                                    dataFreight.PFR_DUE_DATE = string.IsNullOrEmpty(itemFreight.FR_DueDate) ? dataFreight.PFR_DUE_DATE : DateTime.ParseExact(itemFreight.FR_DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;

                                    dataFreight.PFR_FX_AGREEMENT = (checkNumeric(itemFreight.FR_ExcAgree)) ? Convert.ToDecimal(itemFreight.FR_ExcAgree.Replace(",", "")) : dataFreight.PFR_FX_AGREEMENT;

                                    dataFreight.PFR_FX_BOT = (checkNumeric(itemFreight.FR_ExcBOT)) ? Convert.ToDecimal(itemFreight.FR_ExcBOT.Replace(",", "")) : dataFreight.PFR_FX_BOT;

                                    dataFreight.PFR_TOTAL_AMOUNT_THB = (checkNumeric(itemFreight.FR_ExcTotal)) ? Convert.ToDecimal(itemFreight.FR_ExcTotal.Replace(",", "")) : dataFreight.PFR_TOTAL_AMOUNT_THB;
                                    dataFreight.PFR_CERTIFIED_DATE = string.IsNullOrEmpty(itemFreight.FR_ExcCertiDate) ? dataFreight.PFR_CERTIFIED_DATE : DateTime.ParseExact(itemFreight.FR_ExcCertiDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataFreight.PFR_APPROVED_DATE = string.IsNullOrEmpty(itemFreight.FR_ExcApproveDate) ? dataFreight.PFR_APPROVED_DATE : DateTime.ParseExact(itemFreight.FR_ExcApproveDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataFreight.PFR_REMARK = itemFreight.FR_Remark;
                                    dataFreight.PFR_MEMO_NO = itemFreight.FR_MemoNo;
                                    dataFreight.PFR_PO_NO = itemFreight.FR_PONo;
                                    dataFreight.PFR_CREATED_DATE = dtNow;
                                    dataFreight.PFR_CREATED_BY = User;
                                    dataFreight.PFR_UPDATED_DATE = dtNow;
                                    dataFreight.PFR_UPDATED_BY = User;
                                    dataFreight.PFR_FREIGHT_SUB_TYPE = (checkNumeric(itemFreight.FR_CostType)) ? Convert.ToDecimal(itemFreight.FR_CostType.Replace(",", "")) : dataFreight.PFR_FREIGHT_SUB_TYPE;
                                    dataFreight.PFR_PRODUCT = string.IsNullOrEmpty(itemFreight.FR_Product) ? "" : itemFreight.FR_Product; // (checkNumeric(itemFreight.FR_Product)) ? Convert.ToDecimal(itemFreight.FR_Product.Replace(",", "")) : dataFreight.PFR_PRODUCT;
                                    dataFreight.PFR_PORT = (checkNumeric(itemFreight.FR_Port)) ? Convert.ToDecimal(itemFreight.FR_Port.Replace(",", "")) : dataFreight.PFR_PORT;

                                    string sDateFrom = "";
                                    string sDateTo = "";
                                    SplitDateFromTo(itemFreight.FR_PeriodDate, ref sDateFrom, ref sDateTo);
                                    dataFreight.PFR_PERIOD_FROM = string.IsNullOrEmpty(sDateFrom) ? dataFreight.PFR_PERIOD_FROM : DateTime.ParseExact(sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataFreight.PFR_PERIOD_TO = string.IsNullOrEmpty(sDateTo) ? dataFreight.PFR_PERIOD_TO : DateTime.ParseExact(sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    //dataFreight.PFR_PERIOD_FROM = dtNow;
                                    //dataFreight.PFR_PERIOD_TO = dtNow;
                                    dataFreight.PFR_VOLUME = (checkNumeric(itemFreight.FR_Volume)) ? Convert.ToDecimal(itemFreight.FR_Volume.Replace(",", "")) : dataFreight.PFR_VOLUME;
                                    dataFreight.PFR_VOLUME_UNIT = itemFreight.FR_VolumeUnit;
                                    dataFreight.PFR_PRICE_PER_BBL = 0;
                                    dataFreight.PFR_INVOICE_STATUS = itemFreight.FR_InvoiceSTS;
                                    dataFreight.PFR_PRICE_UNIT = string.IsNullOrEmpty(itemFreight.FR_PriceUnit) ? "" : itemFreight.FR_PriceUnit;
                                    dataFreight.PFR_INVOICE_NO = string.IsNullOrEmpty(itemFreight.FR_InvoiceNo) ? "" : itemFreight.FR_InvoiceNo;

                                    dataDAL.Save(dataFreight, context);
                                    //context.SaveChanges();
                                    if (!string.IsNullOrEmpty(itemFreight.FR_MatCodeList))
                                    {

                                        string[] arrFreightMat = itemFreight.FR_MatCodeList.Split(',');
                                        int FreightMatCount = arrFreightMat.Length;
                                        if (FreightMatCount > 0)
                                        {
                                            for (int i = 0; i < FreightMatCount; i++)
                                            {
                                                if (string.IsNullOrEmpty(arrFreightMat[i].Trim()))
                                                    continue;

                                                PCF_FREIGHT_MATERIAL dataFreightMat = new PCF_FREIGHT_MATERIAL();
                                                dataFreightMat.PFM_TRIP_NO = tripNo;
                                                dataFreightMat.PFM_ITEM_NO = Convert.ToDecimal(itemFreight.FR_ItemNo);
                                                dataFreightMat.PFM_MET_NUM = arrFreightMat[i].Trim();
                                                dataFreightMat.PFM_CREATED_DATE = dtNow;
                                                dataFreightMat.PFM_CREATED_BY = User;
                                                dataFreightMat.PFM_UPDATED_DATE = dtNow;
                                                dataFreightMat.PFM_UPDATED_BY = User;
                                                dataDAL.Save(dataFreightMat, context);
                                                //context.SaveChanges();
                                            }
                                        }
                                    }

                                }

                                if (dataDetail.crude_approve_detail.dDetail_FreightFile != null) {
                                    int no = 0;
                                    foreach (FileViewModel itemFF in dataDetail.crude_approve_detail.dDetail_FreightFile)
                                    {
                                        string PAF_STATUS = itemFF.PAF_STATUS ?? "";
                                        if (PAF_STATUS.Equals("D"))
                                            continue;

                                        no++;                                       

                                        PCF_ATTACH_FILE dataAttFile = new PCF_ATTACH_FILE();
                                        dataAttFile.PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        dataAttFile.PAF_ITEM_NO = checkNumeric(itemFF.PAF_ITEM_NO) ? Convert.ToDecimal(itemFF.PAF_ITEM_NO) : 0;
                                        dataAttFile.PAF_NO = no;
                                        dataAttFile.PAF_FILE_FOR = itemFF.PAF_FILE_FOR ?? "";
                                        dataAttFile.PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        dataAttFile.PAF_PATH = itemFF.PAF_PATH ?? "";
                                        dataAttFile.PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        dataDAL.Save(dataAttFile, context);

                                        //string PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        //string PAF_ITEM_NO = itemFF.PAF_ITEM_NO ?? "";
                                        //string PAF_NO = no.ToString();
                                        //string PAF_FILE_FOR = "FREIGHT";
                                        //string PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        //string PAF_PATH = itemFF.PAF_PATH ?? "";
                                        //string PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        //string PAF_UPLOAD_DATE = "sysdate";

                                        //string sql = "insert into PCF_ATTACH_FILE (PAF_TRIP_NO, PAF_FILE_FOR, PAF_ITEM_NO, PAF_FILE_NAME, PAF_PATH, PAF_NO, PAF_DESCRIPTION, PAF_UPLOAD_DATE) ";
                                        //sql += " values ('" + PAF_TRIP_NO + "', '" + PAF_FILE_FOR + "', '" + PAF_ITEM_NO + "', '" + PAF_FILE_NAME + "', '" + PAF_PATH + "', '" + PAF_NO + "', '" + PAF_DESCRIPTION + "', " + PAF_UPLOAD_DATE + " ) ";
                                        //dataDAL.SaveAttachFile(sql, context);
                                         
                                    }
                                }
                                
                            }



                            #endregion

                            #region "PCF_SURVEYOR"

                            if (dataDetail.crude_approve_detail.dDetail_Surveyor != null && HeadStatus != "DELETE")
                            {
                                foreach (SurveyorViewModel ItemSur in dataDetail.crude_approve_detail.dDetail_Surveyor)
                                {
                                    string isdelete = ItemSur.IS_DELETE == null ? "" : ItemSur.IS_DELETE;
                                    string sPONO = ItemSur.Sur_PO == null ? "" : ItemSur.Sur_PO;
                                    if (isdelete.Equals("X") && sPONO.Equals("")) {
                                        continue;
                                    }
                                    PCF_PO_SURVEYOR dataSurveyor = new PCF_PO_SURVEYOR();
                                    dataSurveyor.PPS_TRIP_NO = tripNo;
                                    dataSurveyor.PPS_ITEM_NO = Convert.ToDecimal(ItemSur.Sur_ItemNo);
                                    if (ItemSur.Sur_Type != "")
                                        dataSurveyor.PPS_PO_SERVEYOR_TYPE = Convert.ToDecimal(ItemSur.Sur_Type);


                                    //dataSurveyor.PPS_LOADING_DATE = (string.IsNullOrEmpty(ItemSur.Sur_LoadDate)) ? dataSurveyor.PPS_LOADING_DATE : DateTime.ParseExact(ItemSur.Sur_LoadDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    string sDateFrom = "";
                                    string sDateTo = "";
                                    SplitDateFromTo(ItemSur.Sur_LoadDate, ref sDateFrom, ref sDateTo);
                                    dataSurveyor.PPS_LOADING_FROM_DATE = string.IsNullOrEmpty(sDateFrom) ? dataSurveyor.PPS_LOADING_FROM_DATE : DateTime.ParseExact(sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataSurveyor.PPS_LOADING_TO_DATE = string.IsNullOrEmpty(sDateTo) ? dataSurveyor.PPS_LOADING_TO_DATE : DateTime.ParseExact(sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                                    dataSurveyor.PPS_LOADING_PORT = (checkNumeric(ItemSur.Sur_LoadPort)) ? Convert.ToDecimal(ItemSur.Sur_LoadPort.Replace(",", "")) : dataSurveyor.PPS_LOADING_PORT;
                                    //dataSurveyor.PPS_DISCHARGING_DATE = (string.IsNullOrEmpty(ItemSur.Sur_DischargeDate)) ? dataSurveyor.PPS_DISCHARGING_DATE : DateTime.ParseExact(ItemSur.Sur_DischargeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    sDateFrom = "";
                                    sDateTo = "";
                                    SplitDateFromTo(ItemSur.Sur_DischargeDate, ref sDateFrom, ref sDateTo);
                                    dataSurveyor.PPS_DISCHARGING_FROM_DATE = string.IsNullOrEmpty(sDateFrom) ? dataSurveyor.PPS_DISCHARGING_FROM_DATE : DateTime.ParseExact(sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataSurveyor.PPS_DISCHARGING_FROM_TO = string.IsNullOrEmpty(sDateTo) ? dataSurveyor.PPS_DISCHARGING_FROM_TO : DateTime.ParseExact(sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataSurveyor.PPS_DISCHARGING_PORT = (checkNumeric(ItemSur.Sur_DiscPort)) ? Convert.ToDecimal(ItemSur.Sur_DiscPort.Replace(",", "")) : dataSurveyor.PPS_DISCHARGING_PORT;
                                    dataSurveyor.PPS_VESSEL = string.IsNullOrEmpty(ItemSur.Sur_Vessel) ? null : ItemSur.Sur_Vessel;
                                    dataSurveyor.PPS_SUPPLIER = string.IsNullOrEmpty(ItemSur.Sur_Surveyor) ? null : ItemSur.Sur_Surveyor;  // "1000090";
                                                                                                                                           //if (ItemSur.Sur_Amount != "")
                                    dataSurveyor.PPS_AMOUNT = (checkNumeric(ItemSur.Sur_Amount)) ? Convert.ToDecimal(ItemSur.Sur_Amount.Replace(",", "")) : 0;

                                    dataSurveyor.PPS_CURRENCY = ItemSur.Sur_AmountUnit;
                                     
                                    dataSurveyor.PPS_COST_SHARING = (checkNumeric(ItemSur.Sur_CostSharing)) ? Convert.ToDecimal(ItemSur.Sur_CostSharing.Replace(",", "")) : 0;

                                    dataSurveyor.PPS_INVOICE_NO = string.IsNullOrEmpty(ItemSur.INVOICE_NO) ? "" : ItemSur.INVOICE_NO;

                                    dataSurveyor.PPS_INVOICE_STATUS = ItemSur.Sur_InvoiceSTS;
                                    dataSurveyor.PPS_REMARK = ItemSur.Sur_Remark;
                                    dataSurveyor.PPS_PO_NO = ItemSur.Sur_PO;
                                    dataSurveyor.PPS_CREATED_DATE = dtNow;
                                    dataSurveyor.PPS_CREATED_BY = User;
                                    dataSurveyor.PPS_UPDATED_DATE = dtNow;
                                    dataSurveyor.PPS_UPDATED_BY = User;
                                    dataDAL.Save(dataSurveyor, context);
                                    //context.SaveChanges();
                                    if (ItemSur.Sur_SurMat != null)
                                    {
                                        string[] arrSurmat = ItemSur.Sur_SurMat.Split(',');
                                        int SurMatCount = arrSurmat.Length;
                                        if (SurMatCount > 0)
                                        {
                                            for (int i = 0; i < SurMatCount; i++)
                                            {
                                                if (string.IsNullOrEmpty(arrSurmat[i].Trim()))
                                                    continue;

                                                PCF_PO_SURVEYOR_MATERIAL dataSurveyorMat = new PCF_PO_SURVEYOR_MATERIAL();
                                                dataSurveyorMat.PSM_TRIP_NO = tripNo;
                                                dataSurveyorMat.PSM_ITEM_NO = Convert.ToDecimal(ItemSur.Sur_ItemNo);
                                                dataSurveyorMat.PSM_MET_NUM = arrSurmat[i].Trim();
                                                dataSurveyorMat.PSM_CREATED_DATE = dtNow;
                                                dataSurveyorMat.PSM_CREATED_BY = User;
                                                dataSurveyorMat.PSM_UPDATED_DATE = dtNow;
                                                dataSurveyorMat.PSM_UPDATED_BY = User;
                                                dataDAL.Save(dataSurveyorMat, context);
                                                //context.SaveChanges();
                                            }
                                        }
                                    }

                                }

                                if (dataDetail.crude_approve_detail.dDetail_SurveyorFile != null && HeadStatus != "DELETE")
                                {
                                    int no = 0;
                                    foreach (FileViewModel itemFF in dataDetail.crude_approve_detail.dDetail_SurveyorFile)
                                    {
                                        string PAF_STATUS = itemFF.PAF_STATUS ?? "";
                                        if (PAF_STATUS.Equals("D"))
                                            continue;

                                        no++;

                                        PCF_ATTACH_FILE dataAttFile = new PCF_ATTACH_FILE();
                                        dataAttFile.PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        dataAttFile.PAF_ITEM_NO = checkNumeric(itemFF.PAF_ITEM_NO) ? Convert.ToDecimal(itemFF.PAF_ITEM_NO) : 0;
                                        dataAttFile.PAF_NO = no;
                                        dataAttFile.PAF_FILE_FOR = itemFF.PAF_FILE_FOR ?? "";
                                        dataAttFile.PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        dataAttFile.PAF_PATH = itemFF.PAF_PATH ?? "";
                                        dataAttFile.PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        dataDAL.Save(dataAttFile, context);

                                        //string PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        //string PAF_ITEM_NO = itemFF.PAF_ITEM_NO ?? "";
                                        //string PAF_NO = no.ToString();
                                        //string PAF_FILE_FOR = "SURVEYOR";
                                        //string PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        //string PAF_PATH = itemFF.PAF_PATH ?? "";
                                        //string PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        //string PAF_UPLOAD_DATE = "sysdate";

                                        //string sql = "insert into PCF_ATTACH_FILE (PAF_TRIP_NO, PAF_FILE_FOR, PAF_ITEM_NO, PAF_FILE_NAME, PAF_PATH, PAF_NO, PAF_DESCRIPTION, PAF_UPLOAD_DATE) ";
                                        //sql += " values ('" + PAF_TRIP_NO + "', '" + PAF_FILE_FOR + "', '" + PAF_ITEM_NO + "', '" + PAF_FILE_NAME + "', '" + PAF_PATH + "', '" + PAF_NO + "', '" + PAF_DESCRIPTION + "', " + PAF_UPLOAD_DATE + " ) ";
                                        //dataDAL.SaveAttachFile(sql, context);
                                        ////dataDAL.Save(DataAT, context);
                                    }
                                }


                            }
                            #endregion

                            #region "PCF_OTHER_COST"
                            if (dataDetail.crude_approve_detail.dDetail_Other != null && HeadStatus != "DELETE")
                            {
                                foreach (OtherViewModel ItemOth in dataDetail.crude_approve_detail.dDetail_Other)
                                {
                                    PCF_OTHER_COST dataOther = new PCF_OTHER_COST();
                                    dataOther.POC_TRIP_NO = tripNo;

                                    dataOther.POC_ITEM_NO = (checkNumeric(ItemOth.OC_ItemNo)) ? Convert.ToDecimal(ItemOth.OC_ItemNo.Replace(",", "")) : dataOther.POC_ITEM_NO;
                                    dataOther.POC_OTHER_COST_TYPE = (checkNumeric(ItemOth.OC_Type)) ? Convert.ToDecimal(ItemOth.OC_Type) : dataOther.POC_OTHER_COST_TYPE;
                                    dataOther.POC_SUPPLIER = ItemOth.OC_Vendor;
                                    dataOther.POC_AMOUNT = (checkNumeric(ItemOth.OC_Amount)) ? Convert.ToDecimal(ItemOth.OC_Amount) : dataOther.POC_AMOUNT;

                                    dataOther.POC_CURRENCY = ItemOth.OC_AmountUnit;
                                    dataOther.POC_DUE_DATE = string.IsNullOrEmpty(ItemOth.OC_DueDate) ? dataOther.POC_DUE_DATE : DateTime.ParseExact(ItemOth.OC_DueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataOther.POC_FX_AGREEMENT = (checkNumeric(ItemOth.OC_FXAgree)) ? Convert.ToDecimal(ItemOth.OC_FXAgree.Replace(",", "")) : dataOther.POC_FX_AGREEMENT;
                                    dataOther.POC_FX_BOT = (checkNumeric(ItemOth.OC_FXBOT)) ? Convert.ToDecimal(ItemOth.OC_FXBOT) : dataOther.POC_FX_BOT;
                                    dataOther.POC_TOTAL_AMOUNT_THB = (checkNumeric(ItemOth.OC_Total)) ? Convert.ToDecimal(ItemOth.OC_Total.Replace(",", "")) : dataOther.POC_TOTAL_AMOUNT_THB;

                                    string sDateFrom = "";
                                    string sDateTo = "";
                                    SplitDateFromTo(ItemOth.OC_OperationDate, ref sDateFrom, ref sDateTo);
                                    dataOther.POC_PERIOD_FROM = string.IsNullOrEmpty(sDateFrom) ? dataOther.POC_PERIOD_FROM : DateTime.ParseExact(sDateFrom, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataOther.POC_PERIOD_TO = string.IsNullOrEmpty(sDateTo) ? dataOther.POC_PERIOD_TO : DateTime.ParseExact(sDateTo, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataOther.POC_CERTIFIED_DATE = string.IsNullOrEmpty(ItemOth.OC_CerDate) ? dataOther.POC_CERTIFIED_DATE : DateTime.ParseExact(ItemOth.OC_CerDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataOther.POC_APPROVED_DATE = string.IsNullOrEmpty(ItemOth.OC_ApprDate) ? dataOther.POC_APPROVED_DATE : DateTime.ParseExact(ItemOth.OC_ApprDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    dataOther.POC_INVOICE_STATUS = ItemOth.OC_InvoiceSTS;
                                    dataOther.POC_REMARK = ItemOth.OC_Remark;
                                    dataOther.POC_MEMO_NO = ItemOth.OC_MemoNo;
                                    dataOther.POC_CREATED_DATE = dtNow;
                                    dataOther.POC_CREATED_BY = User;
                                    dataOther.POC_UPDATED_DATE = dtNow;
                                    dataOther.POC_UPDATED_BY = User;

                                    dataOther.POC_PORT = (checkNumeric(ItemOth.OC_Port)) ? Convert.ToDecimal(ItemOth.OC_Port.Replace(",", "")) : dataOther.POC_PORT;

                                    dataOther.POC_PRICE = (checkNumeric(ItemOth.OC_Price)) ? Convert.ToDecimal(ItemOth.OC_Price.Replace(",", "")) : dataOther.POC_PRICE;
                                    dataOther.POC_HOURS = null;

                                    dataOther.POC_INVOICE_NO = string.IsNullOrEmpty(ItemOth.INVOICE_NO) ? "" : ItemOth.INVOICE_NO;

                                    dataDAL.Save(dataOther, context);
                                    //context.SaveChanges();
                                }

                                if (dataDetail.crude_approve_detail.dDetail_OtherFile != null && HeadStatus != "DELETE")
                                {
                                    int no = 0;
                                    foreach (FileViewModel itemFF in dataDetail.crude_approve_detail.dDetail_OtherFile)
                                    {
                                        string PAF_STATUS = itemFF.PAF_STATUS ?? "";
                                        if (PAF_STATUS.Equals("D"))
                                            continue;

                                        no++;
                                        PCF_ATTACH_FILE dataAttFile = new PCF_ATTACH_FILE();
                                        dataAttFile.PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        dataAttFile.PAF_ITEM_NO = checkNumeric(itemFF.PAF_ITEM_NO) ? Convert.ToDecimal(itemFF.PAF_ITEM_NO) : 0;
                                        dataAttFile.PAF_NO = no;
                                        dataAttFile.PAF_FILE_FOR = itemFF.PAF_FILE_FOR ?? "";
                                        dataAttFile.PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        dataAttFile.PAF_PATH = itemFF.PAF_PATH ?? "";
                                        dataAttFile.PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        dataDAL.Save(dataAttFile, context);

                                        //string PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                        //string PAF_ITEM_NO = itemFF.PAF_ITEM_NO ?? "";
                                        //string PAF_NO = no.ToString();
                                        //string PAF_FILE_FOR = "OTHER";
                                        //string PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                        //string PAF_PATH = itemFF.PAF_PATH ?? "";
                                        //string PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                        //string PAF_UPLOAD_DATE = "sysdate";

                                        //string sql = "insert into PCF_ATTACH_FILE (PAF_TRIP_NO, PAF_FILE_FOR, PAF_ITEM_NO, PAF_FILE_NAME, PAF_PATH, PAF_NO, PAF_DESCRIPTION, PAF_UPLOAD_DATE) ";
                                        //sql += " values ('" + PAF_TRIP_NO + "', '" + PAF_FILE_FOR + "', '" + PAF_ITEM_NO + "', '" + PAF_FILE_NAME + "', '" + PAF_PATH + "', '" + PAF_NO + "', '" + PAF_DESCRIPTION + "', " + PAF_UPLOAD_DATE + " ) ";
                                        //dataDAL.SaveAttachFile(sql, context);
                                        ////dataDAL.Save(DataAT, context);
                                    }
                                }

                            }

                            #endregion

                            #region "PCF_CUSTOM_VAT"

                            if (dataDetail.crude_approve_detail.dDetail_Material != null && HeadStatus != "DELETE")
                            {
                                foreach (var item in dataDetail.crude_approve_detail.dDetail_Material)
                                {
                                    PCF_CUSTOMS_VAT dataCustoms = new PCF_CUSTOMS_VAT();
                                    dataCustoms.TRIP_NO = tripNo;

                                    dataCustoms.MAT_ITEM_NO = (checkNumeric(item.PMA_ITEM_NO)) ? Convert.ToDecimal(item.PMA_ITEM_NO.Replace(",", "")) : dataCustoms.MAT_ITEM_NO;
                                    dataCustoms.CUSTOMS_PRICE = (checkNumeric(item.CST_PRICE)) ? Convert.ToDecimal(item.CST_PRICE.Replace(",", "")) : dataCustoms.CUSTOMS_PRICE;
                                    dataCustoms.FREIGHT_AMOUNT = (checkNumeric(item.CST_FREIGHT_AMOUNT)) ? Convert.ToDecimal(item.CST_FREIGHT_AMOUNT.Replace(",", "")) : dataCustoms.FREIGHT_AMOUNT;
                                    dataCustoms.INSURANCE_AMOUNT = (checkNumeric(item.CST_INSURANCE_AMOUNT)) ? Convert.ToDecimal(item.CST_INSURANCE_AMOUNT) : dataCustoms.INSURANCE_AMOUNT;
                                    dataCustoms.INSURANCE_RATE = (checkNumeric(item.CST_INSURANCE_RATE)) ? Convert.ToDecimal(item.CST_INSURANCE_RATE) : dataCustoms.INSURANCE_RATE;
                                    dataCustoms.INVOICE_STATUS = item.CST_INVOICE_STATUS ?? "";
                                    dataCustoms.BL_DATE = string.IsNullOrEmpty(item.CST_BL_DATE) ? dataCustoms.BL_DATE : DateTime.ParseExact(item.CST_BL_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                                    dataDAL.SaveCustomsVAT(dataCustoms, context);
                                    //dataDAL.Save(dataCustoms, context);
                                    //context.SaveChanges();
                                }
                            }

                            if (dataDetail.crude_approve_detail.dDetail_CustomsFile != null && HeadStatus != "DELETE")
                            {
                                int no = 0;
                                foreach (FileViewModel itemFF in dataDetail.crude_approve_detail.dDetail_CustomsFile)
                                {
                                    string PAF_STATUS = itemFF.PAF_STATUS ?? "";
                                    if (PAF_STATUS.Equals("D"))
                                        continue;

                                    no++;                                    

                                    PCF_ATTACH_FILE dataAttFile = new PCF_ATTACH_FILE();
                                    dataAttFile.PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                    dataAttFile.PAF_ITEM_NO = checkNumeric(itemFF.PAF_ITEM_NO) ? Convert.ToDecimal(itemFF.PAF_ITEM_NO) : 0;
                                    dataAttFile.PAF_NO = no;
                                    dataAttFile.PAF_FILE_FOR = itemFF.PAF_FILE_FOR ?? "";
                                    dataAttFile.PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                    dataAttFile.PAF_PATH = itemFF.PAF_PATH ?? "";
                                    dataAttFile.PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                    dataDAL.Save(dataAttFile, context);

                                    //string PAF_TRIP_NO = itemFF.PAF_TRIP_NO ?? "";
                                    //string PAF_ITEM_NO = itemFF.PAF_ITEM_NO ?? "";
                                    //string PAF_NO = no.ToString();
                                    //string PAF_FILE_FOR = "CUSTOMS";
                                    //string PAF_FILE_NAME = itemFF.PAF_FILE_NAME ?? "";
                                    //string PAF_PATH = itemFF.PAF_PATH ?? "";
                                    //string PAF_DESCRIPTION = itemFF.PAF_DESCRIPTION ?? "";
                                    //string PAF_UPLOAD_DATE = "sysdate";

                                    //string sql = "insert into PCF_ATTACH_FILE (PAF_TRIP_NO, PAF_FILE_FOR, PAF_ITEM_NO, PAF_FILE_NAME, PAF_PATH, PAF_NO, PAF_DESCRIPTION, PAF_UPLOAD_DATE) ";
                                    //sql += " values ('" + PAF_TRIP_NO + "', '" + PAF_FILE_FOR + "', '" + PAF_ITEM_NO + "', '" + PAF_FILE_NAME + "', '" + PAF_PATH + "', '" + PAF_NO + "', '" + PAF_DESCRIPTION + "', " + PAF_UPLOAD_DATE + " ) ";
                                    //dataDAL.SaveAttachFile(sql, context);
                                    
                                }
                            }


                            #endregion

                            #region "PRICE TO SAP"
                            if (UpdatePriceSTS == true && dataDetail.crude_approve_detail.dDetail_PriceToSap != null && HeadStatus != "DELETE")
                            {
                                DateTime createDate = DateTime.Now;
                                String sDateTime = createDate.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US")) + ' ' + createDate.ToString("HH:mm:ss", new System.Globalization.CultureInfo("th-TH"));
                                //TO_DATE('2012-07-18 13:27:18', 'YYYY-MM-DD HH24:MI:SS')
                                //dataDAL.UpdatePrice0(tripNo, sDateTime, context);

                                ////TOP purchase for TOP
                                //if (PurchaseType.Equals("1"))
                                //{
                                //    DateTime createDateInsert = DateTime.Now.AddSeconds(1);
                                //    int i = 0;
                                //    foreach (PriceToSapViewModel item in dataDetail.crude_approve_detail.dDetail_PriceToSap)
                                //    {
                                //        i++;
                                //        PRICING_TO_SAP dataPricing = new PRICING_TO_SAP();
                                //        dataPricing.TRANS_TYPE = item.TRANS_TYPE;
                                //        dataPricing.COM_CODE = item.COM_CODE;
                                //        dataPricing.TRIP_NO = tripNo;
                                //        dataPricing.CREATE_DATE = createDateInsert;
                                //        dataPricing.CMCS_ETA = string.IsNullOrEmpty(item.CMCS_ETA) ? dataPricing.CMCS_ETA : DateTime.ParseExact(item.CMCS_ETA, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //        dataPricing.MAT_NUM = item.MAT_NUM;
                                //        dataPricing.PARTLY_ORDER = checkNumeric(item.PARTLY_ORDER) ? Convert.ToDecimal(item.PARTLY_ORDER) : 0;
                                //        dataPricing.VENDOR_NO = item.VENDOR_NO;
                                //        dataPricing.PLANT = item.PLANT;
                                //        dataPricing.GROUP_LOCATION = item.GROUP_LOCATION;
                                //        dataPricing.EXCHANGE_RATE = checkNumeric(item.EXCHANGE_RATE) ? Convert.ToDecimal(item.EXCHANGE_RATE) : 0;
                                //        dataPricing.BASE_PRICE = checkNumeric(item.BASE_PRICE) ? Convert.ToDecimal(item.BASE_PRICE) : 0;
                                //        dataPricing.PREMIUM_OSP = checkNumeric(item.PREMIUM_OSP) ? Convert.ToDecimal(item.PREMIUM_OSP) : 0;
                                //        dataPricing.PREMIUM_MARKET = checkNumeric(item.PREMIUM_MARKET) ? Convert.ToDecimal(item.PREMIUM_MARKET) : 0;
                                //        dataPricing.OTHER_COST = checkNumeric(item.OTHER_COST) ? Convert.ToDecimal(item.OTHER_COST) : 0;
                                //        dataPricing.FREIGHT_PRICE = checkNumeric(item.FREIGHT_PRICE) ? Convert.ToDecimal(item.FREIGHT_PRICE) : 0;
                                //        dataPricing.INSURANCE_PRICE = checkNumeric(item.INSURANCE_PRICE) ? Convert.ToDecimal(item.INSURANCE_PRICE) : 0;
                                //        dataPricing.TOTAL_AMOUNT_THB = checkNumeric(item.TOTAL_AMOUNT_THB) ? Convert.ToDecimal(item.TOTAL_AMOUNT_THB) : 0;
                                //        dataPricing.TOTAL_AMOUNT_USD = checkNumeric(item.TOTAL_AMOUNT_USD) ? Convert.ToDecimal(item.TOTAL_AMOUNT_USD) : 0;
                                //        dataPricing.QTY = checkNumeric(item.QTY) ? Convert.ToDecimal(item.QTY) : 0;
                                //        dataPricing.QTY_UNIT = item.QTY_UNIT;
                                //        dataPricing.CURRENCY = item.CURRENCY;
                                //        //dataPricing.SAP_RECEIVED_DATE = item.SAP_RECEIVED_DATE;
                                //        dataPricing.STATUS = item.STATUS;
                                //        dataPricing.GROUP_DATA = checkNumeric(item.GROUP_DATA) ? Convert.ToDecimal(item.GROUP_DATA) : 0;
                                //        dataPricing.FLAG_PLANNING = item.FLAG_PLANNING;
                                //        dataPricing.DUE_DATE = string.IsNullOrEmpty(item.DUE_DATE) ? dataPricing.DUE_DATE : DateTime.ParseExact(item.DUE_DATE, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //        dataPricing.DATA_FROM = item.DATA_FROM;
                                //        dataPricing.CODE = i;
                                //        dataPricing.REMARK = item.REMARK;

                                //        dataDAL.Save(dataPricing, context);
                                //    }
                                //}

                            }
                            #endregion

                            context.SaveChanges();

                            dbContextTransaction.Commit();
                            isSuccess = true;

                            string datafrom = "";
                            if (dataDetail.crude_approve_detail.dDetail_PriceToSap != null && dataDetail.crude_approve_detail.dDetail_PriceToSap.Count > 0) 
                                datafrom = dataDetail.crude_approve_detail.dDetail_PriceToSap.FirstOrDefault().DATA_FROM ?? "";


                            InsertVesselPrice(tripNo, datafrom, dataDetail.crude_approve_detail.dDetail_Material, context, dataDAL);

                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }
            #endregion
            log.Info("# End State CPAIImportPlanCreateDataState >> InsertDB # ");
            return isSuccess;


        }

        private void InsertVesselPrice( string tripno, string datafrom , List<MaterialViewModel> PCFMaterailModel, EntityCPAIEngine context, CRUDE_IMPORT_PLAN_DAL dataDAL) {
            try
            {
                if (PCFMaterailModel == null)
                    return;

                foreach (var item in PCFMaterailModel) {
                    dataDAL.InsertVesselPrice(tripno, item.PMA_MET_NUM ?? "", datafrom, context);
                }
            }
            catch (Exception ex) {

            }
        }



        //public bool InsertDB_bk(string TransID, Dictionary<string, ExtendValue> etxValue, CrudeApproveFormViewModel_Detail dataDetail, double AvgWS)
        //{
        //    log.Info("# Start State CPAIImportPlanCreateDataState >> InsertDB #  ");
        //    #region Insert Data into DB
        //    DateTime dtNow = DateTime.Now;
        //    bool isSuccess = false;

        //    try
        //    {
        //        using (var context = new EntityCPAIEngine())
        //        {
        //            using (var dbContextTransaction = context.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    string User = etxValue.GetValue(CPAIConstantUtil.User);

        //                    CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
        //                    dataDAL.Delete(TransID);

        //                    #region CPAI_VESSEL_SCHEDULE
        //                    DateTime dtLaycanLoadFrom = DateTime.MinValue;
        //                    CPAI_TCE_MK dataSCH = new CPAI_TCE_MK();
        //                    dataSCH.CTM_ROW_ID = TransID;
        //                    dataSCH.CTM_TASK_NO = etxValue.GetValue(CPAIConstantUtil.DocNo);
        //                    dataSCH.CTM_FK_MT_VEHICLE = dataDetail.tce_mk_detail.vessel;

        //                    if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.date_from))
        //                        dataSCH.CTM_DATE_FROM = DateTime.ParseExact(dataDetail.tce_mk_detail.date_from, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //                    if (!string.IsNullOrEmpty(dataDetail.tce_mk_detail.date_to))
        //                        dataSCH.CTM_DATE_TO = DateTime.ParseExact(dataDetail.tce_mk_detail.date_to, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        //                    dataSCH.CTM_TD = dataDetail.tce_mk_detail.td;
        //                    dataSCH.CTM_MONTH = dataDetail.tce_mk_detail.month;
        //                    dataSCH.CTM_TC_RATE = dataDetail.tce_mk_detail.tc_rate;
        //                    dataSCH.CTM_OPERATING_DAYS = dataDetail.tce_mk_detail.operating_days;
        //                    dataSCH.CTM_BUNKER_FO = dataDetail.tce_mk_detail.bunker_cost_fo;
        //                    dataSCH.CTM_BUNKER_GO = dataDetail.tce_mk_detail.bunker_cost_go;
        //                    dataSCH.CTM_OTHER_COST = dataDetail.tce_mk_detail.other_cost;
        //                    dataSCH.CTM_FLAT_RATE = dataDetail.tce_mk_detail.flat_rate;
        //                    dataSCH.CTM_MINLOAD = dataDetail.tce_mk_detail.minload;
        //                    dataSCH.CTM_THEORITICAL_DAYS = dataDetail.tce_mk_detail.theoritical_days;
        //                    dataSCH.CTM_FO_MT = dataDetail.tce_mk_detail.fo_mt;
        //                    dataSCH.CTM_GO_MT = dataDetail.tce_mk_detail.go_mt;
        //                    dataSCH.CTM_DEMURRAGE = dataDetail.tce_mk_detail.demurrage;

        //                    dataSCH.CTM_TC_NUM = dataDetail.tce_mk_detail.tc_num;
        //                    dataSCH.CTM_FO_UNIT_PRICE = dataDetail.tce_mk_detail.fo_unit_price;
        //                    dataSCH.CTM_GO_UNIT_PRICE = dataDetail.tce_mk_detail.go_unit_price;

        //                    #region Calcualate
        //                    //Time charter cost = TC rate - Operating days;
        //                    //Total Port Cost (incl.Discharge Port) = Port charge 1 + Port charge 2 + Port charge 3 + Discharge port
        //                    //Total Bunker cost = Bunker used - FO + Bunker used - GO
        //                    //TOTAL COST = Time charter cost + Port charge 1 + Port charge 2 + Discharge port + Total Bunker cost
        //                    //WS BITR TD (M-1) = (SELECT MCR_TD?? -> CTM_TD FROM MT_CRUDE_ROUTES WHERE CTM_MONTH = MCR_DATE.MONTH - 1) / COUNT
        //                    //Total Market Cost (2) By Annie (2015-2016) = (WS BITR TD (M-1) / 100) * Flat rate * 265000
        //                    //Saving/Loss from Market (2)-(1) = Total Market Cost (2) By Annie (2015-2016) - TOTAL COST
        //                    //SPOT Variable cost (bunker+P/D) = Total Port Cost (incl.Discharge Port) - Total Bunker cost
        //                    //TCE benefit/Day = ((Total Market Cost (2) By Annie (2015-2016) / 35) - (SPOT Variable cost (bunker+P/D) / Operating days)) - TC rate
        //                    //TCE benefit/Voy = TCE benefit/Day * Operating days
        //                    //TCE / Day = TC rate + TCE benefit/Day

        //                    //initial values
        //                    double tc_rate = 0, operating_days = 0, bunker_fo = 0, bunker_go = 0, flat_rate = 0;
        //                    double.TryParse(dataDetail.tce_mk_detail.tc_rate, out tc_rate);
        //                    double.TryParse(dataDetail.tce_mk_detail.operating_days, out operating_days);
        //                    double.TryParse(dataDetail.tce_mk_detail.bunker_cost_fo, out bunker_fo);
        //                    double.TryParse(dataDetail.tce_mk_detail.bunker_cost_go, out bunker_go);
        //                    double.TryParse(dataDetail.tce_mk_detail.flat_rate, out flat_rate);

        //                    int theoritical_days = 0, minload = 0;
        //                    int.TryParse(dataDetail.tce_mk_detail.theoritical_days, out theoritical_days);
        //                    int.TryParse(dataDetail.tce_mk_detail.minload, out minload);

        //                    List<double> port_charge = new List<double>();
        //                    if (dataDetail.tce_mk_detail.load_ports != null)
        //                    {
        //                        foreach (TceMkLoadPort p in dataDetail.tce_mk_detail.load_ports)
        //                        {
        //                            double port_value = 0;
        //                            double.TryParse(p.port_value, out port_value);
        //                            port_charge.Add(port_value);
        //                        }
        //                    }

        //                    List<double> discharge_port = new List<double>();
        //                    if (dataDetail.tce_mk_detail.dis_ports != null)
        //                    {
        //                        foreach (TceMkDisPort p in dataDetail.tce_mk_detail.dis_ports)
        //                        {
        //                            double port_value = 0;
        //                            double.TryParse(p.port_value, out port_value);
        //                            discharge_port.Add(port_value);
        //                        }
        //                    }

        //                    //Time charter cost
        //                    double timeCharterCost = tc_rate * operating_days;
        //                    double totalLoadPort = port_charge.Count > 0 ? port_charge.Sum() : 0;
        //                    double totalDisPort = discharge_port.Count > 0 ? discharge_port.Sum() : 0;
        //                    double totalPortCost = totalLoadPort + totalDisPort;
        //                    double totalBunkerCost = bunker_go + bunker_fo;

        //                    //Total freight cost (1)
        //                    double totalCost = timeCharterCost + totalLoadPort + totalDisPort + totalBunkerCost;

        //                    //WS BITR TD2 (M-1)
        //                    double WS_BITR_TD = AvgWS;  //double WS_BITR_TD = dataDAL.CalEverageWS(DateTime.ParseExact(dataDetail.tce_mk_detail.month, "MM/yyyy", CultureInfo.InvariantCulture), dataDetail.tce_mk_detail.td);

        //                    //Total Market cost (2)
        //                    double totalMarketCost = (WS_BITR_TD / 100) * flat_rate * minload;

        //                    //Saving/Loss from Market (2)-(1)
        //                    double SavingLossFromMarket = totalMarketCost - totalCost;

        //                    //SPOT Variable cost (bunker+P/D)
        //                    double SPOTVariableCost = totalPortCost + totalBunkerCost;

        //                    //TCE benefit/day
        //                    double TCEBenefitDay = ((totalMarketCost / theoritical_days) - (SPOTVariableCost / operating_days)) - tc_rate;
        //                    double TCEBenefitVoy = TCEBenefitDay * operating_days;
        //                    double TCEDay = tc_rate + TCEBenefitDay;

        //                    #endregion

        //                    dataSCH.CTM_TIME_CHARTER_COST = timeCharterCost.ToString();
        //                    dataSCH.CTM_TOTAL_PORT_COST = totalPortCost.ToString();
        //                    dataSCH.CTM_TOTAL_BUNKER_COST = totalBunkerCost.ToString();
        //                    dataSCH.CTM_TOTAL_COST = totalCost.ToString();
        //                    dataSCH.CTM_WS_BITR_TD = WS_BITR_TD.ToString();
        //                    dataSCH.CTM_TOTAL_MARKET_COST = totalMarketCost.ToString();
        //                    dataSCH.CTM_SAVING_LOSS_MARKET = SavingLossFromMarket.ToString();
        //                    dataSCH.CTM_SPOT_VARIABLE_COST = SPOTVariableCost.ToString();
        //                    dataSCH.CTM_TCE_BENEFIT_DAY = TCEBenefitDay.ToString();
        //                    dataSCH.CTM_TCE_BENEFIT_VOY = TCEBenefitVoy.ToString();
        //                    dataSCH.CTM_TCE_DAY = TCEDay.ToString();
        //                    dataSCH.CTM_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
        //                    dataSCH.CTM_CREATED_DATE = dtNow;
        //                    dataSCH.CTM_CREATED_BY = User;
        //                    dataSCH.CTM_UPDATED_DATE = dtNow;
        //                    dataSCH.CTM_UPDATED_BY = User;

        //                    dataDAL.Save(dataSCH, context);
        //                    #endregion

        //                    #region TCE_MK_LOAD_PORT
        //                    CPAI_TCE_MK_PORT_DAL dataLoadPortDAL = new CPAI_TCE_MK_PORT_DAL();
        //                    CPAI_TCE_MK_PORT dataTCEMKLP;
        //                    foreach (var itemLoadPort in dataDetail.tce_mk_detail.load_ports)
        //                    {
        //                        dataTCEMKLP = new CPAI_TCE_MK_PORT();
        //                        dataTCEMKLP.TMP_ROW_ID = Guid.NewGuid().ToString("N");
        //                        dataTCEMKLP.TMP_FK_TCE_MK = TransID;
        //                        dataTCEMKLP.TMP_ORDER_PORT = itemLoadPort.port_order;
        //                        dataTCEMKLP.TMP_POST_COST = itemLoadPort.port_value;
        //                        if (itemLoadPort.port != "")
        //                        {
        //                            dataTCEMKLP.TMP_FK_PORT = Convert.ToDecimal(itemLoadPort.port);
        //                        }
        //                        dataTCEMKLP.TMP_PORT_TYPE = "L";
        //                        dataTCEMKLP.TMP_CREATED_DATE = dtNow;
        //                        dataTCEMKLP.TMP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
        //                        dataTCEMKLP.TMP_UPDATED = dtNow;
        //                        dataTCEMKLP.TMP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
        //                        dataLoadPortDAL.Save(dataTCEMKLP, context);
        //                    }
        //                    #endregion

        //                    #region TCE_MK_DIS_PORT
        //                    CPAI_TCE_MK_PORT_DAL dataDisPortDAL = new CPAI_TCE_MK_PORT_DAL();
        //                    CPAI_TCE_MK_PORT dataTCEMKDP;
        //                    foreach (var itemDisPort in dataDetail.tce_mk_detail.dis_ports)
        //                    {
        //                        dataTCEMKDP = new CPAI_TCE_MK_PORT();
        //                        dataTCEMKDP.TMP_ROW_ID = Guid.NewGuid().ToString("N");
        //                        dataTCEMKDP.TMP_FK_TCE_MK = TransID;
        //                        dataTCEMKDP.TMP_ORDER_PORT = itemDisPort.port_order;
        //                        dataTCEMKDP.TMP_POST_COST = itemDisPort.port_value;
        //                        if (itemDisPort.port != "")
        //                        {
        //                            dataTCEMKDP.TMP_FK_PORT = Convert.ToDecimal(itemDisPort.port);
        //                        }
        //                        dataTCEMKDP.TMP_PORT_TYPE = "D";
        //                        dataTCEMKDP.TMP_CREATED_DATE = dtNow;
        //                        dataTCEMKDP.TMP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
        //                        dataTCEMKDP.TMP_UPDATED = dtNow;
        //                        dataTCEMKDP.TMP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
        //                        dataLoadPortDAL.Save(dataTCEMKDP, context);
        //                    }
        //                    #endregion

        //                    dbContextTransaction.Commit();
        //                    isSuccess = true;
        //                }
        //                catch (Exception ex)
        //                {
        //                    string res = ex.Message;
        //                    dbContextTransaction.Rollback();
        //                    isSuccess = false;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        isSuccess = false;
        //    }
        //    #endregion
        //    log.Info("# End State CPAIImportPlanCreateDataState >> InsertDB # ");
        //    return isSuccess;
        //}


        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            //try
            //{
            //    log.Info("# Start State CPAIImportPlanCreateDataState >> UpdateDB #  ");
            //    using (var context = new EntityCPAIEngine())
            //    {
            //        using (var dbContextTransaction = context.Database.BeginTransaction())
            //        {
            //            try
            //            {
            //                //update CPAI_TCE_MK
            //                CPAI_TCE_MK_DAL dataDAL = new CPAI_TCE_MK_DAL();
            //                CPAI_TCE_MK data = new CPAI_TCE_MK();
            //                data.CTM_ROW_ID = TransID;
            //                data.CTM_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
            //                data.CTM_UPDATED_DATE = DateTime.Now;
            //                data.CTM_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
            //                dataDAL.Update(data);
            //                isSuccess = true;
            //            }
            //            catch (Exception ex)
            //            {
            //                string res = ex.Message;
            //                dbContextTransaction.Rollback();
            //                isSuccess = false;
            //            }
            //        }
            //    }
            //    log.Info("# End State CPAIImportPlanCreateDataState >> UpdateDB # ");
            //}
            //catch (Exception ex)
            //{
            //    log.Info("# Error CPAIImportPlanCreateDataState >> UpdateDB # :: Exception >>> " + ex);
            //    string res = ex.Message;
            //    isSuccess = false;
            //}
            return isSuccess;
        }
    }
}