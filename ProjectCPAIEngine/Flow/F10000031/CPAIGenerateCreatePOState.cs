﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;

using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;

using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using System.Globalization;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using com.pttict.engine.dal.Utility;
using System.Linq;

namespace ProjectCPAIEngine.Flow.F10000031
{
    public class CPAIGenerateCreatePOState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGenerateCreatePOState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    ConfigManagement configManagement = new ConfigManagement();
                    String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                    CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                    tempData = JSonConvertUtil.jsonToModel<CreatePOGoodIntransit>(item);
                    if (tempData != null)
                    {
                        PurchasingCreateServiceConnectorImpl POService = new PurchasingCreateServiceConnectorImpl();
                        if (tempData.createPO != null)
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            foreach (CreatePO i in tempData.createPO)
                            {
                                POService = new PurchasingCreateServiceConnectorImpl();
                                if (i.purchasingCreate != null)
                                {
                                    String detail_content = js.Serialize(i.purchasingCreate);
                                    DownstreamResponse<string> po_no = null;

                                    try
                                    {
                                        po_no = POService.connect(config, detail_content);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (tempData.ItemList.dDetail_Material != null)
                                        {
                                            foreach (MaterialViewModel m in tempData.ItemList.dDetail_Material)
                                            {
                                                if (m.PMA_ITEM_NO == i.item && m.PMA_TRIP_NO == i.tripNo)
                                                {
                                                    m.RET_STS = "E";
                                                    m.RET_MSG = ex.Message;
                                                }
                                            }
                                        }
                                        continue;
                                    }

                                    
                                    if (po_no != null)
                                    {
                                        string ret_sts = string.IsNullOrEmpty(po_no.resultCode) ? "" : po_no.resultCode;
                                        string ret_msg = string.IsNullOrEmpty(po_no.resultDesc) ? "" : po_no.resultDesc;
                                        string ret_pono = string.IsNullOrEmpty(po_no.responseData) ? "" : po_no.responseData.ToString();

                                        if (tempData.ItemList.dDetail_Material != null)
                                        {
                                            foreach (MaterialViewModel m in tempData.ItemList.dDetail_Material)
                                            {
                                                if (m.PMA_ITEM_NO == i.item && m.PMA_TRIP_NO == i.tripNo)
                                                {
                                                    if (ret_sts == "1")
                                                    {
                                                        m.PMA_PO_NO = ret_pono;
                                                        m.RET_STS = "S";
                                                        m.PMA_PO_MAT_ITEM = i.ItemNoPOCrude;
                                                        m.PMA_PO_FREIGHT_ITEM = i.ItemNoPOFreight;
                                                        m.PMA_PO_INSURANCE_ITEM = i.ItemNoPOInsur;
                                                        m.PMA_PO_CUSTOMS_ITEM = i.ItemNoPOCustom;
                                                        m.RET_MSG = "";

                                                        var queryDetail = tempData.ItemList.dDetail.Where(p => p.h_TripNo.Equals(m.PMA_TRIP_NO) && p.MatItemNo.Equals(m.PMA_ITEM_NO)).ToList();
                                                        foreach (var itmDetail in queryDetail) {
                                                            itmDetail.PONoCrude = ret_pono;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        m.RET_STS = "E";
                                                        m.RET_MSG = ret_msg;

                                                        var qHead = tempData.ItemList.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(i.tripNo)).First();
                                                        if (qHead != null)
                                                        {
                                                            qHead.PHE_SAVE_STS = "E";
                                                            qHead.PHE_SAVE_MSG = ret_msg;
                                                        }

                                                    }
                                                    
                                                    continue;
                                                }
                                            }
                                        }

                                    }

                                }
                            }

                            string newXml = js.Serialize(tempData);
                            ExtendValue respTrans = new ExtendValue();
                            respTrans.value = newXml;
                            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;

                            etxValue.SetValue(CPAIConstantUtil.DataDetailInput, respTrans);
                        }

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        stateModel.BusinessModel.currentCode = currentCode;
                        respCodeManagement.setCurrentCodeMapping(stateModel);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIGenerateCreatePOState # :: Exception >>> " + ex);
                log.Error("CPAIGenerateCreatePOState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

    }

}
