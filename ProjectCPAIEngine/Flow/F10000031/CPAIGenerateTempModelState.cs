﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000031
{
    public class CPAIGenerateTempModelState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIGenerateModelState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                string sAction = etxValue.GetValue(CPAIConstantUtil.Action);
                string fixtripno = etxValue.GetValue("trip_no");
                string fixitemno = etxValue.GetValue("item_no"); 

                ConfigManagement configManagement = new ConfigManagement();
                String content = stateModel.BusinessModel.etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                    CrudeApproveFormViewModel_Detail crudeItem = JSonConvertUtil.jsonToModel<CrudeApproveFormViewModel_Detail>(item);
                    tempData.ItemList = new CrudeApproveFormViewModel_Detail();
                    tempData.ItemList = crudeItem;

                    if (tempData.ItemList != null)
                    {
                        tempData.createPO = new List<CreatePO>();
                        //Crate data for service Create PO

                        if (sAction.Equals("PO") || sAction.Equals("POGIT"))
                        {
                            tempData.createPO = CreatePoItem(tempData, fixtripno, fixitemno);
                        }

                        tempData.goodIntransit = new List<GoodIntransit>();
                        //Crate data for service Good Intran
                        if (sAction.Equals("GIT") || sAction.Equals("POGIT"))
                        {
                            tempData.goodIntransit = GoodIntransitItem(tempData, fixtripno, fixitemno);
                        }
                        else
                        {
                            tempData.goodIntransit = null;
                        }


                        string newXml = js.Serialize(tempData);
                        //ExtendValue respTrans = new ExtendValue();
                        //respTrans.value = newXml;
                        //respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;

                        etxValue.SetValue(CPAIConstantUtil.DataDetailInput, newXml);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIGenerateTempModelState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIGenerateModelState # :: Exception >>> " + ex);
                log.Error("CPAIGenerateModelState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        private void getPOHeaderConfig(string comcode, string purchaseType, string incoterm, string UserRole, ref string StorageLocation, ref string PurGroup)
        {
            string ret = "";
            try
            {
                CIP_Config CIPConfig = CrudeImportPlanServiceModel.getCIPConfig("CIP_IMPORT_PLAN");
                if (CIPConfig == null)
                    return;

                if (CIPConfig.PCF_MT_STORAGE_LOCATION_DETAIL != null)
                {
                    var query = CIPConfig.PCF_MT_STORAGE_LOCATION_DETAIL.Where(p => p.COMPANY_CODE.Equals(comcode) && p.PURCHASE_TYPE.Equals(purchaseType) && p.INCOTERMS.Equals(incoterm)).ToList();
                    if (query != null && query.Count > 0)
                        StorageLocation = query[0].CODE;
                }

                if (CIPConfig.PCF_PO_SURVEYOR_PUR_GROUP != null) {
                    var query = CIPConfig.PCF_PO_SURVEYOR_PUR_GROUP.Where(p => p.ROLE.Equals(UserRole)).ToList();
                    if (query != null && query.Count > 0)
                        PurGroup = query[0].VALUE;

                }
                 
            }
            catch (Exception ex) {

            }
           


        }

        private void getMaterialGroup(ref string mgFright, ref string mgInsur, ref string mgCustom)
        {
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var query = (from m in context.MT_MATERIALS_GROUP select m).ToList();

                    if (query == null)
                        return;

                    var qFreight = query.Where(p => p.MMT_GROUP_NAME.Equals("Freight")).First();
                    if (qFreight != null)
                        mgFright = qFreight.MMT_GROUP_CODE;

                    var qInsur = query.Where(p => p.MMT_GROUP_NAME.Equals("Insurance")).First();
                    if (qInsur != null)
                        mgInsur = qInsur.MMT_GROUP_CODE;

                    var qCustom = query.Where(p => p.MMT_GROUP_NAME.Equals("Customs Duty")).First();
                    if (qCustom != null)
                        mgCustom = qCustom.MMT_GROUP_CODE;


                };
            }
            catch (Exception ex) { }
        }

        private string retItemNoSap(string sItem) {
            string ret = "";
            try
            {
                ret = sItem.PadLeft(6, '0');
            }
            catch (Exception ex) { }
            
            return ret;
        }
        public List<CreatePO> CreatePoItem(CreatePOGoodIntransit temp, string fixtripno, string fixitemno)
        {
            var qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixitemno != "")
                qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixtripno)).ToList();

            if (qHead == null)
                return null;

            if (qHead.Count == 0)
                return null;

            string UserName = Const.User.Name;

            string mgFreight = "";
            string mgInsur = "";
            string mgCustom = "";
            getMaterialGroup(ref mgFreight, ref mgInsur, ref mgCustom);

            List<CreatePO> itemList = new List<CreatePO>();
            //string CurrentDate = DateTime.Now.ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

            foreach (var itmHead in qHead)
            {

                string tripno = itmHead.PHE_TRIP_NO;

                var qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && string.IsNullOrEmpty(p.PMA_PO_NO)).ToList();
                if (fixitemno != "")
                    qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(fixitemno) && string.IsNullOrEmpty(p.PMA_PO_NO)).ToList();

                foreach (var itmMat in qMaterial)
                {
                    var qMatPrice = temp.ItemList.dDetail.Where(p => p.h_TripNo.Equals(tripno) && p.MatItemNo.Equals(itmMat.PMA_ITEM_NO)).OrderByDescending(o => o.MatPriceItemNo).First();
                    if (qMatPrice == null)
                        continue;

                    string storageLocation = "";
                    string PurGroup = "";

                    getPOHeaderConfig(itmHead.PHE_COMPANY_CODE, itmMat.PMA_PURCHASE_TYPE, itmMat.PMA_INCOTERMS, Const.User.UserGroup, ref storageLocation, ref PurGroup);

                    string sBL_Date = "";
                    if (!string.IsNullOrEmpty(itmMat.PMA_BL_DATE))
                        sBL_Date = DateTime.ParseExact(itmMat.PMA_BL_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

                    string ItemNo_Crude = "0";
                    string ItemNo_Freight = "0";
                    string ItemNo_Insurance = "0";
                    string ItemNo_Custom = "0";
                    int ItemNoRunning = 0;

                    // PO Header
                    CreatePO i = new CreatePO();
                    i.tripNo = tripno;
                    i.item = itmMat.PMA_ITEM_NO;

                    i.purchasingCreate = new PurchasingCreateModel();
                    i.purchasingCreate.ZPOHEADER = new ZPOHEADER();
                    i.purchasingCreate.ZPOHEADERX = new ZPOHEADERX();
                    i.purchasingCreate.ZPOITEM = new List<ZPOITEM>();
                    i.purchasingCreate.ZPOITEMX = new List<ZPOITEMX>();
                    i.purchasingCreate.ZPOACCOUNT = new List<ZPOACCOUNT>();
                    i.purchasingCreate.ZPOACCOUNTX = new List<ZPOACCOUNTX>();
                    i.purchasingCreate.RTEURN = new List<BAPIRET2>();

                    ZPOHEADER zpoheader = new ZPOHEADER();
                    //zpoheader.PO_NUMBER 
                    zpoheader.COMP_CODE = itmHead.PHE_COMPANY_CODE;
                    //zpoheader.DOC_TYPE
                    //zpoheader.DELETE_IND 
                    zpoheader.CREAT_DATE = sBL_Date;
                    //zpoheader.CREATED_BY 
                    zpoheader.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0'); // "0000000013";
                    //zpoheader.PURCH_ORG 
                    zpoheader.PUR_GROUP = PurGroup; // "502";
                    zpoheader.CURRENCY = qMatPrice.i_CURRENCY;
                    //zpoheader.EXCH_RATE = 0;
                    //zpoheader.EXCH_RATESpecified = true;
                    zpoheader.DOC_DATE = sBL_Date;
                    zpoheader.INCOTERMS1 = itmMat.PMA_INCOTERMS; // FOB
                    zpoheader.OIC_AORGIN = itmMat.PMA_ORIGIN_COUNTRY; // "Singapore";
                    zpoheader.CARGO_NO = tripno;
                    i.purchasingCreate.ZPOHEADER = zpoheader;

                    ZPOHEADERX zpoheaderx = new ZPOHEADERX();
                    //zpoheaderx.PO_NUMBER 
                    zpoheaderx.COMP_CODE = "X";
                    zpoheaderx.DOC_TYPE = "X";
                    //zpoheaderx.DELETE_IND 
                    zpoheaderx.CREAT_DATE = "X";
                    zpoheaderx.CREATED_BY = "X";
                    zpoheaderx.VENDOR = "X";
                    zpoheaderx.PURCH_ORG = "X";
                    zpoheaderx.PUR_GROUP = "X";
                    zpoheaderx.CURRENCY = "X";
                    zpoheaderx.EXCH_RATE = "X";
                    zpoheaderx.DOC_DATE = "X";
                    zpoheaderx.INCOTERMS1 = "X";
                    //zpoheaderx.OIC_AORGIN 
                    //zpoheaderx.CARGO_NO 
                    i.purchasingCreate.ZPOHEADERX = zpoheaderx;

                    string MatIO = getIOMat(itmMat.PMA_MET_NUM);

                    Decimal qty = 0;
                    ZPOITEM zpoitem = new ZPOITEM();
                    ZPOITEMX zpoitemx = new ZPOITEMX();
                    ZPOACCOUNT zpoaccount = new ZPOACCOUNT();
                    ZPOACCOUNTX zpoaccountx = new ZPOACCOUNTX();
                    // PO Crude
                    if (GenPOCrude(ref zpoitem, ref zpoitemx, ref zpoaccount, ref zpoaccountx, ref ItemNoRunning, itmMat
                        , itmHead, qMatPrice, MatIO, UserName, storageLocation, ref qty))
                    {
                        i.purchasingCreate.ZPOITEM.Add(zpoitem);
                        i.purchasingCreate.ZPOITEMX.Add(zpoitemx);
                        i.purchasingCreate.ZPOACCOUNT.Add(zpoaccount);
                        i.purchasingCreate.ZPOACCOUNTX.Add(zpoaccountx);
                        ItemNo_Crude = ItemNoRunning.ToString();
                    }


                    // PO Freight
                    zpoitem = new ZPOITEM();
                    zpoitemx = new ZPOITEMX();
                    zpoaccount = new ZPOACCOUNT();
                    zpoaccountx = new ZPOACCOUNTX();
                    if (!string.IsNullOrEmpty(itmMat.FREIGHT_PRICE) && Convert.ToDecimal(itmMat.FREIGHT_PRICE) != 0)
                    {
                        if (GenPOFreight(ref zpoitem, ref zpoitemx, ref zpoaccount, ref zpoaccountx, ref ItemNoRunning, itmMat
                            , itmHead, qMatPrice, MatIO, UserName, mgFreight, qty))
                        {
                            i.purchasingCreate.ZPOITEM.Add(zpoitem);
                            i.purchasingCreate.ZPOITEMX.Add(zpoitemx);
                            i.purchasingCreate.ZPOACCOUNT.Add(zpoaccount);
                            i.purchasingCreate.ZPOACCOUNTX.Add(zpoaccountx);
                            ItemNo_Freight = ItemNoRunning.ToString();
                        }
                    }



                    // PO Insurance
                    zpoitem = new ZPOITEM();
                    zpoitemx = new ZPOITEMX();
                    zpoaccount = new ZPOACCOUNT();
                    zpoaccountx = new ZPOACCOUNTX();
                    if (!string.IsNullOrEmpty(itmMat.INSURANCE_PRICE) && Convert.ToDecimal(itmMat.INSURANCE_PRICE) != 0)
                    {
                        if (GenPOInsurance(ref zpoitem, ref zpoitemx, ref zpoaccount, ref zpoaccountx, ref ItemNoRunning, itmMat
                            , itmHead, qMatPrice, MatIO, UserName, mgInsur, qty))
                        {
                            i.purchasingCreate.ZPOITEM.Add(zpoitem);
                            i.purchasingCreate.ZPOITEMX.Add(zpoitemx);
                            i.purchasingCreate.ZPOACCOUNT.Add(zpoaccount);
                            i.purchasingCreate.ZPOACCOUNTX.Add(zpoaccountx);
                            ItemNo_Insurance = ItemNoRunning.ToString();
                        }
                    }


                    // PO Customs
                    zpoitem = new ZPOITEM();
                    zpoitemx = new ZPOITEMX();
                    zpoaccount = new ZPOACCOUNT();
                    zpoaccountx = new ZPOACCOUNTX();
                    if (!string.IsNullOrEmpty(itmMat.IMPORTDUTY_PRICE) && Convert.ToDecimal(itmMat.IMPORTDUTY_PRICE) != 0)
                    {
                        if (GenPOCustoms(ref zpoitem, ref zpoitemx, ref zpoaccount, ref zpoaccountx, ref ItemNoRunning, itmMat
                            , itmHead, qMatPrice, MatIO, UserName, mgCustom, qty))
                        {
                            i.purchasingCreate.ZPOITEM.Add(zpoitem);
                            i.purchasingCreate.ZPOITEMX.Add(zpoitemx);
                            i.purchasingCreate.ZPOACCOUNT.Add(zpoaccount);
                            i.purchasingCreate.ZPOACCOUNTX.Add(zpoaccountx);
                            ItemNo_Custom = ItemNoRunning.ToString();
                        }
                    }


                    BAPIRET2 bapiret2 = new BAPIRET2();
                    i.purchasingCreate.RTEURN.Add(bapiret2);


                    i.ItemNoPOCrude = ItemNo_Crude;
                    i.ItemNoPOFreight = ItemNo_Freight;
                    i.ItemNoPOInsur = ItemNo_Insurance;
                    i.ItemNoPOCustom = ItemNo_Custom;
                    itemList.Add(i);


                }
            }


            return itemList;
        }

        private Boolean GenPOCrude(ref ZPOITEM zpoitem, ref ZPOITEMX zpoitemx, ref ZPOACCOUNT zpoaccount, ref ZPOACCOUNTX zpoaccountx
            , ref int ItemNoRunning, MaterialViewModel itmMat, HeaderViewModel itmHead, CrudeImportPlanViewModel_Detail qMatPrice, string MatIO
            , string UserName, string storageLocation, ref Decimal qty)
        {
            Boolean ret = true;
            try
            {

                ItemNoRunning += 10;

                string itemNoSap = retItemNoSap(ItemNoRunning.ToString());

                zpoitem.PO_ITEM = itemNoSap;


                // "10";
                //zpoitem.DELETE_IND 

                // 40
                // train : CRU/supplier name (10 digit)/tripno/formula
                // vessel : Vendor (Search Term) / Material Code / Vessel Name / Trip No. โดย ไม่ต้องใส่ CRU มาแล้ว --> '- BP SING / YMURBAN / TATEYAMA / 2016/009999

                string shortText = "";
                if (itmHead.PHE_CHANNEL.Equals("VESSEL"))
                {
                    shortText = (string.IsNullOrEmpty(itmMat.PMA_SUPPLIER_SEARCHTERM) ? "" : itmMat.PMA_SUPPLIER_SEARCHTERM )
                                + (string.IsNullOrEmpty(itmMat.PMA_MET_NUM) ? "" : "/" + itmMat.PMA_MET_NUM)
                                + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT)
                                + (string.IsNullOrEmpty(itmHead.PHE_TRIP_NO) ? "" : "/" + itmHead.PHE_TRIP_NO);

                    if (shortText.Length > 40)
                        shortText = shortText.Substring(0, 40);
                }
                else
                {
                    shortText = "CRU"
                        + (string.IsNullOrEmpty(qMatPrice.h_Supplier) ? "" : "/" + (qMatPrice.h_Supplier.Length > 10 ? qMatPrice.h_Supplier.Substring(0, 10) : qMatPrice.h_Supplier))
                        + (string.IsNullOrEmpty(itmMat.PMA_TRIP_NO) ? "" : "/" + itmMat.PMA_TRIP_NO)
                        + (string.IsNullOrEmpty(itmMat.PMA_FORMULA) ? "" : "/" + itmMat.PMA_FORMULA);

                    if (shortText.Length > 40)
                        shortText = shortText.Substring(0, 40);
                }

                zpoitem.SHORT_TEXT = shortText; // "CRU/PTT/TBA/CF11170501";
                zpoitem.MATERIAL = itmMat.PMA_MET_NUM;// "YETH";
                zpoitem.PLANT = "1200";
                zpoitem.STGE_LOC = storageLocation;
                zpoitem.TRACKINGNO = itmMat.PMA_TRIP_NO;// "CF11170501";

                qty = 0;
                if (qMatPrice.i_PriceFor.Equals("BBL"))
                {
                    qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_BBL) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_BBL);
                }
                else if (qMatPrice.i_PriceFor.Equals("MT"))
                {
                    qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_MT) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_MT);
                }
                else if (qMatPrice.i_PriceFor.Equals("ML"))
                {
                    qty = string.IsNullOrEmpty(itmMat.PMA_VOLUME_ML) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_ML);
                }

                zpoitem.QUANTITY = qty; // (Decimal)801004.000;
                zpoitem.QUANTITYSpecified = true;
                zpoitem.PO_UNIT = qMatPrice.i_PriceFor;
                zpoitem.NET_PRICE = string.IsNullOrEmpty(qMatPrice.ProActual) ? 0 : decimal.Round(Convert.ToDecimal(qMatPrice.ProActual), 2, MidpointRounding.AwayFromZero);
                zpoitem.NET_PRICESpecified = true;
                zpoitem.CURRENCY = qMatPrice.i_CURRENCY;
                zpoitem.ACCTASSCAT = "H";
                zpoitem.PLAN_DEL = 0;
                zpoitem.PLAN_DELSpecified = true;
                zpoitem.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0'); // "0000000013";
                zpoitem.TRAN_ID = "CRU";
                zpoitem.CARGO_NO = itmMat.PMA_TRIP_NO;// "CF11170501";
                zpoitem.PREQ_NAME = UserName;
                zpoitem.GR_BASEDIV = "X";


                zpoitemx.PO_ITEM = itemNoSap;
                //zpoitemx.DELETE_IND 
                zpoitemx.SHORT_TEXT = "X";
                zpoitemx.MATERIAL = "X";
                zpoitemx.PLANT = "X";
                zpoitemx.STGE_LOC = "X";
                zpoitemx.TRACKINGNO = "X";
                //zpoitemx.MATL_GROUP 
                zpoitemx.QUANTITY = "X";
                zpoitemx.PO_UNIT = "X";
                zpoitemx.NET_PRICE = "X";
                zpoitemx.CURRENCY = "X";
                zpoitemx.ACCTASSCAT = "X";
                //zpoitemx.PLAN_DEL = "";
                //zpoitemx.VENDOR = "";
                zpoitemx.TRAN_ID = "X";
                //zpoitemx.CARGO_NO = "";
                zpoitemx.PREQ_NAME = "X";
                zpoitemx.GR_BASEDIV = "X";


                zpoaccount.PO_ITEM = itemNoSap;
                //zpoaccount.DELETE_IND 
                //zpoaccount.GL_ACCOUNT 
                //zpoaccount.COSTCENTER 
                zpoaccount.ORDERID = MatIO; // getIOMat(itmMat.PMA_MET_NUM); // "D11C-ETHA";
                zpoaccount.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0'); // itmMat.PMA_SUPPLIER;
                zpoaccount.TRAN_IND = "CRU";


                zpoaccountx.PO_ITEM = itemNoSap;
                //zpoaccountx.DELETE_IND 
                zpoaccountx.GL_ACCOUNT = "X";
                //zpoaccountx.COSTCENTER 
                zpoaccountx.ORDERID = "X";
                //zpoaccountx.VENDOR = "";
                //zpoaccountx.TRAN_IND = "";

                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        private Boolean GenPOFreight(ref ZPOITEM zpoitem, ref ZPOITEMX zpoitemx, ref ZPOACCOUNT zpoaccount, ref ZPOACCOUNTX zpoaccountx
            , ref int ItemNoRunning, MaterialViewModel itmMat, HeaderViewModel itmHead
            , CrudeImportPlanViewModel_Detail qMatPrice, string MatIO, string UserName, string mgFreight, decimal qty)
        {
            Boolean ret = true;
            try
            {

                ItemNoRunning += 10;

                string itemNoSap = retItemNoSap(ItemNoRunning.ToString());

                zpoitem.PO_ITEM = itemNoSap;// "10";

                // vessel : FRI/vendor name/vessel name
                string shortText = "FRI"
                                    + (string.IsNullOrEmpty(qMatPrice.h_Supplier) ? "" : "/" + (qMatPrice.h_Supplier.Length > 10 ? qMatPrice.h_Supplier.Substring(0, 10) : qMatPrice.h_Supplier))
                                    + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                if (shortText.Length > 40)
                    shortText = shortText.Substring(0, 40);

                decimal fAmount = qty * Convert.ToDecimal(itmMat.FREIGHT_PRICE);
                fAmount = decimal.Round(fAmount, 2, MidpointRounding.AwayFromZero);

                //zpoitem.DELETE_IND 
                zpoitem.SHORT_TEXT = shortText;// "FRI/PTT/TBA/CF11170501";
                //zpoitem.MATERIAL = "";
                zpoitem.PLANT = "1200";
                //zpoitem.STGE_LOC = "";
                zpoitem.TRACKINGNO = itmMat.PMA_TRIP_NO;
                zpoitem.MATL_GROUP = mgFreight; // "Z81PU08";
                zpoitem.QUANTITY = (Decimal)1;
                zpoitem.QUANTITYSpecified = true;
                zpoitem.PO_UNIT = "LE";
                zpoitem.NET_PRICE = fAmount; // (Decimal)684334.4500;
                zpoitem.NET_PRICESpecified = true;
                zpoitem.CURRENCY = qMatPrice.i_CURRENCY;
                zpoitem.ACCTASSCAT = "S";
                zpoitem.PLAN_DEL = 0;
                zpoitem.PLAN_DELSpecified = true;
                zpoitem.VENDOR = "ONETIMEFRT";
                zpoitem.TRAN_ID = "FRI";
                zpoitem.CARGO_NO = itmMat.PMA_TRIP_NO;
                zpoitem.PREQ_NAME = UserName;// "zSunisaK";


                zpoitemx.PO_ITEM = itemNoSap;
                //zpoitemx.DELETE_IND 
                zpoitemx.SHORT_TEXT = "X";
                //zpoitemx.MATERIAL = "X";
                zpoitemx.PLANT = "X";
                zpoitemx.STGE_LOC = "X";
                zpoitemx.TRACKINGNO = "X";
                zpoitemx.MATL_GROUP = "X";
                zpoitemx.QUANTITY = "X";
                zpoitemx.PO_UNIT = "X";
                zpoitemx.NET_PRICE = "X";
                zpoitemx.CURRENCY = "X";
                zpoitemx.ACCTASSCAT = "X";
                //zpoitemx.PLAN_DEL = "";
                //zpoitemx.VENDOR = "";
                zpoitemx.TRAN_ID = "X";
                //zpoitemx.CARGO_NO = "";
                zpoitemx.PREQ_NAME = "X";
                //zpoitemx.GR_BASEDIV = "X";

                zpoaccount.PO_ITEM = itemNoSap;
                //zpoaccount.DELETE_IND 
                //zpoaccount.GL_ACCOUNT 
                //zpoaccount.COSTCENTER 
                zpoaccount.ORDERID = MatIO; // "D11C-ETHA";
                zpoaccount.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0');  // itmMat.PMA_SUPPLIER;// "0000000013";
                zpoaccount.TRAN_IND = "FRI";

                zpoaccountx.PO_ITEM = itemNoSap;
                //zpoaccountx.DELETE_IND 
                zpoaccountx.GL_ACCOUNT = "X";
                //zpoaccountx.COSTCENTER 
                zpoaccountx.ORDERID = "X";
                //zpoaccountx.VENDOR = "";
                //zpoaccountx.TRAN_IND = "";

                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        private Boolean GenPOInsurance(ref ZPOITEM zpoitem, ref ZPOITEMX zpoitemx, ref ZPOACCOUNT zpoaccount, ref ZPOACCOUNTX zpoaccountx
            , ref int ItemNoRunning, MaterialViewModel itmMat, HeaderViewModel itmHead
            , CrudeImportPlanViewModel_Detail qMatPrice, string MatIO, string UserName, string mgInsur, Decimal qty)
        {
            Boolean ret = true;
            try
            {
                ItemNoRunning += 10;

                string itemNoSap = retItemNoSap(ItemNoRunning.ToString());

                zpoitem.PO_ITEM = itemNoSap;// "10";

                string shortText = "INS"
                                  + (string.IsNullOrEmpty(qMatPrice.h_Supplier) ? "" : "/" + qMatPrice.h_Supplier)
                                  + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                if (shortText.Length > 40)
                    shortText = shortText.Substring(0, 40);

                Decimal InsurAmount = qty * Convert.ToDecimal(itmMat.INSURANCE_PRICE);
                InsurAmount = decimal.Round(InsurAmount, 2, MidpointRounding.AwayFromZero);

                zpoitem.SHORT_TEXT = shortText; // "INS/PTT/TBA/CF11170501";
                //zpoitem.MATERIAL = "";
                zpoitem.PLANT = "1200";
                //zpoitem.STGE_LOC = "";
                zpoitem.TRACKINGNO = itmMat.PMA_TRIP_NO;
                zpoitem.MATL_GROUP = mgInsur;// "Z81PU09";
                zpoitem.QUANTITY = (Decimal)1;
                zpoitem.QUANTITYSpecified = true;
                zpoitem.PO_UNIT = "LE";
                zpoitem.NET_PRICE = InsurAmount; // (Decimal)488.3859;
                zpoitem.NET_PRICESpecified = true;
                zpoitem.CURRENCY = qMatPrice.i_CURRENCY;
                zpoitem.ACCTASSCAT = "S";
                zpoitem.PLAN_DEL = 0;
                zpoitem.PLAN_DELSpecified = true;
                zpoitem.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0');// itmMat.PMA_SUPPLIER;// "0002000065";
                zpoitem.TRAN_ID = "INS";
                zpoitem.CARGO_NO = itmMat.PMA_TRIP_NO;
                zpoitem.PREQ_NAME = UserName;


                zpoitemx.PO_ITEM = itemNoSap;
                //zpoitemx.DELETE_IND 
                zpoitemx.SHORT_TEXT = "X";
                //zpoitemx.MATERIAL = "X";
                zpoitemx.PLANT = "X";
                zpoitemx.STGE_LOC = "X";
                zpoitemx.TRACKINGNO = "X";
                zpoitemx.MATL_GROUP = "X";
                zpoitemx.QUANTITY = "X";
                zpoitemx.PO_UNIT = "X";
                zpoitemx.NET_PRICE = "X";
                zpoitemx.CURRENCY = "X";
                zpoitemx.ACCTASSCAT = "X";
                //zpoitemx.PLAN_DEL = "";
                //zpoitemx.VENDOR = "";
                zpoitemx.TRAN_ID = "X";
                //zpoitemx.CARGO_NO = "";
                zpoitemx.PREQ_NAME = "X";
                //zpoitemx.GR_BASEDIV = "X";

                zpoaccount.PO_ITEM = itemNoSap;
                //zpoaccount.DELETE_IND 
                //zpoaccount.GL_ACCOUNT 
                //zpoaccount.COSTCENTER 
                zpoaccount.ORDERID = MatIO; // "D11C-ETHA";
                zpoaccount.VENDOR = itmMat.PMA_SUPPLIER;
                zpoaccount.TRAN_IND = "INS";

                zpoaccountx.PO_ITEM = itemNoSap;
                //zpoaccountx.DELETE_IND 
                zpoaccountx.GL_ACCOUNT = "X";
                //zpoaccountx.COSTCENTER 
                zpoaccountx.ORDERID = "X";
                //zpoaccountx.VENDOR = "";
                //zpoaccountx.TRAN_IND = "";

                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        private Boolean GenPOCustoms(ref ZPOITEM zpoitem, ref ZPOITEMX zpoitemx, ref ZPOACCOUNT zpoaccount, ref ZPOACCOUNTX zpoaccountx
           , ref int ItemNoRunning, MaterialViewModel itmMat, HeaderViewModel itmHead
            , CrudeImportPlanViewModel_Detail qMatPrice, string MatIO, string UserName, string mgCustom, Decimal qty)
        {
            Boolean ret = true;
            try
            {
                ItemNoRunning += 10;

                zpoitem.PO_ITEM = ItemNoRunning.ToString();
                string shortText = "CUS"
                                  + (string.IsNullOrEmpty(qMatPrice.h_Supplier) ? "" : "/" + qMatPrice.h_Supplier)
                                  + (string.IsNullOrEmpty(itmHead.PHE_VESSEL_TEXT) ? "" : "/" + itmHead.PHE_VESSEL_TEXT);

                Decimal CusAmount = qty * Convert.ToDecimal(itmMat.IMPORTDUTY_PRICE);
                CusAmount = decimal.Round(CusAmount, 2, MidpointRounding.AwayFromZero);

                //zpoitem.DELETE_IND 
                zpoitem.SHORT_TEXT = shortText; // "CUS/PTT/TBA/CF11170501";
                //zpoitem.MATERIAL = "";
                zpoitem.PLANT = "1200";
                //zpoitem.STGE_LOC = "";
                zpoitem.TRACKINGNO = itmMat.PMA_TRIP_NO;
                zpoitem.MATL_GROUP = mgCustom; // "Z81PU11";
                zpoitem.QUANTITY = (Decimal)1;
                zpoitem.QUANTITYSpecified = true;
                zpoitem.PO_UNIT = "LE";
                zpoitem.NET_PRICE = CusAmount;
                zpoitem.NET_PRICESpecified = true;
                zpoitem.CURRENCY = qMatPrice.i_CURRENCY;
                zpoitem.ACCTASSCAT = "S";
                zpoitem.PLAN_DEL = 0;
                zpoitem.PLAN_DELSpecified = true;
                zpoitem.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0');// itmMat.PMA_SUPPLIER; // "CUSTOMS";
                zpoitem.TRAN_ID = "CUS";
                zpoitem.CARGO_NO = itmMat.PMA_TRIP_NO;
                zpoitem.PREQ_NAME = UserName;


                zpoitemx.PO_ITEM = ItemNoRunning.ToString();
                //zpoitemx.DELETE_IND 
                zpoitemx.SHORT_TEXT = "X";
                //zpoitemx.MATERIAL = "X";
                zpoitemx.PLANT = "X";
                zpoitemx.STGE_LOC = "X";
                zpoitemx.TRACKINGNO = "X";
                zpoitemx.MATL_GROUP = "X";
                zpoitemx.QUANTITY = "X";
                zpoitemx.PO_UNIT = "X";
                zpoitemx.NET_PRICE = "X";
                zpoitemx.CURRENCY = "X";
                zpoitemx.ACCTASSCAT = "X";
                //zpoitemx.PLAN_DEL = "";
                //zpoitemx.VENDOR = "";
                zpoitemx.TRAN_ID = "X";
                //zpoitemx.CARGO_NO = "";
                zpoitemx.PREQ_NAME = "X";
                //zpoitemx.GR_BASEDIV = "X";


                zpoaccount.PO_ITEM = ItemNoRunning.ToString();
                //zpoaccount.DELETE_IND 
                //zpoaccount.GL_ACCOUNT 
                //zpoaccount.COSTCENTER 
                zpoaccount.ORDERID = MatIO;
                zpoaccount.VENDOR = string.IsNullOrEmpty(itmMat.PMA_SUPPLIER) ? "" : itmMat.PMA_SUPPLIER.PadLeft(10, '0');// itmMat.PMA_SUPPLIER;// "CUSTOMS";
                zpoaccount.TRAN_IND = "CUS";

                zpoaccountx.PO_ITEM = ItemNoRunning.ToString();
                //zpoaccountx.DELETE_IND 
                zpoaccountx.GL_ACCOUNT = "X";
                //zpoaccountx.COSTCENTER 
                zpoaccountx.ORDERID = "X";
                //zpoaccountx.VENDOR = "";
                //zpoaccountx.TRAN_IND = "";

                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        private string formatNumberGIT(string svalue) {
            string ret = "0";
            try
            {
                if (string.IsNullOrEmpty(svalue))
                    return ret;

                ret = Convert.ToDecimal(svalue).ToString("###0.000");

            }
            catch (Exception ex) {

            }
            return ret;
        }

        public List<GoodIntransit> GoodIntransitItem(CreatePOGoodIntransit temp, string fixtripno, string fixitemno)
        {
            var qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_IS_CHECK.Equals(true)).ToList();
            if (fixitemno != "")
                qHead = temp.ItemList.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(fixtripno)).ToList();

            if (qHead == null)
                return null;

            if (qHead.Count == 0)
                return null;

            string UserName = Const.User.Name;

            List<GoodIntransit> itemList = new List<GoodIntransit>();

            foreach (var itmHead in qHead)
            {
               

                string tripno = itmHead.PHE_TRIP_NO;

                var qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno)).ToList();
                if (fixitemno != "")
                    qMaterial = temp.ItemList.dDetail_Material.Where(p => p.PMA_TRIP_NO.Equals(tripno) && p.PMA_ITEM_NO.Equals(fixitemno)).ToList();

                foreach (var itmMat in qMaterial)
                {

                    if (!string.IsNullOrEmpty(itmMat.PMA_GIT_NO) && !itmMat.PMA_GIT_NO.Equals("0"))
                        continue;

                    string storageLocation = "";
                    string PurGroup = "";
                    string line_id = "0010";

                    getPOHeaderConfig(itmHead.PHE_COMPANY_CODE, itmMat.PMA_PURCHASE_TYPE, itmMat.PMA_INCOTERMS, Const.User.UserGroup, ref storageLocation, ref PurGroup);

                    
                    string sBL_Date = "";
                    if (!string.IsNullOrEmpty(itmMat.PMA_BL_DATE))
                        sBL_Date = DateTime.ParseExact(itmMat.PMA_BL_DATE, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));

                    //sBL_Date = "2017-07-19";

                    Decimal QTY_BBL = string.IsNullOrEmpty(itmMat.PMA_VOLUME_BBL) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_BBL.Replace(",", ""));
                    Decimal TEMP = string.IsNullOrEmpty(itmMat.PMA_TEMP) ? 0 : Convert.ToDecimal(itmMat.PMA_TEMP.Replace(",", ""));
                    Decimal DEN = string.IsNullOrEmpty(itmMat.PMA_DENSITY) ? 0 : Convert.ToDecimal(itmMat.PMA_DENSITY.Replace(",", ""));
                    Decimal BSW = string.IsNullOrEmpty(itmMat.PMA_BSW) ? 0 : Convert.ToDecimal(itmMat.PMA_BSW.Replace(",", ""));
                    GoodIntransit i = new GoodIntransit();
                    i.tripNo = itmMat.PMA_TRIP_NO;
                    i.item = itmMat.PMA_ITEM_NO;
                    i.intansitPost = new IntansitPostModel();
                    i.intansitPost.ZGOODSMVT_HEADER = new ZGOODSMVT_HEADER();
                    i.intansitPost.ZGOODSMVT_CODE = new ZGOODSMVT_CODE();
                    i.intansitPost.ZGOODSMVT_ITEM_01 = new List<ZGOODSMVT_ITEM_01>();
                    i.intansitPost.ZGOODSMVT_ITEM_PARAM = new List<BAPIOIL2017_GM_ITM_CRTE_PARAM>();
                    i.intansitPost.ZGOODSMVT_ITEM_QUAN = new List<ZGOODSMVT_ITEM_QUAN>();
                    i.intansitPost.GM_HEAD_RET = new BAPI2017_GM_HEAD_RET();
                    i.intansitPost.RETURN = new List<BAPIRET2>();

                    ZGOODSMVT_CODE zgoodsmvt_code = new ZGOODSMVT_CODE();
                    // fix 01
                    zgoodsmvt_code.GM_CODE = "01";
                    i.intansitPost.ZGOODSMVT_CODE = zgoodsmvt_code;

                    ZGOODSMVT_HEADER zgoodsmvt_header = new ZGOODSMVT_HEADER();
                    // BL DAte
                    zgoodsmvt_header.PSTNG_DATE = sBL_Date;
                    // BL Date
                    zgoodsmvt_header.DOC_DATE = sBL_Date;
                    // system time
                    zgoodsmvt_header.PSTNG_TIME = "00:00:00";

                    //zgoodsmvt_header.REF_DOC_NO

                    // trip no
                    zgoodsmvt_header.BILL_OF_LADING = itmMat.PMA_TRIP_NO; // "CF11170501";
                    //zgoodsmvt_header.GR_GI_SLIP_NO = "";
                    zgoodsmvt_header.PR_UNAME = UserName;// "zSunisaK";
                    // trip no
                    zgoodsmvt_header.HEADER_TXT = itmMat.PMA_TRIP_NO; //"CF11170501";
                    //zgoodsmvt_header.VER_GR_GI_SLI
                    //zgoodsmvt_header.VER_GR_GI_SLIPX
                    //zgoodsmvt_header.EXT_WMS
                    //zgoodsmvt_header.REF_DOC_NO_LONG

                    // trip no
                    zgoodsmvt_header.BILL_OF_LADING_LONG = itmMat.PMA_TRIP_NO;

                    //// case TOP Purchase for swap
                    //zgoodsmvt_header.REF_DOC_NO = "SWAP";
                    i.intansitPost.ZGOODSMVT_HEADER = zgoodsmvt_header;

                    ZGOODSMVT_ITEM_01 zgoodsmvt_item_01 = new ZGOODSMVT_ITEM_01();
                    zgoodsmvt_item_01.LINE_ID = line_id;// "0010";
                    //zgoodsmvt_item_01.PARENT_ID
                    //zgoodsmvt_item_01.LINE_DEPTH 
                    zgoodsmvt_item_01.MATERIAL = itmMat.PMA_MET_NUM;// "YETH";
                    zgoodsmvt_item_01.PLANT = "1200";
                    zgoodsmvt_item_01.STGE_LOC = storageLocation; // "12B1";
                    //zgoodsmvt_item_01.BATCH
                    zgoodsmvt_item_01.MOVE_TYPE = "101";
                    //zgoodsmvt_item_01.STCK_TYPE
                    //zgoodsmvt_item_01.SPEC_STOCK
                    //zgoodsmvt_item_01.VENDOR
                    //zgoodsmvt_item_01.CUSTOMER

                    // volume BBL
                    zgoodsmvt_item_01.ENTRY_QNT = QTY_BBL; // (decimal)1.000;
                    zgoodsmvt_item_01.ENTRY_QNTSpecified = true;
                    zgoodsmvt_item_01.ENTRY_UOM = "BBL"; // fix BBL

                    //zgoodsmvt_item_01.ORDERPR_UN
                    zgoodsmvt_item_01.PO_NUMBER =  itmMat.PMA_PO_NO;
                    zgoodsmvt_item_01.PO_ITEM = "000010";
                    zgoodsmvt_item_01.ITEM_TEXT = itmMat.PMA_TRIP_NO;// "CF11170501";
                    zgoodsmvt_item_01.GR_RCPT = UserName;// "zSunisaK";
                    //zgoodsmvt_item_01.ORDERID
                    //zgoodsmvt_item_01.ORDER_ITNO
                    zgoodsmvt_item_01.MVT_IND = "B";
                    //zgoodsmvt_item_01.MOVE_REAS
                    //zgoodsmvt_item_01.COST_OBJ
                    //zgoodsmvt_item_01.EAN_UPC >?</ EAN_UPC
                    //zgoodsmvt_item_01.DELIV_NUMB_TO_SEARCH
                    //zgoodsmvt_item_01.DELIV_ITEM_TO_SEARCH
                    //zgoodsmvt_item_01.VENDRBATCH
                    zgoodsmvt_item_01.BL_DATE = sBL_Date; // "2017-05-02";

                    zgoodsmvt_item_01.L15 = string.IsNullOrEmpty(itmMat.PMA_VOLUME_L15) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_L15.Replace(",", ""));// (decimal)0;
                    zgoodsmvt_item_01.L15Specified = true;

                    zgoodsmvt_item_01.L30 = string.IsNullOrEmpty(itmMat.PMA_VOLUME_ML) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_ML.Replace(",", "")); // (decimal)1.249;
                    zgoodsmvt_item_01.L30Specified = true;

                    // ไม่มี
                    zgoodsmvt_item_01.L = 0;
                    zgoodsmvt_item_01.LSpecified = true;

                    zgoodsmvt_item_01.LTO = string.IsNullOrEmpty(itmMat.PMA_VOLUME_LTON) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_LTON.Replace(",", ""));// (decimal)1.315;
                    zgoodsmvt_item_01.LTOSpecified = true;

                    zgoodsmvt_item_01.MT = string.IsNullOrEmpty(itmMat.PMA_VOLUME_MT) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_MT.Replace(",", "")); // (decimal)1.926;
                    zgoodsmvt_item_01.MTSpecified = true;

                    // Volume BBL
                    zgoodsmvt_item_01.BB6 = QTY_BBL;
                    zgoodsmvt_item_01.BB6Specified = true;

                    zgoodsmvt_item_01.KG = string.IsNullOrEmpty(itmMat.PMA_VOLUME_KG) ? 0 : Convert.ToDecimal(itmMat.PMA_VOLUME_KG.Replace(",", "")); //  (decimal)1.000;
                    zgoodsmvt_item_01.KGSpecified = true;

                    zgoodsmvt_item_01.TEMPER = TEMP; // (decimal)2;
                    zgoodsmvt_item_01.TEMPERSpecified = true;

                    zgoodsmvt_item_01.BSW = BSW;
                    zgoodsmvt_item_01.BSWSpecified = true;
                    i.intansitPost.ZGOODSMVT_ITEM_01.Add(zgoodsmvt_item_01);

                    BAPIOIL2017_GM_ITM_CRTE_PARAM zgoodsmvt_item_param = new BAPIOIL2017_GM_ITM_CRTE_PARAM();
                    zgoodsmvt_item_param.LINE_ID = line_id;// "0010";
                    zgoodsmvt_item_param.CALCULATEMISSING = "X";

                    // มี den ค่อยส่ง
                    zgoodsmvt_item_param.BASEDENSITY = Double.Parse(DEN.ToString());
                    zgoodsmvt_item_param.BASEDENSITYSpecified = true;
                    zgoodsmvt_item_param.BASEDENSITYUOM = "KGL";
                    zgoodsmvt_item_param.TESTDENSITY = Double.Parse(DEN.ToString());
                    zgoodsmvt_item_param.TESTDENSITYSpecified = true;
                    zgoodsmvt_item_param.TESTDENSITY_UOM = "KGL";


                    // fix
                    zgoodsmvt_item_param.TESTTEMPERATURE_DENSITY = 15;
                    zgoodsmvt_item_param.TESTTEMPERATURE_DENSITYSpecified = true;
                    zgoodsmvt_item_param.TESTTEMP_DENSITY_UOM = "CEL";

                    // temp บนหน้าจอ
                    zgoodsmvt_item_param.MATERIALTEMPERATURE = Double.Parse(TEMP.ToString());
                    zgoodsmvt_item_param.MATERIALTEMPERATURESpecified = true;
                    zgoodsmvt_item_param.MATERIALTEMPERATURE_UOM = "CEL";

                    // BSW
                    //zgoodsmvt_item_param.BASESEDIMENTWATERCONTENT = Double.Parse(BSW.ToString());
                    //zgoodsmvt_item_param.BASESEDIMENTWATERCONTENTSpecified = true;


                    i.intansitPost.ZGOODSMVT_ITEM_PARAM.Add(zgoodsmvt_item_param);

                    ZGOODSMVT_ITEM_QUAN zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();

                    // L15, L30, LTO, MT, BBL, KG, BSW
                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_L15))
                    {
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_L15));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_L15));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "L15";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }


                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_ML))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_ML));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_ML));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "L30";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }


                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_LTON))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_LTON));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_LTON));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "LTO";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }

                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_MT))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_MT));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_MT));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "MT";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }

                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_BBL))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_BBL));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_BBL));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "BBL";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }

                    if (!string.IsNullOrEmpty(itmMat.PMA_VOLUME_KG))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;/// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(formatNumberGIT(itmMat.PMA_VOLUME_KG));
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(formatNumberGIT(itmMat.PMA_VOLUME_KG));
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYUOM = "KG";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }

                    if (!string.IsNullOrEmpty(itmMat.PMA_BSW))
                    {
                        zgoodsmvt_item_quan = new ZGOODSMVT_ITEM_QUAN();
                        zgoodsmvt_item_quan.LINE_ID = line_id;// "0010";
                        zgoodsmvt_item_quan.QUANTITYFLOAT = Convert.ToDouble(itmMat.PMA_BSW);
                        zgoodsmvt_item_quan.QUANTITYFLOATSpecified = true;
                        zgoodsmvt_item_quan.QUANTITYPACKED = Convert.ToDecimal(itmMat.PMA_BSW);
                        zgoodsmvt_item_quan.QUANTITYPACKEDSpecified = true;
                        //zgoodsmvt_item_quan.QUANTITYUOM = "KG";
                        i.intansitPost.ZGOODSMVT_ITEM_QUAN.Add(zgoodsmvt_item_quan);
                    }


                    BAPIRET2 bapiret2 = new BAPIRET2();
                    i.intansitPost.RETURN.Add(bapiret2);

                    itemList.Add(i);
                }
            }
            
            return itemList;
        }

        public string getIOMat(string MatCode)
        {
            string ret = "";
            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    var query = (from m in context.MT_MATERIALS.Where(p => p.MET_NUM.Equals(MatCode))
                                 select new
                                 {
                                     MatIO = m.MET_IO
                                 }).First();

                    if (query != null)
                        ret = query.MatIO;
                };
            }
            catch (Exception ex)
            {
                ret = "";
            }

            return ret;
        }
    }

}
