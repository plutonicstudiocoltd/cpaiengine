﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000037
{
    public class CPAIHedgingDealByPassVerifyState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingDealByPassVerifyState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                // SET Verify flag to "NO" is SUBMIT : "YES" is HOLD
                etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Verify_Flag, new ExtendValue { value = CPAIConstantUtil.NO_FLAG, encryptFlag = "N" });

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                //string NextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);

                HedgDealRootObject dataDetail = null;
                bool isRunDB = false;
                string VerifyStatus = "";
                string VerifyReason = "";
                string VerifyFile = "";
                string VerifyRemark = "";

                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(item);

                    // Bypass
                    if (dataDetail.hedging_deal.verify != null )
                    {
                        VerifyReason = dataDetail.hedging_deal.verify.other_reason;
                        VerifyFile = dataDetail.hedging_deal.verify.other_file;
                        VerifyStatus = dataDetail.hedging_deal.verify.other_bypass;
                        VerifyRemark = dataDetail.hedging_deal.verify.other_remark;
                        if (dataDetail.hedging_deal.verify.other_bypass == "T")
                        {
                            //NextStatus = CPAIConstantUtil.STATUS_SUBMIT;
                            currentCode = CPAIConstantRespCodeUtil.ACTION_BYPASS_RESP_CODE;
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, CPAIConstantUtil.Hedg_Verify_Status_ByPass, "", dataDetail.hedging_deal.verify.other_reason, dataDetail.hedging_deal.verify.other_file, etxValue);
                            //etxValue.SetValue(CPAIConstantUtil.NextStatus, new ExtendValue { value = NextStatus, encryptFlag = "N" });

                            if (isRunDB)
                            {
                                dataDetail.hedging_deal.verify.other_remark = VerifyRemark;
                                dataDetail.hedging_deal.verify.other_bypass = VerifyStatus;
                                dataDetail.hedging_deal.verify.other_file = VerifyFile;
                                dataDetail.hedging_deal.verify.other_reason = VerifyReason;

                                dataDetail.hedging_deal.verify.cds = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                                dataDetail.hedging_deal.verify.annual_framework = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                                dataDetail.hedging_deal.verify.committee_framework = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                                dataDetail.hedging_deal.verify.credit_limit = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                                dataDetail.hedging_deal.verify.credit_rating = CPAIConstantUtil.Hedg_Verify_Status_ByPass;

                                string dataDetailString = JSonConvertUtil.modelToJson(dataDetail);
                                etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                            }
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                    }
                    else
                    {
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                }
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingDealByPassVerifyState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingDealByPassVerifyState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingDealByPassVerifyState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool UpdateDB(string TransID, string VerifyStatus, string VerifyRemark, string VerifyReason, string VerifyFile, Dictionary<string, ExtendValue> etxValue)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingDealByPassVerifyState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_HEDG_DAIL 
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_VRF_CDS_STATUS = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            dataHDDA.HDDA_VRF_COMMITTEE_FW_STATUS = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            dataHDDA.HDDA_VRF_CREDIT_LM_STATUS = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            dataHDDA.HDDA_VRF_CREDIT_RT_STATUS = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            dataHDDA.HDDA_VRF_ANNUAL_FW_STATUS = CPAIConstantUtil.Hedg_Verify_Status_ByPass;
                            dataHDDA.HDDA_VRF_OTHER_STATUS = "T";
                            dataHDDA.HDDA_VRF_OTHER_REMARK = VerifyRemark;
                            dataHDDA.HDDA_VRF_OTHER_REASON = VerifyReason;
                            dataHDDA.HDDA_VRF_OTHER_FILE = VerifyFile;
                            
                            dataDAL.UpdateVerifyOther(dataHDDA, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingDealByPassVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIHedgingDealByPassVerifyState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealByPassVerifyState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}