﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALHedg;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000037
{
    public class CPAIHedgingDealUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingDealUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";

                HedgDealRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<HedgDealRootObject>(item);
                    DateTime now = DateTime.Now;
                    string nowStr = ShareFn.ConvertDateTimeToDateStringFormat(now, "dd/MM/yyyy HH:mm");
                    string VerifyFlag = etxValue.GetValue(CPAIConstantUtil.Hedg_Deal_Verify_Flag); // flag = "YES" is HOLD, Flag = "NO" is nextstatus
                    string nextStatus = Convert.ToString(VerifyFlag) == CPAIConstantUtil.YES_FLAG ? CPAIConstantUtil.STATUS_HOLD : etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);

                    string t_deal_date = string.IsNullOrEmpty(dataDetail.hedging_deal.deal_date) ? "" : dataDetail.hedging_deal.deal_date;
                    string t_deal_no = etxValue.GetValue(CPAIConstantUtil.DocNo);
                    string t_ticket_no = string.IsNullOrEmpty(dataDetail.hedging_deal.ref_ticket_no) ? "" : dataDetail.hedging_deal.ref_ticket_no;
                    string t_hedg_type = string.IsNullOrEmpty(dataDetail.hedging_deal.fw_hedged_type) ? "" : dataDetail.hedging_deal.fw_hedged_type;
                    string t_tool = string.IsNullOrEmpty(dataDetail.hedging_deal.tool) ? "" : dataDetail.hedging_deal.tool;
                    string t_underlying = string.IsNullOrEmpty(dataDetail.hedging_deal.underlying) ? "" : dataDetail.hedging_deal.underlying;
                    string t_underlying_vs = string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_vs) ? "" : dataDetail.hedging_deal.underlying_vs;
                    string t_company = string.IsNullOrEmpty(dataDetail.hedging_deal.company) ? "" : dataDetail.hedging_deal.company;
                    string t_counterparty = string.IsNullOrEmpty(dataDetail.hedging_deal.counter_parties) ? "" : dataDetail.hedging_deal.counter_parties;
                    string t_trade_for = string.IsNullOrEmpty(dataDetail.hedging_deal.labix_trade_thru_top) ? "" : dataDetail.hedging_deal.labix_trade_thru_top;
                    string t_pre_deal_no = string.IsNullOrEmpty(dataDetail.hedging_deal.ref_pre_deal_no) ? "" : dataDetail.hedging_deal.ref_pre_deal_no;
                    string t_price = string.IsNullOrEmpty(dataDetail.hedging_deal.price) ? "" : dataDetail.hedging_deal.price;
                    string t_volume_mnt = string.IsNullOrEmpty(dataDetail.hedging_deal.volume_month) ? "" : dataDetail.hedging_deal.volume_month;
                    string t_contract_no = "";
                    if (dataDetail.hedging_deal.ref_ticket_no != null) {
                        if (dataDetail.hedging_deal.contract != null) {
                            foreach (var excItem in dataDetail.hedging_deal.contract)
                            {
                                t_contract_no += t_contract_no != "" ? "|" : "";
                                t_contract_no += excItem.contract_no;
                            }
                        }
                    }

                    string t_status_tracking = nextStatus;
                    
                    #region add data history
                        DataHistoryDAL dthMan = new DataHistoryDAL();
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                    #endregion

                    bool isRunDB = false;
                    if (IsDealExist(stateModel.EngineModel.ftxTransId))
                    {
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT) || currentAction.Equals(CPAIConstantUtil.ACTION_FAIL) || currentAction.Equals(CPAIConstantUtil.ACTION_REVISE))
                        {
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue, note);

                            if (isRunDB)
                            {
                                //set reject flag
                                dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                            }
                        }
                        else if (currentAction.Equals(CPAIConstantUtil.ACTION_COMPLETE) || currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
                        {
                            isRunDB = UpdateCompleteDB(stateModel.EngineModel.ftxTransId, stateModel.EngineModel.nextState, etxValue, dataDetail, note);
                        }
                        else
                        {
                            isRunDB = UpdateDB(stateModel.EngineModel.ftxTransId, nextStatus, etxValue, note);
                        }
                    }
                    else
                    {
                        isRunDB = InsertDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, note);
                    }


                    #region INSERT JSON
                    if (isRunDB)
                    {
                        //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                        #region Set dataDetail
                        etxValue.SetValue(CPAIConstantUtil.System, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Type, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.Type), encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Date, new ExtendValue { value = t_deal_date, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_No, new ExtendValue { value = t_deal_no, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Ticket_No, new ExtendValue { value = t_ticket_no, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Type, new ExtendValue { value = t_hedg_type, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Tools, new ExtendValue { value = t_tool, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying, new ExtendValue { value = t_underlying, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying_vs, new ExtendValue { value = t_underlying_vs, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Company, new ExtendValue { value = t_company, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Counterpartie, new ExtendValue { value = t_counterparty, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Create_by, new ExtendValue { value = User, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Trade_For, new ExtendValue { value = t_trade_for, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Pre_Deal_No, new ExtendValue { value = t_pre_deal_no, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Volume_mnt, new ExtendValue { value = t_volume_mnt, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Price, new ExtendValue { value = t_price,encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Contract_no, new ExtendValue { value = t_contract_no, encryptFlag = "N" });
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Status_Tracking, new ExtendValue { value = t_status_tracking, encryptFlag = "N" });
                        if (dataDetail != null)
                        {
                            dataDetail.hedging_deal.deal_no = t_deal_no;
                            dataDetail.hedging_deal.status = nextStatus;
                            dataDetail.hedging_deal.ref_pre_deal_no = t_pre_deal_no;
                            dataDetail.hedging_deal.create_by = User;
                            dataDetail.hedging_deal.create_date = nowStr;
                            dataDetail.hedging_deal.update_by = User;
                            dataDetail.hedging_deal.update_date = nowStr;
                            dataDetail.hedging_deal.status_tracking = t_status_tracking;

                            string dataDetailString = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                        }
                        #endregion
                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                  }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingDealUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingDealUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingDealUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool IsDealExist(string TransID)
        {
            log.Info("#Start State CPAIHedgingDealUpdateDataStatus >> IsExist # ");
            bool IsExist = false;
            try
            {
                HEDG_DEAL_DATA dataDeal = new HEDG_DEAL_DATA();
                HEDG_DEAL_DATA_DAL DealDAL = new HEDG_DEAL_DATA_DAL();
                dataDeal = DealDAL.GetDEALDATAByID(TransID);
                if (dataDeal != null)
                {
                    IsExist = true;
                }
            }
            catch(Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealUpdateDataState >> IsExist # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                IsExist = false;
            }
            return IsExist;
        }

        public bool InsertDB(string TransID, Dictionary<string, ExtendValue> etxValue, HedgDealRootObject dataDetail, string reason)
        {
            log.Info("# Start State CPAIHedgingDealUpdateDataState >> InsertDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);

                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();

                            dataDAL.Delete(TransID, context);

                            #region CPAI_HEDGING_DEAL
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_DEAL_NO = etxValue.GetValue(CPAIConstantUtil.DocNo);
                            dataHDDA.HDDA_PRE_DEAL_NO = dataDetail.hedging_deal.ref_pre_deal_no;
                            dataHDDA.HDDA_TICKET_NO = dataDetail.hedging_deal.ref_ticket_no;
                            dataHDDA.HDDA_UNWIDE_STATUS = string.IsNullOrEmpty(dataDetail.hedging_deal.unwide_status) ? "F" : dataDetail.hedging_deal.unwide_status;
                            dataHDDA.HDDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataHDDA.HDDA_REVISION = dataDetail.hedging_deal.revision;
                            dataHDDA.HDDA_SUGGESTION = dataDetail.hedging_deal.suggestion;
                            dataHDDA.HDDA_ENG_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                            dataHDDA.HDDA_ENG_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.deal_date))
                                dataHDDA.HDDA_DEAL_DATE = DateTime.ParseExact(dataDetail.hedging_deal.deal_date, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                            dataHDDA.HDDA_DEAL_TYPE = dataDetail.hedging_deal.deal_type;
                            dataHDDA.HDDA_M1M_STATUS = dataDetail.hedging_deal.m1m_status;
                            dataHDDA.HDDA_FK_COMPANY = dataDetail.hedging_deal.company;
                            dataHDDA.HDDA_FK_COUNTER_PARTIES = dataDetail.hedging_deal.counter_parties;
                            dataHDDA.HDDA_CP_ADDITIONAL = dataDetail.hedging_deal.counter_additional;
                            dataHDDA.HDDA_LB_STATUS = dataDetail.hedging_deal.labix_trade_thru_top;
                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.labix_fee))
                                dataHDDA.HDDA_LB_FEE = Convert.ToDecimal(dataDetail.hedging_deal.labix_fee);

                            dataHDDA.HDDA_LB_FEE_UNIT = dataDetail.hedging_deal.labix_fee_unit;
                            dataHDDA.HDDA_FK_UNDERLY = dataDetail.hedging_deal.underlying;
                            dataHDDA.HDDA_UNDERLY_CF_OPR = dataDetail.hedging_deal.underlying_cf_opr;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_cf_val))
                                dataHDDA.HDDA_UNDERLY_CF_VAL = Convert.ToDecimal(dataDetail.hedging_deal.underlying_cf_val);
                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_weight_percent))
                                dataHDDA.HDDA_UNDERLY_WEIGHT_PERCENT = Convert.ToDecimal(dataDetail.hedging_deal.underlying_weight_percent);

                            dataHDDA.HDDA_FK_REF_UNDERLY = dataDetail.hedging_deal.ref_underlying;
                            dataHDDA.HDDA_FK_UNDERLY_VS = dataDetail.hedging_deal.underlying_vs;
                            dataHDDA.HDDA_UNDERLY_VS_CF_OPR = dataDetail.hedging_deal.underlying_vs_cf_opr;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_vs_cf_val))
                                dataHDDA.HDDA_UNDERLY_VS_CF_VAL = Convert.ToDecimal(dataDetail.hedging_deal.underlying_vs_cf_val);
                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.underlying_vs_weight_percent))
                                dataHDDA.HDDA_UNDERLY_VS_WEIGHT_PERCENT = Convert.ToDecimal(dataDetail.hedging_deal.underlying_vs_weight_percent);

                            dataHDDA.HDDA_FK_REF_UNDERLY_VS = dataDetail.hedging_deal.ref_underlying_vs;
                            dataHDDA.HDDA_FK_FW_ANNUAL = dataDetail.hedging_deal.fw_annual_id;
                            dataHDDA.HDDA_FK_HEDG_TYPE = dataDetail.hedging_deal.fw_hedged_type;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.tenor.start_date))
                            {
                                dataHDDA.HDDA_TENOR_DATE_START = DateTime.ParseExact(dataDetail.hedging_deal.tenor.start_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                dataHDDA.HDDA_TENOR_MONTH_EXT = DateTime.ParseExact(dataDetail.hedging_deal.tenor.start_date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MMM");
                                dataHDDA.HDDA_TENOR_YEAR_EXT = DateTime.ParseExact(dataDetail.hedging_deal.tenor.start_date, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy");
                            }
                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.tenor.end_date))
                                dataHDDA.HDDA_TENOR_DATE_END = DateTime.ParseExact(dataDetail.hedging_deal.tenor.end_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataHDDA.HDDA_TENOR_RESULT = dataDetail.hedging_deal.tenor.result;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.tenor.excerise_date))
                                dataHDDA.HDDA_EXCERISE_DATE = DateTime.ParseExact(dataDetail.hedging_deal.tenor.excerise_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.tenor.extend_date))
                                dataHDDA.HDDA_TENOR_DATE_EXT = DateTime.ParseExact(dataDetail.hedging_deal.tenor.extend_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataHDDA.HDDA_TENOR_RESULT_EXT = dataDetail.hedging_deal.tenor.result_extend;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.volume_month))
                                dataHDDA.HDDA_VOL_MNT = Convert.ToDecimal(dataDetail.hedging_deal.volume_month);

                            dataHDDA.HDDA_VOL_MNT_UNIT = dataDetail.hedging_deal.volume_month_unit;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.volume_month_total))
                                dataHDDA.HDDA_LB_FEE = Convert.ToDecimal(dataDetail.hedging_deal.volume_month_total);

                            dataHDDA.HDDA_VOL_MNT_TOOL_UNIT = dataDetail.hedging_deal.volume_month_total_unit;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.price))
                                dataHDDA.HDDA_PRICE = Convert.ToDecimal(dataDetail.hedging_deal.price);

                            dataHDDA.HDDA_PRICE_UNIT = dataDetail.hedging_deal.unit_price;

                            if (!string.IsNullOrEmpty(dataDetail.hedging_deal.payment_day))
                                dataHDDA.HDDA_PAYMENT_DAY = Convert.ToDecimal(dataDetail.hedging_deal.payment_day);

                            dataHDDA.HDDA_PAYMENT_DAY_UNIT = dataDetail.hedging_deal.payment_day_unit;
                            dataHDDA.HDDA_FK_TOOL = dataDetail.hedging_deal.tool;

                            if (dataDetail.hedging_deal.verify != null)
                            {
                                dataHDDA.HDDA_VRF_CREDIT_RT_STATUS = dataDetail.hedging_deal.verify.credit_rating;
                                dataHDDA.HDDA_VRF_CREDIT_RT_REASON = dataDetail.hedging_deal.verify.credit_rating_reason;
                                dataHDDA.HDDA_VRF_CREDIT_RT_FILE = dataDetail.hedging_deal.verify.credit_rating_file;

                                dataHDDA.HDDA_VRF_CDS_STATUS = dataDetail.hedging_deal.verify.cds;
                                dataHDDA.HDDA_VRF_CDS_REASON = dataDetail.hedging_deal.verify.cds_reason;
                                dataHDDA.HDDA_VRF_CDS_FILE = dataDetail.hedging_deal.verify.cds_file;

                                dataHDDA.HDDA_VRF_CREDIT_LM_STATUS = dataDetail.hedging_deal.verify.credit_limit;
                                dataHDDA.HDDA_VRF_CREDIT_LM_REASON = dataDetail.hedging_deal.verify.credit_limit_reason;
                                dataHDDA.HDDA_VRF_CREDIT_LM_FILE = dataDetail.hedging_deal.verify.credit_limit_file;

                                dataHDDA.HDDA_VRF_ANNUAL_FW_STATUS = dataDetail.hedging_deal.verify.annual_framework;
                                dataHDDA.HDDA_VRF_ANNUAL_FW_REASON = dataDetail.hedging_deal.verify.annual_framework_reason;
                                dataHDDA.HDDA_VRF_ANNUAL_FW_FILE = dataDetail.hedging_deal.verify.annual_framework_file;

                                dataHDDA.HDDA_VRF_COMMITTEE_FW_STATUS = dataDetail.hedging_deal.verify.committee_framework;
                                dataHDDA.HDDA_VRF_COMMITTEE_FW_REASON = dataDetail.hedging_deal.verify.committee_framework_reason;
                                dataHDDA.HDDA_VRF_COMMITTEE_FW_FILE = dataDetail.hedging_deal.verify.committee_framework_file;

                                dataHDDA.HDDA_VRF_OTHER_STATUS = dataDetail.hedging_deal.verify.other_bypass;
                                dataHDDA.HDDA_VRF_OTHER_REASON = dataDetail.hedging_deal.verify.other_reason;
                                dataHDDA.HDDA_VRF_OTHER_FILE = dataDetail.hedging_deal.verify.other_file;
                            }

                            dataHDDA.HDDA_CREATED = dtNow;
                            dataHDDA.HDDA_CREATED_BY = User;
                            dataHDDA.HDDA_UPDATED = dtNow;
                            dataHDDA.HDDA_UPDATED_BY = User;
                            dataHDDA.HDDA_REMARK = dataDetail.hedging_deal.note;
                            dataHDDA.HDDA_REASON = reason;
                            dataHDDA.HDDA_FK_TICKET = dataDetail.hedging_deal.ref_ticket_no;
                            dataHDDA.HDDA_REF_DEAL_NO = dataDetail.hedging_deal.ref_deal_no;

                            dataHDDA.HDDA_STATUS_TRACKING = etxValue.GetValue(CPAIConstantUtil.NextStatus);

                            dataDAL.Save(dataHDDA, context);
                            #endregion

                            #region HEDG_DEAL_BID_PRICE
                            HEDG_DEAL_BID_PRICE_DAL dataPriceDAL = new HEDG_DEAL_BID_PRICE_DAL();
                            if (dataDetail.hedging_deal.other_bid_price != null)
                            {
                                foreach (var item in dataDetail.hedging_deal.other_bid_price)
                                {
                                    HEDG_DEAL_BID_PRICE dataPrice = new HEDG_DEAL_BID_PRICE();
                                    dataPrice.HDBP_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataPrice.HDBP_FK_DEAL = TransID;
                                    dataPrice.HDBP_ORDER = item.order;
                                    dataPrice.HDBP_FK_CONTER_PARTIES = item.counter_parties;
                                    if (!string.IsNullOrEmpty(item.price))
                                    {
                                        dataPrice.HDBP_PRICE = Convert.ToDecimal(item.price);
                                    }

                                    dataPrice.HDBP_CREATED = dtNow;
                                    dataPrice.HDBP_CREATED_BY = User;
                                    dataPrice.HDBP_UPDATED = dtNow;
                                    dataPrice.HDBP_UPDATED_BY = User;
                                    dataPriceDAL.Save(dataPrice, context);
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIHedgingDealUpdateDataState :: Exception >>>  " + tem);
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIHedgingDealUpdateDataState :: Exception >>>  " + tem);
            }
            #endregion
            log.Info("# End State CPAIHedgingDealUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        public bool InsertJson()
        {
            bool IsSuccess = false;
            return IsSuccess;
        }

        public bool UpdateDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, string reason)
        {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingDealUpdateDataState >> UpdateDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            //update CPAI_HEDG_DAIL 
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_REASON = reason;
                            dataHDDA.HDDA_STATUS = NextStatus;
                            dataHDDA.HDDA_STATUS_TRACKING = NextStatus;
                            dataHDDA.HDDA_UPDATED = DateTime.Now;
                            dataHDDA.HDDA_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            dataDAL.UpdateStatus(dataHDDA, context);
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingDealUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIHedgingDealUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        public bool UpdateCompleteDB(string TransID, string NextStatus, Dictionary<string, ExtendValue> etxValue, HedgDealRootObject dataDetail, string reason)
        {
            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingDealUpdateDataState >> UpdateCompleteDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);

                            //update CPAI_HEDG_DAIL 
                            HEDG_DEAL_DATA dataHDDA = new HEDG_DEAL_DATA();
                            HEDG_DEAL_DATA_DAL dataDAL = new HEDG_DEAL_DATA_DAL();
                            dataHDDA.HDDA_ROW_ID = TransID;
                            dataHDDA.HDDA_REASON = reason;
                            dataHDDA.HDDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataHDDA.HDDA_UPDATED = dtNow;
                            dataHDDA.HDDA_UPDATED_BY = User;
                            dataDAL.UpdateStatus(dataHDDA, context);

                            #region HEDG_DEAL_CONTRACT

                            HEDG_DEAL_CONTRACT_DAL dataContractDAL = new HEDG_DEAL_CONTRACT_DAL();
                            if (dataDetail.hedging_deal.contract != null)
                            {
                                dataContractDAL.Delete(TransID);
                                foreach (var item in dataDetail.hedging_deal.contract)
                                {
                                    HEDG_DEAL_CONTRACT dataContract = new HEDG_DEAL_CONTRACT();
                                    dataContract.HDCT_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataContract.HDCT_FK_DEAL = TransID;
                                    dataContract.HDCT_ORDER = item.order;
                                    dataContract.HDCT_CONTRACT_NO = item.contract_no;
                                    dataContract.HDCT_CONTRACT_FILE = item.contract_file;
                                    dataContract.HDCT_CREATED = dtNow;
                                    dataContract.HDCT_CREATED_BY = User;
                                    dataContract.HDCT_UPDATED = dtNow;
                                    dataContract.HDCT_UPDATED_BY = User;
                                    dataContractDAL.Save(dataContract, context);
                                }
                            }
                            #endregion

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingDealUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAIHedgingDealUpdateDataState >> UpdateDB # ");
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingDealUpdateDataState >> UpdateDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}