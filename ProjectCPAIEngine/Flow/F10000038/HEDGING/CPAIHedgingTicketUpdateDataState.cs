﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCharter;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.DALVessel;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
//using service_model = ProjectCPAIEngine.Areas.CPAIMVC.Models;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using static ProjectCPAIEngine.Areas.CPAIMVC.ViewModels.HedgingTicketViewModel;

namespace ProjectCPAIEngine.Flow.F10000038
{
    public class CPAIHedgingTicketUpdateDataState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingTicketUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            ShareFn _FN = new ShareFn();
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string EngineNote = String.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Note)) ?  "-" : etxValue.GetValue(CPAIConstantUtil.Note);

                TradingTicketRootObject dataDetail = null;
                if (item != null)
                {
                    dataDetail = JSonConvertUtil.jsonToModel<TradingTicketRootObject>(item);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    string currentStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                    string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    string User = etxValue.GetValue(CPAIConstantUtil.User);

                    string t_ticket_no = etxValue.GetValue(CPAIConstantUtil.DocNo);
                    string t_ticket_date = dataDetail.trading_ticket.ticket_date;
                    string t_trader = dataDetail.trading_ticket.trader;
                    string t_deal_no = "";
                    string t_counterparties = "";
                    foreach (var excItem in dataDetail.deal_detail.hedging_execution.hedging_execution_data) {
                        t_deal_no += t_deal_no != "" ? "|":"";
                        t_deal_no += excItem.deal_no;

                        t_counterparties += t_counterparties != "" ? "|" : "";
                        t_counterparties += excItem.counter_parties_id;
                    }

                    string t_tool = dataDetail.deal_detail.tool_id;
                    string t_underlying = dataDetail.deal_detail.underlying_id;
                    string t_underlying_vs = dataDetail.deal_detail.underlying_vs_id;
                    string t_dealtype = dataDetail.deal_detail.deal_type;

                    //if (currentAction.Equals(CPAIConstantUtil.ACTION_SUBMIT) 
                    //    || currentAction.Equals(CPAIConstantUtil.ACTION_REJECT) 
                    //    || currentAction.Equals(CPAIConstantUtil.ACTION_EDIT))
                    //{
                    #region add data history
                    DataHistoryDAL dthMan = new DataHistoryDAL();
                        DateTime now = DateTime.Now;
                        CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                        dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                        dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                        dataHistory.DTH_ACTION_DATE = now;
                        dataHistory.DTH_ACTION_BY = User;
                        dataHistory.DTH_NOTE = EngineNote;
                        dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                        dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                        dataHistory.DTH_REJECT_FLAG = etxValue.GetValue(CPAIConstantUtil.RejectFlag);
                        dataHistory.DTH_CREATED_BY = User;
                        dataHistory.DTH_CREATED_DATE = now;
                        dataHistory.DTH_UPDATED_BY = User;
                        dataHistory.DTH_UPDATED_DATE = now;
                        dthMan.Save(dataHistory);
                        #endregion

                        #region Insert Update DB

                        bool isRunDB = false;
                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1) 
                        || (currentAction.Equals(CPAIConstantUtil.ACTION_DRAFT) 
                        && !currentAction.Equals(CPAIConstantUtil.ACTION_REJECT)))
                        {
                            if (DealListIsValid(dataDetail.deal_detail.hedging_execution.hedging_execution_data))
                            {
                                isRunDB = saveDB(stateModel.EngineModel.ftxTransId, etxValue, dataDetail, EngineNote, etxValue.GetValue(CPAIConstantUtil.DocNo));
                            }
                            else {
                                currentCode = CPAIConstantRespCodeUtil.DEAL_TICKETED_RESP_CODE;
                                //throw new Exception(currentCode);
                            }
                        }
                        else
                        {
                            isRunDB = updateStatusDB(stateModel.EngineModel.ftxTransId,dataDetail.deal_detail.removed_deals, stateModel.EngineModel.nextState, etxValue,dataDetail.deal_detail.hedging_execution.hedging_execution_data, EngineNote);
                        }
                        #endregion

                        #region rejectFlag
                        if (isRunDB)
                        {
                            //set reject flag
                            dthMan.UpdateRejectFlag(stateModel.EngineModel.ftxRowId, etxValue.GetValue(CPAIConstantUtil.User), "Y");
                        }
                        #endregion

                        #region INSERT JSON
                        if (isRunDB)
                        {
                            //set data detail (json when action = "-" or DRAFT or SUBMIT only)
                            #region Set dataDetail
                            etxValue.SetValue(CPAIConstantUtil.System, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Type, new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.Type), encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });

                            etxValue.SetValue(CPAIConstantUtil.Hedg_Ticket_No,new ExtendValue { value= t_ticket_no,encryptFlag = "N"});
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Ticket_Date, new ExtendValue { value = t_ticket_date, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Ticket_Trader,new ExtendValue { value = t_trader, encryptFlag="N"});
                            if (currentAction.Equals(CPAIConstantUtil.ACTION_REJECT))
                            {
                                etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_No, new ExtendValue { value = "", encryptFlag = "N" });
                            }
                            else if(currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_1)){
                                etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_No, new ExtendValue { value = t_deal_no, encryptFlag = "N" });
                            }
                            
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Tools,new ExtendValue { value= t_tool,encryptFlag="N"});
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying,new ExtendValue { value = t_underlying,encryptFlag="N"});
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Underlying_vs,new ExtendValue { value = t_underlying_vs,encryptFlag="N"});
                            etxValue.SetValue(CPAIConstantUtil.Hedg_Deal_Type,new ExtendValue { value = t_dealtype,encryptFlag="N"});
                        etxValue.SetValue(CPAIConstantUtil.Hedg_Counterparties,new ExtendValue { value = t_counterparties, encryptFlag="N"});
                            etxValue.SetValue(CPAIConstantUtil.CreateBy,new ExtendValue { value= User,encryptFlag="N"});

                        //set data_detail
                        string dataDetailString = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                            if (!string.IsNullOrEmpty(dataDetailString))
                            {
                                etxValue.SetValue(CPAIConstantUtil.DataDetailInput, new ExtendValue { value = dataDetailString, encryptFlag = "N" });
                            }
                            #endregion
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                        }
                        else
                        {
                            if(currentCode != CPAIConstantRespCodeUtil.DEAL_TICKETED_RESP_CODE)
                                currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                        }
                        #endregion
                    }
                //}
                else
                {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingTicketUpdateDataState # :: Code >>> " + currentCode);

            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingTicketUpdateDataState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingTicketUpdateDataState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public bool saveDB(string TransID, Dictionary<string, ExtendValue> etxValue, TradingTicketRootObject dataDetail, string reason, string docNo) {
            log.Info("# Start State CPAIHedgingTicketUpdateDataState >> updateStatusDB #  ");
            #region Insert data to DB

            DateTime dtNow = DateTime.Now;
            bool isSuccess = false;

            try
            {
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            string User = etxValue.GetValue(CPAIConstantUtil.User);

                            string ticketRowId = TransID;

                            #region Trading Ticket
                            HEDG_TICKET_DAL ticketDAL = new HEDG_TICKET_DAL();
                            HEDG_TICKET dataTicket = null;

                            string t_next_status = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            string t_current_action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);


                            dataTicket = ticketDAL.GetTicketByTicketId(ticketRowId);
                            bool isNewRecord = true;
                            if (dataTicket != null)
                            {
                                isNewRecord = false;
                            }
                            else {
                                dataTicket = new HEDG_TICKET();
                            }
                             
                            dataTicket.HTDA_ROW_ID = ticketRowId;
                            dataTicket.HTDA_TICKET_NO = docNo;
                            dataTicket.HTDA_TRADER = dataDetail.trading_ticket.trader;

                            if (!string.IsNullOrEmpty(dataDetail.trading_ticket.ticket_date))
                                dataTicket.HTDA_TICKET_DATE = DateTime.ParseExact(dataDetail.trading_ticket.ticket_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dataTicket.HTDA_STATUS = t_next_status;
                            dataTicket.HTDA_CHART_LIMIT = dataDetail.deal_detail.hedging_chart.limit;
                            dataTicket.HTDA_CHART_TARGET = dataDetail.deal_detail.hedging_chart.target;
                            dataTicket.HTDA_CHART_VOL = dataDetail.deal_detail.hedging_chart.limit;
                            dataTicket.HTDA_REMARK = dataDetail.trading_ticket.note;
                            dataTicket.HTDA_DEAL_TYPE = dataDetail.deal_detail.deal_type;
                            dataTicket.HTDA_UNDERLYING_ID = dataDetail.deal_detail.underlying_id;
                            dataTicket.HTDA_UNDERLYING_NAME = dataDetail.deal_detail.underlying_name;

                            if (!string.IsNullOrEmpty(dataDetail.deal_detail.underlying_vs_id))
                                dataTicket.HTDA_UNDERLYING_VS_ID = dataDetail.deal_detail.underlying_vs_id;

                            if (!string.IsNullOrEmpty(dataDetail.deal_detail.underlying_vs_name))
                                dataTicket.HTDA_UNDERLYING_VS_NAME = dataDetail.deal_detail.underlying_vs_name;
                            dataTicket.HTDA_TOOL_ID = dataDetail.deal_detail.tool_id;
                            dataTicket.HTDA_TOOL_NAME = dataDetail.deal_detail.tool_name;
                            dataTicket.HTDA_EXC_PRC_UNIT = dataDetail.deal_detail.hedging_execution.price_unit;
                            dataTicket.HTDA_EXC_VOL_UNIT = dataDetail.deal_detail.hedging_execution.volume_mnt_unit;
                            dataTicket.HTDA_EXC_PST_PRC_UNIT = dataDetail.deal_detail.hedging_execution.hedging_position.price_unit;
                            dataTicket.HTDA_EXC_PST_VOL_UNIT = dataDetail.deal_detail.hedging_execution.hedging_position.volume_unit;
                            dataTicket.HTDA_CREATED = dtNow;
                            dataTicket.HTDA_CREATED_BY = User;
                            dataTicket.HTDA_UPDATED = dtNow;
                            dataTicket.HTDA_UPDATED_BY = User;
                            dataTicket.HTDA_REASON = reason;
                            dataTicket.HTDA_FK_FW_ANNUAL_D = dataDetail.deal_detail.annual_detail_id;
                            dataTicket.HTDA_HEDG_TYPE_NAME = dataDetail.deal_detail.hedg_type_name;
                            dataTicket.HTDA_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);

                            if (isNewRecord)
                            {
                                dataTicket.HTDA_REVISION = "0";
                                ticketDAL.Save(dataTicket, context);
                            }
                            else {
                                dataTicket.HTDA_REVISION = (int.Parse(dataTicket.HTDA_REVISION)+1).ToString();
                                ticketDAL.Update(dataTicket, context);
                            }
                            

                            #endregion

                            #region Execution

                            HEDG_TICKET_EXECUTE_DAL executionDAL = new HEDG_TICKET_EXECUTE_DAL();
                            HEDG_TICKET_EXECUTE dataExecution = null;

                            executionDAL.DeleteAll(ticketRowId);

                            HEDG_DEAL_DATA_DAL dealDAL = new HEDG_DEAL_DATA_DAL();
                            HEDG_DEAL_DATA dataDeal = null;
                            foreach (var excItem in dataDetail.deal_detail.hedging_execution.hedging_execution_data)
                            {
                                if (excItem.record_status.ToUpper().Equals("N"))
                                {
                                    dataExecution = new HEDG_TICKET_EXECUTE();
                                    dataExecution.HTDA_ROW_ID = Guid.NewGuid().ToString().Replace("-", "");

                                    dataExecution.HTDA_ORDER = excItem.order;
                                    dataExecution.HTDA_FK_TICKET = ticketRowId;
                                    dataExecution.HTDA_FK_DEAL = excItem.deal_id;

                                    if (!string.IsNullOrEmpty(excItem.deal_date))
                                        dataExecution.HTDA_DEAL_DATE = DateTime.ParseExact(excItem.deal_date, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    dataExecution.HTDA_TENOR = excItem.tenor;
                                    dataExecution.HTDA_DEAL_NO = excItem.deal_no;
                                    if (!string.IsNullOrEmpty(excItem.counter_parties_id))
                                        dataExecution.HTDA_COUNTER_PARTIES_ID = excItem.counter_parties_id;
                                    if (!string.IsNullOrEmpty(excItem.counter_parties_name))
                                        dataExecution.HTDA_COUNTER_PARTIES_NAME = excItem.counter_parties_name;

                                    if (!string.IsNullOrEmpty(excItem.price))
                                        dataExecution.HTDA_PRICE = Convert.ToDecimal(excItem.price);

                                    if (!string.IsNullOrEmpty(excItem.volume))
                                        dataExecution.HTDA_VOLUME = Convert.ToDecimal(excItem.volume);
                                    if (excItem.bid_price != null)
                                    {
                                        if (excItem.bid_price.Count > 0)
                                            dataExecution.HTDA_BID_PRICE = new JavaScriptSerializer().Serialize(excItem.bid_price);
                                    }
                                    else
                                    {
                                        dataExecution.HTDA_BID_PRICE = "";
                                    }

                                    dataExecution.HTDA_CREATED = dtNow;
                                    dataExecution.HTDA_CREATED_BY = User;
                                    dataExecution.HTDA_UPDATED = dtNow;
                                    dataExecution.HTDA_UPDATED_BY = User;

                                    executionDAL.Save(dataExecution, context);
                                }
                                else if (excItem.record_status.ToUpper().Equals("D")) {
                                    dataDeal = new HEDG_DEAL_DATA();
                                    executionDAL.Delete(dataTicket.HTDA_ROW_ID, excItem.deal_id, context);
                                    dataDeal.HDDA_ROW_ID = excItem.deal_id;
                                    dataDeal.HDDA_TICKET_NO = null;
                                    dataDeal.HDDA_FK_TICKET = null;
                                    dataDeal.HDDA_UPDATED_BY = User;
                                    dataDeal.HDDA_UPDATED = DateTime.Now;
                                    dealDAL.Update(dataDeal,context);
                                }
                            }


                            #endregion

                            #region Position

                            HEDG_TICKET_POST_DAL positionDAL = new HEDG_TICKET_POST_DAL();
                            HEDG_TICKET_POST dataPosition = null;

                            positionDAL.DeleteByTicketId(ticketRowId,context);

                            foreach (var postItem in dataDetail.deal_detail.hedging_execution.hedging_position.hedging_position_data)
                            {
                                dataPosition = new HEDG_TICKET_POST();
                                dataPosition.HTDA_ROW_ID = Guid.NewGuid().ToString().Replace("-", "");
                                dataPosition.HTDA_ORDER = postItem.order;
                                dataPosition.HTDA_FK_TICKET = ticketRowId;
                                dataPosition.HTDA_TENOR = postItem.tenor;

                                if (!string.IsNullOrEmpty(postItem.price))
                                    dataPosition.HTDA_PRICE = Convert.ToDecimal(postItem.price);

                                if (!string.IsNullOrEmpty(postItem.volume))
                                    dataPosition.HTDA_VOLUME = Convert.ToDecimal(postItem.volume);

                                dataPosition.HTDA_CREATED = dtNow;
                                dataPosition.HTDA_CREATED_BY = User;
                                dataPosition.HTDA_UPDATED = dtNow;
                                dataPosition.HTDA_UPDATED_BY = User;

                                positionDAL.Save(dataPosition, context);
                            }

                            #endregion

                            #region Target
                            HEDG_TICKET_TARGET_DAL targetDAL = new HEDG_TICKET_TARGET_DAL();
                            HEDG_TICKET_TARGET dataTarget = null;

                            targetDAL.DeleteByTicketId(ticketRowId,context);

                            //for (int i = 0;i< dataDetail.deal_detail.hedging_target.Count;i++) {
                            //    HedgingTarget targetItem = dataDetail.deal_detail.hedging_target[0];
                            foreach (var targetItem in dataDetail.deal_detail.hedging_target)
                            {
                                dataTarget = new HEDG_TICKET_TARGET();
                                dataTarget.HTDA_ROW_ID = Guid.NewGuid().ToString().Replace("-", "");
                                dataTarget.HTDA_FK_TICKET = ticketRowId;
                                dataTarget.HTDA_ORDER = targetItem.order;
                                dataTarget.HTDA_TENOR = targetItem.tenor;

                                if (!string.IsNullOrEmpty(targetItem.price))
                                    dataTarget.HTDA_PRICE = Convert.ToDecimal(targetItem.price);

                                if (!string.IsNullOrEmpty(targetItem.volume))
                                    dataTarget.HTDA_VOLUME = Convert.ToDecimal(targetItem.volume);
                                if (!string.IsNullOrEmpty(targetItem.remaining))
                                    dataTarget.HTDA_REMAINING = Convert.ToDecimal(targetItem.remaining);
                                dataTarget.HTDA_CREATED = dtNow;
                                dataTarget.HTDA_CREATED_BY = User;
                                dataTarget.HTDA_UPDATED = dtNow;
                                dataTarget.HTDA_UPDATED_BY = User;

                                targetDAL.Save(dataTarget, context);
                            }

                            #endregion

                            #region Attached Files
                            HEDG_TICKET_ATTACH_FILE_DAL fileDAL = new HEDG_TICKET_ATTACH_FILE_DAL();
                            HEDG_TICKET_ATTACH_FILE dataFile = new HEDG_TICKET_ATTACH_FILE();

                            fileDAL.DeleteByTicketId(ticketRowId, context);

                            string attach_items = etxValue.GetValue(CPAIConstantUtil.AttachItems);
                            attach_file Att = new JavaScriptSerializer().Deserialize<attach_file>(attach_items);
                            foreach (var item in Att.attach_items)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    dataFile = new HEDG_TICKET_ATTACH_FILE();
                                    dataFile.HTAF_ROW_ID = Guid.NewGuid().ToString("N");
                                    dataFile.HTAF_FK_TICKET = ticketRowId;
                                    dataFile.HTAF_PATH = item;
                                    dataFile.HTAF_INFO = "";
                                    dataFile.HTAF_TYPE = "ATT";
                                    dataFile.HTAF_CREATED = dtNow;
                                    dataFile.HTAF_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    dataFile.HTAF_UPDATED = dtNow;
                                    dataFile.HTAF_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                    fileDAL.Save(dataFile, context);
                                }
                            }

                            #endregion

                            if (t_current_action.Equals(CPAIConstantUtil.ACTION_APPROVE_1))
                            {
                                #region TicketNo stamp on deals
                                HEDG_DEAL_DATA ent = new HEDG_DEAL_DATA();
                                HEDG_DEAL_DATA_DAL dal = new HEDG_DEAL_DATA_DAL();

                                FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
                                List<FUNCTION_TRANSACTION> listTran;
                                FUNCTION_TRANSACTION modelTran;
                                foreach (var excItem in dataDetail.deal_detail.hedging_execution.hedging_execution_data)
                                {

                                    DateTime dt;

                                    dt = new DateTime();
                                    ent = context.HEDG_DEAL_DATA.Where(x => x.HDDA_DEAL_NO.Trim().ToUpper() == excItem.deal_no.Trim().ToUpper()).FirstOrDefault();
                                    if (ent != null)
                                    {
                                        var _search = context.HEDG_DEAL_DATA.SingleOrDefault(p => p.HDDA_ROW_ID == ent.HDDA_ROW_ID);
                                        #region Set Value
                                        _search.HDDA_TICKET_NO = docNo;
                                        _search.HDDA_FK_TICKET = ticketRowId;
                                        _search.HDDA_FLAG_TICKET = CPAIConstantUtil.Hedg_Deal_Track_Status_Ticketed;
                                        _search.HDDA_STATUS_TRACKING = CPAIConstantUtil.Hedg_Deal_Track_Status_Ticketed;
                                        _search.HDDA_UPDATED_BY = User;
                                        _search.HDDA_UPDATED = dt;
                                        #endregion
                                        context.SaveChanges();
                                    }

                                    listTran = funcTran.GetDealByDealNo(excItem.deal_no.Trim().ToUpper());
                                    foreach (var item in listTran)
                                    {
                                        modelTran = item;
                                        modelTran.FTX_INDEX6 = item.FTX_INDEX6;
                                        modelTran.FTX_INDEX7 = docNo.Trim();
                                        modelTran.FTX_INDEX20 = CPAIConstantUtil.Hedg_Deal_Track_Status_Ticketed;
                                        modelTran.FTX_UPDATED_BY = User;
                                        modelTran.FTX_UPDATED = dt;
                                        funcTran.UpdateTicket(modelTran);
                                    }
                                }
                                #endregion

                                #region TicketNo stamp on deals
                                //foreach (var excItem in dataDetail.deal_detail.hedging_execution.hedging_execution_data)
                                //{
                                //    new service_model.HedgingDealServiceModel().AssignTicketNo(docNo, ticketRowId, excItem.deal_no, User, context);

                                //}
                                #endregion
                            }
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error CPAIHedgingTicketUpdateDataState >> updateStatusDB :: Exception >>>  " + tem);
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error CPAIHedgingTicketUpdateDataState >> updateStatusDB :: Exception >>>  " + tem);
                throw;
            }

            #endregion
            log.Info("# End State CPAIHedgingTicketUpdateDataState >> updateStatusDB # ");
            return isSuccess;
        }
        public bool updateStatusDB(string TranId,string[]removeDeals, string NextStatus, Dictionary<string, ExtendValue> etxValue,List<HedgingExecutionData> hdgExcData, string note) {
            bool isSuccess = false;
            try
            {
                log.Info("# Start State CPAIHedgingTicketUpdateDataState >> updateStatusDB #  ");
                using (var context = new EntityCPAIEngine())
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            HEDG_TICKET dataTicket = new HEDG_TICKET();
                            HEDG_TICKET_DAL ticketDAL = new HEDG_TICKET_DAL();
                            string t_user = etxValue.GetValue(CPAIConstantUtil.User);

                            dataTicket.HTDA_ROW_ID = TranId;
                            dataTicket.HTDA_STATUS = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                            dataTicket.HTDA_REASON = note;
                            dataTicket.HTDA_UPDATED = DateTime.Now;
                            dataTicket.HTDA_UPDATED_BY = t_user;
                            ticketDAL.UpdateStatus(dataTicket,context);

                            HEDG_DEAL_DATA_DAL dealDAL = new HEDG_DEAL_DATA_DAL();
                            HEDG_DEAL_DATA dataDeal = null;

                            FunctionTransactionDAL funcTran = new FunctionTransactionDAL();
                            List<FUNCTION_TRANSACTION> listTran;
                            FUNCTION_TRANSACTION modelTran;

                            foreach (var excItem in hdgExcData)
                            {
                                dataDeal = new HEDG_DEAL_DATA();
                                dataDeal.HDDA_ROW_ID = excItem.deal_id;
                                dataDeal.HDDA_UPDATED_BY = t_user;
                                dataDeal.HDDA_UPDATED = DateTime.Now;
                                dataDeal.HDDA_FK_TICKET = dataTicket.HTDA_ROW_ID;
                                if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REJECT))
                                {
                                    dataDeal.HDDA_TICKET_NO = null;
                                    dataDeal.HDDA_FK_TICKET = null;
                                    dataDeal.HDDA_FLAG_TICKET += "_R";
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.STATUS_SUBMIT;
                                }else if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_CANCEL))
                                {
                                    dataDeal.HDDA_TICKET_NO = null;
                                    dataDeal.HDDA_FK_TICKET = null;
                                    dataDeal.HDDA_FLAG_TICKET += "_C";
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.STATUS_SUBMIT;
                                }
                                else if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_APPROVE_2)) {
                                    dataDeal.HDDA_FLAG_TICKET = CPAIConstantUtil.Hedg_Deal_Track_Status_Verified;
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.Hedg_Deal_Track_Status_Verified;
                                }
                                else if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_APPROVE_3))
                                {
                                    dataDeal.HDDA_FLAG_TICKET = CPAIConstantUtil.Hedg_Deal_Track_Status_Endorsed;
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.Hedg_Deal_Track_Status_Endorsed;
                                }
                                else if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_APPROVE_4))
                                {
                                    dataDeal.HDDA_FLAG_TICKET = CPAIConstantUtil.Hedg_Deal_Track_Status_Approved;
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.Hedg_Deal_Track_Status_Approved;
                                }
                                else if (etxValue.GetValue(CPAIConstantUtil.CurrentAction).Equals(CPAIConstantUtil.ACTION_REVISE))
                                {
                                    dataDeal.HDDA_FLAG_TICKET = CPAIConstantUtil.Hedg_Deal_Track_Status_Revised;
                                    dataDeal.HDDA_STATUS_TRACKING = CPAIConstantUtil.Hedg_Deal_Track_Status_Revised;
                                }
                                dealDAL.Update(dataDeal, context);

                                listTran = funcTran.GetDealByDealNo(excItem.deal_no.Trim().ToUpper());
                                foreach (var item in listTran)
                                {
                                    modelTran = item;

                                    modelTran.FTX_INDEX20 = dataDeal.HDDA_STATUS_TRACKING;
                                    modelTran.FTX_UPDATED_BY = t_user;
                                    modelTran.FTX_UPDATED = DateTime.Now;
                                    funcTran.UpdateTicket(modelTran);
                                }
                            }

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Info("# Error CPAIHedgingTicketUpdateDataState >> updateStatusDB # :: Exception >>> " + ex.Message);
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                        log.Info("# End State CPAIHedgingTicketUpdateDataState >> updateStatusDB # ");
                    }
                }
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingTicketUpdateDataState >> updateStatusDB # :: Exception >>> " + ex.Message);
                string res = ex.Message;
                isSuccess = false;
            }
            return isSuccess;
        }

        private bool DealListIsValid(List<HedgingExecutionData> hdgExcData) {
            bool isSuccess = false;
            HEDG_DEAL_DATA_DAL dealDAL = new HEDG_DEAL_DATA_DAL();
            HEDG_DEAL_DATA dealData = null;
            try
            {
                foreach (var excItem in hdgExcData)
                {
                    dealData = dealDAL.GetDEALDATAByID(excItem.deal_id);
                    if (dealData != null)
                    {
                        if (string.IsNullOrEmpty(dealData.HDDA_TICKET_NO)&&!dealData.HDDA_STATUS.Equals(CPAIConstantUtil.STATUS_HOLD))
                        {
                            isSuccess = true;
                        }
                        else {
                            isSuccess = false;
                            break;
                        }
                    }
                    else {
                        isSuccess = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIHedgingTicketUpdateDataState >> DealListIsValid # :: Exception >>> " + ex.Message);
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}