﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000038
{
    public class CPAIHedgingTicketVerifyRequireInputContinueState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyListTxnHedgingDealRequireInputState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
            string action = etxValue.GetValue(CPAIConstantUtil.CurrentAction);

            try
            {
                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.NextStatus)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_NEXT_STATUS_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_PRE_DEALS_SYSTEM_RESP_CODE;
                // else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.UserGroup)))
                //     currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                //else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Note)))
                //    currentCode = CPAIConstantRespCodeUtil.INVALID_NOTE_RESP_CODE;
                else
                {
                    if (action.Equals(CPAIConstantUtil.ACTION_APPROVE + "_1") || action.Equals(CPAIConstantUtil.ACTION_DRAFT))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.DataDetailInput)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                        }
                        else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.CurrentAction)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_CURRENT_ACTION_RESP_CODE;
                        }
                        else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_PRE_DEALS_SYSTEM_RESP_CODE;
                        }
                        else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Type)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_PRE_DEALS_TYPE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];                            
                        }
                    }
                    else if (action.Equals(CPAIConstantUtil.ACTION_REJECT) || action.Equals(CPAIConstantUtil.ACTION_CANCEL) || action.Contains(CPAIConstantUtil.ACTION_APPROVE))
                    {
                        if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Action)))
                        {
                            currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                        }
                        else
                        {
                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];                            
                        }
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                    }
                }
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyListTxnHedgingDealRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIVerifyListTxnHedgingDealRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyListTxnHedgingDealRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}