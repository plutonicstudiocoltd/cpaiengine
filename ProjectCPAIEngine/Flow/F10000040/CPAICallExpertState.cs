﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000040
{
    public class CPAICallExpertState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICallExpertState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                COO_EXPERT_DAL expertDal = new COO_EXPERT_DAL();
                List<COO_EXPERT> experts = expertDal.GetAllByID(stateModel.EngineModel.ftxTransId);

                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                CooRootObject dataDetail = JSonConvertUtil.jsonToModel<CooRootObject>(item);
                string json = new JavaScriptSerializer().Serialize(dataDetail);

                //call function F10000041 : APPROVE_1
                RequestCPAI req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000041;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.ParentTrxId = stateModel.EngineModel.ftxTransId;
                req.ParentActId = stateModel.EngineModel.parentActID;
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_1 });
                req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE });
                req.Req_parameters.P.Add(new P { K = "user", V = user });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
                req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                req.Req_parameters.P.Add(new P { K = "note", V = "Set expert comment to active" });
                req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                req.Extra_xml = "";

                ResponseData resData = new ResponseData();
                RequestData reqData = new RequestData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                log.Debug(" resp function F10000041 >> " + resData.result_code);

                //call function F10000042 : APPROVE_2
                req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000042;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.ParentTrxId = stateModel.EngineModel.ftxTransId;
                req.ParentActId = stateModel.EngineModel.parentActID;
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_2 });
                req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_EXPERT_APPROVE });
                req.Req_parameters.P.Add(new P { K = "user", V = user });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.COOL });
                req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                req.Req_parameters.P.Add(new P { K = "note", V = "Set expert comment to active" });
                req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                req.Extra_xml = "";

                resData = new ResponseData();
                reqData = new RequestData();
                service = new ServiceProvider.ProjService();

                xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                log.Debug(" resp function F10000042 >> " + resData.result_code);

                currentCode = resData.result_code;

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICallExpertState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICallExpertState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}