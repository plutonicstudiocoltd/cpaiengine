using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000045
{
    public class CPAIListTxnApprovalFormState: BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIApprovalFormMaintainDBState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                List<CPAI_ACTION_FUNCTION> result;
                if (String.IsNullOrWhiteSpace(function_code))
                {
                    //result = acfMan.findByUserGroupAndSystem(userGroup, system);
                    result = acfMan.findByUserGroupAndSystemDelegate(lstUserGroup, system);//set user group deligate by job 25072017
                }
                else
                {
                    //result = acfMan.findByUserGroupAndSystemAndFuncitonId(userGroup, system, function_code);
                    result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, function_code);//set user group deligate by job 25072017
                }
                List<PAFEncrypt> lstAllTx = new List<PAFEncrypt>();
                if (result.Count == 0)
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                        }
                        lstAllTx = lstAllTx.DistinctBy(m => m.req_transaction_id).ToList();
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnApprovalFormState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIListTxnApprovalFormState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void setExtraXml(StateModel stateModel, List<PAFEncrypt> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnPAFState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            List_PAFtrx li = new List_PAFtrx();
            li.PAFTransaction = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnPAFState >> setExtraXml # :: Code >>> ");
        }


        public List<PAFEncrypt> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnPAFState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string user = etxValue.GetValue(CPAIConstantUtil.User);
            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            FLOW_MANAGEMENT_DAL ftxMan = new FLOW_MANAGEMENT_DAL();
            List<FunctionTransaction> result;
            List<PAFEncrypt> list = new List<PAFEncrypt>();
            List<string> lstStatus = new List<string>();
            if (!string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
            {
                lstStatus = new List<string>(actionFunction.ACF_FUNCTION_STATUS.Split('|'));
            }
            result = ftxMan.findCPAIApprovalFormTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, null, fromDate, toDate, null, null, user);
            for (int i = 0; i < result.Count; i++)
            {
                FunctionTransaction ftx = result[i];
                PAFEncrypt m = new PAFEncrypt();
                PAF_DATA data = PAF_DATA_DAL.GetNoWrap(ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                m.status = ftx.STATUS;
                m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                m.assay_ref = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                m.products = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                m.origin = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                m.categories = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                m.kerogen = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                m.characteristic = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                m.maturity = ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                m.assay_date = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                m.assay_from = ftx.ASSAY_FROM;
                m.assay_file = ftx.ASSAY_FILE;
                m.support_doc_file = ftx.SUPPORT_FILE;
                m.density = ftx.DENSITY;
                m.total_sulphur = ftx.SULPHUR;
                m.total_acid_number = ftx.ACID;
                m.expert_units = ftx.UNIT;
                m.requester_name = ftx.REQUESTER_NAME;
                m.purchase_no = data.PDA_FORM_ID;
                m.doc_no = data.PDA_FORM_ID;
                //m.purchase_no = PAF_DATA_DAL.GetPurchaseNO(ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                m.template_name = PAF_DATA_DAL.CreateTemplateName(data.PDA_TEMPLATE);
                m.draft_cam_file = ftx.DRAFT_CAM_FILE;
                m.final_cam_file = ftx.FINAL_CAM_FILE;
                m.draft_cam_status = ftx.DRAFT_CAM_STATUS;
                m.final_cam_status = ftx.FINAL_CAM_STATUS;
                m.updated_date = PAFHelper.ToDateString(data.PDA_UPDATED);
                m.created_date = PAFHelper.ToDateString(data.PDA_CREATED);
                m.updated_by = data.PDA_UPDATED_BY;
                m.created_by = data.PDA_CREATED_BY;
                m.template_id = data.PDA_TEMPLATE;
                m.for_company = (data != null && data.MT_COMPANY != null) ? data.MT_COMPANY.MCO_SHORT_NAME : "";
               
                m.awarded_summary = PAF_DATA_DAL.GetAwardedSummary(data);

                m.list_awarded = PAFListHelper.GetAwarded(data);
                m.list_customer_reviews = PAFListHelper.GetCustomerReview(data);

                m.contract_period = data.PDA_CONTRACT_DATE();
                m.loading_period = data.PDA_LOADING_DATE();
                m.loading_prd = data.PDA_LOADING_DATE();
                m.pricing_period = String.IsNullOrEmpty(data.PDA_PRICING_PERIOD) ? "-" : data.PDA_PRICING_PERIOD;
                m.discharging_period = data.PDA_DISCHARGING_DATE();

                #region bitumen section
                if (data.PAF_BTM_GRADE_ITEMS != null && data.PAF_BTM_GRADE_ITEMS.Count() > 0) {
                    PAF_BTM_GRADE_ITEMS grade = data.PAF_BTM_GRADE_ITEMS.FirstOrDefault();
                    m.bitumen_grade = grade.MT_MATERIALS != null ? grade.MT_MATERIALS.MET_MAT_DES_ENGLISH :null;
                    m.weighted_price = PAFListHelper.GetWeightedPrice(data);
                    m.weighted_price_diff_argus = PAFListHelper.GetWeightedDiffArgus(data);
                    m.weighted_price_diff_hsfo = PAFListHelper.GetWeightedPriceDiffHSFO(data);
                    m.list_bitumen_summary = PAFListHelper.GetBitumenSummary(data);
                }
                #endregion

                #region other form section
                if (data.PDA_TEMPLATE == "CMPS_OTHER")
                {
                    m.topic = data.PDA_TOPIC;
                    m.brief_market = PAFListHelper.GetBriefMarket(data);
                }
                #endregion

                list.Add(m);
            }

            log.Info("# End State CPAIListTxnPAFState >> getTransaction # :: Code >>> ");
            return list;
        }

    }




}
