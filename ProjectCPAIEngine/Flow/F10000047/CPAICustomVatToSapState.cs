﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.VatpostingCreateModel;
using static ProjectCPAIEngine.Model.VatMIROInvoiceServiceModel;
using System.Globalization;

namespace ProjectCPAIEngine.Flow.F10000047
{
    public class CPAICustomVatToSapState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {
            Boolean vatgo = false;
            Boolean mirogo = false;
            decimal totalmiro = 0;

            log.Info("# Start State CPAICustomVatToSapState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                if (item != null)
                {
                    CustomVat dataDetail = JSonConvertUtil.jsonToModel<CustomVat>(item);

                    #region "Vat Posting"
                    // Vat MIRO
                    VatMIROInvoiceServiceModel tempMIRO = new VatMIROInvoiceServiceModel();
                    tempMIRO.headerdata = new List<ZHEADERDATA>();
                    tempMIRO.itemData = new List<ZITEMDATE>();                    
                    var cfgHeaderData = getHeaderData();
                    var cfgdItemData = getItemData();


                    //VatCreate
                    var cfgDocHeader = getDocumentHeader();
                    VatpostingCreateModel temp = new VatpostingCreateModel();
                    temp.documentHeader = new List<BAPIACHE09>();

                    var itemFirst = dataDetail.CustomVat_Calc.FirstOrDefault();
                    BAPIACHE09 tmpDH = new BAPIACHE09();
                    tmpDH.OBJ_TYPE = cfgDocHeader.OBJ_TYPE;
                    tmpDH.OBJ_KEY = cfgDocHeader.OBJ_KEY;
                    tmpDH.BUS_ACT = cfgDocHeader.BUS_ACT;
                    tmpDH.USERNAME = cfgDocHeader.USERNAME;
                    tmpDH.HEADER_TXT = "VAT Crude/" + itemFirst.TripNo.Substring(0,9) + "/" + itemFirst.MetNum.Substring(1,4);
                    tmpDH.COMP_CODE = itemFirst.CompanyCode;
                    tmpDH.DOC_DATE = DateTime.ParseExact(dataDetail.sDocumentDate, format, provider).ToString("yyyy-MM-dd");
                    tmpDH.PSTNG_DATE = DateTime.ParseExact(dataDetail.sPostingDate, format, provider).ToString("yyyy-MM-dd");       
                    tmpDH.FISC_YEAR = DateTime.ParseExact(dataDetail.sPostingDate, format, provider).ToString("yyyy");
                    tmpDH.FIS_PERIOD = DateTime.ParseExact(dataDetail.sPostingDate, format, provider).ToString("MM"); //Month of PSTNG_DATE
                    tmpDH.DOC_TYPE = cfgDocHeader.DOC_TYPE;
                    tmpDH.REF_DOC_NO = cfgDocHeader.REF_DOC_NO;
                    temp.documentHeader.Add(tmpDH);

                    //DOCUMENTHEADER

                    temp.accountPay = new List<BAPIACAP09>();
                    //var cfgAccountPay = getAccountPayable();
                    //foreach (var itemAP in dataDetail.CustomVat_Calc)
                    //{
                    //    BAPIACAP09 tmpAP = new BAPIACAP09();
                    //    tmpAP.VENDOR_NO = cfgAccountPay.VENDOR_NO;
                    //    tmpAP.REF_KEY_1 = cfgAccountPay.REF_KEY_1;
                    //    tmpAP.PMNTTRMS = cfgAccountPay.PMNTTRMS;
                    //    tmpAP.ITEM_TEXT = itemAP.TripNo + "/" + itemAP.MatItemName + "/" + itemAP.VesselName + "/VAT";
                    //    tmpAP.BUSINESSPLACE = cfgAccountPay.BUSINESSPLACE;
                    //    tmpAP.TAX_CODE = cfgAccountPay.TAX_CODE;
                    //    temp.accountPay.Add(tmpAP);

                    //}//ACCOUNTPAYABLE loop
                    var cfgAccountPay = getAccountPayable();
                    BAPIACAP09 tmpAP = new BAPIACAP09();
                    tmpAP.VENDOR_NO = cfgAccountPay.VENDOR_NO;
                    tmpAP.REF_KEY_1 = cfgAccountPay.REF_KEY_1;
                    tmpAP.PMNTTRMS = cfgAccountPay.PMNTTRMS;
                    tmpAP.ITEM_TEXT = itemFirst.TripNo + "/" + itemFirst.MatItemName + "/" + itemFirst.VesselName + "/VAT";
                    tmpAP.BUSINESSPLACE = cfgAccountPay.BUSINESSPLACE;
                    tmpAP.TAX_CODE = cfgAccountPay.TAX_CODE;
                    temp.accountPay.Add(tmpAP);
                    //ACCOUNTPAYABLE


                    temp.accountTax = new List<BAPIACTX09>();
                    string sTaxCodeVat = "", sGLAcc = "";
                    var qryGLTax = dataDetail.GLAccountVATList.Where(x => x.Type == "VAT").Select(v => v.TaxCodeVat).ToList();
                    var qryGL = dataDetail.GLAccountVATList.Where(x => x.Type == "VAT").Select(v => v.GLAccount).ToList();
                    sTaxCodeVat = qryGLTax[0].ToString();
                    sGLAcc = qryGL[0].ToString();
                    foreach (var e in dataDetail.CustomVat_Calc)
                    {
                        BAPIACTX09 tmp = new BAPIACTX09();
                        tmp.GL_ACCOUNT = sGLAcc;
                        tmp.TAX_CODE = sTaxCodeVat;
                        temp.accountTax.Add(tmp);
                    }//ACCOUNTTAX

                    /***  ACCOUNTGL  ***/

                    temp.accountGL = new List<BAPIACGL09>();
                    string sGLAccMIRO = "";
                    //var qryAGL = null ;                    
                    foreach (var glAcc in dataDetail.GLAccountVATList)
                    {
                        if (glAcc.Type.ToString() == "IMPORT_DUTY")
                        {
                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {

                                var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "IMPORT_DUTY").Select(v => v.GLAccount).ToList();
                                sGLAccMIRO = qryAGL[0].ToString();

                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }
                        if (glAcc.Type.ToString() == "EXCISE_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "EXCISE_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {
                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }
                        if (glAcc.Type.ToString() == "MUNICIPAL_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "MUNICIPAL_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {
                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }
                        if (glAcc.Type.ToString() == "DEPOSIT_OF_IMPORT_DUTY")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_IMPORT_DUTY").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {
                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_EXCISE_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_EXCISE_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {
                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_MUNICIPAL_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_MUNICIPAL_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemAGL in dataDetail.CustomVat_Calc)
                            {
                                BAPIACGL09 tmpAGL = new BAPIACGL09();
                                tmpAGL.GL_ACCOUNT = sGLAccMIRO;
                                tmpAGL.ALLOC_NMBR = itemAGL.TripNo;
                                temp.accountGL.Add(tmpAGL);
                            }//ACCOUNTGL
                        }
                    }/***  ACCOUNTGL  ***/


                    /***  CURRENCYAMOUNT ***/
                    temp.currencyAm = new List<BAPIACCR09>();
                    string sCurrencyAM = "";
                    var qryCMACurrency = dataDetail.GLAccountVATList.Where(x => x.Type == "VAT").Select(v => v.Currency).ToList();
                    sCurrencyAM = qryCMACurrency[0].ToString();
                    //var qryCMAdIduty = dataDetail.GLAccountVATList.Where(x => x.Type == "")
                    foreach (var glAcc in dataDetail.GLAccountVATList)
                    {
                        if (glAcc.Type.ToString() == "VAT")
                        {
                            foreach (var itemCMA in dataDetail.CustomVat_Calc)
                            {

                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(itemCMA.CorrectVAT);
                                tmpCMA.AMT_BASE = Decimal.Parse(itemCMA.TaxBase);

                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT

                        }

                        #region "Import duty"
                        
                        if (glAcc.Type.ToString() == "IMPORT_DUTY")
                        {
                            foreach (var itemImp in dataDetail.CustomVat_Calc)
                            {
                                Boolean pono = String.IsNullOrEmpty(itemImp.PoNo);
                                Boolean ipdt = (Double.Parse(itemImp.ImportDuty) > 0);
                                if (!(ipdt && !pono))
                                {
                                    BAPIACCR09 tmpCMA = new BAPIACCR09();
                                    tmpCMA.CURRENCY = sCurrencyAM;
                                    tmpCMA.AMT_DOCCUR = Decimal.Parse(itemImp.ImportDuty);
                                    tmpCMA.AMT_BASE = 0;
                                    temp.currencyAm.Add(tmpCMA);
                                    vatgo = true;
                                }
                                else
                                {
                                   // if (tempMIRO.headerdata.Count() == 0)
                                   // {
                                        var itemF = dataDetail.CustomVat_Calc.FirstOrDefault();
                                        ZHEADERDATA tmpHeaderD = new ZHEADERDATA();
                                        tmpHeaderD.INVOICE_IND = cfgHeaderData.INVOICE_IND;
                                        tmpHeaderD.DOC_TYPE = cfgHeaderData.DOC_TYPE;
                                        tmpHeaderD.DOC_DATE = DateTime.ParseExact(dataDetail.sDocumentDate, format, provider).ToString("yyyy-MM-dd");
                                        tmpHeaderD.PSTNG_DATE = DateTime.ParseExact(dataDetail.sPostingDate, format, provider).ToString("yyyy-MM-dd");
                                        tmpHeaderD.REF_DOC_NO = itemF.TripNo;
                                        tmpHeaderD.COMP_CODE = itemF.CompanyCode;
                                        tmpHeaderD.DIFF_INV = cfgHeaderData.DIFF_INV;
                                        tmpHeaderD.CURRENCY = cfgHeaderData.CURRENCY;
                                        tmpHeaderD.EXCH_RATE = decimal.Parse(dataDetail.sROE);
                                        tmpHeaderD.GROSS_AMOUNT = decimal.Parse(dataDetail.sTotalImportDuty);
                                        tmpHeaderD.HEADER_TXT = "VAT Crude/" + itemF.TripNo + "/" + itemF.MatItemName.Substring(0, 4);
                                        tmpHeaderD.BUSINESS_PLACE = cfgHeaderData.BUSINESSPLACE;
                                        tmpHeaderD.ALLOC_NMBR = itemF.TripNo;
                                        tmpHeaderD.PMNTTRMS = cfgHeaderData.PMNTTRMS;
                                        tempMIRO.headerdata.Add(tmpHeaderD);
                                    //}

                                    ZITEMDATE tmpData = new ZITEMDATE();
                                    tmpData.PO_NUMBER = itemImp.PoNo;
                                    tmpData.PO_ITEM = itemImp.MatItemNo;
                                    tmpData.TAX_CODE = cfgdItemData.TAX_CODE;
                                    tmpData.ITEM_AMOUNT = decimal.Parse(itemImp.ImportDuty);
                                    totalmiro += tmpData.ITEM_AMOUNT;
                                    tmpData.QUANTITY = cfgdItemData.QUANTITY;
                                    tmpData.PO_UNIT = cfgdItemData.PO_UNIT;
                                    tmpData.ITEM_TEXT = itemImp.TripNo + "/" + itemImp.MatItemName + "/" + itemImp.VesselName + "/Import Duty";

                                    tempMIRO.itemData.Add(tmpData);
                                    mirogo = true;
                                }

                            }//CURRENCYAMOUNT
                           
                        }
                        #endregion
                        if (glAcc.Type.ToString() == "EXCISE_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "EXCISE_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemEXtax in dataDetail.CustomVat_Calc)
                            {
                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(itemEXtax.ExciseTax);
                                tmpCMA.AMT_BASE = 0;
                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT
                        }
                        if (glAcc.Type.ToString() == "MUNICIPAL_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "MUNICIPAL_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var tmpMUNtax in dataDetail.CustomVat_Calc)
                            {
                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(tmpMUNtax.MunicipalTax);
                                tmpCMA.AMT_BASE = 0;
                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT
                        }
                        if (glAcc.Type.ToString() == "DEPOSIT_OF_IMPORT_DUTY")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_IMPORT_DUTY").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemDOID in dataDetail.CustomVat_Calc)
                            {
                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(itemDOID.DepositOfImportDuty);
                                tmpCMA.AMT_BASE = 0;
                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_EXCISE_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_EXCISE_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemDOET in dataDetail.CustomVat_Calc)
                            {
                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(itemDOET.DepositOfExciseTax);
                                tmpCMA.AMT_BASE = 0;
                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_MUNICIPAL_TAX")
                        {
                            var qryAGL = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_MUNICIPAL_TAX").Select(v => v.GLAccount).ToList();
                            sGLAccMIRO = qryAGL[0].ToString();

                            foreach (var itemDOMT in dataDetail.CustomVat_Calc)
                            {
                                BAPIACCR09 tmpCMA = new BAPIACCR09();
                                tmpCMA.CURRENCY = sCurrencyAM;
                                tmpCMA.AMT_DOCCUR = Decimal.Parse(itemDOMT.DepositOfMunicipalTax);
                                tmpCMA.AMT_BASE = 0;
                                temp.currencyAm.Add(tmpCMA);
                            }//CURRENCYAMOUNT
                        }
                    }/*  CURRENCYAMOUNT */


                   

                    BAPIACCR09 tmpCMATotal = new BAPIACCR09();
                    //tmpCMATotal.CURRENCY = sCurrencyAM;
                    tmpCMATotal.DISC_BASE = Decimal.Parse(dataDetail.sTotalAll) - totalmiro;
                  
                    temp.currencyAm.Add(tmpCMATotal);

                    /***  EXTENSION2 ***/

                    temp.extension2 = new List<BAPIPAREX>();
                    string valuepart1 = "";

                    //var qryCMAdIduty = dataDetail.GLAccountVATList.Where(x => x.Type == "")
                    foreach (var glAcc in dataDetail.GLAccountVATList)
                    {


                        if (glAcc.Type.ToString() == "VAT")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "VAT").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/VAT";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2

                        }

                        if (glAcc.Type.ToString() == "IMPORT_DUTY")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "IMPORT_DUTY").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Import Duty";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2

                        }

                        if (glAcc.Type.ToString() == "EXCISE_TAX")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "EXCISE_TAX").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Excise Tax";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2
                        }
                        if (glAcc.Type.ToString() == "MUNICIPAL_TAX")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "MUNICIPAL_TAX").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Municipal Tax";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2
                        }
                        if (glAcc.Type.ToString() == "DEPOSIT_OF_IMPORT_DUTY")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_IMPORT_DUTY").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Deposit of Import Duty";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_EXCISE_TAX")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_EXCISE_TAX").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Deposit of Excise Tax";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2
                        }

                        if (glAcc.Type.ToString() == "DEPOSIT_OF_MUNICIPAL_TAX")
                        {
                            var qryValuepart1 = dataDetail.GLAccountVATList.Where(x => x.Type == "DEPOSIT_OF_MUNICIPAL_TAX").Select(v => v.ValuePart1).ToList();
                            valuepart1 = qryValuepart1[0].ToString();

                            foreach (var itemExt2 in dataDetail.CustomVat_Calc)
                            {

                                BAPIPAREX tmpCMA = new BAPIPAREX();
                                tmpCMA.VALUEPART1 = valuepart1;
                                tmpCMA.VALUEPART2 = itemExt2.TripNo + "/" + itemExt2.MatItemName + "/" + itemExt2.VesselName + "/Deposit of Municipal Tax";
                                temp.extension2.Add(tmpCMA);
                            }//EXTENSION2
                        }
                    }/*** EXTENSION2 ***/

                   
                            if(vatgo)
                            {
                                ConfigManagement configManagement = new ConfigManagement();
                                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                                JavaScriptSerializer js = new JavaScriptSerializer();
                                String content = js.Serialize(temp);
                                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason


                                VatCreateServiceConnectorlmpl VatService = new VatCreateServiceConnectorlmpl();
                                string conf = js.Serialize(jsonConfig.SERVICEVAT1);
                                DownstreamResponse<string> res = VatService.connect(conf, content);
                            }
                            if(mirogo)
                            {
                                ConfigManagement configManagement = new ConfigManagement();
                                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                                JavaScriptSerializer js = new JavaScriptSerializer();
                                String contentMiro = js.Serialize(tempMIRO);
                                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                                var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason


                                VatMIROInvoiceServiceConnectorImpl VatServiceMiro = new VatMIROInvoiceServiceConnectorImpl();
                                String confmiro = js.Serialize(jsonConfig.SERVICEVATmiro);
                                DownstreamResponse<string> resMiro = VatServiceMiro.connect(confmiro, contentMiro);
                           }
                       
                        
                             

                    /////downstream
                    //String contentMiro = js.Serialize(tempMIRO);
                    //String confmiro = js.Serialize(jsonConfig.SERVICEVATmiro);
                    //VatMIROInvoiceServiceConnectorImpl VatServiceMiro = new VatMIROInvoiceServiceConnectorImpl();
                    //DownstreamResponse<string> resMiro = VatServiceMiro.connect(confmiro, contentMiro);



                }
                #endregion



            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICustomVatToSapState # :: Exception >>> " + ex);
                log.Error("CPAICustomVatToSapState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public DOCUMENTHEADER getDocumentHeader()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_VAT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigVat dataList = (GlobalConfigVat)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigVat));
            return dataList.DOCUMENTHEADER;
        }

        public ACCOUNTPAYABLE getAccountPayable()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_VAT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigVat dataList = (GlobalConfigVat)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigVat));

            return dataList.ACCOUNTPAYABLE;
        }

        public HEADERDATA getHeaderData()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_VAT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigVat dataList = (GlobalConfigVat)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigVat));

            return dataList.HEADERDATA;
        }
        public ITEMDATA getItemData()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_VAT");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigVat dataList = (GlobalConfigVat)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigVat));

            return dataList.ITEMDATA;
        }
        [Serializable]
        public class GlobalConfigVat
        {
            public DOCUMENTHEADER DOCUMENTHEADER { get; set; }
            public ACCOUNTPAYABLE ACCOUNTPAYABLE { get; set; }
            public HEADERDATA HEADERDATA { get; set; }
            public ITEMDATA ITEMDATA { get; set; }
        }
        public class DOCUMENTHEADER
        {
            public string OBJ_TYPE { get; set; }
            public string OBJ_KEY { get; set; }
            public string BUS_ACT { get; set; }
            public string USERNAME { get; set; }
            public string DOC_TYPE { get; set; }
            public string REF_DOC_NO { get; set; }
        }

        public class ACCOUNTPAYABLE
        {
            public string VENDOR_NO { get; set; }
            public string REF_KEY_1 { get; set; }
            public string PMNTTRMS { get; set; }
            public string BUSINESSPLACE { get; set; }
            public string TAX_CODE { get; set; }

        }

        public class HEADERDATA
        {
            public string INVOICE_IND { get; set; }
            public string DOC_TYPE { get; set; }
            public string DIFF_INV { get; set; }
            public string CURRENCY { get; set; }
            public string BUSINESSPLACE { get; set; }
            public string PMNTTRMS { get; set; }

        }

        public class ITEMDATA
        {
            public string TAX_CODE { get; set; }
            public decimal QUANTITY { get; set; }
            public string PO_UNIT { get; set; }

        }
        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }

        public class ConfigServiceModel
        {
            public ConfigModel SERVICEVAT1 { get; set; }
            public ConfigModel SERVICEVATmiro { get; set; }
        }

    }
}
