﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Flow.F10000051
{
    public class CPAIVerifyGetBtnHedgingTicketRequireInputState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVerifyGetBtnHedgingTicketRequireInputState #  ");
            stateModel.BusinessModel.currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            try
            {                
                if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Channel)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_CHANNEL_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.System)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_SYSTEM_RESP_CODE;
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.User)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                /*else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.Type)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_TYPE_RESP_CODE;*/
                else if (string.IsNullOrEmpty(etxValue.GetValue(CPAIConstantUtil.TransactionId)))
                    currentCode = CPAIConstantRespCodeUtil.INVALID_TRANSACTION_ID_RESP_CODE;                  
                else
                {
                    ExtendValue extStatus = new ExtendValue();
                    UserGroupDAL service = new UserGroupDAL();
                    string user = etxValue.GetValue(CPAIConstantUtil.User);
                    string system = etxValue.GetValue(CPAIConstantUtil.System);
                    CPAI_USER_GROUP r = service.findByUserAndSystem(user, system);
                    if (r != null && r.USG_USER_GROUP != null)
                    {
                        extStatus.value = r.USG_USER_GROUP;
                        etxValue.SetValue(CPAIConstantUtil.UserGroup, extStatus);

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    }
                    else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_USER_GROUP_RESP_CODE;
                    }
                }
                etxValue.SetValue(CPAIConstantUtil.ip_server, NetWorkIPAddress.getIpAddress());
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVerifyGetBtnHedgingTicketRequireInputState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIVerifyGetBtnHedgingTicketRequireInputState # :: Exception >>> " + ex);
                log.Error("CPAIVerifyGetBtnHedgingTicketRequireInputState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}
