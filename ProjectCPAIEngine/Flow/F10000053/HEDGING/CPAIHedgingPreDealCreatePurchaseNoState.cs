﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using com.pttict.engine.model;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.DAL.DALMaster;
using com.pttict.engine.utility;

namespace ProjectCPAIEngine.Flow.F10000053
{
    public class CPAIHedgingPreDealCreatePurchaseNoState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIHedgingPreDealCreatePurchaseNoState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string last_seq = MasterData.GetSequences("HG_PDEL_SEQ");
                string docNo = "PDEL-" + DateTime.Now.ToString("yyMM") + "-" + last_seq;

                ExtendValue extDocNumber = new ExtendValue();
                extDocNumber.value = docNo;
                //doc no.
                etxValue.Add(CPAIConstantUtil.DocNo, extDocNumber);
                etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.DocNo, extDocNumber);
                //create by
                ExtendValue extUser = new ExtendValue();
                extUser.value = etxValue.GetValue(CPAIConstantUtil.User);
                etxValue.Add(CPAIConstantUtil.CreateBy, extUser);


                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIHedgingPreDealCreatePurchaseNoState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAIHedgingPreDealCreatePurchaseNoState # :: Exception >>> " + ex);
                log.Error("CPAIHedgingPreDealCreatePurchaseNoState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}