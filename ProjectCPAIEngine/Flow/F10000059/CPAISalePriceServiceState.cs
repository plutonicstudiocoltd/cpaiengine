﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using static ProjectCPAIEngine.Model.SalesPriceCreateModel;
using System.Globalization;


namespace ProjectCPAIEngine.Flow.F10000059
{
    public class CPAISalePriceServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }


        public void doAction(StateModel stateModel)
        {


            log.Info("# Start State CPAISalePriceServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            CultureInfo provider = new CultureInfo("en-US");
            string format = "dd/MM/yyyy";

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try
            {
                if (status == "CREATE")
                {
                    var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                    string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                    if (item != null)
                    {

                        SaleOrderSalePriceModel dataDetail = JSonConvertUtil.jsonToModel<SaleOrderSalePriceModel>(item);
                        string doctype = "";
                        if (dataDetail != null)
                        {
                            if (dataDetail.SaleOrder != null)
                            {
                                if (dataDetail.SaleOrder.mORDER_HEADER_IN != null)
                                {
                                    doctype = dataDetail.SaleOrder.mORDER_HEADER_IN.FirstOrDefault().DOC_TYPE;
                                }
                            }
                        }

                        var cfgHeader = getHeader();
                        List<SalesPriceCreateModel> temp = new List<SalesPriceCreateModel>();

                        //Boolean noSaleDoc = String.IsNullOrEmpty(dataDetail.SaleOrder.SALESDOCUMENT_IN);
                        //Boolean noflag = String.IsNullOrEmpty(dataDetail.SaleOrder.Flag);
                        //if (noSaleDoc && noflag) //Create Sale price befor create docment sale order (only create)
                        //if (noflag) //Create Sale price befor create docment sale order (create,change)
                        //{
                        for (int num = 0; num < dataDetail.SalePrice.Count; num++)
                        {
                            SalesPriceCreateModel t = new SalesPriceCreateModel();
                            t.DATAB = dataDetail.SalePrice[num].DATAB;// "2017-06-01";
                            t.DATBI = dataDetail.SalePrice[num].DATBI;// "2017-06-01";
                            t.KBETR = dataDetail.SalePrice[num].KBETR; //"1860.5502";//Unit Price
                            t.KMEIN = dataDetail.SalePrice[num].KMEIN; //"BBL";//Sales Unit
                            t.KONWA = dataDetail.SalePrice[num].KONWA; //"TH4";//Currency
                            t.KOTABNR = cfgHeader.HEADER.KOTABNR; //"901"; //Global Config
                            t.KPEIN = cfgHeader.HEADER.KPEIN; //1;//Global Config
                            t.KRECH = cfgHeader.HEADER.KRECH; //"";//Global Config
                            t.KSCHL = dataDetail.SalePrice[num].KSCHL; //"ZCR1"; //Provisional Price ,Actual ZCR3
                            t.KUNNR = dataDetail.SalePrice[num].KUNNR; //"0000000013";//Customer Code
                            t.MATNR = dataDetail.SalePrice[num].MATNR; //"YMB";//Material
                            t.VKORG = dataDetail.SalePrice[num].VKORG; //"1100";//Company Code
                            t.VTWEG = dataDetail.SalePrice[num].VTWEG; //"31";//DC
                            t.WERKS = dataDetail.SalePrice[num].WERKS; //"1200";//Plant
                            t.ZZVSTEL = dataDetail.SalePrice[num].ZZVSTEL; //"12SV";//Shipping Point

                            temp.Add(t);
                        }


                        //temp.DATAB = dataDetail.SalePrice.DATAB;// "2017-06-01";
                        //temp.DATBI = dataDetail.SalePrice.DATBI;// "2017-06-01";
                        //temp.KBETR = dataDetail.SalePrice.KBETR; //"1860.5502";//Unit Price
                        //temp.KMEIN = dataDetail.SalePrice.KMEIN; //"BBL";//Sales Unit
                        //temp.KONWA = dataDetail.SalePrice.KONWA; //"TH4";//Currency
                        //temp.KOTABNR = cfgHeader.HEADER.KOTABNR; //"901"; //Global Config
                        //temp.KPEIN = cfgHeader.HEADER.KPEIN; //1;//Global Config
                        //temp.KRECH = cfgHeader.HEADER.KRECH; //"";//Global Config
                        //temp.KSCHL = dataDetail.SalePrice.KSCHL; //"ZCR1"; //Provisional Price ,Actual ZCR3
                        //temp.KUNNR = dataDetail.SalePrice.KUNNR; //"0000000013";//Customer Code
                        //temp.MATNR = dataDetail.SalePrice.MATNR; //"YMB";//Material
                        //temp.VKORG = dataDetail.SalePrice.VKORG; //"1100";//Company Code
                        //temp.VTWEG = dataDetail.SalePrice.VTWEG; //"31";//DC
                        //temp.WERKS = dataDetail.SalePrice.WERKS; //"1200";//Plant
                        //temp.ZZVSTEL = dataDetail.SalePrice.ZZVSTEL; //"12SV";//Shipping Point

                        //if (doctype == "Z1P3")//Clearline
                        //{
                        //if(!string.IsNullOrEmpty(cfgHeader.CLEAR_LINE.DISTRIBUTION_CHANNEL))
                        //{
                        //    temp.VTWEG = cfgHeader.CLEAR_LINE.DISTRIBUTION_CHANNEL;
                        //}
                        //if (!string.IsNullOrEmpty(cfgHeader.CLEAR_LINE.SHIPPING_POINT))
                        //{
                        //    temp.ZZVSTEL = cfgHeader.CLEAR_LINE.SHIPPING_POINT;
                        //}
                        //}

                        ConfigManagement configManagement = new ConfigManagement();
                        String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        String content = js.Serialize(temp);
                        var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        var jsonConfig = javaScriptSerializer.Deserialize<ConfigServiceModel>(config); //convert jason

                        SalesPriceCreateServiceConnectorImpl SalesPriceCreateService = new SalesPriceCreateServiceConnectorImpl();
                        String conf = js.Serialize(jsonConfig.SALESPRICE);
                        DownstreamResponse<string> res = SalesPriceCreateService.connect(conf, content);

                        if (res.resultCode == "1")
                        {
                            //string Sap_logon = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("|") + 1, res.responseData.ToString().IndexOf("M") - res.responseData.ToString().IndexOf("|") - 1);
                            //string Sap_status_msg = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("M") + 1);
                            //string Sap_status_msg = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("M") + 1, res.responseData.ToString().IndexOf("R") - res.responseData.ToString().IndexOf("M") - 1);
                            //string result = res.responseData.ToString().Substring(res.responseData.ToString().IndexOf("R") + 1);

                            //ExtendValue extRowsSalesDoc = new ExtendValue
                            //{ value = result, encryptFlag = ConstantDBUtil.NO_FLAG };
                            //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.SALESDOCUMENT, extRowsSalesDoc);

                            //ExtendValue extRowsSap_status_msg = new ExtendValue
                            //{ value = Sap_status_msg, encryptFlag = ConstantDBUtil.NO_FLAG };
                            //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_status_msg, extRowsSap_status_msg);

                            //ExtendValue extRowsSap_logon = new ExtendValue
                            //{ value = Sap_logon, encryptFlag = ConstantDBUtil.NO_FLAG };
                            //etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Sap_logon, extRowsSap_logon);
                            //}

                            stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }
                        else
                        {
                            ExtendValue extRowsSap_StatusCode = new ExtendValue
                            { value = res.resultCode, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.Status, extRowsSap_StatusCode);

                            ExtendValue extRowsSap_StatusDesc = new ExtendValue
                            { value = res.resultDesc, encryptFlag = ConstantDBUtil.NO_FLAG };
                            etxValue.SetValue(ConstantUtil.RESP + CPAIConstantUtil.DataDetail, extRowsSap_StatusDesc);

                            stateModel.BusinessModel.currentCode = currentCode;
                        }

                    }
                }
                else
                {
                    stateModel.BusinessModel.currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    respCodeManagement.setCurrentCodeMapping(stateModel);
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAISalePriceServiceState # :: Exception >>> " + ex);
                log.Error("CPAISalePriceServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        public GlobalConfigSalesPrice getHeader()
        {
            var strJSON = "{ { \"Name\":\"\",\"Code\":\"\"} }";

            string JsonD = MasterData.GetJsonMasterSetting("CIP_SALESPRICE");
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            GlobalConfigSalesPrice dataList = (GlobalConfigSalesPrice)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(GlobalConfigSalesPrice));
            return dataList;
        }

        [Serializable]
        public class GlobalConfigSalesPrice
        {
            public HEADER HEADER { get; set; }
            public CLEAR_LINE_CONFIG CLEAR_LINE { get; set; }

        }

        public class HEADER
        {
            public string KOTABNR { get; set; }
            public decimal KPEIN { get; set; }
            public string KRECH { get; set; }
        }

        public class CLEAR_LINE_CONFIG
        {
            public string DISTRIBUTION_CHANNEL { get; set; }
            public string SHIPPING_POINT { get; set; }
            public string DIVISION { get; set; }
            public string DATE_TYPE { get; set; }
            public string ITM_NUMBER { get; set; }

        }

        public class ConfigModel
        {
            public string sap_url { get; set; }
            public string sap_user { get; set; }
            public string sap_pass { get; set; }
            public string connect_time_out { get; set; }
        }

        public class ConfigServiceModel
        {
            public ConfigModel SALESPRICE { get; set; }
        }

    }
}