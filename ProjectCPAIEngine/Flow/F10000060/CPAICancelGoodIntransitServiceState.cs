﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using com.pttict.engine.dal.Utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using ProjectCPAIEngine.DAL.DALTce;
using com.pttict.engine.dal.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.ServiceProvider;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.Service;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;

namespace ProjectCPAIEngine.Flow.F10000060
{
    public class CPAICancelGoodIntransitServiceState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        public Dictionary<string, string> counter_txn { get; set; }

        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICancelGoodIntransitServiceState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (item != null)
                {
                    ConfigManagement configManagement = new ConfigManagement();
                    String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);

                    CreatePOGoodIntransit tempData = new CreatePOGoodIntransit();
                    tempData = JSonConvertUtil.jsonToModel<CreatePOGoodIntransit>(item);
                    if (tempData != null)
                    {
                        IntansitCancelServiceConnectorImpl service = new IntansitCancelServiceConnectorImpl();
                        if (tempData.goodIntransitCancel != null)
                        {
                            JavaScriptSerializer js = new JavaScriptSerializer();
                            foreach (GoodIntransitCancel i in tempData.goodIntransitCancel)
                            {
                                service = new IntansitCancelServiceConnectorImpl();
                                if (i.intansitCancel != null)
                                {
                                    String detail_content = js.Serialize(i.intansitCancel);
                                    DownstreamResponse<string> result = service.connect(config, detail_content);
                                    if (tempData.ItemList.dDetail_Material != null)
                                    {
                                        foreach (MaterialViewModel m in tempData.ItemList.dDetail_Material)
                                        {

                                            if (m.PMA_ITEM_NO == i.item && m.PMA_TRIP_NO == i.tripNo)
                                            {
                                                if (result.resultCode == "1")
                                                {
                                                    //Return new mat doc
                                                    m.PMA_GIT_NO = "";// = result.resultDesc;
                                                    m.RET_STS = "S";
                                                }
                                                else
                                                {
                                                    m.RET_MSG = string.IsNullOrEmpty(result.resultDesc) ? "" : result.resultDesc;
                                                    m.RET_STS = "E";

                                                    var qHead = tempData.ItemList.dDetail_Header.Where(p => p.PHE_TRIP_NO.Equals(i.tripNo)).First();
                                                    if (qHead != null)
                                                    {
                                                        qHead.PHE_SAVE_STS = "E";
                                                        qHead.PHE_SAVE_MSG = m.RET_MSG;
                                                    }
                                                }
                                               
                                                //currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                                                continue;
                                            }
                                        }
                                    }

                                }
                            }

                            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                            string newXml = js.Serialize(tempData);
                            ExtendValue respTrans = new ExtendValue();
                            respTrans.value = newXml;
                            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;

                            etxValue.SetValue(CPAIConstantUtil.DataDetailInput, respTrans);

                            stateModel.BusinessModel.currentCode = currentCode;
                            respCodeManagement.setCurrentCodeMapping(stateModel);
                        }

                        
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("# Error CPAICancelGoodIntransitServiceState # :: Exception >>> " + ex);
                log.Error("CPAICancelGoodIntransitServiceState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
        
       
    }

}
