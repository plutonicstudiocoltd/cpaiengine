﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.dao;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000061 {
    public class CPAIListTxnVcoolState : BasicBean, StateFlowAction {
        private XmlParser xmlParser = new XmlParser();
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel) {
            log.Info("# Start State CPAIListTxnVcoolState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            //string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try {
                   Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                List<CPAI_ACTION_FUNCTION> result;
                if (String.IsNullOrWhiteSpace(function_code)) {
                    //result = acfMan.findByUserGroupAndSystem(userGroup, system);
                    result = acfMan.findByUserGroupAndSystemDelegate(lstUserGroup, system);//set user group deligate by job 25072017
                } else {
                    //result = acfMan.findByUserGroupAndSystemAndFuncitonId(userGroup, system, function_code);
                    result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, function_code);//set user group deligate by job 25072017
                }

                List<Transaction> lstAllTx = new List<Transaction>();
                if (result.Count == 0) {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                } else if (result.Count == 1) {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                } else {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++) {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER)) {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null) {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    } else {
                        for (int i = 0; i < result.Count; i++) {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                            if (acf.ACF_USR_GROUP == "SCEP") break;
                            if (acf.ACF_USR_GROUP == "SCSC") break;
                        }
                        lstAllTx = lstAllTx.GroupBy(x => x.purchase_no).Select(g => g.OrderBy(x => x.function_id).First()).ToList();
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIListTxnVcoolState # :: Code >>> " + currentCode);
            } catch (Exception ex) {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIListTxnVcoolState # :: Exception >>> " + ex);
                log.Error("CPAIListTxnVcoolState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public List<Transaction> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction) {
            log.Info("# Start State CPAIListTxnVcoolState >> getTransaction #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            FunctionTransactionDAL ftxMan = new FunctionTransactionDAL();
            List<FunctionTransaction> result;
            List<Transaction> list = new List<Transaction>();

            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            string mobileFlag = etxValue.GetValue(CPAIConstantUtil.MobileFlag);
            string channel = etxValue.GetValue(CPAIConstantUtil.Channel);
            string type = "";

            string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup);

            List<string> lstStatus = new List<string>();
            List<string> lstStatusMobileCondition = new List<string>();
            if (string.IsNullOrWhiteSpace(status)) {

                if (mobileFlag.Equals(CPAIConstantUtil.YES_FLAG)) {
                    if (channel == "MOBILE") {
                        lstStatus.Add("ALL");
                        if (!string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB)) {
                            lstStatusMobileCondition = new List<string>(actionFunction.ACF_FUNCTION_STATUS_MB.Split('|'));
                        }
                    } else {
                        if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB)) {
                            lstStatus.Add("ALL");
                        } else {
                            status = actionFunction.ACF_FUNCTION_STATUS_MB;
                            lstStatus = new List<string>(status.Split('|'));
                        }
                    }

                } else {
                    if (channel == "MOBILE") {
                        lstStatus.Add("ALL");
                        if (!string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB)) {
                            lstStatusMobileCondition = new List<string>(actionFunction.ACF_FUNCTION_STATUS_MB.Split('|'));
                        }
                    } else {
                        if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS_MB)) {
                            lstStatus.Add("ALL");
                        } else {
                            status = actionFunction.ACF_FUNCTION_STATUS_MB;
                            lstStatus = new List<string>(status.Split('|'));
                        }
                    }
                    //if (string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS)) {
                    //    lstStatus.Add("ALL");
                    //} else {
                    //    status = actionFunction.ACF_FUNCTION_STATUS;
                    //    lstStatus = new List<string>(status.Split('|'));
                    //}
                }

            } else {
                if (channel == "MOBILE") {
                    lstStatus.Add("ALL");
                    if (!string.IsNullOrWhiteSpace(status)) {
                        //lstStatusMobileCondition = new List<string>(actionFunction.ACF_FUNCTION_STATUS_MB.Split('|'));
                        lstStatusMobileCondition = new List<string>(status.Split('|'));
                        if (lstStatusMobileCondition.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP) || lstStatusMobileCondition.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                            if (lstStatusMobileCondition.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                                type = "SCVP";
                                lstStatusMobileCondition.Add(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE);
                            } else if (lstStatus.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                type = "CMVP";
                                lstStatusMobileCondition.Add(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE);
                            }
                            lstStatusMobileCondition.Add(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK);
                        }
                    }
                } else {
                    lstStatus = new List<string>(status.Split('|'));
                    if (lstStatus.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP) || lstStatus.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                        if (lstStatus.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                            type = "SCVP";
                            lstStatus.Add(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE);
                        } else if (lstStatus.Contains(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                            type = "CMVP";
                            lstStatus.Add(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE);
                        }
                        lstStatus.Add(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK);
                    }
                }
                
            }

            // create index to make search criteria
            var lstIndexValues = new List<string>();
            lstIndexValues.Add(actionFunction.ACF_SYSTEM);
            lstIndexValues.Add(actionFunction.ACF_TYPE);
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index3));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index4));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index5));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index6));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index7));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index8));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index9));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index10));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index11));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index12));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index13));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index14));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index15));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index16));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index17));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index18));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index19));
            lstIndexValues.Add(etxValue.GetValue(CPAIConstantUtil.Index20));

            if (!string.IsNullOrWhiteSpace(fromDate) && !string.IsNullOrWhiteSpace(toDate)) {
                result = ftxMan.findCPAIVcoolTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, fromDate, toDate);
            } else {
                result = ftxMan.findCPAIVcoolTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, lstIndexValues, null, null);
            }

            if (actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.VCOOL_FUNCTION_CODE) || actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.VCOOL_EXPERT_FUNCTION_CODE) || actionFunction.ACF_FK_FUNCTION.Equals(CPAIConstantUtil.VCOOL_EXPERT_SH_FUNCTION_CODE)) {
                for (int i = 0; i < result.Count; i++) {
                    FunctionTransaction ftx = result[i];
                    Transaction m = new Transaction();
                    m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                    m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                    m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                    m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                    m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                    m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                    m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                    m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                    m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                    m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                    m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                    //m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) || ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE) ? setStatusREVISE(etxValue.GetValue(CPAIConstantUtil.UserGroup), ftx.FUNCTION_TRANSACTION.FTX_INDEX4, ftx.FUNCTION_TRANSACTION.FTX_INDEX8) : ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    if ( (channel == CPAIConstantUtil.MOBILE && ftx.VCO_DATA_STATUS == CPAIConstantUtil.STATUS_TERMINATED)) {
                        m.status = ftx.VCO_DATA_STATUS;
                    } else {// MOBILE
                        m.status = ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)
                        || ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)
                             || ftx.FUNCTION_TRANSACTION.FTX_INDEX4.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)
                        ? setStatusREVISE(etxValue.GetValue(CPAIConstantUtil.UserGroup), ftx.FUNCTION_TRANSACTION.FTX_INDEX4, ftx.FUNCTION_TRANSACTION.FTX_INDEX8, channel) : ftx.FUNCTION_TRANSACTION.FTX_INDEX4;
                    }
                    m.date_purchase = ftx.FUNCTION_TRANSACTION.FTX_INDEX6;
                    m.purchase_no = ftx.FUNCTION_TRANSACTION.FTX_INDEX8;
                    m.product_name = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                    m.origin = ftx.FUNCTION_TRANSACTION.FTX_INDEX11;
                    m.incoterm = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                    m.formula_p = String.IsNullOrEmpty(ftx.FUNCTION_TRANSACTION.FTX_INDEX14) ? "" : String.Format("{0} $/bbl", ftx.FUNCTION_TRANSACTION.FTX_INDEX14);
                    m.loading_period = ftx.LOADING_PERIOD_FROM + " to " + ftx.LOADING_PERIOD_TO;
                    m.discharging_period = ftx.DISCHARGING_PERIOD_FROM + " to " + ftx.DISCHARGING_PERIODE_TO;
                    m.volume_kbbl_max = ftx.VOLUMN_KBBL_MAX;
                    m.volume_kt_max = ftx.VOLUMN_KT_MAX;
                    m.supplier = ftx.SUPPLIER_NAME;
                    m.final_price = ftx.FINAL_PRICE;
                    m.lp_result = ftx.LP_RESULT != null ? ShareFn.EncodeString(ftx.LP_RESULT) : "";
                    m.user_group = etxValue.GetValue(CPAIConstantUtil.UserGroup);
                    m.user_group_delegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate);
                    m.tpc_month = ftx.TPC_MONTH;
                    m.tpc_year = ftx.TPC_YEAR;
                    m.sc_status = ftx.SC_STATUS;
                    m.tn_status = ftx.TN_STATUS;
                    m.cm_status = ftx.CM_STATUS;
                    m.vco_data_status = ftx.VCO_DATA_STATUS;
                    m.vco_revise_scep_status = ftx.REVISE_SCEP_STATUS;
                    m.vco_revise_scsc_status = ftx.REVISE_SCSC_STATUS;
                    m.scsc_agree_flag = ftx.SCSC_AGREE_FLAG;

                    m.revise_dischange_period = ftx.REVISE_DISCHANGE_PERIOD_FROM + " to " + ftx.REVISE_DISCHANGE_PERIOD_TO;

                    m.purchase_result = ftx.PURCHASE_RESULT;
                    //using (EntityCPAIEngine context = new EntityCPAIEngine())
                    //{
                    //    var getVCool = context.VCO_DATA.SingleOrDefault(a => a.VCDA_ROW_ID == m.transaction_id);
                    //    if(getVCool != null)
                    //    {
                    //        var newDate = getVCool.VCDA_DATE_PURCHASE;
                    //        if(newDate != null)
                    //        {
                    //            m.created_date = newDate.Value.ToString("dd/MM/yyyy");
                    //        }                            
                    //    }                        
                    //}
                    if (channel == CPAIConstantUtil.MOBILE) {
                        if (lstStatusMobileCondition.Contains(m.status)) {
                            using (var context = new EntityCPAIEngine()) {
                                var query = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == ftx.FUNCTION_TRANSACTION.FTX_INDEX8).ToList().FirstOrDefault();
                                if (query != null) {
                                    m.quantity_kbbl_max = query.VCDA_QUANTITY_KBBL_MAX;
                                    m.benchmark_price = query.VCDA_BENCHMARK_PRICE;
                                    m.premium_maximum = query.VCDA_PREMIUM_MAXIMUM;
                                    m.requested_name = query.VCDA_CREATED_BY;
                                }
                            }
                            list.Add(m);
                        }
                        //if (m.status.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP) || m.status.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))
                        //{
                        //    m.status = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK;
                        //}

                    } else {
                        list.Add(m);
                    }


                }

            }
            log.Info("# End State CPAIListTxnVcoolState >> getTransaction # :: Code >>> " + currentCode);
            return list;
        }

        public void setExtraXml(StateModel stateModel, List<Transaction> lstAllTx) {
            log.Info("# Start State CPAIListTxnVcoolState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            TransactionList li = new TransactionList();
            li.trans = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnVcoolState >> setExtraXml # :: Code >>> " + currentCode);
        }

        public string getReason(string row_id) {
            log.Info("# Start State CPAIListTxnVcoolState >> getReason #  ");
            string reason = "-";
            ExtendTransactionDAL etxMan = new ExtendTransactionDAL();
            var etxResult = etxMan.findByFtxRowIdAndKey(row_id, CPAIConstantUtil.Note);
            if (etxResult != null && etxResult.Count > 0) {
                reason = etxResult[0].ETX_VALUE;
            }

            log.Info("# End State CPAIListTxnVcoolState >> getReason # :: Code >>> " + currentCode);
            return reason;
        }

        private String encryptRSAData(StateModel stateModel, string data) {
            log.Info("# Start State CPAIListTxnVcoolState >> encryptRSAData #  ");
            AppUserDao appUserDao = new AppUserDaoImpl();
            SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
            APP_USER appUser = appUserDao.findByAppId(stateModel.EngineModel.appUser);
            if (appUser != null) {
                SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PUBLIC_KEY_SCN_KEY);
                if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0) {
                    String pubKey = scn.SCN_VALUE;
                    String cipherText = data.Trim();
                    try {
                        String encryptMsg = RSAHelper.EncryptText(cipherText, pubKey);
                        log.Info("# End State CPAIListTxnVcoolState >> encryptRSAData # :: Code >>> " + currentCode);
                        return encryptMsg;
                    } catch (Exception ex) {
                        currentCode = CPAIConstantRespCodeUtil.CANNOT_ENCRYPT_MESSAGE_RESP_CODE;
                        var tem = MessageExceptionUtil.GetaAllMessages(ex);
                        log.Error("# Error :: Exception >>>  " + tem);
                        log.Error("# Error CPAIListTxnVcoolState >> encryptRSAData # :: Exception >>> " + currentCode + " : " + ex.Message);
                        throw new Exception();
                    }
                } else {
                    //can not get priKey
                    currentCode = ConstantRespCodeUtil.CAN_NOT_GET_KEY_ENGINE_RESP_CODE[0];
                    log.Error("# Error CPAIListTxnVcoolState >> encryptRSAData # :: Exception >>> " + currentCode);
                    throw new Exception();
                }
            } else {
                currentCode = ConstantRespCodeUtil.NOT_FOUND_APP_USER_ENGINE_RESP_CODE[0];
                log.Error("# Error CPAIListTxnVcoolState >> encryptRSAData # :: Exception >>> " + currentCode);
                throw new Exception();
            }
        }

        public string setStatusREVISE(string userGroup, string status, string purchase_no, string channel) {
            string Status = status;
            var MOBILE_TEXT = "MOBILE";
            if (userGroup=="CMCS_SH") {
                userGroup = "CMVP";
            }
            using (var context = new EntityCPAIEngine()) {
                var query = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == purchase_no).ToList().FirstOrDefault();

                if (!String.IsNullOrEmpty(query.VCDA_SC_STATUS) && userGroup == "SCVP") {
                    if (query.VCDA_SC_STATUS.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                        Status = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                    }
                    if (channel.ToString() == CPAIConstantUtil.MOBILE) {
                        var SCVP_MOBILE_Condition = new List<string>(
                            new string[] {
                                CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK,
                                 CPAIConstantUtil.STATUS_APPROVED_BY_CMVP
                            }
                        );
                        if (SCVP_MOBILE_Condition.Contains(Status)) {
                            Status = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                        }
                    }
                } else if (userGroup == "CMVP") {
                    if (!String.IsNullOrEmpty(query.VCDA_CM_STATUS) && !(query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.VCOOL_ENDING_CMCS_FLOW) ||
                        query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) ||
                        query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))) {
                        Status = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE;
                    }
                    if (channel.ToString() == CPAIConstantUtil.MOBILE) {
                        var CMVP_MOBILE_Condition = new List<string>(
                            new string[] {
                                CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK,
                                 CPAIConstantUtil.STATUS_APPROVED_BY_SCVP
                            }
                        );
                        if (CMVP_MOBILE_Condition.Contains(Status)) {
                            Status = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
                        }
                    }
                } else if (userGroup == "SCEP" || userGroup == "SCEP_SH" || userGroup == "SCSC" || userGroup == "SCSC_SH") {
                    //Status = query.VCDA_SC_STATUS;
                    if (!String.IsNullOrEmpty(query.VCDA_SC_STATUS) && query.VCDA_SC_STATUS.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                        if (userGroup == "SCEP" || userGroup == "SCEP_SH") {
                            Status = query.VCDA_REVISE_SCEP_STATUS;
                        } else if (userGroup == "SCSC" || userGroup == "SCSC_SH") {
                            Status = query.VCDA_REVISE_SCSC_STATUS;
                        }
                    }
                } else if (userGroup == "CMCS") {
                    if (!String.IsNullOrEmpty(query.VCDA_REVISE_SCSC_STATUS) && (query.VCDA_REVISE_SCSC_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA))) {
                        Status = query.VCDA_REVISE_SCSC_STATUS;
                    } else if (!String.IsNullOrEmpty(query.VCDA_CM_STATUS) && query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.VCOOL_BEGINNING_CMCS_FLOW)) {
                        Status = query.VCDA_CM_STATUS;
                    }
                }
            }
            return Status;
        }


        // Old one
        //public string setStatusREVISE(string userGroup, string status, string purchase_no) {
        //    string Status = status;
        //    using (var context = new EntityCPAIEngine()) {
        //        var query = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == purchase_no).ToList().FirstOrDefault();
        //        if (userGroup == "SCVP") {
        //            if (status.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
        //                Status = query.VCDA_SC_STATUS;
        //            }
        //        } else if (userGroup == "CMVP") {
        //            if (status.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
        //                Status = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
        //            }
        //        } else if (userGroup == "SCEP" || userGroup == "SCEP_SH" || userGroup == "SCSC" || userGroup == "SCSC_SH") {
        //            Status = query.VCDA_SC_STATUS;
        //            if (query.VCDA_SC_STATUS.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
        //                if (userGroup == "SCEP" || userGroup == "SCEP_SH") {
        //                    Status = query.VCDA_REVISE_SCEP_STATUS;
        //                } else if (userGroup == "SCSC" || userGroup == "SCSC_SH") {
        //                    Status = query.VCDA_REVISE_SCSC_STATUS;
        //                }
        //            }
        //        } else if (userGroup == "CMCS") {
        //            if (purchase_no == "VCO-1712-0004") {

        //            }
        //            if (status.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
        //                if (query.VCDA_REVISE_SCSC_STATUS != null && query.VCDA_REVISE_SCSC_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA)) {
        //                    Status = query.VCDA_REVISE_SCSC_STATUS;
        //                } else if (query.VCDA_CM_STATUS != null && query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT)) {
        //                    Status = query.VCDA_CM_STATUS;
        //                }
        //            } else if (status.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
        //                Status = query.VCDA_REVISE_SCSC_STATUS;
        //            } else {
        //                Status = query.VCDA_CM_STATUS;
        //            }



        //        }
        //    }
        //    return Status;
        //}

    }
}