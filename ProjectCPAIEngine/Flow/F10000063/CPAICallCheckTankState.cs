﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace ProjectCPAIEngine.Flow.F10000063
{
    public class CPAICallCheckTankState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {

            log.Info("# Start State CPAICallCheckTankState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                VcoRootObject dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);
                var json = new JavaScriptSerializer().Serialize(dataDetail);

                //call function F10000064 : APPROVE_4
                RequestCPAI req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000064;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.ParentTrxId = stateModel.EngineModel.ftxTransId;
                req.ParentActId = stateModel.EngineModel.parentActID;
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_4 });
                req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK });
                req.Req_parameters.P.Add(new P { K = "user", V = user });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                req.Req_parameters.P.Add(new P { K = "note", V = "Generate a Check Tank Transaction" });
                req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                req.Extra_xml = "";

                ResponseData resData = new ResponseData();
                RequestData reqData = new RequestData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                log.Debug(" resp function F10000064 >> " + resData.result_code);

                //call function F10000065 : APPROVE_6
                req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000065;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.ParentTrxId = stateModel.EngineModel.ftxTransId;
                req.ParentActId = stateModel.EngineModel.parentActID;
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_6 });
                req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_CHECK_IMPACT });
                req.Req_parameters.P.Add(new P { K = "user", V = user });
                req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                req.Req_parameters.P.Add(new P { K = "note", V = "Generate a Check Impact Transaction" });
                req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                req.Extra_xml = "";

                resData = new ResponseData();
                reqData = new RequestData();
                service = new ServiceProvider.ProjService();

                xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                log.Debug(" resp function F10000065 >> " + resData.result_code);

                currentCode = resData.result_code;

                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICallCheckTankState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICallCheckTankState # :: Exception >>> " + ex);
                log.Error("xxx::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}