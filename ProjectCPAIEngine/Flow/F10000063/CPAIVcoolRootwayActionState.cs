﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000063
{
    public class CPAIVcoolRootwayActionState : BasicBean, StateFlowAction
    {
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAIVcoolRootwayActionState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string function_code = stateModel.EngineModel.functionId;
                string update_flag = etxValue.GetValue(CPAIConstantUtil.Update_flag);
                string status = etxValue.GetValue(CPAIConstantUtil.Status);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string revAction = etxValue.GetValue(CPAIConstantUtil.ACTION_REVISE_STATUS);
                string revStatus = etxValue.GetValue(CPAIConstantUtil.REVISE_STATUS);
                
                if (CPAIConstantUtil.FLOW_CMCS.Contains(revStatus) || CPAIConstantUtil.FLOW_SCEP.Contains(revStatus)
                    || CPAIConstantUtil.FLOW_SCSC.Contains(revStatus)) // Edited zkavin.i on 2018 04 24
                {
                    action = revAction;
                    if (action.Contains(CPAIConstantUtil.ACTION_APPROVE))
                    {
                        if (update_flag.Equals("Y"))
                        {
                            currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_AND_UPDATE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_RESP_CODE;
                        }
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_REJECT) || action.Contains(CPAIConstantUtil.ACTION_CALLBACK))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_REJECT_RESP_CODE;
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_CANCEL))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_CANCEL_RESP_CODE;
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_DRAFT))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_DRAFT_RESP_CODE;
                    }  else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                    }
                }
                else
                {
                    if (action.Contains(CPAIConstantUtil.ACTION_APPROVE))
                    {
                        if (update_flag.Equals("Y"))
                        {
                            currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_AND_UPDATE_RESP_CODE;
                        }
                        else
                        {
                            currentCode = CPAIConstantRespCodeUtil.ACTION_APPROVE_RESP_CODE;
                        }
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_REJECT) || action.Contains(CPAIConstantUtil.ACTION_CALLBACK))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_REJECT_RESP_CODE;
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_CANCEL))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_CANCEL_RESP_CODE;
                    }
                    else if (action.Contains(CPAIConstantUtil.ACTION_DRAFT))
                    {
                        currentCode = CPAIConstantRespCodeUtil.ACTION_DRAFT_RESP_CODE;
                    }  else
                    {
                        currentCode = CPAIConstantRespCodeUtil.INVALID_ACTION_RESP_CODE;
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAIVcoolRootwayActionState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVcoolRootwayActionState # :: Exception >>> " + ex);
                log.Error("CPAIVcoolRootwayActionState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }
    }
}