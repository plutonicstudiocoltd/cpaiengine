﻿using com.pttict.downstream.common.model;
using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.downstream;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using DSMail.model;
using DSMail.service;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000063 {
    public class CPAIVcoolSendMailApproveState : BasicBean, StateFlowAction {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();

        public void doAction(StateModel stateModel) {
            log.Info("# Start State CPAIVcoolSendMailApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.PrevStatus);
                string status = etxValue.GetValue(CPAIConstantUtil.Status);
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string revaction = etxValue.GetValue(CPAIConstantUtil.ACTION_REVISE_STATUS);
                string revstatus = etxValue.GetValue(CPAIConstantUtil.REVISE_STATUS);// the status in case of running revising during vp process
                string charterFor = etxValue.GetValue(CPAIConstantUtil.CharterFor);
                ActionMessageDAL amsMan = new ActionMessageDAL();

                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();

                string checkSendMail = etxValue.GetValue(CPAIConstantUtil.CheckSendMail_M05);
                string checkSendMailApprove_8 = etxValue.GetValue(CPAIConstantUtil.CheckSendMail_Approve8);
                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "");
                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "");
                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Not_In_UserGroup, "");
                if (checkSendMail == "T") {
                    if (action == "APPROVE_3" || revaction == "APPROVE_14") {
                        List<CPAI_ACTION_MESSAGE> newResults = new List<CPAI_ACTION_MESSAGE>();
                        newResults = amsMan.findUserGroupMail_M05("M_05", system, type, stateModel.EngineModel.functionId, prevStatus, checkSendMail); //Get Email Detail. 
                        //etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "M_05");
                        if (newResults.Count == 0) {
                            currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                            currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                        } else {
                            for (int i = 0; i < newResults.Count; i++) {
                                findUserAndSendMail(stateModel, newResults[i], system, action);
                            }
                        }
                    }
                }

                if (status.Equals(CPAIConstantUtil.STATUS_TERMINATED)) {
                    // cancel document.
                    results = amsMan.findUserGroupMail(action, system, type, "F10000063", prevStatus, charterFor);
                    etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, action);
                    using (var context = new EntityCPAIEngine()) {
                        string Tran = stateModel.EngineModel.ftxTransId;
                        string po_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);

                        DAL.DALVCOOL.VCO_DATA_DAL tDataDal = new DAL.DALVCOOL.VCO_DATA_DAL();
                        VCO_DATA tData = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == po_no).ToList().FirstOrDefault();
                        DAL.DALVCOOL.VCO_COMMENT_DAL tCommentDal = new DAL.DALVCOOL.VCO_COMMENT_DAL();
                        VCO_COMMENT tComment = tCommentDal.GetByDataID(tData.VCDA_ROW_ID);

                        UserGroupDAL userDal = new UserGroupDAL();
                        CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                        var canecledComment = "";

                        etxValue.SetValue(CPAIConstantUtil.CancelBy, r.USG_USER_GROUP.ToUpper());
                        switch (r.USG_USER_GROUP.ToUpper()) {
                            case "SCVP":
                                canecledComment = tComment.VCCO_SCVP;
                                break;
                            case "CMVP":
                                canecledComment = tComment.VCCO_CMVP;
                                break;
                            case "TNVP":
                                canecledComment = tComment.VCCO_TNVP;
                                break;
                            case "EVPC":
                                canecledComment = tComment.VCCO_EVPC;
                                break;
                            default:
                                canecledComment = "";
                                break;
                        }
                        etxValue.SetValue(CPAIConstantUtil.CanceledComment, canecledComment);
                    }
                } else if (action.Contains(CPAIConstantUtil.ACTION_CALLBACK) || revaction.Contains(CPAIConstantUtil.ACTION_CALLBACK)) {
                    var temp_results = amsMan.findUserGroupMail("CALLBACK", system, type, "F10000063", prevStatus, charterFor).FirstOrDefault();
                    etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "CALLBACK");
                    UserGroupDAL userDal = new UserGroupDAL();
                    CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));

                    temp_results.AMS_USR_GROUP = r.USG_USER_GROUP.ToUpper() + "_SH";
                    results.Add(temp_results);
                    etxValue.SetValue(CPAIConstantUtil.GroupUserHead, temp_results.AMS_USR_GROUP);
                } else if (revstatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)
                      || revstatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
                    // this case occur only in case of beginning revise process from vp process
                    using (var context = new EntityCPAIEngine()) {
                        string Tran = stateModel.EngineModel.ftxTransId;
                        string po_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);

                        DAL.DALVCOOL.VCO_DATA_DAL tDataDal = new DAL.DALVCOOL.VCO_DATA_DAL();
                        VCO_DATA tData = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == po_no).ToList().FirstOrDefault();
                        DAL.DALVCOOL.VCO_COMMENT_DAL tCommentDal = new DAL.DALVCOOL.VCO_COMMENT_DAL();
                        VCO_COMMENT tComment = tCommentDal.GetByDataID(tData.VCDA_ROW_ID);

                        if (revstatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) { // SCVP
                            List<CPAI_ACTION_MESSAGE> temp = new List<CPAI_ACTION_MESSAGE>();
                            results = amsMan.findUserGroupMail("REJECT_5", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "REJECT_5");


                            if (tComment.VCCO_SCEP_FLAG == "W" && tComment.VCCO_SCSC_FLAG != "W") {
                                temp = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "SCEP").ToList();
                                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "SCEP");
                            } else if (tComment.VCCO_SCSC_FLAG == "W" && tComment.VCCO_SCEP_FLAG != "W") {
                                temp = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "SCSC").ToList();
                                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "SCSC");
                            } else {
                                temp = results.Where(x => x.AMS_USR_GROUP.ToUpper() != "CMCS").ToList();
                                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Not_In_UserGroup, "CMCS");
                            }
                            results = temp;
                        } else {// CMVP
                            List<CPAI_ACTION_MESSAGE> temp = new List<CPAI_ACTION_MESSAGE>();
                            results = amsMan.findUserGroupMail("REJECT_5", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "REJECT_5");
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "CMCS").ToList();
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "CMCS");
                        }
                    }
                } else if (revstatus.Equals(CPAIConstantUtil.VCOOL_ENDING_CMCS_FLOW)
                    || revstatus.Equals(CPAIConstantUtil.VCOOL_ENDING_SCEP_FLOW)
               || revstatus.Equals(CPAIConstantUtil.VCOOL_ENDING_SCSC_FLOW)) {//APPROVE_8 case
                    // In case of finishing revise process and come back to VP process. So the next process is going to be APPROVE_8 again.

                    if (revstatus.Equals(CPAIConstantUtil.VCOOL_ENDING_CMCS_FLOW)) {//Back to CMPV
                        results = amsMan.findUserGroupMail("APPROVE_8", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_8");
                        results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "CMVP").ToList();
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "CMVP");
                    } else { //Back to SCVP but need to check that all revise processes are completed.
                        using (var context = new EntityCPAIEngine()) {
                            string Tran = stateModel.EngineModel.ftxTransId;
                            string po_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);

                            DAL.DALVCOOL.VCO_DATA_DAL tDataDal = new DAL.DALVCOOL.VCO_DATA_DAL();
                            VCO_DATA tData = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == po_no).ToList().FirstOrDefault();
                            DAL.DALVCOOL.VCO_COMMENT_DAL tCommentDal = new DAL.DALVCOOL.VCO_COMMENT_DAL();
                            VCO_COMMENT tComment = tCommentDal.GetByDataID(tData.VCDA_ROW_ID);

                            if ((tComment.VCCO_SCSC_FLAG != "W") &&
                                      (tComment.VCCO_SCEP_FLAG != "W")) {
                                results = amsMan.findUserGroupMail("APPROVE_8", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.
                                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_8");
                                results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "SCVP").ToList();
                                etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "SCVP");
                            }
                        }
                    }

                } else if (CPAIConstantUtil.FLOW_CMCS.Contains(revstatus) || CPAIConstantUtil.FLOW_SCEP.Contains(revstatus)
                    || CPAIConstantUtil.FLOW_SCSC.Contains(revstatus)) { // in revising process except the beginning and ending.

                    if (revaction == "APPROVE_15") {
                        //results = amsMan.findUserGroupMail("APPROVE_5", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        results = amsMan.findUserGroupMail("APPROVE_15", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_15");
                    } else if (revaction == "APPROVE_14") {
                        results = amsMan.findUserGroupMail("APPROVE_3", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_3");
                    } else if (revaction == "APPROVE_17") {
                        results = amsMan.findUserGroupMail("APPROVE_17", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_17");
                    } else if (revaction == "APPROVE_19") {
                        results = amsMan.findUserGroupMail("APPROVE_2", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "APPROVE_2");
                    } else if (revaction == "REJECT_6") {
                        results = amsMan.findUserGroupMail("REJECT_6", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "REJECT_6");
                    } else if (revaction == "REJECT_5") {
                        results = amsMan.findUserGroupMail("REJECT", system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, "REJECT");
                    } else {
                        results = amsMan.findUserGroupMail(revaction, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor);
                        etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, revaction);
                    }
                } else if (action == "APPROVE_8" && status == CPAIConstantUtil.STATUS_WAITING_VP_CHECK_IMPACT) {// coming TNVP Process
                    results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.
                    etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, action);
                    //results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "TNVP").ToList();
                } else if (action == "APPROVE_8" && status != CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) {
                    // There is no sending email in this case.
                    // The cause of action equal APPROVE_8 that send email will consist of coming first time in the vp process and
                    // come back from revising process.
                } else {
                    results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.
                    etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_Action, action);

                    //if (action == "APPROVE_8") {
                    //    using (EntitiesEngine entity = new EntitiesEngine()) {
                    //        var detailVcool = entity.FUNCTION_TRANSACTION.SingleOrDefault(a => a.FTX_TRANS_ID == checkSendMailApprove_8);
                    //        if (detailVcool != null) {
                    //            List<CPAI_ACTION_MESSAGE> newResults = new List<CPAI_ACTION_MESSAGE>();
                    //            using (var context = new EntityCPAIEngine()) {
                    //                var purNum = detailVcool.FTX_INDEX8;
                    //                var chkStatus = context.VCO_DATA.SingleOrDefault(a => a.VCDA_PURCHASE_NO == purNum);
                    //                if (chkStatus != null) {
                    //                    foreach (var item in results) {
                    //                        if (item.AMS_USR_GROUP == "CMVP") {
                    //                            if (chkStatus.VCDA_CM_STATUS != "CMVP APPROVED" && chkStatus.VCDA_CM_STATUS != "CMVP REJECT") {
                    //                                newResults.Add(item);
                    //                            }
                    //                        } else {
                    //                            newResults.Add(item);
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //            results = new List<CPAI_ACTION_MESSAGE>();
                    //            results = newResults;
                    //        }
                    //    }
                    //}
                }



                //remarked zkavin.i 2018 04 23 casuse all result shold be added in all case above.
                //results = amsMan.findUserGroupMail(action, system, type, stateModel.EngineModel.functionId, prevStatus, charterFor); //Get Email Detail.

                if (results.Count == 0) {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_MAIL_RESP_CODE;
                } else {
                    UserGroupDAL ugDAL = new UserGroupDAL();
                    List<CPAI_USER_GROUP> ug = ugDAL.getUserList(etxValue.GetValue(CPAIConstantUtil.User), ConstantPrm.SYSTEM.VCOOL);
                    if (action == "REJECT_3" || revstatus == "REJECT_3") { // zkavin.i add revstatus == "REJECT_3"
                        if (ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("SCVP")).Any()) {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "SCSC_SH").ToList();
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "SCSC_SH");
                        } else if (ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("CMVP")).Any()) {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "CMCS").ToList();
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "CMCS");
                        } else if (ug.Where(x => x.USG_USER_GROUP.ToUpper().Contains("TNVP")).Any()) {
                            results = results.Where(x => x.AMS_USR_GROUP.ToUpper() == "TNPB").ToList();
                            etxValue.SetValue(CPAIConstantUtil.Vcool_Noti_UserGroup, "TNPB");
                        }
                    }
                    for (int i = 0; i < results.Count; i++) {
                        findUserAndSendMail(stateModel, results[i], system, action);
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolSendMailApproveState # :: Code >>> " + currentCode);
            } catch (Exception ex) {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAIVcoolSendMailApproveState # :: Exception >>> " + ex);
                log.Error("CPAIVcoolSendMailApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system, string action) {
            List<USERS> userResult = new List<USERS>();
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER)) //ไม่ได้ส่งให้ใครเป็นพิเศษ
            {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                userResult = userGMan.findUserMailByGroupSystem(userGroup, system);

                if (userResult.Count == 0) {
                    log.Info("user not found");
                } else {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            } else {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1) {
                    log.Info("user not found");
                } else {
                    prepareSendMail(stateModel, actionMessage, userResult);
                }
            }

        }

        public void prepareSendMail(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, List<USERS> userMail) {

            string jsonText = Regex.Replace(actionMessage.AMS_MAIL_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            MailDetail detail = JSonConvertUtil.jsonToModel<MailDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));

            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            if (userResult.Count == 0) {
                log.Info("login user not found");
            }

            if (subject.Contains("#pn")) {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body.Contains("#pn")) {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            //
            if (subject.Contains("#user")) {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user")) {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user")) {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user")) {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }

            if (body.Contains("#to_group_user")) {
                body = body.Replace("#to_group_user", etxValue.GetValue(CPAIConstantUtil.GroupUserHead));
            }

            if (body.Contains($"#{CPAIConstantUtil.CanceledComment}")) {
                body = body.Replace($"#{CPAIConstantUtil.CanceledComment}", etxValue.GetValue(CPAIConstantUtil.CanceledComment));
            }


            //get create by
            //string tem = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            //List<USERS> create_by = userMan.findByLogin(tem);
            //string create_by_v = "";
            //if (create_by != null && create_by[0] != null && create_by[0].USR_FIRST_NAME_EN != null)
            //{
            //    //string last_name = create_by[0].USR_LAST_NAME_EN != null ? create_by[0].USR_LAST_NAME_EN  : "";
            //    //create_by_v = string.Format("{0} {1}", create_by[0].USR_FIRST_NAME_EN, last_name);
            //    create_by_v = create_by[0].USR_FIRST_NAME_EN;
            //}
            //if (body.Contains("#create_by"))
            //{
            //    body = body.Replace("#create_by", create_by_v);
            //}

            //vcool
            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000063)) {
                subject = mms.getSubjectF10000063(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000063(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000064)) {
                subject = mms.getSubjectF10000064(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000064(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000065)) {
                subject = mms.getSubjectF10000065(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000065(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            //
            if (body.Contains("#token")) {
                //for loop send mail
                string tran_id = "";
                using (var context = new EntityCPAIEngine()) {
                    string id = stateModel.EngineModel.ftxTransId;
                    DAL.DALVCOOL.VCO_DATA_DAL DAL = new DAL.DALVCOOL.VCO_DATA_DAL();
                    var query_data = DAL.GetByID(id);
                    if (query_data == null) {
                        string po_no = etxValue.GetValue(CPAIConstantUtil.PurchaseNumber);
                        var query_check = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == po_no).ToList().FirstOrDefault();
                        tran_id = query_check.VCDA_ROW_ID;
                    }
                }
                string temp_body = body;
                foreach (USERS um in userMail) {
                    temp_body = body;
                    //UsersDAL u = new UsersDAL();
                    //List <USERS> usr =  u.findByRowId(ug.USG_FK_USERS);
                    //string userapprove = usr[0].USR_LOGIN;
                    //gen token 
                    string token = Guid.NewGuid().ToString("N");
                    //insert token
                    DateTime now = DateTime.Now;
                    ApproveTokenDAL service = new ApproveTokenDAL();
                    CPAI_APPROVE_TOKEN at = new CPAI_APPROVE_TOKEN();
                    at.TOK_ROW_ID = token;
                    at.TOK_TOKEN = token;
                    at.TOK_USED_TYPE = etxValue.GetValue(CPAIConstantUtil.Type);
                    at.TOK_FK_USER = um.USR_ROW_ID;
                    at.TOK_TRASACTION_ID = stateModel.EngineModel.ftxTransId;
                    at.TOK_USER_SYSTEM = etxValue.GetValue(CPAIConstantUtil.System);
                    at.TOK_STATUS = "ACTIVE";
                    at.TOK_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_CREATED_DATE = now;
                    at.TOK_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    at.TOK_UPDATED_DATE = now;
                    //at.USERS = ug.USERS;
                    service.Save(at);
                    string out_body = temp_body.Replace("#token", token);

                    List<String> lstMailTo = new List<String>();
                    lstMailTo.Add(um.USR_EMAIL);
                    sendMail(stateModel, lstMailTo, subject, out_body);
                }
            } else {
                //list mail to
                List<String> lstMailTo = new List<String>();
                for (int i = 0; i < userMail.Count; i++) {
                    lstMailTo.Add(userMail[i].USR_EMAIL);
                }
                sendMail(stateModel, lstMailTo, subject, body);
            }

        }

        public void sendMail(StateModel stateModel, List<String> lstMailTo, string subject, string body) {
            string mailTo = "";
            for (var i = 0; i < lstMailTo.Count; i++) {
                if (mailTo.Length > 0) {
                    mailTo += ";";
                }
                mailTo += lstMailTo[i];
            }

            //call downstream send mail
            DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel> connector = new DownstreamController<MailServiceConnectorImpl, ResponseSendMailModel>(new MailServiceConnectorImpl(), new ResponseSendMailModel());
            //find by config and content from DB
            ConfigManagement configManagement = new ConfigManagement();
            String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
            String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

            var contentObj = JSonConvertUtil.jsonToModel<MailContent>(content);
            contentObj.mail_to = mailTo;
            contentObj.mail_subject = subject;
            contentObj.mail_body = body;

            String finalContent = JSonConvertUtil.modelToJson(contentObj);

            //call downstream
            DownstreamResponse<ResponseSendMailModel> downResp = connector.invoke(stateModel, config, finalContent);
            stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            currentCode = downResp.getResultCode();
        }
    }
}