﻿using System;
using System.Collections.Generic;
using com.pttict.engine;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.DAL.Entity;
using com.pttict.downstream.common.utilities;
using ProjectCPAIEngine.Flow.Model;
using DSMail.model;
using com.pttict.downstream.common.model;
using DSMail.service;
using com.pttict.engine.downstream;
using ProjectCPAIEngine.DAL.DALBunker;
using System.Text.RegularExpressions;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using DSFcm.service;
using DSFcm.model;
using com.pttict.engine.dal.Utility;
using System.Linq;
namespace ProjectCPAIEngine.Flow.F10000063 {
    public class CPAIVcoolSendNotiApproveState : BasicBean, StateFlowAction {
        string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
        string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

        private UsersDAL userMan = new UsersDAL();
        private UserGroupDAL userGMan = new UserGroupDAL();
        private Dictionary<string, ExtendValue> etxValue = null;
        private MailMappingService mms = new MailMappingService();
        public void doAction(StateModel stateModel) {
            log.Info("# Start State CPAISendNotiApproveState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();
            stateModel.BusinessModel.currentNameSpace = currentNameSpace;
            try {
                etxValue = stateModel.BusinessModel.etxValue;

                string action = etxValue.GetValue(CPAIConstantUtil.Vcool_Noti_Action);
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string type = etxValue.GetValue(CPAIConstantUtil.Type);

                string in_usergroup = etxValue.GetValue(CPAIConstantUtil.Vcool_Noti_UserGroup);
                string not_usergroup = etxValue.GetValue(CPAIConstantUtil.Vcool_Noti_Not_In_UserGroup);
                string checkSendMail = etxValue.GetValue(CPAIConstantUtil.CheckSendMail_M05);
                ActionMessageDAL amsMan = new ActionMessageDAL();
                List<CPAI_ACTION_MESSAGE> results = new List<CPAI_ACTION_MESSAGE>();

                 if (checkSendMail == "T" && (action == "APPROVE_3" || action == "APPROVE_14")) {
                    var newResults = amsMan.findUserGroupNoti("M_05", system, type, stateModel.EngineModel.functionId); //Get Email Detail. 
                    for (int i = 0; i < newResults.Count; i++) {
                        if (!string.IsNullOrEmpty(newResults[i].AMS_NOTIFICATION_DETAIL)) {
                            findUserAndSendNoti(stateModel, newResults[i], system);
                        }
                    }
                }

                if (action.Contains(CPAIConstantUtil.ACTION_CALLBACK)) { // CALL BACK Case
                    var temp_results = amsMan.findUserGroupNoti("CALLBACK", system, type, "F10000063").FirstOrDefault();

                    UserGroupDAL userDal = new UserGroupDAL();
                    CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));

                    temp_results.AMS_USR_GROUP = r.USG_USER_GROUP.ToUpper() + "_SH";
                    results.Add(temp_results);
                    etxValue.SetValue(CPAIConstantUtil.GroupUserHead, temp_results.AMS_USR_GROUP);

                    //results = amsMan.findUserGroupNoti(action, system, type, stateModel.EngineModel.functionId);
                } else {
                    results = amsMan.findUserGroupNoti(action, system, type, stateModel.EngineModel.functionId);
                    if (!String.IsNullOrEmpty(in_usergroup)) {
                        results = results.Where(f => f.AMS_USR_GROUP == in_usergroup).ToList();
                    }
                    if (!String.IsNullOrEmpty(not_usergroup)) {
                        results = results.Where(f => f.AMS_USR_GROUP != not_usergroup).ToList();
                    }
                }
               

                

                if (results.Count == 0) {
                    currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                } else {
                    for (int i = 0; i < results.Count; i++) {
                        if ( !string.IsNullOrEmpty( results[i].AMS_NOTIFICATION_DETAIL )) {
                            findUserAndSendNoti(stateModel, results[i], system);
                        }
                    }
                }

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);

                log.Info("# End State CPAISendNotiApproveState # :: Code >>> " + currentCode);
            } catch (Exception ex) {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);

                log.Info("# Error CPAISendNotiApproveState # :: Exception >>> " + ex);
                log.Error("CPAISendNotiApproveState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void findUserAndSendNoti(StateModel stateModel, CPAI_ACTION_MESSAGE actionMessage, string system) {
            log.Info("# Start State CPAISendNotiApproveState >> findUserAndSendNoti #  ");
            if (string.IsNullOrWhiteSpace(actionMessage.AMS_FK_USER)) {
                //not override user
                string userGroup = actionMessage.AMS_USR_GROUP;
                //List<USERS> userResult = userMan.findByUserGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroup(userGroup);
                //List<USERS> userResult = userGMan.findUserByGroupSystem(userGroup, system);
                List<USERS> userResult = userGMan.findUserNotiByGroupSystem(userGroup, system);

                if (userResult.Count == 0) {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    currentCode = CPAIConstantRespCodeUtil.NOT_FOUND_DATA_SEND_NOTI_RESP_CODE;
                    log.Info("user not found");
                } else {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++) {
                        if (userResult[i].USR_NOTI_ID != null && userResult[i].USR_OS != null) {
                            lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                        }
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            } else {
                //override user
                string usrRowId = actionMessage.AMS_FK_USER;
                List<USERS> userResult = userMan.findByRowId(usrRowId);
                if (userResult.Count != 1) {
                    // user not found
                    //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                    //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                    //throw new Exception("user not found");
                    log.Info("user not found");
                } else {
                    Dictionary<string, string> lstNotiTo = new Dictionary<string, string>();
                    for (int i = 0; i < userResult.Count; i++) {
                        lstNotiTo[userResult[i].USR_NOTI_ID] = userResult[i].USR_OS;
                    }
                    prepareSendNoti(stateModel, lstNotiTo, actionMessage);
                }
            }

            log.Info("# End State CPAISendNotiApproveState >> findUserAndSendNoti # :: Code >>> " + currentCode);
        }
        public void prepareSendNoti(StateModel stateModel, Dictionary<string, string> lstNotiTo, CPAI_ACTION_MESSAGE actionMessage) {
            log.Info("# Start State CPAISendNotiApproveState >> prepareSendNoti #  ");
            string jsonText = Regex.Replace(actionMessage.AMS_NOTIFICATION_DETAIL, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            NotiDetail detail = JSonConvertUtil.jsonToModel<NotiDetail>(jsonText);
            string subject = detail.subject;
            string body = detail.body;
            string action_data = detail.action_data;


            CPAI_USER_GROUP user_group = userGMan.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System)); //do action

            //get user
            List<USERS> userResult = userMan.findByLogin(etxValue.GetValue(CPAIConstantUtil.User));
            if (userResult != null && userResult.Count == 0) {
                //currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                //currentCode = CPAIConstantRespCodeUtil.INVALID_USER_RESP_CODE;
                //throw new Exception("cancel user not found");
                log.Info("invalid user");
            } else {
                log.Info("found user");
            }
            //
            if (subject != null && subject.Contains("#pn")) {
                subject = subject.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (body != null && body.Contains("#pn")) {
                body = body.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (action_data != null && action_data.Contains("#pn")) {
                action_data = action_data.Replace("#pn", etxValue.GetValue(CPAIConstantUtil.PurchaseNumber));
            }
            if (subject.Contains("#user")) {
                subject = subject.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            if (body.Contains("#user")) {
                body = body.Replace("#user", string.Format("{0} {1}", userResult[0].USR_FIRST_NAME_EN, userResult[0].USR_LAST_NAME_EN));
            }
            //
            if (subject.Contains("#group_user")) {
                subject = subject.Replace("#group_user", user_group.USG_USER_GROUP);
            }
            if (body.Contains("#group_user")) {
                body = body.Replace("#group_user", user_group.USG_USER_GROUP);
            }

            if (body.Contains("#to_group_user")) {
                body = body.Replace("#to_group_user", etxValue.GetValue(CPAIConstantUtil.GroupUserHead));
            }

            if (body.Contains($"#{CPAIConstantUtil.CanceledComment}")) {
                body = body.Replace($"#{CPAIConstantUtil.CanceledComment}", etxValue.GetValue(CPAIConstantUtil.CanceledComment));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000063)) {
                subject = mms.getSubjectF10000063(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000063(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000064)) {
                subject = mms.getSubjectF10000064(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000064(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            if (stateModel.EngineModel.functionId.ToUpper().Equals(CPAIConstantUtil.F10000065)) {
                subject = mms.getSubjectF10000065(subject, etxValue.GetValue(CPAIConstantUtil.DataDetail));
                body = mms.getBodyF10000065(body, etxValue.GetValue(CPAIConstantUtil.DataDetail));
            }

            currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
            foreach (var item in lstNotiTo) {
                log.Debug("sendNoti");
                var key = item.Key;
                var value = item.Value;
                sendNoti(stateModel, key, value, subject, body, action_data);
            }

            log.Info("# End State CPAISendNotiApproveState >> prepareSendNoti # :: Code >>> " + currentCode);
        }

        public void sendNoti(StateModel stateModel, string key, string value, string subject, string body, string action_data) {
            log.Info("# Start State CPAISendNotiApproveState >> sendNoti #  ");
            if (key != null && !String.IsNullOrEmpty(key)) {
                //call downstream send mail
                DownstreamController<FCMServiceConnectorImpl, string> connector = new DownstreamController<FCMServiceConnectorImpl, string>(new FCMServiceConnectorImpl(), "");
                //find by config and content from DB
                ConfigManagement configManagement = new ConfigManagement();
                String config = configManagement.getDownstreamConfig(stateModel.EngineModel.downstreamApiCode);
                String content = configManagement.getDownstreamContent(stateModel.EngineModel.downstreamApiCode);

                var configObj = JSonConvertUtil.jsonToModel<RequestFcmModel>(config);
                configObj.os = value;

                String finalConfig = JSonConvertUtil.modelToJson(configObj);

                var contentObj = JSonConvertUtil.jsonToModel<ContentFcmModel>(content);
                contentObj.deviceId = key;
                contentObj.body_i = body;
                contentObj.title_i = subject;
                contentObj.action_data_i = action_data;

                String finalContent = JSonConvertUtil.modelToJson(contentObj);

                //call downstream
                DownstreamResponse<string> downResp = connector.invoke(stateModel, finalConfig, finalContent);
                currentCode = downResp.getResultCode();
                stateModel.BusinessModel.currentNameSpace = downResp.getNameSpace();
            } else {
                //null noti id
                currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
                currentCode = CPAIConstantRespCodeUtil.INVALID_USER_NOTI_ID_RESP_CODE;
            }

            log.Info("# End State CPAISendNotiApproveState >> sendNoti # :: Code >>> " + currentCode);
        }

        public string getBodyF10000001(string body, string json) {
            log.Info("# Start State CPAISendNotiApproveState >> getBodyF10000001 #  ");
            BunkerPurchase dataDetail = JSonConvertUtil.jsonToModel<BunkerPurchase>(json);
            string supplier = "";
            string grade = "";
            string volume = "";
            string spec = "";
            string price = "";
            int count = 0;

            //string currency_symbol = dataDetail.currency_symbol != null ? " " + dataDetail.currency_symbol + "/ ton" : " $/ ton";
            string currency_symbol = dataDetail.currency_symbol != null ? " " + dataDetail.currency_symbol : "";
            foreach (Product pd in dataDetail.product) {
                if (count == 0) {
                    grade = pd.product_name;
                    volume = String.Format("{0:n}", double.Parse(pd.product_qty));
                    spec = pd.product_spec;
                } else {
                    grade = grade + CPAIConstantUtil.delimeter_and + pd.product_name;
                    volume = volume + CPAIConstantUtil.delimeter_and + String.Format("{0:n}", double.Parse(pd.product_qty));
                    spec = spec + CPAIConstantUtil.delimeter_and + pd.product_spec;
                }

                count++;
            }

            foreach (OffersItem of in dataDetail.offers_items) {
                if (of.final_flag.Equals("Y")) {
                    supplier = of.supplier;
                    price = of.final_price + " " + currency_symbol;
                    break;
                }
            }

            if (body.Contains("#reason")) {
                body = body.Replace("#reason", dataDetail.reason);
            }
            if (body.Contains("#vessel")) {
                string id = dataDetail.vessel;
                MT_VEHICLE vessel = VehicleDAL.GetVehicleById(id);
                body = body.Replace("#vessel", vessel.VEH_VEH_TEXT);
            }
            if (grade != null && body.Contains("#grade")) {
                body = body.Replace("#grade", grade);
            }
            if (supplier != null && body.Contains("#supplier")) {
                MT_VENDOR vendor = VendorDAL.GetVendorById(supplier);
                body = body.Replace("#supplier", vendor.VND_NAME1);
            }
            if (body.Contains("#volume")) {
                body = body.Replace("#volume", volume);
            }
            if (body.Contains("#price")) {
                body = body.Replace("#price", price);
            }
            if (body.Contains("#location")) {
                body = body.Replace("#location", dataDetail.supplying_location);
            }
            if (body.Contains("#delivery_date")) {
                //09/11/2016
                String date_from = dataDetail.delivery_date_range.date_from != null ? dataDetail.delivery_date_range.date_from : "";
                String date_to = dataDetail.delivery_date_range.date_to != null ? dataDetail.delivery_date_range.date_to : "";
                String date = "";
                ShareFn sf = new ShareFn();
                if (date_to.Equals("")) {
                    date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                    date = date_from;
                } else {
                    date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                    date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                    date = date_from + " to " + date_to;
                }
                body = body.Replace("#delivery_date", date);
            }
            if (body.Contains("#tc")) {
                body = body.Replace("#tc", dataDetail.trip_no);
            }

            log.Info("# End State CPAISendNotiApproveState >> getBodyF10000001 # :: Code >>> " + currentCode);
            return body;
        }

        public string getSubjectF10000006(string subject, string json) {
            log.Info("# Start State CPAISendNotiApproveState >> getSubjectF10000006 #  ");
            ChiRootObject dataDetail = JSonConvertUtil.jsonToModel<ChiRootObject>(json);

            if (subject.Contains("#load_month")) {
                subject = subject.Replace("#load_month", dataDetail.chi_evaluating.loading_month + " " + dataDetail.chi_evaluating.loading_year);
            }

            log.Info("# End State CPAISendNotiApproveState >> getSubjectF10000006 # :: Code >>> " + currentCode);
            return subject;
        }

        public string getBodyF10000006(string body, string json) {
            /*
                # load_month
                # reason
                # vessel
                # owner
                # broker

                # laycan
                # chi_detail
                # othercost
                # freight
            */
            log.Info("# Start State CPAISendNotiApproveState >> getBodyF10000006 #  ");
            ChiRootObject dataDetail = JSonConvertUtil.jsonToModel<ChiRootObject>(json);

            if (body.Contains("#load_month")) {
                body = body.Replace("#load_month", dataDetail.chi_evaluating.loading_month + " " + dataDetail.chi_evaluating.loading_year);
            }
            if (body.Contains("#laycan")) {

                String date_from = dataDetail.chi_evaluating.laycan_from != null ? dataDetail.chi_evaluating.laycan_from : "";
                String date_to = dataDetail.chi_evaluating.laycan_to != null ? dataDetail.chi_evaluating.laycan_to : "";
                String date = "";
                ShareFn sf = new ShareFn();
                if (date_to.Equals("")) {
                    date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                    date = date_from;
                } else {
                    date_from = sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_from, "dd-MMM-yyyy") : "";
                    date_to = sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") != null ? sf.ConvertDateFormatBackFormat(date_to, "dd-MMM-yyyy") : "";
                    date = date_from + " to " + date_to;
                }
                body = body.Replace("#laycan", date);
            }
            if (body.Contains("#othercost")) {
                body = body.Replace("#othercost", dataDetail.chi_proposed_for_approve.exten_cost);
            }
            if (body.Contains("#freight")) {
                body = body.Replace("#freight", dataDetail.chi_proposed_for_approve.est_freight);
            }
            if (body.Contains("#reason")) {
                body = body.Replace("#reason", dataDetail.chi_reason);
            }

            string vessel = string.Empty;
            string broker = string.Empty;
            string owner = string.Empty;
            string final_deal = string.Empty;
            foreach (ChiOffersItem of in dataDetail.chi_negotiation_summary.chi_offers_items) {
                if (of.final_flag.Equals("Y")) {
                    vessel = of.vessel;
                    broker = of.broker;
                    owner = of.owner;
                    final_deal = of.final_deal;
                    List<ChiRoundInfo> rinfo = new List<ChiRoundInfo>();
                    foreach (ChiRoundItem rt in of.chi_round_items) {
                        int no_before = 0;
                        int no = 0;
                        if (Int32.TryParse(rt.round_no, out no)) {
                            if (no > no_before) {
                                rinfo = rt.chi_round_info;
                                Int32.TryParse(rt.round_no, out no_before);
                            }
                        }
                    }
                    string chi_detail = "";
                    if (body.Contains("#chi_detail")) {
                        if (rinfo != null && rinfo.Count > 0) {
                            foreach (ChiRoundInfo r in rinfo) {
                                if (r.type.ToLower().Equals("min loaded")) {
                                    chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " KT <br>";
                                } else if (r.type.ToLower().Equals("dem")) {
                                    chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + " KUSD / Day <br>";
                                } else {
                                    chi_detail = chi_detail + r.type + " : " + String.Format("{0:n}", double.Parse(r.offer)) + "<br>";
                                }
                            }
                        } else {
                            chi_detail = "not found final deal";
                        }
                        body = body.Replace("#chi_detail", chi_detail);
                    }
                    break;
                }
            }
            if (body.Contains("#vessel")) {
                string id = vessel;
                MT_VEHICLE vessel_data = VehicleDAL.GetVehicleById(id);
                body = body.Replace("#vessel", vessel_data.VEH_VEH_TEXT);
            }
            if (body.Contains("#owner")) {
                body = body.Replace("#owner", owner);
            }
            if (body.Contains("#broker")) {
                MT_VENDOR vendor = VendorDAL.GetVendorById(broker);
                body = body.Replace("#broker", vendor.VND_NAME1);
            }

            log.Info("# End State CPAISendNotiApproveState >> getBodyF10000006 # :: Code >>> " + currentCode);
            return body;
        }
    }
}