﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCOOL;
using ProjectCPAIEngine.DAL.DALVCOOL;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
namespace ProjectCPAIEngine.Flow.F10000064 {
    public class CPAIVcoolExpertUpdateDataState : BasicBean, StateFlowAction {
        public void doAction(StateModel stateModel) {
            log.Info("# Start State CPAIVcoolExpertUpdateDataState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
                //set index
                
                string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);
                string currentStatus = etxValue.GetValue(CPAIConstantUtil.Status); // current status
                string userGroup ="";
                string prevStatus = etxValue.GetValue(CPAIConstantUtil.Status);
                string channel = etxValue.GetValue(CPAIConstantUtil.Channel);
                string currentAction = etxValue.GetValue(CPAIConstantUtil.CurrentAction); //next action
                var item = etxValue.GetValue(CPAIConstantUtil.DataDetailInput);
                if (prevStatus == null) {
                    prevStatus = "NEW";
                }
                etxValue.SetValue(CPAIConstantUtil.PrevStatus, new ExtendValue { value = prevStatus, encryptFlag = "N" });
                etxValue.SetValue(CPAIConstantUtil.CheckSendMail_Approve8, new ExtendValue { value = stateModel.EngineModel.ftxTransId, encryptFlag = "N" });

                VcoRootObject dataDetail = new VcoRootObject();
                FunctionTransactionDAL ftDal = new FunctionTransactionDAL();
                var currentStatusInPage = "";
                FUNCTION_TRANSACTION ft = ftDal.GetFnTranDetail(stateModel.EngineModel.ftxTransId);

                if (channel == CPAIConstantUtil.MOBILE) { // zkavin.i 05 04 2018 for Mobile.
                    var actionREJECT = "";
                    if (currentAction.Contains("REJECT")) {
                        actionREJECT = "&REJECT";
                    }

                    if (String.IsNullOrEmpty(userGroup)) {
                        UserGroupDAL userDal = new UserGroupDAL();
                        var r = userDal.findByUserAndSystems(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                        //if (r != null && r.USG_USER_GROUP != null) {
                        //    userGroup = r.USG_USER_GROUP;
                        //}
                        userGroup = r;
                    }

                    currentStatusInPage = getWorkFlowStatusRevise(ft.FTX_INDEX8, userGroup);
                    if (String.IsNullOrEmpty(currentStatusInPage)) {
                        currentStatusInPage = currentStatus;
                    }
                  
                    var noteMobile = etxValue.GetValue(CPAIConstantUtil.Note);
                    var dataDetailFromService = VCoolServiceModel.getVCoolData(stateModel.EngineModel.ftxTransId, etxValue.GetValue(CPAIConstantUtil.User));
                    Dictionary<string, object> inputData = new Dictionary<string, object>();

                    if (!String.IsNullOrEmpty( item )) {
                        var dataDetailFromMobile = ShareFn.DecodeString(item);
                        etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetailFromMobile, encryptFlag = "N" });
                        inputData = JSonConvertUtil.deserializeToDictionary(dataDetailFromMobile);
                    }
                   

                    item = dataDetailFromService;
                    dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);

                    VCoolServiceModel.setValueForMobile( ref dataDetail);

                    item = JSonConvertUtil.modelToJson(dataDetail);

                    var vcoMapping = new VcoMappingMobileData(); // Class to map json data from mobile to service data.
                    vcoMapping.MapingToVcoRootObject(ref dataDetail, userGroup, currentStatusInPage + actionREJECT
                        , noteMobile, inputData);
                    etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = JSonConvertUtil.modelToJson(dataDetail) , encryptFlag = "N" });
                }

                if (item != null) {

                    if (channel == CPAIConstantUtil.WEB) {// Web
                        dataDetail = JSonConvertUtil.jsonToModel<VcoRootObject>(item);
                        currentStatusInPage = dataDetail.requester_info.currentStatus;
                    }

                    DataHistoryDAL dthMan = new DataHistoryDAL();
                    string action = etxValue.GetValue(CPAIConstantUtil.Action); //current action
                    

                    #region add data history
                    //add data history
                    DateTime now = DateTime.Now;
                    CPAI_DATA_HISTORY dataHistory = new CPAI_DATA_HISTORY();
                    dataHistory.DTH_ROW_ID = Guid.NewGuid().ToString("N");
                    dataHistory.DTH_ACTION = etxValue.GetValue(CPAIConstantUtil.CurrentAction);
                    dataHistory.DTH_ACTION_DATE = now;
                    dataHistory.DTH_ACTION_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_NOTE = etxValue.GetValue(CPAIConstantUtil.Note);
                    dataHistory.DTH_JSON_DATA = etxValue.GetValue(CPAIConstantUtil.DataDetail);
                    dataHistory.DTH_TXN_REF = stateModel.EngineModel.ftxRowId;
                    dataHistory.DTH_REJECT_FLAG = "N";
                    dataHistory.DTH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_CREATED_DATE = now;
                    dataHistory.DTH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                    dataHistory.DTH_UPDATED_DATE = now;
                    dthMan.Save(dataHistory);
                    #endregion

                    #region Update DB
                    //update reason only in data_detail 
                    string note = etxValue.GetValue(CPAIConstantUtil.Note) != null ? etxValue.GetValue(CPAIConstantUtil.Note) : "-";
                    
                   
                    string ref_id = stateModel.EngineModel.ftxTransId;
                    if (ft != null && ft.FTX_PARENT_TRX_ID != null) {
                        ref_id = ft.FTX_PARENT_TRX_ID;
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        etxValue.SetValue(CPAIConstantUtil.PurchaseNumber, new ExtendValue { value = parent_ft.FTX_INDEX8, encryptFlag = "N" });
                    }

                    UserGroupDAL userDal = new UserGroupDAL();
                    CPAI_USER_GROUP r = userDal.findByUserAndSystem(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (r != null && r.USG_USER_GROUP != null) {
                        userGroup = r.USG_USER_GROUP;
                    }
                    CPAI_USER_GROUP dlg = userDal.findByUserAndSystemDelegate(etxValue.GetValue(CPAIConstantUtil.User), etxValue.GetValue(CPAIConstantUtil.System));
                    if (dlg != null && dlg.USG_USER_GROUP != null) {
                        userGroup = dlg.USG_USER_GROUP;
                    }

                    bool isRunDB = false;
                    bool callF63Flag = false;
                   
                    
                 
                    /*
                        action => currentAction
                        APPROVE_4 => APPROVE_4 = SAVE DRAFT
                        APPROVE_4 => APPROVE_5 = Submit (Agree/Disagree)
                        APPROVE_5 => APPROVE_4 = Rejected by SCSC Section Head
                        APPROVE_5 => APPROVE_8 = Approve (Agree/Disagree)
                        APPROVE_7 => APPROVE_4 = date is not in period
                        APPROVE_7 => APPROVE_8 = date is in period
                        APPROVE_8 => APPROVE_5 = Rejected by SCVP
                        APPROVE_8 => APPROVE_6 = Rejected by TNVP
                        APPROVE_8 => APPROVE_7 = Rejected by CMVP
                        APPROVE_8 => APPROVE_9 = APPROVED
                    */

                   

                    isRunDB = UpdateDetailDB_New(ref_id, nextStatus, currentStatusInPage, currentAction, etxValue, dataDetail, ref callF63Flag, ft);
                    if (((currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE))
                        || (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK))
                        || (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK) && !nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK))
                         || (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSED_ETA))
                        || (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_TANK))
                        || (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)))
                        //|| ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSED_ETA))
                        //|| ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_TANK))
                        //|| ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)))
                        && isRunDB) {
                        isRunDB = InsertDB_ETA_Date_History(ref_id, userGroup, etxValue, dataDetail);
                    }
                    if (callF63Flag) {
                        FUNCTION_TRANSACTION parent_ft = ftDal.GetFnTranDetail(ref_id);
                        var json = new JavaScriptSerializer().Serialize(dataDetail);
                        RequestCPAI req = new RequestCPAI();
                        req.Function_id = ConstantPrm.FUNCTION.F10000063;
                        req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                        req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                        req.Req_transaction_id = parent_ft.FTX_REQ_TRANS;
                        req.State_name = "CPAIVerifyRequireInputContinueState";
                        req.Req_parameters = new Req_parameters();
                        req.Req_parameters.P = new List<P>();
                        req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                        req.Req_parameters.P.Add(new P { K = "current_action", V = CPAIConstantUtil.ACTION_APPROVE_9 });
                        req.Req_parameters.P.Add(new P { K = "next_status", V = CPAIConstantUtil.STATUS_WAITING_CONFIRM_PRICE });
                        req.Req_parameters.P.Add(new P { K = "user", V = etxValue.GetValue(CPAIConstantUtil.User) });
                        req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.VCOOL });
                        req.Req_parameters.P.Add(new P { K = "type", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                        req.Req_parameters.P.Add(new P { K = "note", V = "Go to Waiting Confirm Price Status" });
                        req.Req_parameters.P.Add(new P { K = "attach_items", V = "" });
                        req.Req_parameters.P.Add(new P { K = "data_detail_input", V = json });
                        req.Extra_xml = "";

                        ResponseData resData = new ResponseData();
                        RequestData reqData = new RequestData();
                        ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                        var xml = ShareFunction.XMLSerialize(req);
                        reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                        resData = service.CallService(reqData);
                        log.Debug(" resp function F10000063 >> " + resData.result_code);
                    }
                    #endregion

                    #region INSERT JSON
                    if (isRunDB) {
                        //First time when this transaction is initialized.
                        if (action.Equals("") && currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_4)) {
                            string date_purchase = dataDetail.crude_info.purchase_date != null ? dataDetail.crude_info.purchase_date : "";
                            string products = dataDetail.crude_info.crude_name != null ? dataDetail.crude_info.crude_name : "";
                            string origin = dataDetail.crude_info.origin != null ? dataDetail.crude_info.origin : "";
                            string tpc_plan_month = dataDetail.crude_info.tpc_plan_month != null ? dataDetail.crude_info.tpc_plan_month : "";
                            string tpc_plan_year = dataDetail.crude_info.tpc_plan_year != null ? dataDetail.crude_info.tpc_plan_year : "";
                            string loading_from = dataDetail.crude_info.loading_date_from != null ? dataDetail.crude_info.loading_date_from : "";
                            string loading_to = dataDetail.crude_info.loading_date_to != null ? dataDetail.crude_info.loading_date_to : "";
                            string incoterm = dataDetail.crude_info.incoterm != null ? dataDetail.crude_info.incoterm : "";
                            string formula_p = dataDetail.crude_info.formula_price != null ? dataDetail.crude_info.formula_price : "";
                            string discharging_from = dataDetail.crude_info.discharging_date_from != null ? dataDetail.crude_info.discharging_date_from : "";
                            string discharging_to = dataDetail.crude_info.discharging_date_to != null ? dataDetail.crude_info.discharging_date_to : "";
                            string supplier = dataDetail.crude_info.supplier != null ? dataDetail.crude_info.supplier : "";
                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);

                            //set data
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.DatePurchase, new ExtendValue { value = date_purchase, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Products, new ExtendValue { value = products, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Origin, new ExtendValue { value = origin, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_month, new ExtendValue { value = tpc_plan_month, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Tpc_plan_year, new ExtendValue { value = tpc_plan_year, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_from, new ExtendValue { value = loading_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Loading_to, new ExtendValue { value = loading_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.incoterm, new ExtendValue { value = incoterm, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Formula_p, new ExtendValue { value = formula_p, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_from, new ExtendValue { value = discharging_from, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Discharging_to, new ExtendValue { value = discharging_to, encryptFlag = "N" });
                            etxValue.SetValue(CPAIConstantUtil.Supplier, new ExtendValue { value = supplier, encryptFlag = "N" });
                        }

                        currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];
                    } else {
                        currentCode = CPAIConstantRespCodeUtil.CAN_NOT_INSERT_DB_RESP_CODE;
                    }
                    #endregion
                } else {
                    currentCode = CPAIConstantRespCodeUtil.INVALID_DATA_DETAIL_RESP_CODE;
                }
                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAIVcoolExpertUpdateDataState # :: Code >>> " + currentCode);
            } catch (Exception ex) {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error CPAIVcoolExpertUpdateDataState >> UpdateDB # :: Exception >>>> ", ex);
                log.Error("CPAIVcoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public string getWorkFlowStatusRevise(string purchaseNo, string userGroup) {// The condition will be same as method in VCoolServiceModel.cs
            VCO_DATA_DAL dal = new VCO_DATA_DAL();
            string PO_NO = purchaseNo;
            string Status = "";
            using (var context = new EntityCPAIEngine()) {
                VCO_DATA query = context.VCO_DATA.Where(x => x.VCDA_PURCHASE_NO == PO_NO).ToList().FirstOrDefault();
                if (query != null) {
                   
                    if (!string.IsNullOrEmpty(query.VCDA_SC_STATUS) && query.VCDA_SC_STATUS.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                        if (userGroup.Equals("SCEP") || userGroup.Equals("SCEP_SH")) {
                            Status = query.VCDA_REVISE_SCEP_STATUS;
                        } else if (userGroup.Equals("SCSC") || userGroup.Equals("SCSC_SH")) {
                            Status = query.VCDA_REVISE_SCSC_STATUS;
                        }
                        else if (userGroup.Equals("CMCS")) {
                            if (!string.IsNullOrEmpty(query.VCDA_REVISE_SCSC_STATUS)) {
                                if (query.VCDA_REVISE_SCSC_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA)) {
                                    Status = query.VCDA_REVISE_SCSC_STATUS;
                                    return Status;
                                }
                            }
                        }
                    }

                    if (userGroup.Equals("CMVP")) {
                        if (!string.IsNullOrEmpty(query.VCDA_CM_STATUS) &&
                        query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT)
                        ) {
                            Status = query.VCDA_CM_STATUS;
                        } else if (!string.IsNullOrEmpty(query.VCDA_CM_STATUS) && query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                            Status = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                        } else {
                            //Status = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
                            Status = CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK;
                        }
                    } else if (userGroup.Equals("CMCS")) {
                        if (!string.IsNullOrEmpty(query.VCDA_CM_STATUS)) {
                            if (query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT) ||
                                query.VCDA_CM_STATUS.Equals(CPAIConstantUtil.STATUS_REVISED_PURCHASE_COMMENT)) {
                                Status = query.VCDA_CM_STATUS;
                            } 
                        }
                    }
                    
                }
            }

            return Status;
        }

        public bool UpdateDetailDB(string TransID, string nextStatus, string currentStatus, string currentAction, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail, ref bool flag) {
            log.Info("# Start State CPAIVcoolExpertUpdateDataState >> UpdateDetailDB #  ");

            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            string tUser = etxValue.GetValue(CPAIConstantUtil.User);

            bool isSuccess = false;
            try {
                using (var context = new EntityCPAIEngine()) {
                    using (var dbContextTransaction = context.Database.BeginTransaction()) {
                        try {

                            #region Comment
                            VCO_COMMENT_DAL tCommentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT tComment = tCommentDal.GetByDataID(TransID);

                            VCO_DATA_DAL tDataDal = new VCO_DATA_DAL();
                            VCO_DATA tData = tDataDal.GetByID(TransID);


                            DateTime? discharging_from = null;
                            DateTime? discharging_to = null;

                            if (tData != null) {
                                discharging_from = tData.VCDA_DISCHARGING_DATE_FROM;
                                discharging_to = tData.VCDA_DISCHARGING_DATE_TO;
                            }

                            if (dataDetail != null) {
                                tData.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? tData.VCDA_DISCHARGING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                tData.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? tData.VCDA_DISCHARGING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");

                                tComment.VCCO_CMCS_REVISE = string.IsNullOrEmpty(dataDetail.comment.revise_cmvp) ? tComment.VCCO_CMCS_REVISE : dataDetail.comment.revise_cmvp;
                                tComment.VCCO_CMCS_NOTE = String.IsNullOrEmpty(dataDetail.comment.cmcs_note) ? tComment.VCCO_CMCS_NOTE : dataDetail.comment.cmcs_note;
                                tComment.VCCO_CMCS_REQUEST = String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? tComment.VCCO_CMCS_REQUEST : dataDetail.comment.cmcs_request; //CMCS Comment on request date.
                                tComment.VCCO_SCSC = String.IsNullOrEmpty(dataDetail.comment.scsc) ? tComment.VCCO_SCSC : dataDetail.comment.scsc;
                                if (currentAction != "CALLBACK_4" && currentAction != "CALLBACK_2") {
                                    tComment.VCCO_SCSC_AGREE_FLAG = dataDetail.comment.scsc_agree_flag.ToUpper().Equals(CPAIConstantUtil.VCOOL_SCSC_DISAGREE) ? "N" : "Y";
                                }
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA) || currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE))
                                    tComment.VCCO_CMCS_PROPOSE_DISCHARGE = dataDetail.comment.cmcs_propose_discharge;
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))
                                    tComment.VCCO_CMVP = dataDetail.comment.cmvp;
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP))
                                    tComment.VCCO_SCVP = dataDetail.comment.scvp;
                            }

                            if ((currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                                tData.VCDA_REASON_SCEP_SH = "";
                                tData.VCDA_REASON_SCSC_SH = "";
                                tData.VCDA_SCSC_SH_NOTE = "";
                                tComment.VCCO_SCEP_FLAG = string.IsNullOrEmpty(dataDetail.comment.scep_flag) ? "" : dataDetail.comment.scep_flag;
                                tComment.VCCO_SCSC_FLAG = string.IsNullOrEmpty(dataDetail.comment.scsc_flag) ? "" : dataDetail.comment.scsc_flag;
                                tComment.VCCO_SCVP_REVISE = string.IsNullOrEmpty(dataDetail.comment.revise_scvp) ? "" : dataDetail.comment.revise_scvp;
                            }
                            //------ ICE Edit -------//
                            else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISED_PURCHASE_COMMENT)) ||
                                (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT))) {
                                tData.VCDA_REASON_CMCS_SH = "";
                                tComment.VCCO_CMCS_NOTE = dataDetail.comment.cmcs_note;
                                tComment.VCCO_CMCS_PROPOSE_DISCHARGE = dataDetail.comment.cmcs_propose_discharge;
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_LP_RUN)) {
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;

                                tDataDal.Update(tData, context);
                                if (dataDetail != null && dataDetail.crude_compare != null) {
                                    VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
                                    data_compare_dal.Delete(TransID, context);
                                    int i = 0;
                                    foreach (var item in dataDetail.crude_compare.yield) {
                                        VCO_CRUDE_COMPARISON data_compare = new VCO_CRUDE_COMPARISON();
                                        data_compare.VCCP_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + i++;
                                        data_compare.VCCP_ORDER = item.order;
                                        data_compare.VCCP_YIELD = item.yield;
                                        //data_compare.VCCP_BASE_CRUDE 
                                        data_compare.VCCP_BASE_VALUE = item.base_value;
                                        data_compare.VCCP_COMPARE_CRUDE = item.compare_crude;
                                        data_compare.VCCP_COMPARE_VALUE = item.compare_value;
                                        //data_compare.VCCP_DELTA_CRUDE 
                                        data_compare.VCCP_DELTA_VALUE = item.delta_value;
                                        data_compare.VCCP_CREATED = dtNow;
                                        data_compare.VCCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_UPDATED = dtNow;
                                        data_compare.VCCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_FK_VCO_DATA = TransID;
                                        data_compare_dal.Save(data_compare, context);
                                    }

                                }
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_RUN_LP)) {
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;


                                if (dataDetail != null && dataDetail.crude_compare != null) {
                                    VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
                                    data_compare_dal.Delete(TransID, context);
                                    int i = 0;
                                    foreach (var item in dataDetail.crude_compare.yield) {
                                        VCO_CRUDE_COMPARISON data_compare = new VCO_CRUDE_COMPARISON();
                                        data_compare.VCCP_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + i++;
                                        data_compare.VCCP_ORDER = item.order;
                                        data_compare.VCCP_YIELD = item.yield;
                                        //data_compare.VCCP_BASE_CRUDE 
                                        data_compare.VCCP_BASE_VALUE = item.base_value;
                                        data_compare.VCCP_COMPARE_CRUDE = item.compare_crude;
                                        data_compare.VCCP_COMPARE_VALUE = item.compare_value;
                                        //data_compare.VCCP_DELTA_CRUDE 
                                        data_compare.VCCP_DELTA_VALUE = item.delta_value;
                                        data_compare.VCCP_CREATED = dtNow;
                                        data_compare.VCCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_UPDATED = dtNow;
                                        data_compare.VCCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_FK_VCO_DATA = TransID;
                                        data_compare_dal.Save(data_compare, context);
                                    }
                                }
                                tDataDal.Update(tData, context);
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_RUN_LP)) {
                                tComment.VCCO_SCEP_FLAG = "Y";
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;
                                if (!String.IsNullOrEmpty(tData.VCDA_PREMIUM) && !String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) {
                                    if (Convert.ToDecimal(tData.VCDA_PREMIUM) > Convert.ToDecimal(dataDetail.crude_info.premium_maximum)) {
                                        etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "T", encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "F", encryptFlag = "N" });
                                    }
                                }
                                tDataDal.Update(tData, context);
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY)) {

                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)) {

                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)) {
                                tComment.VCCO_SCSC_FLAG = "Y";
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
                                tComment.VCCO_CMVP = dataDetail.comment.cmvp;
                            }
                            //------End ICE Edit -------//
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                                tComment.VCCO_CMVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                tComment.VCCO_SCVP_FLAG = "Y";
                            }
                            if ((currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) && nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                tComment.VCCO_CMVP_FLAG = "Y";
                                tComment.VCCO_SCVP_FLAG = "Y";
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                tComment.VCCO_SCVP_FLAG = "Y";
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                tComment.VCCO_CMVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_RUN_LP)) {
                                tComment.VCCO_SCEP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)) {
                                tComment.VCCO_SCSC_FLAG = "Y";
                            }

                            tComment.VCCO_UPDATED_BY = tUser;
                            tComment.VCCO_UPDATED = dtNow;

                            tCommentDal.Update(tComment, context);
                            #endregion

                            /*
                                action => currentAction
                                APPROVE_4 => APPROVE_4 = SAVE DRAFT
                                APPROVE_4 => APPROVE_5 = Submit (Agree/Disagree)
                                APPROVE_5 => APPROVE_4 = Rejected by SCSC Section Head
                                APPROVE_5 => APPROVE_8 = Approve (Agree/Disagree)
                                APPROVE_7 => APPROVE_4 = date is not in period
                                APPROVE_7 => APPROVE_8 = date is in period
                                APPROVE_8 => APPROVE_5 = Rejected by SCVP
                                APPROVE_8 => APPROVE_6 = Rejected by TNVP
                                APPROVE_8 => APPROVE_7 = Rejected by CMVP
                                APPROVE_8 => APPROVE_9 = APPROVED
                            */
                            string newNextStatus = nextStatus;
                            string CMNextStatus = tData.VCDA_CM_STATUS;
                            string NextStatus_SCSC = tData.VCDA_REVISE_SCSC_STATUS;
                            string NextStatus_SCEP = tData.VCDA_REVISE_SCEP_STATUS;
                            if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)) {
                                if (tComment.VCCO_SCSC_AGREE_FLAG == "Y") {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                } else {
                                    newNextStatus = CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_7, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE, encryptFlag = "N" });
                                }
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                                newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK))
                                  && nextStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                                if (tComment.VCCO_SCEP_FLAG == "W") {
                                    NextStatus_SCEP = CPAIConstantUtil.STATUS_REVISE_LP_RUN;
                                }
                                if (tComment.VCCO_SCSC_FLAG == "W") {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY;
                                }
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });

                                //etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                //etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                //etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });

                            }
                              //------ ICE Edit -------//
                              else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE))) {
                                if (tComment.VCCO_SCEP_FLAG == "W") {
                                    NextStatus_SCEP = CPAIConstantUtil.STATUS_REVISE_LP_RUN;
                                }
                                if (tComment.VCCO_SCSC_FLAG == "W") {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY;
                                }
                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            }
                              //------ Revise By 2 VP ------//
                              else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE))) {
                                newNextStatus = tData.VCDA_SC_STATUS;
                                CMNextStatus = CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT;
                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) ||
                                  currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
                                CMNextStatus = CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT;
                                //if(nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE))
                                //{
                                //    newNextStatus = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE;
                                //}
                                //else
                                //{
                                //    newNextStatus = tData.VCDA_SC_STATUS;
                                //}
                                newNextStatus = tData.VCDA_SC_STATUS;

                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if (((currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISED_PURCHASE_COMMENT)) ||
                              ((currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT))) {
                                CMNextStatus = nextStatus;
                                //nextStatus = tData.VCDA_SC_STATUS;
                                newNextStatus = tData.VCDA_SC_STATUS;
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                }
                                //else if (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE))
                                //{
                                //    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                //    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                //}
                                else {
                                    if (tComment.VCCO_SCVP_FLAG == "Y") {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_9, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                    }
                                }
                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_LP_RUN)) {
                                newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                NextStatus_SCEP = nextStatus;

                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                } else {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                }
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_RUN_LP)) {
                                NextStatus_SCEP = nextStatus;

                                newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                } else {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                }
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_RUN_LP)) {
                                NextStatus_SCEP = nextStatus;

                                if (tComment.VCCO_SCSC_FLAG != "W") {
                                    newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                    } else {
                                        if (tComment.VCCO_CMVP_FLAG == "Y") {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        } else {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        }
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                } else {
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                }
                            }
                              //------ Revise Tank AVAILABILITY ------//
                              else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY)) {
                                if (tComment.VCCO_SCSC_AGREE_FLAG == "Y") {
                                    NextStatus_SCSC = nextStatus;
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                } else {
                                    NextStatus_SCSC = nextStatus;
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                }
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_TANK)) {
                                if (tComment.VCCO_SCSC_AGREE_FLAG == "Y") {
                                    NextStatus_SCSC = nextStatus;
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                } else {
                                    NextStatus_SCSC = nextStatus;
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                }
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK)) {
                                if (tComment.VCCO_SCSC_AGREE_FLAG == "Y") {
                                    NextStatus_SCSC = nextStatus;
                                    if (tComment.VCCO_SCEP_FLAG != "W") {
                                        newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                        if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                        } else {
                                            if (tComment.VCCO_CMVP_FLAG == "Y") {
                                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP, encryptFlag = "N" });
                                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                            } else {
                                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK, encryptFlag = "N" });
                                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                            }
                                        }
                                        etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    } else {
                                        newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                        etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                        if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        } else {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        }
                                    }
                                } else {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA;
                                    newNextStatus = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = "APPROVE_17", encryptFlag = "N" });
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                }
                            } else if ((currentStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSED_ETA)) {

                                if (tData.VCDA_DISCHARGING_DATE_FROM >= tData.VCDA_SCSC_REVISED_DATE_FROM && tData.VCDA_DISCHARGING_DATE_TO <= tData.VCDA_SCSC_REVISED_DATE_TO) {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK;
                                    newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                    } else {
                                        if (tComment.VCCO_CMVP_FLAG == "Y") {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        } else {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        }
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                } else if (tData.VCDA_DISCHARGING_DATE_FROM >= discharging_from && tData.VCDA_DISCHARGING_DATE_TO <= discharging_to) {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK;
                                    newNextStatus = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                    } else {
                                        if (tComment.VCCO_CMVP_FLAG == "Y") {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        } else {
                                            etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK, encryptFlag = "N" });
                                            etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_8, encryptFlag = "N" });
                                        }
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                } else {
                                    NextStatus_SCSC = CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY;
                                    newNextStatus = tData.VCDA_SC_STATUS;
                                    if (currentStatus.Equals(CPAIConstantUtil.STATUS_REQUEST_TO_REVISE)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_REQUEST_TO_REVISE, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }

                                }
                            }
                              //------End ICE Edit -------//
                              else if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)) {
                                if (tData.VCDA_DISCHARGING_DATE_FROM >= tData.VCDA_SCSC_REVISED_DATE_FROM && tData.VCDA_DISCHARGING_DATE_TO <= tData.VCDA_SCSC_REVISED_DATE_TO) {
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                }
                                  //else if (tData.VCDA_DISCHARGING_DATE_FROM >= discharging_from && tData.VCDA_DISCHARGING_DATE_TO <= discharging_to)
                                  //{
                                  //    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                  //    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                  //}
                                  else {
                                    newNextStatus = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_4, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK, encryptFlag = "N" });
                                }
                            } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {

                                bool vpFlag = tComment.VCCO_SCVP_FLAG == "Y" && tComment.VCCO_TNVP_FLAG == "Y" && tComment.VCCO_CMVP_FLAG == "Y" ? true : false;
                                flag = vpFlag;
                                //call F63 to update main transaction status
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)) {
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = "REJECT_5", encryptFlag = "N" });
                            } else {
                                etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                            }

                            tData.VCDA_REVISE_SCSC_STATUS = NextStatus_SCSC;
                            tData.VCDA_REVISE_SCEP_STATUS = NextStatus_SCEP;
                            tData.VCDA_SC_STATUS = newNextStatus;
                            tData.VCDA_CM_STATUS = CMNextStatus;
                            tData.VCDA_UPDATED = dtNow;
                            tData.VCDA_UPDATED_BY = tUser;
                            if (dataDetail != null && dataDetail.comment.scsc_agree_flag == CPAIConstantUtil.VCOOL_SCSC_DISAGREE) {
                                tData.VCDA_SCSC_REVISED_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                tData.VCDA_SCSC_REVISED_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                            }
                            if (dataDetail != null && dataDetail.requester_info != null) {
                                if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK) || nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY)) {
                                    tData.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                                }
                                tData.VCDA_TNVP_NOTE = dataDetail.requester_info.reason_reject_TNVP;
                                tData.VCDA_SCVP_NOTE = dataDetail.requester_info.reason_reject_SCVP;
                            }


                            if (nextStatus == "WAITING CMVP SCVP APPROVE" && tData.VCDA_CM_STATUS == null) {
                                tData.VCDA_CM_STATUS = "WAITING CMVP SCVP APPROVE";
                            } else if (nextStatus == "WAITING SCVP APPROVE" && tData.VCDA_CM_STATUS == "WAITING CMVP SCVP APPROVE") {
                                tData.VCDA_CM_STATUS = "CMVP APPROVED";
                            } else if (nextStatus == "SCVP REQUEST TO REVISE" && tData.VCDA_CM_STATUS == "CMVP APPROVED") {
                                tData.VCDA_CM_STATUS = "CMVP APPROVED";
                            } else if (nextStatus == "SCVP REQUEST TO REVISE" && tData.VCDA_CM_STATUS == "WAITING CMVP SCVP APPROVE") {
                                tData.VCDA_CM_STATUS = "WAITING CMVP APPROVE";
                            } else if (nextStatus == "CHECK TANK APPROVED" && tData.VCDA_CM_STATUS == "WAITING CMVP APPROVE") {
                                tData.VCDA_CM_STATUS = "CMVP APPROVED";
                            } else if (nextStatus == "CMVP REQUEST TO REVISE") {
                                tData.VCDA_CM_STATUS = "REVISE PURCHASE COMMENT";
                            }

                            tDataDal.UpdateSCStatus(tData, context);

                            if (dataDetail != null) {
                                dataDetail.comment.cmcs_request = tComment.VCCO_CMCS_REQUEST;
                                dataDetail.comment.cmvp = tComment.VCCO_CMVP;
                                dataDetail.comment.tnvp = tComment.VCCO_TNVP;
                                dataDetail.comment.scvp = tComment.VCCO_SCVP;
                                dataDetail.comment.evpc = tComment.VCCO_EVPC;
                            }

                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        } catch (Exception ex) {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolExpertUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateDetailDB # ");
            } catch (Exception ex) {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }

        public bool InsertDB_ETA_Date_History(string TransID, string UserGroup, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail) {
            log.Info("# Start State CPAIVcoolUpdateDataState >> InsertDB #  ");
            string nextStatus = etxValue.GetValue(CPAIConstantUtil.NextStatus);

            #region Insert Data into DB            
            bool isSuccess = false;
            DateTime dtNow;

            try {
                using (var context = new EntityCPAIEngine()) {
                    using (var dbContextTransaction = context.Database.BeginTransaction()) {
                        try {
                            dtNow = DateTime.Now;
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            #region VCO_ETA_DATE_HISTORY
                            VCO_ETA_DATE_HISTORY_DAL etaDateHistoryDal = new VCO_ETA_DATE_HISTORY_DAL();
                            VCO_ETA_DATE_HISTORY etaDateHistory = new VCO_ETA_DATE_HISTORY();
                            etaDateHistory.VCDH_ROW_ID = Guid.NewGuid().ToString("N");
                            etaDateHistory.VCDH_FK_VCO_DATA = TransID;
                            etaDateHistory.VCDH_USER_GROUP = UserGroup;
                            if (UserGroup.Contains("SC")) {
                                etaDateHistory.VCDH_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                etaDateHistory.VCDH_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                            } else {
                                etaDateHistory.VCDH_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");
                                etaDateHistory.VCDH_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                            }
                            if (UserGroup == "SCSC") {
                                etaDateHistory.VCDH_STATUS = "SUBMIT";
                            } else if (UserGroup == "SCSC_SH") {
                                etaDateHistory.VCDH_STATUS = "VERIFY";
                            } else if (UserGroup == "CMCS") {
                                etaDateHistory.VCDH_STATUS = "SUBMIT";
                            }

                            etaDateHistory.VCDH_CREATED = dtNow;
                            etaDateHistory.VCDH_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistory.VCDH_UPDATED = dtNow;
                            etaDateHistory.VCDH_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                            etaDateHistoryDal.Save(etaDateHistory, context);
                            #endregion                            
                            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            dbContextTransaction.Commit();
                            isSuccess = true;
                        } catch (Exception ex) {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAIVcoolUpdateDataState >> Rollback # :: Exception >>> ", ex);
                            log.Error("CPAIVcoolUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            string res = ex.Message;
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
            } catch (Exception e) {
                var tem = MessageExceptionUtil.GetaAllMessages(e);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Error("# Error InsertDB >> Rollback # :: Exception >>> ", e);
                log.Error("InsertDB::Rollback >>> " + e.GetEngineMessage());
                isSuccess = false;
            }
            #endregion

            log.Info("# End State CPAIVcoolUpdateDataState >> InsertDB # ");
            return isSuccess;
        }

        //zkavin.i Add this method to replace the old method.
        public bool UpdateDetailDB_New(string TransID, string nextStatus, string currentStatus, string currentAction, Dictionary<string, ExtendValue> etxValue, VcoRootObject dataDetail, ref bool flag, FUNCTION_TRANSACTION ft) {
            log.Info("# Start State CPAIVcoolExpertUpdateDataState >> UpdateDetailDB #  ");
            #region Insert Data into DB
            DateTime dtNow = DateTime.Now;
            string tUser = etxValue.GetValue(CPAIConstantUtil.User);

            bool isSuccess = false;
            try {
                using (var context = new EntityCPAIEngine()) {
                    using (var dbContextTransaction = context.Database.BeginTransaction()) {
                        try {

                            #region Comment
                            VCO_COMMENT_DAL tCommentDal = new VCO_COMMENT_DAL();
                            VCO_COMMENT tComment = tCommentDal.GetByDataID(TransID);

                            VCO_DATA_DAL tDataDal = new VCO_DATA_DAL();
                            VCO_DATA tData = tDataDal.GetByID(TransID);


                            DateTime? discharging_from = null;
                            DateTime? discharging_to = null;

                            if (tData != null) {
                                discharging_from = tData.VCDA_DISCHARGING_DATE_FROM;
                                discharging_to = tData.VCDA_DISCHARGING_DATE_TO;
                            }

                            if (dataDetail != null) {
                                tData.VCDA_DISCHARGING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_to)) ? tData.VCDA_DISCHARGING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_to, "dd-MMM-yyyy");
                                tData.VCDA_DISCHARGING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.discharging_date_from)) ? tData.VCDA_DISCHARGING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.discharging_date_from, "dd-MMM-yyyy");

                                tComment.VCCO_CMCS_REVISE = string.IsNullOrEmpty(dataDetail.comment.revise_cmvp) ? tComment.VCCO_CMCS_REVISE : dataDetail.comment.revise_cmvp;
                                tComment.VCCO_CMCS_NOTE = String.IsNullOrEmpty(dataDetail.comment.cmcs_note) ? tComment.VCCO_CMCS_NOTE : dataDetail.comment.cmcs_note;
                                tComment.VCCO_CMCS_REQUEST = String.IsNullOrEmpty(dataDetail.comment.cmcs_request) ? tComment.VCCO_CMCS_REQUEST : dataDetail.comment.cmcs_request; //CMCS Comment on request date.
                                tComment.VCCO_SCSC = String.IsNullOrEmpty(dataDetail.comment.scsc) ? tComment.VCCO_SCSC : dataDetail.comment.scsc;
                                if (currentAction != "CALLBACK_4" && currentAction != "CALLBACK_2") {
                                    tComment.VCCO_SCSC_AGREE_FLAG = dataDetail.comment.scsc_agree_flag.ToUpper().Equals(CPAIConstantUtil.VCOOL_SCSC_DISAGREE) ? "N" : "Y";
                                }
                                if (currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_PROPOSE_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) || currentStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA) || currentStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE))
                                    tComment.VCCO_CMCS_PROPOSE_DISCHARGE = dataDetail.comment.cmcs_propose_discharge;
                                if (currentStatus.Equals(CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP))
                                    tComment.VCCO_CMVP = dataDetail.comment.cmvp;
                                if (currentStatus.Equals(CPAIConstantUtil.DESCRIPTION_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) || currentStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP))
                                    tComment.VCCO_SCVP = dataDetail.comment.scvp;
                            }

                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                                tData.VCDA_REASON_SCEP_SH = "";
                                tData.VCDA_REASON_SCSC_SH = "";
                                tData.VCDA_SCSC_SH_NOTE = "";
                                tComment.VCCO_SCEP_FLAG = string.IsNullOrEmpty(dataDetail.comment.scep_flag) ? "" : dataDetail.comment.scep_flag;
                                tComment.VCCO_SCSC_FLAG = string.IsNullOrEmpty(dataDetail.comment.scsc_flag) ? "" : dataDetail.comment.scsc_flag;
                                tComment.VCCO_SCVP_REVISE = string.IsNullOrEmpty(dataDetail.comment.revise_scvp) ? "" : dataDetail.comment.revise_scvp;
                            }
                            //------ ICE Edit -------//
                            else if ((nextStatus.Equals(CPAIConstantUtil.STATUS_REVISED_PURCHASE_COMMENT)) ||
                                (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT))) {
                                tData.VCDA_REASON_CMCS_SH = "";
                                tComment.VCCO_CMCS_NOTE = dataDetail.comment.cmcs_note;
                                tComment.VCCO_CMCS_PROPOSE_DISCHARGE = dataDetail.comment.cmcs_propose_discharge;
                            } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_LP_RUN)) {
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;
                                tDataDal.Update(tData, context);
                                if (dataDetail != null && dataDetail.crude_compare != null) {
                                    VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
                                    data_compare_dal.Delete(TransID, context);
                                    int i = 0;
                                    foreach (var item in dataDetail.crude_compare.yield) {
                                        VCO_CRUDE_COMPARISON data_compare = new VCO_CRUDE_COMPARISON();
                                        data_compare.VCCP_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + i++;
                                        data_compare.VCCP_ORDER = item.order;
                                        data_compare.VCCP_YIELD = item.yield;
                                        //data_compare.VCCP_BASE_CRUDE 
                                        data_compare.VCCP_BASE_VALUE = item.base_value;
                                        data_compare.VCCP_COMPARE_CRUDE = item.compare_crude;
                                        data_compare.VCCP_COMPARE_VALUE = item.compare_value;
                                        //data_compare.VCCP_DELTA_CRUDE 
                                        data_compare.VCCP_DELTA_VALUE = item.delta_value;
                                        data_compare.VCCP_CREATED = dtNow;
                                        data_compare.VCCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_UPDATED = dtNow;
                                        data_compare.VCCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_FK_VCO_DATA = TransID;
                                        data_compare_dal.Save(data_compare, context);
                                    }

                                }
                            } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_RUN_LP)) {
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;


                                if (dataDetail != null && dataDetail.crude_compare != null) {
                                    VCO_CRUDE_COMPARISON_DAL data_compare_dal = new VCO_CRUDE_COMPARISON_DAL();
                                    data_compare_dal.Delete(TransID, context);
                                    int i = 0;
                                    foreach (var item in dataDetail.crude_compare.yield) {
                                        VCO_CRUDE_COMPARISON data_compare = new VCO_CRUDE_COMPARISON();
                                        data_compare.VCCP_ROW_ID = ShareFn.GenerateCodeByDate("CPAI") + i++;
                                        data_compare.VCCP_ORDER = item.order;
                                        data_compare.VCCP_YIELD = item.yield;
                                        //data_compare.VCCP_BASE_CRUDE 
                                        data_compare.VCCP_BASE_VALUE = item.base_value;
                                        data_compare.VCCP_COMPARE_CRUDE = item.compare_crude;
                                        data_compare.VCCP_COMPARE_VALUE = item.compare_value;
                                        //data_compare.VCCP_DELTA_CRUDE 
                                        data_compare.VCCP_DELTA_VALUE = item.delta_value;
                                        data_compare.VCCP_CREATED = dtNow;
                                        data_compare.VCCP_CREATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_UPDATED = dtNow;
                                        data_compare.VCCP_UPDATED_BY = etxValue.GetValue(CPAIConstantUtil.User);
                                        data_compare.VCCP_FK_VCO_DATA = TransID;
                                        data_compare_dal.Save(data_compare, context);
                                    }
                                }
                                tDataDal.Update(tData, context);
                            } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_RUN_LP)) {
                                tComment.VCCO_SCEP_FLAG = "Y";
                                tComment.VCCO_SCEP = (String.IsNullOrEmpty(dataDetail.comment.scep)) ? string.Empty : dataDetail.comment.scep;
                                tComment.VCCO_LP_RUN_SUMMARY = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary)) ? string.Empty : dataDetail.comment.lp_run_summary;
                                tComment.VCCO_LP_RUN_ATTACH_FILE = dataDetail.comment.lp_run_attach_file;
                                tComment.VCCO_LP_RUN_SUMMARY_MOBILE = (String.IsNullOrEmpty(dataDetail.comment.lp_run_summary_mobile)) ? string.Empty : dataDetail.comment.lp_run_summary_mobile;
                                tData.VCDA_PREMIUM_MAXIMUM = (String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) ? tData.VCDA_PREMIUM_MAXIMUM : dataDetail.crude_info.premium_maximum;
                                tData.VCDA_PROCESSING_DATE_FROM = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_form)) ? tData.VCDA_PROCESSING_DATE_FROM : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_form, "dd-MMM-yyyy");
                                tData.VCDA_PROCESSING_DATE_TO = (String.IsNullOrEmpty(dataDetail.crude_info.propcessing_period_to)) ? tData.VCDA_PROCESSING_DATE_TO : ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.propcessing_period_to, "dd-MMM-yyyy");
                                tData.VCDA_REASON_SCEP_SH = (String.IsNullOrEmpty(dataDetail.requester_info.reason_reject_SCEP_SH)) ? tData.VCDA_REASON_SCEP_SH : dataDetail.requester_info.reason_reject_SCEP_SH;
                                tData.VCDA_COMPARISON_JSON = dataDetail.crude_compare != null && dataDetail.crude_compare.comparison_json != null ? new JavaScriptSerializer().Serialize(dataDetail.crude_compare.comparison_json) : tData.VCDA_COMPARISON_JSON;
                                if (!String.IsNullOrEmpty(tData.VCDA_PREMIUM) && !String.IsNullOrEmpty(dataDetail.crude_info.premium_maximum)) {
                                    if (Convert.ToDecimal(tData.VCDA_PREMIUM) > Convert.ToDecimal(dataDetail.crude_info.premium_maximum)) {
                                        etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "T", encryptFlag = "N" });
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.CheckSendMail_M05, new ExtendValue { value = "F", encryptFlag = "N" });
                                    }
                                }
                                tDataDal.Update(tData, context);
                            } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
                                tComment.VCCO_CMVP = dataDetail.comment.cmvp;
                            }

                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                                tComment.VCCO_CMVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                tComment.VCCO_SCVP_FLAG = "Y";
                            }
                            if (nextStatus.Equals(CPAIConstantUtil.STATUS_CHECK_TANK_APPROVED)) {
                                tComment.VCCO_CMVP_FLAG = "Y";
                                tComment.VCCO_SCVP_FLAG = "Y";
                            }

                            tComment.VCCO_UPDATED_BY = tUser;
                            tComment.VCCO_UPDATED = dtNow;


                            #endregion


                            string currentStatusInFunctionTransaction = ft.FTX_INDEX4;// current status in database (FUNCTION_TRANSACTION).
                            string newNextStatus = nextStatus;
                            string CMNextStatus = tData.VCDA_CM_STATUS;
                            string NextStatus_SCSC = tData.VCDA_REVISE_SCSC_STATUS;
                            string NextStatus_SCEP = tData.VCDA_REVISE_SCEP_STATUS;

                            var allStatusForVP = CPAIConstantUtil.FLOW_IN_VP;
                            
                            var flowSCSC = CPAIConstantUtil.FLOW_SCSC;

                            var flowSCEP = CPAIConstantUtil.FLOW_SCEP;

                            var flowCMCS = CPAIConstantUtil.FLOW_CMCS;

                            if (!allStatusForVP.Contains(currentStatusInFunctionTransaction)) {// action before VP position.
                                if (currentStatusInFunctionTransaction == null) {// come in case of creating flow
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                } else if (currentStatusInFunctionTransaction.Equals(CPAIConstantUtil.STATUS_WAITING_APPROVE_TANK) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) &&
                                    tComment.VCCO_SCSC_AGREE_FLAG == "N") { // use verify button but disagree about eta data that why it need to send the document to edit eta date instead of the normal route from button.
                                    //Special case in SCSC process                                      
                                    nextStatus = CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_7, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    tData.VCDA_SCSC_REVISED_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                    tData.VCDA_SCSC_REVISED_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                                } else if (currentStatusInFunctionTransaction.Equals(CPAIConstantUtil.STATUS_WAITING_EDIT_ETA_DATE) && nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK) &&
                                   !(tData.VCDA_DISCHARGING_DATE_FROM >= tData.VCDA_SCSC_REVISED_DATE_FROM && tData.VCDA_DISCHARGING_DATE_TO <= tData.VCDA_SCSC_REVISED_DATE_TO)) {
                                    //special route in case of Editing ETA in case of the ETA date different from SCSC data. so it need to send back to SCSC for approving again.
                                    nextStatus = CPAIConstantUtil.STATUS_WAITING_CHECK_TANK;
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = CPAIConstantUtil.ACTION_APPROVE_4, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                } else {// normal route from button data. (CPAI_ACTION_BUTTON table*)
                                    newNextStatus = nextStatus;
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                }
                                tData.VCDA_SC_STATUS = nextStatus;
                            } else { //during VP position.
                                if (allStatusForVP.Contains(currentStatus)) { //action in VP process.
                                    if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_8) || currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_9)) {
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = currentAction, encryptFlag = "N" });

                                        if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) {
                                            tData.VCDA_CM_STATUS = CPAIConstantUtil.STATUS_APPROVED_BY_CMVP;
                                        } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_SCVP)) {
                                            tData.VCDA_SC_STATUS = CPAIConstantUtil.STATUS_APPROVED_BY_SCVP;
                                        }

                                        if (currentAction.Equals(CPAIConstantUtil.ACTION_APPROVE_9)) {// Both VP Approve
                                            bool vpFlag = tComment.VCCO_SCVP_FLAG == "Y" && tComment.VCCO_TNVP_FLAG == "Y" && tComment.VCCO_CMVP_FLAG == "Y" ? true : false;
                                            flag = vpFlag;
                                        }
                                    } else {
                                        etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = ft.FTX_INDEX3, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = ft.FTX_INDEX4, encryptFlag = "N" });
                                    }
                                    // VP request to revise
                                    if (nextStatus.Equals(CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE)) {
                                        if (tComment.VCCO_SCSC_FLAG == "W") {
                                            tData.VCDA_REVISE_SCSC_STATUS = CPAIConstantUtil.VCOOL_BEGINNING_SCSC_FLOW;
                                        } else {
                                            tData.VCDA_REVISE_SCSC_STATUS = "";
                                        }
                                        if (tComment.VCCO_SCEP_FLAG == "W") {
                                            tData.VCDA_REVISE_SCEP_STATUS = CPAIConstantUtil.VCOOL_BEGINNING_SCEP_FLOW;
                                        } else {
                                            tData.VCDA_REVISE_SCEP_STATUS = "";
                                        }
                                        tData.VCDA_SC_STATUS = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE;
                                        //Reject to revise from vp
                                        etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.REVISE_STATUS, new ExtendValue { value = CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE)) {
                                        nextStatus = CPAIConstantUtil.VCOOL_BEGINNING_CMCS_FLOW;
                                        tData.VCDA_CM_STATUS = nextStatus;
                                        //Reject to revise from vp
                                        etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                        etxValue.SetValue(CPAIConstantUtil.REVISE_STATUS, new ExtendValue { value = CPAIConstantUtil.STATUS_CMVP_REQUEST_TO_REVISE, encryptFlag = "N" });
                                    }
                                } else {//during revising process.
                                    etxValue.SetValue(CPAIConstantUtil.Action, new ExtendValue { value = ft.FTX_INDEX3, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.Status, new ExtendValue { value = ft.FTX_INDEX4, encryptFlag = "N" });
                                    
                                    if (flowSCSC.Contains(nextStatus) || flowSCEP.Contains(nextStatus)) {//SCVP revise process
                                        //Special case in SCSC process.
                                        if (currentStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVE_TANK) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_APPROVED_TANK) &&
                                       tComment.VCCO_SCSC_AGREE_FLAG == "N") { // use verify button but disagree about eta data that why it need to send the document to edit eta date instead of the normal route from button.                                   
                                            nextStatus = CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA;
                                            tData.VCDA_SCSC_REVISED_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                            tData.VCDA_SCSC_REVISED_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                                            currentAction = "APPROVE_17";
                                        } else if (currentStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSE_ETA) && nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSED_ETA) &&
                                            !(tData.VCDA_DISCHARGING_DATE_FROM >= tData.VCDA_SCSC_REVISED_DATE_FROM && tData.VCDA_DISCHARGING_DATE_TO <= tData.VCDA_SCSC_REVISED_DATE_TO)) {
                                            //special route in case of Editing ETA in case of the ETA date different from SCSC data. so it need to send back to SCSC for approving again.
                                            currentAction = "APPROVE_4";
                                            nextStatus = CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY;
                                        } else if (nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_PROPOSED_ETA)) {// CMCS agree with ETA date from SCSC.
                                            nextStatus = CPAIConstantUtil.VCOOL_ENDING_SCSC_FLOW;
                                        }

                                        if (flowSCSC.Contains(nextStatus)) {// process revised flow.
                                            tData.VCDA_REVISE_SCSC_STATUS = nextStatus;
                                        } else if (flowSCEP.Contains(nextStatus)) {
                                            tData.VCDA_REVISE_SCEP_STATUS = nextStatus;
                                        }

                                        if (!String.IsNullOrEmpty(tData.VCDA_REVISE_SCSC_STATUS) && tData.VCDA_REVISE_SCSC_STATUS.Equals(CPAIConstantUtil.VCOOL_ENDING_SCSC_FLOW)
                                            && tComment.VCCO_SCSC_FLAG == "W") {
                                            tComment.VCCO_SCSC_FLAG = "Y";
                                        }
                                        if (!String.IsNullOrEmpty(tData.VCDA_REVISE_SCEP_STATUS) && tData.VCDA_REVISE_SCEP_STATUS.Equals(CPAIConstantUtil.VCOOL_ENDING_SCEP_FLOW)
                                            && tComment.VCCO_SCEP_FLAG == "W") {
                                            tComment.VCCO_SCEP_FLAG = "Y";
                                        }

                                        // Set VCDA_SC_STATUS and SC flag
                                        //      if (((tData.VCDA_REVISE_SCSC_STATUS.Equals( CPAIConstantUtil.VCOOL_ENDING_SCSC_FLOW) && tComment.VCCO_SCSC_FLAG == "W") || tComment.VCCO_SCSC_FLAG != "W" || tComment.VCCO_SCSC_FLAG == "Y") &&
                                        //((tData.VCDA_REVISE_SCEP_STATUS.Equals(CPAIConstantUtil.VCOOL_ENDING_SCEP_FLOW) && tComment.VCCO_SCEP_FLAG == "W") || tComment.VCCO_SCEP_FLAG != "W" || tComment.VCCO_SCEP_FLAG == "Y")) {
                                        if ((tComment.VCCO_SCSC_FLAG != "W") && // When completed revise process so SC will back to SCVP process again.
                                        (tComment.VCCO_SCEP_FLAG != "W")) {
                                            //tData.VCDA_SC_STATUS = CPAIConstantUtil.VCOOL_SC_COMPLETE_REVISE;
                                            tData.VCDA_SC_STATUS = currentStatusInFunctionTransaction; 
                                        }

                                    } else if (flowCMCS.Contains(nextStatus)) { //CMVP revise process
                                        tData.VCDA_CM_STATUS = nextStatus;
                                        if (tData.VCDA_CM_STATUS == CPAIConstantUtil.VCOOL_ENDING_CMCS_FLOW) {
                                            tComment.VCCO_CMVP_FLAG = "Y";
                                        }
                                    }
                                    etxValue.SetValue(CPAIConstantUtil.ACTION_REVISE_STATUS, new ExtendValue { value = currentAction, encryptFlag = "N" });
                                    etxValue.SetValue(CPAIConstantUtil.REVISE_STATUS, new ExtendValue { value = nextStatus, encryptFlag = "N" });
                                }
                            }

                            tCommentDal.Update(tComment, context);

                            if (dataDetail != null && dataDetail.comment.scsc_agree_flag == CPAIConstantUtil.VCOOL_SCSC_DISAGREE) {
                                tData.VCDA_SCSC_REVISED_DATE_FROM = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_from, "dd-MMM-yyyy");
                                tData.VCDA_SCSC_REVISED_DATE_TO = ShareFn.ConvertStringDateFormatToDatetime(dataDetail.crude_info.revised_date_to, "dd-MMM-yyyy");
                            }
                            if (dataDetail != null && dataDetail.requester_info != null) {
                                if (nextStatus.Equals(CPAIConstantUtil.STATUS_WAITING_CHECK_TANK) || nextStatus.Equals(CPAIConstantUtil.STATUS_REVISE_TANK_AVAILABILITY)) {
                                    tData.VCDA_SCSC_SH_NOTE = dataDetail.requester_info.reason_reject_SCSC_SH;
                                }
                                //There is no field in cmvp reason for this case. commented zkavin.i.
                                //tData.VCDA_TNVP_NOTE = dataDetail.requester_info.reason_reject_TNVP;
                                tData.VCDA_SCVP_NOTE = dataDetail.requester_info.reason_reject_SCVP;
                            }

                            tData.VCDA_UPDATED = dtNow;
                            tData.VCDA_UPDATED_BY = tUser;

                            tDataDal.UpdateSCStatus(tData, context);


                            if (dataDetail != null) {
                                dataDetail.comment.cmcs_request = tComment.VCCO_CMCS_REQUEST;
                                dataDetail.comment.cmvp = tComment.VCCO_CMVP;
                                dataDetail.comment.tnvp = tComment.VCCO_TNVP;
                                dataDetail.comment.scvp = tComment.VCCO_SCVP;
                                dataDetail.comment.evpc = tComment.VCCO_EVPC;
                            }


                            string dataDetail_o = JSonConvertUtil.modelToJson(dataDetail);
                            etxValue.SetValue(CPAIConstantUtil.DataDetail, new ExtendValue { value = dataDetail_o, encryptFlag = "N" });

                            dbContextTransaction.Commit();
                            isSuccess = true;
                        } catch (Exception ex) {
                            var tem = MessageExceptionUtil.GetaAllMessages(ex);
                            log.Error("# Error :: Exception >>>  " + tem);
                            log.Error("# Error CPAICoolExpertUpdateDataState >> UpdateDetailDB # :: Rollback >>>> ", ex);
                            log.Error("CPAICoolExpertUpdateDataState::Rollback >>> " + ex.GetEngineMessage());
                            dbContextTransaction.Rollback();
                            isSuccess = false;
                        }
                    }
                }
                log.Info("# End State CPAICoolExpertUpdateDataState >> UpdateDetailDB # ");
            } catch (Exception ex) {
                log.Error("# Error UpdateCommentDB >> UpdateCommentDB # :: Exception >>>> ", ex);
                log.Error("CPAICoolExpertUpdateDataState::Exception >>> " + ex.GetEngineMessage());
                isSuccess = false;
            }
            #endregion
            return isSuccess;
        }



    }
}