﻿using com.pttict.downstream.common.utilities;
using com.pttict.engine;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.model;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALCDO;
using ProjectCPAIEngine.DAL.DALPAF;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Flow.Model;
using ProjectCPAIEngine.Flow.Utilities;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.F10000091
{
    public class CPAICDOListTxnState : BasicBean, StateFlowAction
    {
        private XmlParser xmlParser = new XmlParser();
        private string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;
        public void doAction(StateModel stateModel)
        {
            log.Info("# Start State CPAICDOListTxnState #  ");
            RespCodeManagement respCodeManagement = new RespCodeManagement();

            string currentNameSpace = CPAIConstantUtil.PROJECT_NAME_SPACE;
            //string currentCode = CPAIConstantRespCodeUtil.INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE;

            stateModel.BusinessModel.currentNameSpace = currentNameSpace;

            try
            {
                Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;

                string user = etxValue.GetValue(CPAIConstantUtil.User);
                string userGroup = etxValue.GetValue(CPAIConstantUtil.UserGroup) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroup) : "";
                string userGroupDelegate = etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) != null ? etxValue.GetValue(CPAIConstantUtil.UserGroupDelegate) : "";
                string system = etxValue.GetValue(CPAIConstantUtil.System);
                string function_code = etxValue.GetValue(CPAIConstantUtil.Function_code);

                List<string> lstUserGroup = new List<string>();
                if (!string.IsNullOrEmpty(userGroup)) lstUserGroup.Add(userGroup);
                if (!string.IsNullOrEmpty(userGroupDelegate)) lstUserGroup.Add(userGroupDelegate);

                ActionFunctionDAL acfMan = new ActionFunctionDAL();
                List<CPAI_ACTION_FUNCTION> result;
                if (String.IsNullOrWhiteSpace(function_code))
                {
                    //result = acfMan.findByUserGroupAndSystem(userGroup, system);
                    result = acfMan.findByUserGroupAndSystemDelegate(lstUserGroup, system);//set user group deligate by job 25072017
                }
                else
                {
                    //result = acfMan.findByUserGroupAndSystemAndFuncitonId(userGroup, system, function_code);
                    result = acfMan.findByUserGroupAndSystemAndFuncitonIdDelegate(lstUserGroup, system, function_code);//set user group deligate by job 25072017
                }
                List<CDOEncrypt> lstAllTx = new List<CDOEncrypt>();
                if (result.Count == 0)
                {
                    // action function not found
                    currentCode = CPAIConstantRespCodeUtil.ACTION_FUNCTION_NOT_FOUND_RESP_CODE;
                }
                else if (result.Count == 1)
                {
                    CPAI_ACTION_FUNCTION actionFunction = result[0];
                    lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                    setExtraXml(stateModel, lstAllTx);
                }
                else
                {
                    CPAI_ACTION_FUNCTION actionFunction = null;
                    for (int i = 0; i < result.Count; i++)
                    {
                        if (!String.IsNullOrWhiteSpace(result[0].ACF_FK_USER))
                        {
                            actionFunction = result[0];
                            break;
                        }
                    }
                    if (actionFunction != null)
                    {
                        lstAllTx.AddRange(getTransaction(stateModel, actionFunction));
                        setExtraXml(stateModel, lstAllTx);
                    }
                    else
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            CPAI_ACTION_FUNCTION acf = result[i];
                            lstAllTx.AddRange(getTransaction(stateModel, acf));
                        }
                        lstAllTx = lstAllTx.DistinctBy(m => m.req_transaction_id).ToList();
                        setExtraXml(stateModel, lstAllTx);
                    }
                }

                currentCode = ConstantRespCodeUtil.SUCCESS_RESP_CODE[0];

                //map response code to response description
                stateModel.BusinessModel.currentCode = currentCode;
                respCodeManagement.setCurrentCodeMapping(stateModel);
                log.Info("# End State CPAICDOListTxnState # :: Code >>> " + currentCode);
            }
            catch (Exception ex)
            {
                var tem = MessageExceptionUtil.GetaAllMessages(ex);
                log.Error("# Error :: Exception >>>  " + tem);
                log.Info("# Error CPAICDOListTxnState # :: Exception >>> " + ex);
                log.Error("CPAICDOListTxnState::Exception >>> ", ex);
                stateModel.BusinessModel.currentCode = currentCode;  //Internal System Exception
                //map response code to response description
                respCodeManagement.setCurrentCodeMapping(stateModel);   //will be get response description from DB
                stateModel.BusinessModel.currentDesc = stateModel.BusinessModel.currentDesc + "[" + ex.GetEngineMessage() + "]";
            }
        }

        public void setExtraXml(StateModel stateModel, List<CDOEncrypt> lstAllTx)
        {
            log.Info("# Start State CPAIListTxnPAFState >> setExtraXml #  ");
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            List_CDOtrx li = new List_CDOtrx();
            li.CDOTransaction = lstAllTx;

            // set extra xml
            string xml = ShareFunction.XMLSerialize(li);
            xml = CPAIXMLParser.RemoveXmlDefinition(xml);
            ExtendValue respTrans = new ExtendValue();
            respTrans.value = xml;
            respTrans.encryptFlag = ConstantDBUtil.NO_FLAG;
            etxValue.Add(ConstantUtil.EXTRA_XML_RESP_ETX_VALUE, respTrans);

            ExtendValue extRowsPerPage = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.RowsPerPage), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extPageNumber = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.PageNumber), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extStatus = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.Status), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extSystem = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.System), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extFromDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.FromDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            ExtendValue extToDate = new ExtendValue
            { value = etxValue.GetValue(CPAIConstantUtil.ToDate), encryptFlag = ConstantDBUtil.NO_FLAG };
            // set response
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.RowsPerPage, extRowsPerPage);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.PageNumber, extPageNumber);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.Status, extStatus);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.System, extSystem);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.FromDate, extFromDate);
            etxValue.Add(ConstantUtil.RESP + CPAIConstantUtil.ToDate, extToDate);
            log.Info("# End State CPAIListTxnPAFState >> setExtraXml # :: Code >>> ");
        }


        public List<CDOEncrypt> getTransaction(StateModel stateModel, CPAI_ACTION_FUNCTION actionFunction)
        {
            log.Info("# Start State CPAIListTxnPAFState >> getTransaction #  ");
            CDO_DATA_DAL dafMan = new CDO_DATA_DAL();
            Dictionary<string, ExtendValue> etxValue = stateModel.BusinessModel.etxValue;
            string user = etxValue.GetValue(CPAIConstantUtil.User);
            string fromDate = etxValue.GetValue(CPAIConstantUtil.FromDate);
            string toDate = etxValue.GetValue(CPAIConstantUtil.ToDate);
            string status = etxValue.GetValue(CPAIConstantUtil.Status);
            string create_by = etxValue.GetValue(CPAIConstantUtil.CreateBy);
            string vessel = etxValue.GetValue("vessel");
            FLOW_MANAGEMENT_DAL ftxMan = new FLOW_MANAGEMENT_DAL();
            List<FunctionTransaction> result;
            List<CDOEncrypt> list = new List<CDOEncrypt>();
            List<string> lstStatus = new List<string>();
            if (!string.IsNullOrWhiteSpace(actionFunction.ACF_FUNCTION_STATUS))
            {
                lstStatus = new List<string>(actionFunction.ACF_FUNCTION_STATUS.Split('|'));
            }
            result = ftxMan.findCPAICDOTransaction(actionFunction.ACF_FK_FUNCTION, lstStatus, null, fromDate, toDate, create_by, user);

            for (int i = 0; i < result.Count; i++)
            {
                FunctionTransaction ftx = result[i];
                CDOEncrypt m = new CDOEncrypt();
                CDO_DATA data = dafMan.Get(ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                //string for_company = dafMan.GetCompanyFromID(data.DDA_FOR_COMPANY);

                m.function_id = ftx.FUNCTION_TRANSACTION.FTX_FK_FUNCTION;
                m.function_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.result_namespace = ftx.FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE;
                m.result_status = ftx.FUNCTION_TRANSACTION.FTX_RETURN_STATUS;
                m.result_code = ftx.FUNCTION_TRANSACTION.FTX_RETURN_CODE;
                m.result_desc = ftx.FUNCTION_TRANSACTION.FTX_RETURN_DESC;
                m.req_transaction_id = ftx.FUNCTION_TRANSACTION.FTX_REQ_TRANS;
                m.transaction_id = ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID;
                m.response_message = ftx.FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE;
                m.system = ftx.FUNCTION_TRANSACTION.FTX_INDEX1;
                m.type = ftx.FUNCTION_TRANSACTION.FTX_INDEX2;
                m.action = ftx.FUNCTION_TRANSACTION.FTX_INDEX3;
                m.status = ftx.STATUS;
                m.assay_ref = ftx.FUNCTION_TRANSACTION.FTX_INDEX10;
                m.products = ftx.FUNCTION_TRANSACTION.FTX_INDEX12;
                m.origin = ftx.FUNCTION_TRANSACTION.FTX_INDEX13;
                m.categories = ftx.FUNCTION_TRANSACTION.FTX_INDEX14;
                m.kerogen = ftx.FUNCTION_TRANSACTION.FTX_INDEX15;
                m.characteristic = ftx.FUNCTION_TRANSACTION.FTX_INDEX16;
                m.maturity = ftx.FUNCTION_TRANSACTION.FTX_INDEX17;
                m.assay_date = ftx.FUNCTION_TRANSACTION.FTX_INDEX19;
                m.assay_from = ftx.ASSAY_FROM;
                m.assay_file = ftx.ASSAY_FILE;
                m.support_doc_file = ftx.SUPPORT_FILE;
                m.density = ftx.DENSITY;
                m.total_sulphur = ftx.SULPHUR;
                m.total_acid_number = ftx.ACID;
                m.expert_units = ftx.UNIT;
                //m.requester_name = data.CDA_CREATED_BY;
                //m.purchase_no = data.CDA_FORM_ID;
                //m.purchase_no = PAF_DATA_DAL.GetPurchaseNO(ftx.FUNCTION_TRANSACTION.FTX_TRANS_ID);
                //m.template_name = PAF_DATA_DAL.CreateTemplateName(data.PDA_TEMPLATE);
                m.draft_cam_file = ftx.DRAFT_CAM_FILE;
                m.final_cam_file = ftx.FINAL_CAM_FILE;
                m.draft_cam_status = ftx.DRAFT_CAM_STATUS;
                m.final_cam_status = ftx.FINAL_CAM_STATUS;

                //m.date_purchase = PAFHelper.ToOnlyDateString(data.CDA_DOC_DATE);
                //m.updated_date = PAFHelper.ToDateString(data.CDA_UPDATED);
                //m.updated_by = data.CDA_UPDATED_BY;
                //m.created_date = PAFHelper.ToDateString(data.CDA_CREATED);
                //m.created_by = data.CDA_CREATED_BY;

                #region demurrage
                //m.user_group = data.DDA_USER_GROUP;
                //m.from_id = data.DDA_FORM_ID;
                //m.for_company = dafMan.GetCompanyFromID(data.DDA_FOR_COMPANY);
                //m.type_of_transaction = data.DDA_TYPE_OF_TRANSECTION;
                //m.incoterm = data.DDA_INCOTERM;
                //m.demurrage_type = for_company + " " + dafMan.ShowDemurrageType(data.DDA_DEMURAGE_TYPE, for_company);
                //m.counterparty = dafMan.GetCounterpartyByTypeAndId(data.DDA_COUNTERPARTY_TYPE, data.DDA_SHIP_OWNER, data.DDA_BROKER, data.DDA_SUPPLIER, data.DDA_CUSTOMER, data.DDA_CUSTOMER_BROKER);
                //m.vessel_name = dafMan.GetVesselName(data.DDA_VESSEL);
                //m.product_crude = dafMan.GetProductOrCrude(data.CDO_MATERIAL.ToList());
                //m.cp_date_contract_date = PAFHelper.ToDateString(data.DDA_CONTRACT_DATE);
                //m.agreed_loading_laycan = dafMan.ToDateFromToFormat(data.DDA_AGREED_LOADING_LAYCAN_FROM, data.DDA_AGREED_LOADING_LAYCAN_TO);
                //m.agreed_discharging_laycan = dafMan.ToDateFromToFormat(data.DDA_AGREED_DC_LAYCAN_FROM, data.DDA_AGREED_DC_LAYCAN_TO);
                //m.agreed_laycan = dafMan.ToDateFromToFormat(data.DDA_AGREED_LAYCAN_FROM, data.DDA_AGREED_LAYCAN_TO);
                //m.time_on_demurrage = dafMan.ShowTextTimeAndHrs(data.DDA_TIME_ON_DEM_TEXT, data.DDA_TIME_ON_DEM_HRS);

                //m.contract_laytime = dafMan.ShowTextTimeAndHrs(data.DDA_LAYTIME_CONTRACT_TEXT, data.DDA_LAYTIME_CONTRACT_HRS);
                //m.actual_net_laytime = dafMan.ShowTextTimeAndHrs(data.DDA_NTS_SETTLED_TIME_TEXT, data.DDA_NTS_SETTLED_HRS);

                //if (data.DDA_IS_ADDRESS_COMMISSION == "Y")
                //{
                //    m.address_commission = string.Format("{0:n2}", data.DDA_AC_SETTLED_VALUE);
                //}
                //if (data.DDA_IS_SHARING_TO_TOP == "Y")
                //{
                //    m.demurrage_sharing_to_top = string.Format("{0:n2}", data.DDA_DST_SETTLED_VALUE);
                //}
                //if (data.DDA_IS_BROKER_COMMISSION == "Y")
                //{
                //    m.broker_commission = string.Format("{0:n2}", data.DDA_BROKER_COMMISSION_VALUE);
                //}
                //if ((data.DDA_DEMURAGE_TYPE ?? "").ToLower() == "paid")
                //{
                //    //m.net_payment_demurrage_value = string.Format("{0:n0}", data.DDA_NET_PAYMENT_DEM_VALUE);
                //    m.demurrage_paid_settled = string.Format("{0:n2}", data.DDA_NET_PAYMENT_DEM_VALUE_OV) + " " + data.DDA_CURRENCY_UNIT;
                //    m.agreed_laycan = null;
                //}
                //else
                //{
                //    //m.net_receiving_demurrage_value = string.Format("{0:n0}", data.DDA_NET_PAYMENT_DEM_VALUE);
                //    m.demurrage_received_settled = string.Format("{0:n2}", data.DDA_NET_PAYMENT_DEM_VALUE_OV) + " " + data.DDA_CURRENCY_UNIT;
                //    m.agreed_loading_laycan = null;
                //    m.agreed_discharging_laycan = null;

                //}
                #endregion

                list.Add(m);
            }

            log.Info("# End State CPAIListTxnPAFState >> getTransaction # :: Code >>> ");
            return list;
        }
    }
}
