﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class ApproveItem
    {
        public string approve_action { get; set; }
        public string approve_action_by { get; set; }
    }
}