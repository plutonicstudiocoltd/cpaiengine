﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class CAMTemplate
    {
        public string row { get; set; } //"13|14" or "13", | = merge
        public string column { get; set; } //"B|C" or "B", | = merge
        public string bg_color { get; set; } //"#FF0000"
        public string alignment { get; set; } //"H" = Horizonral or "V" = Vertical
        public string value { get; set; }
        public string text { get; set; }
        public string hyperlink { get; set; }

        public string[] image { get; set; }

        public string formula { get; set; }

        public string hidden { get; set; } // "Y" or "N"
       
        public string merge { get; set; } // "Y" or "N"
        public string font_name { get; set; } // "Calibri" or "Tahoma"
        public string font_bold { get; set; } // "Y" or "N"
        public string font_color { get; set; } // "#FF0000"
        public string font_size { get; set; } // "16"
        public string number_format { get; set; } // "#,##0.0000"
        public string indent { get; set; } // "Y" or "N"
        public string center { get; set; } // "Y" or "N"
        public string freeze_row { get; set; }  // "Y" or "N"
        public string freeze_col { get; set; } // "Y" or "N"

        public string border_style { get; set; } // "None" or "Thin" or "Thick" or "Medium" or "Hair" or "Double" or "Dashed" or "Dotted"
        public string border_color { get; set; } // "#FF0000"
        public string width { get; set; }

        public string font_italic { get; set; } // "Y" or "N"
        public string font_underline { get; set; } // "Y" or "N"
        public string font_textwrap { get; set; } // "Y" or "N"
        public string font_horizontal_alignment { get; set; } // "L" or "C" or "R"
        public string font_vertical_alignment { get; set; } // "T" or "M" or "B"

        public string border_top_style { get; set; } // "None" or "Thin" or "Thick" or "Medium" or "Hair" or "Double" or "Dashed" or "Dotted"
        public string border_top_color { get; set; } // "#FF0000"
        public string border_bottom_style { get; set; } // "None" or "Thin" or "Thick" or "Medium" or "Hair" or "Double" or "Dashed" or "Dotted"
        public string border_bottom_color { get; set; } // "#FF0000"
        public string border_left_style { get; set; } // "None" or "Thin" or "Thick" or "Medium" or "Hair" or "Double" or "Dashed" or "Dotted"
        public string border_left_color { get; set; } // "#FF0000"
        public string border_right_style { get; set; } // "None" or "Thin" or "Thick" or "Medium" or "Hair" or "Double" or "Dashed" or "Dotted"
        public string border_right_color { get; set; } // "#FF0000"

        public CAMTemplate()
        {
            this.hidden = "N";
            this.merge = "N";
            this.font_name = "Calibri";
            this.font_bold = "N";
            this.font_color = "#000000";
            this.font_size = "16";
            this.number_format = "#,##0.00";
            this.indent = "N";
            this.center = "N";

            this.border_style = "0";
            this.border_color = "#000000";

            this.font_italic = "N";
            this.font_underline = "N";
            this.font_textwrap = "N";
            this.font_horizontal_alignment = "L";
            this.font_vertical_alignment = "M";

            this.border_top_style = "0";
            this.border_top_color = "#000000";
            this.border_bottom_style = "0";
            this.border_bottom_color = "#000000";
            this.border_left_style = "0";
            this.border_left_color = "#000000";
            this.border_right_style = "0";
            this.border_right_color = "#000000";
        }

        public CAMTemplate(string row, string bg_color, string alignment)
        {
            this.row = row;
            this.bg_color = bg_color;
            this.alignment = alignment;
        }

        public CAMTemplate(string row, string column, string bg_color, string alignment) : this(row, bg_color, alignment)
        {
            this.column = column;
        }
    }

    public class ExpertTemplate : CAMTemplate
    {
        public ExpertTemplate()
        {
            this.hidden = "N";
            this.merge = "N";
            this.font_name = "Calibri";
            this.font_bold = "N";
            this.font_color = "#000000";
            this.font_size = "16";
            this.number_format = "#,##0.00";
            this.indent = "N";
            this.center = "N";

            this.border_style = "0";
            this.border_color = "#000000";

            this.font_italic = "N";
            this.font_underline = "N";
            this.font_textwrap = "N";
            this.font_horizontal_alignment = "L";
            this.font_vertical_alignment = "M";

            this.border_top_style = "0";
            this.border_top_color = "#000000";
            this.border_bottom_style = "0";
            this.border_bottom_color = "#000000";
            this.border_left_style = "0";
            this.border_left_color = "#000000";
            this.border_right_style = "0";
            this.border_right_color = "#000000";
        }
    }

    public class SummaryTemplate : CAMTemplate
    {
        public SummaryTemplate()
        {
            this.hidden = "N";
            this.merge = "N";
            this.font_name = "Calibri";
            this.font_bold = "N";
            this.font_color = "#000000";
            this.font_size = "12";
            this.number_format = "#,##0.00";
            this.indent = "N";
            this.center = "N";

            this.border_style = "0";
            this.border_color = "#000000";

            this.font_italic = "N";
            this.font_underline = "N";
            this.font_textwrap = "N";
            this.font_horizontal_alignment = "L";
            this.font_vertical_alignment = "M";

            this.border_top_style = "0";
            this.border_top_color = "#000000";
            this.border_bottom_style = "0";
            this.border_bottom_color = "#000000";
            this.border_left_style = "0";
            this.border_left_color = "#000000";
            this.border_right_style = "0";
            this.border_right_color = "#000000";
        }
    }

}