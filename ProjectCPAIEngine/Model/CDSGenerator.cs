﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public partial class CDSGenerator
    {
        private static List<CDO_DATA> cdo_dat = null;
        private static List<CDS_ATTACH_FILE> cds_attach_file_dat = null;
        private static List<CDS_BIDDING_ITEMS> cds_bidding_items_dat = null;
        private static List<CDS_CRUDE_PURCHASE_DETAIL> cds_crude_purchase_detail_dat = null;
        private static List<CDS_PROPOSAL_ITEMS> cds_proposal_items_dat = null;
        private static List<MT_MATERIALS> mt_materials_dat = null;
        private static List<MT_VENDOR> mt_vendor_dat = null;

        private static string CRUDE_NAME;
        private static string CRUDE_NAME_HEADER;

        private static List<string> STYLES_GEN_NAME;
        private static int CURRENT_STYLES_GEN_NAME_IDX;

        private static object Query<T>(string query)
        {
            EntityCPAIEngine db = new EntityCPAIEngine();
            var d = db.Database.SqlQuery<T>(query);
            if (d != null)
            {
                var a = d.ElementAt(0);
                return d.ElementAt(0);
            }
            return null;
        }
    }

    public partial class CDSGenerator
    {
        private static ShareFn _FN = new ShareFn();
        private static double NORMAL_ROW_HEIGHT = 10;
        private static double PAPER_WIDTH = 20;
        private static double GLOBAL_FONT_SIZE = 7;
        private static double GLOBAL_WHITE_BOX_FONT_SIZE = 6;
        private static double GLOBAL_BLACK_BOX_FONT_SIZE = 9.5;

        private static Unit getWidthUnitFromRatio(double r)
        {
            return Unit.FromCentimeter(PAPER_WIDTH * r);
        }

        private static string FormatNumber<T>(T number, int maxDecimals = 2, bool forceShowDecimal = true)
        {
            if (number == null || number.ToString() == "")
            {
                return "";
            }

            if (forceShowDecimal == false)
            {
                return Regex.Replace(String.Format("{0:n" + maxDecimals + "}", double.Parse(number.ToString())),
                     @"[" + System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator + "]?0+$", "");
            }
            else
            {
                string fixed_decimal = "";
                for (int i = 0; i < maxDecimals; i++)
                {
                    fixed_decimal += "0";
                }
                return String.Format("{0:0." + fixed_decimal + "}", double.Parse(number.ToString()));
            }
        }

        private static Paragraph DrawText(Cell cell, string text, TextFormat format = TextFormat.NotBold, ParagraphAlignment align = ParagraphAlignment.Left
            , VerticalAlignment v_align = VerticalAlignment.Center, byte[] rgba = null)
        {
            Paragraph paragraph = cell.AddParagraph();
            paragraph.Format.Alignment = align;
            cell.VerticalAlignment = v_align;
            if (text == null)
                return paragraph;
            paragraph.AddFormattedText(text, format);
            if (rgba != null)
            {
                paragraph.Format.Font.Color = Color.FromArgb(rgba[3], rgba[0], rgba[1], rgba[2]);
            }
            return paragraph;
        }

        private static Paragraph DrawMultipleText(Document doc, Cell cell, string[] texts, TextFormat[] formats = null, ParagraphAlignment align = ParagraphAlignment.Left
            , VerticalAlignment v_align = VerticalAlignment.Center, List<byte[]> rgba = null, double[] font_size = null)
        {
            Paragraph paragraph = cell.AddParagraph();
            paragraph.Format.Alignment = align;
            cell.VerticalAlignment = v_align;


            if (formats == null)
            {
                formats = new TextFormat[texts.Length];
                for (int i = 0; i < texts.Length; i++)
                {
                    formats[i] = TextFormat.NotBold;
                }
            }

            if (rgba == null)
            {
                rgba = new List<byte[]>();
                for (int i = 0; i < texts.Length; i++)
                {
                    if (i <= rgba.Count)
                    {
                        rgba.Add(new byte[] { 0, 0, 0, 255 });
                    }
                }
            }

            if (font_size == null)
            {
                font_size = new double[texts.Length];
                for (int i = 0; i < texts.Length; i++)
                {
                    font_size[i] = GLOBAL_FONT_SIZE;
                }
            }


            for (int i = 0; i < texts.Length; i++)
            {
                if (texts[i] == null)
                {
                    continue;
                }

                string s_name = GetCurrentStylesName();
                Style s = doc.Styles.AddStyle(s_name, "Normal");
                s.Font.Bold = formats[i] == TextFormat.Bold ? true : false;
                //s.Font.Underline = formats[i] == TextFormat.Underline ? true : false;
                s.Font.Italic = formats[i] == TextFormat.Italic ? true : false;
                s.Font.Color = Color.FromArgb(rgba[i][3], rgba[i][0], rgba[i][1], rgba[i][2]);
                s.Font.Size = font_size[i];
                paragraph.AddFormattedText(texts[i], s_name);
            }
            return paragraph;
        }

        private static Image DrawImage(Cell cell, string img_path, ParagraphAlignment align = ParagraphAlignment.Left, VerticalAlignment v_align = VerticalAlignment.Top)
        {
            Paragraph paragraph2 = cell.AddParagraph();
            paragraph2.Format.Alignment = align;
            cell.VerticalAlignment = v_align;
            var logoImage = paragraph2.AddImage(img_path);
            return logoImage;
        }

        private static string DrawWhiteSpaces(int num)
        {
            StringBuilder str = new StringBuilder();
            for (int i = 0; i < num; i++)
            {
                str.Append("\u00A0");
            }
            return str.ToString();
        }

        private static string DrawCheckbox(bool b)
        {
            string str = "";
            if (b == true)
            {
                //str = "[T]";
                str = "\u25A0";
            }
            else
            {
                //str = "[F]";
                str = "\u2610";
            }
            return str;
        }

        private static void CB_CDASaleTypeDecider(ref bool[] checkboxes, string choice)
        {
            if (choice == null)
                return;

            choice = choice.ToLower();
            choice = choice.Replace(" ", "-");
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            if (choice == "term")
            {
                checkboxes[0] = true;
            }

            if (choice == "mini-term")
            {
                checkboxes[1] = true;
            }

            if (choice == "spot")
            {
                checkboxes[2] = true;
            }
        }

        private static void CB_CBIFeedstockDecider(ref bool[] checkboxes, string choice)
        {
            if (choice == null)
                return;
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "crude")
            {
                checkboxes[0] = true;
            }

            if (choice == "lr")
            {
                checkboxes[1] = true;
            }

            if (choice == "vgo")
            {
                checkboxes[2] = true;
            }
        }

        private static void CB_CDASaleMethodDecider(ref bool[] checkboxes, string choice)
        {
            if (choice == null)
                return;
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "customer-tender")
            {
                checkboxes[0] = true;
            }

            if (choice == "supplier-tender")
            {
                checkboxes[1] = true;
            }

            if (choice == "private-negotiation")
            {
                checkboxes[2] = true;
            }
        }

        private static void CB_ReasonBestPriceDecider(ref bool[] checkboxes, string choice, string output0, string output1, string reason)
        {
            if (choice == null)
                return;
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "best-price")
            {
                checkboxes[0] = true;
                output0 = reason;
            }

            if (choice == "others")
            {
                checkboxes[1] = true;
                output1 = reason;
            }
        }

        private static void CB_CSIPaymentTermDecider(ref bool[] checkboxes, ref bool[] sub_checkboxes0, ref bool[] sub_checkboxes1, string choice, string sub_choice)
        {
            if (choice == null)
                return;
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "pre-payment")
            {
                checkboxes[0] = true;
            }

            if (choice == "credit")
            {
                checkboxes[1] = true;
                if (sub_choice == "0000000000001" || sub_choice == "0000000000003")
                {
                    sub_checkboxes0[1] = true;
                }
                if (sub_choice == "0000000000002" || sub_choice == "0000000000004")
                {
                    sub_checkboxes0[0] = true;
                }
                if (sub_choice == "0000000000005")
                {
                    sub_checkboxes0[2] = true;
                }

            }

            if (choice == "l/c")
            {
                checkboxes[2] = true;
                if (sub_choice == "0000000000001" || sub_choice == "0000000000003")
                {
                    sub_checkboxes1[1] = true;
                }
                if (sub_choice == "0000000000002" || sub_choice == "0000000000004")
                {
                    sub_checkboxes1[0] = true;
                }
                if (sub_choice == "0000000000005")
                {
                    sub_checkboxes1[2] = true;
                }
            }

            if (choice == "other")
            {
                checkboxes[3] = true;
            }
        }

        private static string D_PropersalReasonConverter(string choice)
        {
            if (choice == null)
                return "";
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "0000000000001" || choice == "0000000000002")
            {
                return "B/L";
            }
            else if (choice == "0000000000003" || choice == "0000000000004")
            {
                return "NOR";
            }
            else
            {
                return "";
            }
        }

        private static bool ConvertStringsTo2WaysBool(string true_condition, bool true_condition_output, string input)
        {
            if (input == true_condition)
            {
                return true_condition_output;
            }
            else
            {
                return !true_condition_output;
            }
        }

        private static string KBBL_KTONConverter(string choice)
        {
            if (choice == null)
                return "";
            choice = choice.ToLower();
            choice = choice.TrimStart();
            choice = choice.TrimEnd();
            choice = choice.Replace(" ", "-");
            if (choice == "kbbl")
                return "0000000000001";
            else if (choice == "kton")
                return "0000000000002";
            else
                return "";
        }

        public static string AddOrdinal(int num)
        {
            if (num <= 0) return num.ToString();

            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num + "th";
            }

            switch (num % 10)
            {
                case 1:
                    return num + "st";
                case 2:
                    return num + "nd";
                case 3:
                    return num + "rd";
                default:
                    return num + "th";
            }

        }

        private static void CreateStylesName()
        {
            STYLES_GEN_NAME = new List<string>();
            for (int i = 0; i < 512; i++)
            {
                STYLES_GEN_NAME.Add("STY_" + i);
            }
        }

        private static string GetCurrentStylesName()
        {
            return STYLES_GEN_NAME[CURRENT_STYLES_GEN_NAME_IDX++];
        }

        public static String genPDF(CDS_DATA cdsData)
        {
            CreateStylesName();

            if (cdsData.CDO_DATA != null)
                cdo_dat = cdsData.CDO_DATA.OrderBy(b => b.CDO_ROW_ID).ToList();

            if (cdsData.CDS_BIDDING_ITEMS != null)
                cds_bidding_items_dat = cdsData.CDS_BIDDING_ITEMS.OrderBy(b => b.CBI_ROW_ID).ToList();

            if (cdsData.CDS_CRUDE_PURCHASE_DETAIL != null)
                cds_crude_purchase_detail_dat = cdsData.CDS_CRUDE_PURCHASE_DETAIL.OrderBy(b => b.CPD_ROW_ID).ToList();

            if (cdsData.CDS_PROPOSAL_ITEMS != null)
                cds_proposal_items_dat = cdsData.CDS_PROPOSAL_ITEMS.OrderBy(b => b.CSI_ROW_ID).ToList();

            Document document = new Document();
            document.DefaultPageSetup.RightMargin = 18;
            document.DefaultPageSetup.LeftMargin = 18;
            document.DefaultPageSetup.TopMargin = 18;
            document.DefaultPageSetup.BottomMargin = 25;
            document.AddSection();

            Style style = document.Styles["Normal"];
            style.Font.Name = "Tahoma";
            style.Font.Size = GLOBAL_FONT_SIZE;

            genTable1(document, cdsData);
            genTable2(document, cdsData);
            genTable3(document, cdsData);
            genTable4(document, cdsData);
            genTable5(document, cdsData);
            genTable6(document, cdsData);
            genTable7(document, cdsData);
            genTable8(document, cdsData);
            genTable9(document, cdsData);

            PdfDocumentRenderer docRend = new PdfDocumentRenderer(true);
            docRend.Document = document;
            docRend.RenderDocument();
            //string fileName = "C:\\test\\doc.pdf";
            String n = string.Format("CDS{0}.pdf", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
            string fileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", n);
            docRend.PdfDocument.Save(fileName);
            ProcessStartInfo processInfo = new ProcessStartInfo();
            processInfo.FileName = fileName;
            Process.Start(processInfo);

            return n;
        }

        //Crude/Feedstock Sale Approval Form 
        public static void genTable1(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.0;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;
            Column column1 = table.AddColumn(getWidthUnitFromRatio(0.33));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(0.34));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(0.33));
            Row row1 = table.AddRow();

            DrawText(row1.Cells[1], "Crude/Feedstock Sale Approval Form", TextFormat.Underline, ParagraphAlignment.Center, v_align: VerticalAlignment.Center);

            string logo_path = _FN.GetPathLogo();
            Image logoImage = DrawImage(row1.Cells[2], logo_path, ParagraphAlignment.Right);
            logoImage.WrapFormat.DistanceLeft = 460;
            logoImage.Width = "2.0cm";
            logoImage.LockAspectRatio = true;

            /////////////////////////////////////////////////////////////////

            doc.LastSection.Add(table);
        }

        //Requested by :
        public static void genTable2(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;
            Column column1 = table.AddColumn(getWidthUnitFromRatio(4.0 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(6.0 / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
            Row row1 = table.AddRow();

            row1.Cells[1].Borders.Top.Visible = false;

            string name = cdsData.CDA_CREATED_BY != null ? cdsData.CDA_CREATED_BY : "";
            DrawText(row1.Cells[0], "Requested by : " + name, TextFormat.Bold);
            string date = cdsData.CDA_DOC_DATE != null ? cdsData.CDA_DOC_DATE?.ToString("dd MMM yyyy") : "";
            DrawText(row1.Cells[2], "Sale date :  " + date, TextFormat.Bold, ParagraphAlignment.Center);
            /////////////////////////////////////////////////////////////////

            doc.LastSection.Add(table);
        }

        //Crude Purchase Reference no. : 
        public static void genTable3(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;
            Column column1 = table.AddColumn(getWidthUnitFromRatio(7.0 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            row1.Cells[1].MergeRight = 1;

            /////////////////////////////////////////////////////////////////
            string crude_ref_no = cdsData.CDA_FORM_ID != null ? cdsData.CDA_FORM_ID : "";
            DrawText(row1.Cells[0], "Sale document no. : " + crude_ref_no, TextFormat.Bold);

            bool[] checkboxes0 = new bool[3];
            checkboxes0[0] = false;
            checkboxes0[1] = false;
            checkboxes0[2] = false;
            if (cds_bidding_items_dat != null)
            {
                CB_CBIFeedstockDecider(ref checkboxes0, cds_bidding_items_dat.ElementAt(0).CBI_FEEDSTOCK);
            }
            DrawMultipleText(doc, row1.Cells[1], new string[]{"Feedstock : " ,
                 DrawCheckbox(checkboxes0[0]), " CRUDE   ",
                DrawCheckbox(checkboxes0[1]), " LR   ",
                DrawCheckbox(checkboxes0[2]), " VGO   "}
                , new TextFormat[] { TextFormat.Bold, TextFormat.NotBold, TextFormat.NotBold, TextFormat.NotBold, TextFormat.NotBold, TextFormat.NotBold, TextFormat.NotBold }
                , font_size: new double[] { GLOBAL_FONT_SIZE, checkboxes0[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes0[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes0[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE });
            /////////////////////////////////////////////////////////////////
            string crude_name = cds_proposal_items_dat != null ?
                (checkboxes0[0] == true ? cds_proposal_items_dat.ElementAt(0).MT_MATERIALS.MET_MAT_DES_ENGLISH : cds_bidding_items_dat.ElementAt(0).CBI_MATERIAL_NAME_OTHER)
                : "";
            CRUDE_NAME = crude_name;
            CRUDE_NAME_HEADER = checkboxes0[0] == true ? "Crude Name" : "Name";
            string nation = cds_proposal_items_dat.ElementAt(0).MT_MATERIALS != null ? cds_proposal_items_dat.ElementAt(0).MT_MATERIALS.MET_ORIG_CRTY : "";
            string tpc_plan = cdsData != null ? cdsData.CDA_TPC_PLAN?.ToString("MMM yyyy") : "";
            DrawText(row2.Cells[0], CRUDE_NAME_HEADER + " : " + crude_name + "\n"
                + "Origin : " + nation + "\n"
                + "TPC Plan : " + tpc_plan
                , TextFormat.Bold);

            bool[] checkboxes1 = new bool[3];
            checkboxes1[0] = false;
            checkboxes1[1] = false;
            checkboxes1[2] = false;
            if (cdsData != null)
            {
                if (cdsData.CDS_MT_SALE_TYPE != null)
                    CB_CDASaleTypeDecider(ref checkboxes1, cdsData.CDA_SALE_TYPE);
            }
            DrawMultipleText(doc, row2.Cells[1], new string[] {DrawCheckbox(checkboxes1[0]) , " Term" + "\n"
                , DrawCheckbox(checkboxes1[1]) , " Mini term" + "\n"
                , DrawCheckbox(checkboxes1[2]) , " Spot" }
                , font_size: new double[] { checkboxes1[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes1[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes1[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE });

            bool[] checkboxes2 = new bool[3];
            checkboxes2[0] = false;
            checkboxes2[1] = false;
            checkboxes2[2] = false;
            if (cdsData != null)
            {
                if (cdsData.CDS_MT_SALE_METHOD != null)
                    CB_CDASaleMethodDecider(ref checkboxes2, cdsData.CDA_SALE_METHOD);
            }
            DrawMultipleText(doc, row2.Cells[2], new string[]{ DrawCheckbox(checkboxes2[0]) ," Customer tender" + "\n"
                , DrawCheckbox(checkboxes2[1]) , " Supplier tender" + "\n"
                , DrawCheckbox(checkboxes2[2]) , " Private negotiation" }
                , v_align: VerticalAlignment.Center
                , font_size: new double[] { checkboxes2[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes2[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes2[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE });
            /////////////////////////////////////////////////////////////////

            doc.LastSection.Add(table);
        }

        //Brief Market Situation / Reason for selling 
        public static void genTable4(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;
            Column column1 = table.AddColumn(getWidthUnitFromRatio(1));
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();

            row1.Cells[0].Borders.Bottom.Visible = false;
            row1.Cells[0].Borders.Top.Visible = false;

            /////////////////////////////////////////////////////////////////
            string description = cdsData.CDA_BRIEF != null ? cdsData.CDA_BRIEF : "";
            DrawText(row1.Cells[0], "Brief Market Situlation / Reason for selling", TextFormat.Bold);
            HtmlRemoval htmlRemove = new HtmlRemoval();
            Paragraph p = DrawText(row2.Cells[0], "", v_align: VerticalAlignment.Top);
            htmlRemove.ValuetoParagraph(description, p);

            /////////////////////////////////////////////////////////////////

            doc.LastSection.Add(table);
        }

        //Crude Purchase Detail (For Reference)
        public static void genTable5(Document doc, CDS_DATA cdsData)
        {

            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;

            //int data_row_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'"); //Count from db
            int data_row_num = cdsData.CDS_CRUDE_PURCHASE_DETAIL.Count();

            double incoterm_percent_view = 1.0 * 20.0 / 100.0;
            double incoterm_result_view = 1.0 - incoterm_percent_view;
            double supplier_result_view = 1.5 + incoterm_percent_view;

            Column column1 = table.AddColumn(getWidthUnitFromRatio(1.3 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(1.2 / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(supplier_result_view / 13));
            Column column4 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column5 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column6 = table.AddColumn(getWidthUnitFromRatio(incoterm_result_view / 13));
            Column column7 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column8 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column9 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column10 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column11 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column12 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));

            /////////////////////////////////////////////////////////////////
            //Static row
            Row row_header = table.AddRow();
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();

            //Vertical merge
            row1.Cells[0].MergeDown = 3;
            row1.Cells[1].MergeDown = 3;
            row1.Cells[2].MergeDown = 3;
            row1.Cells[3].MergeDown = 3;
            row1.Cells[4].MergeDown = 3;

            row2.Cells[5].MergeDown = 2;
            row2.Cells[6].MergeDown = 2;
            row2.Cells[7].MergeDown = 2;
            row2.Cells[8].MergeDown = 2;
            row2.Cells[9].MergeDown = 2;

            row1.Cells[10].MergeDown = 3;
            row1.Cells[11].MergeDown = 3;

            //Horizontal Merge
            row_header.Cells[0].MergeRight = 11;
            row1.Cells[5].MergeRight = 4;

            /////////////////////////////////////////////////////////////////

            //Dynamic row
            List<Row> dynamic_row = new List<Row>();
            for (int j = 0; j < data_row_num; j++)
            {
                dynamic_row.Add(table.AddRow());
            }

            /////////////////////////////////////////////////////////////////
            DrawText(row_header.Cells[0], "Crude Purchase Detail (For Reference)", TextFormat.Bold);
            DrawText(row1.Cells[0], "Purchase no.", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[1], CRUDE_NAME_HEADER, TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[2], "Supplier", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[3], "Contact Person", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            string quantity_unit_txt = cdsData.CDS_CRUDE_PURCHASE_DETAIL.ElementAt(0).CDP_DATA.CDP_OFFER_ITEMS.ElementAt(0).COI_QUANTITY_UNIT;
            DrawText(row1.Cells[4], "Quantity (" + quantity_unit_txt + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            string margin_unit_txt_tmp = cdsData.CDS_CRUDE_PURCHASE_DETAIL.ElementAt(0).CDP_DATA.CDP_OFFER_ITEMS.ElementAt(0).COI_QUANTITY_UNIT;
            string margin_unit_txt_tmp2 = KBBL_KTONConverter(margin_unit_txt_tmp);
            string margin_unit_txt = (string)Query<string>("SELECT VALUE FROM CDS_MT_PRICE_PER_QAUNTITY_UNIT WHERE ID = '" + margin_unit_txt_tmp2 + "'");
            DrawText(row1.Cells[5], "Offer Price or Premium (" + margin_unit_txt + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[10], "Rank/ Round ", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[11], "Margin vs LP (" + margin_unit_txt + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

            DrawText(row2.Cells[5], "Inco-terms", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[6], "Purchase Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[7], "Market/ Source", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[8], "LP Max Purchase Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[9], "Benchmark Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

            int k = 0;
            int average_rows_counter = 0;
            int another_dynamic_rows_num = 0;
            double average = 0;
            foreach (Row r in dynamic_row)
            {
                var cdp_offer_items_dat = cdsData.CDS_CRUDE_PURCHASE_DETAIL.OrderBy(b => b.CPD_ROW_ID).ElementAt(k).CDP_DATA.CDP_OFFER_ITEMS.OrderBy(b => b.COI_ROW_ID).ToList();
                var cdp_dat = cdsData.CDS_CRUDE_PURCHASE_DETAIL.OrderBy(b => b.CPD_ROW_ID).ToList().ElementAt(k).CDP_DATA;
                for (int kk = 0, kk_count = 0; kk_count < cdp_offer_items_dat.Count(); kk_count++)
                {
                    //Check final flag
                    string final_flag_txt = cdp_offer_items_dat.ElementAt(kk_count).COI_FINAL_FLAG;
                    bool final_flag = ConvertStringsTo2WaysBool("Y", true, final_flag_txt);
                    if (final_flag == false)
                    {
                        continue;
                    }

                    Row another_dynamic_row = null;
                    if (kk > 0)
                    {
                        another_dynamic_row = table.AddRow(); //If data in CDP_OFFER_ITEMS have more than 1
                        another_dynamic_rows_num++;
                    }

                    var elem = cdp_offer_items_dat.ElementAt(kk_count);
                    string purchase_no_txt = cdp_dat != null && cdp_dat.CDA_PURCHASE_NO != null ? cdp_dat.CDA_PURCHASE_NO : "";
                    string crude_txt = CRUDE_NAME;
                    string supplier_txt = cdp_offer_items_dat != null && elem.MT_VENDOR.VND_NAME1 != null ? elem.MT_VENDOR.VND_NAME1 + (cdp_offer_items_dat != null && elem.MT_VENDOR.VND_NAME2 != null ? " " + elem.MT_VENDOR.VND_NAME2 : "") : "";
                    string contact_person_txt = cdp_offer_items_dat != null && elem.COI_CONTACT_PERSON != null ? elem.COI_CONTACT_PERSON : "";
                    string quantity_txt = "";
                    if (cdp_offer_items_dat != null && elem.COI_QUANTITY != null && elem.COI_QUANTITY_MAX != null)
                    {
                        double q0 = double.Parse(elem.COI_QUANTITY, System.Globalization.CultureInfo.InvariantCulture);
                        double q1 = double.Parse(elem.COI_QUANTITY_MAX, System.Globalization.CultureInfo.InvariantCulture);
                        quantity_txt = q0 == q1 ? FormatNumber(q1, forceShowDecimal: false) : FormatNumber(q0, forceShowDecimal: false) + " - " + FormatNumber(q1, forceShowDecimal: false);
                    }
                    string incoterms_txt = cdp_offer_items_dat != null && elem.COI_INCOTERMS != null ? elem.COI_INCOTERMS : "";
                    var cdp_round_items_dat = elem.CDP_ROUND_ITEMS.OrderBy(b => b.CRI_ROW_ID);
                    var cdp_round_dat = cdp_round_items_dat.ElementAt(0).CDP_ROUND.OrderBy(b => b.CRD_ROW_ID);
                    double last_round = double.MinValue;
                    for (int crd_round = cdp_round_dat.Count() - 1; crd_round >= 0; crd_round--)
                    {
                        if (cdp_round_dat.ElementAt(crd_round).CRD_ROUND_VALUE != null)
                        {
                            last_round = double.Parse(cdp_round_dat.ElementAt(crd_round).CRD_ROUND_VALUE, System.Globalization.CultureInfo.InvariantCulture);
                            break;
                        }
                    }
                    string purchase_price_txt = cdp_round_dat != null && last_round != double.MinValue ? FormatNumber(last_round) : "";
                    if (last_round != double.MinValue)
                        average += last_round;
                    string market_source_txt = cdp_offer_items_dat != null && elem.COI_MARKET_SOURCE != null ? FormatNumber(elem.COI_MARKET_SOURCE) : "";
                    string lp_max_purchase_txt = cdp_offer_items_dat != null && elem.COI_LATEST_LP_PLAN_PRICE != null ? FormatNumber(elem.COI_LATEST_LP_PLAN_PRICE) : "";
                    string benchmark_txt = cdp_offer_items_dat != null && elem.COI_BECHMARK_PRICE != null ? elem.COI_BECHMARK_PRICE : "";
                    string rank_round_txt = cdp_offer_items_dat != null && elem.COI_RANK_ROUND != null ? elem.COI_RANK_ROUND : "";
                    string margin_vs_lp_txt = cdp_offer_items_dat != null && elem.COI_MARGIN_LP != null ? FormatNumber(elem.COI_MARGIN_LP) : "";
                    Row alt_name_r = kk == 0 ? r : another_dynamic_row;
                    DrawText(alt_name_r.Cells[0], purchase_no_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    if (kk == 0)
                        DrawText(r.Cells[1], crude_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[2], supplier_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[3], contact_person_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[4], quantity_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[5], incoterms_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[6], purchase_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[7], market_source_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[8], lp_max_purchase_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[9], benchmark_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[10], rank_round_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    DrawText(alt_name_r.Cells[11], margin_vs_lp_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    average_rows_counter++;
                    kk++;
                }

                k++;
            }

            //Merge down on crude name column
            dynamic_row[0].Cells[1].MergeDown = data_row_num - 1 + another_dynamic_rows_num;
            /////////////////////////////////////////////////////////////////

            Row row_footer = table.AddRow();
            row_footer.Cells[0].MergeRight = 4;
            row_footer.Cells[7].MergeRight = 4;

            DrawText(row_footer.Cells[5], "Average", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row_footer.Cells[6], FormatNumber((average / average_rows_counter), 4), TextFormat.Bold, ParagraphAlignment.Center, rgba: new byte[] { 66, 134, 244, 255 });

            doc.LastSection.Add(table);
        }

        //Negotiation Summary
        public static void genTable6(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;

            //int data_row_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'"); //Count from db
            int data_row_num = cdsData.CDS_BIDDING_ITEMS.Count();

            int offer_round_num = cds_bidding_items_dat.ElementAt(0).CDS_ROUND.Count();

            double incoterm_percent_view = 1.0 * 20.0 / 100.0;
            double incoterm_result_view = 1.0 - incoterm_percent_view;
            double customer_result_view = 1.5 + incoterm_percent_view;
            double margin_purchasing_price_view = 1 * 50.0 / 100.0;
            double margin_p0 = 1.0 - margin_purchasing_price_view;
            double margin_p1 = 1.0 - margin_purchasing_price_view;

            Column column1 = table.AddColumn(getWidthUnitFromRatio(1.3 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(1.2 / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(customer_result_view / 13));
            Column column4 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column5 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column6 = table.AddColumn(getWidthUnitFromRatio(incoterm_result_view / 13));
            string[] idx_order_txt = new string[offer_round_num];
            for (int i = 0; i < offer_round_num; i++)
            {
                idx_order_txt[i] = AddOrdinal(i + 1);
            }
            for (int i = 0; i < offer_round_num; i++)
            {
                Column column_dynamic;
                if (offer_round_num == 1) { column_dynamic = table.AddColumn(getWidthUnitFromRatio((((margin_p0 + margin_p1) / 2.0) + 2.0) / 13)); }
                else if (offer_round_num == 2) { column_dynamic = table.AddColumn(getWidthUnitFromRatio((((margin_p0 + margin_p1) / 2.0) + 1.0) / 13)); }
                else { column_dynamic = table.AddColumn(getWidthUnitFromRatio(((margin_p0 + margin_p1 + 2.0) / offer_round_num) / 13)); }
            }
            Column column7 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column8 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column9 = table.AddColumn(getWidthUnitFromRatio(margin_p0 / 13));
            Column column10 = table.AddColumn(getWidthUnitFromRatio(margin_p1 / 13));

            /////////////////////////////////////////////////////////////////
            //Static row
            Row row_header = table.AddRow();
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();

            //Vertical merge
            row1.Cells[0].MergeDown = 3;
            row1.Cells[1].MergeDown = 3;
            row1.Cells[2].MergeDown = 3;
            row1.Cells[3].MergeDown = 3;
            row1.Cells[4].MergeDown = 3;

            row2.Cells[5].MergeDown = 2;
            int offer_round_dynamic_col = 0;
            for (; offer_round_dynamic_col < offer_round_num; offer_round_dynamic_col++)
            {
                row2.Cells[6 + offer_round_dynamic_col].MergeDown = 1;
            }
            row2.Cells[7 + offer_round_dynamic_col - 1].MergeDown = 2;
            row2.Cells[8 + offer_round_dynamic_col - 1].MergeDown = 2;

            row1.Cells[9 + offer_round_dynamic_col - 1].MergeDown = 3;
            row1.Cells[10 + offer_round_dynamic_col - 1].MergeDown = 3;

            //Horizontal Merge
            row_header.Cells[0].MergeRight = 10 + offer_round_num - 1;
            row1.Cells[1].MergeRight = 1;
            row1.Cells[5].MergeRight = 3 + offer_round_num - 1;
            row2.Cells[6].MergeRight = offer_round_num - 1;
            row2.Cells[8 + offer_round_num].MergeRight = 1;
            row1.Cells[8 + offer_round_num].MergeRight = 1;
            /////////////////////////////////////////////////////////////////

            //Dynamic row
            List<Row> dynamic_row = new List<Row>();
            for (int j = 0; j < data_row_num; j++)
            {
                dynamic_row.Add(table.AddRow());
                dynamic_row[j].Cells[1].MergeRight = 1;
                dynamic_row[j].Cells[9 + offer_round_num - 1].MergeRight = 1;
            }
            dynamic_row[0].Cells[0].MergeDown = dynamic_row.Count - 1;
            /////////////////////////////////////////////////////////////////
            DrawText(row_header.Cells[0], "Negotiation Summary", TextFormat.Bold);
            DrawText(row1.Cells[0], CRUDE_NAME_HEADER, TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[1], "Customer", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row1.Cells[3], "Contact Person", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            string quantity_unit_txt = cds_bidding_items_dat.ElementAt(0).CBI_QUANTITY_UNIT;
            DrawText(row1.Cells[4], "Quantity (" + quantity_unit_txt + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            string unit_margin_vs_purchase = cds_bidding_items_dat.ElementAt(0).CDS_MT_PRICE_PER_QAUNTITY_UNIT.VALUE;
            DrawText(row1.Cells[5], "Offer Price or Premium (" + unit_margin_vs_purchase + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

            DrawText(row2.Cells[5], "Inco-terms", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[6], "Offer / Negotiation round", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[7 + (offer_round_num - 1)], "Market/ Source", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            DrawText(row2.Cells[8 + (offer_round_num - 1)], "Benchmark Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

            DrawText(row1.Cells[9 + (offer_round_num - 1)], "Margin vs Purchasing Price (" + unit_margin_vs_purchase + ")", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

            for (int i = 0; i < offer_round_num; i++)
            {
                DrawText(row4.Cells[6 + i], idx_order_txt[i], TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
            }

            int k = 0;
            foreach (Row r in dynamic_row)
            {
                var elem = cds_bidding_items_dat.ElementAt(k);

                string crude_name_txt = CRUDE_NAME;
                //CUSTOMER_NAME
                string customer_txt = "?";
                for (int i = 0; i < cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.Count(); i++)
                {
                    if (cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL != null)
                    {
                        for (int j = 0; j < cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.Count(); j++)
                        {
                            if (cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_SYSTEM == "CDS" &&
                                cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_TYPE == "OTHER" &&
                            cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_FK_CUST_CONTROL_ID ==
                            cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_ROW_ID)
                            {
                                customer_txt = cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_1 + " " +
                            cds_bidding_items_dat.ElementAt(k).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_2;
                                break;
                            }
                        }
                    }
                }
                string contact_person_txt = cds_bidding_items_dat != null && elem.CBI_CONTACT_PERSON != null ? elem.CBI_CONTACT_PERSON : "";
                string quantity_txt = "";
                if (cds_bidding_items_dat != null && elem.CBI_QUANTITY_MIN != null && elem.CBI_QUANTITY_MAX != null)
                {
                    double q0 = double.Parse(elem.CBI_QUANTITY_MIN.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    double q1 = double.Parse(elem.CBI_QUANTITY_MAX.ToString(), System.Globalization.CultureInfo.InvariantCulture);

                    quantity_txt = q0 == q1 ? FormatNumber(q1, forceShowDecimal: false) : FormatNumber(q0, forceShowDecimal: false) + " - " + FormatNumber(q1, forceShowDecimal: false);
                }
                string incoterms_txt = cds_bidding_items_dat != null && elem.CBI_INCOTERM != null ? elem.CBI_INCOTERM : "";
                string[] offer_val_txt = new string[offer_round_num];
                for (int ii = 0; ii < offer_round_num; ii++)
                {
                    offer_val_txt[ii] = FormatNumber(cds_bidding_items_dat.ElementAt(0).CDS_ROUND.ElementAt(ii).CRN_VALUE);
                }
                string market_source_txt = cds_bidding_items_dat != null && elem.CBI_MARKET_PRICE != null ? FormatNumber(elem.CBI_MARKET_PRICE) : "";
                string benchmark_txt = cds_bidding_items_dat != null && elem.CBI_BENCHMARK_PRICE != null ? elem.CBI_BENCHMARK_PRICE : "";
                string margin_vs_purchasing_price_txt = cds_bidding_items_dat != null && elem.CBI_MARGIN != null ? FormatNumber(elem.CBI_MARGIN, 3) : "";

                if (k == 0)
                    DrawText(r.Cells[0], crude_name_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                DrawText(r.Cells[1], customer_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                DrawText(r.Cells[3], contact_person_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                DrawText(r.Cells[4], quantity_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                DrawText(r.Cells[5], incoterms_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                offer_round_dynamic_col = 0;
                for (; offer_round_dynamic_col < offer_round_num; offer_round_dynamic_col++)
                {
                    DrawText(r.Cells[6 + offer_round_dynamic_col], offer_val_txt[offer_round_dynamic_col], align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                }
                DrawText(r.Cells[7 + offer_round_dynamic_col - 1], market_source_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                DrawText(r.Cells[8 + offer_round_dynamic_col - 1], benchmark_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                if (cds_bidding_items_dat != null && elem.CBI_MARGIN != null)
                {
                    if (elem.CBI_MARGIN >= 0)
                        DrawText(r.Cells[9 + offer_round_dynamic_col - 1], margin_vs_purchasing_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
                    else
                        DrawText(r.Cells[9 + offer_round_dynamic_col - 1], margin_vs_purchasing_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center, rgba: new byte[] { 255, 0, 0, 255 });
                }
                k++;
            }


            doc.LastSection.Add(table);
            /////////////////////////////////////////////////////////////////

            //Draw Sub Table 
            for (int j = 0; j < data_row_num; j++)
            {
                genSubTable6_0(doc, cdsData, j);
            }

        }

        //Awarded Customer :
        private static void genSubTable6_0(Document doc, CDS_DATA cdsData, int elementAt)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;

            Column column1 = table.AddColumn(getWidthUnitFromRatio(4.0 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio((1.5 + 2.0) / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio((1.5 + 1.0) / 13));
            Column column4 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));

            /////////////////////////////////////////////////////////////////
            //Static row
            Row row_header = table.AddRow();
            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();
            Row row4 = table.AddRow();
            Row row5 = table.AddRow();
            Row row6 = table.AddRow();
            Row row7 = table.AddRow();
            Row row8 = table.AddRow();
            Row row9 = table.AddRow();

            //Vertical merge
            row2.Cells[2].MergeDown = 2;
            row2.Cells[3].MergeDown = 2;

            row5.Cells[0].MergeDown = 4;
            row5.Cells[1].MergeDown = 4;
            row5.Cells[2].MergeDown = 4;
            row5.Cells[3].MergeDown = 4;

            //Horizontal merge
            row_header.Cells[0].MergeRight = 3;
            row1.Cells[2].MergeRight = 1;
            row2.Cells[2].MergeRight = 1;
            row5.Cells[1].MergeRight = 1;

            row1.Cells[2].Borders.Bottom.Visible = false;
            row2.Cells[2].Borders.Top.Visible = false;

            /////////////////////////////////////////////////////////////////
            //CUSTOMER_NAME
            string customer_txt = "?";
            for (int i = 0; i < cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.Count(); i++)
            {
                if (cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL != null)
                {
                    for (int j = 0; j < cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.Count(); j++)
                    {
                        if (cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_SYSTEM == "CDS" &&
                            cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_TYPE == "OTHER" &&
                            cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_FK_CUST_CONTROL_ID ==
                            cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_ROW_ID)
                        {
                            customer_txt = cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_1 + " " +
                        cds_bidding_items_dat.ElementAt(elementAt).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_2;
                            break;
                        }
                    }
                }
            }
            string contract_period_txt = "";
            if (cds_proposal_items_dat != null)
            {
                DateTime? d0 = cds_proposal_items_dat.ElementAt(elementAt).CSI_CONTRACT_PERIOD_FROM;
                DateTime? d1 = cds_proposal_items_dat.ElementAt(elementAt).CSI_CONTRACT_PERIOD_TO;
                contract_period_txt = (d0 != null && d1 != null) ? d0?.ToString("dd MMM yyyy") + " - " + d1?.ToString("dd MMM yyyy") : "-";
            }
            string loading_period_txt = "";
            if (cds_proposal_items_dat != null)
            {
                DateTime? d0 = cds_proposal_items_dat.ElementAt(elementAt).CSI_LOADING_PERIOD_FROM;
                DateTime? d1 = cds_proposal_items_dat.ElementAt(elementAt).CSI_LOADING_PERIOD_TO;
                loading_period_txt = (d0 != null && d1 != null) ? d0?.ToString("dd MMM yyyy") + " - " + d1?.ToString("dd MMM yyyy") : "-";
            }
            string discharging_period_txt = "";
            if (cds_proposal_items_dat != null)
            {
                DateTime? d0 = cds_proposal_items_dat.ElementAt(elementAt).CSI_DISCHARGING_PERIOD_FROM;
                DateTime? d1 = cds_proposal_items_dat.ElementAt(elementAt).CSI_DISCHARGING_PERIOD_TO;
                discharging_period_txt = (d0 != null && d1 != null) ? d0?.ToString("dd MMM yyyy") + " - " + d1?.ToString("dd MMM yyyy") : "-";
            }
            string gt_c_txt = cds_proposal_items_dat != null ? cds_proposal_items_dat.ElementAt(elementAt).CSI_GT_AND_C : "";
            string any_other_special_txt = cds_proposal_items_dat != null && cds_proposal_items_dat.ElementAt(0).CSI_OTHER_SPECIAL_TERM != null
                ? cds_proposal_items_dat.ElementAt(0).CSI_OTHER_SPECIAL_TERM : "";

            DrawText(row_header.Cells[0], "Awarded Customer : " + customer_txt, TextFormat.Bold);
            DrawText(row1.Cells[0], "Contract Period", TextFormat.Bold);
            DrawText(row1.Cells[1], contract_period_txt);
            DrawText(row1.Cells[2], "Any other special terms and conditions :", TextFormat.Bold);

            DrawText(row2.Cells[0], "Loading Period", TextFormat.Bold);
            DrawText(row2.Cells[1], loading_period_txt);
            DrawText(row2.Cells[2], any_other_special_txt);

            DrawText(row3.Cells[0], "Discharging Period", TextFormat.Bold);
            DrawText(row3.Cells[1], discharging_period_txt);

            DrawText(row4.Cells[0], "GT&C", TextFormat.Bold);
            DrawText(row4.Cells[1], gt_c_txt);

            DrawText(row5.Cells[0], "Payment Term :", TextFormat.Bold, v_align: VerticalAlignment.Top);
            bool[] checkboxes0 = new bool[4];
            checkboxes0[0] = false;
            checkboxes0[1] = false;
            checkboxes0[2] = false;
            checkboxes0[3] = false;
            bool[] sub0_checkboxes0_1 = new bool[3];
            sub0_checkboxes0_1[0] = false;
            sub0_checkboxes0_1[1] = false;
            sub0_checkboxes0_1[2] = false;
            bool[] sub0_checkboxes0_2 = new bool[3];
            sub0_checkboxes0_2[0] = false;
            sub0_checkboxes0_2[1] = false;
            sub0_checkboxes0_2[2] = false;
            string others_sub_description = cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM_OTHER != null ? cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM_OTHER : "-";
            string payment_day_txt = cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM_DETAIL != null ? cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM_DETAIL : "-";
            string payment_day_txt_credit = "-";
            string payment_day_txt_lc = "-";
            string bl_or_nor_txt = cds_proposal_items_dat != null && cds_proposal_items_dat.ElementAt(0).CSI_PAYMENT_TERM_SUB_DETAIL != null
                ? D_PropersalReasonConverter(cds_proposal_items_dat.ElementAt(0).CSI_PAYMENT_TERM_SUB_DETAIL) : "B/L";
            if (cds_proposal_items_dat != null)
                CB_CSIPaymentTermDecider(ref checkboxes0, ref sub0_checkboxes0_1, ref sub0_checkboxes0_2, cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM
                    , cds_proposal_items_dat.ElementAt(elementAt).CSI_PAYMENT_TERM_SUB_DETAIL);
            if (checkboxes0[1] == true)
                payment_day_txt_credit = payment_day_txt;
            if (checkboxes0[2] == true)
                payment_day_txt_lc = payment_day_txt;

            DrawMultipleText(doc, row5.Cells[1], new string[] {DrawCheckbox(checkboxes0[0]) , " Pre-payment" + "\n"
                , DrawCheckbox(checkboxes0[1]) , " Credit "+ payment_day_txt_credit + " days from "+ bl_or_nor_txt + " date " + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_1[0]) , " " + bl_or_nor_txt + " date = day zero" + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_1[1]) , " " + bl_or_nor_txt + " date = day one" + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_1[2]) , " Others: "+ others_sub_description }
                , font_size: new double[] { checkboxes0[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes0[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_1[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_1[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_1[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE });
            DrawMultipleText(doc, row5.Cells[3], new string[]{ DrawCheckbox(checkboxes0[2]) , " L/C "+ payment_day_txt_lc + " days from " + bl_or_nor_txt + " date" + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_2[0]) , " " + bl_or_nor_txt + " date = day zero" + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_2[1]) , " " + bl_or_nor_txt + " date = day one" + "\n"
                + DrawWhiteSpaces(14) , DrawCheckbox(sub0_checkboxes0_2[2]) , " Others: "+ others_sub_description + "\n"
                , DrawCheckbox(checkboxes0[3]) , " Others" }
                , font_size: new double[] { checkboxes0[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_2[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_2[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , sub0_checkboxes0_2[2] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE , checkboxes0[3] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                , GLOBAL_FONT_SIZE });

            //Coloring
            row_header.Cells[0].Shading.Color = Color.FromRgb(211, 211, 211);

            doc.LastSection.Add(table);
        }

        //Proposal
        public static void genTable7(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;

            //int data_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'"); //Count from db
            int data_num = cds_bidding_items_dat.Count();
            int data_line_per_num = 5;
            int show_awarded_succeed = 0;

            Column column1 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column2 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
            Column column3 = table.AddColumn(getWidthUnitFromRatio(2.0 / 13));
            Column column4 = table.AddColumn(getWidthUnitFromRatio(0.5 / 13));
            Column column5 = table.AddColumn(getWidthUnitFromRatio(0.5 / 13));
            Column column6 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column7 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column8 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
            Column column9 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));

            /////////////////////////////////////////////////////////////////
            //Dynamic rows
            List<Row> dynamic_rows = new List<Row>();
            for (int i = 0; i < data_num; i++)
            {
                string show_award_query = cds_bidding_items_dat.ElementAt(i).CBI_IS_AWARDED;
                bool show_award = true;//ConvertStringsTo2WaysBool("Y", true, show_award_query);
                if (show_award == false)
                    continue;


                for (int j = 0; j < data_line_per_num; j++)
                {
                    dynamic_rows.Add(table.AddRow());
                }

                //Horizontal merge
                dynamic_rows[0 + (i * data_line_per_num)].Cells[2].MergeRight = 6;
                dynamic_rows[1 + (i * data_line_per_num)].Cells[3].MergeRight = 5;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[3].MergeRight = 5;
                dynamic_rows[3 + (i * data_line_per_num)].Cells[2].MergeRight = 3;
                dynamic_rows[3 + (i * data_line_per_num)].Cells[6].MergeRight = 1;
                dynamic_rows[4 + (i * data_line_per_num)].Cells[2].MergeRight = 3;
                dynamic_rows[4 + (i * data_line_per_num)].Cells[6].MergeRight = 1;

                //Clear bottom/top line
                for (int j = 0; j < 8; j++)
                {
                    dynamic_rows[0 + (i * data_line_per_num)].Cells[1 + j].Borders.Bottom.Visible = false;
                    dynamic_rows[1 + (i * data_line_per_num)].Cells[1 + j].Borders.Top.Visible = false;
                    dynamic_rows[1 + (i * data_line_per_num)].Cells[1 + j].Borders.Bottom.Visible = false;
                    dynamic_rows[2 + (i * data_line_per_num)].Cells[1 + j].Borders.Top.Visible = false;
                    dynamic_rows[2 + (i * data_line_per_num)].Cells[1 + j].Borders.Bottom.Visible = false;
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[1 + j].Borders.Top.Visible = false;
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[1 + j].Borders.Bottom.Visible = false;
                    dynamic_rows[4 + (i * data_line_per_num)].Cells[1 + j].Borders.Top.Visible = false;
                }

                dynamic_rows[1 + (i * data_line_per_num)].Cells[2].Borders.Right.Visible = false;
                dynamic_rows[1 + (i * data_line_per_num)].Cells[2].Borders.Bottom.Visible = false;
                dynamic_rows[1 + (i * data_line_per_num)].Cells[3].Borders.Left.Visible = false;
                dynamic_rows[1 + (i * data_line_per_num)].Cells[3].Borders.Bottom.Visible = false;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[2].Borders.Right.Visible = false;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[2].Borders.Top.Visible = false;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[3].Borders.Left.Visible = false;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[3].Borders.Top.Visible = false;

                for (int j = 0; j < (3) + 1; j++)
                {
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[2 + j].Borders.Right.Visible = false;
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[3 + j].Borders.Left.Visible = false;
                    dynamic_rows[4 + (i * data_line_per_num)].Cells[2 + j].Borders.Right.Visible = false;
                    dynamic_rows[4 + (i * data_line_per_num)].Cells[3 + j].Borders.Left.Visible = false;
                }

                for (int j = 0; j < (1) + 1; j++)
                {
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[6 + j].Borders.Right.Visible = false;
                    dynamic_rows[3 + (i * data_line_per_num)].Cells[7 + j].Borders.Left.Visible = false;
                    dynamic_rows[4 + (i * data_line_per_num)].Cells[6 + j].Borders.Right.Visible = false;
                    dynamic_rows[4 + (i * data_line_per_num)].Cells[7 + j].Borders.Left.Visible = false;
                }

                dynamic_rows[1 + (i * data_line_per_num)].Cells[1].Borders.Bottom.Visible = false;
                dynamic_rows[2 + (i * data_line_per_num)].Cells[1].Borders.Top.Visible = false; ;

                show_awarded_succeed++;
            }

            //Interrupt intersection
            if (show_awarded_succeed <= 0)
            {
                doc.LastSection.Add(table);
                return;
            }

            //Vertical merge
            dynamic_rows[0].Cells[0].MergeDown = dynamic_rows.Count - 1;

            //Coloring
            dynamic_rows[0].Cells[0].Shading.Color = Color.FromRgb(192, 192, 192);

            ///////////////////////////////////////////////////////////////////
            DrawText(dynamic_rows[0].Cells[0], "Proposal", TextFormat.Bold, ParagraphAlignment.Center, v_align: VerticalAlignment.Top);
            for (int ii = 0; ii < data_num; ii++)
            {
                //string show_award_query = (string)Query<string>("select CBI_IS_AWARDED from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'");
                string show_award_query = cds_bidding_items_dat.ElementAt(ii).CBI_IS_AWARDED;
                bool show_award = true;// ConvertStringsTo2WaysBool("Y",true,show_award_query);
                if (show_award == true)
                {
                    //CUSTOMER_NAME
                    string customer_txt = "?";
                    for (int i = 0; i < cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.Count(); i++)
                    {
                        if (cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL != null)
                        {
                            for (int j = 0; j < cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.Count(); j++)
                            {
                                if (cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_SYSTEM == "CDS" &&
                                    cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_TYPE == "OTHER" &&
                            cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MT_CUST_CONTROL.ElementAt(j).MCR_FK_CUST_CONTROL_ID ==
                            cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_ROW_ID)
                                {
                                    customer_txt = cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_1 + " " +
                                cds_bidding_items_dat.ElementAt(ii).MT_CUST.MT_CUST_DETAIL.ElementAt(i).MCD_NAME_2;
                                    break;
                                }
                            }
                        }
                    }

                    string award_to_str = customer_txt;
                    bool[] checkboxes0 = new bool[2];
                    checkboxes0[0] = false;
                    checkboxes0[1] = false;
                    string reason_best_price_str = "";
                    string reason_others_str = "";

                    if (cds_proposal_items_dat != null)
                        CB_ReasonBestPriceDecider(ref checkboxes0, cds_proposal_items_dat.ElementAt(ii).CSI_REASON, reason_best_price_str, reason_others_str, cds_proposal_items_dat.ElementAt(ii).CSI_REASON_OTHER);

                    string reason_best_price_field0 = "";
                    string reason_best_price_field1 = "";
                    decimal? field1_val = decimal.MinValue;
                    string reason_best_price_field2 = "";
                    double field3_val0 = double.MinValue;
                    double field3_val1 = double.MinValue;
                    if (true)
                    //if (checkboxes0[0] == true)
                    {
                        reason_best_price_field0 = "Higher than purchasing price by";
                        reason_best_price_field1 = cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE.ToString();
                        field1_val = cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE;
                        string unit_txt = cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE_UNIT;
                        reason_best_price_field2 = (string)Query<string>("SELECT VALUE FROM CDS_MT_PRICE_PER_QAUNTITY_UNIT WHERE ID = '" + unit_txt + "'");
                        //if (cds_proposal_items_dat != null)
                        //{
                        //	double q0 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE_MIN_USD.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        //	double q1 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE_MAX_USD.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        //	reason_best_price_field3 = q0 == q1 ? q1.ToString() : q0.ToString() + " - " + q1.ToString();
                        //}
                    }

                    if (checkboxes0[1] == true)
                    {
                        reason_others_str = cds_proposal_items_dat.ElementAt(ii).CSI_REASON_OTHER;
                    }

                    string pricing_period_str = "";
                    if (cds_proposal_items_dat != null)
                    {
                        DateTime? d0 = cds_proposal_items_dat.ElementAt(ii).CSI_PRICING_PERIOD_FROM;
                        DateTime? d1 = cds_proposal_items_dat.ElementAt(ii).CSI_PRICING_PERIOD_TO;
                        pricing_period_str = (d0 != null && d1 != null) ? d0?.ToString("dd MMM yyyy") + " - " + d1?.ToString("dd MMM yyyy") : "-";
                    }
                    string quantity_str = "";
                    if (cds_proposal_items_dat != null)
                    {
                        double q0 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_QUANTITY_MIN.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        double q1 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_QUANTITY_MAX.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        quantity_str = q0 == q1 ? FormatNumber(q1, forceShowDecimal: false) : FormatNumber(q0, forceShowDecimal: false) + " - " + FormatNumber(q1, forceShowDecimal: false);
                    }
                    //string credit_condition_str = true ? "x" : "x";
                    string tolerance_percent_str = "";
                    if (cds_proposal_items_dat != null)
                    {
                        string sign = cds_proposal_items_dat.ElementAt(ii).CSI_TOLERANCE_SIGN;
                        string tolerance = cds_proposal_items_dat.ElementAt(ii).CSI_TOLERANCE.ToString();
                        string tolerance_option = cds_proposal_items_dat.ElementAt(ii).CSI_TOLERANCE_OPTION;
                        tolerance_percent_str = sign + " " + FormatNumber(tolerance) + " " + tolerance_option + " tolerance";
                    }
                    DrawText(dynamic_rows[0 + (ii * data_line_per_num)].Cells[1], "Award to : ", TextFormat.Bold, ParagraphAlignment.Right);
                    DrawText(dynamic_rows[0 + (ii * data_line_per_num)].Cells[2], award_to_str);
                    DrawText(dynamic_rows[1 + (ii * data_line_per_num)].Cells[1], "Reason : ", TextFormat.Bold, ParagraphAlignment.Right);
                    DrawMultipleText(doc, dynamic_rows[1 + (ii * data_line_per_num)].Cells[2], new string[] { DrawCheckbox(checkboxes0[0]), " Best price" }
                    , font_size: new double[] { checkboxes0[0] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                    , GLOBAL_FONT_SIZE });

                    if (cds_proposal_items_dat != null)
                    {
                        field3_val0 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE_MIN_USD.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                        field3_val1 = double.Parse(cds_proposal_items_dat.ElementAt(ii).CSI_HIGH_CDP_PRICE_MAX_USD.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                    }

                    //if (checkboxes0[0] == true)
                    if (true)
                    {
                        List<byte[]> txt_colors = new List<byte[]>();
                        txt_colors.Add(new byte[] { 0, 0, 0, 255 }); //Black
                        if (field1_val < 0)
                            txt_colors.Add(new byte[] { 255, 0, 0, 255 });//Red
                        else
                            txt_colors.Add(new byte[] { 0, 0, 0, 255 });
                        txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black
                        bool is_one_string = true;
                        if (field3_val0 != double.MinValue && field3_val1 != double.MinValue)
                        {
                            if (field3_val0 == field3_val1)
                            {
                                if (field3_val1 < 0)
                                    txt_colors.Add(new byte[] { 255, 0, 0, 255 });//Red
                                else
                                    txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black
                            }
                            else
                            {
                                is_one_string = false;
                                if (field3_val0 < 0)
                                    txt_colors.Add(new byte[] { 255, 0, 0, 255 });//Red
                                else
                                    txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black

                                txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black

                                if (field3_val1 < 0)
                                    txt_colors.Add(new byte[] { 255, 0, 0, 255 });//Red
                                else
                                    txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black
                            }

                        }
                        txt_colors.Add(new byte[] { 0, 0, 0, 255 });//Black
                        if (is_one_string == true)
                        {
                            DrawMultipleText(doc, dynamic_rows[1 + (ii * data_line_per_num)].Cells[3], new string[] { reason_best_price_field0 +" ", FormatNumber(reason_best_price_field1,3,false)
                        , " "+reason_best_price_field2+" or ",FormatNumber(field3_val0,3,false)," $"}, v_align: VerticalAlignment.Center, rgba: txt_colors);
                        }
                        else
                        {
                            DrawMultipleText(doc, dynamic_rows[1 + (ii * data_line_per_num)].Cells[3], new string[] { reason_best_price_field0 +" ", FormatNumber(reason_best_price_field1,3,false)
                        , " "+reason_best_price_field2+" or ",
                                field3_val0 >= 0 ? FormatNumber(field3_val0,3,false) : "("+FormatNumber(field3_val0,3,false)+")" ,
                                " - ",
                                field3_val1 >= 0 ? FormatNumber(field3_val1,3,false) : "("+FormatNumber(field3_val1,3,false)+")",
                                " $" }, v_align: VerticalAlignment.Center, rgba: txt_colors);

                        }
                    }

                    DrawMultipleText(doc, dynamic_rows[2 + (ii * data_line_per_num)].Cells[2], new string[] { DrawCheckbox(checkboxes0[1]), " Others : " + reason_others_str }
                    , font_size: new double[] { checkboxes0[1] == true ? GLOBAL_BLACK_BOX_FONT_SIZE :GLOBAL_WHITE_BOX_FONT_SIZE
                    , GLOBAL_FONT_SIZE });

                    //DrawText(dynamic_rows[2 + (ii * data_line_per_num)].Cells[3], reason_others_str);
                    DrawText(dynamic_rows[3 + (ii * data_line_per_num)].Cells[1], "Pricing Period : ", TextFormat.Bold, ParagraphAlignment.Right);
                    DrawText(dynamic_rows[3 + (ii * data_line_per_num)].Cells[2], pricing_period_str);
                    //DrawText(dynamic_rows[3 + (ii * data_line_per_num)].Cells[6], "Credit condition : ", TextFormat.Bold, ParagraphAlignment.Right);
                    //DrawText(dynamic_rows[3 + (ii * data_line_per_num)].Cells[8], credit_condition_str);
                    string quantity_unit_txt = cds_proposal_items_dat.ElementAt(ii).CSI_QUANTITY_UNIT;
                    DrawText(dynamic_rows[4 + (ii * data_line_per_num)].Cells[1], "Quantity (" + quantity_unit_txt + ") : ", TextFormat.Bold, ParagraphAlignment.Right);
                    DrawText(dynamic_rows[4 + (ii * data_line_per_num)].Cells[2], FormatNumber(quantity_str, forceShowDecimal: false));
                    DrawText(dynamic_rows[4 + (ii * data_line_per_num)].Cells[6], DrawWhiteSpaces(3) + "Tolerance (%) : ", TextFormat.Bold, ParagraphAlignment.Right);
                    DrawText(dynamic_rows[4 + (ii * data_line_per_num)].Cells[8], tolerance_percent_str);
                }
            }

            doc.LastSection.Add(table);
        }

        //Note 
        public static void genTable8(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Width = 0.4;
            table.Rows.Height = NORMAL_ROW_HEIGHT;
            table.Rows.VerticalAlignment = VerticalAlignment.Center;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;

            table.Format.Font.Bold = true;

            Column column1 = table.AddColumn(getWidthUnitFromRatio(13.0 / 13));

            Row row1 = table.AddRow();
            Row row2 = table.AddRow();
            Row row3 = table.AddRow();

            //Vertical merge
            row2.Cells[0].MergeDown = 1;

            row1.Cells[0].Borders.Bottom.Visible = false;
            row2.Cells[0].Borders.Top.Visible = false;
            ///////////////////////////////////////////////////////////////////
            string note = cdsData.CDA_NOTE != null ? cdsData.CDA_NOTE : "";
            DrawText(row1.Cells[0], "Note", TextFormat.Bold);
            DrawText(row2.Cells[0], note, v_align: VerticalAlignment.Top);

            doc.LastSection.Add(table);
        }

        //Signatures
        public static void genTable9(Document doc, CDS_DATA cdsData)
        {
            Table table = new Table();
            table.Borders.Left.Width = 0.4;
            table.Borders.Right.Width = 0.4;
            table.Borders.Top.Width = 0.2;
            table.Borders.Bottom.Width = 0.4;
            table.Format.Font.Bold = true;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Format.Font.Size = GLOBAL_FONT_SIZE;



            Column column1 = table.AddColumn(Unit.FromCentimeter(5));
            Column column2 = table.AddColumn(Unit.FromCentimeter(5));
            Column column3 = table.AddColumn(Unit.FromCentimeter(5));
            Column column4 = table.AddColumn(Unit.FromCentimeter(5));

            Row row1 = table.AddRow();


            row1.Height = 10;

            var listHistory = getHistoryByTxns(cdsData.CDA_ROW_ID);

            row1.Cells[0].AddParagraph(DrawWhiteSpaces(2));
            row1.Cells[1].AddParagraph(DrawWhiteSpaces(2));
            row1.Cells[2].AddParagraph(DrawWhiteSpaces(2));
            row1.Cells[3].AddParagraph(DrawWhiteSpaces(2));

            //row1.Cells[0].AddParagraph("");
            row1.Cells[0].AddParagraph("Requested By");
            Paragraph ppp0 = row1.Cells[0].AddParagraph("Trader");
            ppp0.Format.Alignment = ParagraphAlignment.Left;


            //row1.Cells[1].AddParagraph("");
            row1.Cells[1].AddParagraph("Verified By");
            Paragraph ppp1 = row1.Cells[1].AddParagraph("S/H");
            ppp1.Format.Alignment = ParagraphAlignment.Left;

            //row1.Cells[2].AddParagraph("");
            row1.Cells[2].AddParagraph("Endorsed By");

            //row1.Cells[3].AddParagraph("");
            row1.Cells[3].AddParagraph("Approved By");

            row1.Cells[2].AddParagraph(DrawWhiteSpaces(2));
            row1.Cells[3].AddParagraph(DrawWhiteSpaces(2));
            Paragraph p3 = row1.Cells[2].AddParagraph(DrawWhiteSpaces(2));
            Paragraph p4 = row1.Cells[3].AddParagraph(DrawWhiteSpaces(2));


            var hSubmit = listHistory.Where(h => h.DTH_ACTION == "SUBMIT").ToList();
            var hVerify = listHistory.Where(h => h.DTH_ACTION == "VERIFY").ToList();
            var hEndorse = listHistory.Where(h => h.DTH_ACTION == "ENDORSE").ToList();
            var hApprove = listHistory.Where(h => h.DTH_ACTION == "APPROVE").ToList();

            //String section = cdsData.USER_GROUP + "";
            string section = "CMCS";

            if (hSubmit.Count > 0)
            {

                Paragraph p2 = row1.Cells[0].AddParagraph("");

                var img = p2.AddImage(getSignatureImg(hSubmit.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                row1.Cells[0].AddParagraph("( " + section + "-" + getNameOfUser(hSubmit.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hVerify.Count > 0)
                {

                    Paragraph p2 = row1.Cells[0].AddParagraph("");

                    var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                    row1.Cells[0].AddParagraph("( " + section + "-" + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {

                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("");
                    row1.Cells[0].AddParagraph("( " + section + " )");
                }

            }

            if (hVerify.Count > 0)
            {

                Paragraph p2 = row1.Cells[1].AddParagraph("");

                p2.Format.LeftIndent = -40;
                var img = p2.AddImage(getSignatureImg(hVerify.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                row1.Cells[1].AddParagraph("( " + section + "-" + getNameOfUser(hVerify.FirstOrDefault().DTH_ACTION_BY) + " )");
            }
            else
            {
                if (hEndorse.Count > 0)
                {

                    //Paragraph p2 = row1.Cells[1].AddParagraph(" S/H:");
                    Paragraph p2 = row1.Cells[1].AddParagraph("");

                    var img = p2.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));
                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                    row1.Cells[1].AddParagraph("( " + section + "-" + getNameOfUser(hEndorse.FirstOrDefault().DTH_ACTION_BY) + " )");
                }
                else
                {
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("");
                    row1.Cells[1].AddParagraph("( " + section + " )");
                }

            }

            if (hEndorse.Count > 0)
            {
                Paragraph p2 = row1.Cells[1].AddParagraph("");
                p2.AddSpace(8);
                p2.Format.LeftIndent = -40;
                var img = p3.AddImage(getSignatureImg(hEndorse.FirstOrDefault().DTH_ACTION_BY));

                img.Width = "2.3cm";
                img.LockAspectRatio = true;

            }
            else
            {
                if (hApprove.Count > 0)
                {
                    Paragraph p2 = row1.Cells[1].AddParagraph("");
                    p2.AddSpace(8);
                    p2.Format.LeftIndent = -40;
                    var img = p3.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));

                    img.Width = "2.3cm";
                    img.LockAspectRatio = true;
                }
                else
                {
                    row1.Cells[2].AddParagraph("");
                    row1.Cells[2].AddParagraph("");
                    row1.Cells[2].AddParagraph("");

                }
            }

            if (hApprove.Count > 0)
            {
                Paragraph p2 = row1.Cells[1].AddParagraph("");
                p2.AddSpace(8);
                p2.Format.LeftIndent = -40;
                var img = p4.AddImage(getSignatureImg(hApprove.FirstOrDefault().DTH_ACTION_BY));
                img.Width = "2.3cm";
                img.LockAspectRatio = true;
                //img.WrapFormat.DistanceLeft = 20;
            }
            else
            {
                row1.Cells[3].AddParagraph("");
                row1.Cells[3].AddParagraph("");
                row1.Cells[3].AddParagraph("");
            }


            row1.Cells[2].AddParagraph("CMVP");
            row1.Cells[3].AddParagraph("EVPC");
            row1.Cells[2].AddParagraph("");
            row1.Cells[3].AddParagraph("");

            doc.LastSection.Add(table);
        }

        public static List<CPAI_DATA_HISTORY> getHistoryByTxns(String dthTxnRef)
        {
            using (EntityCPAIEngine entity = new EntityCPAIEngine())
            {
                List<CPAI_DATA_HISTORY> results = entity.CPAI_DATA_HISTORY.Where(x => x.DTH_TXN_REF == dthTxnRef && x.DTH_REJECT_FLAG == "N").ToList();
                if (results != null)
                {
                    return results;
                }
                else
                {
                    return new List<CPAI_DATA_HISTORY>();
                }
            }
        }

        public static string getSignatureImg(string UserName)
        {
            List<string> extentions = new List<string>(); extentions.Add(".jpg"); extentions.Add(".png"); extentions.Add(".gif");
            ShareFn sFn = new ShareFn();
            string pathSignature = "";
            foreach (string _itemExt in extentions)
            {
                if (File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt))
                {
                    pathSignature = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content", "images", "Signature", UserName) + _itemExt;
                    break;
                }
            }
            return pathSignature;
        }

        public static String getNameOfUser(string userName)
        {

            using (var context = new EntityCPAIEngine())
            {
                var users = context.USERS.Where(x => x.USR_LOGIN.ToUpper() == userName.ToUpper() && x.USR_STATUS == "ACTIVE").ToList();
                if (users.Count > 0)
                {
                    return users.FirstOrDefault().USR_FIRST_NAME_EN.ToUpper() + "";
                }
                else
                {
                    return "";
                }
            }
        }

        //public static void genTable9(Document doc, CDS_DATA cdsData)
        //{
        //	Table table = new Table();
        //	table.Borders.Width = 0.4;
        //	table.Rows.Height = NORMAL_ROW_HEIGHT;
        //	table.Rows.VerticalAlignment = VerticalAlignment.Center;
        //	table.Format.Alignment = ParagraphAlignment.Center;
        //	table.Format.Font.Size = GLOBAL_FONT_SIZE;

        //	table.Format.Font.Bold = true;

        //	Column column1 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
        //	Column column2 = table.AddColumn(getWidthUnitFromRatio(3.5 / 13));
        //	Column column3 = table.AddColumn(getWidthUnitFromRatio(3.5 / 13));
        //	Column column4 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));

        //	Row row1 = table.AddRow();
        //	Row row2 = table.AddRow();
        //	Row row3 = table.AddRow();
        //	Row row4 = table.AddRow();

        //	row2.Cells[0].Borders.Bottom.Visible = false;
        //	row3.Cells[0].Borders.Top.Visible = false;
        //	row2.Cells[1].Borders.Bottom.Visible = false;
        //	row3.Cells[1].Borders.Top.Visible = false;
        //	row2.Cells[2].Borders.Bottom.Visible = false;
        //	row3.Cells[2].Borders.Top.Visible = false;
        //	row2.Cells[3].Borders.Bottom.Visible = false;
        //	row3.Cells[3].Borders.Top.Visible = false;

        //	row3.Cells[0].Borders.Bottom.Visible = false;
        //	row4.Cells[0].Borders.Top.Visible = false;
        //	row3.Cells[1].Borders.Bottom.Visible = false;
        //	row4.Cells[1].Borders.Top.Visible = false;
        //	row3.Cells[2].Borders.Bottom.Visible = false;
        //	row4.Cells[2].Borders.Top.Visible = false;
        //	row3.Cells[3].Borders.Bottom.Visible = false;
        //	row4.Cells[3].Borders.Top.Visible = false;

        //	///////////////////////////////////////////////////////////////////
        //	DrawText(row1.Cells[0], "Requested by", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[1], "Verified by", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[2], "Endorsed by", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[3], "Approved by", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

        //	DrawText(row2.Cells[0], "Trader", TextFormat.Bold, ParagraphAlignment.Left);
        //	DrawText(row2.Cells[1], "S/H", TextFormat.Bold, ParagraphAlignment.Left);

        //	DrawText(row4.Cells[0], "( CMCS-Siriphat )", TextFormat.Bold, ParagraphAlignment.Center);
        //	DrawText(row4.Cells[1], "( CMCS-Anawat )", TextFormat.Bold, ParagraphAlignment.Center);
        //	DrawText(row4.Cells[2], "CMVP", TextFormat.Bold, ParagraphAlignment.Center);
        //	DrawText(row4.Cells[3], "EVPC", TextFormat.Bold, ParagraphAlignment.Center);

        //	string img0 = "";
        //	DrawImage(row3.Cells[0], img0, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	string img1 = "";
        //	DrawImage(row3.Cells[1], img1, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	string img2 = "";
        //	DrawImage(row3.Cells[2], img2, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	string img3 = "";
        //	DrawImage(row3.Cells[3], img3, ParagraphAlignment.Center, VerticalAlignment.Center);

        //	doc.LastSection.Add(table);
        //}

        ///////////////////

        ////Crude Purchase Detail (For Reference)
        //public static void genTable5(Document doc, CDS_DATA cdsData)
        //{
        //	Table table = new Table();
        //	table.Borders.Width = 0.4;
        //	table.Rows.Height = NORMAL_ROW_HEIGHT;
        //	table.Rows.VerticalAlignment = VerticalAlignment.Center;
        //	table.Format.Alignment = ParagraphAlignment.Center;
        //	table.Format.Font.Size = GLOBAL_FONT_SIZE;

        //	table.Format.Font.Bold = true;

        //	int offer_round_dynamic_col_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '"+ cdsData.CDA_ROW_ID+ "'"); //Count from db 

        //	int data_row_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'"); //Count from db

        //	Column column1 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column2 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column3 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column4 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));

        //	Column column5 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	//Dynamic column
        //	double factor = 3;
        //	switch (offer_round_dynamic_col_num)
        //	{
        //		case 0:
        //		case 1: factor = 3;
        //			break;
        //		case 2: factor = 1.5;
        //			break;
        //		case 3: factor = 1;
        //			break;
        //	}
        //	for (int i =0;i< offer_round_dynamic_col_num; i++)
        //	{
        //		Column column_dynamic = table.AddColumn(getWidthUnitFromRatio(factor / 13));
        //	}
        //	Column column6 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column7 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column8 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));

        //	Column column9 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column10 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));

        //	/////////////////////////////////////////////////////////////////
        //	//Static row
        //	Row row_header = table.AddRow();
        //	Row row1 = table.AddRow();
        //	Row row2 = table.AddRow();
        //	Row row3 = table.AddRow();
        //	Row row4 = table.AddRow();

        //	//Vertical merge
        //	row1.Cells[0].MergeDown = 3;
        //	row1.Cells[1].MergeDown = 3;
        //	row1.Cells[2].MergeDown = 3;
        //	row1.Cells[3].MergeDown = 3;

        //	row2.Cells[4].MergeDown = 2;
        //	int tmp_idx_before_dynamic = 4;
        //	for (int i = 0; i < offer_round_dynamic_col_num; i++)
        //	{
        //		tmp_idx_before_dynamic++;
        //		row2.Cells[tmp_idx_before_dynamic].MergeDown = 1;
        //	}
        //	row2.Cells[tmp_idx_before_dynamic+1].MergeDown = 2;
        //	row2.Cells[tmp_idx_before_dynamic+2].MergeDown = 2;
        //	row2.Cells[tmp_idx_before_dynamic+3].MergeDown = 2;

        //	row1.Cells[tmp_idx_before_dynamic+4].MergeDown = 3;
        //	row1.Cells[tmp_idx_before_dynamic+5].MergeDown = 3;

        //	//Horizontal Merge
        //	row_header.Cells[0].MergeRight = (10+ offer_round_dynamic_col_num) - 1;
        //	row1.Cells[4].MergeRight = 3 + offer_round_dynamic_col_num;
        //	row2.Cells[5].MergeRight = offer_round_dynamic_col_num - 1;
        //	/////////////////////////////////////////////////////////////////

        //	//Dynamic row
        //	List<Row> dynamic_row = new List<Row>();
        //	for (int j =0;j<data_row_num; j++)
        //	{
        //		dynamic_row.Add(table.AddRow());
        //	}

        //	/////////////////////////////////////////////////////////////////
        //	DrawText(row_header.Cells[0],"Crude Purchase Detail (For Reference)",TextFormat.Bold);
        //	DrawText(row1.Cells[0], "Crude", TextFormat.Bold,ParagraphAlignment.Center,VerticalAlignment.Center);
        //	DrawText(row1.Cells[1], "Supplier", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[2], "Contact Person", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[3], "Quantity (KBBL)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[4], "Offer Price or Premium ($/bbl)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[4+ offer_round_dynamic_col_num+3+1], "Rank/ Round ", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[4 + offer_round_dynamic_col_num + 3 + 2], "Margin vs LP ($/bbl)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

        //	DrawText(row2.Cells[4], "Inco terms", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[5], "Offer / Negotiation round", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[5+ (offer_round_dynamic_col_num-1)+1], "Market/ Source", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[5+ (offer_round_dynamic_col_num - 1)+2], "LP Max Purchase Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[5+ (offer_round_dynamic_col_num - 1) + 3], "Benchmark Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	string[] order_text = new string[] { "1st", "2nd", "3rd" };
        //	for (int i = 0; i < offer_round_dynamic_col_num; i++)
        //	{
        //		int start_idx = 5;
        //		Cell target = row4.Cells[start_idx+i];
        //		DrawText(target, order_text[i], TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	}

        //	for (int k = 0; k < data_row_num; k++)
        //	{
        //		foreach (Row r in dynamic_row)
        //		{
        //			string crude_txt = mt_materials_dat != null ? mt_materials_dat.ElementAt(k).MET_MAT_DES_ENGLISH : "";
        //			string supplier_txt = true ? "x" : "x";
        //			string contact_person_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_CONTACT_PERSON : "";
        //			string quantity_txt = "";
        //			if (cds_bidding_items_dat != null)
        //			{
        //				quantity_txt = cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MIN == cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX ?
        //					cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX.ToString() :
        //					(cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MIN.ToString() + " - " + cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX.ToString());
        //			}
        //			string incoterms_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_INCOTERM : "";
        //			string[] offers_txt = new string[3];
        //			offers_txt[0] = "x";
        //			offers_txt[1] = "x";
        //			offers_txt[2] = "x";
        //			string market_source_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_MARKET_PRICE.ToString() : "";
        //			string lp_max_purchase_txt = true ? "x" : "x";
        //			string benchmark_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_BENCHMARK_PRICE : "";
        //			string rank_round_txt = true ? "x" : "x";
        //			string margin_vs_lp_txt = true ? "x" : "x";
        //			DrawText(r.Cells[0], crude_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[1], supplier_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[2], contact_person_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[3], quantity_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[4], incoterms_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			tmp_idx_before_dynamic = 4;
        //			for (int i = 0; i < offer_round_dynamic_col_num; i++)
        //			{
        //				tmp_idx_before_dynamic++;
        //				DrawText(r.Cells[tmp_idx_before_dynamic], offers_txt[i], align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			}
        //			DrawText(r.Cells[tmp_idx_before_dynamic + 1], market_source_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[tmp_idx_before_dynamic + 2], lp_max_purchase_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[tmp_idx_before_dynamic + 3], benchmark_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[tmp_idx_before_dynamic + 4], rank_round_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[tmp_idx_before_dynamic + 5], margin_vs_lp_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //		}
        //	}
        //	/////////////////////////////////////////////////////////////////
        //	doc.LastSection.Add(table);
        //}

        ////Negotiation Summary
        //public static void genTable6(Document doc, CDS_DATA cdsData)
        //{
        //	Table table = new Table();
        //	table.Borders.Width = 0.4;
        //	table.Rows.Height = NORMAL_ROW_HEIGHT;
        //	table.Rows.VerticalAlignment = VerticalAlignment.Center;
        //	table.Format.Alignment = ParagraphAlignment.Center;
        //	table.Format.Font.Size = GLOBAL_FONT_SIZE;

        //	table.Format.Font.Bold = true;

        //	int data_row_num = (int)Query<int>("select count(CBI_ROW_ID) from CDS_BIDDING_ITEMS where CBI_FK_CDS_DATA = '" + cdsData.CDA_ROW_ID + "'"); //Count from db

        //	Column column1 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column2 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column3 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column4 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column5 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column6 = table.AddColumn(getWidthUnitFromRatio(3.0 / 13));
        //	Column column7 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column8 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column9 = table.AddColumn(getWidthUnitFromRatio(1.0 / 13));
        //	Column column10 = table.AddColumn(getWidthUnitFromRatio(2.0 / 13));

        //	/////////////////////////////////////////////////////////////////
        //	//Static row
        //	Row row_header = table.AddRow();
        //	Row row1 = table.AddRow();
        //	Row row2 = table.AddRow();
        //	Row row3 = table.AddRow();

        //	//Vertical merge
        //	row1.Cells[0].MergeDown = 2;
        //	row1.Cells[1].MergeDown = 2;
        //	row1.Cells[2].MergeDown = 2;
        //	row1.Cells[3].MergeDown = 2;

        //	row2.Cells[4].MergeDown = 1;
        //	row2.Cells[5].MergeDown = 1;
        //	row2.Cells[6].MergeDown = 1;
        //	row2.Cells[7].MergeDown = 1;
        //	row2.Cells[8].MergeDown = 1;

        //	row1.Cells[9].MergeDown = 2;

        //	//Horizontal Merge
        //	row_header.Cells[0].MergeRight = 9;
        //	row1.Cells[4].MergeRight = 4;
        //	/////////////////////////////////////////////////////////////////

        //	//Dynamic row
        //	List<Row> dynamic_row = new List<Row>();
        //	for (int j = 0; j < data_row_num; j++)
        //	{
        //		dynamic_row.Add(table.AddRow());
        //	}

        //	/////////////////////////////////////////////////////////////////
        //	DrawText(row_header.Cells[0], "Negotiation Summary", TextFormat.Bold);
        //	DrawText(row1.Cells[0], "Crude", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[1], "Customer", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[2], "Contact Person", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[3], "Quantity (KBBL)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[4], "Offer Price or Premium ($/bbl)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row1.Cells[9], "Margin vs Purchasing Price ($/bbl)", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

        //	DrawText(row2.Cells[4], "Inco terms", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[5], "Selling Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[6], "Market/ Source", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[7], "Purchasing Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);
        //	DrawText(row2.Cells[8], "Benchmark Price", TextFormat.Bold, ParagraphAlignment.Center, VerticalAlignment.Center);

        //	int k = 0;
        //		foreach (Row r in dynamic_row)
        //		{
        //			string crude_txt = mt_materials_dat != null ? mt_materials_dat.ElementAt(k).MET_MAT_DES_ENGLISH : "";
        //			string supplier_txt = true ? "x" : "x";
        //			string contact_person_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_CONTACT_PERSON : "";
        //			string quantity_txt = "";
        //			if (cds_bidding_items_dat != null)
        //			{
        //				quantity_txt = cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MIN == cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX ?
        //					cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX.ToString() :
        //					(cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MIN.ToString() + " - " + cds_bidding_items_dat.ElementAt(k).CBI_QUANTITY_MAX.ToString());
        //			}
        //			string incoterms_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_INCOTERM : "";
        //			string selling_price_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_PRICE_UNIT : "";
        //			string market_source_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_MARKET_PRICE.ToString() : "";
        //			string purchasing_price_txt = true ? "x" : "x";
        //			string benchmark_txt = cds_bidding_items_dat != null ? cds_bidding_items_dat.ElementAt(k).CBI_BENCHMARK_PRICE : "";
        //			string margin_vs_purchasing_price_txt = true ? "x" : "x";
        //			DrawText(r.Cells[0], crude_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[1], supplier_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[2], contact_person_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[3], quantity_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[4], incoterms_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[5], selling_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[6], market_source_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[7], purchasing_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[8], benchmark_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //			DrawText(r.Cells[9], margin_vs_purchasing_price_txt, align: ParagraphAlignment.Center, v_align: VerticalAlignment.Center);
        //		k++;
        //		}


        //	doc.LastSection.Add(table);
        //	/////////////////////////////////////////////////////////////////

        //	//Draw Sub Table 
        //	for (int j = 0; j < data_row_num; j++)
        //	{
        //		genSubTable6(doc, cdsData);
        //	}

        //}
    }
}