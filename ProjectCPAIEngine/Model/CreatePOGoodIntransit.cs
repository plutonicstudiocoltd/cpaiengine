﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{

    public class CreatePOGoodIntransit
    {
        public CrudeApproveFormViewModel_Detail ItemList { get; set; } //MaterialViewModel FreightViewModel
        public List<CreatePO> createPO { get; set; }
        public List<GoodIntransit> goodIntransit { get; set; }

        public List<GoodIntransitCancel> goodIntransitCancel { get; set; }

    }

    public class CreatePO
    {
        public string tripNo { get; set; }
        public string item { get; set; }
        public string ItemNoPOCrude { get; set; }
        public string ItemNoPOFreight { get; set; }
        public string ItemNoPOInsur { get; set; }
        public string ItemNoPOCustom { get; set; }
        public PurchasingCreateModel purchasingCreate { get; set; }
    }

    public class PurchasingCreateModel
    {
        public ZPOHEADER ZPOHEADER { get; set; }
        public ZPOHEADERX ZPOHEADERX { get; set; }
        public List<ZPOITEM> ZPOITEM { get; set; }
        public List<ZPOITEMX> ZPOITEMX { get; set; }
        public List<ZPOACCOUNT> ZPOACCOUNT { get; set; }
        public List<ZPOACCOUNTX> ZPOACCOUNTX { get; set; }
        public List<BAPIRET2> RTEURN { get; set; }
    }

    public class GoodIntransit
    {
        public string tripNo { get; set; }
        public string item { get; set; }
        public IntansitPostModel intansitPost { get; set; }
    }
    public class IntansitPostModel
    {
        public ZGOODSMVT_CODE ZGOODSMVT_CODE { get; set; }
        public ZGOODSMVT_HEADER ZGOODSMVT_HEADER { get; set; }
        public List<BAPIRET2> RETURN { get; set; }
        public List<ZGOODSMVT_ITEM_01> ZGOODSMVT_ITEM_01 { get; set; }
        public List<BAPIOIL2017_GM_ITM_CRTE_PARAM> ZGOODSMVT_ITEM_PARAM { get; set; }
        public List<ZGOODSMVT_ITEM_QUAN> ZGOODSMVT_ITEM_QUAN { get; set; }
        public BAPI2017_GM_HEAD_RET GM_HEAD_RET { get; set; }
    }
    public class GoodIntransitCancel
    {
        public string tripNo { get; set; }
        public string item { get; set; }
        public IntansitCancelModel intansitCancel { get; set; }
    }
    public class IntansitCancelModel
    {
        public BAPI2017_GM_ITEM_04 GM_ITEM_04 { get; set; }
        public List<BAPIRET2> RETURN { get; set; }
        public BAPI2017_GM_HEAD_RET GM_HEAD_RET { get; set; }
        public MORE_DETAI more_detai { get; set; }
    }

    public class MORE_DETAI
    {
        public string USER_NAME { get; set; }
        public string MATERIAL_DOCUMENT { get; set; }
        public string YEAR { get; set; }
        public string DATE { get; set; }
    }

    #region IntansitPostModel
    public class ZGOODSMVT_ITEM_QUAN
    {
        private string lINE_IDField;

        private string pARENT_IDField;

        private string lINE_DEPTHField;

        private double qUANTITYFLOATField;

        private bool qUANTITYFLOATFieldSpecified;

        private decimal qUANTITYPACKEDField;

        private bool qUANTITYPACKEDFieldSpecified;

        private string qUANTITYUOMField;

        private string qUANTITYISOUOMField;

        private string qUANTITYCHECKField;

        
        
        public string LINE_ID
        {
            get
            {
                return this.lINE_IDField;
            }
            set
            {
                this.lINE_IDField = value;
            }
        }

        
        
        public string PARENT_ID
        {
            get
            {
                return this.pARENT_IDField;
            }
            set
            {
                this.pARENT_IDField = value;
            }
        }

        
        
        public string LINE_DEPTH
        {
            get
            {
                return this.lINE_DEPTHField;
            }
            set
            {
                this.lINE_DEPTHField = value;
            }
        }

        
        
        public double QUANTITYFLOAT
        {
            get
            {
                return this.qUANTITYFLOATField;
            }
            set
            {
                this.qUANTITYFLOATField = value;
            }
        }

        
        
        public bool QUANTITYFLOATSpecified
        {
            get
            {
                return this.qUANTITYFLOATFieldSpecified;
            }
            set
            {
                this.qUANTITYFLOATFieldSpecified = value;
            }
        }

        
        
        public decimal QUANTITYPACKED
        {
            get
            {
                return this.qUANTITYPACKEDField;
            }
            set
            {
                this.qUANTITYPACKEDField = value;
            }
        }

        
        
        public bool QUANTITYPACKEDSpecified
        {
            get
            {
                return this.qUANTITYPACKEDFieldSpecified;
            }
            set
            {
                this.qUANTITYPACKEDFieldSpecified = value;
            }
        }

        
        
        public string QUANTITYUOM
        {
            get
            {
                return this.qUANTITYUOMField;
            }
            set
            {
                this.qUANTITYUOMField = value;
            }
        }

        
        
        public string QUANTITYISOUOM
        {
            get
            {
                return this.qUANTITYISOUOMField;
            }
            set
            {
                this.qUANTITYISOUOMField = value;
            }
        }

        
        
        public string QUANTITYCHECK
        {
            get
            {
                return this.qUANTITYCHECKField;
            }
            set
            {
                this.qUANTITYCHECKField = value;
            }
        }
    }
    public class BAPIOIL2017_GM_ITM_CRTE_PARAM
    {

        private string lINE_IDField;

        private string pARENT_IDField;

        private string lINE_DEPTHField;

        private string cONVERSIONGROUPField;

        private string cALCULATEMISSINGField;

        private string uSEDEFAULTPARAMETERSField;

        private double fIXEDDENSITYField;

        private bool fIXEDDENSITYFieldSpecified;

        private string fIXEDDENSITYUOMField;

        private string fIXEDDENSITYUOM_ISOField;

        private double tHERMALEXPCOEFFField;

        private bool tHERMALEXPCOEFFFieldSpecified;

        private double bASEDENSITYField;

        private bool bASEDENSITYFieldSpecified;

        private string bASEDENSITYUOMField;

        private string bASEDENSITYUOM_ISOField;

        private double bASEHEATINGVALUEField;

        private bool bASEHEATINGVALUEFieldSpecified;

        private string bASEHEATINGVALUEUOMField;

        private string bASEHEATINGVALUEUOM_ISOField;

        private double tESTTEMPERATURE_DENSITYField;

        private bool tESTTEMPERATURE_DENSITYFieldSpecified;

        private string tESTTEMP_DENSITY_UOMField;

        private string tESTTEMPDENSITY_UOM_ISOField;

        private double mATERIALTEMPERATUREField;

        private bool mATERIALTEMPERATUREFieldSpecified;

        private string mATERIALTEMPERATURE_UOMField;

        private string mATERIALTEMPERATURE_UOM_ISOField;

        private double tESTHEATINGVALUEField;

        private bool tESTHEATINGVALUEFieldSpecified;

        private string tESTHEATINGVALUE_UOMField;

        private string tESTHEATINGVALUE_UOM_ISOField;

        private double tESTDENSITYField;

        private bool tESTDENSITYFieldSpecified;

        private string tESTDENSITY_UOMField;

        private string tESTDENSITY_UOM_ISOField;

        private double mATERIALPRESSUREField;

        private bool mATERIALPRESSUREFieldSpecified;

        private string mATERIALPRESSURE_UOMField;

        private string mATERIALPRESSURE_UOM_ISOField;

        private double tESTPRESSURE_DENSITYField;

        private bool tESTPRESSURE_DENSITYFieldSpecified;

        private string tESTPRESSDENSITY_UOMField;

        private string tESTPRESSDENSITY_UOM_ISOField;

        private double tESTPRESSURECOMBUSTIONField;

        private bool tESTPRESSURECOMBUSTIONFieldSpecified;

        private string tESTPRESSURECOMBUSTION_UOMField;

        private string tESTPRESSURECOMB_UOM_ISOField;

        private double tESTTEMPERATURECOMBUSTIONField;

        private bool tESTTEMPERATURECOMBUSTIONFieldSpecified;

        private string tESTTEMPERATURECOMB_UOMField;

        private string tESTTEMPERATURECOMB_UOM_ISOField;

        private double aTMOSPHERICPRESSUREField;

        private bool aTMOSPHERICPRESSUREFieldSpecified;

        private string aTMOSPHERICPRESSURE_UOMField;

        private string aTMOSPHERICPRESS_UOM_ISOField;

        private double vAPORPRESSUREField;

        private bool vAPORPRESSUREFieldSpecified;

        private string vAPORPRESSURE_UOMField;

        private string vAPORPRESSURE_UOM_ISOField;

        private double mATERIALCOMBUSTIONTEMPERATUREField;

        private bool mATERIALCOMBUSTIONTEMPERATUREFieldSpecified;

        private string mATERIALCOMBUSTIONTEMP_UOMField;

        private string mATERIALCOMBUSTIONTEMP_UOM_ISOField;

        private double mATERIALCOMBUSTIONPRESSUREField;

        private bool mATERIALCOMBUSTIONPRESSUREFieldSpecified;

        private string mATERIALCOMBUSTIONPRESS_UOMField;

        private string mATERIALCOMBUSTIONPRESS_UOM_ISField;

        private double tESTTEMPCALHEATVALUEField;

        private bool tESTTEMPCALHEATVALUEFieldSpecified;

        private string tESTTEMPCALHEATVALUE_UOMField;

        private string tESTTEMPCALHEATVALUE_UOM_ISOField;

        private double tESTPRESSCALHEATVALUEField;

        private bool tESTPRESSCALHEATVALUEFieldSpecified;

        private string tESTPRESSCALHEATVALUE_UOMField;

        private string tESTPRESSCALHEATVALUE_UOM_ISOField;

        private string hYDROMETERINDICATORField;

        private string aIRBUOYANCYINDICATORField;

        private decimal aIRBUOYANCYCONSTANTField;

        private bool aIRBUOYANCYCONSTANTFieldSpecified;

        private double mETERCORRECTIONFACTORField;

        private bool mETERCORRECTIONFACTORFieldSpecified;

        private double bASESEDIMENTWATERCONTENTField;

        private bool bASESEDIMENTWATERCONTENTFieldSpecified;

        private string bSW_UOMField;

        private string bSW_UOM_ISOField;

        private string hEATINGVALUECLASSField;

        private string hEATINGVALUECLASS_TOField;

        private double cH4FRACTIONField;

        private bool cH4FRACTIONFieldSpecified;

        private double n2FRACTIONField;

        private bool n2FRACTIONFieldSpecified;

        private double cO2FRACTIONField;

        private bool cO2FRACTIONFieldSpecified;

        private double c2H6FRACTIONField;

        private bool c2H6FRACTIONFieldSpecified;

        private double c3H8FRACTIONField;

        private bool c3H8FRACTIONFieldSpecified;

        private double h2OFRACTIONField;

        private bool h2OFRACTIONFieldSpecified;

        private double h2SFRACTIONField;

        private bool h2SFRACTIONFieldSpecified;

        private double h2FRACTIONField;

        private bool h2FRACTIONFieldSpecified;

        private double cOFRACTIONField;

        private bool cOFRACTIONFieldSpecified;

        private double o2FRACTIONField;

        private bool o2FRACTIONFieldSpecified;

        private double iSOC4H10FRACTIONField;

        private bool iSOC4H10FRACTIONFieldSpecified;

        private double nC4H10FRACTIONField;

        private bool nC4H10FRACTIONFieldSpecified;

        private double iSOC5H12FRACTIONField;

        private bool iSOC5H12FRACTIONFieldSpecified;

        private double nC5H12FRACTIONField;

        private bool nC5H12FRACTIONFieldSpecified;

        private double c6H14FRACTIONField;

        private bool c6H14FRACTIONFieldSpecified;

        private double c7H16FRACTIONField;

        private bool c7H16FRACTIONFieldSpecified;

        private double c8H18FRACTIONField;

        private bool c8H18FRACTIONFieldSpecified;

        private double c9H20FRACTIONField;

        private bool c9H20FRACTIONFieldSpecified;

        private double c10H22FRACTIONField;

        private bool c10H22FRACTIONFieldSpecified;

        private double hELIUMFRACTIONField;

        private bool hELIUMFRACTIONFieldSpecified;

        private double aRGONFRACTIONField;

        private bool aRGONFRACTIONFieldSpecified;

        private string gASCOMPOSITIONFRACTION_UOMField;

        private string gASCOMPOSITIONFRACTION_UOM_ISOField;

        private double mOLARDENSITYField;

        private bool mOLARDENSITYFieldSpecified;

        private string mOLARDENSITY_UOMField;

        private string mOLARDENSITY_UOM_ISOField;

        private double mOLECULARWEIGHTField;

        private bool mOLECULARWEIGHTFieldSpecified;

        private string mOLECULARWEIGHT_UOMField;

        private string mOLECULARWEIGHT_UOM_ISOField;

        private double hYDROCARBONMOLFRACTIONField;

        private bool hYDROCARBONMOLFRACTIONFieldSpecified;

        private string cUSTOMERPCHAR1Field;

        private double cUSTOMERPFLOAT1Field;

        private bool cUSTOMERPFLOAT1FieldSpecified;

        private string cUSTOMERP1_UOMField;

        private string cUSTOMERP1_UOM_ISOField;

        private string cUSTOMERPCHAR2Field;

        private double cUSTOMERPFLOAT2Field;

        private bool cUSTOMERPFLOAT2FieldSpecified;

        private string cUSTOMERP2_UOMField;

        private string cUSTOMERP2_UOM_ISOField;

        private string cUSTOMERPCHAR3Field;

        private double cUSTOMERPFLOAT3Field;

        private bool cUSTOMERPFLOAT3FieldSpecified;

        private string cUSTOMERP3_UOMField;

        private string cUSTOMERP3_UOM_ISOField;

        
        
        public string LINE_ID
        {
            get
            {
                return this.lINE_IDField;
            }
            set
            {
                this.lINE_IDField = value;
            }
        }

        
        
        public string PARENT_ID
        {
            get
            {
                return this.pARENT_IDField;
            }
            set
            {
                this.pARENT_IDField = value;
            }
        }

        
        
        public string LINE_DEPTH
        {
            get
            {
                return this.lINE_DEPTHField;
            }
            set
            {
                this.lINE_DEPTHField = value;
            }
        }

        
        
        public string CONVERSIONGROUP
        {
            get
            {
                return this.cONVERSIONGROUPField;
            }
            set
            {
                this.cONVERSIONGROUPField = value;
            }
        }

        
        
        public string CALCULATEMISSING
        {
            get
            {
                return this.cALCULATEMISSINGField;
            }
            set
            {
                this.cALCULATEMISSINGField = value;
            }
        }

        
        
        public string USEDEFAULTPARAMETERS
        {
            get
            {
                return this.uSEDEFAULTPARAMETERSField;
            }
            set
            {
                this.uSEDEFAULTPARAMETERSField = value;
            }
        }

        
        
        public double FIXEDDENSITY
        {
            get
            {
                return this.fIXEDDENSITYField;
            }
            set
            {
                this.fIXEDDENSITYField = value;
            }
        }

        
        
        public bool FIXEDDENSITYSpecified
        {
            get
            {
                return this.fIXEDDENSITYFieldSpecified;
            }
            set
            {
                this.fIXEDDENSITYFieldSpecified = value;
            }
        }

        
        
        public string FIXEDDENSITYUOM
        {
            get
            {
                return this.fIXEDDENSITYUOMField;
            }
            set
            {
                this.fIXEDDENSITYUOMField = value;
            }
        }

        
        
        public string FIXEDDENSITYUOM_ISO
        {
            get
            {
                return this.fIXEDDENSITYUOM_ISOField;
            }
            set
            {
                this.fIXEDDENSITYUOM_ISOField = value;
            }
        }

        
        
        public double THERMALEXPCOEFF
        {
            get
            {
                return this.tHERMALEXPCOEFFField;
            }
            set
            {
                this.tHERMALEXPCOEFFField = value;
            }
        }

        
        
        public bool THERMALEXPCOEFFSpecified
        {
            get
            {
                return this.tHERMALEXPCOEFFFieldSpecified;
            }
            set
            {
                this.tHERMALEXPCOEFFFieldSpecified = value;
            }
        }

        
        
        public double BASEDENSITY
        {
            get
            {
                return this.bASEDENSITYField;
            }
            set
            {
                this.bASEDENSITYField = value;
            }
        }

        
        
        public bool BASEDENSITYSpecified
        {
            get
            {
                return this.bASEDENSITYFieldSpecified;
            }
            set
            {
                this.bASEDENSITYFieldSpecified = value;
            }
        }

        
        
        public string BASEDENSITYUOM
        {
            get
            {
                return this.bASEDENSITYUOMField;
            }
            set
            {
                this.bASEDENSITYUOMField = value;
            }
        }

        
        
        public string BASEDENSITYUOM_ISO
        {
            get
            {
                return this.bASEDENSITYUOM_ISOField;
            }
            set
            {
                this.bASEDENSITYUOM_ISOField = value;
            }
        }

        
        
        public double BASEHEATINGVALUE
        {
            get
            {
                return this.bASEHEATINGVALUEField;
            }
            set
            {
                this.bASEHEATINGVALUEField = value;
            }
        }

        
        
        public bool BASEHEATINGVALUESpecified
        {
            get
            {
                return this.bASEHEATINGVALUEFieldSpecified;
            }
            set
            {
                this.bASEHEATINGVALUEFieldSpecified = value;
            }
        }

        
        
        public string BASEHEATINGVALUEUOM
        {
            get
            {
                return this.bASEHEATINGVALUEUOMField;
            }
            set
            {
                this.bASEHEATINGVALUEUOMField = value;
            }
        }

        
        
        public string BASEHEATINGVALUEUOM_ISO
        {
            get
            {
                return this.bASEHEATINGVALUEUOM_ISOField;
            }
            set
            {
                this.bASEHEATINGVALUEUOM_ISOField = value;
            }
        }

        
        
        public double TESTTEMPERATURE_DENSITY
        {
            get
            {
                return this.tESTTEMPERATURE_DENSITYField;
            }
            set
            {
                this.tESTTEMPERATURE_DENSITYField = value;
            }
        }

        
        
        public bool TESTTEMPERATURE_DENSITYSpecified
        {
            get
            {
                return this.tESTTEMPERATURE_DENSITYFieldSpecified;
            }
            set
            {
                this.tESTTEMPERATURE_DENSITYFieldSpecified = value;
            }
        }

        
        
        public string TESTTEMP_DENSITY_UOM
        {
            get
            {
                return this.tESTTEMP_DENSITY_UOMField;
            }
            set
            {
                this.tESTTEMP_DENSITY_UOMField = value;
            }
        }

        
        
        public string TESTTEMPDENSITY_UOM_ISO
        {
            get
            {
                return this.tESTTEMPDENSITY_UOM_ISOField;
            }
            set
            {
                this.tESTTEMPDENSITY_UOM_ISOField = value;
            }
        }

        
        
        public double MATERIALTEMPERATURE
        {
            get
            {
                return this.mATERIALTEMPERATUREField;
            }
            set
            {
                this.mATERIALTEMPERATUREField = value;
            }
        }

        
        
        public bool MATERIALTEMPERATURESpecified
        {
            get
            {
                return this.mATERIALTEMPERATUREFieldSpecified;
            }
            set
            {
                this.mATERIALTEMPERATUREFieldSpecified = value;
            }
        }

        
        
        public string MATERIALTEMPERATURE_UOM
        {
            get
            {
                return this.mATERIALTEMPERATURE_UOMField;
            }
            set
            {
                this.mATERIALTEMPERATURE_UOMField = value;
            }
        }

        
        
        public string MATERIALTEMPERATURE_UOM_ISO
        {
            get
            {
                return this.mATERIALTEMPERATURE_UOM_ISOField;
            }
            set
            {
                this.mATERIALTEMPERATURE_UOM_ISOField = value;
            }
        }

        
        
        public double TESTHEATINGVALUE
        {
            get
            {
                return this.tESTHEATINGVALUEField;
            }
            set
            {
                this.tESTHEATINGVALUEField = value;
            }
        }

        
        
        public bool TESTHEATINGVALUESpecified
        {
            get
            {
                return this.tESTHEATINGVALUEFieldSpecified;
            }
            set
            {
                this.tESTHEATINGVALUEFieldSpecified = value;
            }
        }

        
        
        public string TESTHEATINGVALUE_UOM
        {
            get
            {
                return this.tESTHEATINGVALUE_UOMField;
            }
            set
            {
                this.tESTHEATINGVALUE_UOMField = value;
            }
        }

        
        
        public string TESTHEATINGVALUE_UOM_ISO
        {
            get
            {
                return this.tESTHEATINGVALUE_UOM_ISOField;
            }
            set
            {
                this.tESTHEATINGVALUE_UOM_ISOField = value;
            }
        }

        
        
        public double TESTDENSITY
        {
            get
            {
                return this.tESTDENSITYField;
            }
            set
            {
                this.tESTDENSITYField = value;
            }
        }

        
        
        public bool TESTDENSITYSpecified
        {
            get
            {
                return this.tESTDENSITYFieldSpecified;
            }
            set
            {
                this.tESTDENSITYFieldSpecified = value;
            }
        }

        
        
        public string TESTDENSITY_UOM
        {
            get
            {
                return this.tESTDENSITY_UOMField;
            }
            set
            {
                this.tESTDENSITY_UOMField = value;
            }
        }

        
        
        public string TESTDENSITY_UOM_ISO
        {
            get
            {
                return this.tESTDENSITY_UOM_ISOField;
            }
            set
            {
                this.tESTDENSITY_UOM_ISOField = value;
            }
        }

        
        
        public double MATERIALPRESSURE
        {
            get
            {
                return this.mATERIALPRESSUREField;
            }
            set
            {
                this.mATERIALPRESSUREField = value;
            }
        }

        
        
        public bool MATERIALPRESSURESpecified
        {
            get
            {
                return this.mATERIALPRESSUREFieldSpecified;
            }
            set
            {
                this.mATERIALPRESSUREFieldSpecified = value;
            }
        }

        
        
        public string MATERIALPRESSURE_UOM
        {
            get
            {
                return this.mATERIALPRESSURE_UOMField;
            }
            set
            {
                this.mATERIALPRESSURE_UOMField = value;
            }
        }

        
        
        public string MATERIALPRESSURE_UOM_ISO
        {
            get
            {
                return this.mATERIALPRESSURE_UOM_ISOField;
            }
            set
            {
                this.mATERIALPRESSURE_UOM_ISOField = value;
            }
        }

        
        
        public double TESTPRESSURE_DENSITY
        {
            get
            {
                return this.tESTPRESSURE_DENSITYField;
            }
            set
            {
                this.tESTPRESSURE_DENSITYField = value;
            }
        }

        
        
        public bool TESTPRESSURE_DENSITYSpecified
        {
            get
            {
                return this.tESTPRESSURE_DENSITYFieldSpecified;
            }
            set
            {
                this.tESTPRESSURE_DENSITYFieldSpecified = value;
            }
        }

        
        
        public string TESTPRESSDENSITY_UOM
        {
            get
            {
                return this.tESTPRESSDENSITY_UOMField;
            }
            set
            {
                this.tESTPRESSDENSITY_UOMField = value;
            }
        }

        
        
        public string TESTPRESSDENSITY_UOM_ISO
        {
            get
            {
                return this.tESTPRESSDENSITY_UOM_ISOField;
            }
            set
            {
                this.tESTPRESSDENSITY_UOM_ISOField = value;
            }
        }

        
        
        public double TESTPRESSURECOMBUSTION
        {
            get
            {
                return this.tESTPRESSURECOMBUSTIONField;
            }
            set
            {
                this.tESTPRESSURECOMBUSTIONField = value;
            }
        }

        
        
        public bool TESTPRESSURECOMBUSTIONSpecified
        {
            get
            {
                return this.tESTPRESSURECOMBUSTIONFieldSpecified;
            }
            set
            {
                this.tESTPRESSURECOMBUSTIONFieldSpecified = value;
            }
        }

        
        
        public string TESTPRESSURECOMBUSTION_UOM
        {
            get
            {
                return this.tESTPRESSURECOMBUSTION_UOMField;
            }
            set
            {
                this.tESTPRESSURECOMBUSTION_UOMField = value;
            }
        }

        
        
        public string TESTPRESSURECOMB_UOM_ISO
        {
            get
            {
                return this.tESTPRESSURECOMB_UOM_ISOField;
            }
            set
            {
                this.tESTPRESSURECOMB_UOM_ISOField = value;
            }
        }

        
        
        public double TESTTEMPERATURECOMBUSTION
        {
            get
            {
                return this.tESTTEMPERATURECOMBUSTIONField;
            }
            set
            {
                this.tESTTEMPERATURECOMBUSTIONField = value;
            }
        }

        
        
        public bool TESTTEMPERATURECOMBUSTIONSpecified
        {
            get
            {
                return this.tESTTEMPERATURECOMBUSTIONFieldSpecified;
            }
            set
            {
                this.tESTTEMPERATURECOMBUSTIONFieldSpecified = value;
            }
        }

        
        
        public string TESTTEMPERATURECOMB_UOM
        {
            get
            {
                return this.tESTTEMPERATURECOMB_UOMField;
            }
            set
            {
                this.tESTTEMPERATURECOMB_UOMField = value;
            }
        }

        
        
        public string TESTTEMPERATURECOMB_UOM_ISO
        {
            get
            {
                return this.tESTTEMPERATURECOMB_UOM_ISOField;
            }
            set
            {
                this.tESTTEMPERATURECOMB_UOM_ISOField = value;
            }
        }

        
        
        public double ATMOSPHERICPRESSURE
        {
            get
            {
                return this.aTMOSPHERICPRESSUREField;
            }
            set
            {
                this.aTMOSPHERICPRESSUREField = value;
            }
        }

        
        
        public bool ATMOSPHERICPRESSURESpecified
        {
            get
            {
                return this.aTMOSPHERICPRESSUREFieldSpecified;
            }
            set
            {
                this.aTMOSPHERICPRESSUREFieldSpecified = value;
            }
        }

        
        
        public string ATMOSPHERICPRESSURE_UOM
        {
            get
            {
                return this.aTMOSPHERICPRESSURE_UOMField;
            }
            set
            {
                this.aTMOSPHERICPRESSURE_UOMField = value;
            }
        }

        
        
        public string ATMOSPHERICPRESS_UOM_ISO
        {
            get
            {
                return this.aTMOSPHERICPRESS_UOM_ISOField;
            }
            set
            {
                this.aTMOSPHERICPRESS_UOM_ISOField = value;
            }
        }

        
        
        public double VAPORPRESSURE
        {
            get
            {
                return this.vAPORPRESSUREField;
            }
            set
            {
                this.vAPORPRESSUREField = value;
            }
        }

        
        
        public bool VAPORPRESSURESpecified
        {
            get
            {
                return this.vAPORPRESSUREFieldSpecified;
            }
            set
            {
                this.vAPORPRESSUREFieldSpecified = value;
            }
        }

        
        
        public string VAPORPRESSURE_UOM
        {
            get
            {
                return this.vAPORPRESSURE_UOMField;
            }
            set
            {
                this.vAPORPRESSURE_UOMField = value;
            }
        }

        
        
        public string VAPORPRESSURE_UOM_ISO
        {
            get
            {
                return this.vAPORPRESSURE_UOM_ISOField;
            }
            set
            {
                this.vAPORPRESSURE_UOM_ISOField = value;
            }
        }

        
        
        public double MATERIALCOMBUSTIONTEMPERATURE
        {
            get
            {
                return this.mATERIALCOMBUSTIONTEMPERATUREField;
            }
            set
            {
                this.mATERIALCOMBUSTIONTEMPERATUREField = value;
            }
        }

        
        
        public bool MATERIALCOMBUSTIONTEMPERATURESpecified
        {
            get
            {
                return this.mATERIALCOMBUSTIONTEMPERATUREFieldSpecified;
            }
            set
            {
                this.mATERIALCOMBUSTIONTEMPERATUREFieldSpecified = value;
            }
        }

        
        
        public string MATERIALCOMBUSTIONTEMP_UOM
        {
            get
            {
                return this.mATERIALCOMBUSTIONTEMP_UOMField;
            }
            set
            {
                this.mATERIALCOMBUSTIONTEMP_UOMField = value;
            }
        }

        
        
        public string MATERIALCOMBUSTIONTEMP_UOM_ISO
        {
            get
            {
                return this.mATERIALCOMBUSTIONTEMP_UOM_ISOField;
            }
            set
            {
                this.mATERIALCOMBUSTIONTEMP_UOM_ISOField = value;
            }
        }

        
        
        public double MATERIALCOMBUSTIONPRESSURE
        {
            get
            {
                return this.mATERIALCOMBUSTIONPRESSUREField;
            }
            set
            {
                this.mATERIALCOMBUSTIONPRESSUREField = value;
            }
        }

        
        
        public bool MATERIALCOMBUSTIONPRESSURESpecified
        {
            get
            {
                return this.mATERIALCOMBUSTIONPRESSUREFieldSpecified;
            }
            set
            {
                this.mATERIALCOMBUSTIONPRESSUREFieldSpecified = value;
            }
        }

        
        
        public string MATERIALCOMBUSTIONPRESS_UOM
        {
            get
            {
                return this.mATERIALCOMBUSTIONPRESS_UOMField;
            }
            set
            {
                this.mATERIALCOMBUSTIONPRESS_UOMField = value;
            }
        }

        
        
        public string MATERIALCOMBUSTIONPRESS_UOM_IS
        {
            get
            {
                return this.mATERIALCOMBUSTIONPRESS_UOM_ISField;
            }
            set
            {
                this.mATERIALCOMBUSTIONPRESS_UOM_ISField = value;
            }
        }

        
        
        public double TESTTEMPCALHEATVALUE
        {
            get
            {
                return this.tESTTEMPCALHEATVALUEField;
            }
            set
            {
                this.tESTTEMPCALHEATVALUEField = value;
            }
        }

        
        
        public bool TESTTEMPCALHEATVALUESpecified
        {
            get
            {
                return this.tESTTEMPCALHEATVALUEFieldSpecified;
            }
            set
            {
                this.tESTTEMPCALHEATVALUEFieldSpecified = value;
            }
        }

        
        
        public string TESTTEMPCALHEATVALUE_UOM
        {
            get
            {
                return this.tESTTEMPCALHEATVALUE_UOMField;
            }
            set
            {
                this.tESTTEMPCALHEATVALUE_UOMField = value;
            }
        }

        
        
        public string TESTTEMPCALHEATVALUE_UOM_ISO
        {
            get
            {
                return this.tESTTEMPCALHEATVALUE_UOM_ISOField;
            }
            set
            {
                this.tESTTEMPCALHEATVALUE_UOM_ISOField = value;
            }
        }

        
        
        public double TESTPRESSCALHEATVALUE
        {
            get
            {
                return this.tESTPRESSCALHEATVALUEField;
            }
            set
            {
                this.tESTPRESSCALHEATVALUEField = value;
            }
        }

        
        
        public bool TESTPRESSCALHEATVALUESpecified
        {
            get
            {
                return this.tESTPRESSCALHEATVALUEFieldSpecified;
            }
            set
            {
                this.tESTPRESSCALHEATVALUEFieldSpecified = value;
            }
        }

        
        
        public string TESTPRESSCALHEATVALUE_UOM
        {
            get
            {
                return this.tESTPRESSCALHEATVALUE_UOMField;
            }
            set
            {
                this.tESTPRESSCALHEATVALUE_UOMField = value;
            }
        }

        
        
        public string TESTPRESSCALHEATVALUE_UOM_ISO
        {
            get
            {
                return this.tESTPRESSCALHEATVALUE_UOM_ISOField;
            }
            set
            {
                this.tESTPRESSCALHEATVALUE_UOM_ISOField = value;
            }
        }

        
        
        public string HYDROMETERINDICATOR
        {
            get
            {
                return this.hYDROMETERINDICATORField;
            }
            set
            {
                this.hYDROMETERINDICATORField = value;
            }
        }

        
        
        public string AIRBUOYANCYINDICATOR
        {
            get
            {
                return this.aIRBUOYANCYINDICATORField;
            }
            set
            {
                this.aIRBUOYANCYINDICATORField = value;
            }
        }

        
        
        public decimal AIRBUOYANCYCONSTANT
        {
            get
            {
                return this.aIRBUOYANCYCONSTANTField;
            }
            set
            {
                this.aIRBUOYANCYCONSTANTField = value;
            }
        }

        
        
        public bool AIRBUOYANCYCONSTANTSpecified
        {
            get
            {
                return this.aIRBUOYANCYCONSTANTFieldSpecified;
            }
            set
            {
                this.aIRBUOYANCYCONSTANTFieldSpecified = value;
            }
        }

        
        
        public double METERCORRECTIONFACTOR
        {
            get
            {
                return this.mETERCORRECTIONFACTORField;
            }
            set
            {
                this.mETERCORRECTIONFACTORField = value;
            }
        }

        
        
        public bool METERCORRECTIONFACTORSpecified
        {
            get
            {
                return this.mETERCORRECTIONFACTORFieldSpecified;
            }
            set
            {
                this.mETERCORRECTIONFACTORFieldSpecified = value;
            }
        }

        
        
        public double BASESEDIMENTWATERCONTENT
        {
            get
            {
                return this.bASESEDIMENTWATERCONTENTField;
            }
            set
            {
                this.bASESEDIMENTWATERCONTENTField = value;
            }
        }

        
        
        public bool BASESEDIMENTWATERCONTENTSpecified
        {
            get
            {
                return this.bASESEDIMENTWATERCONTENTFieldSpecified;
            }
            set
            {
                this.bASESEDIMENTWATERCONTENTFieldSpecified = value;
            }
        }

        
        
        public string BSW_UOM
        {
            get
            {
                return this.bSW_UOMField;
            }
            set
            {
                this.bSW_UOMField = value;
            }
        }

        
        
        public string BSW_UOM_ISO
        {
            get
            {
                return this.bSW_UOM_ISOField;
            }
            set
            {
                this.bSW_UOM_ISOField = value;
            }
        }

        
        
        public string HEATINGVALUECLASS
        {
            get
            {
                return this.hEATINGVALUECLASSField;
            }
            set
            {
                this.hEATINGVALUECLASSField = value;
            }
        }

        
        
        public string HEATINGVALUECLASS_TO
        {
            get
            {
                return this.hEATINGVALUECLASS_TOField;
            }
            set
            {
                this.hEATINGVALUECLASS_TOField = value;
            }
        }

        
        
        public double CH4FRACTION
        {
            get
            {
                return this.cH4FRACTIONField;
            }
            set
            {
                this.cH4FRACTIONField = value;
            }
        }

        
        
        public bool CH4FRACTIONSpecified
        {
            get
            {
                return this.cH4FRACTIONFieldSpecified;
            }
            set
            {
                this.cH4FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double N2FRACTION
        {
            get
            {
                return this.n2FRACTIONField;
            }
            set
            {
                this.n2FRACTIONField = value;
            }
        }

        
        
        public bool N2FRACTIONSpecified
        {
            get
            {
                return this.n2FRACTIONFieldSpecified;
            }
            set
            {
                this.n2FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double CO2FRACTION
        {
            get
            {
                return this.cO2FRACTIONField;
            }
            set
            {
                this.cO2FRACTIONField = value;
            }
        }

        
        
        public bool CO2FRACTIONSpecified
        {
            get
            {
                return this.cO2FRACTIONFieldSpecified;
            }
            set
            {
                this.cO2FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C2H6FRACTION
        {
            get
            {
                return this.c2H6FRACTIONField;
            }
            set
            {
                this.c2H6FRACTIONField = value;
            }
        }

        
        
        public bool C2H6FRACTIONSpecified
        {
            get
            {
                return this.c2H6FRACTIONFieldSpecified;
            }
            set
            {
                this.c2H6FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C3H8FRACTION
        {
            get
            {
                return this.c3H8FRACTIONField;
            }
            set
            {
                this.c3H8FRACTIONField = value;
            }
        }

        
        
        public bool C3H8FRACTIONSpecified
        {
            get
            {
                return this.c3H8FRACTIONFieldSpecified;
            }
            set
            {
                this.c3H8FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double H2OFRACTION
        {
            get
            {
                return this.h2OFRACTIONField;
            }
            set
            {
                this.h2OFRACTIONField = value;
            }
        }

        
        
        public bool H2OFRACTIONSpecified
        {
            get
            {
                return this.h2OFRACTIONFieldSpecified;
            }
            set
            {
                this.h2OFRACTIONFieldSpecified = value;
            }
        }

        
        
        public double H2SFRACTION
        {
            get
            {
                return this.h2SFRACTIONField;
            }
            set
            {
                this.h2SFRACTIONField = value;
            }
        }

        
        
        public bool H2SFRACTIONSpecified
        {
            get
            {
                return this.h2SFRACTIONFieldSpecified;
            }
            set
            {
                this.h2SFRACTIONFieldSpecified = value;
            }
        }

        
        
        public double H2FRACTION
        {
            get
            {
                return this.h2FRACTIONField;
            }
            set
            {
                this.h2FRACTIONField = value;
            }
        }

        
        
        public bool H2FRACTIONSpecified
        {
            get
            {
                return this.h2FRACTIONFieldSpecified;
            }
            set
            {
                this.h2FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double COFRACTION
        {
            get
            {
                return this.cOFRACTIONField;
            }
            set
            {
                this.cOFRACTIONField = value;
            }
        }

        
        
        public bool COFRACTIONSpecified
        {
            get
            {
                return this.cOFRACTIONFieldSpecified;
            }
            set
            {
                this.cOFRACTIONFieldSpecified = value;
            }
        }

        
        
        public double O2FRACTION
        {
            get
            {
                return this.o2FRACTIONField;
            }
            set
            {
                this.o2FRACTIONField = value;
            }
        }

        
        
        public bool O2FRACTIONSpecified
        {
            get
            {
                return this.o2FRACTIONFieldSpecified;
            }
            set
            {
                this.o2FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double ISOC4H10FRACTION
        {
            get
            {
                return this.iSOC4H10FRACTIONField;
            }
            set
            {
                this.iSOC4H10FRACTIONField = value;
            }
        }

        
        
        public bool ISOC4H10FRACTIONSpecified
        {
            get
            {
                return this.iSOC4H10FRACTIONFieldSpecified;
            }
            set
            {
                this.iSOC4H10FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double NC4H10FRACTION
        {
            get
            {
                return this.nC4H10FRACTIONField;
            }
            set
            {
                this.nC4H10FRACTIONField = value;
            }
        }

        
        
        public bool NC4H10FRACTIONSpecified
        {
            get
            {
                return this.nC4H10FRACTIONFieldSpecified;
            }
            set
            {
                this.nC4H10FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double ISOC5H12FRACTION
        {
            get
            {
                return this.iSOC5H12FRACTIONField;
            }
            set
            {
                this.iSOC5H12FRACTIONField = value;
            }
        }

        
        
        public bool ISOC5H12FRACTIONSpecified
        {
            get
            {
                return this.iSOC5H12FRACTIONFieldSpecified;
            }
            set
            {
                this.iSOC5H12FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double NC5H12FRACTION
        {
            get
            {
                return this.nC5H12FRACTIONField;
            }
            set
            {
                this.nC5H12FRACTIONField = value;
            }
        }

        
        
        public bool NC5H12FRACTIONSpecified
        {
            get
            {
                return this.nC5H12FRACTIONFieldSpecified;
            }
            set
            {
                this.nC5H12FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C6H14FRACTION
        {
            get
            {
                return this.c6H14FRACTIONField;
            }
            set
            {
                this.c6H14FRACTIONField = value;
            }
        }

        
        
        public bool C6H14FRACTIONSpecified
        {
            get
            {
                return this.c6H14FRACTIONFieldSpecified;
            }
            set
            {
                this.c6H14FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C7H16FRACTION
        {
            get
            {
                return this.c7H16FRACTIONField;
            }
            set
            {
                this.c7H16FRACTIONField = value;
            }
        }

        
        
        public bool C7H16FRACTIONSpecified
        {
            get
            {
                return this.c7H16FRACTIONFieldSpecified;
            }
            set
            {
                this.c7H16FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C8H18FRACTION
        {
            get
            {
                return this.c8H18FRACTIONField;
            }
            set
            {
                this.c8H18FRACTIONField = value;
            }
        }

        
        
        public bool C8H18FRACTIONSpecified
        {
            get
            {
                return this.c8H18FRACTIONFieldSpecified;
            }
            set
            {
                this.c8H18FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C9H20FRACTION
        {
            get
            {
                return this.c9H20FRACTIONField;
            }
            set
            {
                this.c9H20FRACTIONField = value;
            }
        }

        
        
        public bool C9H20FRACTIONSpecified
        {
            get
            {
                return this.c9H20FRACTIONFieldSpecified;
            }
            set
            {
                this.c9H20FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double C10H22FRACTION
        {
            get
            {
                return this.c10H22FRACTIONField;
            }
            set
            {
                this.c10H22FRACTIONField = value;
            }
        }

        
        
        public bool C10H22FRACTIONSpecified
        {
            get
            {
                return this.c10H22FRACTIONFieldSpecified;
            }
            set
            {
                this.c10H22FRACTIONFieldSpecified = value;
            }
        }

        
        
        public double HELIUMFRACTION
        {
            get
            {
                return this.hELIUMFRACTIONField;
            }
            set
            {
                this.hELIUMFRACTIONField = value;
            }
        }

        
        
        public bool HELIUMFRACTIONSpecified
        {
            get
            {
                return this.hELIUMFRACTIONFieldSpecified;
            }
            set
            {
                this.hELIUMFRACTIONFieldSpecified = value;
            }
        }

        
        
        public double ARGONFRACTION
        {
            get
            {
                return this.aRGONFRACTIONField;
            }
            set
            {
                this.aRGONFRACTIONField = value;
            }
        }

        
        
        public bool ARGONFRACTIONSpecified
        {
            get
            {
                return this.aRGONFRACTIONFieldSpecified;
            }
            set
            {
                this.aRGONFRACTIONFieldSpecified = value;
            }
        }

        
        
        public string GASCOMPOSITIONFRACTION_UOM
        {
            get
            {
                return this.gASCOMPOSITIONFRACTION_UOMField;
            }
            set
            {
                this.gASCOMPOSITIONFRACTION_UOMField = value;
            }
        }

        
        
        public string GASCOMPOSITIONFRACTION_UOM_ISO
        {
            get
            {
                return this.gASCOMPOSITIONFRACTION_UOM_ISOField;
            }
            set
            {
                this.gASCOMPOSITIONFRACTION_UOM_ISOField = value;
            }
        }

        
        
        public double MOLARDENSITY
        {
            get
            {
                return this.mOLARDENSITYField;
            }
            set
            {
                this.mOLARDENSITYField = value;
            }
        }

        
        
        public bool MOLARDENSITYSpecified
        {
            get
            {
                return this.mOLARDENSITYFieldSpecified;
            }
            set
            {
                this.mOLARDENSITYFieldSpecified = value;
            }
        }

        
        
        public string MOLARDENSITY_UOM
        {
            get
            {
                return this.mOLARDENSITY_UOMField;
            }
            set
            {
                this.mOLARDENSITY_UOMField = value;
            }
        }

        
        
        public string MOLARDENSITY_UOM_ISO
        {
            get
            {
                return this.mOLARDENSITY_UOM_ISOField;
            }
            set
            {
                this.mOLARDENSITY_UOM_ISOField = value;
            }
        }

        
        
        public double MOLECULARWEIGHT
        {
            get
            {
                return this.mOLECULARWEIGHTField;
            }
            set
            {
                this.mOLECULARWEIGHTField = value;
            }
        }

        
        
        public bool MOLECULARWEIGHTSpecified
        {
            get
            {
                return this.mOLECULARWEIGHTFieldSpecified;
            }
            set
            {
                this.mOLECULARWEIGHTFieldSpecified = value;
            }
        }

        
        
        public string MOLECULARWEIGHT_UOM
        {
            get
            {
                return this.mOLECULARWEIGHT_UOMField;
            }
            set
            {
                this.mOLECULARWEIGHT_UOMField = value;
            }
        }

        
        
        public string MOLECULARWEIGHT_UOM_ISO
        {
            get
            {
                return this.mOLECULARWEIGHT_UOM_ISOField;
            }
            set
            {
                this.mOLECULARWEIGHT_UOM_ISOField = value;
            }
        }

        
        
        public double HYDROCARBONMOLFRACTION
        {
            get
            {
                return this.hYDROCARBONMOLFRACTIONField;
            }
            set
            {
                this.hYDROCARBONMOLFRACTIONField = value;
            }
        }

        
        
        public bool HYDROCARBONMOLFRACTIONSpecified
        {
            get
            {
                return this.hYDROCARBONMOLFRACTIONFieldSpecified;
            }
            set
            {
                this.hYDROCARBONMOLFRACTIONFieldSpecified = value;
            }
        }

        
        
        public string CUSTOMERPCHAR1
        {
            get
            {
                return this.cUSTOMERPCHAR1Field;
            }
            set
            {
                this.cUSTOMERPCHAR1Field = value;
            }
        }

        
        
        public double CUSTOMERPFLOAT1
        {
            get
            {
                return this.cUSTOMERPFLOAT1Field;
            }
            set
            {
                this.cUSTOMERPFLOAT1Field = value;
            }
        }

        
        
        public bool CUSTOMERPFLOAT1Specified
        {
            get
            {
                return this.cUSTOMERPFLOAT1FieldSpecified;
            }
            set
            {
                this.cUSTOMERPFLOAT1FieldSpecified = value;
            }
        }

        
        
        public string CUSTOMERP1_UOM
        {
            get
            {
                return this.cUSTOMERP1_UOMField;
            }
            set
            {
                this.cUSTOMERP1_UOMField = value;
            }
        }

        
        
        public string CUSTOMERP1_UOM_ISO
        {
            get
            {
                return this.cUSTOMERP1_UOM_ISOField;
            }
            set
            {
                this.cUSTOMERP1_UOM_ISOField = value;
            }
        }

        
        
        public string CUSTOMERPCHAR2
        {
            get
            {
                return this.cUSTOMERPCHAR2Field;
            }
            set
            {
                this.cUSTOMERPCHAR2Field = value;
            }
        }

        
        
        public double CUSTOMERPFLOAT2
        {
            get
            {
                return this.cUSTOMERPFLOAT2Field;
            }
            set
            {
                this.cUSTOMERPFLOAT2Field = value;
            }
        }

        
        
        public bool CUSTOMERPFLOAT2Specified
        {
            get
            {
                return this.cUSTOMERPFLOAT2FieldSpecified;
            }
            set
            {
                this.cUSTOMERPFLOAT2FieldSpecified = value;
            }
        }

        
        
        public string CUSTOMERP2_UOM
        {
            get
            {
                return this.cUSTOMERP2_UOMField;
            }
            set
            {
                this.cUSTOMERP2_UOMField = value;
            }
        }

        
        
        public string CUSTOMERP2_UOM_ISO
        {
            get
            {
                return this.cUSTOMERP2_UOM_ISOField;
            }
            set
            {
                this.cUSTOMERP2_UOM_ISOField = value;
            }
        }

        
        
        public string CUSTOMERPCHAR3
        {
            get
            {
                return this.cUSTOMERPCHAR3Field;
            }
            set
            {
                this.cUSTOMERPCHAR3Field = value;
            }
        }

        
        
        public double CUSTOMERPFLOAT3
        {
            get
            {
                return this.cUSTOMERPFLOAT3Field;
            }
            set
            {
                this.cUSTOMERPFLOAT3Field = value;
            }
        }

        
        
        public bool CUSTOMERPFLOAT3Specified
        {
            get
            {
                return this.cUSTOMERPFLOAT3FieldSpecified;
            }
            set
            {
                this.cUSTOMERPFLOAT3FieldSpecified = value;
            }
        }

        
        
        public string CUSTOMERP3_UOM
        {
            get
            {
                return this.cUSTOMERP3_UOMField;
            }
            set
            {
                this.cUSTOMERP3_UOMField = value;
            }
        }

        
        
        public string CUSTOMERP3_UOM_ISO
        {
            get
            {
                return this.cUSTOMERP3_UOM_ISOField;
            }
            set
            {
                this.cUSTOMERP3_UOM_ISOField = value;
            }
        }
    }
    public class ZGOODSMVT_ITEM_01
    {

        private string lINE_IDField;

        private string pARENT_IDField;

        private string lINE_DEPTHField;

        private string mATERIALField;

        private string pLANTField;

        private string sTGE_LOCField;

        private string bATCHField;

        private string mOVE_TYPEField;

        private string sTCK_TYPEField;

        private string sPEC_STOCKField;

        private string vENDORField;

        private string cUSTOMERField;

        private string sALES_ORDField;

        private string s_ORD_ITEMField;

        private string sCHED_LINEField;

        private string vAL_TYPEField;

        private decimal eNTRY_QNTField;

        private bool eNTRY_QNTFieldSpecified;

        private string eNTRY_UOMField;

        private string eNTRY_UOM_ISOField;

        private decimal pO_PR_QNTField;

        private bool pO_PR_QNTFieldSpecified;

        private string oRDERPR_UNField;

        private string oRDERPR_UN_ISOField;

        private string pO_NUMBERField;

        private string pO_ITEMField;

        private string sHIPPINGField;

        private string cOMP_SHIPField;

        private string nO_MORE_GRField;

        private string iTEM_TEXTField;

        private string gR_RCPTField;

        private string uNLOAD_PTField;

        private string cOSTCENTERField;

        private string oRDERIDField;

        private string oRDER_ITNOField;

        private string cALC_MOTIVEField;

        private string aSSET_NOField;

        private string sUB_NUMBERField;

        private string rESERV_NOField;

        private string rES_ITEMField;

        private string rES_TYPEField;

        private string wITHDRAWNField;

        private string mOVE_MATField;

        private string mOVE_PLANTField;

        private string mOVE_STLOCField;

        private string mOVE_BATCHField;

        private string mOVE_VAL_TYPEField;

        private string mVT_INDField;

        private string mOVE_REASField;

        private string rL_EST_KEYField;

        private string rEF_DATEField;

        private string cOST_OBJField;

        private string pROFIT_SEGM_NOField;

        private string pROFIT_CTRField;

        private string wBS_ELEMField;

        private string nETWORKField;

        private string aCTIVITYField;

        private string pART_ACCTField;

        private decimal aMOUNT_LCField;

        private bool aMOUNT_LCFieldSpecified;

        private decimal aMOUNT_SVField;

        private bool aMOUNT_SVFieldSpecified;

        private string rEF_DOC_YRField;

        private string rEF_DOCField;

        private string rEF_DOC_ITField;

        private string eXPIRYDATEField;

        private string pROD_DATEField;

        private string fUNDField;

        private string fUNDS_CTRField;

        private string cMMT_ITEMField;

        private string vAL_SALES_ORDField;

        private string vAL_S_ORD_ITEMField;

        private string vAL_WBS_ELEMField;

        private string gL_ACCOUNTField;

        private string iND_PROPOSE_QUANXField;

        private string xSTOBField;

        private string eAN_UPCField;

        private string dELIV_NUMB_TO_SEARCHField;

        private string dELIV_ITEM_TO_SEARCHField;

        private string sERIALNO_AUTO_NUMBERASSIGNMENTField;

        private string vENDRBATCHField;

        private string sTGE_TYPEField;

        private string sTGE_BINField;

        private decimal sU_PL_STCK_1Field;

        private bool sU_PL_STCK_1FieldSpecified;

        private decimal sT_UN_QTYY_1Field;

        private bool sT_UN_QTYY_1FieldSpecified;

        private string sT_UN_QTYY_1_ISOField;

        private string uNITTYPE_1Field;

        private decimal sU_PL_STCK_2Field;

        private bool sU_PL_STCK_2FieldSpecified;

        private decimal sT_UN_QTYY_2Field;

        private bool sT_UN_QTYY_2FieldSpecified;

        private string sT_UN_QTYY_2_ISOField;

        private string uNITTYPE_2Field;

        private string sTGE_TYPE_PCField;

        private string sTGE_BIN_PCField;

        private string nO_PST_CHGNTField;

        private string gR_NUMBERField;

        private string sTGE_TYPE_STField;

        private string sTGE_BIN_STField;

        private string mATDOC_TR_CANCELField;

        private string mATITEM_TR_CANCELField;

        private string mATYEAR_TR_CANCELField;

        private string nO_TRANSFER_REQField;

        private string cO_BUSPROCField;

        private string aCTTYPEField;

        private string sUPPL_VENDField;

        private string mATERIAL_EXTERNALField;

        private string mATERIAL_GUIDField;

        private string mATERIAL_VERSIONField;

        private string mOVE_MAT_EXTERNALField;

        private string mOVE_MAT_GUIDField;

        private string mOVE_MAT_VERSIONField;

        private string fUNC_AREAField;

        private string tR_PART_BAField;

        private string pAR_COMPCOField;

        private string dELIV_NUMBField;

        private string dELIV_ITEMField;

        private string nB_SLIPSField;

        private string nB_SLIPSXField;

        private string gR_RCPTXField;

        private string uNLOAD_PTXField;

        private string sPEC_MVMTField;

        private string gRANT_NBRField;

        private string cMMT_ITEM_LONGField;

        private string fUNC_AREA_LONGField;

        private decimal qUANTITYField;

        private bool qUANTITYFieldSpecified;

        private string bASE_UOMField;

        private string bASE_UOM_ISOField;

        private string bL_DATEField;

        private decimal l15Field;

        private bool l15FieldSpecified;

        private decimal l30Field;

        private bool l30FieldSpecified;

        private decimal lField;

        private bool lFieldSpecified;

        private decimal lTOField;

        private bool lTOFieldSpecified;

        private decimal mtField;

        private bool mtFieldSpecified;

        private decimal bB6Field;

        private bool bB6FieldSpecified;

        private decimal kgField;

        private bool kgFieldSpecified;

        private decimal aPIField;

        private bool aPIFieldSpecified;

        private decimal tEMPERField;

        private bool tEMPERFieldSpecified;

        private decimal dENField;

        private bool dENFieldSpecified;

        private decimal bSWField;

        private bool bSWFieldSpecified;

        
        
        public string LINE_ID
        {
            get
            {
                return this.lINE_IDField;
            }
            set
            {
                this.lINE_IDField = value;
            }
        }

        
        
        public string PARENT_ID
        {
            get
            {
                return this.pARENT_IDField;
            }
            set
            {
                this.pARENT_IDField = value;
            }
        }

        
        
        public string LINE_DEPTH
        {
            get
            {
                return this.lINE_DEPTHField;
            }
            set
            {
                this.lINE_DEPTHField = value;
            }
        }

        
        
        public string MATERIAL
        {
            get
            {
                return this.mATERIALField;
            }
            set
            {
                this.mATERIALField = value;
            }
        }

        
        
        public string PLANT
        {
            get
            {
                return this.pLANTField;
            }
            set
            {
                this.pLANTField = value;
            }
        }

        
        
        public string STGE_LOC
        {
            get
            {
                return this.sTGE_LOCField;
            }
            set
            {
                this.sTGE_LOCField = value;
            }
        }

        
        
        public string BATCH
        {
            get
            {
                return this.bATCHField;
            }
            set
            {
                this.bATCHField = value;
            }
        }

        
        
        public string MOVE_TYPE
        {
            get
            {
                return this.mOVE_TYPEField;
            }
            set
            {
                this.mOVE_TYPEField = value;
            }
        }

        
        
        public string STCK_TYPE
        {
            get
            {
                return this.sTCK_TYPEField;
            }
            set
            {
                this.sTCK_TYPEField = value;
            }
        }

        
        
        public string SPEC_STOCK
        {
            get
            {
                return this.sPEC_STOCKField;
            }
            set
            {
                this.sPEC_STOCKField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string CUSTOMER
        {
            get
            {
                return this.cUSTOMERField;
            }
            set
            {
                this.cUSTOMERField = value;
            }
        }

        
        
        public string SALES_ORD
        {
            get
            {
                return this.sALES_ORDField;
            }
            set
            {
                this.sALES_ORDField = value;
            }
        }

        
        
        public string S_ORD_ITEM
        {
            get
            {
                return this.s_ORD_ITEMField;
            }
            set
            {
                this.s_ORD_ITEMField = value;
            }
        }

        
        
        public string SCHED_LINE
        {
            get
            {
                return this.sCHED_LINEField;
            }
            set
            {
                this.sCHED_LINEField = value;
            }
        }

        
        
        public string VAL_TYPE
        {
            get
            {
                return this.vAL_TYPEField;
            }
            set
            {
                this.vAL_TYPEField = value;
            }
        }

        
        
        public decimal ENTRY_QNT
        {
            get
            {
                return this.eNTRY_QNTField;
            }
            set
            {
                this.eNTRY_QNTField = value;
            }
        }

        
        
        public bool ENTRY_QNTSpecified
        {
            get
            {
                return this.eNTRY_QNTFieldSpecified;
            }
            set
            {
                this.eNTRY_QNTFieldSpecified = value;
            }
        }

        
        
        public string ENTRY_UOM
        {
            get
            {
                return this.eNTRY_UOMField;
            }
            set
            {
                this.eNTRY_UOMField = value;
            }
        }

        
        
        public string ENTRY_UOM_ISO
        {
            get
            {
                return this.eNTRY_UOM_ISOField;
            }
            set
            {
                this.eNTRY_UOM_ISOField = value;
            }
        }

        
        
        public decimal PO_PR_QNT
        {
            get
            {
                return this.pO_PR_QNTField;
            }
            set
            {
                this.pO_PR_QNTField = value;
            }
        }

        
        
        public bool PO_PR_QNTSpecified
        {
            get
            {
                return this.pO_PR_QNTFieldSpecified;
            }
            set
            {
                this.pO_PR_QNTFieldSpecified = value;
            }
        }

        
        
        public string ORDERPR_UN
        {
            get
            {
                return this.oRDERPR_UNField;
            }
            set
            {
                this.oRDERPR_UNField = value;
            }
        }

        
        
        public string ORDERPR_UN_ISO
        {
            get
            {
                return this.oRDERPR_UN_ISOField;
            }
            set
            {
                this.oRDERPR_UN_ISOField = value;
            }
        }

        
        
        public string PO_NUMBER
        {
            get
            {
                return this.pO_NUMBERField;
            }
            set
            {
                this.pO_NUMBERField = value;
            }
        }

        
        
        public string PO_ITEM
        {
            get
            {
                return this.pO_ITEMField;
            }
            set
            {
                this.pO_ITEMField = value;
            }
        }

        
        
        public string SHIPPING
        {
            get
            {
                return this.sHIPPINGField;
            }
            set
            {
                this.sHIPPINGField = value;
            }
        }

        
        
        public string COMP_SHIP
        {
            get
            {
                return this.cOMP_SHIPField;
            }
            set
            {
                this.cOMP_SHIPField = value;
            }
        }

        
        
        public string NO_MORE_GR
        {
            get
            {
                return this.nO_MORE_GRField;
            }
            set
            {
                this.nO_MORE_GRField = value;
            }
        }

        
        
        public string ITEM_TEXT
        {
            get
            {
                return this.iTEM_TEXTField;
            }
            set
            {
                this.iTEM_TEXTField = value;
            }
        }

        
        
        public string GR_RCPT
        {
            get
            {
                return this.gR_RCPTField;
            }
            set
            {
                this.gR_RCPTField = value;
            }
        }

        
        
        public string UNLOAD_PT
        {
            get
            {
                return this.uNLOAD_PTField;
            }
            set
            {
                this.uNLOAD_PTField = value;
            }
        }

        
        
        public string COSTCENTER
        {
            get
            {
                return this.cOSTCENTERField;
            }
            set
            {
                this.cOSTCENTERField = value;
            }
        }

        
        
        public string ORDERID
        {
            get
            {
                return this.oRDERIDField;
            }
            set
            {
                this.oRDERIDField = value;
            }
        }

        
        
        public string ORDER_ITNO
        {
            get
            {
                return this.oRDER_ITNOField;
            }
            set
            {
                this.oRDER_ITNOField = value;
            }
        }

        
        
        public string CALC_MOTIVE
        {
            get
            {
                return this.cALC_MOTIVEField;
            }
            set
            {
                this.cALC_MOTIVEField = value;
            }
        }

        
        
        public string ASSET_NO
        {
            get
            {
                return this.aSSET_NOField;
            }
            set
            {
                this.aSSET_NOField = value;
            }
        }

        
        
        public string SUB_NUMBER
        {
            get
            {
                return this.sUB_NUMBERField;
            }
            set
            {
                this.sUB_NUMBERField = value;
            }
        }

        
        
        public string RESERV_NO
        {
            get
            {
                return this.rESERV_NOField;
            }
            set
            {
                this.rESERV_NOField = value;
            }
        }

        
        
        public string RES_ITEM
        {
            get
            {
                return this.rES_ITEMField;
            }
            set
            {
                this.rES_ITEMField = value;
            }
        }

        
        
        public string RES_TYPE
        {
            get
            {
                return this.rES_TYPEField;
            }
            set
            {
                this.rES_TYPEField = value;
            }
        }

        
        
        public string WITHDRAWN
        {
            get
            {
                return this.wITHDRAWNField;
            }
            set
            {
                this.wITHDRAWNField = value;
            }
        }

        
        
        public string MOVE_MAT
        {
            get
            {
                return this.mOVE_MATField;
            }
            set
            {
                this.mOVE_MATField = value;
            }
        }

        
        
        public string MOVE_PLANT
        {
            get
            {
                return this.mOVE_PLANTField;
            }
            set
            {
                this.mOVE_PLANTField = value;
            }
        }

        
        
        public string MOVE_STLOC
        {
            get
            {
                return this.mOVE_STLOCField;
            }
            set
            {
                this.mOVE_STLOCField = value;
            }
        }

        
        
        public string MOVE_BATCH
        {
            get
            {
                return this.mOVE_BATCHField;
            }
            set
            {
                this.mOVE_BATCHField = value;
            }
        }

        
        
        public string MOVE_VAL_TYPE
        {
            get
            {
                return this.mOVE_VAL_TYPEField;
            }
            set
            {
                this.mOVE_VAL_TYPEField = value;
            }
        }

        
        
        public string MVT_IND
        {
            get
            {
                return this.mVT_INDField;
            }
            set
            {
                this.mVT_INDField = value;
            }
        }

        
        
        public string MOVE_REAS
        {
            get
            {
                return this.mOVE_REASField;
            }
            set
            {
                this.mOVE_REASField = value;
            }
        }

        
        
        public string RL_EST_KEY
        {
            get
            {
                return this.rL_EST_KEYField;
            }
            set
            {
                this.rL_EST_KEYField = value;
            }
        }

        
        
        public string REF_DATE
        {
            get
            {
                return this.rEF_DATEField;
            }
            set
            {
                this.rEF_DATEField = value;
            }
        }

        
        
        public string COST_OBJ
        {
            get
            {
                return this.cOST_OBJField;
            }
            set
            {
                this.cOST_OBJField = value;
            }
        }

        
        
        public string PROFIT_SEGM_NO
        {
            get
            {
                return this.pROFIT_SEGM_NOField;
            }
            set
            {
                this.pROFIT_SEGM_NOField = value;
            }
        }

        
        
        public string PROFIT_CTR
        {
            get
            {
                return this.pROFIT_CTRField;
            }
            set
            {
                this.pROFIT_CTRField = value;
            }
        }

        
        
        public string WBS_ELEM
        {
            get
            {
                return this.wBS_ELEMField;
            }
            set
            {
                this.wBS_ELEMField = value;
            }
        }

        
        
        public string NETWORK
        {
            get
            {
                return this.nETWORKField;
            }
            set
            {
                this.nETWORKField = value;
            }
        }

        
        
        public string ACTIVITY
        {
            get
            {
                return this.aCTIVITYField;
            }
            set
            {
                this.aCTIVITYField = value;
            }
        }

        
        
        public string PART_ACCT
        {
            get
            {
                return this.pART_ACCTField;
            }
            set
            {
                this.pART_ACCTField = value;
            }
        }

        
        
        public decimal AMOUNT_LC
        {
            get
            {
                return this.aMOUNT_LCField;
            }
            set
            {
                this.aMOUNT_LCField = value;
            }
        }

        
        
        public bool AMOUNT_LCSpecified
        {
            get
            {
                return this.aMOUNT_LCFieldSpecified;
            }
            set
            {
                this.aMOUNT_LCFieldSpecified = value;
            }
        }

        
        
        public decimal AMOUNT_SV
        {
            get
            {
                return this.aMOUNT_SVField;
            }
            set
            {
                this.aMOUNT_SVField = value;
            }
        }

        
        
        public bool AMOUNT_SVSpecified
        {
            get
            {
                return this.aMOUNT_SVFieldSpecified;
            }
            set
            {
                this.aMOUNT_SVFieldSpecified = value;
            }
        }

        
        
        public string REF_DOC_YR
        {
            get
            {
                return this.rEF_DOC_YRField;
            }
            set
            {
                this.rEF_DOC_YRField = value;
            }
        }

        
        
        public string REF_DOC
        {
            get
            {
                return this.rEF_DOCField;
            }
            set
            {
                this.rEF_DOCField = value;
            }
        }

        
        
        public string REF_DOC_IT
        {
            get
            {
                return this.rEF_DOC_ITField;
            }
            set
            {
                this.rEF_DOC_ITField = value;
            }
        }

        
        
        public string EXPIRYDATE
        {
            get
            {
                return this.eXPIRYDATEField;
            }
            set
            {
                this.eXPIRYDATEField = value;
            }
        }

        
        
        public string PROD_DATE
        {
            get
            {
                return this.pROD_DATEField;
            }
            set
            {
                this.pROD_DATEField = value;
            }
        }

        
        
        public string FUND
        {
            get
            {
                return this.fUNDField;
            }
            set
            {
                this.fUNDField = value;
            }
        }

        
        
        public string FUNDS_CTR
        {
            get
            {
                return this.fUNDS_CTRField;
            }
            set
            {
                this.fUNDS_CTRField = value;
            }
        }

        
        
        public string CMMT_ITEM
        {
            get
            {
                return this.cMMT_ITEMField;
            }
            set
            {
                this.cMMT_ITEMField = value;
            }
        }

        
        
        public string VAL_SALES_ORD
        {
            get
            {
                return this.vAL_SALES_ORDField;
            }
            set
            {
                this.vAL_SALES_ORDField = value;
            }
        }

        
        
        public string VAL_S_ORD_ITEM
        {
            get
            {
                return this.vAL_S_ORD_ITEMField;
            }
            set
            {
                this.vAL_S_ORD_ITEMField = value;
            }
        }

        
        
        public string VAL_WBS_ELEM
        {
            get
            {
                return this.vAL_WBS_ELEMField;
            }
            set
            {
                this.vAL_WBS_ELEMField = value;
            }
        }

        
        
        public string GL_ACCOUNT
        {
            get
            {
                return this.gL_ACCOUNTField;
            }
            set
            {
                this.gL_ACCOUNTField = value;
            }
        }

        
        
        public string IND_PROPOSE_QUANX
        {
            get
            {
                return this.iND_PROPOSE_QUANXField;
            }
            set
            {
                this.iND_PROPOSE_QUANXField = value;
            }
        }

        
        
        public string XSTOB
        {
            get
            {
                return this.xSTOBField;
            }
            set
            {
                this.xSTOBField = value;
            }
        }

        
        
        public string EAN_UPC
        {
            get
            {
                return this.eAN_UPCField;
            }
            set
            {
                this.eAN_UPCField = value;
            }
        }

        
        
        public string DELIV_NUMB_TO_SEARCH
        {
            get
            {
                return this.dELIV_NUMB_TO_SEARCHField;
            }
            set
            {
                this.dELIV_NUMB_TO_SEARCHField = value;
            }
        }

        
        
        public string DELIV_ITEM_TO_SEARCH
        {
            get
            {
                return this.dELIV_ITEM_TO_SEARCHField;
            }
            set
            {
                this.dELIV_ITEM_TO_SEARCHField = value;
            }
        }

        
        
        public string SERIALNO_AUTO_NUMBERASSIGNMENT
        {
            get
            {
                return this.sERIALNO_AUTO_NUMBERASSIGNMENTField;
            }
            set
            {
                this.sERIALNO_AUTO_NUMBERASSIGNMENTField = value;
            }
        }

        
        
        public string VENDRBATCH
        {
            get
            {
                return this.vENDRBATCHField;
            }
            set
            {
                this.vENDRBATCHField = value;
            }
        }

        
        
        public string STGE_TYPE
        {
            get
            {
                return this.sTGE_TYPEField;
            }
            set
            {
                this.sTGE_TYPEField = value;
            }
        }

        
        
        public string STGE_BIN
        {
            get
            {
                return this.sTGE_BINField;
            }
            set
            {
                this.sTGE_BINField = value;
            }
        }

        
        
        public decimal SU_PL_STCK_1
        {
            get
            {
                return this.sU_PL_STCK_1Field;
            }
            set
            {
                this.sU_PL_STCK_1Field = value;
            }
        }

        
        
        public bool SU_PL_STCK_1Specified
        {
            get
            {
                return this.sU_PL_STCK_1FieldSpecified;
            }
            set
            {
                this.sU_PL_STCK_1FieldSpecified = value;
            }
        }

        
        
        public decimal ST_UN_QTYY_1
        {
            get
            {
                return this.sT_UN_QTYY_1Field;
            }
            set
            {
                this.sT_UN_QTYY_1Field = value;
            }
        }

        
        
        public bool ST_UN_QTYY_1Specified
        {
            get
            {
                return this.sT_UN_QTYY_1FieldSpecified;
            }
            set
            {
                this.sT_UN_QTYY_1FieldSpecified = value;
            }
        }

        
        
        public string ST_UN_QTYY_1_ISO
        {
            get
            {
                return this.sT_UN_QTYY_1_ISOField;
            }
            set
            {
                this.sT_UN_QTYY_1_ISOField = value;
            }
        }

        
        
        public string UNITTYPE_1
        {
            get
            {
                return this.uNITTYPE_1Field;
            }
            set
            {
                this.uNITTYPE_1Field = value;
            }
        }

        
        
        public decimal SU_PL_STCK_2
        {
            get
            {
                return this.sU_PL_STCK_2Field;
            }
            set
            {
                this.sU_PL_STCK_2Field = value;
            }
        }

        
        
        public bool SU_PL_STCK_2Specified
        {
            get
            {
                return this.sU_PL_STCK_2FieldSpecified;
            }
            set
            {
                this.sU_PL_STCK_2FieldSpecified = value;
            }
        }

        
        
        public decimal ST_UN_QTYY_2
        {
            get
            {
                return this.sT_UN_QTYY_2Field;
            }
            set
            {
                this.sT_UN_QTYY_2Field = value;
            }
        }

        
        
        public bool ST_UN_QTYY_2Specified
        {
            get
            {
                return this.sT_UN_QTYY_2FieldSpecified;
            }
            set
            {
                this.sT_UN_QTYY_2FieldSpecified = value;
            }
        }

        
        
        public string ST_UN_QTYY_2_ISO
        {
            get
            {
                return this.sT_UN_QTYY_2_ISOField;
            }
            set
            {
                this.sT_UN_QTYY_2_ISOField = value;
            }
        }

        
        
        public string UNITTYPE_2
        {
            get
            {
                return this.uNITTYPE_2Field;
            }
            set
            {
                this.uNITTYPE_2Field = value;
            }
        }

        
        
        public string STGE_TYPE_PC
        {
            get
            {
                return this.sTGE_TYPE_PCField;
            }
            set
            {
                this.sTGE_TYPE_PCField = value;
            }
        }

        
        
        public string STGE_BIN_PC
        {
            get
            {
                return this.sTGE_BIN_PCField;
            }
            set
            {
                this.sTGE_BIN_PCField = value;
            }
        }

        
        
        public string NO_PST_CHGNT
        {
            get
            {
                return this.nO_PST_CHGNTField;
            }
            set
            {
                this.nO_PST_CHGNTField = value;
            }
        }

        
        
        public string GR_NUMBER
        {
            get
            {
                return this.gR_NUMBERField;
            }
            set
            {
                this.gR_NUMBERField = value;
            }
        }

        
        
        public string STGE_TYPE_ST
        {
            get
            {
                return this.sTGE_TYPE_STField;
            }
            set
            {
                this.sTGE_TYPE_STField = value;
            }
        }

        
        
        public string STGE_BIN_ST
        {
            get
            {
                return this.sTGE_BIN_STField;
            }
            set
            {
                this.sTGE_BIN_STField = value;
            }
        }

        
        
        public string MATDOC_TR_CANCEL
        {
            get
            {
                return this.mATDOC_TR_CANCELField;
            }
            set
            {
                this.mATDOC_TR_CANCELField = value;
            }
        }

        
        
        public string MATITEM_TR_CANCEL
        {
            get
            {
                return this.mATITEM_TR_CANCELField;
            }
            set
            {
                this.mATITEM_TR_CANCELField = value;
            }
        }

        
        
        public string MATYEAR_TR_CANCEL
        {
            get
            {
                return this.mATYEAR_TR_CANCELField;
            }
            set
            {
                this.mATYEAR_TR_CANCELField = value;
            }
        }

        
        
        public string NO_TRANSFER_REQ
        {
            get
            {
                return this.nO_TRANSFER_REQField;
            }
            set
            {
                this.nO_TRANSFER_REQField = value;
            }
        }

        
        
        public string CO_BUSPROC
        {
            get
            {
                return this.cO_BUSPROCField;
            }
            set
            {
                this.cO_BUSPROCField = value;
            }
        }

        
        
        public string ACTTYPE
        {
            get
            {
                return this.aCTTYPEField;
            }
            set
            {
                this.aCTTYPEField = value;
            }
        }

        
        
        public string SUPPL_VEND
        {
            get
            {
                return this.sUPPL_VENDField;
            }
            set
            {
                this.sUPPL_VENDField = value;
            }
        }

        
        
        public string MATERIAL_EXTERNAL
        {
            get
            {
                return this.mATERIAL_EXTERNALField;
            }
            set
            {
                this.mATERIAL_EXTERNALField = value;
            }
        }

        
        
        public string MATERIAL_GUID
        {
            get
            {
                return this.mATERIAL_GUIDField;
            }
            set
            {
                this.mATERIAL_GUIDField = value;
            }
        }

        
        
        public string MATERIAL_VERSION
        {
            get
            {
                return this.mATERIAL_VERSIONField;
            }
            set
            {
                this.mATERIAL_VERSIONField = value;
            }
        }

        
        
        public string MOVE_MAT_EXTERNAL
        {
            get
            {
                return this.mOVE_MAT_EXTERNALField;
            }
            set
            {
                this.mOVE_MAT_EXTERNALField = value;
            }
        }

        
        
        public string MOVE_MAT_GUID
        {
            get
            {
                return this.mOVE_MAT_GUIDField;
            }
            set
            {
                this.mOVE_MAT_GUIDField = value;
            }
        }

        
        
        public string MOVE_MAT_VERSION
        {
            get
            {
                return this.mOVE_MAT_VERSIONField;
            }
            set
            {
                this.mOVE_MAT_VERSIONField = value;
            }
        }

        
        
        public string FUNC_AREA
        {
            get
            {
                return this.fUNC_AREAField;
            }
            set
            {
                this.fUNC_AREAField = value;
            }
        }

        
        
        public string TR_PART_BA
        {
            get
            {
                return this.tR_PART_BAField;
            }
            set
            {
                this.tR_PART_BAField = value;
            }
        }

        
        
        public string PAR_COMPCO
        {
            get
            {
                return this.pAR_COMPCOField;
            }
            set
            {
                this.pAR_COMPCOField = value;
            }
        }

        
        
        public string DELIV_NUMB
        {
            get
            {
                return this.dELIV_NUMBField;
            }
            set
            {
                this.dELIV_NUMBField = value;
            }
        }

        
        
        public string DELIV_ITEM
        {
            get
            {
                return this.dELIV_ITEMField;
            }
            set
            {
                this.dELIV_ITEMField = value;
            }
        }

        
        
        public string NB_SLIPS
        {
            get
            {
                return this.nB_SLIPSField;
            }
            set
            {
                this.nB_SLIPSField = value;
            }
        }

        
        
        public string NB_SLIPSX
        {
            get
            {
                return this.nB_SLIPSXField;
            }
            set
            {
                this.nB_SLIPSXField = value;
            }
        }

        
        
        public string GR_RCPTX
        {
            get
            {
                return this.gR_RCPTXField;
            }
            set
            {
                this.gR_RCPTXField = value;
            }
        }

        
        
        public string UNLOAD_PTX
        {
            get
            {
                return this.uNLOAD_PTXField;
            }
            set
            {
                this.uNLOAD_PTXField = value;
            }
        }

        
        
        public string SPEC_MVMT
        {
            get
            {
                return this.sPEC_MVMTField;
            }
            set
            {
                this.sPEC_MVMTField = value;
            }
        }

        
        
        public string GRANT_NBR
        {
            get
            {
                return this.gRANT_NBRField;
            }
            set
            {
                this.gRANT_NBRField = value;
            }
        }

        
        
        public string CMMT_ITEM_LONG
        {
            get
            {
                return this.cMMT_ITEM_LONGField;
            }
            set
            {
                this.cMMT_ITEM_LONGField = value;
            }
        }

        
        
        public string FUNC_AREA_LONG
        {
            get
            {
                return this.fUNC_AREA_LONGField;
            }
            set
            {
                this.fUNC_AREA_LONGField = value;
            }
        }

        
        
        public decimal QUANTITY
        {
            get
            {
                return this.qUANTITYField;
            }
            set
            {
                this.qUANTITYField = value;
            }
        }

        
        
        public bool QUANTITYSpecified
        {
            get
            {
                return this.qUANTITYFieldSpecified;
            }
            set
            {
                this.qUANTITYFieldSpecified = value;
            }
        }

        
        
        public string BASE_UOM
        {
            get
            {
                return this.bASE_UOMField;
            }
            set
            {
                this.bASE_UOMField = value;
            }
        }

        
        
        public string BASE_UOM_ISO
        {
            get
            {
                return this.bASE_UOM_ISOField;
            }
            set
            {
                this.bASE_UOM_ISOField = value;
            }
        }

        
        
        public string BL_DATE
        {
            get
            {
                return this.bL_DATEField;
            }
            set
            {
                this.bL_DATEField = value;
            }
        }

        
        
        public decimal L15
        {
            get
            {
                return this.l15Field;
            }
            set
            {
                this.l15Field = value;
            }
        }

        
        
        public bool L15Specified
        {
            get
            {
                return this.l15FieldSpecified;
            }
            set
            {
                this.l15FieldSpecified = value;
            }
        }

        
        
        public decimal L30
        {
            get
            {
                return this.l30Field;
            }
            set
            {
                this.l30Field = value;
            }
        }

        
        
        public bool L30Specified
        {
            get
            {
                return this.l30FieldSpecified;
            }
            set
            {
                this.l30FieldSpecified = value;
            }
        }

        
        
        public decimal L
        {
            get
            {
                return this.lField;
            }
            set
            {
                this.lField = value;
            }
        }

        
        
        public bool LSpecified
        {
            get
            {
                return this.lFieldSpecified;
            }
            set
            {
                this.lFieldSpecified = value;
            }
        }

        
        
        public decimal LTO
        {
            get
            {
                return this.lTOField;
            }
            set
            {
                this.lTOField = value;
            }
        }

        
        
        public bool LTOSpecified
        {
            get
            {
                return this.lTOFieldSpecified;
            }
            set
            {
                this.lTOFieldSpecified = value;
            }
        }

        
        
        public decimal MT
        {
            get
            {
                return this.mtField;
            }
            set
            {
                this.mtField = value;
            }
        }

        
        
        public bool MTSpecified
        {
            get
            {
                return this.mtFieldSpecified;
            }
            set
            {
                this.mtFieldSpecified = value;
            }
        }

        
        
        public decimal BB6
        {
            get
            {
                return this.bB6Field;
            }
            set
            {
                this.bB6Field = value;
            }
        }

        
        
        public bool BB6Specified
        {
            get
            {
                return this.bB6FieldSpecified;
            }
            set
            {
                this.bB6FieldSpecified = value;
            }
        }

        
        
        public decimal KG
        {
            get
            {
                return this.kgField;
            }
            set
            {
                this.kgField = value;
            }
        }

        
        
        public bool KGSpecified
        {
            get
            {
                return this.kgFieldSpecified;
            }
            set
            {
                this.kgFieldSpecified = value;
            }
        }

        
        
        public decimal API
        {
            get
            {
                return this.aPIField;
            }
            set
            {
                this.aPIField = value;
            }
        }

        
        
        public bool APISpecified
        {
            get
            {
                return this.aPIFieldSpecified;
            }
            set
            {
                this.aPIFieldSpecified = value;
            }
        }

        
        
        public decimal TEMPER
        {
            get
            {
                return this.tEMPERField;
            }
            set
            {
                this.tEMPERField = value;
            }
        }

        
        
        public bool TEMPERSpecified
        {
            get
            {
                return this.tEMPERFieldSpecified;
            }
            set
            {
                this.tEMPERFieldSpecified = value;
            }
        }

        
        
        public decimal DEN
        {
            get
            {
                return this.dENField;
            }
            set
            {
                this.dENField = value;
            }
        }

        
        
        public bool DENSpecified
        {
            get
            {
                return this.dENFieldSpecified;
            }
            set
            {
                this.dENFieldSpecified = value;
            }
        }

        
        
        public decimal BSW
        {
            get
            {
                return this.bSWField;
            }
            set
            {
                this.bSWField = value;
            }
        }

        
        
        public bool BSWSpecified
        {
            get
            {
                return this.bSWFieldSpecified;
            }
            set
            {
                this.bSWFieldSpecified = value;
            }
        }
    }
    public class ZGOODSMVT_CODE
    {
        private string gM_CODEField;
        public string GM_CODE
        {
            get
            {
                return this.gM_CODEField;
            }
            set
            {
                this.gM_CODEField = value;
            }
        }
    }
    public class ZGOODSMVT_HEADER
    {

        private string pSTNG_DATEField;

        private string dOC_DATEField;

        private string pSTNG_TIMEField;

        private string rEF_DOC_NOField;

        private string bILL_OF_LADINGField;

        private string gR_GI_SLIP_NOField;

        private string pR_UNAMEField;

        private string hEADER_TXTField;

        private string vER_GR_GI_SLIPField;

        private string vER_GR_GI_SLIPXField;

        private string eXT_WMSField;

        private string rEF_DOC_NO_LONGField;

        private string bILL_OF_LADING_LONGField;

        private string bAR_CODEField;
        
        public string PSTNG_DATE
        {
            get
            {
                return this.pSTNG_DATEField;
            }
            set
            {
                this.pSTNG_DATEField = value;
            }
        }
        
        public string DOC_DATE
        {
            get
            {
                return this.dOC_DATEField;
            }
            set
            {
                this.dOC_DATEField = value;
            }
        }
        
        public string PSTNG_TIME
        {
            get
            {
                return this.pSTNG_TIMEField;
            }
            set
            {
                this.pSTNG_TIMEField = value;
            }
        }
        
        public string REF_DOC_NO
        {
            get
            {
                return this.rEF_DOC_NOField;
            }
            set
            {
                this.rEF_DOC_NOField = value;
            }
        }
        
        public string BILL_OF_LADING
        {
            get
            {
                return this.bILL_OF_LADINGField;
            }
            set
            {
                this.bILL_OF_LADINGField = value;
            }
        }
        
        public string GR_GI_SLIP_NO
        {
            get
            {
                return this.gR_GI_SLIP_NOField;
            }
            set
            {
                this.gR_GI_SLIP_NOField = value;
            }
        }
        
        public string PR_UNAME
        {
            get
            {
                return this.pR_UNAMEField;
            }
            set
            {
                this.pR_UNAMEField = value;
            }
        }
        
        public string HEADER_TXT
        {
            get
            {
                return this.hEADER_TXTField;
            }
            set
            {
                this.hEADER_TXTField = value;
            }
        }
        
        public string VER_GR_GI_SLIP
        {
            get
            {
                return this.vER_GR_GI_SLIPField;
            }
            set
            {
                this.vER_GR_GI_SLIPField = value;
            }
        }
        
        public string VER_GR_GI_SLIPX
        {
            get
            {
                return this.vER_GR_GI_SLIPXField;
            }
            set
            {
                this.vER_GR_GI_SLIPXField = value;
            }
        }
        
        public string EXT_WMS
        {
            get
            {
                return this.eXT_WMSField;
            }
            set
            {
                this.eXT_WMSField = value;
            }
        }
        
        public string REF_DOC_NO_LONG
        {
            get
            {
                return this.rEF_DOC_NO_LONGField;
            }
            set
            {
                this.rEF_DOC_NO_LONGField = value;
            }
        }
        
        public string BILL_OF_LADING_LONG
        {
            get
            {
                return this.bILL_OF_LADING_LONGField;
            }
            set
            {
                this.bILL_OF_LADING_LONGField = value;
            }
        }
        
        public string BAR_CODE
        {
            get
            {
                return this.bAR_CODEField;
            }
            set
            {
                this.bAR_CODEField = value;
            }
        }
    }
    public class BAPI2017_GM_HEAD_RET
    {
        private string mAT_DOCField;

        private string dOC_YEARField;
        
        public string MAT_DOC
        {
            get
            {
                return this.mAT_DOCField;
            }
            set
            {
                this.mAT_DOCField = value;
            }
        }
        
        
        public string DOC_YEAR
        {
            get
            {
                return this.dOC_YEARField;
            }
            set
            {
                this.dOC_YEARField = value;
            }
        }
    }
    public class BAPIRET2
    {
        private string tYPEField;

        private string idField;

        private string nUMBERField;

        private string mESSAGEField;

        private string lOG_NOField;

        private string lOG_MSG_NOField;

        private string mESSAGE_V1Field;

        private string mESSAGE_V2Field;

        private string mESSAGE_V3Field;

        private string mESSAGE_V4Field;

        private string pARAMETERField;

        private string rOWField;

        private string fIELDField;

        private string sYSTEMField;
        
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
        
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
        
        public string NUMBER
        {
            get
            {
                return this.nUMBERField;
            }
            set
            {
                this.nUMBERField = value;
            }
        }
        
        public string MESSAGE
        {
            get
            {
                return this.mESSAGEField;
            }
            set
            {
                this.mESSAGEField = value;
            }
        }
        
        public string LOG_NO
        {
            get
            {
                return this.lOG_NOField;
            }
            set
            {
                this.lOG_NOField = value;
            }
        }

        public string LOG_MSG_NO
        {
            get
            {
                return this.lOG_MSG_NOField;
            }
            set
            {
                this.lOG_MSG_NOField = value;
            }
        }
        
        public string MESSAGE_V1
        {
            get
            {
                return this.mESSAGE_V1Field;
            }
            set
            {
                this.mESSAGE_V1Field = value;
            }
        }
        
        public string MESSAGE_V2
        {
            get
            {
                return this.mESSAGE_V2Field;
            }
            set
            {
                this.mESSAGE_V2Field = value;
            }
        }
        
        public string MESSAGE_V3
        {
            get
            {
                return this.mESSAGE_V3Field;
            }
            set
            {
                this.mESSAGE_V3Field = value;
            }
        }
        
        public string MESSAGE_V4
        {
            get
            {
                return this.mESSAGE_V4Field;
            }
            set
            {
                this.mESSAGE_V4Field = value;
            }
        }
        
        public string PARAMETER
        {
            get
            {
                return this.pARAMETERField;
            }
            set
            {
                this.pARAMETERField = value;
            }
        }
        
        public string ROW
        {
            get
            {
                return this.rOWField;
            }
            set
            {
                this.rOWField = value;
            }
        }
        
        public string FIELD
        {
            get
            {
                return this.fIELDField;
            }
            set
            {
                this.fIELDField = value;
            }
        }
        
        public string SYSTEM
        {
            get
            {
                return this.sYSTEMField;
            }
            set
            {
                this.sYSTEMField = value;
            }
        }
    }
    public class BAPI2017_GM_ITEM_04
    {

        private string mATDOC_ITEMField;

        public string MATDOC_ITEM
        {
            get
            {
                return this.mATDOC_ITEMField;
            }
            set
            {
                this.mATDOC_ITEMField = value;
            }
        }
    }

    #endregion

    #region PurchasingCreateModel
    public class ZPOHEADER
    {

        private string pO_NUMBERField;

        private string cOMP_CODEField;

        private string dOC_TYPEField;

        private string dELETE_INDField;

        private string cREAT_DATEField;

        private string cREATED_BYField;

        private string vENDORField;

        private string pURCH_ORGField;

        private string pUR_GROUPField;

        private string cURRENCYField;

        private decimal eXCH_RATEField;

        private bool eXCH_RATEFieldSpecified;

        private string dOC_DATEField;

        private string iNCOTERMS1Field;

        private string oIC_AORGINField;

        private string cARGO_NOField;

        private string vPER_STARTField;

        private string vPER_ENDField;

        
        
        public string PO_NUMBER
        {
            get
            {
                return this.pO_NUMBERField;
            }
            set
            {
                this.pO_NUMBERField = value;
            }
        }

        
        
        public string COMP_CODE
        {
            get
            {
                return this.cOMP_CODEField;
            }
            set
            {
                this.cOMP_CODEField = value;
            }
        }

        
        
        public string DOC_TYPE
        {
            get
            {
                return this.dOC_TYPEField;
            }
            set
            {
                this.dOC_TYPEField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public string CREAT_DATE
        {
            get
            {
                return this.cREAT_DATEField;
            }
            set
            {
                this.cREAT_DATEField = value;
            }
        }

        
        
        public string CREATED_BY
        {
            get
            {
                return this.cREATED_BYField;
            }
            set
            {
                this.cREATED_BYField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string PURCH_ORG
        {
            get
            {
                return this.pURCH_ORGField;
            }
            set
            {
                this.pURCH_ORGField = value;
            }
        }

        
        
        public string PUR_GROUP
        {
            get
            {
                return this.pUR_GROUPField;
            }
            set
            {
                this.pUR_GROUPField = value;
            }
        }

        
        
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }

        
        
        public decimal EXCH_RATE
        {
            get
            {
                return this.eXCH_RATEField;
            }
            set
            {
                this.eXCH_RATEField = value;
            }
        }

        
        
        public bool EXCH_RATESpecified
        {
            get
            {
                return this.eXCH_RATEFieldSpecified;
            }
            set
            {
                this.eXCH_RATEFieldSpecified = value;
            }
        }

        
        
        public string DOC_DATE
        {
            get
            {
                return this.dOC_DATEField;
            }
            set
            {
                this.dOC_DATEField = value;
            }
        }

        
        
        public string INCOTERMS1
        {
            get
            {
                return this.iNCOTERMS1Field;
            }
            set
            {
                this.iNCOTERMS1Field = value;
            }
        }

        
        
        public string OIC_AORGIN
        {
            get
            {
                return this.oIC_AORGINField;
            }
            set
            {
                this.oIC_AORGINField = value;
            }
        }

        
        
        public string CARGO_NO
        {
            get
            {
                return this.cARGO_NOField;
            }
            set
            {
                this.cARGO_NOField = value;
            }
        }

        
        
        public string VPER_START
        {
            get
            {
                return this.vPER_STARTField;
            }
            set
            {
                this.vPER_STARTField = value;
            }
        }

        
        
        public string VPER_END
        {
            get
            {
                return this.vPER_ENDField;
            }
            set
            {
                this.vPER_ENDField = value;
            }
        }
    }
    public class ZPOHEADERX
    {

        private string pO_NUMBERField;

        private string cOMP_CODEField;

        private string dOC_TYPEField;

        private string dELETE_INDField;

        private string cREAT_DATEField;

        private string cREATED_BYField;

        private string vENDORField;

        private string pURCH_ORGField;

        private string pUR_GROUPField;

        private string cURRENCYField;

        private string eXCH_RATEField;

        private string dOC_DATEField;

        private string iNCOTERMS1Field;

        private string oIC_AORGINField;

        private string cARGO_NOField;

        private string vPER_STARTField;

        private string vPER_ENDField;

        
        
        public string PO_NUMBER
        {
            get
            {
                return this.pO_NUMBERField;
            }
            set
            {
                this.pO_NUMBERField = value;
            }
        }

        
        
        public string COMP_CODE
        {
            get
            {
                return this.cOMP_CODEField;
            }
            set
            {
                this.cOMP_CODEField = value;
            }
        }

        
        
        public string DOC_TYPE
        {
            get
            {
                return this.dOC_TYPEField;
            }
            set
            {
                this.dOC_TYPEField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public string CREAT_DATE
        {
            get
            {
                return this.cREAT_DATEField;
            }
            set
            {
                this.cREAT_DATEField = value;
            }
        }

        
        
        public string CREATED_BY
        {
            get
            {
                return this.cREATED_BYField;
            }
            set
            {
                this.cREATED_BYField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string PURCH_ORG
        {
            get
            {
                return this.pURCH_ORGField;
            }
            set
            {
                this.pURCH_ORGField = value;
            }
        }

        
        
        public string PUR_GROUP
        {
            get
            {
                return this.pUR_GROUPField;
            }
            set
            {
                this.pUR_GROUPField = value;
            }
        }

        
        
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }

        
        
        public string EXCH_RATE
        {
            get
            {
                return this.eXCH_RATEField;
            }
            set
            {
                this.eXCH_RATEField = value;
            }
        }

        
        
        public string DOC_DATE
        {
            get
            {
                return this.dOC_DATEField;
            }
            set
            {
                this.dOC_DATEField = value;
            }
        }

        
        
        public string INCOTERMS1
        {
            get
            {
                return this.iNCOTERMS1Field;
            }
            set
            {
                this.iNCOTERMS1Field = value;
            }
        }

        
        
        public string OIC_AORGIN
        {
            get
            {
                return this.oIC_AORGINField;
            }
            set
            {
                this.oIC_AORGINField = value;
            }
        }

        
        
        public string CARGO_NO
        {
            get
            {
                return this.cARGO_NOField;
            }
            set
            {
                this.cARGO_NOField = value;
            }
        }

        
        
        public string VPER_START
        {
            get
            {
                return this.vPER_STARTField;
            }
            set
            {
                this.vPER_STARTField = value;
            }
        }

        
        
        public string VPER_END
        {
            get
            {
                return this.vPER_ENDField;
            }
            set
            {
                this.vPER_ENDField = value;
            }
        }
    }
    public class ZPOITEM
    {

        private string pO_ITEMField;

        private string dELETE_INDField;

        private string sHORT_TEXTField;

        private string mATERIALField;

        private string eMATERIALField;

        private string pLANTField;

        private string sTGE_LOCField;

        private string tRACKINGNOField;

        private string mATL_GROUPField;

        private decimal qUANTITYField;

        private bool qUANTITYFieldSpecified;

        private string pO_UNITField;

        private string pO_UNIT_ISOField;

        private decimal nET_PRICEField;

        private bool nET_PRICEFieldSpecified;

        private string cURRENCYField;

        private string tAX_CODEField;

        private string iTEM_CATField;

        private string aCCTASSCATField;

        private decimal pLAN_DELField;

        private bool pLAN_DELFieldSpecified;

        private string vENDORField;

        private string tRAN_IDField;

        private string cARGO_NOField;

        private string pREQ_NAMEField;

        private string gR_BASEDIVField;

        
        
        public string PO_ITEM
        {
            get
            {
                return this.pO_ITEMField;
            }
            set
            {
                this.pO_ITEMField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public string SHORT_TEXT
        {
            get
            {
                return this.sHORT_TEXTField;
            }
            set
            {
                this.sHORT_TEXTField = value;
            }
        }

        
        
        public string MATERIAL
        {
            get
            {
                return this.mATERIALField;
            }
            set
            {
                this.mATERIALField = value;
            }
        }

        
        
        public string EMATERIAL
        {
            get
            {
                return this.eMATERIALField;
            }
            set
            {
                this.eMATERIALField = value;
            }
        }

        
        
        public string PLANT
        {
            get
            {
                return this.pLANTField;
            }
            set
            {
                this.pLANTField = value;
            }
        }

        
        
        public string STGE_LOC
        {
            get
            {
                return this.sTGE_LOCField;
            }
            set
            {
                this.sTGE_LOCField = value;
            }
        }

        
        
        public string TRACKINGNO
        {
            get
            {
                return this.tRACKINGNOField;
            }
            set
            {
                this.tRACKINGNOField = value;
            }
        }

        
        
        public string MATL_GROUP
        {
            get
            {
                return this.mATL_GROUPField;
            }
            set
            {
                this.mATL_GROUPField = value;
            }
        }

        
        
        public decimal QUANTITY
        {
            get
            {
                return this.qUANTITYField;
            }
            set
            {
                this.qUANTITYField = value;
            }
        }

        
        
        public bool QUANTITYSpecified
        {
            get
            {
                return this.qUANTITYFieldSpecified;
            }
            set
            {
                this.qUANTITYFieldSpecified = value;
            }
        }

        
        
        public string PO_UNIT
        {
            get
            {
                return this.pO_UNITField;
            }
            set
            {
                this.pO_UNITField = value;
            }
        }

        
        
        public string PO_UNIT_ISO
        {
            get
            {
                return this.pO_UNIT_ISOField;
            }
            set
            {
                this.pO_UNIT_ISOField = value;
            }
        }

        
        
        public decimal NET_PRICE
        {
            get
            {
                return this.nET_PRICEField;
            }
            set
            {
                this.nET_PRICEField = value;
            }
        }

        
        
        public bool NET_PRICESpecified
        {
            get
            {
                return this.nET_PRICEFieldSpecified;
            }
            set
            {
                this.nET_PRICEFieldSpecified = value;
            }
        }

        
        
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }

        
        
        public string TAX_CODE
        {
            get
            {
                return this.tAX_CODEField;
            }
            set
            {
                this.tAX_CODEField = value;
            }
        }

        
        
        public string ITEM_CAT
        {
            get
            {
                return this.iTEM_CATField;
            }
            set
            {
                this.iTEM_CATField = value;
            }
        }

        
        
        public string ACCTASSCAT
        {
            get
            {
                return this.aCCTASSCATField;
            }
            set
            {
                this.aCCTASSCATField = value;
            }
        }

        
        
        public decimal PLAN_DEL
        {
            get
            {
                return this.pLAN_DELField;
            }
            set
            {
                this.pLAN_DELField = value;
            }
        }

        
        
        public bool PLAN_DELSpecified
        {
            get
            {
                return this.pLAN_DELFieldSpecified;
            }
            set
            {
                this.pLAN_DELFieldSpecified = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string TRAN_ID
        {
            get
            {
                return this.tRAN_IDField;
            }
            set
            {
                this.tRAN_IDField = value;
            }
        }

        
        
        public string CARGO_NO
        {
            get
            {
                return this.cARGO_NOField;
            }
            set
            {
                this.cARGO_NOField = value;
            }
        }

        
        
        public string PREQ_NAME
        {
            get
            {
                return this.pREQ_NAMEField;
            }
            set
            {
                this.pREQ_NAMEField = value;
            }
        }

        
        
        public string GR_BASEDIV
        {
            get
            {
                return this.gR_BASEDIVField;
            }
            set
            {
                this.gR_BASEDIVField = value;
            }
        }
    }
    public class ZPOITEMX
    {

        private string pO_ITEMField;

        private string pO_ITEMXField;

        private string dELETE_INDField;

        private string sHORT_TEXTField;

        private string mATERIALField;

        private string eMATERIALField;

        private string pLANTField;

        private string sTGE_LOCField;

        private string tRACKINGNOField;

        private string mATL_GROUPField;

        private string qUANTITYField;

        private string pO_UNITField;

        private string pO_UNIT_ISOField;

        private string nET_PRICEField;

        private string cURRENCYField;

        private string tAX_CODEField;

        private string iTEM_CATField;

        private string aCCTASSCATField;

        private string pLAN_DELField;

        private string vENDORField;

        private string tRAN_IDField;

        private string cARGO_NOField;

        private string pREQ_NAMEField;

        private string gR_BASEDIVField;

        
        
        public string PO_ITEM
        {
            get
            {
                return this.pO_ITEMField;
            }
            set
            {
                this.pO_ITEMField = value;
            }
        }

        
        
        public string PO_ITEMX
        {
            get
            {
                return this.pO_ITEMXField;
            }
            set
            {
                this.pO_ITEMXField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public string SHORT_TEXT
        {
            get
            {
                return this.sHORT_TEXTField;
            }
            set
            {
                this.sHORT_TEXTField = value;
            }
        }

        
        
        public string MATERIAL
        {
            get
            {
                return this.mATERIALField;
            }
            set
            {
                this.mATERIALField = value;
            }
        }

        
        
        public string EMATERIAL
        {
            get
            {
                return this.eMATERIALField;
            }
            set
            {
                this.eMATERIALField = value;
            }
        }

        
        
        public string PLANT
        {
            get
            {
                return this.pLANTField;
            }
            set
            {
                this.pLANTField = value;
            }
        }

        
        
        public string STGE_LOC
        {
            get
            {
                return this.sTGE_LOCField;
            }
            set
            {
                this.sTGE_LOCField = value;
            }
        }

        
        
        public string TRACKINGNO
        {
            get
            {
                return this.tRACKINGNOField;
            }
            set
            {
                this.tRACKINGNOField = value;
            }
        }

        
        
        public string MATL_GROUP
        {
            get
            {
                return this.mATL_GROUPField;
            }
            set
            {
                this.mATL_GROUPField = value;
            }
        }

        
        
        public string QUANTITY
        {
            get
            {
                return this.qUANTITYField;
            }
            set
            {
                this.qUANTITYField = value;
            }
        }

        
        
        public string PO_UNIT
        {
            get
            {
                return this.pO_UNITField;
            }
            set
            {
                this.pO_UNITField = value;
            }
        }

        
        
        public string PO_UNIT_ISO
        {
            get
            {
                return this.pO_UNIT_ISOField;
            }
            set
            {
                this.pO_UNIT_ISOField = value;
            }
        }

        
        
        public string NET_PRICE
        {
            get
            {
                return this.nET_PRICEField;
            }
            set
            {
                this.nET_PRICEField = value;
            }
        }

        
        
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }

        
        
        public string TAX_CODE
        {
            get
            {
                return this.tAX_CODEField;
            }
            set
            {
                this.tAX_CODEField = value;
            }
        }

        
        
        public string ITEM_CAT
        {
            get
            {
                return this.iTEM_CATField;
            }
            set
            {
                this.iTEM_CATField = value;
            }
        }

        
        
        public string ACCTASSCAT
        {
            get
            {
                return this.aCCTASSCATField;
            }
            set
            {
                this.aCCTASSCATField = value;
            }
        }

        
        
        public string PLAN_DEL
        {
            get
            {
                return this.pLAN_DELField;
            }
            set
            {
                this.pLAN_DELField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string TRAN_ID
        {
            get
            {
                return this.tRAN_IDField;
            }
            set
            {
                this.tRAN_IDField = value;
            }
        }

        
        
        public string CARGO_NO
        {
            get
            {
                return this.cARGO_NOField;
            }
            set
            {
                this.cARGO_NOField = value;
            }
        }

        
        
        public string PREQ_NAME
        {
            get
            {
                return this.pREQ_NAMEField;
            }
            set
            {
                this.pREQ_NAMEField = value;
            }
        }

        
        
        public string GR_BASEDIV
        {
            get
            {
                return this.gR_BASEDIVField;
            }
            set
            {
                this.gR_BASEDIVField = value;
            }
        }
    }
    public class ZPOACCOUNT
    {

        private string pO_ITEMField;

        private string sERIAL_NOField;

        private string dELETE_INDField;

        private decimal nET_VALUEField;

        private bool nET_VALUEFieldSpecified;

        private string gL_ACCOUNTField;

        private string cOSTCENTERField;

        private string iTM_NUMBERField;

        private string sCHED_LINEField;

        private string oRDERIDField;

        private string vENDORField;

        private string tRAN_INDField;

        
        
        public string PO_ITEM
        {
            get
            {
                return this.pO_ITEMField;
            }
            set
            {
                this.pO_ITEMField = value;
            }
        }

        
        
        public string SERIAL_NO
        {
            get
            {
                return this.sERIAL_NOField;
            }
            set
            {
                this.sERIAL_NOField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public decimal NET_VALUE
        {
            get
            {
                return this.nET_VALUEField;
            }
            set
            {
                this.nET_VALUEField = value;
            }
        }

        
        
        public bool NET_VALUESpecified
        {
            get
            {
                return this.nET_VALUEFieldSpecified;
            }
            set
            {
                this.nET_VALUEFieldSpecified = value;
            }
        }

        
        
        public string GL_ACCOUNT
        {
            get
            {
                return this.gL_ACCOUNTField;
            }
            set
            {
                this.gL_ACCOUNTField = value;
            }
        }

        
        
        public string COSTCENTER
        {
            get
            {
                return this.cOSTCENTERField;
            }
            set
            {
                this.cOSTCENTERField = value;
            }
        }

        
        
        public string ITM_NUMBER
        {
            get
            {
                return this.iTM_NUMBERField;
            }
            set
            {
                this.iTM_NUMBERField = value;
            }
        }

        
        
        public string SCHED_LINE
        {
            get
            {
                return this.sCHED_LINEField;
            }
            set
            {
                this.sCHED_LINEField = value;
            }
        }

        
        
        public string ORDERID
        {
            get
            {
                return this.oRDERIDField;
            }
            set
            {
                this.oRDERIDField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string TRAN_IND
        {
            get
            {
                return this.tRAN_INDField;
            }
            set
            {
                this.tRAN_INDField = value;
            }
        }
    }
    public class ZPOACCOUNTX
    {

        private string pO_ITEMField;

        private string sERIAL_NOField;

        private string pO_ITEMXField;

        private string sERIAL_NOXField;

        private string dELETE_INDField;

        private string nET_VALUEField;

        private string gL_ACCOUNTField;

        private string cOSTCENTERField;

        private string iTM_NUMBERField;

        private string sCHED_LINEField;

        private string oRDERIDField;

        private string vENDORField;

        private string tRAN_IDField;

        
        
        public string PO_ITEM
        {
            get
            {
                return this.pO_ITEMField;
            }
            set
            {
                this.pO_ITEMField = value;
            }
        }

        
        
        public string SERIAL_NO
        {
            get
            {
                return this.sERIAL_NOField;
            }
            set
            {
                this.sERIAL_NOField = value;
            }
        }

        
        
        public string PO_ITEMX
        {
            get
            {
                return this.pO_ITEMXField;
            }
            set
            {
                this.pO_ITEMXField = value;
            }
        }

        
        
        public string SERIAL_NOX
        {
            get
            {
                return this.sERIAL_NOXField;
            }
            set
            {
                this.sERIAL_NOXField = value;
            }
        }

        
        
        public string DELETE_IND
        {
            get
            {
                return this.dELETE_INDField;
            }
            set
            {
                this.dELETE_INDField = value;
            }
        }

        
        
        public string NET_VALUE
        {
            get
            {
                return this.nET_VALUEField;
            }
            set
            {
                this.nET_VALUEField = value;
            }
        }

        
        
        public string GL_ACCOUNT
        {
            get
            {
                return this.gL_ACCOUNTField;
            }
            set
            {
                this.gL_ACCOUNTField = value;
            }
        }

        
        
        public string COSTCENTER
        {
            get
            {
                return this.cOSTCENTERField;
            }
            set
            {
                this.cOSTCENTERField = value;
            }
        }

        
        
        public string ITM_NUMBER
        {
            get
            {
                return this.iTM_NUMBERField;
            }
            set
            {
                this.iTM_NUMBERField = value;
            }
        }

        
        
        public string SCHED_LINE
        {
            get
            {
                return this.sCHED_LINEField;
            }
            set
            {
                this.sCHED_LINEField = value;
            }
        }

        
        
        public string ORDERID
        {
            get
            {
                return this.oRDERIDField;
            }
            set
            {
                this.oRDERIDField = value;
            }
        }

        
        
        public string VENDOR
        {
            get
            {
                return this.vENDORField;
            }
            set
            {
                this.vENDORField = value;
            }
        }

        
        
        public string TRAN_ID
        {
            get
            {
                return this.tRAN_IDField;
            }
            set
            {
                this.tRAN_IDField = value;
            }
        }
    }

    #endregion

    
}