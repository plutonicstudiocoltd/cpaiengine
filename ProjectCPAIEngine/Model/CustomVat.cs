﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
        
    public class CustomVat
    {
        public List<CustomVat_Calc> CustomVat_Calc { get; set; }
        public List<GLAccountVAT> GLAccountVATList { get; set; }

        public string sROE { get; set; }
        public string sInsuranceRate { get; set; }
        public string sDocumentDate { get; set; }
        public string sPostingDate { get; set; }
        public string sCalculationUnit { get; set; }
        public string sTotalAll { get; set; }
        public string sTotalImportDuty { get; set; }
    }

    public class CustomVat_Calc
    {
        public string No { get; set; }
        public string PoNo { get; set; }
        public string CrudeSupplier { get; set; }
        public string ROE { get; set; }
        public string QtyBBL { get; set; }
        public string QtyMT { get; set; }
        public string QtyML { get; set; }
        public string CustomsPrice { get; set; }
        public string FOB { get; set; }
        public string FRT { get; set; }
        public string CFR { get; set; }
        public string INS { get; set; }
        public string CIF { get; set; }
        public string Premium { get; set; }
        public string TotalUSD100 { get; set; }
        public string Baht100 { get; set; }
        public string Baht105 { get; set; }
        public string VAT { get; set; }
        public string TaxBase { get; set; }
        public string CorrectVAT { get; set; }
        public string ImportDuty { get; set; }
        public string ExciseTax { get; set; }
        public string MunicipalTax { get; set; }
        public string OilFuelFund { get; set; }
        public string DepositOfImportDuty { get; set; }
        public string DepositOfExciseTax { get; set; }
        public string DepositOfMunicipalTax { get; set; }
        public string EnergyOilFuelFund { get; set; }
        public string MatItemNo { get; set; }
        public string TripNo { get; set; }
        public string MetNum { get; set; }
        public string MatItemName { get; set; }
        public string VendorNo { get; set; }
        public string VendorName { get; set; }
        public string VesselNo { get; set; }
        public string VesselName { get; set; }
        public string CompanyCode { get; set; }
        public string FIDocVAT { get; set; }
        public string FIDocVATStatus { get; set; }
        public string FIDocVATMessage { get; set; }
        public string FIDocMIRO { get; set; }
        public string FIDocMIROStatus { get; set; }
        public string FIDocMIROMessage { get; set; }
        public string InvoiceDocMIRO { get; set; }
        public string InvoiceDocMIROStatus { get; set; }
        public string InvoiceDocMIROMessage { get; set; }
    }

    public class GLAccountVAT
    {
        public string Type { get; set; }
        public string GLAccount { get; set; }
        public string TaxCodeVat { get; set; }
        public string ValuePart1 { get; set; }
        public string TaxCodeMIRO { get; set; }
        public string Currency { get; set; }
    }
}