﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class DAFMailDetail: MailDetail
    {
        public String to { get; set; }
        public String cc { get; set; }
    }
}