﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class DAILYTIMESTART
    {
        public string TIME { get; set; }
    }

    public class RootObject
    {
        public string FIND_DATE { get; set; }
        public List<DAILYTIMESTART> DAILY_TIME_START { get; set; }
    }
}