﻿using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    //public class Tenor
    //{
    //    public string start_date { get; set; }
    //    public string end_date { get; set; }
    //    public string result { get; set; }
    //    public string excerise_date { get; set; }
    //    public string extend_date { get; set; }
    //    public string result_extend { get; set; }
    //}

    //public class OtherBidPrice
    //{
    //    public string order { get; set; }
    //    public string counter_parties { get; set; }
    //    public string price { get; set; }
    //}

    //public class Verify
    //{
    //    public string credit_rating { get; set; }
    //    public string credit_rating_reason { get; set; }
    //    public string cds { get; set; }
    //    public string cds_reason { get; set; }
    //    public string credit_limit { get; set; }
    //    public string credit_limit_reason { get; set; }
    //    public string annual_framework { get; set; }
    //    public string annual_framework_reason { get; set; }
    //    public string committee_framework { get; set; }
    //    public string committee_framework_reason { get; set; }
    //    public string other_bypass { get; set; }
    //    public string other_reason { get; set; }
    //}

    //public class HedgingDeal
    //{
    //    public string deal_no { get; set; }
    //    public string ref_pre_deal_no { get; set; }
    //    public string ref_ticket_no { get; set; }
    //    public string ref_deal_no { get; set; }
    //    public string unwide_status { get; set; }
    //    public string status { get; set; }
    //    public string revision { get; set; }
    //    public string create_by { get; set; }
    //    public string create_date { get; set; }
    //    public string update_by { get; set; }
    //    public string update_date { get; set; }
    //    public string suggestion { get; set; }
    //    public string deal_date { get; set; }
    //    public string deal_type { get; set; }
    //    public string m1m_status { get; set; }
    //    public string company { get; set; }
    //    public string counter_parties { get; set; }
    //    public string counter_additional { get; set; }
    //    public string labix_trade_thru_top { get; set; }
    //    public string labix_fee { get; set; }
    //    public string labix_fee_unit { get; set; }
    //    public string underlying { get; set; }
    //    public string underlying_cf_opr { get; set; }
    //    public string underlying_cf_val { get; set; }
    //    public string underlying_weight_percent { get; set; }
    //    public string ref_underlying { get; set; }
    //    public string underlying_vs { get; set; }
    //    public string underlying_vs_cf_opr { get; set; }
    //    public string underlying_vs_cf_val { get; set; }
    //    public string underlying_vs_weight_percent { get; set; }
    //    public string ref_underlying_vs { get; set; }
    //    public string fw_annual_id { get; set; }
    //    public string fw_hedged_type { get; set; }
    //    public Tenor tenor { get; set; }
    //    public string volume_month { get; set; }
    //    public string volume_month_unit { get; set; }
    //    public string volume_month_total { get; set; }
    //    public string volume_month_total_unit { get; set; }
    //    public string price { get; set; }
    //    public string unit_price { get; set; }
    //    public string tool { get; set; }
    //    public string payment_day { get; set; }
    //    public string payment_day_unit { get; set; }
    //    public List<OtherBidPrice> other_bid_price { get; set; }
    //    public Verify verify { get; set; }
    //    public string note { get; set; }
    //}

    public class HedgDealRootObject
    {
        public HedgingDealViewModel_Detail hedging_deal { get; set; }
    }
}