﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ProjectCPAIEngine.Areas.CPAIMVC.ViewModels.HedgingTicketViewModel;

namespace ProjectCPAIEngine.Model
{
    //public class TradingTicket
    //{
    //    public string ticket_no { get; set; }
    //    public string trader { get; set; }
    //    public string ticket_date { get; set; }
    //    public string status { get; set; }
    //    public string revision { get; set; }
    //    public string note { get; set; }
    //}

    //public class HedgingTarget
    //{
    //    public string order { get; set; }
    //    public string tenor { get; set; }
    //    public string price { get; set; }
    //    public string volume { get; set; }
    //    public string remaining { get; set; }
    //}

    //public class HedgingChart
    //{
    //    public string limit { get; set; }
    //    public string target { get; set; }
    //    public string hedged_vol { get; set; }
    //}

    //public class BidPrice
    //{
    //    public string order { get; set; }
    //    public string counter_parties_id { get; set; }
    //    public string counter_parties_name { get; set; }
    //    public string price { get; set; }
    //}

    //public class HedgingExecutionData
    //{
    //    public string order { get; set; }
    //    public string tenor { get; set; }
    //    public string deal_no { get; set; }
    //    public string deal_id { get; set; }
    //    public string deal_date { get; set; }
    //    public string counter_parties_id { get; set; }
    //    public string counter_parties_name { get; set; }
    //    public string price { get; set; }
    //    public string volume { get; set; }
    //    public List<BidPrice> bid_price { get; set; }
    //    public string record_status { get; set; }
    //}

    //public class HedgingPositionData
    //{
    //    public string order { get; set; }
    //    public string tenor { get; set; }
    //    public string price { get; set; }
    //    public string volume { get; set; }
    //}

    //public class HedgingPosition
    //{
    //    public string price_unit { get; set; }
    //    public string volume_unit { get; set; }
    //    public List<HedgingPositionData> hedging_position_data { get; set; }
    //}

    //public class HedgingExecution
    //{
    //    public string price_unit { get; set; }
    //    public string volume_mnt_unit { get; set; }
    //    public List<HedgingExecutionData> hedging_execution_data { get; set; }
    //    public HedgingPosition hedging_position { get; set; }
    //}

    //public class DealDetail
    //{
    //    public string deal_type { get; set; }
    //    public string underlying_id { get; set; }
    //    public string underlying_name { get; set; }
    //    public string underlying_vs_id { get; set; }
    //    public string underlying_vs_name { get; set; }
    //    public string tool_id { get; set; }
    //    public string tool_name { get; set; }
    //    public List<HedgingTarget> hedging_target { get; set; }
    //    public HedgingChart hedging_chart { get; set; }
    //    public HedgingExecution hedging_execution { get; set; }
    //    public string[] removed_deals { get; set; }
    //}

        public class TradingTicketRootObject
        {
            public TradingTicket trading_ticket { get; set; }
            public DealDetail deal_detail { get; set; }
        }
    }