﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class MailContent
    {
        public String mail_to { get; set; }
        public String mail_from { get; set; }
        public String mail_from_name { get; set; }
        public String mail_cc { get; set; }
        public String mail_bcc { get; set; }
        public String mail_subject { get; set; }
        public String mail_body { get; set; }
    }
}