﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Model
{
    public class NotiDetail
    {
        public String subject { get; set; }
        public String body { get; set; }
        public String action_data { get; set; }

    }
}