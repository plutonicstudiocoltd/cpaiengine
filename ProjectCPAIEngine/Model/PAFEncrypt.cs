﻿using ProjectCPAIEngine.Flow.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace ProjectCPAIEngine.Model
{
    [XmlRoot(ElementName = "customer_review")]
    public class PAFCustomerReview
    {
        [XmlAttribute(AttributeName = "suplier")]
        public string suplier { get; set; }
        [XmlAttribute(AttributeName = "customer")]
        public string customer { get; set; }
        [XmlAttribute(AttributeName = "product")]
        public string product { get; set; }
        [XmlAttribute(AttributeName = "quantity")]
        public string quantity { get; set; }
        [XmlAttribute(AttributeName = "tier")]
        public int tier { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string price { get; set; }
        [XmlAttribute(AttributeName = "note")]
        public string note { get; set; }
    }

    [XmlRoot(ElementName = "awarded")]
    public class PAFAwarded
    {
        [XmlAttribute(AttributeName = "suplier")]
        public string suplier { get; set; }
        [XmlAttribute(AttributeName = "customer")]
        public string customer { get; set; }


        [XmlAttribute(AttributeName = "benchmark")]
        public string benchmark { get; set; }
        [XmlAttribute(AttributeName = "premium_discount")]
        public string premium_discount { get; set; }
        [XmlAttribute(AttributeName = "freight")]
        public string freight { get; set; }

        [XmlAttribute(AttributeName = "awarded_bidder")]
        public string bidder { get; set; }
        [XmlAttribute(AttributeName = "awarded_product")]
        public string product { get; set; }
        [XmlAttribute(AttributeName = "awarded_quantity")]
        public string quantity { get; set; }


        [XmlAttribute(AttributeName = "incoterm")]
        public string incoterm { get; set; }
        [XmlAttribute(AttributeName = "awarded_incoterm")]
        public string awarded_incoterm { get; set; }


        [XmlAttribute(AttributeName = "awarded_price")]
        public string price { get; set; }
        [XmlAttribute(AttributeName = "awarded_benefit_2nd")]
        public string benefit_2nd { get; set; }
        [XmlAttribute(AttributeName = "awarded_benefit_market")]
        public string benefit_market { get; set; }



        //cmla
        [XmlAttribute(AttributeName = "packaging")]
        public string packaging { get; set; }
        [XmlAttribute(AttributeName = "destination")]
        public string destination { get; set; }
    }

    [XmlRoot(ElementName = "bitumen_summary")]
    public class PAFBitumenSummary
    {
        [XmlAttribute(AttributeName = "customer")]
        public string customer { get; set; }

        [XmlAttribute(AttributeName = "tier")]
        public string tier { get; set; }

        [XmlAttribute(AttributeName = "price")]
        public string price { get; set; }

        [XmlAttribute(AttributeName = "quantity")]
        public string quantity { get; set; }

        [XmlAttribute(AttributeName = "selling_price_diff_argus")]
        public string selling_price_diff_argus { get; set; }

        [XmlAttribute(AttributeName = "selling_price_diff_hsfo")]
        public string selling_price_diff_hsfo { get; set; }
    }


    [XmlRoot(ElementName = "list_bitumen_summary")]
    public class List_PAFBitumenSummary
    {
        [XmlElement(ElementName = "bitumen_summary")]
        public List<PAFBitumenSummary> bitumen_summary { get; set; }
    }

    [XmlRoot(ElementName = "list_customer_review")]
    public class List_PAFCustomerReview
    {
        [XmlElement(ElementName = "customer_review")]
        public List<PAFCustomerReview> customer_reviews { get; set; }
    }

    [XmlRoot(ElementName = "list_awarded")]
    public class List_PAFAwarded
    {
        [XmlElement(ElementName = "awarded")]
        public List<PAFAwarded> awarded { get; set; }
    }


    [XmlRoot(ElementName = "transaction")]
    public class PAFEncrypt: PAFTransaction
    {
        [XmlElement(ElementName = "for_company")]
        public string for_company { get; set; }
        [XmlElement(ElementName = "doc_no")]
        public string doc_no { get; set; }
        [XmlElement(ElementName = "template_id")]
        public string template_id { get; set; }
        [XmlElement(ElementName = "awarded_summary")]
        public string awarded_summary { get; set; }

        [XmlElement(ElementName = "contract_period")]
        public string contract_period { get; set; }
         [XmlElement(ElementName = "pricing_period")]
        public string pricing_period { get; set; }

        [XmlElement(ElementName = "list_awarded")]
        public List_PAFAwarded list_awarded { get; set; }
        [XmlElement(ElementName = "list_customer_reviews")]
        public List_PAFCustomerReview list_customer_reviews { get; set; }


        //Bitumen
        [XmlElement(ElementName = "bitumen_grade")]
        public string bitumen_grade { get; set; }

        public List_PAFBitumenSummary list_bitumen_summary { get; set; }

        [XmlElement(ElementName = "weighted_price")]
        public string weighted_price { get; set; }
        [XmlElement(ElementName = "weighted_price_diff_argus")]
        public string weighted_price_diff_argus { get; set; }
        [XmlElement(ElementName = "weighted_price_diff_hsfo")]
        public string weighted_price_diff_hsfo { get; set; }

        [XmlElement(ElementName = "topic")]
        public string topic { get; set; }

        [XmlElement(ElementName = "brief_market")]
        public string brief_market { get; set; }
    }

    [XmlRoot(ElementName = "list_trx")]
    public class List_PAFtrx
    {
        [XmlElement(ElementName = "transaction")]
        public List<PAFEncrypt> PAFTransaction { get; set; }
    }
}