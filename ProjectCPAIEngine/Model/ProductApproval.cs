﻿using ProjectCPAIEngine.DAL.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    #region --- Domestic Sale CMCS ----
    public class ProductAprrovalData
    {
        public string PDA_ROW_ID { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_FORM_ID { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_TEMPLATE { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_FOR_COMPANY { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_TYPE { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_SUB_TYPE { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_TYPE_NOTE { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_BRIEF { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PDA_PRICE_UNIT { get; set; } = "";
    }

    public class ProductApprovalRootObject
    {
        public ProductApprovalRootObject()
        {
            //this.DATA = new ProductAprrovalData();
        }
        public PAF_DATA DATA { get; set; }
    }
    #endregion
}