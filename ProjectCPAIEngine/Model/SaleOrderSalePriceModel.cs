﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class SaleOrderSalePriceModel
    {
        public SaleOrderModel SaleOrder { get; set; }
        public List<SalesPriceCreateModel> SalePrice { get; set; }
        //public SalesPriceCreateModel SalePrice { get; set; }
    }
}