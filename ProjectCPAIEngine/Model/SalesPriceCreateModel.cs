﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Model
{
    public class SalesPriceCreateModel
    {
        public string DATAB { get; set; } //Delivery Date
        public string DATBI { get; set; } //Delivery Date
        public string KBETR { get; set; } //Unit Price
        public string KMEIN { get; set; } //Sales Unit
        public string KONWA { get; set; } //Currency
        public string KOTABNR { get; set; } //Global Config
        public decimal KPEIN { get; set; } //Global Config
        public string KRECH { get; set; } //Global Config
        public string KSCHL { get; set; } //Provisional Price
        public string KUNNR { get; set; } //Customer Code
        public string MATNR { get; set; } //Material
        public string VKORG { get; set; } //Company Code
        public string VTWEG { get; set; } //DC
        public string WERKS { get; set; } //Plant
        public string ZZVSTEL { get; set; }//Shipping Point
    }
}
