﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Model
{
    public class TceDetail
    {
        public string voy_no { get; set; }
        public string chi_data_id { get; set; }
        //public string chi_data_no { get; set; }
        public string ship_name { get; set; }
        public string ship_broker { get; set; }
        public string ship_owner { get; set; }
        public string dem { get; set; }
        public string flat_rate { get; set; }
        public string extra_cost { get; set; }
        public string laycan_load_from { get; set; }
        public string laycan_load_to { get; set; }
        public string top_fixture_value { get; set; }
        public string date_fixture { get; set; }
        public string td { get; set; }
        public string min_load { get; set; }
        public string reason { get; set; }
    }

    public class TceRootObject
    {
        public TceDetail tce_detail { get; set; }
    }
}