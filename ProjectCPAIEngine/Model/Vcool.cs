﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ProjectCPAIEngine.Flow.Utilities;
using com.pttict.engine.dal.Entity;
using ProjectCPAIEngine.DAL.Entity;
using System.Globalization;
using System.Text.RegularExpressions;

namespace ProjectCPAIEngine.Model {
    public class VcoRequesterInfo {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string area_unit { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_purchase { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_no { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_status_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string workflow_priority { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_tn { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_tn_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_sc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_sc_description { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCEP_SH { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCSC_SH { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_TNVP { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_SCVP { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_cmvp { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_CMCS_SH { get; set; }
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_scvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status_scsc { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string currentStatus { get; set; } = "";// zKavin.i Add 2018 04 19
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_reject_EVPC { get; set; }
    }

    public class VcoCrudeInfo {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tpc_plan_month { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tpc_plan_year { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchased_by { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_name { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string origin { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_date { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string benchmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string supplier { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string incoterm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kbbl_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kbbl_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string loading_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string crude_ref { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_type { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_method { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging_date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string discharging_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string premium { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string formula_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kt_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string quantity_kt_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_formula_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_quantity_kt_min { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string former_quantity_kt_max { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string propcessing_period_form { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string propcessing_period_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revised_date_from { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revised_date_to { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string premium_maximum { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string purchase_result { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_benchmark_price { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_incoterm { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string final_premium { get; set; } = "";
    }

    public class VcoComment {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string reason_for_purchasing { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_summary { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_summary_mobile { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scep { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scsc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tn { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_note { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_request { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_propose_discharge { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmcs_propose_final { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string evpc { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tnvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scsc_agree_flag { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string tnvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string cmvp_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string lp_run_attach_file { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scep_flag { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string scsc_flag { get; set; } = "";

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revise_scvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revise_cmvp { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string revise_tnvp { get; set; } = "";
    }

    public class DateHistory {
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string order { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string user_group { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string date_history { get; set; } = "";
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string status { get; set; } = "";

        public DateTime UpdateDate { get; set; }
    }


    public class VcoEtaDateHistory {
        public List<DateHistory> history { get; set; }
    }

    public class VcoCrudeYieldComparison {
        public List<VcoCrudeYield> yield { get; set; }
        public List<VcoCrudeYieldJson> comparison_json { get; set; }

        //public VcoCrudeYieldComparison()
        //{
        //    yield = new List<VcoCrudeYield>();
        //    comparison_json = new VcoCrudeYieldJson();
        //}
    }

    public class VcoCrudeYield {
        public string order { get; set; }
        public string yield { get; set; }
        public string base_value { get; set; }
        public string compare_crude { get; set; }
        public string compare_value { get; set; }
        public string delta_value { get; set; }
    }

    public class VcoCrudeYieldJson {
        public string selectedCrude { get; set; }
        public string isChecked { get; set; }
    }


    public class VcoRootObject {
        public VcoRequesterInfo requester_info { get; set; }
        public VcoCrudeInfo crude_info { get; set; }
        public VcoComment comment { get; set; }
        public VcoEtaDateHistory date_history { get; set; }
        public VcoCrudeYieldComparison crude_compare { get; set; }
    }


    public class VcoMappingMobile {
        public string userGroup { get; set; }
        public string status { get; set; }
        public string mobileObjectName { get; set; }
        public string mobilefield { get; set; }
        public string mappedfield { get; set; }
        public string mappedObjectName { get; set; }
    }

    public class VcoMappingMobileData {
        public List<VcoMappingMobile> _vcoMappingMobiles { get; set; }

        public const string txtNote = "note";
        public const string txtVcoComment = "comment";
        public const string txtVcoCrudeInfo = "crude_info";
        public const string txtVcoRequesterInfo = "requester_info";
        public const string txtVcoEtaDateHistory = "date_history";
        public const string txtVcoCrudeYieldComparison = "crude_compare";

        public const string txtJSONRequester = "requester_info";
        public const string txtJSONCrudeInfo = "crude_info";
        public const string txtJSONComment = "comment";
        public const string txtJSONDateHistory = "date_history";
        public const string txtJSONCrudeYieldComparison = "crude_compare";



        public void MapingToVcoRootObject(ref VcoRootObject vcoRootObject, string userGroup, string status
            , string note, Dictionary<string, Object> objectData) {
            try {
                
                if (vcoRootObject != null) {

                    if (CPAIConstantUtil.FLOW_IN_VP.Contains(status) || status == CPAIConstantUtil.STATUS_TERMINATED) {
                        if (userGroup.Contains("CMVP")) {
                            userGroup = "CMVP";
                        }
                        _vcoMappingMobiles = _vcoMappingMobiles.Where(f => f.userGroup == userGroup).ToList();
                    }

                    foreach (var item in _vcoMappingMobiles.Where(f =>
                    (f.status.Contains(status) || (f.status.Equals("*") && !status.Equals("TERMINATED") && !status.Contains("&REJECT"))
                  || (f.status.Equals("*&REJECT") && status.Contains("&REJECT"))
                  ))) {
                        if (item.mobileObjectName == txtNote) {
                            var objTarget = vcoRootObject.GetType().GetProperty(item.mappedObjectName).GetValue(vcoRootObject);
                            if (objTarget != null) {
                                objTarget.GetType().GetProperty(item.mappedfield).SetValue(objTarget, note);
                            }

                        } else {
                            if (objectData != null && objectData.ContainsKey(item.mappedObjectName)) {
                                Dictionary<string, object> mobileObject = ((Dictionary<string, object>)objectData[item.mappedObjectName]);
                                if (mobileObject.ContainsKey(item.mobilefield)) {
                                    var mappedData = (String)mobileObject[item.mobilefield];
                                    var objTarget = vcoRootObject.GetType().GetProperty(item.mappedObjectName).GetValue(vcoRootObject);
                                    if (objTarget != null) {
                                        objTarget.GetType().GetProperty(item.mappedfield).SetValue(objTarget, mappedData);
                                    }

                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {

                throw;
            }
        }

        public VcoMappingMobileData() {
            _vcoMappingMobiles = new List<VcoMappingMobile>();
            SCEP_ADD();
            SCEP_SH_ADD();
            SCEP_SH_ADD_REJECT();
            CMCS_ADD_1();
            TNPB_ADD();
            TNVP_ADD();
            TNVP_ADD_CANCEL();
            TNVP_ADD_REVISE();
            SCSC_ADD_1();
            SCSC_SH_ADD_1();
            SCSC_SH_ADD_1_REJECT();
            CMCS_ADD_2_Edit_ETA();
            SCVP_ADD();
            SCVP_ADD_CANCEL();
            SCVP_ADD_REVISE();
            CMVP_ADD();
            CMVP_ADD_CANCEL();
            CMVP_ADD_REVISE();
            SCEP_ADD_2();
            SCEP_SH_ADD_2();
            SCEP_SH_ADD_2_REJECT();
            CMCS_ADD_REVISE();
            SCSC_ADD_2_REVISE();
            SCSC_SH_ADD_2_REVISE();
            SCSC_SH_ADD_2_REVISE_REJECT();
            CMCS_ADD_REVISE_ETA();
            CMCS_ADD_CONFIRM_PRICE();
            CMCS_SH_ADD();
            CMCS_SH_ADD_REJECT();
            EVPC_ADD();
            EVPC_ADD_CANCEL();
        }

        private void SCEP_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP / WAITING RUN LP
            tempUserGroup = "SCEP";
            tempStatus = "WAITING RUN LP";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scep",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "premium_maximum",
                    mobilefield = "premium_maximum"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "propcessing_period_form",
                    mobilefield = "propcessing_period_form"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "propcessing_period_to",
                   mobilefield = "propcessing_period_to"
               });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "lp_run_summary",
                    mobilefield = "lp_run_summary"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "lp_run_summary_mobile",
                    mobilefield = "lp_run_summary_mobile"
                });
            // END SCEP / WAITING RUN LP
        }

        private void SCEP_SH_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP_SH 
            tempUserGroup = "SCEP_SH";
            tempStatus = "WAITING APPROVE RUN LP";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scep",
                    mobilefield = ""
                });
            // END SCEP_SH 
        }

        private void SCEP_SH_ADD_REJECT() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP_SH 
            tempUserGroup = "SCEP_SH";
            tempStatus = "WAITING APPROVE RUN LP&REJECT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_SCEP_SH",
                    mobilefield = ""
                });
            // END SCEP_SH 
        }

        private void CMCS_ADD_1() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS";
            tempStatus = "WAITING PROPOSE ETA DATE";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_discharge",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_from",
                    mobilefield = "discharging_date_from"
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_to",
                    mobilefield = "discharging_date_to"
                });
            // END CMCS
        }

        private void TNPB_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN TNPB 
            tempUserGroup = "TNPB";
            tempStatus = "WAITING CHECK IMPACT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "tn",
                    mobilefield = ""
                });
            // END TNPB 
        }

        private void TNVP_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN TNVP 
            tempUserGroup = "TNVP";
            tempStatus = "WAITING TNVP APPROVE";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "tnvp",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "revise_tnvp",
                    mobilefield = "revise_tnvp"
                });
            _vcoMappingMobiles.Add( // Cancel Document.
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONRequester,
                   mappedObjectName = txtVcoRequesterInfo,
                   mappedfield = "reason_reject_TNVP",
                   mobilefield = "reason_reject_TNVP"
               });
            // END TNVP 
        }

        private void TNVP_ADD_REVISE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN TNVP 
            tempUserGroup = "TNVP";
            tempStatus = "*&REJECT";
            _vcoMappingMobiles.Add( // Cancel Document.
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtNote,
                   mappedObjectName = txtVcoComment,
                   mappedfield = "revise_tnvp",
                   mobilefield = ""
               });
            // END TNVP 
        }

        private void TNVP_ADD_CANCEL() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN TNVP 
            tempUserGroup = "TNVP";
            tempStatus = "TERMINATED";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_TNVP",
                    mobilefield = ""
                });

            // END TNVP 
        }


        private void SCSC_ADD_1() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC";
            tempStatus = "WAITING CHECK TANK";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc_agree_flag",
                    mobilefield = "scsc_agree_flag"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "revised_date_from",
                   mobilefield = "revised_date_from"
               });
            _vcoMappingMobiles.Add(
           new VcoMappingMobile() {
               userGroup = tempUserGroup,
               status = tempStatus,
               mobileObjectName = txtJSONCrudeInfo,
               mappedObjectName = txtVcoCrudeInfo,
               mappedfield = "revised_date_to",
               mobilefield = "revised_date_to"
           });
            _vcoMappingMobiles.Add( // Cancel Document.
            new VcoMappingMobile() {
                userGroup = tempUserGroup,
                status = tempStatus,
                mobileObjectName = txtJSONComment,
                mappedObjectName = txtVcoComment,
                mappedfield = "cmcs_request",
                mobilefield = "cmcs_request"
            });
            // END SCSC 
        }


        private void SCSC_SH_ADD_1() { // Before revising
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC_SH";
            tempStatus = "WAITING APPROVE TANK";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc_agree_flag",
                    mobilefield = "scsc_agree_flag"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "revised_date_from",
                   mobilefield = "revised_date_from"
               });
            _vcoMappingMobiles.Add(
           new VcoMappingMobile() {
               userGroup = tempUserGroup,
               status = tempStatus,
               mobileObjectName = txtJSONCrudeInfo,
               mappedObjectName = txtVcoCrudeInfo,
               mappedfield = "revised_date_to",
               mobilefield = "revised_date_to"
           });
            _vcoMappingMobiles.Add( // Cancel Document.
            new VcoMappingMobile() {
                userGroup = tempUserGroup,
                status = tempStatus,
                mobileObjectName = txtJSONComment,
                mappedObjectName = txtVcoComment,
                mappedfield = "cmcs_request",
                mobilefield = "cmcs_request"
            });
            // END SCSC 
        }


        private void SCSC_SH_ADD_1_REJECT() { // Before revising
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC_SH";
            tempStatus = "WAITING APPROVE TANK&REJECT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_SCSC_SH",
                    mobilefield = ""
                });

            // END SCSC 
        }

        private void CMCS_ADD_2_Edit_ETA() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS";
            tempStatus = "WAITING EDIT ETA DATE";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_discharge",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_from",
                    mobilefield = "discharging_date_from"
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_to",
                    mobilefield = "discharging_date_to"
                });
            // END CMCS
        }

        private void SCVP_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "SCVP";
            tempStatus = "*";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scvp",
                    mobilefield = ""
                });
            //_vcoMappingMobiles.Add( // Cancel and reson to revise
            //    new VcoMappingMobile() {
            //        userGroup = tempUserGroup,
            //        status = tempStatus,
            //        mobileObjectName = txtJSONRequester,
            //        mappedObjectName = txtVcoRequesterInfo,
            //        mappedfield = "reason_reject_SCVP",
            //        mobilefield = "reason_reject_SCVP"
            //    });
            //_vcoMappingMobiles.Add(
            //    new VcoMappingMobile() {
            //        userGroup = tempUserGroup,
            //        status = tempStatus,
            //        mobileObjectName = txtJSONComment,
            //        mappedObjectName = txtVcoComment,
            //        mappedfield = "scep_flag",
            //        mobilefield = "scep_flag"
            //    });
            //_vcoMappingMobiles.Add(
            //    new VcoMappingMobile() {
            //        userGroup = tempUserGroup,
            //        status = tempStatus,
            //        mobileObjectName = txtJSONComment,
            //        mappedObjectName = txtVcoComment,
            //        mappedfield = "scsc_flag",
            //        mobilefield = "scsc_flag"
            //    });
            //_vcoMappingMobiles.Add(
            //   new VcoMappingMobile() {
            //       userGroup = tempUserGroup,
            //       status = tempStatus,
            //       mobileObjectName = txtJSONComment,
            //       mappedObjectName = txtVcoComment,
            //       mappedfield = "revise_scvp",
            //       mobilefield = "revise_scvp"
            //   });
            // END CMCS
        }

        private void SCVP_ADD_REVISE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "SCVP";
            tempStatus = "*&REJECT";

            _vcoMappingMobiles.Add( // Cancel and reson to revise
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "revise_scvp",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add( // Cancel and reson to revise
              new VcoMappingMobile() {
                  userGroup = tempUserGroup,
                  status = tempStatus,
                  mobileObjectName = txtJSONRequester,
                  mappedObjectName = txtVcoRequesterInfo,
                  mappedfield = "reason_reject_SCVP",
                  mobilefield = "reason_reject_SCVP"
              });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scep_flag",
                    mobilefield = "scep_flag"
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc_flag",
                    mobilefield = "scsc_flag"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONComment,
                   mappedObjectName = txtVcoComment,
                   mappedfield = "revise_scvp",
                   mobilefield = "revise_scvp"
               });
            // END CMCS
        }

        private void SCVP_ADD_CANCEL() {
            var tempUserGroup = "";
            var tempStatus = "";


            tempUserGroup = "SCVP";
            tempStatus = "TERMINATED";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_SCVP",
                    mobilefield = ""
                });

        }



        private void CMVP_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMVP";
            tempStatus = "*";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmvp",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add( // Cancel and reson to revise
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONRequester,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_TNVP",
                    mobilefield = "reason_reject_TNVP"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONComment,
                   mappedObjectName = txtVcoComment,
                   mappedfield = "revise_cmvp",
                   mobilefield = "revise_cmvp"
               });
            // END CMCS
        }

        private void CMVP_ADD_REVISE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMVP";
            tempStatus = "*&REJECT";

            _vcoMappingMobiles.Add( // Cancel and reson to revise
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "revise_cmvp",
                    mobilefield = ""
                });

            // END CMCS
        }


        private void CMVP_ADD_CANCEL() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMVP";
            tempStatus = "TERMINATED";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_TNVP",
                    mobilefield = ""
                });
            // END CMCS
        }

        private void SCEP_ADD_2() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP / WAITING RUN LP
            tempUserGroup = "SCEP";
            tempStatus = "REVISE LP RUN";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scep",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "premium_maximum",
                    mobilefield = "premium_maximum"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "propcessing_period_form",
                    mobilefield = "propcessing_period_form"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "propcessing_period_to",
                   mobilefield = "propcessing_period_to"
               });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "lp_run_summary",
                    mobilefield = "lp_run_summary"
                });
            _vcoMappingMobiles.Add( // SCEP / WAITING RUN LP
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "lp_run_summary_mobile",
                    mobilefield = "lp_run_summary_mobile"
                });
            // END SCEP / WAITING RUN LP
        }

        private void SCEP_SH_ADD_2() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP_SH 
            tempUserGroup = "SCEP_SH";
            tempStatus = "REVISE LP VERIFICATION";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scep",
                    mobilefield = ""
                });
            // END SCEP_SH 
        }


        private void SCEP_SH_ADD_2_REJECT() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP_SH 
            tempUserGroup = "SCEP_SH";
            tempStatus = "REVISE LP VERIFICATION&REJECT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_SCEP_SH",
                    mobilefield = ""
                });
            // END SCEP_SH 
        }

        private void CMCS_ADD_REVISE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCEP_SH 
            tempUserGroup = "CMCS";
            tempStatus = "REVISE PURCHASE COMMENT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_discharge",
                    mobilefield = "cmcs_propose_discharge"
                });
            // END SCEP_SH 
        }

        private void SCSC_ADD_2_REVISE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC";
            tempStatus = "REVISE TANK AVAILABILITY";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc_agree_flag",
                    mobilefield = "scsc_agree_flag"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "revised_date_from",
                   mobilefield = "revised_date_from"
               });
            _vcoMappingMobiles.Add(
           new VcoMappingMobile() {
               userGroup = tempUserGroup,
               status = tempStatus,
               mobileObjectName = txtJSONCrudeInfo,
               mappedObjectName = txtVcoCrudeInfo,
               mappedfield = "revised_date_to",
               mobilefield = "revised_date_to"
           });
            _vcoMappingMobiles.Add( // Cancel Document.
            new VcoMappingMobile() {
                userGroup = tempUserGroup,
                status = tempStatus,
                mobileObjectName = txtJSONComment,
                mappedObjectName = txtVcoComment,
                mappedfield = "cmcs_request",
                mobilefield = "cmcs_request"
            });
            // END SCSC 
        }


        private void SCSC_SH_ADD_2_REVISE() { // Before revising
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC_SH";
            tempStatus = "REVISE TANK VERIFICATION";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONComment,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "scsc_agree_flag",
                    mobilefield = "scsc_agree_flag"
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "revised_date_from",
                   mobilefield = "revised_date_from"
               });
            _vcoMappingMobiles.Add(
           new VcoMappingMobile() {
               userGroup = tempUserGroup,
               status = tempStatus,
               mobileObjectName = txtJSONCrudeInfo,
               mappedObjectName = txtVcoCrudeInfo,
               mappedfield = "revised_date_to",
               mobilefield = "revised_date_to"
           });
            _vcoMappingMobiles.Add( // Cancel Document.
            new VcoMappingMobile() {
                userGroup = tempUserGroup,
                status = tempStatus,
                mobileObjectName = txtJSONComment,
                mappedObjectName = txtVcoComment,
                mappedfield = "cmcs_request",
                mobilefield = "cmcs_request"
            });
            // END SCSC 
        }

        private void SCSC_SH_ADD_2_REVISE_REJECT() { // Before revising
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN SCSC 
            tempUserGroup = "SCSC_SH";
            tempStatus = "REVISE TANK VERIFICATION&REJECT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_SCSC_SH",
                    mobilefield = ""
                });

            // END SCSC 
        }

        private void CMCS_ADD_REVISE_ETA() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS";
            tempStatus = "REVISE PROPOSE ETA";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_discharge",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_from",
                    mobilefield = "discharging_date_from"
                });
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtJSONCrudeInfo,
                    mappedObjectName = txtVcoCrudeInfo,
                    mappedfield = "discharging_date_to",
                    mobilefield = "discharging_date_to"
                });
            // END CMCS
        }

        private void CMCS_ADD_CONFIRM_PRICE() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS";
            tempStatus = "WAITING CONFIRM PRICE";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_final",
                    mobilefield = ""
                });
        }

        private void CMCS_SH_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS_SH";
            tempStatus = "WAITING APPROVE PRICE";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "cmcs_propose_final",
                    mobilefield = ""
                });
            _vcoMappingMobiles.Add(
               new VcoMappingMobile() {
                   userGroup = tempUserGroup,
                   status = tempStatus,
                   mobileObjectName = txtJSONCrudeInfo,
                   mappedObjectName = txtVcoCrudeInfo,
                   mappedfield = "purchase_result",
                   mobilefield = "purchase_result"
               });
        }

        private void CMCS_SH_ADD_REJECT() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "CMCS_SH";
            tempStatus = "WAITING APPROVE PRICE&REJECT";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_CMCS_SH",
                    mobilefield = ""
                });
        }

        private void EVPC_ADD() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "EVPC";
            tempStatus = "WAITING APPROVE SUMMARY";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoComment,
                    mappedfield = "evpc",
                    mobilefield = ""
                });

        }

        private void EVPC_ADD_CANCEL() {
            var tempUserGroup = "";
            var tempStatus = "";

            // BEGIN CMCS 
            tempUserGroup = "EVPC";
            tempStatus = "TERMINATED";
            _vcoMappingMobiles.Add(
                new VcoMappingMobile() {
                    userGroup = tempUserGroup,
                    status = tempStatus,
                    mobileObjectName = txtNote,
                    mappedObjectName = txtVcoRequesterInfo,
                    mappedfield = "reason_reject_EVPC",
                    mobilefield = ""
                });

        }
    }



    public class VCOOLCounterHelper {
        EntitiesEngine entity = new EntitiesEngine();
        EntityCPAIEngine cpai_entity = new EntityCPAIEngine();

        public DateTime parseInputDate(string date) {
            try {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                return r;
            } catch (Exception ex) {
                throw ex;
            }
        }
        public DateTime parsePurchaseDate(string date) {
            try {
                string dateTimeString = Regex.Replace(date, @"[^\u0000-\u007F]", string.Empty);
                DateTime r = new DateTime();
                if (dateTimeString.Contains('-')) {
                    if (dateTimeString.Contains(':')) {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    } else {
                        r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    }
                } else if (dateTimeString.Contains(':')) {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                } else {
                    r = DateTime.ParseExact(dateTimeString, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                //DateTime r = DateTime.ParseExact(dateTimeString, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                return r;
            } catch (Exception ex) {
                throw ex;
            }
        }

        public List<FUNCTION_TRANSACTION> FindByTransactionByFuncIndex(List<FUNCTION_TRANSACTION> functionTransaction, string[] function, string[] index, string[] type, string user_group, string from_date_str, string to_date_str) {
            try {
                var isVPProcess = false;
                var vpProcess = new List<string>();
                var otherProcess = new List<string>();
                string whereString = "";
                string functionString = "";
                List<FUNCTION_TRANSACTION> lstFunction = new List<FUNCTION_TRANSACTION>();
                string query = @"SELECT FUNCTION_TRANSACTION.FTX_ROW_ID AS FTX_ROW_ID, FUNCTION_TRANSACTION.FTX_FK_FUNCTION, FUNCTION_TRANSACTION.FTX_TRANS_ID, FUNCTION_TRANSACTION.FTX_APP_ID, FUNCTION_TRANSACTION.FTX_REQ_TRANS, FUNCTION_TRANSACTION.FTX_RETURN_STATUS, FUNCTION_TRANSACTION.FTX_RETURN_NAME_SPACE, FUNCTION_TRANSACTION.FTX_RETURN_CODE, FUNCTION_TRANSACTION.FTX_RETURN_DESC, FUNCTION_TRANSACTION.FTX_STATE_ACTION, FUNCTION_TRANSACTION.FTX_CURRENT_NAME_SPACE, FUNCTION_TRANSACTION.FTX_CURRENT_CODE, FUNCTION_TRANSACTION.FTX_CURRENT_DESC, FUNCTION_TRANSACTION.FTX_REQUEST_DATE, FUNCTION_TRANSACTION.FTX_RESPONSE_DATE, FUNCTION_TRANSACTION.FTX_ACCUM_RETRY, FUNCTION_TRANSACTION.FTX_RETRY_COUNT, FUNCTION_TRANSACTION.FTX_CURRENT_STATE, FUNCTION_TRANSACTION.FTX_NEXT_STATE, FUNCTION_TRANSACTION.FTX_WAKEUP, FUNCTION_TRANSACTION.FTX_FWD_RESP_CODE_FLAG, FUNCTION_TRANSACTION.FTX_CREATED, FUNCTION_TRANSACTION.FTX_CREATED_BY, FUNCTION_TRANSACTION.FTX_UPDATED, FUNCTION_TRANSACTION.FTX_UPDATED_BY, FUNCTION_TRANSACTION.FTX_RETURN_MESSAGE, FUNCTION_TRANSACTION.FTX_PARENT_TRX_ID, FUNCTION_TRANSACTION.FTX_PARENT_ACT_ID, FUNCTION_TRANSACTION.FTX_INDEX1, FUNCTION_TRANSACTION.FTX_INDEX2, FUNCTION_TRANSACTION.FTX_INDEX3, FUNCTION_TRANSACTION.FTX_INDEX4 AS FTX_INDEX4, FUNCTION_TRANSACTION.FTX_INDEX5, FUNCTION_TRANSACTION.FTX_INDEX6 AS FTX_INDEX6, FUNCTION_TRANSACTION.FTX_INDEX7, FUNCTION_TRANSACTION.FTX_INDEX8, FUNCTION_TRANSACTION.FTX_INDEX9, FUNCTION_TRANSACTION.FTX_INDEX10 AS FTX_INDEX10, FUNCTION_TRANSACTION.FTX_INDEX11, FUNCTION_TRANSACTION.FTX_INDEX12, FUNCTION_TRANSACTION.FTX_INDEX13, FUNCTION_TRANSACTION.FTX_INDEX14, FUNCTION_TRANSACTION.FTX_INDEX15, FUNCTION_TRANSACTION.FTX_INDEX16, FUNCTION_TRANSACTION.FTX_INDEX17, FUNCTION_TRANSACTION.FTX_INDEX18, FUNCTION_TRANSACTION.FTX_INDEX19, FUNCTION_TRANSACTION.FTX_INDEX20 "
                     + " FROM FUNCTION_TRANSACTION JOIN VCO_DATA ON VCO_DATA.VCDA_PURCHASE_NO = FUNCTION_TRANSACTION.FTX_INDEX8 "
                     + " ";

                foreach (var item in function) {
                    if (!String.IsNullOrEmpty(item)) {
                        if (functionString != "") {
                            functionString += " OR ";
                        }
                        functionString += " FUNCTION_TRANSACTION.FTX_FK_FUNCTION = " + item + " ";
                    }
                }

                foreach (var item in index) {
                    if (!String.IsNullOrEmpty(item)) {
                        if (CPAIConstantUtil.FLOW_IN_VP.Contains(item) || CPAIConstantUtil.FLOW_SCSC.Contains(item)
                            || CPAIConstantUtil.FLOW_SCEP.Contains(item) || CPAIConstantUtil.FLOW_CMCS.Contains(item)) {
                            if (!vpProcess.Contains(item)) {
                                if (!item.Equals(CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK)) {
                                    vpProcess.Add(item);
                                }
                            }
                            isVPProcess = true;
                        } else {
                            if (!otherProcess.Contains(item)) {
                                otherProcess.Add(item);
                            }
                        }
                    }
                }

                foreach (var item in otherProcess) {
                    if (whereString != "") {
                        whereString += " OR ";
                    }
                    whereString += " (FUNCTION_TRANSACTION.FTX_INDEX4 = '" + item.ToString() + "' AND VCO_DATA.VCDA_STATUS != '" + CPAIConstantUtil.STATUS_TERMINATED + "' ) ";
                }

                if (isVPProcess == false) {
                    //return functionTransaction;
                } else {

                    foreach (var item in vpProcess) {
                        if (whereString != "") {
                            whereString += " OR ";
                        }

                        if (CPAIConstantUtil.FLOW_IN_VP.Contains(item)) { // VP Process

                            if (item.Equals(CPAIConstantUtil.STATUS_APPROVED_BY_CMVP)) { //SCVP
                                whereString += " ((FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_APPROVED_BY_CMVP + "' OR FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK + "' ) " +
                                     " AND  NVL( VCO_DATA.VCDA_SC_STATUS,'None') != '" + CPAIConstantUtil.STATUS_SCVP_REQUEST_TO_REVISE + "' AND VCO_DATA.VCDA_STATUS = '" + CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL + "')  ";
                            } else {//CMVP
                                whereString += " ((FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_APPROVED_BY_SCVP + "' OR FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK + "' ) " +
                                    " AND NVL( VCO_DATA.VCDA_CM_STATUS,'None') != '" + CPAIConstantUtil.STATUS_REVISE_PURCHASE_COMMENT + "' AND VCO_DATA.VCDA_STATUS = '" + CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL + "') ";
                            }
                        } else if (CPAIConstantUtil.FLOW_SCSC.Contains(item)) { // SCSC Revise Process
                            whereString += " ((FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_APPROVED_BY_CMVP + "' OR FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK + "' ) " +
                                    " AND NVL( VCO_DATA.VCDA_REVISE_SCSC_STATUS,'None') = '" + item + "' AND VCO_DATA.VCDA_STATUS = '" + CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL + "')  ";
                        } else if (CPAIConstantUtil.FLOW_SCEP.Contains(item)) { // SCEP Revise Process
                            whereString += " ((FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_APPROVED_BY_CMVP + "' OR FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK + "' ) " +
                                    " AND  NVL( VCO_DATA.VCDA_REVISE_SCEP_STATUS,'None') = '" + item + "' AND VCO_DATA.VCDA_STATUS = '" + CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL + "')  ";
                        } else if (CPAIConstantUtil.FLOW_CMCS.Contains(item)) { // CM Revise Process
                            whereString += " ((FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_APPROVED_BY_SCVP + "' OR FUNCTION_TRANSACTION.FTX_INDEX4 = '" + CPAIConstantUtil.STATUS_WAITING_VP_APPROVE_TANK + "' ) " +
                                    " AND   NVL( VCO_DATA.VCDA_CM_STATUS,'None') = '" + item + "' AND VCO_DATA.VCDA_STATUS = '" + CPAIConstantUtil.STATUS_CONSENSUS_APPROVAL + "') ";
                        }
                    }

                }

                query += " WHERE 1=1  " + (whereString != "" ? " AND (" + whereString + ") " : "") + "  " +
                       "  " + (functionString != "" ? " AND (" + functionString + ") " : "") + "";

                lstFunction = entity.Database.SqlQuery<FUNCTION_TRANSACTION>(query).ToList();

                if (!string.IsNullOrEmpty(from_date_str) && !string.IsNullOrEmpty(to_date_str)) {
                    DateTime fDate = parseInputDate(from_date_str);
                    DateTime tDate = parseInputDate(to_date_str);
                    lstFunction = lstFunction.Where(x => x.FTX_INDEX6 != null && (parsePurchaseDate(x.FTX_INDEX6) >= fDate) && (parsePurchaseDate(x.FTX_INDEX6) <= tDate)).ToList();
                }

                return lstFunction;

            } catch (Exception ex) {
                throw ex;
            }

        }

    }


}