﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Flow.Utilities
{
    public class CPAIConstantRespCodeUtil
    {
        public static string INTERNAL_SYSTEM_EXCEPTION_CPAI_RESP_CODE   = "10000001";
        public static string INVALID_CHANNEL_RESP_CODE                  = "10000002";

        public static string INVALID_DATA_DETAIL_RESP_CODE      = "10000003";
        public static string INVALID_CURRENT_ACTION_RESP_CODE   = "10000004";
        public static string INVALID_NEXT_STATUS_RESP_CODE      = "10000005";
        public static string INVALID_USER_RESP_CODE             = "10000006";
        public static string INVALID_USER_GROUP_RESP_CODE       = "10000007";
        public static string INVALID_SYSTEM_RESP_CODE           = "10000008";
        public static string INVALID_TYPE_RESP_CODE             = "10000009";
        public static string INVALID_NOTE_RESP_CODE             = "10000010";
        public static string INVALID_ACTION_RESP_CODE           = "10000011";
        public static string INVALID_PRE_DEALS_SYSTEM_RESP_CODE = "10000012";
        public static string INVALID_PRE_DEALS_TYPE_RESP_CODE   = "10000013";
        public static string INVALID_USER_ID_RESP_CODE          = "10000014";
        public static string INVALID_USER_PASSWORD_RESP_CODE    = "10000015";
        public static string INVALID_USER_SYSTEM_RESP_CODE      = "10000016";
        public static string INVALID_USER_OS_RESP_CODE          = "10000017";
        public static string INVALID_USER_NOTI_ID_RESP_CODE     = "10000018";
        public static string INVALID_USER_AD_FLAG_RESP_CODE     = "10000019";
      


        public static string ACTION_APPROVE_RESP_CODE           = "10000020";
        public static string ACTION_DRAFT_RESP_CODE             = "10000021";
        public static string ACTION_CANCEL_RESP_CODE            = "10000022";
        public static string ACTION_REJECT_RESP_CODE            = "10000023";
        public static string CALL_AD_RESP_CODE                  = "10000024";
        public static string NOT_CALL_AD_RESP_CODE              = "10000025";

        public static string INVALID_RESEND_TRANSACTION_ID_RESP_CODE = "10000026";
        public static string INVALID_RESEND_BY_RESP_CODE             = "10000027";

        public static string INVALID_ROWS_PER_PAGE_RESP_CODE         = "10000028";
        public static string INVALID_PAGE_NUMBER_RESP_CODE           = "10000029";
        public static string INVALID_INPUT_DATE_RESP_CODE            = "10000030";
        public static string INVALID_STATUS_RESP_CODE                 = "10000033"; //f1

        public static string ACTION_FUNCTION_NOT_FOUND_RESP_CODE        = "10000031";
        public static string INVALID_TRANSACTION_ID_RESP_CODE           = "10000032";

        public static string FUNCTION_TRANSACTION_NOT_FOUND_RESP_CODE   = "10000039";
        public static string ACTION_CONTROL_NOT_FOUND_RESP_CODE         = "10000034";
        public static string ACTION_BUTTON_NOT_FOUND_RESP_CODE          = "10000035";
        public static string EXTEND_TRANSACTION_NOT_FOUND_RESP_CODE     = "10000036";
        public static string DATA_HISTORY_NOT_FOUND_RESP_CODE           = "10000037";

        public static string CANNOT_DECRYPT_MESSAGE_RESP_CODE           = "10000038";

        public static string CAN_NOT_INSERT_DB_RESP_CODE = "10000040";

        public static string ACTION_EDIT_SCH_RESP_CODE = "10000041";
        public static string ACTION_EDIT_ACT_RESP_CODE = "10000042";

        public static string INVALID_BUTTON_TYPE_RESP_CODE = "10000043";

        public static string CANNOT_VALIDATE_SCHEDULE_CODE = "10000044";
        public static string CANNOT_VALIDATE_ACTIVITY_CODE = "10000045";

        public static string ACTION_EDIT_TCE_RESP_CODE = "10000046";
        public static string DUP_DATA_RESP_CODE = "10000047";

        public static string CANNOT_ENCRYPT_MESSAGE_RESP_CODE = "10000048";
        public static string NOT_FOUND_DATA_SEND_MAIL_RESP_CODE = "10000049";
        public static string NOT_FOUND_DATA_SEND_NOTI_RESP_CODE = "10000050";

        public static string NOT_AUTHORIZED_RESP_CODE = "10000051";
        public static string NOT_CAL_AVG_WS_RESP_CODE = "10000052";
        public static string NOT_CAL_AVG_WS_BITR_TD2_RESP_CODE = "10000053";

        public static string INVALID_KEY_RESP_CODE = "10000054";
        public static string INVALID_SET_FLAG_RESP_CODE = "10000055";
        public static string USER_NOT_FOUND_RESP_CODE = "10000056";
        public static string UPDATE_DATA_ERROR_RESP_CODE = "10000057";


        public static string ACTION_SH_APPROVE_RESP_CODE = "10000058";

        public static string GEN_PDF_INVALID_DESTINTION_RESP_CODE = "10000059";
        public static string GEN_PDF_TXN_ID_RESP_CODE = "10000060";
        public static string GEN_PDF_INVALID_PATH_RESP_CODE = "10000061";
        public static string GEN_PDF_INVALID_FUNCTION_RESP_CODE = "10000062";
        public static string INVALID_TRIPNO_RESP_CODE = "10000063";
        public static string PLANNIG_BEFORE_BOOKING_RESP_CODE = "10000064";


        public static string ACTION_APPROVE_2_RESP_CODE = "10000065";
        public static string ACTION_APPROVE_3_RESP_CODE = "10000066";
        public static string NOT_FOUND_TCE_WS_RESP_CODE = "10000067";
        public static string NOT_WS_RESP_CODE           = "10000068";

        public static string ACTION_APPROVE_AND_UPDATE_RESP_CODE = "10000069";

        public static string UPDATE_MOBILE_RESP_CODE = "10000070";

        //public static string ACTION_SUBMIT_T_RESP_CODE = "10000071";
        //public static string ACTION_SUBMIT_O_RESP_CODE = "10000072";
        public static string ACTION_EDIT_DEAL_RESP_CODE = "10000071";
        public static string ACTION_BYPASS_RESP_CODE = "10000072";

        public static string DEAL_TICKETED_RESP_CODE = "10000073";

        public static string ACTION_COMPLETE_RESP_CODE = "10000074";
        public static string ACTION_REVISE_RESP_CODE = "10000075";
        public static string ACTION_FAIL_RESP_CODE = "10000076";
        public static string ACTION_EDIT_CONTRACT_RESP_CODE = "10000077";

        public static string CRUDE_PURCHASE_BOND_STATUS_UPDATE_RESP_CODE = "10000078";
    }
}