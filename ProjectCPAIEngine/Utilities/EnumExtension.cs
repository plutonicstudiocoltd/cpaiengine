﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public static class EnumExtension
    {
        public static string ToDescription(this Enum item)
        {
            var memberInfo = item.GetType().GetMember(item.ToString()).FirstOrDefault();
            var attribute = (DescriptionAttribute)memberInfo.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
            return attribute?.Description ?? item.ToString();
        }
    }
}