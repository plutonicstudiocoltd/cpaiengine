﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;

namespace ProjectCPAIEngine.Utilities
{
    public static class JSONSetting
    {
        private static string strJSON = "{   \"type_of_purchase\": [      \"Fix Price\",      \"Floating Price\",      \"Others\"   ],   \"supplying_location\": [      \"Singapore\",      \"Malaysia\",     \"Thailand Sriracha/Rayong\",      \"Fujairah\",      \"Others\"   ],   \"product_name\": [   {	\"full_name\":\"High Sulphur Fuel Oil 380\",	\"short_name\":\"HSFO\"   },   {	\"full_name\":\"High Sulphur Fuel Oil 180\",	\"short_name\":\"HSFO\"   },   {	\"full_name\":\"Marine Gas Oil\",	\"short_name\":\"MGO\"   },   {	\"full_name\":\"Others\",	\"short_name\":\"others\"   }   ],   \"product_unit\": [      \"MT\"   ],   \"contract_type\": [      \"Spot\",      \"Term\"   ],   \"payment_terms\": [      {        \"type\":\"Per-payment\",        \"flag\": \"N\"      },    {         \"type\": \"Credit\",         \"flag\": \"Y\"   },      {         \"type\": \"L/C\",         \"flag\": \"Y\"      },      {         \"type\": \"Others\",         \"flag\": \"N\"      }   ],   \"proposal_reason\": [      \"Best price\",      \"Others\"   ],\"charter_For\": [ {\"value\":\"CMPSIE\", \"text\":\"CMPS - International Export\"},{\"value\":\"CMPSII\", \"text\":\"CMPS - International Import\"},{\"value\":\"CMPSDE\", \"text\":\"CMPS - Domestic Sale\"}],\"unitChit\": [      \"MT\",      \"BBL\", \"LITE\" , \"OTHERS\"   ]}";



        public static Setting getSetting(string TypeConfig)
        {
            string JsonD = MasterData.GetJsonMasterSetting(TypeConfig);
            JObject json = JObject.Parse(string.IsNullOrEmpty(JsonD) ? strJSON : JsonD);
            Setting myDeserializedObjList = (Setting)Newtonsoft.Json.JsonConvert.DeserializeObject(json.ToString(), typeof(Setting));
            return myDeserializedObjList;
        }

        public static string getGlobalConfig(string TypeConfig)
        {
            return MasterData.GetJsonMasterSetting(TypeConfig);
        }



        public static List<string> checkLength(List<string> lstString, int length)
        {
            if (lstString != null)
            {
                for (int i = 0; i < lstString.Count; i++)
                {
                    if (lstString[i].Length > length) lstString[i] = lstString[i].Substring(0, length);
                }

            }
            return lstString;
        }
    }

    [Serializable]
    public class ProductJson
    {
        public string full_name { get; set; }
        public string short_name { get; set; }
        public string spac_name { get; set; }
    }

    [Serializable]
    public class PaymentTerm
    {
        public string type { get; set; }
        public List<string> sub_type { get; set; }
        public string flag { get; set; }
    }

    [Serializable]
    public class VesselSize
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class CharterInFor
    {
        public string value { get; set; }
        public string text { get; set; }
    }

    [Serializable]
    public class FeedStock
    {
        public string key { get; set; }
        public string unit { get; set; }
    }

    [Serializable]
    public class TCEMK
    {
        public string theoritical_days { get; set; }
        public string min_load { get; set; }
    }

    [Serializable]
    public class AssayPosition
    {
        public static string REFERENCE = "reference";
        public static string CRUDE_NAME = "crude";
        public static string ORIGIN = "origin";
        public static string ASSAY_DATE = "assay date";
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class HedgeType
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Annual_FMW_Type
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Annual_FMW_Value_Type
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class UNIT
    {
        public string price { get; set; }
        public string volume { get; set; }
        public string volume_mnt { get; set; }
    }

    [Serializable]
    public class Fee
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Tool
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Unit_Payment
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Create_Deal_Config
    {
        public string company { get; set; }
        public string counterpaties { get; set; }
    }

    [Serializable]
    public class Activation_Status
    {
        public string key { get; set; }
        public string value { get; set; }
        public string visibility { get; set; }
    }

    [Serializable]
    public class Unit_Price
    {
        public string key { get; set; }
        public string value { get; set; }
        public string mapping { get; set; }
    }

    [Serializable]
    public class Unit_Volume
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Min_Max
    {
        public string key { get; set; }
        public string value { get; set; }
    }



    [Serializable]
    public class Knock_Option
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Condition_Option
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Condition_Operand_Option
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    [Serializable]
    public class Formular_Valiable
    {
        public string key { get; set; }
        public string value { get; set; }
    }


    [Serializable]
    public class Condition_Operator_Formular
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    

    [Serializable]
    public class Condition_Operator_Option
    {
        public string key { get; set; }
        public string value { get; set; }
    }


    [Serializable]
    public class Setting
    {
        private List<string> _type_of_purchase;
        private List<string> _supplying_location;
        private List<string> _product_name;
        private List<string> _product_unit;
        private List<string> _contract_type;
        private List<string> _proposal_reason;
        private List<string> _freight;
        private List<string> _prices_term;
        private List<string> _title_eng;
        private List<string> _title_thai;
        private List<string> _company;
        private List<string> _system;
        private List<string> _ad_login_flag;
        private List<string> _status;
        private List<string> _user_group;
        private List<string> _user_group_system;
        private List<string> _type_of_vessel;
        private List<string> _for_feedstock;
        private List<string> _term;
        private List<string> _purchase;
        private List<string> _purchase_result;
        private List<string> _incoterms;
        private List<string> _bechmark_price;
        private List<string> _load_port_volume;
        private List<string> _unitChit;
        private List<string> _supply_source;
        private List<string> _crude_yield;

        private List<string> _fcl_unit;
        private List<string> _fcl_offer_unit;
        private List<string> _fcl_demurrage_unit;
        private List<string> _fcl_offer_by;
        private List<string> _actual_purchase;
        private List<string> _charter_For;

        public List<string> type_of_purchase { get { return JSONSetting.checkLength(_type_of_purchase, 100); } set { _type_of_purchase = value; } }
        public List<string> supplying_location { get { return JSONSetting.checkLength(_supplying_location, 100); } set { _supplying_location = value; } }
        public List<string> product_name { get { return JSONSetting.checkLength(_product_name, 100); } set { _product_name = value; } }
        public List<string> product_unit { get { return JSONSetting.checkLength(_product_unit, 100); } set { _product_unit = value; } }
        public List<string> contract_type { get { return JSONSetting.checkLength(_contract_type, 20); } set { _contract_type = value; } }
        public List<PaymentTerm> payment_terms { get; set; }
        public List<string> proposal_reason { get { return JSONSetting.checkLength(_proposal_reason, 100); } set { _proposal_reason = value; } }
        public List<VesselSize> vessel_size { get; set; }
        public List<string> freight { get { return JSONSetting.checkLength(_freight, 100); } set { _freight = value; } }
        public List<string> prices_term { get { return JSONSetting.checkLength(_prices_term, 100); } set { _prices_term = value; } }
        public List<CharterInFor> charter_For { get; set; }
        public List<MT_VEHICLE> vehicle { get; set; }
        public List<MT_VENDOR> vendor { get; set; }
        public List<ProductJson> products { get; set; }
        public List<string> title_eng { get { return JSONSetting.checkLength(_title_eng, 100); } set { _title_eng = value; } }
        public List<string> title_thai { get { return JSONSetting.checkLength(_title_thai, 100); } set { _title_thai = value; } }
        public List<string> company { get { return JSONSetting.checkLength(_company, 100); } set { _company = value; } }
        public List<string> system { get { return JSONSetting.checkLength(_system, 100); } set { _system = value; } }
        public List<string> ad_login_flag { get { return JSONSetting.checkLength(_ad_login_flag, 100); } set { _ad_login_flag = value; } }
        public List<string> status { get { return JSONSetting.checkLength(_status, 100); } set { _status = value; } }
        public List<string> user_group { get { return JSONSetting.checkLength(_user_group, 100); } set { _user_group = value; } }
        public List<string> user_group_system { get { return JSONSetting.checkLength(_user_group_system, 100); } set { _user_group_system = value; } }
        public List<string> type_of_vessel { get { return JSONSetting.checkLength(_type_of_vessel, 100); } set { _type_of_vessel = value; } }
        public List<string> for_feedstock { get { return JSONSetting.checkLength(_for_feedstock, 100); } set { _for_feedstock = value; } }
        public List<FeedStock> feedstock { get; set; }
        public List<string> term { get { return JSONSetting.checkLength(_term, 100); } set { _term = value; } }
        public List<string> purchase { get { return JSONSetting.checkLength(_purchase, 100); } set { _purchase = value; } }
        public List<string> purchase_result { get { return JSONSetting.checkLength(_purchase_result, 100); } set { _purchase_result = value; } }
        public List<string> incoterms { get { return JSONSetting.checkLength(_incoterms, 100); } set { _incoterms = value; } }
        public List<string> bechmark_price { get { return JSONSetting.checkLength(_bechmark_price, 100); } set { _bechmark_price = value; } }
        public List<string> unitChit { get { return JSONSetting.checkLength(_unitChit, 100); } set { _unitChit = value; } }
        public List<string> supply_source { get { return JSONSetting.checkLength(_supply_source, 100); } set { _supply_source = value; } }
        public List<string> crude_yield { get { return JSONSetting.checkLength(_crude_yield, 100); } set { _crude_yield = value; } }

        public List<string> load_port_volume { get { return JSONSetting.checkLength(_load_port_volume, 100); } set { _load_port_volume = value; } }

        public List<string> toterance_option { get; set; }
        public List<string> toterance_type { get; set; }

        public List<string> workflow_priority { get; set; }
        public List<string> crude_categories { get; set; }
        public List<string> crude_kerogen { get; set; }
        public List<string> crude_characteristic { get; set; }
        public List<string> crude_maturity { get; set; }
        public List<string> assay_from { get; set; }
        public List<string> file_type { get; set; }
        public List<string> approval_day { get; set; }
        public List<AssayPosition> assay_position { get; set; }
        public List<string> cam_status { get; set; }
        public List<string> order_by { get; set; }
        public List<HedgeType> hedge_type { get; set; }
        public List<Annual_FMW_Type> annual_fmw_type { get; set; }
        public List<Annual_FMW_Value_Type> annual_fmw_value_type { get; set; }
        public List<UNIT> unitBig { get; set; }
        public List<Fee> fee { get; set; }
        public List<Tool> tool { get; set; }
        public List<Unit_Payment> unit_payment { get; set; }

        public List<string> company_show_labix { get; set; }
        public Create_Deal_Config create_deal_config { get; set; }

        public List<string> fcl_unit { get { return JSONSetting.checkLength(_fcl_unit, 100); } set { _fcl_unit = value; } }
        public List<string> fcl_offer_unit { get { return JSONSetting.checkLength(_fcl_offer_unit, 100); } set { _fcl_offer_unit = value; } }
        public List<string> fcl_demurrage_unit { get { return JSONSetting.checkLength(_fcl_demurrage_unit, 100); } set { _fcl_demurrage_unit = value; } }
        public List<string> fcl_offer_by { get { return JSONSetting.checkLength(_fcl_offer_by, 100); } set { _fcl_offer_by = value; } }

        public List<string> actual_purchase { get { return JSONSetting.checkLength(_actual_purchase, 100); } set { _actual_purchase = value; } }

        public List<Activation_Status> activation_status { get; set; }
        public List<Unit_Price> unit_price { get; set; }
        public List<Unit_Volume> unit_volume { get; set; }
        public List<Min_Max> min_max { get; set; }

        public List<Knock_Option> knock_option { get; set; }
        public List<Condition_Option> condition_option { get; set; }
        public List<Condition_Operand_Option> condition_operand_option { get; set; }
        public List<Condition_Operator_Option> condition_operator_option { get; set; }
        public List<Formular_Valiable> formular_valiable { get; set; }
        public List<Condition_Operator_Formular> condition_operator_formular { get; set; }

        
    }
}