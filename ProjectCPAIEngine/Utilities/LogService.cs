﻿using log4net;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public class LogService
    {
        private readonly ILog logger_this = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<Error_LogDetailModel> SelectErrorLog(string FromDate, string ToDate, string Module, string Function)
        {
            try
            {
                using (ErrorLogDAL dal = new ErrorLogDAL())
                {

                    return dal.SelectErrorLog(FromDate, ToDate, Module, Function);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// เก็บ Log Error ที่ Database ส่วนที่ DAL ใช้ Log4Net เก็บอย่างเดียวก็ได้ครับ
        /// </summary>
        /// <param name="logger">Instance Log4Net</param>
        /// <param name="ex">Exception Object</param>
        /// <param name="Severity">ระดับความร้ายแรงของ Exception</param>
        /// <param name="UserAction">User ID ที่เข้าใช้ระบบ</param>
        public void InsertErrorLog(ILog logger, Exception ex, ERROR_SEVERITY Severity, string UserAction = "")
        {
            try
            {
                logger.Error(ex);

                Error_LogDetailModel Model = new Error_LogDetailModel();
                //Get a StackTrace object for the exception
                StackTrace st = new StackTrace(ex, true);
                //Get the first stack frame
                StackFrame frame = st.GetFrame(0);

                Model.Module = frame.GetMethod().DeclaringType.FullName;
                Model.Function = frame.GetMethod().Name;
                Model.ERROR_SEVERITY = Convert.ToInt32(Severity);
                Model.EmpID_Action = UserAction;

                SqlException exSqlException = ex as SqlException;
                if (exSqlException != null)
                {
                    Model.ERROR_NUMBER = exSqlException.Number;
                    Model.ERROR_MESSAGE = ex.ToString();
                    Model.ERROR_LINE = exSqlException.LineNumber;
                    Model.ERROR_PROCEDURE = exSqlException.Procedure;
                    Model.ERROR_STATE = exSqlException.State;
                }
                else
                {
                    Model.ERROR_NUMBER = ex.HResult;
                    Model.ERROR_MESSAGE = ex.ToString();
                    Model.ERROR_LINE = LineNumber(ex);
                }

                using (ErrorLogDAL dal = new ErrorLogDAL())
                {
                    System.Threading.Thread t = new System.Threading.Thread(() =>
                    {
                        dal.InsertErrorLog(Model);
                    });
                    t.IsBackground = true;
                    t.Priority = System.Threading.ThreadPriority.Lowest;
                    t.Start();
                }
            }
            catch (Exception ex2)
            {
                logger_this.Error(ex2);
            }
        }

        private int LineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }
    }
}