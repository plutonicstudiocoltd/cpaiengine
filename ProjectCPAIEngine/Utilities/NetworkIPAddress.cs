﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public class NetWorkIPAddress
    {
        public static string getIpAddress()
        {
            string ipAddress = "-";

            try
            {
                NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach(NetworkInterface adapter in networkInterfaces)
                {
                    byte[] hardwareAddress = adapter.GetPhysicalAddress().GetAddressBytes();
                    if (null == hardwareAddress || 0 == hardwareAddress.Length || (0 == hardwareAddress[0] && 0 == hardwareAddress[1] && 0 == hardwareAddress[2])) continue;

                    IPInterfaceProperties adapterProperties = adapter.GetIPProperties();
                    IPAddressCollection dnsServers = adapterProperties.DnsAddresses;
                    if (dnsServers.Count > 0)
                    {
                        foreach (IPAddress dns in dnsServers)
                        {
                            if (!IPAddress.IsLoopback(dns))
                            {
                                string ip = dns.ToString();
                                string hostName = Dns.GetHostName();
                                ipAddress = adapter.Name + "#" + hostName + "#" + ip;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                Console.WriteLine(e.StackTrace);
            }

            return ipAddress;
        }

    }
}