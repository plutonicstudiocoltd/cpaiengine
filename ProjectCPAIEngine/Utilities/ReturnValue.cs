﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectCPAIEngine.Utilities
{
    public class ReturnValue
    {

        private bool _Status;
        public bool Status
        {
            get { return _Status; }
            set { _Status = value; }
        }


        private string _Message;
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        private string _newID;
        public string newID
        {
            get { return _newID; }
            set { _newID = value; }
        }

        private int? _ReturnIdentity = null;
        public int? ReturnIdentity
        {
            get { return _ReturnIdentity; }
            set { _ReturnIdentity = value; }
        }

    }
}