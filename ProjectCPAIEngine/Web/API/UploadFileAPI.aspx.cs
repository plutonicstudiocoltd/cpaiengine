﻿using Newtonsoft.Json;
using ProjectCPAIEngine.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectCPAIEngine.Web.API
{
    public partial class UploadFileAPI : System.Web.UI.Page
    {
        public class Output
        {
            public OutputResult result { get; set; }
            public string file_path { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Output output = new Output();
           
            try
            {
                if (Request.QueryString["Type"] == null ) throw new Exception("Invalid Type.");
                string[] _Param = Request.QueryString["Type"].ToString().Split('|');
                if (_Param.Length >= 2)
                {
                    string Type = _Param[0];
                    string FNID = _Param[1];
                    string rootPath = Request.PhysicalApplicationPath;
                    HttpFileCollection upload_file = Request.Files;
                    HttpPostedFile file = upload_file[0];
                    string file_name = Path.GetFileName(file.FileName);
                    if (file_name != string.Empty)
                    {
                        file_name = string.Format("{0}_{2}_{1}_{3}", DateTime.Now.ToString("yyyyMMddHHmmss"), Type, FNID, Path.GetFileName(file.FileName));
                        if (!Directory.Exists(rootPath + "Web/FileUpload/"))
                            Directory.CreateDirectory(rootPath + "Web/FileUpload/");
                        file.SaveAs(rootPath + "Web/FileUpload/" + file_name);
                    }

                    output.file_path = "Web/FileUpload/" + file_name;
                    output.result = new OutputResult();
                    output.result.Status = "PASS";
                }
                else
                {
                    throw new Exception("Invalid Type.");
                }
            }
            catch (Exception ex)
            {
                output.result = new OutputResult();
                output.result.Status = "ERROR";
                output.result.Description = ex.Message.Replace("\r\n", " ").Replace("\"", "'");
            }

            Response.Write(JsonConvert.SerializeObject(output));
        }
    }
}