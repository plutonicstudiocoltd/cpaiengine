﻿using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectCPAIEngine.Web
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string user = string.Empty;
                string domainUser = string.Empty;
                //windows authen ผ่าน
                if (Const.User != null && !string.IsNullOrEmpty(Const.User.UserName))
                {
                    user = Const.User.UserName;
                }
                else
                {
                    domainUser = HttpContext.Current.User.Identity.Name;
                    user = domainUser.Split('\\')[1].ToString();
                }
                


                string usr_web_menu = USERS_DAL.GetWebMenuByUserName(user.ToUpper());

                //เอา user ไปเช็คสิทธิ์อีกที ว่ามีสิทธิ์ใช้ระบบหรือเปล่า แล้วก็เปิดเมนูตามสิทธิ์
                DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                UserModel _user = new UserModel();
                _user.UserName = user.ToUpper();
                _user.MenuPermission = MakeObjectMenu(_user.UserName);
                _user.UserPassword = "";
                _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                _user.RoleType = perDAL.GetUserRoleType(_user.UserName);
                Const.User = _user;
                Const.User.UserWebMenu = usr_web_menu;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", string.Format("AlertDomainName('{0}')", domainUser), true);
                string path = "~/web/MainBoards.aspx";
                Response.Redirect(path);
            }
            else
            {
                //windows authen ไม่ผ่าน แสดงหน้าจอให้ login AD
                string path = "~/web/Login.aspx";
                Response.Redirect(path);
            }

        }
        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }
    }
}