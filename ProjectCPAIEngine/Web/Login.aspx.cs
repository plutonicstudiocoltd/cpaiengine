﻿using com.pttict.engine.dal.dao;
using com.pttict.engine.dal.Entity;
using com.pttict.engine.dal.Utility;
using com.pttict.engine.utility;
using ProjectCPAIEngine.DAL;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace ProjectCPAIEngine.Web
{
    public partial class Login : System.Web.UI.Page
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        private AppUserDao appUserDao = new AppUserDaoImpl();
        private SpecialConditionDao specialConditionDao = new SpecialConditionDaoImpl();
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            RequestCPAI req = new RequestCPAI();
            string userLogin = txtUsername.Value;
            string password = txtPassword.Value;
            if (!string.IsNullOrEmpty(userLogin) && !string.IsNullOrEmpty(password))
            {
                APP_USER appUser = appUserDao.findByAppId(ConstantPrm.ENGINECONF.EnginAppID);
                if (appUser != null)
                {
                    SPECIAL_CONDITION scn = specialConditionDao.findByKeyTableRef(ConstantUtil.APP_USER_PREFIX, appUser.AUR_ROW_ID, ConstantUtil.PRIVATE_KEY_SCN_KEY);
                    if (scn != null && scn.SCN_ROW_ID != null && scn.SCN_ROW_ID.Trim().Length > 0)
                    {
                        String priKey = scn.SCN_VALUE;
                        password = RSAHelper.EncryptText(password, priKey);
                    }
                }
                req.Function_id = ConstantPrm.FUNCTION.F10000003;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = userLogin });
                req.Req_parameters.P.Add(new P { K = "password", V = password });
                req.Req_parameters.P.Add(new P { K = "user_system", V = ConstantPrm.ENGINECONF.EnginAppID });
                req.Req_parameters.P.Add(new P { K = "user_os", V = "" });
                req.Req_parameters.P.Add(new P { K = "app_version", V = "" });
                req.Req_parameters.P.Add(new P { K = "user_noti_id", V = "" });
                req.State_name = "";

                RequestData reqData = new RequestData();
                ResponseData resData = new ResponseData();
                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);

                ServiceProvider.ProjService service = new ServiceProvider.ProjService();
                resData = service.CallService(reqData);

                if (resData != null && resData.result_desc.ToUpper() == "SUCCESS")
                {
                    if (resData.result_code.Equals("1"))
                    {
                        DAL.UserPermissionDAL perDAL = new DAL.UserPermissionDAL();
                        UserModel _user = new UserModel();
                        _user.UserName = userLogin.ToUpper();
                        _user.MenuPermission = MakeObjectMenu(_user.UserName);
                        _user.UserPassword = password;
                        _user.Name = perDAL.GetUserInfomationName(_user.UserName);
                        _user.RoleType= perDAL.GetUserRoleType(_user.UserName);
                        if(resData.resp_parameters != null)
                        {
                            if (resData.resp_parameters.Count > 0)
                            {
                                _user.UserWebMenu = resData.resp_parameters.Where(p => p.k.Equals("user_web_menu")).FirstOrDefault().v;
                            }
                        }

                        Const.User = _user;                        
                        string path = "~/web/MainBoards.aspx";
                        Response.Redirect(path);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('" + resData.response_message + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('" + resData.response_message + "');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "alert('Please Enter Username and Password');", true);

            }
        }
        private List<MenuPermission> MakeObjectMenu(string UserName)
        {
            List<MenuPermission> lstReturn = new List<MenuPermission>();
            UserPermissionDAL _cls = new UserPermissionDAL();
            lstReturn = _cls.GetUserRoleMenu(UserName);
            return lstReturn;
        }
        public void LoginWindowAuthen()
        {
            string domainUser = User.Identity.Name;
            string user = domainUser.Split('\\')[1].ToString();

        }

        #region XML
        public string postXMLData(string destinationUrl, string requestXml)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            byte[] bytes;
            bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
            request.ContentType = "text/xml; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Method = "POST";
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();
            HttpWebResponse response;
            response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                string responseStr = new StreamReader(responseStream).ReadToEnd();
                return responseStr;
            }
            return null;
        }
        #endregion
    }
}