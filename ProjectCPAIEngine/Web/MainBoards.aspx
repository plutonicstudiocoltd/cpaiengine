﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MainSite.Master" AutoEventWireup="true" CodeBehind="MainBoards.aspx.cs" Inherits="ProjectCPAIEngine.Web.MainBoards" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .table tbody tr {
            cursor: pointer;
        }
            .table tbody tr:hover {
                background-color: #f3f3f3;
            }

        .nav-tabs li{
             cursor: pointer;
             border-bottom:1px solid #F2F2F2;
        }
        .status-box div{
             cursor: pointer;
        }
        .searchIcon {
            cursor: pointer;
        }

    </style>
    
    <div class="main-content-area">
        <asp:HiddenField runat="server" ID="hdfStatus" />
        <asp:HiddenField runat="server" ID="hdfTab" />
        <asp:HiddenField runat="server" ID="hdfType" />
        <asp:UpdatePanel runat="server" ID="UPNGird">
            <ContentTemplate>
                <div class="wrapper-detail">
                    <% =HTMLNavTag.ToString() %>
                    <%-- %><div class="status-task">
                        <div class="status-box"> --%>
                            <% =HTMLStatusBox.ToString() %>
                       <%-- %> </div>
                    </div> --%>
                    <br>
                    <div class="row" id="div_date_range">
                        <div class="pull-right">
                               <%-- <div class="button">
                                    <button class="btn btn-success">Export</button>
                                    <button class="btn btn-primary">Print</button>
                                </div>--%>
                            <table>
                                <tr>
<%--                                    <td style="vertical-align:central; width:22px;">
                                        <img  src="../content/images/bg/bg-date-picker.png" />
                                    </td>--%>
                                    <td style="width: 200px;">
                                        <div>
                                            <input type="text" runat="server" id="txtdate_range" class="form-control input-date-picker" value="" />
                                        </div>
                                    </td>
                                    <td>
                                        &nbsp<span class="glyphicon glyphicon-search searchIcon" onclick="CallByTab('');return false;"></span>
                                    </td>
                                </tr>
                            </table>                    
                        </div>
                    </div>
                   <%-- <div class="sort-box">
                        <select class="form-control">
                            <option>Sort by date</option>
                            <option>Sort by date</option>
                            <option>Sort by date</option>
                            <option>Sort by date</option>
                        </select>
                    </div>--%>
                    <div class="task-box">
                        <% =HTMLDataText.ToString() %>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRefreshClick" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button runat="server" ID="btnRefreshClick" CssClass="btn btn-success" Style="display: none;" OnClick="btnRefreshClick_Click" />
        <script type="text/javascript">
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                LoadAffterPostBack();
            });
            $(document).ready(function () { 
                LoadAffterPostBack();
                if ($('#<%= hdfType.ClientID %>').val() == 'blank') {
                    $("#div_date_range").hide();
                } else {
                    $("#div_date_range").show();
                }
            });
            function LoadAffterPostBack() {
                $('#<%= txtdate_range.ClientID %>').dateRangePicker(
              {
                  startOfWeek: 'monday',
                  format: 'DD/MM/YYYY',
                  autoClose: true
              });
                HideLoadingBox();
            }

            function OpenLinkLocaltion(link) {
                $('#<%= hdfTab.ClientID %>').val("BUNKER");
                window.location.href = link;
            }

            function CallByStatus(status) { 
                $('#<%= hdfStatus.ClientID %>').val(status);
                $('#<%= hdfType.ClientID %>').val("STATUS");
                $('#<%= btnRefreshClick.ClientID %>').click();
            }

            function CallByTab(Tab) {               
                /*if (Tab != "BUNKER" && Tab != "CHARTERING" && Tab != "CRUDE_P" && Tab != "") {
                    OpenBoxPopup("ยังไม่เปิดใช้งาน");
                    return false;
                }*/
                if ($('#<%= hdfTab.ClientID %>').val() != Tab) { 
                    $('#<%= hdfStatus.ClientID %>').val("");
                    if(Tab!="")$('#<%= hdfTab.ClientID %>').val(Tab);
                    $('#<%= hdfType.ClientID %>').val("TAB");
                    $('#<%= btnRefreshClick.ClientID %>').click();
                    LoadingProcess();
                }
            }

        </script>
    </div>
    
</asp:Content>
