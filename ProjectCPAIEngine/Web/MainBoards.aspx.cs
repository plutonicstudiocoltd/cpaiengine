using com.pttict.engine.utility;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Globalization;
using ProjectCPAIEngine.Flow.Utilities;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.IO;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using static ProjectCPAIEngine.Flow.Utilities.CPAIConstantUtil;


namespace ProjectCPAIEngine.Web {

    public partial class MainBoards : System.Web.UI.Page {
        public string HTMLNavTag = "";
        public string HTMLStatusBox = "";

        public StringBuilder HTMLDataText = new StringBuilder();
        public List<BungerTransaction> LstBunkerSS {
            get {
                if (Session["LstBunkerSS"] == null) return new List<BungerTransaction>();
                else { return (List<BungerTransaction>)Session["LstBunkerSS"]; }
            }
            set {
                Session["LstBunkerSS"] = value;
            }
        }

        public List<CharteringTransaction> LstCharteringSS {
            get {
                if (Session["LstCharteringSS"] == null) return new List<CharteringTransaction>();
                else { return (List<CharteringTransaction>)Session["LstCharteringSS"]; }
            }
            set {
                Session["LstCharteringSS"] = value;
            }
        }

        public List<CrudePurchaseEncrypt> LstCrudePurchaseSS {
            get {
                if (Session["LstCrudePurchaseSS"] == null) return new List<CrudePurchaseEncrypt>();
                else { return (List<CrudePurchaseEncrypt>)Session["LstCrudePurchaseSS"]; }
            }
            set {
                Session["LstCrudePurchaseSS"] = value;
            }
        }

        public List<CoolEncrypt> LstCoolSS {
            get {
                if (Session["LstCoolSS"] == null) return new List<CoolEncrypt>();
                else { return (List<CoolEncrypt>)Session["LstCoolSS"]; }
            }
            set {
                Session["LstCoolSS"] = value;
            }
        }

        public List<VCoolEncrypt> LstVCoolSS {
            get {
                if (Session["LstVCoolSS"] == null) return new List<VCoolEncrypt>();
                else { return (List<VCoolEncrypt>)Session["LstVCoolSS"]; }
            }
            set {
                Session["LstVCoolSS"] = value;
            }
        }
        //public List<HedgDealEncrypt> LstHedgDealSS
        //{
        //    get
        //    {
        //        if (Session["LstHedgDealSS"] == null) return new List<HedgDealEncrypt>();
        //        else { return (List<HedgDealEncrypt>)Session["LstHedgDealSS"]; }
        //    }
        //    set
        //    {
        //        Session["LstHedgDealSS"] = value;
        //    }
        //}

        //public List<HedgTicketEncrypt> LstHedgTicketSS
        //{
        //    get
        //    {
        //        if (Session["LstHedgTicketSS"] == null) return new List<HedgTicketEncrypt>();
        //        else { return (List<HedgTicketEncrypt>)Session["LstHedgTicketSS"]; }
        //    }
        //    set
        //    {
        //        Session["LstHedgTicketSS"] = value;
        //    }
        //}

        public List<PAFEncrypt> LstPafTicketSS {
            get {
                if (Session["LstPafTicketSS"] == null) return new List<PAFEncrypt>();
                else { return (List<PAFEncrypt>)Session["LstPafTicketSS"]; }
            }
            set {
                Session["LstPafTicketSS"] = value;
            }
        }

        public List<DAFEncrypt> LstDafTicketSS {
            get {
                if (Session["LstDafTicketSS"] == null) return new List<DAFEncrypt>();
                else { return (List<DAFEncrypt>)Session["LstDafTicketSS"]; }
            }
            set {
                Session["LstDafTicketSS"] = value;
            }
        }

        public List<CDSEncrypt> LstCdsTicketSS {
            get {
                if (Session["LstCdsTicketSS"] == null) return new List<CDSEncrypt>();
                else { return (List<CDSEncrypt>)Session["LstCdsTicketSS"]; }
            }
            set {
                Session["LstCdsTicketSS"] = value;
            }
        }

        ShareFn _FN = new ShareFn();
        public string MCCTypeSS {
            get {
                string MCC = !string.IsNullOrEmpty(Request.QueryString["Type"]) ? Request.QueryString["Type"].ToString() : Session["Charter_Type"] != null ? Session["Charter_Type"].ToString() : ConstantPrm.SYSTEMTYPE.CRUDE.Encrypt();
                return MCC;
            }
            set {
                Session["Charter_Type"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e) {
            if (!Page.IsPostBack) {
                //hdfTab.Value = ConstantPrm.SYSTEM.BUNKER;
                hdfStatus.Value = "";
                txtdate_range.Value = DateTime.Now.AddDays(-90).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + " to " + DateTime.Now.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //if (Const.User.RoleType == "2") hdfStatus.Value = ConstantPrm.ACTION.WAITING;
                //LoadDashBoardData();

                if (Const.User.RoleType == "2") hdfStatus.Value = ConstantPrm.ACTION.WAITING_CERTIFIED;
                LoadDashBoardData2();
            }
        }

        private CounterDetail LoadWaitingEvent() {
            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000018;
            req.App_user = "cpaiios";// ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = "ios@132";// ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBMobile });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ConstantPrm.SYSTEM.CON_TXN });
            req.Extra_xml = "";
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            CounterDetail model = new Model.CounterDetail();
            if (!string.IsNullOrEmpty(_DataJson)) {
                ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
                model = new JavaScriptSerializer().Deserialize<CounterDetail>(_model.data_detail);
            }
            return model;
        }

        private void LoadDashBoardData() {
            try {
                string _from = "";
                string _to = "";
                string[] Delivaeryrange = txtdate_range.Value.SplitWord(" to ");
                if (Delivaeryrange.Length >= 2) {
                    _from = _FN.ConvertDateFormat(Delivaeryrange[0], true);
                    _to = _FN.ConvertDateFormatTo(Delivaeryrange[1], false, true);
                } else {
                    _to = "";
                    _from = "";
                }
                RequestCPAI req = new RequestCPAI();
                req.Function_id = ConstantPrm.FUNCTION.F10000004;
                req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                req.State_name = "";
                req.Req_parameters = new Req_parameters();
                req.Req_parameters.P = new List<P>();
                req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                req.Req_parameters.P.Add(new P { K = "system", V = hdfTab.Value });
                req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
                req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
                req.Req_parameters.P.Add(new P { K = "status", V = "" });
                req.Req_parameters.P.Add(new P { K = "from_date", V = _from });
                req.Req_parameters.P.Add(new P { K = "to_date", V = _to });
                req.Extra_xml = "";

                ResponseData resData = new ResponseData();
                RequestData reqData = new RequestData();
                ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                var xml = ShareFunction.XMLSerialize(req);
                reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                resData = service.CallService(reqData);
                string _DataJson = resData.extra_xml;

                if (hdfTab.Value == ConstantPrm.SYSTEM.BUNKER) {
                    List_Bunkertrx _model = ShareFunction.DeserializeXMLFileToObject<List_Bunkertrx>(_DataJson);
                    if (_model != null && _model.BungerTransaction != null && _model.BungerTransaction != null) {
                        if (_model.BungerTransaction.Count > 0) {
                            foreach (var Item in _model.BungerTransaction) {
                                if (Item.Status == null) Item.Status = "";
                                if (Item.System == null) Item.System = "";
                            }
                        }
                        LstBunkerSS = _model.BungerTransaction;
                        LoadDataToGrid();
                    } else {
                        throw new Exception(resData.response_message);
                    }
                } else if (hdfTab.Value == ConstantPrm.SYSTEM.CHARTERING) {
                    List_Charteringtrx _model = ShareFunction.DeserializeXMLFileToObject<List_Charteringtrx>(_DataJson);
                    if (_model != null && _model.CharteringTransaction != null && _model.CharteringTransaction != null) {
                        if (_model.CharteringTransaction.Count > 0) {
                            foreach (var Item in _model.CharteringTransaction) {
                                if (Item.Status == null) Item.Status = "";
                                if (Item.System == null) Item.System = "";
                            }
                        }
                        LstCharteringSS = _model.CharteringTransaction;
                        LoadDataToGrid();
                    } else {
                        throw new Exception(resData.response_message);
                    }
                } else if (hdfTab.Value == ConstantPrm.SYSTEM.CRUDE_PURCHASE) {
                    List_CrudePurchasetrx _model = ShareFunction.DeserializeXMLFileToObject<List_CrudePurchasetrx>(_DataJson);
                    if (_model != null && _model.CrudePurchaseTransaction != null && _model.CrudePurchaseTransaction != null) {
                        if (_model.CrudePurchaseTransaction.Count > 0) {
                            foreach (var Item in _model.CrudePurchaseTransaction) {
                                if (Item.status == null) Item.status = "";
                                if (Item.system == null) Item.system = "";
                            }
                        }
                        LstCrudePurchaseSS = _model.CrudePurchaseTransaction;
                        LoadDataToGrid();
                    } else {
                        throw new Exception(resData.response_message);
                    }
                }
            } catch (Exception ex) {
                if (ex.Message.ToUpper().Trim() == "Invalid user group".ToUpper().Trim()) {
                    Response.Redirect("Blankpage.aspx");
                } else {
                    HTMLStatusBox = string.Format("<label style=\"color: red\">{0}</label>", ex.Message);
                }
            }
        }

        private void LoadDataToGrid() {
            string System = hdfTab.Value;
            if (System == ConstantPrm.SYSTEM.BUNKER) {
                MakeTabMenu(LstBunkerSS, System, hdfStatus.Value);
            } else if (System == ConstantPrm.SYSTEM.CHARTERING) {
                MakeTabMenu(LstCharteringSS, System, hdfStatus.Value);
            } else if (System == ConstantPrm.SYSTEM.CRUDE_PURCHASE) {
                MakeTabMenu(LstCrudePurchaseSS, System, hdfStatus.Value);
            }
        }

        private void MakeTabMenu(List<BungerTransaction> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            CounterDetail FunctionCount = LoadWaitingEvent();
            #region-----------TabSystem------------
            List<string> _TabList = ConstantPrm.SYSTEM.GetAllSystem();
            HTMLNavTag = "";
            HTMLNavTag += string.Format("<div class=\"tabs-4\">");
            HTMLNavTag += "<ul class=\"nav nav-tabs\">";
            int Count = 0;
            foreach (var _tab in _TabList) {
                Count = 0;
                if (FunctionCount != null && FunctionCount.counter_txn != null) {
                    if (_tab.ToUpper() == ConstantPrm.SYSTEM.BUNKER && FunctionCount.counter_txn.Where(x => x.function == "1").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "1").ToList()[0].counter, out Count);
                    else if (_tab.ToUpper() == ConstantPrm.SYSTEM.CRUDE_PURCHASE && FunctionCount.counter_txn.Where(x => x.function == "23").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "23").ToList()[0].counter, out Count);

                    if (_tab.ToUpper() == ConstantPrm.SYSTEM.CHARTERING) {
                        if (FunctionCount.counter_txn.Where(x => x.function == "6").ToList().Count > 0) {
                            Count += Convert.ToInt32(FunctionCount.counter_txn.Where(x => x.function == "6").FirstOrDefault().counter);
                        }
                        if (FunctionCount.counter_txn.Where(x => x.function == "7").ToList().Count > 0) {
                            Count += Convert.ToInt32(FunctionCount.counter_txn.Where(x => x.function == "7").FirstOrDefault().counter);
                        }
                        if (FunctionCount.counter_txn.Where(x => x.function == "25").ToList().Count > 0) {
                            Count += Convert.ToInt32(FunctionCount.counter_txn.Where(x => x.function == "25").FirstOrDefault().counter);
                        }
                        if (FunctionCount.counter_txn.Where(x => x.function == "26").ToList().Count > 0) {
                            Count += Convert.ToInt32(FunctionCount.counter_txn.Where(x => x.function == "26").FirstOrDefault().counter);
                        }
                    }

                }
                string tabName = _tab.Replace("_", " ");
                HTMLNavTag += string.Format("<li {3}><a onclick=\"CallByTab('{2}');return false;\">{0}{1}</a></li>"
                    , tabName == ConstantPrm.SYSTEM.BUNKER ? "BUNKERING" : tabName == "CRUDE P" ? "CRUDE PURCHASE" : tabName
                    , (Count <= 0 || (ActiveSysTab == _tab)) ? "" : "<sup>" + Count.ToString() + "</sup>"
                    , _tab
                    , (ActiveSysTab == _tab) ? "class=\"active\"" : "");
            }
            HTMLNavTag += "</ul>";
            HTMLNavTag += "</div>";
            #endregion------------------------------

            #region-----------TabStatus------------------
            HTMLStatusBox = "";
            var ObjLstData = lstDataAll.Where(x => x.Date_purchase != null && x.System == ActiveSysTab).ToList();
            List<string> StatusList = ConstantPrm.ACTION.GetAllStatus();

            if (ObjLstData != null) {
                string classActive = "style=\"background: #e21789;\"";//background: #e21789;
                HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('');\" {1}><b>{0}</b><span>All Task</span></div>", ObjLstData.Count.ToString(), (ActiveStatus != "") ? "" : classActive);
                foreach (var _status in StatusList) {
                    classActive = "";
                    if (ActiveStatus == _status) classActive = "style=\"background: #e21789;\"";
                    if (_status == ConstantPrm.ACTION.WAITING) {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.Status.IndexOf(_status) >= 0).ToList().Count.ToString(), _status, classActive);
                    } else {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.Status == _status).ToList().Count.ToString(), _status, classActive);

                    }
                }

            }
            MakeDataActiveStatus(ObjLstData, ActiveStatus);
            #endregion------------------------

        }

        private void MakeDataActiveStatus(List<BungerTransaction> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<BungerTransaction>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.Status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.Status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }

            }
        }

        private void LoadDataHTML(List<BungerTransaction> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.Date_purchase != null
                               select lst.Date_purchase.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.Date_purchase != null && x.Date_purchase.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.Date_purchase).ThenByDescending(x => x.Transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Purchase No</th>");
                // _htmlDataHead.Append("<th>Task ID</th>");
                _htmlDataHead.Append("<th>Vessel Name / Voyage No.</th>");
                _htmlDataHead.Append("<th>Product</th>");
                _htmlDataHead.Append("<th>Volume</th>");
                _htmlDataHead.Append("<th>Supply location</th>");
                _htmlDataHead.Append("<th>Created by</th>");
                _htmlDataHead.Append("</tr></thead>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                foreach (var _item in lstByMonth) {
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (_item.System.ToUpper() == ConstantPrm.SYSTEM.BUNKER) {
                        Link = (_item.Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "BunkerPreDuePurchaseCMMT.aspx" : "BunkerPreDuePurchase.aspx";
                    }
                    //_htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}')\">", Link, _item.Transaction_id.Encrypt(), _item.Req_transaction_id.Encrypt(),_item.Purchase_no.Encrypt(),_item.Reason.Encrypt()));
                    //_htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.Date_purchase, "MMMM dd yyyy"), _item.Status));
                    //_htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.Purchase_no));
                    //_htmlDataData.Append(string.Format("<td width=\"15%\">{0} {1}</td>", _item.Vessel, _item.Trip_no));
                    //_htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", (_item.Products==null)?"":_item.Products.Replace("|", "<p>")));
                    //_htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.Volumes == null) ? "" : _item.Volumes.Replace("|", "<p>")));  
                    //_htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.Supplying_location));
                    //_htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.Create_by == null) ? "" : _item.Create_by));
                    //_htmlDataData.Append("</tr>");
                    var tmpproduct = (string.IsNullOrEmpty(_item.Products)) ? "".Split('|') : _item.Products.Split('|');
                    List<string> product = tmpproduct.Where(x => !string.IsNullOrEmpty(x)).ToList();
                    var Valume = (string.IsNullOrEmpty(_item.Volumes)) ? "".Split('|') : _item.Volumes.Split('|');
                    string Cospan = "";
                    if (product.Count > 1) Cospan = "rowspan=\"" + (product.Count).ToString() + "\"";
                    if (string.IsNullOrEmpty(_item.Products)) {
                        product.Add("-");
                        Valume = new string[1]; Valume[0] = "";
                    }
                    bool Firstrow = true;
                    for (int i = 0; i < product.Count; i++) {
                        var newStatus = _item.Status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.Status;
                        if (product[i] != "" && Firstrow) {
                            _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}')\">", Link, _item.Transaction_id.Encrypt(), _item.Req_transaction_id.Encrypt(), _item.Purchase_no.Encrypt(), _item.Reason.Encrypt()));
                            _htmlDataData.Append(string.Format("<td {2} width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.Date_purchase, "MMMM dd yyyy"), newStatus, Cospan));
                            _htmlDataData.Append(string.Format("<td {1} width=\"8%\">{0}</td>", _item.Purchase_no, Cospan));
                            _htmlDataData.Append(string.Format("<td {2} width=\"15%\">{0} {1}</td>", _item.Vessel, _item.Trip_no, Cospan));
                            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", (_item.Products == null) ? "" : product[i]));
                            _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.Volumes == null) ? "" : (Valume.Length > i) ? Valume[i] : ""));
                            _htmlDataData.Append(string.Format("<td {1} width=\"15%\">{0}</td>", _item.Supplying_location, Cospan));
                            _htmlDataData.Append(string.Format("<td {1} width=\"9%\">{0}</td>", (_item.Create_by == null) ? "" : _item.Create_by, Cospan));
                            _htmlDataData.Append("</tr>");
                            Firstrow = false;
                        } else {
                            _htmlDataData.Append(string.Format("<tr>"));
                            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", (_item.Products == null) ? "" : product[i]));
                            _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.Volumes == null) ? "" : (Valume.Length > i) ? Valume[i] : ""));
                            _htmlDataData.Append("</tr>");
                        }
                    }
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        private List<string> ReOrderbyDate(List<string> lstDate) {
            List<string> lstDateOrder = new List<string>();
            foreach (string _item in lstDate) {
                string[] splitD = _item.Split('/');
                if (splitD.Length >= 3) {
                    lstDateOrder.Add(splitD[2] + splitD[1]);
                }
            }
            lstDateOrder = lstDateOrder.OrderByDescending(x => x).ToList();
            for (int i = 0; i < lstDateOrder.Count; i++) {
                lstDateOrder[i] = string.Format("/{1}/{0}", lstDateOrder[i].Substring(0, 4), lstDateOrder[i].Substring(4, 2));
            }
            return lstDateOrder;
        }

        private List<string> ReOrderbyVCoolDate(List<string> lstDate) {
            List<string> lstDateOrder = new List<string>();
            foreach (string _item in lstDate) {
                string[] splitD = _item.Split('-');
                if (splitD.Length >= 3) {
                    lstDateOrder.Add(splitD[1] + "-" + splitD[2]);
                }
            }

            lstDateOrder.Reverse();
            lstDateOrder = lstDateOrder.OrderBy(x => x).ToList();
            for (int i = 0; i < lstDateOrder.Count; i++) {
                lstDateOrder[i] = string.Format("/{1}/{0}", lstDateOrder[i].Substring(0, 4), lstDateOrder[i].Substring(4, 2));
            }
            return lstDateOrder;
        }

        //private List<string> ReOrderCoolbyDate(List<string> lstDate)
        //{
        //    List<string> lstDateOrder = new List<string>();
        //    foreach (string _item in lstDate)
        //    {
        //        _item = 
        //        string[] splitD = _item.Split('/');
        //        if (splitD.Length >= 3)
        //        {
        //            lstDateOrder.Add(splitD[2] + splitD[1]);
        //        }
        //    }
        //    lstDateOrder = lstDateOrder.OrderByDescending(x => x).ToList();
        //    for (int i = 0; i < lstDateOrder.Count; i++)
        //    {
        //        lstDateOrder[i] = string.Format("/{1}/{0}", lstDateOrder[i].Substring(0, 4), lstDateOrder[i].Substring(4, 2));
        //    }
        //    return lstDateOrder;
        //}

        private void MakeTabMenu(List<CharteringTransaction> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            #region-----------TabSystem------------
            List<string> _TabList = ConstantPrm.SYSTEM.GetAllSystem();
            CounterDetail FunctionCount = LoadWaitingEvent();
            HTMLNavTag = "";
            HTMLNavTag += string.Format("<div class=\"tabs-4\">");
            HTMLNavTag += "<ul class=\"nav nav-tabs\">";
            int Count = 0;
            foreach (var _tab in _TabList) {
                Count = 0;
                if (FunctionCount != null && FunctionCount.counter_txn != null) {
                    if (_tab.ToUpper() == ConstantPrm.SYSTEM.BUNKER && FunctionCount.counter_txn.Where(x => x.function == "1").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "1").ToList()[0].counter, out Count);
                    else if (_tab.ToUpper() == ConstantPrm.SYSTEM.CHARTERING && FunctionCount.counter_txn.Where(x => x.function == "6").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "6").ToList()[0].counter, out Count);
                    else if (_tab.ToUpper() == ConstantPrm.SYSTEM.CRUDE_PURCHASE && FunctionCount.counter_txn.Where(x => x.function == "23").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "23").ToList()[0].counter, out Count);

                }
                string tabName = _tab.Replace("_", " ");
                HTMLNavTag += string.Format("<li {3}><a onclick=\"CallByTab('{2}');return false;\">{0}{1}</a></li>"
                   , tabName == ConstantPrm.SYSTEM.BUNKER ? "BUNKERING" : tabName == "CRUDE P" ? "CRUDE PURCHASE" : tabName
                   , (Count <= 0 || (ActiveSysTab == _tab)) ? "" : "<sup>" + Count.ToString() + "</sup>"
                   , _tab
                   , (ActiveSysTab == _tab) ? "class=\"active\"" : "");
            }
            HTMLNavTag += "</ul>";
            HTMLNavTag += "</div>";
            #endregion------------------------------

            #region-----------TabStatus------------------
            HTMLStatusBox = "";
            var ObjLstData = lstDataAll.Where(x => x.Date_purchase != null && x.System == ActiveSysTab).ToList();
            List<string> StatusList = ConstantPrm.ACTION.GetAllStatus();

            if (ObjLstData != null) {
                string classActive = "style=\"background: #e21789;\"";//background: #e21789;
                HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('');\" {1}><b>{0}</b><span>All Task</span></div>", ObjLstData.Count.ToString(), (ActiveStatus != "") ? "" : classActive);
                foreach (var _status in StatusList) {
                    classActive = "";
                    if (ActiveStatus == _status) classActive = "style=\"background: #e21789;\"";
                    if (_status == ConstantPrm.ACTION.WAITING) {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.Status.IndexOf(_status) >= 0).ToList().Count.ToString(), _status, classActive);
                    } else {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.Status == _status).ToList().Count.ToString(), _status, classActive);

                    }
                }

            }
            MakeDataActiveStatus(ObjLstData, ActiveStatus);
            #endregion------------------------

        }

        private void LoadDataHTML(List<CharteringTransaction> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.Date_purchase != null
                               select lst.Date_purchase.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.Date_purchase != null && x.Date_purchase.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.Date_purchase).ThenByDescending(x => x.Transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Purchase No</th>");
                // _htmlDataHead.Append("<th>Task ID</th>");
                _htmlDataHead.Append("<th>Vessel Name</th>");
                _htmlDataHead.Append("<th>Laycan</th>");
                _htmlDataHead.Append("<th>Broker</th>");
                _htmlDataHead.Append("<th>Created by</th>");
                _htmlDataHead.Append("</tr></thead>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                //using (DAL.Entity.EntityCPAIEngine context = new DAL.Entity.EntityCPAIEngine())
                //{
                //    var query = from v in context.MT_VEHICLE
                //                join vc in context.MT_VEHICLE_CONTROL on v.VEH_ID equals vc.MCC_FK_VEHICLE
                //                select v;
                //    query.ToList();
                //}                    

                foreach (var _item in lstByMonth) {
                    var newStatus = _item.Status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.Status;
                    var vessel = DropdownServiceModel.getVehicle((MCCTypeSS.Decrypt().ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) ? "CHIVESMT" : "CHIVESCS", true);
                    var checkVewssel = vessel.SingleOrDefault(a => a.Value == _item.Vessel);
                    if (checkVewssel != null) {
                        _item.Vessel = checkVewssel.Text;
                    }
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (_item.System.ToUpper() == ConstantPrm.SYSTEM.CHARTERING) {
                        if (_item.Function_id == CPAIConstantUtil.CHARTER_IN_FUNCTION_CODE) {
                            //CharterIn
                            Link = "../CPAIMVC/CharterIn";
                        } else if (_item.Function_id == CPAIConstantUtil.CHARTER_IN_CMMT_FUNCTION_CODE) {
                            Link = "../CPAIMVC/CharterIn/CharterInCMMT";
                        } else {
                            //CharterOut
                            Link = "../CPAIMVC/CharterOut";
                            if (_item.Type.ToUpper() == ConstantPrm.SYSTEMTYPE.VESSEL) Link = "../CPAIMVC/CharterOut/CharterOutCMMT";
                        }
                    }
                    Type = _item.Type.ToUpper();
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.Transaction_id.Encrypt(), _item.Req_transaction_id.Encrypt(), _item.Purchase_no.Encrypt(), _item.Reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.Date_purchase, "MMMM dd yyyy"), newStatus));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.Purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.Vessel));
                    _htmlDataData.Append(string.Format("<td width=\"10%\">{0} to {1}</td>", (_item.Laycan_from == null) ? "" : _item.Laycan_from.Replace("|", "<p>"), (_item.Laycan_to == null) ? "" : _item.Laycan_to.Replace("|", "<p>")));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.Cust_name == null) ? "" : _item.Cust_name.Replace("|", "<p>")));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.Create_by == null) ? "" : _item.Create_by));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        private void MakeDataActiveStatus(List<CharteringTransaction> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CharteringTransaction>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.Status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.Status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }

            }
        }

        private void MakeTabMenu(List<CrudePurchaseEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            #region-----------TabSystem------------
            List<string> _TabList = ConstantPrm.SYSTEM.GetAllSystem();
            CounterDetail FunctionCount = LoadWaitingEvent();
            HTMLNavTag = "";
            HTMLNavTag += string.Format("<div class=\"tabs-4\">");
            HTMLNavTag += "<ul class=\"nav nav-tabs\">";
            int Count = 0;
            foreach (var _tab in _TabList) {
                Count = 0;
                if (FunctionCount != null && FunctionCount.counter_txn != null) {
                    if (_tab.ToUpper() == ConstantPrm.SYSTEM.BUNKER && FunctionCount.counter_txn.Where(x => x.function == "1").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "1").ToList()[0].counter, out Count);
                    else if (_tab.ToUpper() == ConstantPrm.SYSTEM.CHARTERING && FunctionCount.counter_txn.Where(x => x.function == "6").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "6").ToList()[0].counter, out Count);
                    else if (_tab.ToUpper() == ConstantPrm.SYSTEM.CRUDE_PURCHASE && FunctionCount.counter_txn.Where(x => x.function == "23").ToList().Count > 0) int.TryParse(FunctionCount.counter_txn.Where(x => x.function == "23").ToList()[0].counter, out Count);

                }
                string tabName = _tab.Replace("_", " ");
                HTMLNavTag += string.Format("<li {3}><a onclick=\"CallByTab('{2}');return false;\">{0}{1}</a></li>"
                   , tabName == ConstantPrm.SYSTEM.BUNKER ? "BUNKERING" : tabName == "CRUDE P" ? "CRUDE PURCHASE" : tabName
                   , (Count <= 0 || (ActiveSysTab == _tab)) ? "" : "<sup>" + Count.ToString() + "</sup>"
                   , _tab
                   , (ActiveSysTab == _tab) ? "class=\"active\"" : "");
            }
            HTMLNavTag += "</ul>";
            HTMLNavTag += "</div>";
            #endregion------------------------------

            #region-----------TabStatus------------------
            HTMLStatusBox = "";
            var ObjLstData = lstDataAll.Where(x => x.date_purchase != null && x.system == ActiveSysTab).ToList();
            List<string> StatusList = ConstantPrm.ACTION.GetAllStatus();

            if (ObjLstData != null) {
                string classActive = "style=\"background: #e21789;\"";//background: #e21789;
                HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('');\" {1}><b>{0}</b><span>All Task</span></div>", ObjLstData.Count.ToString(), (ActiveStatus != "") ? "" : classActive);
                foreach (var _status in StatusList) {
                    classActive = "";
                    if (ActiveStatus == _status) classActive = "style=\"background: #e21789;\"";
                    if (_status == ConstantPrm.ACTION.WAITING) {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.status.IndexOf(_status) >= 0).ToList().Count.ToString(), _status, classActive);
                    } else {
                        HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{1}');\" {2}><b>{0}</b><span>{1}</span></div>", ObjLstData.Where(x => x.status == _status).ToList().Count.ToString(), _status, classActive);

                    }
                }

            }
            MakeDataActiveStatus(ObjLstData, ActiveStatus);
            #endregion------------------------

        }

        private void LoadDataHTML(List<CrudePurchaseEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.date_purchase != null
                               select lst.date_purchase.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.date_purchase != null && x.date_purchase.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.date_purchase).ThenByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Purchase No</th>");
                _htmlDataHead.Append("<th>Feed stock</th>");
                _htmlDataHead.Append("<th>Name</th>");
                _htmlDataHead.Append("<th>Supplier</th>");
                _htmlDataHead.Append("<th>Volume</th>");
                _htmlDataHead.Append("<th>Margin</th>");
                _htmlDataHead.Append("<th>Created by</th>");
                _htmlDataHead.Append("</tr></thead>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {
                    var newStatus = _item.status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.status;
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.CRUDE_PURCHASE) {
                        Link = "../CPAIMVC/CrudePurchase";
                    }
                    Type = _item.type.ToUpper();
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy"), newStatus));
                    _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.feed_stock));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.product_name));
                    _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.supplier_name));
                    _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.volumes));
                    _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.margin));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.create_by == null) ? "" : _item.create_by));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        private void LoadDataHTML(List<CoolEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";
            for (int i = 0; i < lstDataByStatus.Count; i++) {
                if (lstDataByStatus[i].date_purchase.Contains("-")) {
                    string datetimeString = string.Empty;
                    if (lstDataByStatus[i].date_purchase.Length > 17) {
                        datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].date_purchase, "dd-MMM-yyyy HH:mm:ss", null, DateTimeStyles.None));
                    } else {
                        datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].date_purchase, "dd-MMM-yyyy HH:mm", null, DateTimeStyles.None));
                    }
                    lstDataByStatus[i].date_purchase = datetimeString;
                }
                if (!string.IsNullOrEmpty(lstDataByStatus[i].status)) {
                    lstDataByStatus[i].status_description = CoolServiceModel.getWorkflowStatusDescription(lstDataByStatus[i].status);
                }
            }

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.date_purchase != null
                               select lst.date_purchase.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.date_purchase != null && x.date_purchase.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.date_purchase).ThenByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Document No</th>");
                _htmlDataHead.Append("<th>Crude Name</th>");
                _htmlDataHead.Append("<th>Country</th>");
                _htmlDataHead.Append("<th>Assay Reference No.</th>");
                _htmlDataHead.Append("<th>Requester</th>");
                _htmlDataHead.Append("</tr></thead>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.COOL) {
                        if (_item.status == "DRAFT" || _item.status == "WAITING APPROVE DRAFT CAM") {
                            Link = "../CPAIMVC/Cool";
                        } else if (_item.status == "WAITING EXPERT APPROVAL") {
                            Link = "../CPAIMVC/Cool/CoolExpert";
                        } else if (_item.status == "WAITING CREATE FINAL CAM" || _item.status == "WAITING APPROVE FINAL CAM" || _item.status == "APPROVED") {
                            Link = "../CPAIMVC/Cool/CoolFinalCAM";
                        } else {
                            Link = "../CPAIMVC/Cool";
                        }
                    }
                    Type = _item.type.ToUpper();
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy"), _item.status_description));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.products));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.origin == null) ? "" : _item.origin));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.assay_ref == null) ? "" : _item.assay_ref));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.requester_name == null) ? "" : _item.requester_name));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        private void LoadDataHTML(List<VCoolEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";
            for (int i = 0; i < lstDataByStatus.Count; i++) {
                if (lstDataByStatus[i].date_purchase.Contains("-") && lstDataByStatus[i].date_purchase.Contains(":") && lstDataByStatus[i].date_purchase.Length > 20) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].date_purchase, "dd-MMM-yyyy HH:mm:ss", null, DateTimeStyles.None));
                    lstDataByStatus[i].date_purchase = datetimeString;
                } else if (lstDataByStatus[i].date_purchase.Contains("-") && lstDataByStatus[i].date_purchase.Contains(":")) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].date_purchase, "dd-MMM-yyyy HH:mm", null, DateTimeStyles.None));
                    lstDataByStatus[i].date_purchase = datetimeString;
                } else if (lstDataByStatus[i].date_purchase.Contains("-")) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].date_purchase, "dd-MMM-yyyy", null, DateTimeStyles.None));
                    lstDataByStatus[i].date_purchase = datetimeString;
                }
                if (!string.IsNullOrEmpty(lstDataByStatus[i].status)) {
                    lstDataByStatus[i].status_description = VCoolServiceModel.getWorkflowStatusDescription(lstDataByStatus[i].status);
                }
            }

            var userGroups = VCoolServiceModel.getUserGroup(Const.User.UserName);
            var hideConfident = false;
            if (userGroups.Contains("SCSC") || userGroups.Contains("SCSC_SH") || userGroups.Contains("TNPB")) {
                hideConfident = true;
            }


            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.date_purchase != null
                               select lst.date_purchase.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.date_purchase != null && x.date_purchase.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate);

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Purchase No.</th>");
                _htmlDataHead.Append("<th>Crude Name</th>");
                _htmlDataHead.Append("<th>Country/Origin</th>");
                _htmlDataHead.Append("<th>Purchase Date</th>");
                _htmlDataHead.Append("<th>Discharging Date</th>");
                if (!hideConfident) {
                    _htmlDataHead.Append("<th>Incoterm</th>");
                    _htmlDataHead.Append("<th>Formula Price</th>");
                }

                _htmlDataHead.Append("</tr></thead>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {

                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.VCOOL) {
                        if (_item.status == "DRAFT") {
                            Link = "../CPAIMVC/VCool";
                        } else if (_item.status == "WAITING RUN LP" || _item.status == "WAITING APPROVE RUN LP" || _item.status == "REVISE LP RUN" || _item.status == "REVISE LP VERIFICATION") {
                            Link = "../CPAIMVC/VCool/LPRun";
                        } else if (_item.status == "WAITING PROPOSE ETA DATE" || _item.status == "WAITING EDIT ETA DATE" || _item.status == "WAITING APPROVE PRICE" || _item.status == "WAITING CONFIRM PRICE" || _item.status == "WAITING APPROVE SUMMARY" || _item.status == "REVISE PROPOSE ETA" || _item.status == "REVISE PURCHASE COMMENT") {
                            Link = "../CPAIMVC/VCool/PCAFinalPrice";
                        } else if (_item.status == "WAITING CHECK TANK" || _item.status == "WAITING APPROVE TANK" || _item.status == "REVISE TANK VERIFICATION" || _item.status == "REVISE TANK AVAILABILITY") {
                            Link = "../CPAIMVC/VCool/SCSCTank";
                        } else if (_item.status == "WAITING CHECK IMPACT") {
                            Link = "../CPAIMVC/VCool/TNCheckTech";
                        } else if (_item.status == "WAITING CONFIRM PRICE" || _item.status == "WAITING APPROVE PRICE") {
                            Link = "../CPAIMVC/VCool/PCAFinalPrice";
                        } else if (_item.status == "WAITING CMVP SCVP APPROVE" || _item.status == "WAITING TNVP APPROVE" || _item.status == "CONSENSUS APPROVAL" || _item.status == "CHECK IMPACT APPROVED" || _item.status == "CHECK TANK APPROVED" || _item.status == "WAITING SCVP APPROVE" || _item.status == "WAITING CMVP APPROVE") {
                            Link = "../CPAIMVC/VCool/Consensus";
                        } else {
                            Link = "../CPAIMVC/VCool";
                        }
                    }

                    Type = _item.type.ToUpper();
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy"), _item.status_description));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.product_name));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.origin == null) ? "" : _item.origin));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.date_purchase == null) ? "" : _item.date_purchase));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.discharging_period == null) ? "" : _item.discharging_period));
                    if (!hideConfident) {
                        _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.incoterm == null) ? "" : _item.incoterm));
                        _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.formula_p == null) ? "" : _item.formula_p));
                    }
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        //private void LoadDataHTML(List<HedgDealEncrypt> lstDataByStatus)
        //{
        //    HTMLDataText.Clear();
        //    StringBuilder _htmlDataHead = new StringBuilder();
        //    StringBuilder _htmlDataData = new StringBuilder();
        //    string HeadingDiv = "";

        //    var ObjDistinct = (from lst in lstDataByStatus
        //                       where lst.deal_date != null
        //                       select lst.deal_date.Substring(2, 8)).Distinct().ToList().OrderByDescending(x => x).ToList();
        //    var _reorderDate = ReOrderbyDate(ObjDistinct);
        //    foreach (var _objDate in _reorderDate)
        //    {
        //        var lstByMonth = lstDataByStatus.Where(x => x.deal_date != null && x.deal_date.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.deal_date).ThenByDescending(x => x.transaction_id).ToList();
        //        _htmlDataHead.Clear();
        //        _htmlDataData.Clear();
        //        //-----------------Div-----------------
        //        HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
        //        HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

        //        //-----------------Header-----------------
        //        _htmlDataHead.Append("<table class=\"table\">");
        //        _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
        //        _htmlDataHead.Append("<th>Deal No</th>");
        //        _htmlDataHead.Append("<th>Company</th>");
        //        _htmlDataHead.Append("<th>Counter Party</th>");
        //        _htmlDataHead.Append("<th>Underlying</th>");
        //        _htmlDataHead.Append("<th>Created By</th>");
        //        _htmlDataHead.Append("</tr></thead>");

        //        //-----------------Data-----------------
        //        _htmlDataData.Append("<tbody>");
        //        string Link = "";
        //        string Type = "";
        //        string screen_type = "DEAL";
        //        foreach (var _item in lstByMonth)
        //        {
        //            //DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //            if (_item.system.ToUpper() == ConstantPrm.SYSTEM.HEDG_DEAL)
        //            {
        //                Link = "../CPAIMVC/HedgingDeal/Edit";
        //            }
        //            Type = _item.type.ToUpper();
        //            _item.deal_date = _item.deal_date.Substring(0, 10);
        //            _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Type={4}&System={5}&Screen_Type={6}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.deal_no.Encrypt(), Type.Encrypt(), _item.system.Encrypt(), screen_type.Encrypt()));
        //            _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.deal_date, "MMMM dd yyyy"), _item.status));
        //            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.deal_no));
        //            _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.seller));
        //            _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.buyer));
        //            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.underlying_name));
        //            _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.create_by == null) ? "" : _item.create_by));
        //            _htmlDataData.Append("</tr>");
        //        }
        //        _htmlDataData.Append("</tbody>");

        //        //-----------------Finish-----------------
        //        HTMLDataText.Append(HeadingDiv);
        //        HTMLDataText.Append(_htmlDataHead);
        //        HTMLDataText.Append(_htmlDataData);
        //        HTMLDataText.Append("</table>");
        //        HTMLDataText.Append("</div>");
        //    }
        //}

        //private void LoadDataHTML(List<HedgTicketEncrypt> lstDataByStatus)
        //{
        //    HTMLDataText.Clear();
        //    StringBuilder _htmlDataHead = new StringBuilder();
        //    StringBuilder _htmlDataData = new StringBuilder();
        //    string HeadingDiv = "";

        //    var ObjDistinct = (from lst in lstDataByStatus
        //                       where lst.ticket_date != null
        //                       select lst.ticket_date.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
        //    var _reorderDate = ReOrderbyDate(ObjDistinct);
        //    foreach (var _objDate in _reorderDate)
        //    {
        //        var lstByMonth = lstDataByStatus.Where(x => x.ticket_date != null && x.ticket_date.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.ticket_date).ThenByDescending(x => x.transaction_id).ToList();
        //        _htmlDataHead.Clear();
        //        _htmlDataData.Clear();
        //        //-----------------Div-----------------
        //        HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
        //        HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

        //        //-----------------Header-----------------
        //        _htmlDataHead.Append("<table class=\"table\">");
        //        _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
        //        _htmlDataHead.Append("<th>Deal No</th>");
        //        _htmlDataHead.Append("<th>Company</th>");
        //        _htmlDataHead.Append("<th>Counter Party</th>");
        //        _htmlDataHead.Append("<th>Underlying</th>");
        //        _htmlDataHead.Append("<th>Created By</th>");
        //        _htmlDataHead.Append("</tr></thead>");

        //        //-----------------Data-----------------
        //        _htmlDataData.Append("<tbody>");
        //        string Link = "";
        //        string Type = "";
        //        foreach (var _item in lstByMonth)
        //        {
        //            //DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //            if (_item.system.ToUpper() == ConstantPrm.SYSTEM.HEDG_TCKT)
        //            {
        //                Link = "../CPAIMVC/HedgingTicket/Edit";
        //            }
        //            Type = _item.type.ToUpper();
        //            _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Type={4}&System={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.ticket_no.Encrypt(), Type.Encrypt(), _item.system.Encrypt()));
        //            _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.ticket_date, "MMMM dd yyyy"), _item.status));
        //            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.ticket_no));
        //            _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.seller));
        //            _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.buyer));
        //            _htmlDataData.Append(string.Format("<td width=\"10%\">{0}</td>", _item.underlying_name));
        //            _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.create_by == null) ? "" : _item.create_by));
        //            _htmlDataData.Append("</tr>");
        //        }
        //        _htmlDataData.Append("</tbody>");

        //        //-----------------Finish-----------------
        //        HTMLDataText.Append(HeadingDiv);
        //        HTMLDataText.Append(_htmlDataHead);
        //        HTMLDataText.Append(_htmlDataData);
        //        HTMLDataText.Append("</table>");
        //        HTMLDataText.Append("</div>");
        //    }
        //}
        private void LoadDataHTML(List<PAFEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";
            for (int i = 0; i < lstDataByStatus.Count; i++) {
                if (lstDataByStatus[i].created_date.Contains("-")) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].created_date, "dd-MMM-yyyy HH:mm", null, DateTimeStyles.None));
                    lstDataByStatus[i].created_date = datetimeString;
                }
            }

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.created_date != null && lst.created_date != ""
                               select lst.created_date.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.created_date != null && x.created_date.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.created_date).ThenByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Document No</th>");
                _htmlDataHead.Append("<th>Form Type</th>");
                _htmlDataHead.Append("<th>For</th>");
                _htmlDataHead.Append("<th>Awarded Customer/Supplier</th>");
                _htmlDataHead.Append("<th>Created By</th>");
                _htmlDataHead.Append("<th>Updated By</th>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {
                    var newStatus = _item.status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.status;
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.PAF) {
                        Link = "../CPAIMVC/ApprovalForm/#/";

                        //if (_item.status == "DRAFT" || _item.status == "WAITING APPROVE DRAFT CAM")
                        //{
                        //    Link = "../CPAIMVC/Cool";
                        //}
                        //else if (_item.status == "WAITING EXPERT APPROVAL")
                        //{
                        //    Link = "../CPAIMVC/Cool/CoolExpert";
                        //}
                        //else if (_item.status == "WAITING CREATE FINAL CAM" || _item.status == "WAITING APPROVE FINAL CAM" || _item.status == "APPROVED")
                        //{
                        //    Link = "../CPAIMVC/Cool/CoolFinalCAM";
                        //}
                        //else
                        //{
                        //    Link = "../CPAIMVC/Cool";
                        //}
                    }
                    Type = _item.type.ToUpper();
                    //switch (_item.template_name)
                    //{
                    //    case "Product Approval Form (Domestic)":
                    //        Link += "cmps/dom/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                    //        break;
                    //    case "Product Approval Form (International)":
                    //        Link += "cmps/inter/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                    //        break;
                    //    case "Product Approval Form (Import)":
                    //        Link += "cmps/import/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                    //        break;
                    //}

                    switch (_item.template_id) {
                        case "CMPS_DOM_SALE":
                            Link += "cmps/dom/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMPS_INTER_SALE":
                            Link += "cmps/inter/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMPS_IMPORT":
                            Link += "cmps/import/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMPS_OTHER":
                            Link += "cmps/other/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMLA_FORM_1":
                            Link += "cmla/form-1/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMLA_FORM_2":
                            Link += "cmla/form-2/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMLA_IMPORT":
                            Link += "cmla/import/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                        case "CMLA_BITUMEN":
                            Link += "cmla/bitumen/" + _item.req_transaction_id + "/" + _item.transaction_id + "/edit";
                            break;
                    }
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}')\">", Link));
                    //_htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.created_date, "MMMM dd yyyy"), newStatus));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}</td>", _item.template_name));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", (_item.for_company == null) ? "" : _item.for_company));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.awarded_summary == null) ? "" : _item.awarded_summary));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.created_by == null) ? "" : _item.created_by));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", (_item.updated_by == null) ? "" : _item.updated_by));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        private void MakeDataActiveStatus(List<CrudePurchaseEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CrudePurchaseEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }

            }
        }

        protected void btnRefreshClick_Click(object sender, EventArgs e) {

            if (hdfType.Value == "TAB") {
                //LoadDashBoardData();
                if (Const.User.RoleType == "2") hdfStatus.Value = ConstantPrm.ACTION.WAITING_CERTIFIED;
                LoadDashBoardData2();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "HideBox", "HideLoadingBox();", true);
            } else {
                //LoadDataToGrid();
                LoadDataToGrid2();
            }
        }

        //new -----------------------------------03/04/2017----------------------------------

        public static string gtTab = "";
        public static Areas.CPAIMVC.ViewModels.MainBoardsModel gMainBoards;
        const int gnStatusLength = 7;

        private CounterDetail CounterTransetion(string ptSystem) {
            string _from = "";
            string _to = "";
            string[] Delivaeryrange = txtdate_range.Value.SplitWord(" to ");
            if (Delivaeryrange.Length >= 2) {
                _from = _FN.ConvertDateFormat(Delivaeryrange[0], true);
                _to = _FN.ConvertDateFormatTo(Delivaeryrange[1], false, true);
            }

            RequestCPAI req = new RequestCPAI();
            req.Function_id = ConstantPrm.FUNCTION.F10000018;
            req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
            req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
            req.State_name = "";
            req.Req_parameters = new Req_parameters();
            req.Req_parameters.P = new List<P>();
            req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
            req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
            req.Req_parameters.P.Add(new P { K = "system", V = ptSystem });
            req.Req_parameters.P.Add(new P { K = "from_date", V = _from });
            req.Req_parameters.P.Add(new P { K = "to_date", V = _to });
            req.Extra_xml = "";
            ResponseData resData = new ResponseData();
            RequestData reqData = new RequestData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();

            var xml = ShareFunction.XMLSerialize(req);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            CounterDetail model = new Model.CounterDetail();
            if (!string.IsNullOrEmpty(_DataJson)) {
                ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");
                model = new JavaScriptSerializer().Deserialize<CounterDetail>(_model.data_detail);
            }
            return model;
        }

        private void LoadDashBoardData2() {
            try {
                //Const.User.UserWebMenu = null;
                if (Const.User.UserWebMenu != null && Const.User.UserWebMenu != "blank") {

                    //Areas.CPAIMVC.ViewModels.MainBoardsModel obj = JsonConvert.DeserializeObject<Areas.CPAIMVC.ViewModels.MainBoardsModel>(File.OpenText(@"D:\JSON\menu.json").ReadToEnd());
                    Areas.CPAIMVC.ViewModels.MainBoardsModel obj = JsonConvert.DeserializeObject<Areas.CPAIMVC.ViewModels.MainBoardsModel>(Const.User.UserWebMenu);
                    //txtdate_range.Value = null;

                    if (hdfTab.Value == "") hdfTab.Value = obj.menu.ElementAt(0).name;
                    gMainBoards = obj;
                    MakeTabMenuHTML();

                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.COOL ? "" : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.VCOOL ? "" : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.HEDG_DEAL ? ConstantPrm.ACTION.DRAFT : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.HEDG_TCKT ? ConstantPrm.ACTION.WAITING_VERIFY : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.PAF ? "" : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.DAF ? "" : hdfStatus.Value;
                    hdfStatus.Value = hdfTab.Value == ConstantPrm.SYSTEM.CDS ? "" : hdfStatus.Value;

                    string _from = "";
                    string _to = "";
                    string[] Delivaeryrange = txtdate_range.Value.SplitWord(" to ");
                    if (Delivaeryrange.Length >= 2) {
                        _from = _FN.ConvertDateFormat(Delivaeryrange[0], true);
                        _to = _FN.ConvertDateFormatTo(Delivaeryrange[1], false, true);
                    } else {
                        _to = "";
                        _from = "";
                    }

                    string func_id = "";
                    if (hdfTab.Value == ConstantPrm.SYSTEM.COOL) {
                        func_id = ConstantPrm.FUNCTION.F10000039;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.VCOOL) {
                        func_id = ConstantPrm.FUNCTION.F10000061;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_DEAL) {
                        //func_id = ConstantPrm.FUNCTION.F10000049;
                        func_id = ConstantPrm.FUNCTION.F10000048;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_TCKT) {
                        //func_id = ConstantPrm.FUNCTION.F10000055;
                        func_id = ConstantPrm.FUNCTION.F10000050;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.PAF) {
                        func_id = ConstantPrm.FUNCTION.F10000045;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.DAF) {
                        func_id = ConstantPrm.FUNCTION.F10000081;
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.CDS) {
                        func_id = ConstantPrm.FUNCTION.F10000088;
                    } else {
                        func_id = ConstantPrm.FUNCTION.F10000004;
                    }
                    string tSystem = "";
                    tSystem = hdfTab.Value;

                    RequestCPAI req = new RequestCPAI();
                    req.Function_id = func_id;
                    req.App_user = ConstantPrm.ENGINECONF.EnginAppID;
                    req.App_password = ConstantPrm.ENGINECONF.EnginAppPassword;
                    req.Req_transaction_id = ConstantPrm.EnginGetEngineID();
                    req.State_name = "";
                    req.Req_parameters = new Req_parameters();
                    req.Req_parameters.P = new List<P>();
                    req.Req_parameters.P.Add(new P { K = "channel", V = ConstantPrm.ENGINECONF.WEBChannel });
                    req.Req_parameters.P.Add(new P { K = "user", V = Const.User.UserName });
                    req.Req_parameters.P.Add(new P { K = "system", V = tSystem });
                    req.Req_parameters.P.Add(new P { K = "page_number", V = "1" });
                    req.Req_parameters.P.Add(new P { K = "rows_per_page", V = "20" });
                    req.Req_parameters.P.Add(new P { K = "status", V = "" });
                    req.Req_parameters.P.Add(new P { K = "from_date", V = _from });
                    req.Req_parameters.P.Add(new P { K = "to_date", V = _to });

                    #region Get FunctionCool Old
                    //if (hdfTab.Value == ConstantPrm.SYSTEM.COOL)
                    //{
                    //    if (!string.IsNullOrEmpty(Const.User.UserName))
                    //    {
                    //        var units = CoolServiceModel.getUserGroup(Const.User.UserName);
                    //        string u = "";
                    //        foreach (var item in units)
                    //        {
                    //            u += item + "|";
                    //        }
                    //        u = u.ReplaceLast("|", "");
                    //        if (units != null && units.Any())
                    //        {
                    //            if (units.Contains("EXPERT"))
                    //            {
                    //                req.Req_parameters.P.Add(new P { K = "function_code", V = "41" });
                    //                req.Req_parameters.P.Add(new P { K = "units", V = u });
                    //            }
                    //            else if (units.Contains("EXPERT_SH"))
                    //            {
                    //                req.Req_parameters.P.Add(new P { K = "function_code", V = "42" });
                    //                req.Req_parameters.P.Add(new P { K = "areas", V = u });
                    //            }
                    //            else
                    //            {
                    //                req.Req_parameters.P.Add(new P { K = "function_code", V = "40" });
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        req.Req_parameters.P.Add(new P { K = "function_code", V = "40" });
                    //    }
                    //}
                    #endregion Get FunctionCool Old

                    #region Get FunctionCool New
                    //
                    if (hdfTab.Value == ConstantPrm.SYSTEM.COOL) {
                        req.Req_parameters.P.Add(new P { K = "boarding", V = "Y" });
                        if (!string.IsNullOrEmpty(Const.User.UserName)) {
                            var units = CoolServiceModel.getUserGroup(Const.User.UserName);
                            string u = "";
                            foreach (var item in units) {
                                u += item + "|";
                            }
                            u = u.ReplaceLast("|", "");
                            if (units != null && units.Any()) {
                                if (units.Contains("SCEP") && units.Contains("EXPERT")) {
                                    req.Req_parameters.P.Add(new P { K = "tracking", V = "T" });
                                }
                                if (units.Contains("EXPERT")) {
                                    req.Req_parameters.P.Add(new P { K = "units", V = u });
                                }
                                if (units.Contains("EXPERT_SH")) {
                                    req.Req_parameters.P.Add(new P { K = "areas", V = u });
                                }
                            }
                        } else {
                            req.Req_parameters.P.Add(new P { K = "tracking", V = "T" });
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.VCOOL) {
                        req.Req_parameters.P.Add(new P { K = "function_code", V = VCOOL_FUNCTION_CODE });
                        req.Req_parameters.P.Add(new P { K = "index_1", V = ConstantPrm.SYSTEM.VCOOL });
                        req.Req_parameters.P.Add(new P { K = "index_2", V = ConstantPrm.SYSTEMTYPE.CRUDE });
                    }
                    #endregion Get FunctionCool New

                    req.Extra_xml = "";

                    ResponseData resData = new ResponseData();
                    RequestData reqData = new RequestData();
                    ServiceProvider.ProjService service = new ServiceProvider.ProjService();

                    var xml = ShareFunction.XMLSerialize(req);
                    reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
                    resData = service.CallService(reqData);
                    string _DataJson = resData.extra_xml;



                    if (hdfTab.Value == ConstantPrm.SYSTEM.BUNKER) {
                        List_Bunkertrx _model = ShareFunction.DeserializeXMLFileToObject<List_Bunkertrx>(_DataJson);
                        if (_model != null && _model.BungerTransaction != null && _model.BungerTransaction != null) {
                            if (_model.BungerTransaction.Count > 0) {
                                foreach (var Item in _model.BungerTransaction) {
                                    if (Item.Status == null) Item.Status = "";
                                    if (Item.System == null) Item.System = "";
                                }
                            }
                            LstBunkerSS = _model.BungerTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.CHARTERING) {
                        List_Charteringtrx _model = ShareFunction.DeserializeXMLFileToObject<List_Charteringtrx>(_DataJson);
                        if (_model != null && _model.CharteringTransaction != null && _model.CharteringTransaction != null) {
                            if (_model.CharteringTransaction.Count > 0) {
                                foreach (var Item in _model.CharteringTransaction) {
                                    if (Item.Status == null) Item.Status = "";
                                    if (Item.System == null) Item.System = "";
                                }
                            }
                            LstCharteringSS = _model.CharteringTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.CRUDE_PURCHASE) {
                        List_CrudePurchasetrx _model = ShareFunction.DeserializeXMLFileToObject<List_CrudePurchasetrx>(_DataJson);
                        if (_model != null && _model.CrudePurchaseTransaction != null && _model.CrudePurchaseTransaction != null) {
                            if (_model.CrudePurchaseTransaction.Count > 0) {
                                foreach (var Item in _model.CrudePurchaseTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstCrudePurchaseSS = _model.CrudePurchaseTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.COOL) {
                        List_Cooltrx _model = ShareFunction.DeserializeXMLFileToObject<List_Cooltrx>(_DataJson);
                        if (_model != null && _model.CoolTransaction != null && _model.CoolTransaction != null) {
                            if (_model.CoolTransaction.Count > 0) {
                                foreach (var Item in _model.CoolTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstCoolSS = _model.CoolTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.VCOOL) {
                        //_DataJson = $"<list_trx>;\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"4dd4157ad7664230ac70b54f4685ecf4\" transaction_id=\"201804201155390269317\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"19-Apr-2018\" purchase_no=\"VCO-1804-0071\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"WAITING SCVP APPROVE\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"aa137ec1dc7448f7946974d3533f7c51\" transaction_id=\"201805140948430280663\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"07-May-2018\" purchase_no=\"VCO-1805-0017\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"30-May-2018 to 30-May-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"OSP\" premium_maximum=\"17\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"WAITING SCVP APPROVE\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"30cba45976d24e35865ab647fd200a68\" transaction_id=\"201804271611230273069\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"27-Apr-2018\" purchase_no=\"VCO-1804-0089\" supplier=\"17\" final_price=\"OSP+95 FOB\" product_name=\"Montara Crude\" incoterm=\"FOB\" formula_p=\"OSP+95 FOB $/bbl\" discharging_period=\"23-Jan-2018 to 31-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Australia\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING CMVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"OSP\" premium_maximum=\"94\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"ea1d620b0fd84a298a5e086903110a47\" transaction_id=\"201805031146220276839\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0002\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"06-Jun-2018 to 08-Jun-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"PHA+QjwvcD4=\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING CMVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"REVISED PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"25ac8321e9934936980af2d4d000ea09\" transaction_id=\"201805141357470281094\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"14-May-2018\" purchase_no=\"VCO-1805-0019\" supplier=\"CPAI1000001\" final_price=\"OSP+11 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+11 FCA $/bbl\" discharging_period=\"01-Jan-2018 to 03-Jan-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"300\" volume_kt_max=\"39\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING CMVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"0\" requested_name=\"PORNPIMAN\" quantity_kbbl_max=\"300\" cm_status=\"REVISED PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"d41e5882a5404174afe3e371640043d3\" transaction_id=\"201804081555350265536\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"08-Apr-2018\" purchase_no=\"VCO-1804-0041\" supplier=\"\" final_price=\"DUBAI+100 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"DUBAI+100 FOB $/bbl\" discharging_period=\"01-May-2018 to 01-May-2018\" loading_period=\"14-Apr-2018 to 15-Apr-2018\" lp_result=\"PHA+azwvcD4=\" origin=\"Equatorial\" volume_kbbl_max=\"100\" volume_kt_max=\"13\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING SCVP APPROVE\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"DUBAI\" premium_maximum=\"100\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"100\" cm_status=\"WAITING SCVP APPROVE\"></transaction>\r\n<transaction function_id=\"63\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"1250d034d4da4182ae29241a088b8565\" transaction_id=\"201804111113560267536\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_12\" status=\"APPROVED\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0063\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"CHECK TANK APPROVED\" tn_status=\"CHECK IMPACT APPROVED\" vco_data_status=\"APPROVED\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISED PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"115ee4722d4a4db69b0ecae800492ae7\" transaction_id=\"201803211511330261652\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Mar-2018\" purchase_no=\"VCO-1803-0019\" supplier=\"17\" final_price=\"OSP+51 FOB\" product_name=\"Camar\" incoterm=\"FOB\" formula_p=\"OSP+51 FOB $/bbl\" discharging_period=\"20-Mar-2018 to 22-Mar-2018\" loading_period=\" to \" lp_result=\"\" origin=\"Indonesia\" volume_kbbl_max=\"42\" volume_kt_max=\"5.46\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Mar\" tpc_year=\"2018\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"51\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"42\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"cc98125887774ae4940308d2d9f2c6cf\" transaction_id=\"201803211351110261303\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Mar-2018\" purchase_no=\"VCO-1803-0018\" supplier=\"17\" final_price=\"OSP+31 FOB\" product_name=\"AKPO blend\" incoterm=\"FOB\" formula_p=\"OSP+31 FOB $/bbl\" discharging_period=\"20-Mar-2018 to 23-Mar-2018\" loading_period=\"01-Jan-2018 to 02-Jan-2018\" lp_result=\"\" origin=\"Nigeria\" volume_kbbl_max=\"35\" volume_kt_max=\"4.55\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Mar\" tpc_year=\"2018\" sc_status=\"CMVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"31\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"35\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"1765711755ae4422a371040eb41b1d98\" transaction_id=\"201803211549450261959\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"21-Mar-2018\" purchase_no=\"VCO-1803-0023\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"Bongkot\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"02-Apr-2018 to 02-Apr-2018\" loading_period=\"09-Jan-2018 to 10-Jan-2018\" lp_result=\"PHA+czwvcD4=\" origin=\"Thailand\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"CMVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"4f77baa051674aa3b9ae5e5a927e288d\" transaction_id=\"201803231043300262191\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"21-Mar-2018\" purchase_no=\"VCO-1803-0025\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"Bongkot\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"21-Mar-2018 to 22-Mar-2018\" loading_period=\"09-Jan-2018 to 10-Jan-2018\" lp_result=\"\" origin=\"Thailand\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Mar\" tpc_year=\"2018\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Invalid Status\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"10000033\" result_desc=\"Invalid Status\" req_transaction_id=\"3949b3dbd6fd4b88a609e02e136aad8b\" transaction_id=\"201803291119070262741\" response_message=\"ʶҹ���¡�����١��ͧ\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_9\" status=\"WAITING SCVP APPROVE\" date_purchase=\"10-Mar-2018\" purchase_no=\"VCO-1803-0032\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Alba\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"29-Dec-2017 to 30-Dec-2017\" loading_period=\"22-Dec-2017 to 23-Dec-2017\" lp_result=\"\" origin=\"United Kingdom\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Mar\" tpc_year=\"2018\" sc_status=\"WAITING SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"6044ad89ca0d4bb390d28536fa2dfe31\" transaction_id=\"201803211047100261107\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Mar-2018\" purchase_no=\"VCO-1803-0017\" supplier=\"17\" final_price=\"OSP+31 FOB\" product_name=\"AKPO blend\" incoterm=\"FOB\" formula_p=\"OSP+31 FOB $/bbl\" discharging_period=\"21-Mar-2018 to 22-Mar-2018\" loading_period=\"01-Jan-2018 to 02-Jan-2018\" lp_result=\"\" origin=\"Nigeria\" volume_kbbl_max=\"35\" volume_kt_max=\"4.55\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Mar\" tpc_year=\"2018\" sc_status=\"CMVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"31\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"35\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"44be4783c89a42f3a074fb2e636996d1\" transaction_id=\"201804031141580263301\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"21-Mar-2018\" purchase_no=\"VCO-1804-0004\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Cancel\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"2\" result_desc=\"Cancel\" req_transaction_id=\"4e97c85eb9044de0b36a353e0844dcb0\" transaction_id=\"201804031612180263907\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"03-Apr-2018\" purchase_no=\"VCO-1804-0012\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"12-Apr-2018 to 13-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"68a7df4abc9c4a5c8d7a96b2d704b785\" transaction_id=\"201804051054130264363\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"DRAFT_9\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"03-Apr-2018\" purchase_no=\"VCO-1804-0018\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"11-Apr-2018 to 12-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"WAITING CMVP APPROVE\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"4a8bf928556040a58e4e1a892328788b\" transaction_id=\"201805071038500278364\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0004\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"12-Apr-2018 to 12-Apr-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"770d86abb7404e6989a0cb7e4e6a4cb0\" transaction_id=\"201805071107590278503\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0006\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"06-Jun-2018 to 06-Jun-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"TERMINATED\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"c13d046bedc447f8bb050bfe9a77a00d\" transaction_id=\"201804171550560268082\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0065\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"WAITING CMVP APPROVE\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"EXCEPTION\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"24f380e5e1c548268b1ebfd9e36deacc\" transaction_id=\"201804250923560271096\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"24-Apr-2018\" purchase_no=\"VCO-1804-0078\" supplier=\"\" final_price=\"OSP+22 CIP\" product_name=\"ASENG\" incoterm=\"CIP\" formula_p=\"OSP+22 CIP $/bbl\" discharging_period=\"11-May-2018 to 15-May-2018\" loading_period=\"26-Apr-2018 to 27-Apr-2018\" lp_result=\"\" origin=\"Equatorial\" volume_kbbl_max=\"2\" volume_kt_max=\"0.26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"20\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"2\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"c1827f684965480386e3e3b70d338db5\" transaction_id=\"201804301356420274466\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0094\" supplier=\"1000010\" final_price=\"DUBAI+55 FOB\" product_name=\"Al Shaheen\" incoterm=\"FOB\" formula_p=\"DUBAI+55 FOB $/bbl\" discharging_period=\"01-Feb-2018 to 28-Feb-2018\" loading_period=\"01-Dec-2017 to 31-Dec-2017\" lp_result=\"\" origin=\"Qatar\" volume_kbbl_max=\"690\" volume_kt_max=\"89.7\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING TNVP APPROVE\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"DUBAI\" premium_maximum=\"55\" requested_name=\"CHAYANEE\" quantity_kbbl_max=\"690\" cm_status=\"REVISED PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"0bce64534d52421fbe8f5f6e1356a7c7\" transaction_id=\"201804301424370274891\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0102\" supplier=\"1000010\" final_price=\"DUBAI+55 FOB\" product_name=\"Al Shaheen\" incoterm=\"FOB\" formula_p=\"DUBAI+55 FOB $/bbl\" discharging_period=\"01-Feb-2018 to 28-Feb-2018\" loading_period=\"01-Dec-2017 to 31-Dec-2017\" lp_result=\"\" origin=\"Qatar\" volume_kbbl_max=\"690\" volume_kt_max=\"89.7\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"DUBAI\" premium_maximum=\"55\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"690\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"268d307529bb49989d643af64054eb85\" transaction_id=\"201804271605320272931\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"27-Apr-2018\" purchase_no=\"VCO-1804-0086\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"Walio\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"28-Apr-2018 to 29-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Indonesia\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"98\" requested_name=\"CHAYANEE\" quantity_kbbl_max=\"200\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Invalid Status\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"10000033\" result_desc=\"Invalid Status\" req_transaction_id=\"a9573152f38942e381437823935e13ee\" transaction_id=\"201804091448140265872\" response_message=\"ʶҹ���¡�����١��ͧ\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_9\" status=\"WAITING SCVP APPROVE\" date_purchase=\"04-Apr-2018\" purchase_no=\"VCO-1804-0044\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"CMVP APPROVED\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"MAIL\" result_status=\"SUCCESS\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"00cb09f2f11540779dcf304c478278a4\" transaction_id=\"201804301425370274914\" response_message=\"����¡�������\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0104\" supplier=\"1000010\" final_price=\"DUBAI+55 FOB\" product_name=\"Al Shaheen\" incoterm=\"FOB\" formula_p=\"DUBAI+55 FOB $/bbl\" discharging_period=\"01-Feb-2018 to 28-Feb-2018\" loading_period=\"01-Dec-2017 to 31-Dec-2017\" lp_result=\"\" origin=\"Qatar\" volume_kbbl_max=\"690\" volume_kt_max=\"89.7\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\"vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"DUBAI\" premium_maximum=\"55\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"690\" cm_status=\"WAITING SCVP APPROVE\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"385e6237ccac48d5a3315461a43e634e\" transaction_id=\"201804101705370266941\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"04-Apr-2018\" purchase_no=\"VCO-1804-0056\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"6ec7ea42abc54a45b577d5c153be422b\" transaction_id=\"201804110938520267339\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"04-Apr-2018\" purchase_no=\"VCO-1804-0058\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Reject\" result_namespace=\"CPAI\" result_status=\"PROCESSING\" result_code=\"2\" result_desc=\"Reject\" req_transaction_id=\"677691d95243490687fb9a83c0d4b989\" transaction_id=\"201804110948070267390\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"04-Apr-2018\" purchase_no=\"VCO-1804-0059\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING CMVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"67884f6215be4fb08d9733c667b0df81\" transaction_id=\"201804301519140275875\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0109\" supplier=\"CPAI1000001\" final_price=\"DUBAI+20 FOB\" product_name=\"Murban\" incoterm=\"FOB\" formula_p=\"DUBAI+20 FOB $/bbl\" discharging_period=\"01-May-2018 to 10-May-2018\" loading_period=\"17-Dec-2017 to 18-Dec-2017\" lp_result=\"\" origin=\"Thailand\" volume_kbbl_max=\"17\" volume_kt_max=\"2.21\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"DUBAI\" premium_maximum=\"20\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"17\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"8d9d4184c9394e34876b0009c83328e6\" transaction_id=\"201804301118080273660\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0092\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"Van Gogh\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"10-May-2018 to 12-May-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Australia\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING TNVP APPROVE\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"99\" requested_name=\"CHAYANEE\" quantity_kbbl_max=\"200\" cm_status=\"\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"ae8eacbd6ab24531ac7b6ccbfd9a9376\" transaction_id=\"201804301340280274308\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"CANCEL\" status=\"TERMINATED\" date_purchase=\"30-Apr-2018\" purchase_no=\"VCO-1804-0090\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"Walio\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"PHA+bW9iaWxlIHJldmlzZTwvcD4=\" origin=\"Indonesia\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"TERMINATED\" tn_status=\"TERMINATED\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"100\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Invalid Status\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"10000033\" result_desc=\"Invalid Status\" req_transaction_id=\"162d1ffa60df4b89845d9a4a179c6e63\" transaction_id=\"201804101304180266702\" response_message=\"ʶҹ���¡�����١��ͧ\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0054\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"15-Apr-2018 to 16-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"WAITING SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"WAITING CMVP APPROVE\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"d0d80f060fe248cca4ffe6b12e8caf77\" transaction_id=\"201805041133190277166\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"02-May-2018\" purchase_no=\"VCO-1804-0099\" supplier=\"17\" final_price=\"OSP+21 FOB\" product_name=\"AKPO blend\" incoterm=\"FOB\" formula_p=\"OSP+21 FOB $/bbl\" discharging_period=\"24-May-2018 to 25-May-2018\" loading_period=\"01-Jan-2018 to 02-Jan-2018\" lp_result=\"PHA+dGVzdCBtb2JpbGUgNCA0IDQgNTwvcD4=\" origin=\"Nigeria\" volume_kbbl_max=\"15\" volume_kt_max=\"1.95\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"21\" requested_name=\"CHAYANEE\" quantity_kbbl_max=\"15\" cm_status=\"WAITING SCVP APPROVE\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"74f08b85ecd14e19910b1021f0373204\" transaction_id=\"201805071305420278869\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0007\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"01-Jun-2018 to 01-Jun-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"11346739fbec40a9855b0851ef49b9ad\" transaction_id=\"201805071314100278949\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0008\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"01-Apr-2018 to 10-Apr-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"f28acfe3655a45c7ae51b1ca7eb4a8da\" transaction_id=\"201805071254120278770\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"APPROVE_8\" status=\"SCVP REQUEST TO REVISE\" date_purchase=\"03-May-2018\" purchase_no=\"VCO-1805-0012\" supplier=\"CPAI1000001\" final_price=\"OSP+19 FCA\" product_name=\"Karpal\" incoterm=\"FCA\" formula_p=\"OSP+19 FCA $/bbl\" discharging_period=\"01-Apr-2018 to 10-Apr-2018\" loading_period=\"25-Dec-2017 to 26-Dec-2017\" lp_result=\"\" origin=\"Unknown\" volume_kbbl_max=\"16\" volume_kt_max=\"2.08\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"SCVP REQUEST TO REVISE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"19\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"16\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"efbb25025bb3436abaec5b9cd7486ded\" transaction_id=\"201804091611480265992\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"04-Apr-2018\" purchase_no=\"VCO-1804-0046\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING CMVP SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISE PURCHASE COMMENT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"717997da10e649c9a5d7becb16f69ec2\" transaction_id=\"201804101831090267010\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0057\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"11-Apr-2018 to 12-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"CANCEL\" tn_status=\"CANCEL\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"fa4fe283a2544a8f8421f54ec3a7eac6\" transaction_id=\"201804101312570266821\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0055\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"12-Apr-2018 to 13-Apr-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Dec\" tpc_year=\"2017\" sc_status=\"CANCEL\" tn_status=\"CANCEL\" vco_data_status=\"TERMINATED\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"CMVP REJECT\"></transaction>\r\n<transaction function_id=\"64\" function_desc=\"Success\" result_namespace=\"CPAI\" result_status=\"-\" result_code=\"1\" result_desc=\"Success\" req_transaction_id=\"7bb2d1f1eed84a5984c9e6f7361ca448\" transaction_id=\"201804111423120267745\" response_message=\"Your transaction has been processed successfully.\" system=\"VCOOL\" type=\"CRUDE\" action=\"REJECT_5\" status=\"CMVP REQUEST TO REVISE\" date_purchase=\"10-Apr-2018\" purchase_no=\"VCO-1804-0060\" supplier=\"17\" final_price=\"OSP+101 FOB\" product_name=\"ASENG\" incoterm=\"FOB\" formula_p=\"OSP+101 FOB $/bbl\" discharging_period=\"27-Jan-2018 to 28-Jan-2018\" loading_period=\"25-Jan-2018 to 26-Jan-2018\" lp_result=\"\" origin=\"Equatorial Guinea\" volume_kbbl_max=\"200\" volume_kt_max=\"26\" user_group=\"SCVP\" user_group_delegate=\"\" tpc_month=\"Apr\" tpc_year=\"2018\" sc_status=\"WAITING SCVP APPROVE\" tn_status=\"WAITING CHECK IMPACT\" vco_data_status=\"CONSENSUS APPROVAL\" benchmark_price=\"OSP\" premium_maximum=\"101\" requested_name=\"VC_CMCS\" quantity_kbbl_max=\"200\" cm_status=\"REVISED PURCHASE COMMENT\"></transaction></list_trx>";
                        //var temp = _DataJson.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                        //var temp_i = 0;
                        //var temp_DATA = "";
                        //try {
                        //    for (int i = 0; i < temp.Length; i++) {
                        //        if (i != 0 && i != temp.Length) {
                        //            temp_i = i;
                        //            var tempData = "<list_trx>\r\n";
                        //            tempData = tempData + temp[i];
                        //            tempData = tempData + "\r\n</list_trx>";
                        //            temp_DATA = tempData;
                        //            List_VCooltrx _model_temp = ShareFunction.DeserializeXMLFileToObject<List_VCooltrx>(tempData);
                        //        }
                        //    }
                        //} catch (Exception) {

                        //    throw;
                        //}

                        //_DataJson = ShareFn.DecodeString(_DataJson);

                        List_VCooltrx _model = ShareFunction.DeserializeXMLFileToObject<List_VCooltrx>(_DataJson);
                        if (_model != null && _model.VCoolTransaction != null && _model.VCoolTransaction != null) {
                            if (_model.VCoolTransaction.Count > 0) {
                                foreach (var Item in _model.VCoolTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstVCoolSS = _model.VCoolTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    }
                      //else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_DEAL)
                      //{
                      //    List_HedgDealtrx _model = ShareFunction.DeserializeXMLFileToObject<List_HedgDealtrx>(_DataJson);
                      //    if (_model != null && _model.HedgDealEncryptTransaction != null)
                      //    {
                      //        if (_model.HedgDealEncryptTransaction.Count > 0)
                      //        {
                      //            foreach (var Item in _model.HedgDealEncryptTransaction)
                      //            {
                      //                if (Item.status == null) Item.status = "";
                      //                if (Item.system == null) Item.system = "";
                      //            }
                      //        }
                      //        LstHedgDealSS = _model.HedgDealEncryptTransaction;
                      //        LoadDataToGrid2();
                      //    }
                      //    else
                      //    {
                      //        throw new Exception(resData.response_message);
                      //    }
                      //}
                      //else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_TCKT)
                      //{
                      //    List_HedgTickettrx _model = ShareFunction.DeserializeXMLFileToObject<List_HedgTickettrx>(_DataJson);
                      //    if (_model != null && _model.HedgTicketEncryptTransaction != null)
                      //    {
                      //        if (_model.HedgTicketEncryptTransaction.Count > 0)
                      //        {
                      //            foreach (var Item in _model.HedgTicketEncryptTransaction)
                      //            {
                      //                if (Item.status == null) Item.status = "";
                      //                if (Item.system == null) Item.system = "";
                      //            }
                      //        }
                      //        LstHedgTicketSS = _model.HedgTicketEncryptTransaction;
                      //        LoadDataToGrid2();
                      //    }
                      //    else
                      //    {
                      //        throw new Exception(resData.response_message);
                      //    }
                      //}
                      else if (hdfTab.Value == ConstantPrm.SYSTEM.PAF) {
                        List_PAFtrx _model = ShareFunction.DeserializeXMLFileToObject<List_PAFtrx>(_DataJson);
                        if (_model != null && _model.PAFTransaction != null) {
                            if (_model.PAFTransaction.Count > 0) {
                                foreach (var Item in _model.PAFTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstPafTicketSS = _model.PAFTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.DAF) {
                        List_DAFtrx _model = ShareFunction.DeserializeXMLFileToObject<List_DAFtrx>(_DataJson);
                        if (_model != null && _model.DAFTransaction != null) {
                            if (_model.DAFTransaction.Count > 0) {
                                foreach (var Item in _model.DAFTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstDafTicketSS = _model.DAFTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    } else if (hdfTab.Value == ConstantPrm.SYSTEM.CDS) {
                        List_CDStrx _model = ShareFunction.DeserializeXMLFileToObject<List_CDStrx>(_DataJson);
                        if (_model != null && _model.CDSTransaction != null) {
                            if (_model.CDSTransaction.Count > 0) {
                                foreach (var Item in _model.CDSTransaction) {
                                    if (Item.status == null) Item.status = "";
                                    if (Item.system == null) Item.system = "";
                                }
                            }
                            LstCdsTicketSS = _model.CDSTransaction;
                            LoadDataToGrid2();
                        } else {
                            throw new Exception(resData.response_message);
                        }
                    }
                } else {
                    hdfType.Value = "blank";
                }
            } catch (Exception ex) {
                if (ex.Message.ToUpper().Trim() == "Invalid user group".ToUpper().Trim()) {
                    Response.Redirect("Blankpage.aspx");
                } else {
                    HTMLStatusBox = string.Format("<label style=\"color: red\">{0}</label>", ex.Message);
                }
            }
        }

        private void LoadDataToGrid2() {
            if (hdfTab.Value == ConstantPrm.SYSTEM.BUNKER) {
                MakeTabMenuBunger(LstBunkerSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.CHARTERING) {
                MakeTabMenuChartering(LstCharteringSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.CRUDE_PURCHASE) {
                MakeTabMenuCrudePurchase(LstCrudePurchaseSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.COOL) {
                MakeTabMenuCool(LstCoolSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.VCOOL) {
                MakeTabMenuVCool(LstVCoolSS, hdfTab.Value, hdfStatus.Value);
            }
              //else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_DEAL)
              //{
              //    MakeTabMenuHedgDeal(LstHedgDealSS, hdfTab.Value, hdfStatus.Value);
              //}
              //else if (hdfTab.Value == ConstantPrm.SYSTEM.HEDG_TCKT)
              //{
              //    MakeTabMenuHedgTicket(LstHedgTicketSS, hdfTab.Value, hdfStatus.Value);
              //}
              else if (hdfTab.Value == ConstantPrm.SYSTEM.PAF) {
                MakeTabMenuPaf(LstPafTicketSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.DAF) {
                MakeTabMenuDaf(LstDafTicketSS, hdfTab.Value, hdfStatus.Value);
            } else if (hdfTab.Value == ConstantPrm.SYSTEM.CDS) {
                MakeTabMenuCds(LstCdsTicketSS, hdfTab.Value, hdfStatus.Value);
            }
        }

        private void MakeTabMenuHTML() {
            HTMLNavTag = "";
            HTMLNavTag += string.Format("<div class=\"tabs-4\">");
            HTMLNavTag += "<ul class=\"nav nav-tabs\">";
            foreach (var _tab in gMainBoards.menu) {
                CounterDetail data = CounterTransetion(_tab.name);
                int counter = 0;

                for (int i = 0; i < data.counter_txn.Count(); i++) {
                    counter += Convert.ToInt32(data.counter_txn.ElementAt(i).counter);
                }

                HTMLNavTag += string.Format("<li {3}><a onclick=\"CallByTab('{2}');return false;\">{0}{1}</a></li>"
                    , _tab.tab_name
                    , "<sup>" + counter + "</sup>"
                    , _tab.name
                    , (hdfTab.Value == _tab.name) ? "class=\"active\"" : "");
            }
            HTMLNavTag += "</ul>";
            HTMLNavTag += "</div>";
        }


        private void MakeTabMenuHTML(string[] activeCount, string[] systemType) {
            HTMLNavTag = "";
            HTMLNavTag += string.Format("<div class=\"tabs-4\">");
            HTMLNavTag += "<ul class=\"nav nav-tabs\">";

            foreach (var _tab in gMainBoards.menu) {
                int counter = 0;

                if (systemType.Contains(_tab.name)) {
                    var _systemIndex = systemType.FindIndex(i => i == _tab.name);
                    var _systemType = systemType.ElementAt(_systemIndex);
                    var _activeCount = activeCount.ElementAt(_systemIndex);
                    counter = Convert.ToInt32(_activeCount);
                } else {
                    CounterDetail data = CounterTransetion(_tab.name);
                    for (int i = 0; i < data.counter_txn.Count(); i++) {
                        counter += Convert.ToInt32(data.counter_txn.ElementAt(i).counter);
                    }
                }
                HTMLNavTag += string.Format("<li {3}><a onclick=\"CallByTab('{2}');return false;\">{0}{1}</a></li>"
                    , _tab.tab_name
                    , "<sup>" + counter + "</sup>"
                    , _tab.name
                    , (hdfTab.Value == _tab.name) ? "class=\"active\"" : "");
            }
            HTMLNavTag += "</ul>";
            HTMLNavTag += "</div>";
        }

        private void MakeStatusMenuDetailHTML(string ptSysTabs, string ptStatus, string[] paTab, string[] paStatus, string[] paCount) {
            HTMLStatusBox = "";
            int nTotalLine = paTab.Length / gnStatusLength;
            int nStatus = (paTab.Length % gnStatusLength);
            int total = paTab.Length;
            double nPercentWidth = nTotalLine != 0 ? (100.00 / 7.00) : (100.00 / nStatus);

            for (int nLine = 0; nLine <= nTotalLine; nLine++) {
                if (total >= gnStatusLength) {
                    int nTotalIndex = nLine * gnStatusLength;
                    HTMLStatusBox += "<div class=\"status-task\">";
                    HTMLStatusBox += "<div class=\"status-box\" style=\"width: 100%;\">";
                    for (int i = 0; i < gnStatusLength; i++) {
                        string tStyle = "style=\"width: " + nPercentWidth + "%; \"";
                        if (ptStatus == paStatus[i + nTotalIndex]) tStyle = "style=\"width: " + nPercentWidth + "%; background: #e21789;\"";
                        if (paStatus[i + nTotalIndex] == "*") {
                            if (ptStatus == "") tStyle = "style=\"width: " + nPercentWidth + "%; background: #e21789;\"";
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('all');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i], paTab[i], tStyle);
                        } else if (paStatus[i + nTotalIndex] == ConstantPrm.ACTION.WAITING_ENDORSE || paStatus[i + nTotalIndex] == ConstantPrm.ACTION.WAITING) {
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{3}');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i], paTab[i], tStyle, paStatus[i]);
                        } else {
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{3}');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i + nTotalIndex], paTab[i + nTotalIndex], tStyle, paStatus[i + nTotalIndex]);
                        }
                    }
                    HTMLStatusBox += "</div></div>";
                    total = total - gnStatusLength;
                } else if (total > 0 && total < gnStatusLength) {
                    int nTotalIndex = nTotalLine * gnStatusLength;
                    HTMLStatusBox += "<div class=\"status-task\">";
                    HTMLStatusBox += "<div class=\"status-box\" style=\"width: 100%;\">";
                    for (int i = 0; i < nStatus; i++) {
                        string tStyle = "style=\"width: " + nPercentWidth + "%; \"";
                        if (ptStatus == paStatus[i + nTotalIndex]) tStyle = "style=\"width: " + nPercentWidth + "%; background: #e21789;\"";
                        if (paStatus[i + nTotalIndex] == "*") {
                            if (ptStatus == "") tStyle = "style=\"width: " + nPercentWidth + "%; background: #e21789;\"";
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('all');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i], paTab[i], tStyle);
                        } else if (paStatus[i + nTotalIndex] == ConstantPrm.ACTION.WAITING_ENDORSE || paStatus[i + nTotalIndex] == ConstantPrm.ACTION.WAITING) {
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{3}');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i], paTab[i], tStyle, paStatus[i]);
                        } else {
                            HTMLStatusBox += string.Format("<div onclick=\"CallByStatus('{3}');\" {2}><b>{0}</b><span>{1}</span></div>", paCount[i + nTotalIndex], paTab[i + nTotalIndex], tStyle, paStatus[i + nTotalIndex]);
                        }
                    }
                    HTMLStatusBox += "</div></div>";
                }
            }
        }

        private void MakeTabMenuBunger(List<BungerTransaction> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.Date_purchase != null && x.System == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_CERTIFIED) {
                        atCount[i] = ObjLstData.Where(x => x.Status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.Status == listStatus[i]).ToList().Count.ToString();
                    }
                }
                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }
                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusBunger(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusBunger(List<BungerTransaction> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<BungerTransaction>();

                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING_CERTIFIED) _lstObj = lstDataAll.Where(x => x.Status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.Status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        private void MakeTabMenuChartering(List<CharteringTransaction> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.Date_purchase != null && x.System == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_CERTIFIED) {
                        atCount[i] = ObjLstData.Where(x => x.Status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.Status == listStatus[i]).ToList().Count.ToString();
                    }
                }
                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }

                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusChartering(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusChartering(List<CharteringTransaction> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CharteringTransaction>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.Status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.Status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        private void MakeTabMenuCrudePurchase(List<CrudePurchaseEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.date_purchase != null && x.system == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_CERTIFIED) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    }
                }
                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }
                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusCrudePurchase(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusCrudePurchase(List<CrudePurchaseEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CrudePurchaseEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        private void MakeTabMenuCool(List<CoolEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.date_purchase != null && x.system == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    }
                }

                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }
                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusCool(ObjLstData, ActiveStatus);
            }
        }
        private void MakeTabMenuVCool(List<VCoolEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.date_purchase != null && x.system == ActiveSysTab).ToList();

            var usrGroup = VCoolServiceModel.getUserGroup(Const.User.UserName);
            ObjLstData = VCoolServiceModel.checkCancelData(ObjLstData);
            List<string> statusCondition = new List<string>();

            //zkavin.i old method changed 2018 05 30
            //if (usrGroup.Contains("SCEP_SH")) {
            //    if (usrGroup.Contains("SCEP")) {
            //        ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE RUN LP" || x.status == "REVISE LP VERIFICATION" || x.status == "WAITING RUN LP" || x.status == "REVISE LP RUN").ToList();
            //    } else {
            //        ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE RUN LP" || x.status == "REVISE LP VERIFICATION").ToList();
            //    }
            //    //if (usrGroup.Contains("SCEP"))
            //    //{
            //    //    ObjLstData = ObjLstData.Where(x =>  x.status == "WAITING RUN LP" || x.status == "WAITING APPROVE RUN LP").ToList();
            //    //}
            //    //else
            //    //{
            //    //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE RUN LP" || x.status == "REVISE LP VERIFICATION" || x.status == "REQUEST TO REVISE").ToList();
            //    //}
            //} else if (usrGroup.Contains("SCEP")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING RUN LP" || x.status == "REVISE LP RUN").ToList();
            //    //ObjLstData = ObjLstData.Where(x => x.status == "WAITING RUN LP" || x.status == "REVISE LP RUN" || x.status == "WAITING APPROVE RUN LP" || x.status == "REQUEST TO REVISE").ToList();
            //    //ObjLstData = ObjLstData.Where(x => x.status == "WAITING RUN LP" || x.status == "REVISE LP RUN" || x.status == "REQUEST TO REVISE").ToList();
            //} else if (usrGroup.Contains("SCSC_SH")) {
            //    if (usrGroup.Contains("SCSC")) {
            //        ObjLstData = ObjLstData.Where(x => x.status == "WAITING CHECK TANK" || x.status == "WAITING APPROVE TANK" || x.status == "REVISE TANK VERIFICATION" || x.status == "REVISE TANK AVAILABILITY").ToList();
            //    } else {
            //        ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE TANK" || x.status == "REVISE TANK VERIFICATION").ToList();
            //    }
            //    //if (usrGroup.Contains("SCSC"))
            //    //{
            //    //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING CHECK TANK" || x.status == "SCVP REQUEST TO REVISE" || x.status == "WAITING APPROVE TANK" || x.status == "REQUEST TO REVISE" || x.status == "REVISE TANK VERIFICATION").ToList();
            //    //}
            //    //else
            //    //{
            //    //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE TANK" || x.status == "SCVP REQUEST TO REVISE" || x.status == "REQUEST TO REVISE" || x.status == "REVISE TANK VERIFICATION").ToList();
            //    //}
            //} else if (usrGroup.Contains("SCSC")) {
            //    //|| x.status == "SCVP REQUEST TO REVISE" || x.status == "REQUEST TO REVISE"
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING CHECK TANK" || x.status == "REVISE TANK AVAILABILITY").ToList();
            //} else if (usrGroup.Contains("TNPB")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING CHECK IMPACT").ToList();
            //} else if (usrGroup.Contains("SCVP")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING CMVP SCVP APPROVE" || x.status == "WAITING SCVP APPROVE").ToList();
            //    //ObjLstData = ObjLstData.Where(x => x.status == "WAITING CMVP SCVP APPROVE" || x.status == "CHECK TANK APPROVED" || x.status == "WAITING SCVP APPROVE").ToList();
            //} else if (usrGroup.Contains("CMVP")) {
            //    if (usrGroup.Contains("CMCS_SH")) {
            //        ObjLstData = ObjLstData.Where(x => (x.status == "WAITING CMVP SCVP APPROVE" || x.status == "WAITING CMVP APPROVE" || x.status == "WAITING APPROVE PRICE")).ToList();
            //    } else {
            //        ObjLstData = ObjLstData.Where(x => (x.status == "WAITING CMVP SCVP APPROVE" || x.status == "WAITING CMVP APPROVE")).ToList();
            //    }

            //    //ObjLstData = ObjLstData.Where(x => (x.status == "WAITING CMVP SCVP APPROVE" || x.status == "CHECK TANK APPROVED" || x.status == "WAITING CMVP APPROVE")).ToList();

            //    //List<VCoolEncrypt> newObjLstData = new List<VCoolEncrypt>(); 
            //    //foreach (var item in ObjLstData)
            //    //{
            //    //    using (DAL.Entity.EntityCPAIEngine context = new DAL.Entity.EntityCPAIEngine())
            //    //    {
            //    //       var chkpurNum =  context.VCO_DATA.SingleOrDefault(a => a.VCDA_PURCHASE_NO == item.purchase_no);
            //    //        if(chkpurNum != null)
            //    //        {
            //    //            if (chkpurNum.VCDA_CM_STATUS != "CMVP APPROVED" && chkpurNum.VCDA_CM_STATUS != "CMVP REJECT")
            //    //            {
            //    //                newObjLstData.Add(item);
            //    //            }
            //    //        }
            //    //    }                        
            //    //}
            //    //ObjLstData = new List<VCoolEncrypt>();
            //    //ObjLstData = newObjLstData;
            //} else if (usrGroup.Contains("TNVP")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING TNVP APPROVE").ToList();
            //} else if (usrGroup.Contains("EVPC")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE SUMMARY").ToList();
            //} else if (usrGroup.Contains("CMCS_SH")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "WAITING APPROVE PRICE").ToList();
            //} else if (usrGroup.Contains("CMCS")) {
            //    ObjLstData = ObjLstData.Where(x => x.status == "DRAFT" || x.status == "WAITING EDIT ETA DATE" || x.status == "WAITING PROPOSE ETA DATE" /*|| x.status == "WAITING APPROVE PRICE"*/
            //    || x.status == "WAITING CONFIRM PRICE" || x.status == "REVISE PROPOSE ETA" || x.status == "REVISE PURCHASE COMMENT").ToList();
            //}

            if (usrGroup.Contains("SCEP_SH")) {
                statusCondition.Add(STATUS_WAITING_APPROVE_RUN_LP);
                statusCondition.Add(STATUS_REVISE_APPROVE_RUN_LP);
            }
            if (usrGroup.Contains("SCEP")) {
                statusCondition.Add(STATUS_WAITING_RUN_LP);
                statusCondition.Add(STATUS_REVISE_LP_RUN);
            }
            if (usrGroup.Contains("SCSC_SH")) {
                statusCondition.Add(STATUS_WAITING_APPROVE_TANK);
                statusCondition.Add(STATUS_REVISE_APPROVE_TANK);
            }
            if (usrGroup.Contains("SCSC")) {
                statusCondition.Add(STATUS_WAITING_CHECK_TANK);
                statusCondition.Add(STATUS_REVISE_TANK_AVAILABILITY);
            }
            if (usrGroup.Contains("TNPB")) {
                statusCondition.Add(STATUS_WAITING_CHECK_IMPACT);
            }
            if (usrGroup.Contains("SCVP")) {
                statusCondition.Add(STATUS_WAITING_VP_APPROVE_TANK);
                statusCondition.Add(STATUS_APPROVED_BY_CMVP);
            }
            if (usrGroup.Contains("CMVP")) {
                statusCondition.Add(STATUS_WAITING_VP_APPROVE_TANK);
                statusCondition.Add(STATUS_APPROVED_BY_SCVP);
            }
            if (usrGroup.Contains("TNVP")) {
                statusCondition.Add(STATUS_WAITING_VP_CHECK_IMPACT);
            }
            if (usrGroup.Contains("EVPC")) {
                statusCondition.Add(STATUS_WAITING_APPROVE_SUMMARY);
            }
            if (usrGroup.Contains("CMCS_SH")) {
                statusCondition.Add(STATUS_WAITING_APPROVE_PRICE);
            }
            if (usrGroup.Contains("CMCS")) {
                statusCondition.Add(STATUS_DRAFT);
                statusCondition.Add(STATUS_WAITING_EDIT_ETA_DATE);
                statusCondition.Add(STATUS_WAITING_PROPOSE_ETA_DATE);
                statusCondition.Add(STATUS_WAITING_CONFIRM_PRICE);
                statusCondition.Add(STATUS_REVISE_PROPOSE_ETA);
                statusCondition.Add(STATUS_REVISE_PURCHASE_COMMENT);
            }
            if (statusCondition.Count>0) {
                ObjLstData = ObjLstData.Where(x => statusCondition.Contains(x.status)).ToList();
            } else {
                ObjLstData = ObjLstData.Where(x => x.status == "").ToList();
            }
           

            ObjLstData = ObjLstData.OrderByDescending(x => x.transaction_id).ToList();
            //ObjLstData = ObjLstData.OrderByDescending(x => Convert.ToDateTime(x.created_date)).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string allCount = "0";
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        allCount = ObjLstData.Count.ToString();
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == "APPROVED") {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status.Contains(listStatus[i])).ToList().Count.ToString();
                    }
                }

                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }

                MakeTabMenuHTML(new string[] { allCount }, new string[] { ActiveSysTab });
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusVCool(ObjLstData, ActiveStatus);
            }
        }
        private void MakeDataActiveStatusCool(List<CoolEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CoolEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status.ToUpper() == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }
        private void MakeDataActiveStatusVCool(List<VCoolEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<VCoolEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status.ToUpper() == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }
        //private void MakeTabMenuHedgDeal(List<HedgDealEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus)
        //{
        //    var ObjLstData = lstDataAll.Where(x => x.deal_date != null && x.system == ActiveSysTab).ToList();
        //    string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
        //    string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
        //    if (ObjLstData != null)
        //    {
        //        string[] atCount = new string[listTab.Length];
        //        for (int i = 0; i < listTab.Length; i++)
        //        {
        //            if (listStatus[i] == "*")
        //            {
        //                atCount[i] = ObjLstData.Count.ToString();
        //            }
        //            else if (listStatus[i] == ConstantPrm.ACTION.WAITING_CERTIFIED)
        //            {
        //                atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
        //            }
        //            else
        //            {
        //                atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
        //            }
        //        }
        //        MakeTabMenuHTML();
        //        MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
        //        MakeDataActiveStatusHedgDeal(ObjLstData, ActiveStatus);
        //    }
        //}

        //private void MakeDataActiveStatusHedgDeal(List<HedgDealEncrypt> lstDataAll, string ActiveStatus)
        //{
        //    HTMLDataText.Clear();
        //    if (lstDataAll != null)
        //    {
        //        var _lstObj = new List<HedgDealEncrypt>();
        //        if (ActiveStatus == "") _lstObj = lstDataAll;
        //        else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
        //        else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }

        //        if (_lstObj != null && _lstObj.Count > 0)
        //        {
        //            LoadDataHTML(_lstObj);
        //        }
        //        else
        //        {
        //            HTMLDataText.Append("<div style=\"height: 1024px; \">No Data!!!</div>");
        //        }
        //    }
        //}

        //private void MakeTabMenuHedgTicket(List<HedgTicketEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus)
        //{
        //    var ObjLstData = lstDataAll.Where(x => x.ticket_date != null && x.system == ActiveSysTab).ToList();
        //    string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
        //    string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
        //    if (ObjLstData != null)
        //    {
        //        string[] atCount = new string[listTab.Length];
        //        for (int i = 0; i < listTab.Length; i++)
        //        {
        //            if (listStatus[i] == "*")
        //            {
        //                atCount[i] = ObjLstData.Count.ToString();
        //            }
        //            else if (listStatus[i] == ConstantPrm.ACTION.WAITING_CERTIFIED)
        //            {
        //                atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
        //            }
        //            else
        //            {
        //                atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
        //            }
        //        }
        //        MakeTabMenuHTML();
        //        MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
        //        MakeDataActiveStatusHedgTicket(ObjLstData, ActiveStatus);
        //    }
        //}

        //private void MakeDataActiveStatusHedgTicket(List<HedgTicketEncrypt> lstDataAll, string ActiveStatus)
        //{
        //    HTMLDataText.Clear();
        //    if (lstDataAll != null)
        //    {
        //        var _lstObj = new List<HedgTicketEncrypt>();
        //        if (ActiveStatus == "") _lstObj = lstDataAll;
        //        else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
        //        else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
        //        if (_lstObj != null && _lstObj.Count > 0)
        //        {
        //            LoadDataHTML(_lstObj);
        //        }
        //        else
        //        {
        //            HTMLDataText.Append("<div style=\"height: 1024px; \">No Data!!!</div>");
        //        }
        //    }
        //}

        private void MakeTabMenuPaf(List<PAFEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.updated_date != null && x.system == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_VERIFY) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    }
                }

                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }
                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusPaf(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusPaf(List<PAFEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<PAFEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING_VERIFY) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        #region DAF
        private void MakeTabMenuDaf(List<DAFEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.updated_date != null && x.system == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_VERIFY) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    }
                }
                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }

                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusDaf(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusDaf(List<DAFEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<DAFEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING_VERIFY) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        private void LoadDataHTML(List<DAFEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";
            for (int i = 0; i < lstDataByStatus.Count; i++) {
                if (lstDataByStatus[i].created_date.Contains("-")) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].created_date, "dd-MMM-yyyy HH:mm", null, DateTimeStyles.None));
                    lstDataByStatus[i].created_date = datetimeString;
                }
            }

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.created_date != null && lst.created_date != ""
                               select lst.created_date.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.created_date != null && x.created_date.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.created_date).ThenByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Document No</th>");
                _htmlDataHead.Append("<th>Type</th>");
                _htmlDataHead.Append("<th>Crude/Product</th>");
                _htmlDataHead.Append("<th>Vessel</th>");
                _htmlDataHead.Append("<th>Counterparty</th>");
                _htmlDataHead.Append("<th>Created By</th>");
                //_htmlDataHead.Append("<th>Updated By</th>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {
                    var newStatus = _item.status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.status;
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    string a = _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy");
                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.DAF) {
                        Link = "../CPAIMVC/MainApprovalForm/#/demurrage-approval/";
                    }
                    Type = _item.type.ToUpper();
                    Link += _item.user_group + "/" + _item.req_transaction_id + "/" + _item.transaction_id + "/" + "edit";
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}')\">", Link));
                    //_htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy"), newStatus));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.demurrage_type ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.product_crude ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.vessel_name ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.counterparty ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.created_by ?? ""));
                    //_htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.updated_by ?? ""));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }

        #endregion

        #region CDS
        private void MakeTabMenuCds(List<CDSEncrypt> lstDataAll, string ActiveSysTab, string ActiveStatus) {
            var ObjLstData = lstDataAll.Where(x => x.updated_date != null && x.system == ActiveSysTab).ToList();
            string[] listTab = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().tab.SplitWord("|");
            string[] listStatus = gMainBoards.menu.Where(x => x.name == ActiveSysTab).FirstOrDefault().status.SplitWord("|");
            if (ObjLstData != null) {
                string[] atCount = new string[listTab.Length];
                for (int i = 0; i < listTab.Length; i++) {
                    if (listStatus[i] == "*") {
                        atCount[i] = ObjLstData.Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING_VERIFY) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else if (listStatus[i] == ConstantPrm.ACTION.WAITING) {
                        atCount[i] = ObjLstData.Where(x => x.status.IndexOf(listStatus[i]) >= 0).ToList().Count.ToString();
                    } else {
                        atCount[i] = ObjLstData.Where(x => x.status == listStatus[i]).ToList().Count.ToString();
                    }
                }
                if (listStatus != null) {
                    if (ActiveStatus == "all") {
                        ActiveStatus = "";
                    } else if (ActiveStatus == "") {
                        if (listStatus[0] != "*") {
                            ActiveStatus = listStatus[0];
                        }
                    }
                }

                MakeTabMenuHTML();
                MakeStatusMenuDetailHTML(ActiveSysTab, ActiveStatus, listTab, listStatus, atCount);
                MakeDataActiveStatusCds(ObjLstData, ActiveStatus);
            }
        }

        private void MakeDataActiveStatusCds(List<CDSEncrypt> lstDataAll, string ActiveStatus) {
            HTMLDataText.Clear();
            if (lstDataAll != null) {
                var _lstObj = new List<CDSEncrypt>();
                if (ActiveStatus == "") _lstObj = lstDataAll;
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING_VERIFY) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else if (ActiveStatus == ConstantPrm.ACTION.WAITING) _lstObj = lstDataAll.Where(x => x.status.IndexOf(ActiveStatus) >= 0).ToList();
                else { _lstObj = lstDataAll.Where(x => x.status == ActiveStatus).ToList(); }
                if (_lstObj != null && _lstObj.Count > 0) {
                    LoadDataHTML(_lstObj);
                } else {
                    HTMLDataText.Append("<div style=\"height: 1024px; \">No pending work item</div>");
                }
            }
        }

        private void LoadDataHTML(List<CDSEncrypt> lstDataByStatus) {
            HTMLDataText.Clear();
            StringBuilder _htmlDataHead = new StringBuilder();
            StringBuilder _htmlDataData = new StringBuilder();
            string HeadingDiv = "";
            for (int i = 0; i < lstDataByStatus.Count; i++) {
                if (lstDataByStatus[i].created_date.Contains("-")) {
                    string datetimeString = string.Format("{0:dd}/{0:MM}/{0:yyyy}", DateTime.ParseExact(lstDataByStatus[i].created_date, "dd-MMM-yyyy HH:mm", null, DateTimeStyles.None));
                    lstDataByStatus[i].created_date = datetimeString;
                }
            }

            var ObjDistinct = (from lst in lstDataByStatus
                               where lst.created_date != null && lst.created_date != ""
                               select lst.created_date.Substring(2)).Distinct().ToList().OrderByDescending(x => x).ToList();
            var _reorderDate = ReOrderbyDate(ObjDistinct);
            foreach (var _objDate in _reorderDate) {
                var lstByMonth = lstDataByStatus.Where(x => x.created_date != null && x.created_date.IndexOf(_objDate) > 0).ToList().OrderByDescending(x => x.created_date).ThenByDescending(x => x.transaction_id).ToList();
                _htmlDataHead.Clear();
                _htmlDataData.Clear();
                //-----------------Div-----------------
                HeadingDiv = string.Format("<h2 class=\"title\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#{0}\" aria-expanded=\"true\" aria-controls=\"{0}\">{1} <span>({2} Task)</span></h2>", _objDate.Replace("/", ""), _FN.ConvertDateFormatBackFormat("01" + _objDate, "MMMM yyyy"), lstByMonth.Count);
                HeadingDiv += string.Format("<div class=\"wrapper-table collapse in\" id=\"{0}\">", _objDate.Replace("/", ""));

                //-----------------Header-----------------
                _htmlDataHead.Append("<table class=\"table\">");
                _htmlDataHead.Append("<thead><tr><th>Date/Status</th>");
                _htmlDataHead.Append("<th>Document No</th>");
                _htmlDataHead.Append("<th>Type</th>");
                _htmlDataHead.Append("<th>Crude/Product</th>");
                _htmlDataHead.Append("<th>Vessel</th>");
                _htmlDataHead.Append("<th>Counterparty</th>");
                _htmlDataHead.Append("<th>Created By</th>");
                //_htmlDataHead.Append("<th>Updated By</th>");

                //-----------------Data-----------------
                _htmlDataData.Append("<tbody>");
                string Link = "";
                string Type = "";
                foreach (var _item in lstByMonth) {
                    var newStatus = _item.status == "WAITING CERTIFIED" ? "WAITING ENDORSED" : _item.status;
                    DateTime result = DateTime.ParseExact("2012-04-05", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    string a = _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy");
                    if (_item.system.ToUpper() == ConstantPrm.SYSTEM.CDS) {
                        Link = "../CPAIMVC/MainApprovalForm/#/crude-sale-approval/";
                    }
                    Type = _item.type.ToUpper();
                    Link += _item.req_transaction_id + "/" + _item.transaction_id + "/" + "edit";
                    _htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}')\">", Link));
                    //_htmlDataData.Append(string.Format("<tr onclick=\"OpenLinkLocaltion('{0}?TranID={1}&Tran_Req_ID={2}&PURNO={3}&Reason={4}&Type={5}')\">", Link, _item.transaction_id.Encrypt(), _item.req_transaction_id.Encrypt(), _item.purchase_no.Encrypt(), _item.reason.Encrypt(), Type.Encrypt()));
                    _htmlDataData.Append(string.Format("<td width=\"15%\">{0}<span>{1}</span></td>", _FN.ConvertDateFormatBackFormat(_item.date_purchase, "MMMM dd yyyy"), newStatus));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.purchase_no));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.demurrage_type ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.product_crude ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.vessel_name ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"8%\">{0}</td>", _item.counterparty ?? ""));
                    _htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.created_by ?? ""));
                    //_htmlDataData.Append(string.Format("<td width=\"9%\">{0}</td>", _item.updated_by ?? ""));
                    _htmlDataData.Append("</tr>");
                }
                _htmlDataData.Append("</tbody>");

                //-----------------Finish-----------------
                HTMLDataText.Append(HeadingDiv);
                HTMLDataText.Append(_htmlDataHead);
                HTMLDataText.Append(_htmlDataData);
                HTMLDataText.Append("</table>");
                HTMLDataText.Append("</div>");
            }
        }
        #endregion
    }
}