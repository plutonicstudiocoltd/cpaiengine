﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.DAL.DALBunker;
using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class CharteringInCMMTReport : System.Web.UI.Page
    {

        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        MigraDoc.DocumentObjectModel.Tables.Table table;
        Color TableBorder;
        Color TableGray;
        const int defaultHeight = 10;
        bool isWS = false;

        int FontSize = 8;
        string URL = "";
        List<string> images = new List<string>();
        string FontName = "Tahoma";
        string type = ConstantPrm.SYSTEMTYPE.VESSEL;
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadTest("201703131705000082689");
            //LoadDataCMCS("201703021101300079174");
            //LoadXMLFromTest();
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
            }
            if (Request.QueryString["TranID"] != null)
            {
                LoadDataCharter(Request.QueryString["TranID"].ToString().Decrypt());
            }
            else
            {
                if (Session["ChitRootObject"] != null)
                {
                    ChitRootObject _model = (ChitRootObject)Session["ChitRootObject"];
                    this.updateModel(_model);
                    GenPDF(_model, true, Request.QueryString["TranID"].ToString().Decrypt());
                    Session["ChitRootObject"] = null;
                }
            }
        }

        private void LoadXMLFromTest()
        {
            string PathTest = @"D:\Users\aekkasits\Desktop\CHI CMMT\Data_json_charter.txt";
            if (File.Exists(PathTest))
            {
                StreamReader _sr = new StreamReader(PathTest);
                string Line = _sr.ReadToEnd();
                //Line = Line.Replace("null", "\"\"");
                ChitRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChitRootObject>(Line);
                // GenPDF(_modelReport, true);
            }
        }

        private void LoadTest(string TranID)
        {
            LoadData(TranID);
        }

        public string LoadData(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";

            string _model = CharterInServiceModel.getTransactionCMMTByID(TransactionID);
            if (_model != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                //_model = _model.Replace("null", "\"\"");
                ChitRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChitRootObject>(result);
                pdfPath = GenPDF(_modelReport, redirect, TransactionID);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, "Error", "MainBoards.aspx");
            }
            return pdfPath;
        }

        private void updateModel(ChitRootObject model, bool isTextMode = true)
        {
            //Assign loadport order and discharge port order

            List<SelectListItem> port = DropdownServiceModel.getPortName(true, "", "PORT_CMMT");
            List<SelectListItem> crude = DropdownServiceModel.getMaterial(true, "", "MAT_CMMT");
            List<SelectListItem> suplier = DropdownServiceModel.getVendorFreight("CHISUPMT", true);


            //Assign round_no, order no., order type and final flag
            List<SelectListItem> broker = DropdownServiceModel.getVendorFreight("CHIBROMT", true);
            List<SelectListItem> vehicle = DropdownServiceModel.getVehicle("CHIVESMT", true);
            List<SelectListItem> vendor = DropdownServiceModel.getVendorFreight("CHISUPMT", true);
            List<SelectListItem> cust = DropdownServiceModel.getCustomer("CHR_FRE").OrderBy(x => x.Text).ToList();
            string OwnerName = "";
            if (model.chit_negotiation_summary != null)
            {
                for (int i = 0; i < model.chit_negotiation_summary.chit_offers_items.Count; i++)
                {
                    if (isTextMode)
                    {
                        model.chit_negotiation_summary.chit_offers_items[i].broker_init = MT_VENDOR_DAL.GetNameShort(model.chit_negotiation_summary.chit_offers_items[i].broker);
                        model.chit_negotiation_summary.chit_offers_items[i].broker = broker.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].broker).FirstOrDefault().Text;

                        //model.chit_negotiation_summary.chit_offers_items[i].vessel = vehicle.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].vessel).FirstOrDefault() == null ? "" : vehicle.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].vessel).FirstOrDefault().Text;
                        model.chit_negotiation_summary.chit_offers_items[i].vessel = model.chit_negotiation_summary.chit_offers_items[i].vessel;
                        //model.chit_negotiation_summary.chit_offers_items[i].owner = vendor.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].owner).FirstOrDefault() == null ? "" : vendor.Where(x => x.Value == model.chit_negotiation_summary.chit_offers_items[i].owner).FirstOrDefault().Text;
                        model.chit_negotiation_summary.chit_offers_items[i].owner = model.chit_negotiation_summary.chit_offers_items[i].owner;
                        if (model.chit_negotiation_summary.chit_offers_items[i].final_flag.ToUpper() == "Y") OwnerName = model.chit_negotiation_summary.chit_offers_items[i].owner;
                    }
                }
            }

            if (isTextMode)
            {
                //model.chit_proposed_for_approve.charterer_broker = broker.Where(x => x.Value == model.chit_proposed_for_approve.charterer_broker).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chit_proposed_for_approve.charterer_broker).FirstOrDefault().Text;
                //model.chit_proposed_for_approve.broker_name = broker.Where(x => x.Value == model.chit_proposed_for_approve.broker_name).FirstOrDefault() == null ? "" : broker.Where(x => x.Value == model.chit_proposed_for_approve.broker_name).FirstOrDefault().Text;
                //model.chit_proposed_for_approve.vessel_name = vehicle.Where(x => x.Value == model.chit_proposed_for_approve.vessel_name).FirstOrDefault() == null ? "" : vehicle.Where(x => x.Value == model.chit_proposed_for_approve.vessel_name).FirstOrDefault().Text;
                model.chit_proposed_for_approve.vessel_name = model.chit_proposed_for_approve.vessel_name;
                model.chit_detail.charterer = cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault() == null ? "" : cust.Where(x => x.Value == model.chit_detail.charterer).FirstOrDefault().Text;
                model.chit_proposed_for_approve.owner = OwnerName;

            }
        }

        public string LoadDataCharter(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";
            RequestData reqData = new RequestData();
            reqData.function_id = ConstantPrm.FUNCTION.F10000005;
            reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();
            reqData.state_name = "";
            reqData.req_parameters = new req_parameters();
            reqData.req_parameters.p = new List<p>();
            reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            reqData.req_parameters.p.Add(new p { k = "user", v = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
            reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
            reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.VESSEL });
            reqData.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(reqData);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model.data_detail, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");

                ChitRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChitRootObject>(result);
                using (EntityCPAIEngine entity = new EntityCPAIEngine())
                {
                    var chit_date = entity.CHIT_DATA.SingleOrDefault(a => a.IDAT_ROW_ID == TransactionID);
                    if(chit_date != null)
                    {
                        _modelReport.chit_negotiation_summary.select_charter_for = chit_date.IDAT_CHARTER_FOR;
                        if(!String.IsNullOrEmpty(_modelReport.chit_negotiation_summary.select_charter_for))
                        {
                            _modelReport.chit_negotiation_summary.select_charter_for = _modelReport.chit_negotiation_summary.select_charter_for.Substring(0, 4);
                        }
                    }
                }
                    if (_modelReport.chit_negotiation_summary.chit_offers_items.Count <= 0) _modelReport.chit_negotiation_summary.chit_offers_items.Add(new ChitOffersItem());
                this.updateModel(_modelReport);
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelReport, redirect, TransactionID);
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }
            return pdfPath;
        }

        private string GenPDF(ChitRootObject modelReport, bool redirect, string tranID)
        {
            Document document = CreateDocument(modelReport, tranID);

            document.UseCmykColor = true;
            const bool unicode = true;
            //const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            //string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CHRT{0}-{1}-{2}.pdf", type, DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            string vessel = modelReport.chit_negotiation_summary.chit_offers_items.Where(x => x.final_flag == "Y").FirstOrDefault().vessel;
            string[] arr_date = { };
            if (string.IsNullOrEmpty(modelReport.chit_detail.date))
            {
                arr_date = DateTime.Now.ToString("dd/MM/yyyy").SplitWord("/");
            }
            else
            {
                arr_date = modelReport.chit_detail.date.SplitWord("/");
            }

            if (string.IsNullOrEmpty(vessel))
            {
                vessel = "VESSEL";
            }
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("CHRT{0}-{1}-{2}.pdf", arr_date[2], vessel, arr_date[0] + "-" + arr_date[1] + "-" + arr_date[2]));
            pdfRenderer.PdfDocument.Save(filename);
            if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        public Document CreateDocument(ChitRootObject modelReport, string tranID)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Charter In (CMMT)";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelReport, tranID);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage(ChitRootObject modelReport, string tranID)
        {
            Section section = this.document.AddSection();
            //HeadingReport(section, modelReport);
            int StartOfferIndex = 0;
            while (StartOfferIndex + 1 <= modelReport.chit_negotiation_summary.chit_offers_items[0].chit_round_items.Count)
            {
                if (StartOfferIndex > 0) document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport);
                ReasonExplanationReport(section, modelReport);
                ReasonTraderReport(section, modelReport, tranID);
                OfferTableZone(section, modelReport, StartOfferIndex);
                BottomZoneReport(section, modelReport);
                Note(section, modelReport, tranID);
                FooterReport(section, modelReport);
                //if (StartOfferIndex == 0) FooterReport(section, modelReport);

                StartOfferIndex += 5;
            }


            //OfferTableZone(section, modelReport, StartOfferIndex);
            //BottomZoneReport(section, modelReport);

            //FooterReport(section, modelReport);
            if (!string.IsNullOrEmpty(modelReport.explanationAttach))
            {
                document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport, true);
                ExplanImageReport(section, modelReport);
            }

            if (!string.IsNullOrEmpty(tranID))
            {
                using (var context = new EntityCPAIEngine())
                {
                    var traATT = context.CHIT_ATTACH_FILE.Where(a => a.ITAF_FK_CHIT_DATA == tranID && a.ITAF_TYPE == "TRA").ToList();
                    foreach (var z in traATT)
                    {
                        modelReport.explanationAttachTrader += z.ITAF_PATH + ":" + z.ITAF_INFO + "|";
                    }
                    if (!string.IsNullOrEmpty(modelReport.explanationAttachTrader))
                    {
                        document.LastSection.AddPageBreak();
                        HeadingReport(section, modelReport, true);
                        ExplanImageReportTrader(section, modelReport);
                    }
                }                
            }
            

            //-------------Table Offer-------------

            //-------------------------------------

        }
        Setting _setting = JSONSetting.getSetting("JSON_CHARTER_IN_CRUDE");
        private void HeadingReport(Section section, ChitRootObject modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 1;
            this.table.Rows.Height = 20;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow();
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;


            int icell = 0;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            // Header
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].AddParagraph("Spot Chartering In");
            row.Cells[icell].Format.Font.Size = 10;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            // Logo
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

            // For Vessel
            row.Cells[icell].AddParagraph("For Vessel : " + nullToEmtry(modelReport.chit_proposed_for_approve.vessel_name.ToUpper()));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            //row.Cells[icell].AddParagraph(modelReport.chit_proposed_for_approve.vessel_name.ToUpper());
            //row.Cells[icell].Format.LeftIndent = new Unit(1, UnitType.Millimeter);
            //row.Cells[icell].MergeRight = 4;
            //icell += row.Cells[icell].MergeRight + 1;

            // Charter Party Date
            row.Cells[icell].AddParagraph("Charterer : " + nullToEmtry(modelReport.chit_detail.charterer));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].MergeRight = 7;
            icell += row.Cells[icell].MergeRight + 1;


            // On Subject Date
            row.Cells[icell].AddParagraph("On Subject Date : " + ((string.IsNullOrEmpty(modelReport.chit_detail.date)) ? "-" : _FN.ConvertDateFormatBackFormat(modelReport.chit_detail.date, "dd MMM yyyy")));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;

        }

        private void ReasonExplanationReport(Section section, ChitRootObject modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 1;
            this.table.Rows.Height = 20;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            Row row = table.AddRow();
            int icell = 0;

            //row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Height = 16;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Reason for Chartering In");
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;

            if (modelReport.chit_detail.Reason != "")
            {

                row = table.AddRow(); icell = 0;
                row.HeadingFormat = true;
                row.Format.Font.Size = FontSize - 1;
                row.Height = 18;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Top;
                Paragraph _prg = row.Cells[0].AddParagraph();
                //_prg.AddFormattedText(HtmlRemoval.ValuetoParagraph(modelReport.explanation));
                htmlRemove.ValuetoParagraph(modelReport.chit_detail.Reason, _prg);
                row.Cells[icell].MergeRight = 18;
                row.Cells[icell].Borders.Top.Color = Colors.Transparent;

            }
            else
            {
                row = table.AddRow(); icell = 0;
                row.HeadingFormat = true;
                row.Format.Font.Size = FontSize - 1;
                row.Height = 18;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Top;
                row.Cells[0].AddParagraph("");
                row.Cells[icell].MergeRight = 18;
                row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            }

        }

        private void ReasonTraderReport(Section section, ChitRootObject modelReport, string tranID)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 1;
            this.table.Rows.Height = 20;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            Row row = table.AddRow();
            int icell = 0;

            //row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Height = 16;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Comment from Trader");
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;

            using (EntityCPAIEngine context = new EntityCPAIEngine())
            {
                var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_ROW_ID == tranID);
                if (chitDetail != null)
                {
                    row = table.AddRow(); icell = 0;
                    row.HeadingFormat = true;
                    row.Format.Font.Size = FontSize - 1;
                    row.Height = 18;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Top;
                    Paragraph _prg = row.Cells[0].AddParagraph();
                    htmlRemove.ValuetoParagraph(chitDetail.IDAT_CHARTER_FOR_CMMENT, _prg);
                    row.Cells[icell].MergeRight = 18;
                    row.Cells[icell].Borders.Top.Color = Colors.Transparent;
                }
                else
                {
                    row = table.AddRow(); icell = 0;
                    row.HeadingFormat = true;
                    row.Format.Font.Size = FontSize - 1;
                    row.Height = 18;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Top;
                    row.Cells[0].AddParagraph("");
                    row.Cells[icell].MergeRight = 18;
                    row.Cells[icell].Borders.Top.Color = Colors.Transparent;
                }
            }
        }

        private void Note(Section section, ChitRootObject modelReport, string tranID)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 1;
            this.table.Rows.Height = 20;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            Row row = table.AddRow();
            int icell = 0;

            //row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Height = 16;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Note");
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;

           // using (EntityCPAIEngine context = new EntityCPAIEngine())
            //{
                //var chitDetail = context.CHIT_DATA.SingleOrDefault(a => a.IDAT_ROW_ID == tranID);
                if (!String.IsNullOrEmpty(modelReport.chit_note))
                {
                    row = table.AddRow(); icell = 0;
                    row.HeadingFormat = true;
                    row.Format.Font.Size = FontSize - 1;
                    row.Height = 18;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Top;
                    Paragraph _prg = row.Cells[0].AddParagraph();
                    htmlRemove.ValuetoParagraph(modelReport.chit_note, _prg);
               // row.Cells[icell].AddParagraph(modelReport.chit_note);
                row.Cells[icell].MergeRight = 18;
                    row.Cells[icell].Borders.Top.Color = Colors.Transparent;
                }
                else
                {
                    row = table.AddRow(); icell = 0;
                    row.HeadingFormat = true;
                    row.Format.Font.Size = FontSize - 1;
                    row.Height = 18;
                    row.Format.Font.Bold = true;
                    row.Format.Alignment = ParagraphAlignment.Left;
                    row.VerticalAlignment = VerticalAlignment.Top;
                    row.Cells[0].AddParagraph("");
                    row.Cells[icell].MergeRight = 18;
                    row.Cells[icell].Borders.Top.Color = Colors.Transparent;
                }
            //}
        }

        HtmlRemoval htmlRemove = new HtmlRemoval();
        private void OfferTableZone(Section section, ChitRootObject modelReport, int itemOfferIndex)
        {

            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 2;
            this.table.Rows.Height = defaultHeight;
            this.table.Format.Alignment = ParagraphAlignment.Center;

            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }
            #region--------HeaddingTable-------------


            Row row = table.AddRow();
            row.Cells[0].AddParagraph("Negotiation Summary");
            row.Cells[0].Row.Height = 16;
            row.Cells[0].Format.Font.Bold = true;
            row.Cells[0].Format.Font.Size = 8;
            row.Cells[0].MergeRight = 37;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            //row = table.AddRow();
            //1 -> 16, 7
            //2 -> 8, 5
            //3 -> 6, 5
            //4 -> 4, 7
            //5 -> 4, 3
            int offer_count = 0;
            int offer_width = 0;
            int fixing_width = 0;
            if (itemOfferIndex + 5 <= modelReport.chit_negotiation_summary.chit_offers_items[0].chit_round_items.Count)
            {
                offer_count = 5;
            }
            else
            {
                offer_count = modelReport.chit_negotiation_summary.chit_offers_items[0].chit_round_items.Count % 5;
            }
            switch (offer_count)
            {
                case 1: offer_width = 16; fixing_width = 7; break;
                case 2: offer_width = 8; fixing_width = 7; break;
                case 3: offer_width = 6; fixing_width = 5; break;
                case 4: offer_width = 4; fixing_width = 7; break;
                case 5: offer_width = 4; fixing_width = 3; break;
                default: offer_width = 4; fixing_width = 3; break;
            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("Broker");
            row.Cells[icell].MergeRight = 2;
            row.Cells[0].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Targeted Vessel");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            int icellQ = icell;
            row.Cells[icell].AddParagraph("Owner");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            int icelR2 = icell;
            //int ttSt = (2 * (Quantity.Count - 1));
            row.Cells[icell].AddParagraph("Offer and Counter");
            row.Cells[icell].MergeRight = 22;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Final Deal");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;

            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Fixing Items");
            row.Cells[icell].MergeRight = fixing_width - 1;
            row.Cells[icell].MergeDown = 1;
            icell += row.Cells[icell].MergeRight + 1;
            icelR2 = icell;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph(string.Format("{0}", _FN.GetORDINALNUMBERS(itemOfferIndex + (i + 1))));
                row.Cells[icell].MergeRight = offer_width - 1;
                if (i != offer_count - 1)
                    icell += row.Cells[icell].MergeRight + 1;
            }

            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph("Offer");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("Counter");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
            }

            #endregion----------------------------------

            if (modelReport.chit_negotiation_summary.chit_offers_items.Count > 0)
            {
                modelReport.chit_negotiation_summary.chit_offers_items = modelReport.chit_negotiation_summary.chit_offers_items.OrderByDescending(p => p.final_flag).ToList();
            }
            //Loop Bind Data
            for (int i = 0; i < modelReport.chit_negotiation_summary.chit_offers_items.Count; i++)
            {

                var _offerItem = modelReport.chit_negotiation_summary.chit_offers_items[i];
                int MergeDown = _offerItem.chit_round_items[itemOfferIndex].chit_round_info.Count < 3 ? 2 : _offerItem.chit_round_items[itemOfferIndex].chit_round_info.Count - 1;
                icell = 0;
                row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.broker_init, 3));
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown = MergeDown;
                int vesselcol = row.Cells[icell].MergeRight + 1;
                icell += row.Cells[icell].MergeRight + 4;

                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.owner, 3));
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].MergeDown = MergeDown;
                icell += row.Cells[icell].MergeRight + 1;
                int rcell = icell; bool firstrow = true; string tmpVal = "";
                int count = 0;
                foreach (var item in _offerItem.chit_round_items[itemOfferIndex].chit_round_info)
                {
                    tmpVal = "";
                    if (!firstrow)
                    {
                        row = table.AddRow();
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                    }

                    if (count == 0)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.vessel, 4));
                        row.Cells[vesselcol].MergeRight = 2;
                        row.Cells[vesselcol].MergeDown = MergeDown - 2;
                        count++;
                    }
                    else if (count == (MergeDown - 1))
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], "Year : " + _offerItem.year, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (count == MergeDown)
                    {
                        string dwt = _offerItem.KDWT;
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], "DWT : " + (string.IsNullOrEmpty(dwt) ? "" : _FN.intTostrMoney(dwt)), 3));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else
                    {
                        row.Cells[vesselcol].AddParagraph("");
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }


                    icell = rcell;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], (item.type.ToUpper() == "OTHERS") ? item.typeOther : item.type, fixing_width - 1));
                    row.Cells[icell].MergeRight = fixing_width - 1;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    icell += row.Cells[icell].MergeRight + 1;

                    for (int j = 0; j < offer_count; j++)
                    {
                        tmpVal = _offerItem.chit_round_items[itemOfferIndex + j].chit_round_info.Where(x => x.type == item.type).First().offer;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal, false, "-"), (offer_width / 2)));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                        tmpVal = _offerItem.chit_round_items[itemOfferIndex + j].chit_round_info.Where(x => x.type == item.type).First().counter;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal, false, "-"), (offer_width / 2)));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                    if (firstrow)
                    {
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.final_deal.Replace("#", "\n"), 5));
                        row.Cells[icell].Format.Font.Size = FontSize - 3;
                        row.Cells[icell].MergeRight = 5;
                        row.Cells[icell].MergeDown = MergeDown;
                        firstrow = false;
                    }

                }

                for (int j = count; j < 3; j++)
                {
                    if (j >= 1)
                    {
                        row = table.AddRow();
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                    }
                    if (j == 0)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.vessel, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (j == 1)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _offerItem.year, 2));
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }
                    else if (j == 2)
                    {
                        row.Cells[vesselcol].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[vesselcol], _FN.intTostrMoney(_offerItem.KDWT), 3)).AddFormattedText(" DWT", TextFormat.Bold);
                        row.Cells[vesselcol].MergeRight = 2;
                        count++;
                    }


                    icell = rcell;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", fixing_width - 1));
                    row.Cells[icell].MergeRight = fixing_width - 1;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    icell += row.Cells[icell].MergeRight + 1;

                    for (int k = 0; k < offer_count; k++)
                    {
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", 2));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], "", 2));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                }

            }


        }

        private void BottomZoneReport(Section section, ChitRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.TopPadding = 2;
            this.table.BottomPadding = 2;
            this.table.Rows.Height = 18;
            this.table.Borders.Color = Color.Empty;

            this.table.AddColumn("3cm");
            this.table.AddColumn("4cm");

            this.table.AddColumn("3cm");
            this.table.AddColumn("4cm");

            this.table.AddColumn("2.5cm");
            this.table.AddColumn("2.5cm");

            int icell = 0;
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].AddParagraph("Proposal for approval");
            row.Cells[icell].Format.Font.Size = 8;
            row.Cells[icell].Row.Height = 16;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            List<string> list_key_Column1 = new List<string>();
            List<string> list_value_Column1 = new List<string>();
            list_key_Column1.Add("Charterer : ");
            list_value_Column1.Add(nullToEmtry(modelReport.chit_detail.charterer));
            list_key_Column1.Add("Owner : ");
            list_value_Column1.Add(nullToEmtry(modelReport.chit_proposed_for_approve.owner));
            list_key_Column1.Add("Charter Party Form : ");
            list_value_Column1.Add(nullToEmtry(modelReport.chit_proposed_for_approve.charter_patry));
            list_key_Column1.Add("Vessel Name : ");
            list_value_Column1.Add(nullToEmtry(modelReport.chit_proposed_for_approve.vessel_name));
            list_key_Column1.Add("Loading Laycan : ");
            if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.laycan_from) && string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.laycan_to))
            {
                list_value_Column1.Add("-");
            }
            else
            {
                list_value_Column1.Add(string.Format("{0} to {1}", modelReport.chit_proposed_for_approve.laycan_from, modelReport.chit_proposed_for_approve.laycan_to));
            }

            if (modelReport.chit_detail.chit_cargo_qty.Count == 1)
            {
                list_key_Column1.Add("Cargo : ");
                list_value_Column1.Add(getCarogNameById(modelReport.chit_detail.chit_cargo_qty[0].cargo));
                list_key_Column1.Add("Quantity : ");
                list_value_Column1.Add(_FN.intTostrMoney(modelReport.chit_detail.chit_cargo_qty[0].qty) + " " + (modelReport.chit_detail.chit_cargo_qty[0].unit == "OTHERS" ? modelReport.chit_detail.chit_cargo_qty[0].Others : modelReport.chit_detail.chit_cargo_qty[0].unit));
                list_key_Column1.Add("Tolerance (+/-) : ");
                list_value_Column1.Add(string.IsNullOrEmpty(modelReport.chit_detail.chit_cargo_qty[0].tolerance) ? "-" : modelReport.chit_detail.chit_cargo_qty[0].tolerance + " %");
            }
            else
            {
                for (int i = 0; i < modelReport.chit_detail.chit_cargo_qty.Count; i++)
                {
                    list_key_Column1.Add("Cargo " + (i + 1) + " : ");
                    list_value_Column1.Add(getCarogNameById(modelReport.chit_detail.chit_cargo_qty[i].cargo));
                    list_key_Column1.Add("Quantity " + (i + 1) + " : ");
                    list_value_Column1.Add(_FN.intTostrMoney(modelReport.chit_detail.chit_cargo_qty[i].qty) + " " + (modelReport.chit_detail.chit_cargo_qty[i].unit == "OTHERS" ? modelReport.chit_detail.chit_cargo_qty[i].Others : modelReport.chit_detail.chit_cargo_qty[i].unit));
                    list_key_Column1.Add("Tolerance " + (i + 1) + " (+/-) : ");
                    list_value_Column1.Add(string.IsNullOrEmpty(modelReport.chit_detail.chit_cargo_qty[i].tolerance) ? "-" : modelReport.chit_detail.chit_cargo_qty[i].tolerance + " %");
                }
            }

            list_key_Column1.Add("BSS : ");
            list_value_Column1.Add(modelReport.chit_detail.bss);
            if (modelReport.chit_detail.chit_load_port_control != null)
            {
                if (modelReport.chit_detail.chit_load_port_control.Count == 1)
                {
                    list_key_Column1.Add("Load Port : ");
                    list_value_Column1.Add(getNewLine(modelReport.chit_detail.chit_load_port_control[0].PortName, 27));
                }
                else
                {
                    for (int i = 0; i < modelReport.chit_detail.chit_load_port_control.Count; i++)
                    {
                        list_key_Column1.Add("Load Port " + (i + 1) + " : ");
                        list_value_Column1.Add(getNewLine(modelReport.chit_detail.chit_load_port_control[i].PortName, 27));
                    }
                }

            }

            if (modelReport.chit_detail.chit_discharge_port_control != null)
            {
                if (modelReport.chit_detail.chit_discharge_port_control.Count == 1)
                {
                    list_key_Column1.Add("Discharge Port : ");
                    list_value_Column1.Add(getNewLine(modelReport.chit_detail.chit_discharge_port_control[0].PortName, 27));
                }
                else
                {
                    for (int i = 0; i < modelReport.chit_detail.chit_discharge_port_control.Count; i++)
                    {
                        list_key_Column1.Add("Discharge Port " + (i + 1) + " : ");
                        list_value_Column1.Add(getNewLine(modelReport.chit_detail.chit_discharge_port_control[i].PortName, 27));
                    }
                }
            }

            List<string> list_key_Column2 = new List<string>();
            List<string> list_value_Column2 = new List<string>();
            string tt = modelReport.chit_negotiation_summary.chit_offers_items[0].chit_round_items[0].chit_round_info[0].type;

            string unit_freight = "";
            string unit_print = "";
            for (int i = 0; i < modelReport.chit_negotiation_summary.chit_offers_items.Count; i++)
            {
                if (modelReport.chit_negotiation_summary.chit_offers_items[i].final_flag == "Y")
                {
                    for (int j = 0; j < modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info.Count; j++)
                    {
                        if (modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[j].type == "OTHERS")
                        {
                            unit_freight = modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[j].typeOther;
                            unit_print = modelReport.chit_detail.NegoUnit;
                        }
                        else
                        {
                            if (modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[j].type.Contains("Freight"))
                            {
                                if (modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[j].type.Contains("Lumpsum"))
                                {
                                    unit_freight = modelReport.chit_detail.NegoUnit;
                                    unit_print = modelReport.chit_detail.NegoUnit;
                                }
                                else
                                {
                                    // unit_freight = modelReport.chit_negotiation_summary.chit_offers_items[i].chit_round_items[0].chit_round_info[j].type.SplitWord("(")[1].SplitWord(")")[0];
                                    // unit_print = unit_freight.SplitWord("/")[0];
                                }
                            }
                        }
                    }
                }
            }
            unit_freight = modelReport.chit_detail.NegoUnit;
            list_key_Column2.Add("Freight : ");
            list_value_Column2.Add(_FN.intTostrMoney(modelReport.chit_proposed_for_approve.freight) + " " + unit_freight);

            if (!string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.total_freight))
            {
                list_key_Column2.Add("Total Freight : ");
                list_value_Column2.Add(_FN.intTostrMoney(modelReport.chit_proposed_for_approve.total_freight) + " " + unit_print);
            }

            list_key_Column2.Add("Laytime : ");
            list_value_Column2.Add(modelReport.chit_proposed_for_approve.laytime + " Hrs");
            list_key_Column2.Add("Demurrage : ");
            list_value_Column2.Add(_FN.intTostrMoney(modelReport.chit_proposed_for_approve.demurrage) + " " + unit_print + " PDPR");
            list_key_Column2.Add("Payment Term : ");
            list_value_Column2.Add(nullToEmtry(modelReport.chit_proposed_for_approve.payment_term));
            list_key_Column2.Add("Charterer's Broker : \nName");
            list_value_Column2.Add(nullToEmtry(modelReport.chit_proposed_for_approve.charterer_broker));

            list_key_Column2.Add("Charterer's Broker : \nCommission");
            if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.charterer_brokerCom) || modelReport.chit_proposed_for_approve.charterer_brokerCom == "N/A")
            {
                if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.charterer_brokerCom))
                {
                    list_value_Column2.Add("-");
                }
                else
                {
                    list_value_Column2.Add(modelReport.chit_proposed_for_approve.charterer_brokerCom);
                }
            }
            else
            {
                list_value_Column2.Add(modelReport.chit_proposed_for_approve.charterer_brokerCom + " %");
            }

            list_key_Column2.Add("Owner's Broker : \nName");
            list_value_Column2.Add(nullToEmtry(modelReport.chit_proposed_for_approve.broker_name));

            list_key_Column2.Add("Owner's Broker : \nCommission");
            if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.broker_commission) || modelReport.chit_proposed_for_approve.broker_commission == "N/A")
            {
                if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.broker_commission))
                {
                    list_value_Column2.Add("-");
                }
                else
                {
                    list_value_Column2.Add(modelReport.chit_proposed_for_approve.broker_commission);
                }
            }
            else
            {
                list_value_Column2.Add(modelReport.chit_proposed_for_approve.broker_commission + " %");
            }

            list_key_Column2.Add("Address : \nCommission");
            if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.address_commission) || modelReport.chit_proposed_for_approve.address_commission == "N/A")
            {
                if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.address_commission))
                {
                    list_value_Column2.Add("-");
                }
                else
                {
                    list_value_Column2.Add(modelReport.chit_proposed_for_approve.address_commission);
                }
            }
            else
            {
                list_value_Column2.Add(modelReport.chit_proposed_for_approve.address_commission + " %");
            }

            list_key_Column2.Add("Withholding Tax : ");
            if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.withholding_tax) || modelReport.chit_proposed_for_approve.withholding_tax == "N/A")
            {

                if (string.IsNullOrEmpty(modelReport.chit_proposed_for_approve.withholding_tax))
                {
                    list_value_Column2.Add("-");
                }
                else
                {
                    list_value_Column2.Add(modelReport.chit_proposed_for_approve.withholding_tax);
                }
            }
            else
            {
                list_value_Column2.Add(modelReport.chit_proposed_for_approve.withholding_tax + " %");
            }
            list_key_Column2.Add("Net Freight : ");
            if(String.IsNullOrEmpty(modelReport.chit_proposed_for_approve.net_freight))
            {
                list_value_Column2.Add("-");
            }
            else
            {
                list_value_Column2.Add(_FN.intTostrMoney(modelReport.chit_proposed_for_approve.net_freight) + " " + unit_freight);
            }
            

            List<string> list_key_Column3 = new List<string>();
            List<string> list_value_Column3 = new List<string>();
            list_key_Column3.Add("Vessel's Year Built : ");
            list_value_Column3.Add(nullToEmtry(modelReport.chit_proposed_for_approve.vessel_built));
            list_key_Column3.Add("DWT : ");
            if (modelReport.chit_negotiation_summary.chit_offers_items.Where(x => x.final_flag.ToUpper() == "Y").ToList().Count > 0)
            {
                string dwt = modelReport.chit_negotiation_summary.chit_offers_items.Where(x => x.final_flag.ToUpper() == "Y").ToList()[0].KDWT;
                list_value_Column3.Add(string.IsNullOrEmpty(dwt) ? "-" : _FN.intTostrMoney(dwt));
            }
            else
            {
                list_value_Column3.Add("-");
            }
            list_key_Column3.Add("Other : ");
            list_value_Column3.Add(nullToEmtry(modelReport.chit_proposed_for_approve.Other));

            int[] arr_row = { list_key_Column1.Count, list_key_Column2.Count, list_key_Column3.Count };
            for (int i = 0; i < arr_row.Max(); i++)
            {
                row = table.AddRow(); icell = 0;
                row.Format.Font.Size = FontSize - 1;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Top;

                row.Cells[icell].Borders.Left.Color = Colors.Black;
                row.Cells[icell].AddParagraph(getValueFromList(list_key_Column1, list_key_Column1.Count, i));
                row.Cells[icell].Format.Font.Bold = true;
                icell += row.Cells[icell].MergeRight + 1;
                string value1 = getValueFromList(list_value_Column1, list_value_Column1.Count, i);
                row.Cells[icell].AddParagraph(value1 == null ? "" : value1);
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(getValueFromList(list_key_Column2, list_key_Column2.Count, i));
                row.Cells[icell].Format.Font.Bold = true;
                icell += row.Cells[icell].MergeRight + 1;
                string value2 = getValueFromList(list_value_Column2, list_value_Column2.Count, i);
                row.Cells[icell].AddParagraph(value2);
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(getValueFromList(list_key_Column3, list_key_Column3.Count, i));
                row.Cells[icell].Format.Font.Bold = true;
                icell += row.Cells[icell].MergeRight + 1;
                string value3 = getValueFromList(list_value_Column3, list_value_Column3.Count, i);
                row.Cells[icell].AddParagraph(value3);
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[icell].Borders.Right.Color = Colors.Black;
            }
        }

        private string getValueFromList(List<string> paStr, int pnMax, int pnIdx)
        {
            if (pnMax > pnIdx)
            {
                return paStr[pnIdx];
            }
            else
            {
                return "";
            }
        }

        private string getNewLine(string str, int lenght)
        {
            string rs = "";
            int iii = str.Length / lenght;
            for (int i = 0; i < iii; i++)
            {
                rs += str.Substring(lenght * i, lenght) + "\n";
            }
            rs += str.Substring((lenght * iii), (str.Length % lenght));
            return rs;
        }

        private ParagraphAlignment getParagraphAlignment(string value)
        {
            double dd = 0;
            if (double.TryParse(value, out dd))
            {
                return ParagraphAlignment.Right;
            }
            else
            {
                if (value == "FO" || value == "MGO" || value == "-")
                {
                    return ParagraphAlignment.Center;
                }
                else
                {
                    return ParagraphAlignment.Left;
                }
            }
        }

        private string GetStrCLD(int index, List<ItemCDL> lstLSD, bool GetData = false)
        {
            string RetVal = "";
            if (index < lstLSD.Count)
            {
                if (GetData) RetVal = lstLSD[index].Data.Replace("OTHER,", "");
                else RetVal = lstLSD[index].Label;
            }

            return RetVal;
        }
        List<MT_JETTY> mt_port = PortDAL.GetPortData();
        public List<ItemCDL> GetItemCDL(ChitRootObject modelReport)
        {
            List<SelectListItem> lstCargo = DropdownServiceModel.getMaterial(false, "", "MAT_CMMT");
            List<ItemCDL> lstCDL = new List<Report.CharteringInCMMTReport.ItemCDL>();
            int idx = 0;
            if (modelReport.chit_detail != null && modelReport.chit_detail.chit_cargo_qty != null)
            {
                foreach (var Item in modelReport.chit_detail.chit_cargo_qty.OrderBy(x => x.order))
                {
                    string CarGoString = (lstCargo.Where(x => x.Value == Item.cargo).ToList().Count > 0) ? lstCargo.Where(x => x.Value == Item.cargo).FirstOrDefault().Text : "";
                    lstCDL.Add(new Report.CharteringInCMMTReport.ItemCDL { index = idx, Label = "Cargo " + Item.order + " :", Data = string.Format("{0}, {1}, {2}, {3}", CarGoString, Item.qty, Item.tolerance, Item.unit) });
                    idx++;
                }
            }
            if (modelReport.chit_detail != null && modelReport.chit_detail.chit_load_port_control != null)
            {
                foreach (var Item in modelReport.chit_detail.chit_load_port_control.OrderBy(x => x.PortOrder))
                {
                    string[] PortNamelst = Item.PortName.Split('|');
                    string PortName = "";
                    if (PortNamelst.Length > 1) { PortName = (mt_port.Where(x => x.MTJ_ROW_ID == PortNamelst[1]).ToList().Count > 0) ? mt_port.Where(x => x.MTJ_ROW_ID == PortNamelst[1]).ToList()[0].MTJ_JETTY_NAME : ""; }
                    Item.PortFullName = Item.PortName;
                    lstCDL.Add(new Report.CharteringInCMMTReport.ItemCDL { index = idx, Label = "Load Port " + Item.PortOrder + " :", Data = string.Format("{0} ", Item.PortFullName) });
                    idx++;
                }
            }
            if (modelReport.chit_detail != null && modelReport.chit_detail.chit_discharge_port_control != null)
            {
                foreach (var Item in modelReport.chit_detail.chit_discharge_port_control.OrderBy(x => x.PortOrder))
                {
                    string[] PortNamelst = Item.PortName.Split('|');
                    string PortName = "";
                    if (PortNamelst.Length > 1) { PortName = (mt_port.Where(x => x.MTJ_ROW_ID == PortNamelst[1]).ToList().Count > 0) ? mt_port.Where(x => x.MTJ_ROW_ID == PortNamelst[1]).ToList()[0].MTJ_JETTY_NAME : ""; }
                    Item.DischargeFullName = Item.PortName;
                    lstCDL.Add(new Report.CharteringInCMMTReport.ItemCDL { index = idx, Label = "Discharging Port " + Item.PortOrder + " :", Data = string.Format("{0} ", Item.DischargeFullName) });
                    idx++;
                }
            }
            return lstCDL;
        }

        public class ItemCDL
        {
            public int index { get; set; }
            public string Label { get; set; }
            public string Data { get; set; }
        }


        private List<SelectListItem> getCargoUnit()
        {
            //LoadMaster From JSON
            List<SelectListItem> quantity = new List<SelectListItem>();
            Setting setting = JSONSetting.getSetting("JSON_CHARTER_IN_VESSEL");
            if (setting.unitChit != null)
            {
                for (int i = 0; i < setting.unitChit.Count; i++)
                {
                    quantity.Add(new SelectListItem { Text = setting.unitChit[i], Value = setting.unitChit[i] });
                }
            }

            return quantity;
        }

        private void FooterReport(Section section, ChitRootObject model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;

            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 6;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("1st Verified by");
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Confirm by");
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("2nd Verified by");
            row.Cells[icell].MergeRight = 6;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 5;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            icell = 0;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Trader S/H");
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Bottom.Color = Colors.Transparent;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;

            //row.Cells[icell].AddParagraph("");
            //row.Cells[icell].MergeRight = 1;
            // row.Cells[icell].Borders.Left.Color = Colors.Black;
            //icell += row.Cells[icell].MergeRight + 1;

            //CMMT
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;


            //S/H
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_CON", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[37].Borders.Right.Color = Colors.Black;

            //Trader
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_CON_SH", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[37].Borders.Right.Color = Colors.Black;

            //Trader S/H
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[37].Borders.Right.Color = Colors.Black;

            //CMVP
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(2, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[37].Borders.Right.Color = Colors.Black;

            //EVPC 
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            icell = 0;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_CON");
            row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_CON_SH");
            row.Cells[icell].AddParagraph(string.Format("( {0}{1} )",model.chit_negotiation_summary.select_charter_for, !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( {0}{1} )", model.chit_negotiation_summary.select_charter_for, !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //row.Cells[icell].AddParagraph("Trader S/H");
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Top.Color = Colors.Transparent;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;


            //icell = 0;
            //row = table.AddRow();
            //row.Cells[icell].Borders.Right.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Borders.Top.Color = Colors.White;
            //row.HeadingFormat = true;
            //row.Format.Font.Bold = true;

            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            //row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //row.Cells[icell].MergeRight = 5;
            //row.Cells[icell].Borders.Left.Color = Colors.White;
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].Borders.Right.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            //row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //row.Cells[icell].MergeRight = 4;
            //row.Cells[icell].Borders.Left.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].Borders.Right.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            //row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //row.Cells[icell].MergeRight = 4;
            //row.Cells[icell].Borders.Left.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].Borders.Right.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            //row.Cells[icell].AddParagraph(string.Format("( CMMT{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //row.Cells[icell].MergeRight = 5;
            //row.Cells[icell].Borders.Left.Color = Colors.White;
            //icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].AddParagraph("CMVP");
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //row.Cells[icell].MergeRight = 5;
            //icell += row.Cells[icell].MergeRight + 1;

            //row.Cells[icell].AddParagraph("EVPC");
            //_RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            //row.Cells[icell].MergeRight = 5;
            //icell += row.Cells[icell].MergeRight + 1;

        }

        private void FooterReport_Old(Section section, ChitRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy");
            //row.Cells[icell].MergeRight = 37;
            //icell += row.Cells[icell].MergeRight + 1;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested By");
            row.Cells[icell].MergeRight = 17;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 6;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve2
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve3
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph(string.Format("( CMMT-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(string.Format("( CMMT-{0} )", _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1", true)));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;

        }

        private void ExplanImageReport(Section section, ChitRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("19cm");
            //for (int i = 0; i < 18; i++)
            //{
            //    column = this.table.AddColumn("1cm");
            //}

            Row row = table.AddRow();
            int icell = 0;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            string[] _image = modelReport.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    row = table.AddRow();
                    icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    //row.Format.Font.Bold = true;
                    //row.Cells[icell].AddParagraph("");
                    //row.Cells[icell].MergeRight = 2;
                    //row.Cells[icell].Borders.Right.Color = Colors.White;
                    //icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Top.Color = Color.Empty;
                    row.Cells[icell].Borders.Bottom.Color = Color.Empty;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(12, UnitType.Centimeter);
                    //row.Cells[icell].Borders.Right.Color = Colors.White;
                    //row.Cells[icell].Borders.Left.Color = Colors.White;
                    //row.Cells[icell].MergeRight = 12;
                    //icell += row.Cells[icell].MergeRight + 1;
                    //row.Cells[icell].Borders.Left.Color = Colors.White;
                    //row.Cells[icell].AddParagraph("");
                    //row.Cells[icell].MergeRight = 2;
                    //row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].Borders.Top.Color = Color.Empty;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        //row.Cells[icell].MergeRight = 18;
                    }
                }
            }
        }

        private void ExplanImageReportTrader(Section section, ChitRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("19cm"); 

            Row row = table.AddRow();
            int icell = 0;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            string[] _image = modelReport.explanationAttachTrader.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    row = table.AddRow();
                    icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center; 
                    row.Cells[icell].Borders.Top.Color = Color.Empty;
                    row.Cells[icell].Borders.Bottom.Color = Color.Empty;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(12, UnitType.Centimeter); 
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].Borders.Top.Color = Color.Empty;
                        row.Cells[icell].AddParagraph(itemsplit[1]); 
                    }
                }
            }
        }

        private string getCarogNameById(string id)
        {
            return MaterialsDAL.GetMaterialById(id).MET_MAT_DES_ENGLISH;
        }

        private string nullToEmtry(string value)
        {
            return string.IsNullOrEmpty(value.Trim()) ? "-" : value;
        }
    }
}