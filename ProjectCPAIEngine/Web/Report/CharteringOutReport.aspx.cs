﻿using com.pttict.engine.utility;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp.Pdf;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using ProjectCPAIEngine.Model;
using ProjectCPAIEngine.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using static ProjectCPAIEngine.Utilities.HtmlRemoval;

namespace ProjectCPAIEngine.Web.Report
{
    public partial class CharteringOutReport : System.Web.UI.Page
    {
        #region --- Report Old ---
        //ShareFn _FN = new ShareFn();
        //ReportFunction _RFN = new ReportFunction();
        //Document document;
        //MigraDoc.DocumentObjectModel.Tables.Table table;
        //Color TableBorder;
        //Color TableGray;
        //const int defaultHeight = 10;
        //HtmlRemoval _htmlRemove = new HtmlRemoval();
        //int FontSize = 8;
        //string FontName = "Tahoma";
        //string type = ConstantPrm.SYSTEMTYPE.CRUDE;
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    //LoadXMLFromTest();
        //    if (Request.QueryString["TranID"] != null)
        //    {
        //        LoadDataCharter(Request.QueryString["TranID"].ToString().Decrypt());
        //    }
        //    else
        //    {
        //        if (Session["ChoRootObject"] != null)
        //        {
        //            ChoRootObject _Bunker = (ChoRootObject)Session["ChoRootObject"];
        //            GenPDF(_Bunker, true);
        //            Session["ChoRootObject"] = null;
        //        }
        //    }
        //}

        //public string LoadDataCharter(string TransactionID, bool redirect = true, string UserName = "")
        //{
        //    string pdfPath = "";
        //    RequestData reqData = new RequestData();
        //    reqData.function_id = ConstantPrm.FUNCTION.F10000005;
        //    reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
        //    reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
        //    reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();  
        //    reqData.state_name = "";
        //    reqData.req_parameters = new req_parameters();
        //    reqData.req_parameters.p = new List<p>();
        //    reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
        //    reqData.req_parameters.p.Add(new p { k = "user", v = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
        //    reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
        //    reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
        //    reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
        //    reqData.extra_xml = "";

        //    ResponseData resData = new ResponseData();
        //    ServiceProvider.ProjService service = new ServiceProvider.ProjService();
        //    var xml = ShareFunction.XMLSerialize(reqData);
        //    reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
        //    resData = service.CallService(reqData);
        //    string _DataJson = resData.extra_xml;
        //    ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

        //    if (_model.data_detail != null)
        //    {
        //        _model.data_detail = _model.data_detail.Replace("null", "\"\"");
        //        ChoRootObject _modelBunker = new JavaScriptSerializer().Deserialize<ChoRootObject>(_model.data_detail);
        //        if (_modelBunker.cho_negotiation_summary.cho_offers_items.Count <= 0) _modelBunker.cho_negotiation_summary.cho_offers_items.Add(new ChoOffersItem());
        //        pdfPath = GenPDF(_modelBunker, redirect);
        //    }
        //    else
        //    {
        //        if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
        //    }

        //    return pdfPath;
        //}

        //private void LoadXMLFromTest()
        //{
        //    string PathTest = @"D:\Json.txt";
        //    if (File.Exists(PathTest))
        //    {
        //        StreamReader _sr = new StreamReader(PathTest);
        //        string Line = _sr.ReadToEnd();
        //        ChoRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChoRootObject>(Line);
        //        GenPDF(_modelReport, true);
        //    }


        //}

        //private string GenPDF(ChoRootObject modelReport, bool redirect)
        //{
        //    Document document = CreateDocument(modelReport);           
        //    document.UseCmykColor = true;
        //    const bool unicode = true;
        //    const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
        //    PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);
        //    pdfRenderer.Document = document;
        //    pdfRenderer.RenderDocument();
        //    // Save the document...
        //    string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("{0}{1}.pdf", type, DateTime.Now.ToString("yyyyMMddHHmmssffff")));
        //    pdfRenderer.PdfDocument.Save(filename);
        //    if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
        //    // ...and start a viewer.
        //    //Process.Start(filename);
        //    return filename;
        //}

        //public Document CreateDocument(ChoRootObject modelReport)
        //{
        //    TableGray = Colors.Blue;
        //    TableBorder = Colors.Black;
        //    TableGray = Colors.Gray;
        //    // Create a new MigraDoc document
        //    this.document = new Document();
        //    this.document.Info.Title = "Charter Out";
        //    this.document.DefaultPageSetup.LeftMargin = 30;
        //    this.document.DefaultPageSetup.TopMargin = 30;
        //    this.document.DefaultPageSetup.BottomMargin = 30;
        //    DefineStyles();
        //    _RFN.document = document;
        //    _RFN.FontName = FontName;
        //    CreatePage(modelReport);
        //    return this.document;
        //}

        //void DefineStyles()
        //{
        //    MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
        //    style.Font.Name = FontName;

        //    style = this.document.Styles[StyleNames.Header];
        //    style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

        //    style = this.document.Styles[StyleNames.Footer];
        //    style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
        //    style = this.document.Styles.AddStyle("Table", "Normal");
        //    style.Font.Name = FontName;
        //    style.Font.Name = "Times New Roman";
        //    style.Font.Size = 10;

        //    style = this.document.Styles.AddStyle("Reference", "Normal");
        //    style.ParagraphFormat.SpaceBefore = "5mm";
        //    style.ParagraphFormat.SpaceAfter = "5mm";
        //    style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        //}

        //void CreatePage(ChoRootObject modelReport)
        //{
        //    Section section = this.document.AddSection();
        //    int StartOfferIndex = 0;
        //    var _VesselName = DropdownServiceModel.getVehicle("CHOVESCS", true).Where(x => x.Value.ToUpper() == modelReport.cho_evaluating.vessel_name.ToUpper());
        //    if (_VesselName.ToList().Count > 0)
        //    {
        //        modelReport.cho_evaluating.vessel_name = _VesselName.ToList()[0].Text;
        //    }

        //    while (StartOfferIndex + 1 <= modelReport.cho_negotiation_summary.cho_offers_items[0].cho_round_items.Count)
        //    {
        //        if (StartOfferIndex > 0) document.LastSection.AddPageBreak();
        //        HeadingReport(section, modelReport);
        //        OfferTableZone(section, modelReport, StartOfferIndex);
        //        BottomZoneReport(section, modelReport);
        //        //document.LastSection.AddPageBreak();            
        //        if (StartOfferIndex == 0) FooterReport(section, modelReport);

        //        StartOfferIndex += 2;
        //    }

        //    //-------------Table Offer-------------

        //    //-------------------------------------

        //}
        //Setting _setting = JSONSetting.getSetting("JSON_CHARTER_OUT_CRUDE");
        //private void HeadingReport(Section section, ChoRootObject modelReport)
        //{
        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize;
        //    this.table.Rows.Height = defaultHeight;

        //    Column column = this.table.AddColumn("1cm");
        //    column.Format.Alignment = ParagraphAlignment.Left;
        //    for (int i = 0; i < 18; i++)
        //    {
        //        column = this.table.AddColumn("1cm");
        //        column.Format.Alignment = ParagraphAlignment.Left;
        //    }

        //    // Create the header of the table
        //    Row row = table.AddRow();
        //    Image _img;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    int icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 3;
        //    row.Cells[icell].Borders.Color = Colors.White;
        //    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Borders.Color = Colors.White;
        //    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Chartering Out Memo for Approval");
        //    row.Cells[icell].Format.Font.Size = 9;
        //    row.Cells[icell].MergeRight = 10;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[icell].Borders.Color = Colors.White;
        //    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        //    row.Cells[icell].MergeRight = 3;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    //row.Cells[icell].AddParagraph(string.Format("For Vessel:{0}",modelReport.cho_proposed_for_approve.cho_estimated_cost.cost.ToUpper()));
        //    row.Cells[icell].AddParagraph("Please fill inform as relevant");
        //    row.Cells[icell].MergeRight = 13;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Date");
        //    row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(modelReport.cho_evaluating.date_purchase, "dd MMM yyyy"));
        //    row.Cells[icell].MergeRight = 3;
        //    row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;

        //    row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    icell = 0;
        //    row.Cells[icell].AddParagraph(string.Format("For Vessel:{0}", modelReport.cho_evaluating.vessel_name.ToUpper()));
        //    row.Cells[icell].MergeRight = 18;
        //    icell += row.Cells[icell].MergeRight + 1;


        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Top;
        //    row.Format.Font.Bold = true;
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Type of Fixing :");
        //    row.Cells[icell].MergeRight = 2;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    Table tmpTable = new Table();
        //    MakeTableTypeOfFixing(modelReport.cho_chartering_method.cho_type_of_fixings, tmpTable);
        //    row.Cells[icell].Elements.Add(tmpTable);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    tmpTable = new Table();
        //    MakeTableTypeOfFixingDetail(modelReport.cho_evaluating, tmpTable);
        //    row.Cells[icell].Elements.Add(tmpTable);
        //    row.Cells[icell].MergeRight = 11;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow(); icell = 0;
        //    row.HeadingFormat = true;
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Top;
        //    row.Cells[icell].AddParagraph("Explanation (as necessary)");
        //    row.Cells[0].Borders.Bottom.Color = Colors.White;
        //    row.Cells[icell].MergeRight = 18;

        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Top;
        //    row.Format.Font.Bold = false;
        //    Paragraph _prg = row.Cells[0].AddParagraph();
        //    _htmlRemove.ValuetoParagraph(modelReport.cho_chartering_method.reason, _prg);
        //    row.Cells[0].MergeRight = 18;
        //    row.Cells[0].Borders.Top.Color = Colors.White;



        //}

        //private void OfferTableZone(Section section, ChoRootObject modelReport, int itemOfferIndex)
        //{

        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize - 2;
        //    this.table.Rows.Height = defaultHeight;

        //    Column column = this.table.AddColumn("0.5cm");
        //    for (int i = 0; i < 37; i++)
        //    {
        //        column = this.table.AddColumn("0.5cm");
        //    }
        //    #region--------HeaddingTable-------------


        //    Row row = table.AddRow();
        //    row.Cells[0].AddParagraph("").AddFormattedText("Table1", TextFormat.Underline).AddFormattedText(" : Applied for \"Fixing Counter\"", TextFormat.NoUnderline);
        //    row.Cells[0].MergeRight = 37;
        //    row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
        //    this.table.Format.Alignment = ParagraphAlignment.Left;
        //    row.Cells[0].Format.Font.Bold = true;
        //    //row = table.AddRow();
        //    bool TwoRound = true;

        //    if ((modelReport.cho_negotiation_summary.cho_offers_items[0].cho_round_items.Count - itemOfferIndex) % 2 != 0)
        //    {
        //        if ((modelReport.cho_negotiation_summary.cho_offers_items[0].cho_round_items.Count - itemOfferIndex) == 1)
        //        {
        //            TwoRound = false;
        //        }
        //    }

        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    int icell = 0;
        //    row.Cells[icell].AddParagraph("Charterer");
        //    row.Cells[icell].MergeRight = 4;
        //    row.Cells[0].MergeDown = 2;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Route");
        //    row.Cells[icell].MergeRight = 4;
        //    row.Cells[icell].MergeDown = 2;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    int icellQ = icell;
        //    row.Cells[icell].AddParagraph("Minimun Loaded");
        //    row.Cells[icell].MergeRight = 4;
        //    row.Cells[icell].MergeDown = 2;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    int icelR2 = icell;
        //    //int ttSt = (2 * (Quantity.Count - 1));
        //    row.Cells[icell].AddParagraph("Offer and Counter");
        //    row.Cells[icell].MergeRight = 12;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Fix");
        //    row.Cells[icell].MergeRight = 9;
        //    row.Cells[icell].MergeDown = 2;
        //    icell += row.Cells[icell].MergeRight + 1;


        //    icell = icelR2;
        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Fixing Items");
        //    row.Cells[icell].MergeRight = 4;
        //    row.Cells[icell].MergeDown = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    icelR2 = icell;
        //    if (TwoRound)
        //    {
        //        row.Cells[icell].AddParagraph(string.Format("{0} st", (itemOfferIndex + 1).ToString()));
        //        row.Cells[icell].MergeRight = 3;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph(string.Format("{0} nd", (itemOfferIndex + 2).ToString()));
        //        row.Cells[icell].MergeRight = 3;
        //    }
        //    else
        //    {
        //        row.Cells[icell].AddParagraph(string.Format("{0} st", (itemOfferIndex + 1).ToString()));
        //        row.Cells[icell].MergeRight = 7;
        //    }

        //    icell = icelR2;
        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    if (TwoRound)
        //    {
        //        row.Cells[icell].AddParagraph("Offer");
        //        row.Cells[icell].MergeRight = 1;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph("Counter");
        //        row.Cells[icell].MergeRight = 1;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph("Offer");
        //        row.Cells[icell].MergeRight = 1;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph("Counter");
        //        row.Cells[icell].MergeRight = 1;
        //        icell += row.Cells[icell].MergeRight + 1;
        //    }
        //    else
        //    {
        //        row.Cells[icell].AddParagraph("Offer");
        //        row.Cells[icell].MergeRight = 3;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph("Counter");
        //        row.Cells[icell].MergeRight = 3;
        //        icell += row.Cells[icell].MergeRight + 1;
        //    }

        //    #endregion----------------------------------
        //    var Customer = DropdownServiceModel.getCustomer("CHOCUSCS");
        //    //Loop Bind Data
        //    for (int i = 0; i < modelReport.cho_negotiation_summary.cho_offers_items.Count; i++)
        //    {

        //        var _offerItem = modelReport.cho_negotiation_summary.cho_offers_items[i];
        //        int MergeDown = _offerItem.cho_round_items[itemOfferIndex].cho_round_info.Count - 1;
        //        icell = 0;
        //        row = table.AddRow();
        //        row.Format.Alignment = ParagraphAlignment.Center;
        //        row.VerticalAlignment = VerticalAlignment.Center;
        //        string CustommerName = Customer.Where(x => x.Value.ToUpper() == _offerItem.charterer.ToUpper()).FirstOrDefault().Text;
        //        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], CustommerName, 4));
        //        row.Cells[icell].MergeRight = 4;
        //        row.Cells[icell].MergeDown = MergeDown;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.route_vessel, 4));
        //        row.Cells[icell].MergeRight = 4;
        //        row.Cells[icell].MergeDown = MergeDown;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.route_vessel, 4));
        //        row.Cells[icell].MergeRight = 4;
        //        row.Cells[icell].MergeDown = MergeDown;
        //        icell += row.Cells[icell].MergeRight + 1;
        //        int rcell = icell; bool firstrow = true; string tmpVal = "";
        //        foreach (var item in _offerItem.cho_round_items[itemOfferIndex].cho_round_info)
        //        {
        //            tmpVal = "";
        //            if (!firstrow) { row = table.AddRow(); }


        //            icell = rcell;
        //            row.Format.Alignment = ParagraphAlignment.Center;
        //            row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.type, 4));
        //            row.Cells[icell].MergeRight = 4;
        //            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        //            icell += row.Cells[icell].MergeRight + 1;

        //            if (!TwoRound)
        //            {
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex].cho_round_info.Where(x => x.type == item.type).First().offer;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 3;
        //                icell += row.Cells[icell].MergeRight + 1;
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex].cho_round_info.Where(x => x.type == item.type).First().counter;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 3;
        //                icell += row.Cells[icell].MergeRight + 1;
        //            }
        //            else
        //            {
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex].cho_round_info.Where(x => x.type == item.type).First().offer;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 1;
        //                icell += row.Cells[icell].MergeRight + 1;
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex].cho_round_info.Where(x => x.type == item.type).First().counter;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 1;
        //                icell += row.Cells[icell].MergeRight + 1;
        //                tmpVal = "";
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex + 1].cho_round_info.Where(x => x.type == item.type).First().offer;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 1;
        //                icell += row.Cells[icell].MergeRight + 1;
        //                tmpVal = _offerItem.cho_round_items[itemOfferIndex + 1].cho_round_info.Where(x => x.type == item.type).First().counter;
        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], tmpVal, 3));
        //                row.Cells[icell].MergeRight = 1;
        //                icell += row.Cells[icell].MergeRight + 1;
        //            }
        //            if (firstrow)
        //            {

        //                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.final_deal, 5));
        //                row.Cells[icell].MergeRight = 9;
        //                row.Cells[icell].MergeDown = MergeDown;
        //                firstrow = false;
        //            }

        //        }

        //    }


        //}

        //private void BottomZoneReport(Section section, ChoRootObject modelReport)
        //{
        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize;
        //    this.table.Rows.Height = defaultHeight;
        //    // Before you can add a row, you must define the columns
        //    Column column = this.table.AddColumn("1cm");
        //    for (int i = 0; i < 18; i++)
        //    {
        //        column = this.table.AddColumn("1cm");
        //    }
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Top;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 5;
        //    Table tmpTable = new Table();
        //    MakeTableFinalFixingDetail(modelReport.cho_proposed_for_approve.cho_final_fixing_detail, tmpTable);
        //    row.Cells[icell].Elements.Add(tmpTable);
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 5;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    tmpTable = new Table();
        //    MakeTableEstimatedIncome(modelReport.cho_proposed_for_approve.cho_estimated_income, tmpTable);
        //    row.Cells[icell].Elements.Add(tmpTable);
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 6;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    tmpTable = new Table();
        //    MakeTableEstimatedCost(modelReport.cho_proposed_for_approve.cho_estimated_cost, tmpTable,modelReport.cho_evaluating.vessel_name);
        //    row.Cells[icell].Elements.Add(tmpTable);
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow(); icell = 0;
        //    row.Format.Font.Bold = true;
        //    row.Format.Font.Size = FontSize - 1;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Tentative Performing Period");
        //    row.Cells[icell].MergeRight = 4;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(modelReport.cho_proposed_for_approve.cho_total.ten_preform_period);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
        //    row.Cells[icell].AddParagraph("Estimated Benefit");
        //    row.Cells[icell].MergeRight = 9;

        //    row = table.AddRow(); icell = 0;
        //    row.Format.Font.Bold = true;
        //    row.Format.Font.Size = FontSize - 1;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Total Cost for Charter Out");
        //    row.Cells[icell].MergeRight = 4;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.cho_proposed_for_approve.cho_total.total_cost_charter_out));
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Benefit saving =  Total Cost without Charter out - Total Cost for Charter Out");
        //    row.Cells[icell].MergeRight = 9;

        //    row = table.AddRow(); icell = 0;
        //    row.Format.Font.Bold = true;
        //    row.Format.Font.Size = FontSize - 1;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Total Cost without Charter Out");
        //    row.Cells[icell].MergeRight = 4;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(modelReport.cho_proposed_for_approve.cho_total.total_cost_without));
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph("Total Benefit Saving = "+ _FN.intTostrMoney(modelReport.cho_proposed_for_approve.cho_total.est_benefit));
        //    row.Cells[icell].MergeRight = 9;
        //}

        //private void FooterReport(Section section, ChoRootObject modelReport)
        //{
        //    Image _img;
        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize;
        //    this.table.Rows.Height = defaultHeight;

        //    // Before you can add a row, you must define the columns
        //    Column column = this.table.AddColumn("1cm");
        //    for (int i = 0; i < 18; i++)
        //    {
        //        column = this.table.AddColumn("1cm");
        //    }

        //    Row row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[0].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
        //    row.Cells[0].MergeRight = 18;

        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Format.Font.Underline = Underline.Single;
        //    row.Borders.Color = Colors.White;
        //    row.Borders.Right.Color = Colors.Black;
        //    row.Cells[0].AddParagraph("Requested By");
        //    row.Cells[0].Borders.Left.Color = Colors.Black;
        //    row.Cells[0].MergeRight = 6;
        //    row.Cells[7].AddParagraph("Certified by");
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("Approved by");
        //    row.Cells[13].Borders.Color = Colors.Black;
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Right;
        //    row.Format.Font.Bold = true;
        //    row.Borders.Color = Colors.White;
        //    row.Borders.Left.Color = Colors.Black;
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[0].Borders.Right.Color = Colors.White;
        //    row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[3].Borders.Right.Color = Colors.Black;
        //    //row.Cells[3].AddParagraph("____________________");
        //    _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[7].Borders.Right.Color = Colors.Black;
        //    //row.Cells[7].AddParagraph("________________________");
        //    _img = row.Cells[7].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[13].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[13].AddParagraph("________________________");
        //    _img = row.Cells[13].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[13].Borders.Color = Colors.Black;
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Borders.Color = Colors.White;
        //    row.Borders.Left.Color = Colors.Black;
        //    row.Cells[0].AddParagraph("Charterer :");
        //    row.Cells[0].Borders.Right.Color = Colors.White;
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[3].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
        //    row.Cells[3].Borders.Right.Color = Colors.Black;
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].AddParagraph("CMVP");
        //    row.Cells[7].Borders.Right.Color = Colors.Black;
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("EVPC");
        //    row.Cells[13].Borders.Color = Colors.Black;
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Right;
        //    row.Format.Font.Bold = true;
        //    row.Borders.Color = Colors.White;
        //    row.Borders.Left.Color = Colors.Black;
        //    row.Cells[0].AddParagraph("");
        //    row.Cells[0].Borders.Right.Color = Colors.White;
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[3].AddParagraph("____________________");
        //    _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", true));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[3].Borders.Right.Color = Colors.Black;
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].AddParagraph("");
        //    row.Cells[7].Borders.Right.Color = Colors.Black;
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("");
        //    row.Cells[13].Borders.Color = Colors.Black;
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Borders.Color = Colors.Black;
        //    row.Cells[0].AddParagraph("S/H :");
        //    row.Cells[0].Borders.Right.Color = Colors.White;
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[3].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1", true)));
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].AddParagraph("");
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("");
        //    row.Cells[13].MergeRight = 5;

        //}

        //private void FooterReport2(Section section, ChoRootObject modelReport)
        //{
        //    Image _img;
        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize;
        //    this.table.Rows.Height = 18;

        //    // Before you can add a row, you must define the columns
        //    Column column = this.table.AddColumn("1cm");
        //    for (int i = 0; i < 18; i++)
        //    {
        //        column = this.table.AddColumn("1cm");
        //    }

        //    Row row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[0].AddParagraph("Requestd, certified and approved can be done either via e-mail or hard copy");
        //    row.Cells[0].MergeRight = 18;

        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[0].AddParagraph("Requested By");
        //    row.Cells[0].MergeRight = 6;
        //    row.Cells[7].AddParagraph("Certified by");
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("Approved by");
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Right;
        //    row.Format.Font.Bold = true;
        //    row.Cells[0].AddParagraph("Charterer :");
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[0].MergeDown = 1;
        //    row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[3].AddParagraph("____________________");
        //    _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[3].Borders.Bottom.Color = Colors.White;
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[7].AddParagraph("________________________");
        //    _img = row.Cells[7].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[7].Borders.Bottom.Color = Colors.White;
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[7].MergeDown = 1;
        //    row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[13].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[13].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[13].AddParagraph("________________________");
        //    _img = row.Cells[13].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3"));
        //    _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;
        //    row.Cells[13].MergeDown = 1;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[3].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
        //    row.Cells[3].Borders.Top.Color = Colors.White;
        //    row.Cells[3].MergeRight = 3;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Right;
        //    row.Format.Font.Bold = true;
        //    row.Cells[0].AddParagraph("S/H :");
        //    row.Cells[0].MergeRight = 2;
        //    row.Cells[0].MergeDown = 1;
        //    row.Cells[3].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[3].AddParagraph("____________________");
        //    _img = row.Cells[3].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", true));
        //    _img.Width = new Unit(3, UnitType.Centimeter);

        //    row.Cells[3].Borders.Bottom.Color = Colors.White;
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[7].Borders.Top.Color = Colors.White;
        //    row.Cells[7].AddParagraph("CMVP");
        //    row.Cells[7].Borders.Bottom.Color = Colors.White;
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].VerticalAlignment = VerticalAlignment.Bottom;
        //    row.Cells[13].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[13].Borders.Top.Color = Colors.White;
        //    row.Cells[13].Borders.Bottom.Color = Colors.White;
        //    row.Cells[13].AddParagraph("EVPC");
        //    row.Cells[13].MergeRight = 5;

        //    row = table.AddRow();
        //    row.Height = 18;
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[3].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1", true)));
        //    row.Cells[3].Borders.Top.Color = Colors.White;
        //    row.Cells[3].MergeRight = 3;
        //    row.Cells[7].AddParagraph("");
        //    row.Cells[7].Borders.Top.Color = Colors.White;
        //    row.Cells[7].MergeRight = 5;
        //    row.Cells[13].AddParagraph("");
        //    row.Cells[13].Borders.Top.Color = Colors.White;
        //    row.Cells[13].MergeRight = 5;

        //}

        //public void MakeTableOfferItem(List<ChoRoundItem> Detail, Table table, int indexOffer)
        //{
        //    table.Borders.Color = Colors.Black;
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    for (int i = 0; i < 17; i++)
        //    {
        //        table.AddColumn("0.5cm");
        //    }
        //    var _RoundItem = new List<ChoRoundItem>();
        //    for (int i = 0; i < 2; i++)
        //    {
        //        if (Detail.Where(x => x.round_no == (indexOffer + i).ToString()).ToList().Count > 0)
        //        {
        //            _RoundItem.Add(Detail.Where(x => x.round_no == (indexOffer + i).ToString()).ToList()[0]);
        //        }
        //    }
        //    if (_RoundItem.Count > 0)
        //    {
        //        foreach (var _item in _RoundItem[0].cho_round_info)
        //        {
        //            int icell = 0;
        //            Row row = table.AddRow();
        //            row.Format.Alignment = ParagraphAlignment.Left;
        //            row.VerticalAlignment = VerticalAlignment.Center;

        //            row.Cells[icell].AddParagraph(_item.type);
        //            icell += row.Cells[icell].MergeRight + 1;
        //            row.Cells[icell].AddParagraph(_item.offer);
        //            icell += row.Cells[icell].MergeRight + 1;
        //            row.Cells[icell].AddParagraph(_item.counter);
        //            icell += row.Cells[icell].MergeRight + 1;
        //            row.Cells[icell].AddParagraph(_item.offer);
        //            icell += row.Cells[icell].MergeRight + 1;
        //            row.Cells[icell].AddParagraph(_item.offer);
        //            icell += row.Cells[icell].MergeRight + 1;
        //        }
        //    }

        //}

        //#region MakeTable
        //public void MakeTableTypeOfFixing(List<ChoTypeOfFixing> Detail, Table table)
        //{
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;

        //    for (int i = 0; i < 2; i++)
        //    {
        //        var _item = Detail[i];
        //        if (Detail[i].type_flag == "Y")
        //        {
        //            row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings", FontSize + 2)).AddFormattedText(" " + _item.type_value, new Font(FontName, FontSize - 1));
        //        }
        //        else
        //        {
        //            row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + _item.type_value, new Font(FontName, FontSize - 1));
        //        }
        //    }

        //    row.Cells[icell].Format.Font.Bold = false;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    //row.Cells[icell].AddParagraph("Laycan:");
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph(Detail.laycan);
        //    //row.Cells[icell].Format.Font.Bold = false;
        //    //row.Cells[icell].MergeRight = 2;


        //    //row = table.AddRow(); icell = 0;
        //    //row.Format.Font.Bold = true;
        //    //row.Format.Alignment = ParagraphAlignment.Left;
        //    //row.VerticalAlignment = VerticalAlignment.Center;
        //    //row.Cells[icell].AddParagraph("Freight:");
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph(Detail.freight);
        //    //row.Cells[icell].Format.Font.Bold = false;
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph("Flat rate:");
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph(Detail.flat_rate);
        //    //row.Cells[icell].Format.Font.Bold = false;
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph("at year:");
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph(Detail.year);
        //    //row.Cells[icell].Format.Font.Bold = false;

        //    //row = table.AddRow(); icell = 0;
        //    //row.Format.Font.Bold = true;
        //    //row.Format.Alignment = ParagraphAlignment.Left;
        //    //row.VerticalAlignment = VerticalAlignment.Center;
        //    //row.Cells[icell].AddParagraph("Vessel Size:");
        //    //icell += row.Cells[icell].MergeRight + 1;
        //    //row.Cells[icell].AddParagraph(Detail.vessel_size);
        //    //row.Cells[icell].Format.Font.Bold = false;
        //    //row.Cells[icell].MergeRight = 4;
        //}
        //public void MakeTableTypeOfFixingDetail(ChoEvaluating Detail, Table table)
        //{
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("1.5cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2cm");
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Loading Month:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.loading_month + "-" + Detail.loading_year);
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Laycan:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    string laycan_date = "";
        //    if (!string.IsNullOrEmpty(Detail.laycan_from))
        //    {
        //        laycan_date = _FN.ConvertDateFormatBackFormat(Detail.laycan_from, "dd MMM yyyy") + " to " + _FN.ConvertDateFormatBackFormat(Detail.laycan_to, "dd MMM yyyy");
        //    }
        //    row.Cells[icell].AddParagraph(laycan_date);
        //    row.Cells[icell].MergeRight = 2;
        //    icell += row.Cells[icell].MergeRight + 1;


        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Freight");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.freight);
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Flat rate:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.flat_rate);
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("at year:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.year);
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("vessel state");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.vessel_size);
        //    icell += row.Cells[icell].MergeRight + 3;

        //}


        //public void MakeTableFinalFixingDetail(ChoFinalFixingDetail Detail, Table table)
        //{
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 5;
        //    row.Cells[icell].AddParagraph("Final Fixing Detail");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Charterer:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.charterer);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;


        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Charterer Party:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.charter_patry);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Owner's Broker:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.owner_broker);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Charterer's Broker:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.charterer_broker);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Subject time:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.subject_time);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Laycan:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.laycan_from + "-" + Detail.laycan_to);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Loading:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.loading);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Discharging:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.discharging);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Format.Font.Bold = false;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].AddParagraph("Laytime:");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph(Detail.laytime);
        //    row.Cells[icell].MergeRight = 3;
        //    icell += row.Cells[icell].MergeRight + 1;
        //}
        //public void MakeTableEstimatedIncome(ChoEstimatedIncome Detail, Table table)
        //{
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 5;
        //    row.Cells[icell].AddParagraph("Estimated Income");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Fixing WS");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.fixing_ws));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("%");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Estimated Flat Rate");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.est_flat_rate));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD/MT");
        //    icell += row.Cells[icell].MergeRight + 1;



        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Min Loaded");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.min_loaded));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("K MT");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Add Comm");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.add_comm));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("%");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
        //    row.Cells[icell].AddParagraph("Freight");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.freight));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    row = table.AddRow();
        //    row = table.AddRow();
        //    row = table.AddRow();
        //    row = table.AddRow();
        //    row = table.AddRow();

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
        //    row.Cells[icell].AddParagraph("Income");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.income));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //}
        //public void MakeTableEstimatedCost(ChoEstimatedCost Detail, Table table,string Vesselname)
        //{
        //    table.Format.Font.Name = FontName;
        //    table.Format.Font.Size = FontSize - 1;
        //    table.Rows.Height = 8;
        //    table.AddColumn("1cm");
        //    table.AddColumn("1cm");
        //    table.AddColumn("1.5cm");
        //    table.AddColumn("2cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    table.AddColumn("2.5cm");
        //    int icell = 0;
        //    Row row = table.AddRow();
        //    row.Format.Font.Bold = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Cells[icell].Format.Font.Size = 10;
        //    row.Cells[icell].Format.Font.Underline = Underline.Single;
        //    row.Cells[icell].MergeRight = 5;
        //    row.Cells[icell].AddParagraph("Estimated Cost");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph(string.Format( "1. Cost of {0}", Vesselname));
        //    row.Cells[icell].MergeRight = 2;
        //    icell += row.Cells[icell].MergeRight +1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.cost_of_mt));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("2. Spot in");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("WS");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_1_ws));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("%");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Flat Rate");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_1_flat_rate));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD/MT");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Min Loaded");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_1_min_loaded));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("K MT");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.sum_spot_int_1));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("3. Spot in ");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("WS");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_2_ws));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("%");
        //    icell += row.Cells[icell].MergeRight + 1;


        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Flat Rate");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_2_flat_rate));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD/MT");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Min Loaded");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.spot_in_2_min_loaded));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("K MT");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.sum_spot_int_2));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("4. Awaiting Time");
        //    row.Cells[icell].MergeRight = 2;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    //row.Cells[icell].AddParagraph(Detail.awaiting_time);
        //    row.Cells[icell].AddParagraph("");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row = table.AddRow();
        //    icell = 0;
        //    row.Cells[icell].AddParagraph("Cost");
        //    row.Cells[icell].MergeRight = 1;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("");
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
        //    row.Cells[icell].AddParagraph(_FN.intTostrMoney(Detail.cost));
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("USD");
        //    icell += row.Cells[icell].MergeRight + 1;
        //}

        //#endregion
        #endregion

        #region --- Report New ---

        ShareFn _FN = new ShareFn();
        ReportFunction _RFN = new ReportFunction();
        Document document;
        MigraDoc.DocumentObjectModel.Tables.Table table;
        Color TableBorder;
        Color TableGray;
        const int defaultHeight = 10;
        HtmlRemoval _htmlRemove = new HtmlRemoval();
        int FontSize = 8;
        string URL = "";
        List<string> images = new List<string>();
        string FontName = "Tahoma";
        string type = ConstantPrm.SYSTEMTYPE.CMCS;
        double pageHight = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //LoadJsonFromTest();
            //LoadTest("201703291116060088712");
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "FileUpload"));
            }
            if (Request.QueryString["TranID"] != null)
            {
                var temp = Request.QueryString["TranID"];
                LoadDataCharter(Request.QueryString["TranID"].ToString().Decrypt());
            }
            else
            {
                if (Session["ChosRootObject"] != null)
                {
                    ChosRootObject _model = (ChosRootObject)Session["ChosRootObject"];
                    GenPDF(_model, true);
                    Session["ChosRootObject"] = null;
                }
            }
        }

        private void LoadJsonFromTest()
        {
            string PathTest = @"D:\JSON\json_charter_out_cmcs_v7.json";
            if (File.Exists(PathTest))
            {
                StreamReader _sr = new StreamReader(PathTest);
                string Line = _sr.ReadToEnd();
                ChosRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChosRootObject>(Line);
                GenPDF(_modelReport, true);
            }
        }

        public string LoadDataCharter(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";
            RequestData reqData = new RequestData();
            reqData.function_id = ConstantPrm.FUNCTION.F10000005;
            reqData.app_user = ConstantPrm.ENGINECONF.EnginAppID;
            reqData.app_password = ConstantPrm.ENGINECONF.EnginAppPassword;
            reqData.req_transaction_id = ConstantPrm.EnginGetEngineID();
            reqData.state_name = "";
            reqData.req_parameters = new req_parameters();
            reqData.req_parameters.p = new List<p>();
            reqData.req_parameters.p.Add(new p { k = "channel", v = ConstantPrm.ENGINECONF.WEBChannel });
            reqData.req_parameters.p.Add(new p { k = "user", v = (string.IsNullOrEmpty(UserName)) ? Const.User.UserName : UserName });
            reqData.req_parameters.p.Add(new p { k = "transaction_id", v = TransactionID });
            reqData.req_parameters.p.Add(new p { k = "system", v = ConstantPrm.SYSTEM.CHARTERING });
            reqData.req_parameters.p.Add(new p { k = "type", v = ConstantPrm.SYSTEMTYPE.CRUDE });
            reqData.extra_xml = "";

            ResponseData resData = new ResponseData();
            ServiceProvider.ProjService service = new ServiceProvider.ProjService();
            var xml = ShareFunction.XMLSerialize(reqData);
            reqData = ShareFunction.DeserializeXMLFileToObject<RequestData>(xml);
            resData = service.CallService(reqData);
            string _DataJson = resData.extra_xml;
            ExtraXML _model = ShareFunction.DeserializeXMLFileToObject<ExtraXML>("<ExtraXML>" + _DataJson + "</ExtraXML>");

            if (_model.data_detail != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model.data_detail, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                ChosRootObject _modelRoot = new JavaScriptSerializer().Deserialize<ChosRootObject>(result);
                if (!redirect)
                    URL = JSONSetting.getGlobalConfig("ROOT_URL");
                pdfPath = GenPDF(_modelRoot, redirect);
                if (!redirect)
                {
                    for (int i = 0; i < images.Count(); i++)
                    {
                        File.Delete(images[i]);
                    }
                }
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, resData.response_message, "MainBoards.aspx");
            }

            return pdfPath;
        }

        private void LoadTest(string TranID)
        {
            LoadData(TranID);
        }

        public string LoadData(string TransactionID, bool redirect = true, string UserName = "")
        {
            string pdfPath = "";

            string _model = CharterOutServiceModel.getTransactionCMCSByID(TransactionID);
            if (_model != null)
            {
                string pattern = "[\"](\\w+)[\"]:null";
                Regex rgx = new Regex(pattern);
                string replacement = "\"$1\":\"\"";
                string result = rgx.Replace(_model, replacement);
                result = result.Replace("\"approve_items\":\"\"", "\"approve_items\":null");
                //_model = _model.Replace("null", "\"\"");
                ChosRootObject _modelReport = new JavaScriptSerializer().Deserialize<ChosRootObject>(result);
                pdfPath = GenPDF(_modelReport, redirect);
            }
            else
            {
                if (redirect) _FN.MessageBoxShowLink(this.Page, "Error", "MainBoards.aspx");
            }
            return pdfPath;
        }

        private string GenPDF(ChosRootObject modelReport, bool redirect)
        {
            Document document = CreateDocument(modelReport);
            document.UseCmykColor = true;
            const bool unicode = true;
            const PdfFontEmbedding embedding = PdfFontEmbedding.Automatic;
            PdfDocumentRenderer pdfRenderer = new PdfDocumentRenderer(unicode, embedding);
            pdfRenderer.Document = document;
            pdfRenderer.RenderDocument();
            // Save the document...
            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web", "Report", "TmpFile", string.Format("{0}{1}.pdf", type, DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            pdfRenderer.PdfDocument.Save(filename);
            if (redirect) Response.Redirect(string.Format("TmpFile/{0}", new FileInfo(filename).Name));
            // ...and start a viewer.
            //Process.Start(filename);
            return filename;
        }

        public Document CreateDocument(ChosRootObject modelReport)
        {
            TableGray = Colors.Blue;
            TableBorder = Colors.Black;
            TableGray = Colors.Gray;
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "Charter Out";
            this.document.DefaultPageSetup.LeftMargin = 30;
            this.document.DefaultPageSetup.TopMargin = 30;
            this.document.DefaultPageSetup.BottomMargin = 30;
            DefineStyles();
            _RFN.document = document;
            _RFN.FontName = FontName;
            CreatePage(modelReport);
            return this.document;
        }

        void DefineStyles()
        {
            MigraDoc.DocumentObjectModel.Style style = this.document.Styles["Normal"];
            style.Font.Name = FontName;

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = FontName;
            style.Font.Name = "Times New Roman";
            style.Font.Size = 10;

            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        void CreatePage(ChosRootObject modelReport)
        {
            Section section = this.document.AddSection();
            int StartOfferIndex = 0;
            HeadingReport(section, modelReport, false);
            if (modelReport.chos_no_charter_out_case_a != null)
            {

                NoCharterOutCaseA_Report(section, modelReport);


            }

            if (modelReport.chos_charter_out_case_b != null)
            {
                if (pageHight >= 42)//check if has reason
                {
                    document.LastSection.AddPageBreak();
                    HeaderNew(section, modelReport);
                    CharterOutCase_Report(section, modelReport.chos_charter_out_case_b, modelReport.chos_evaluating.unit);
                }
                else
                {
                    CharterOutCase_Report(section, modelReport.chos_charter_out_case_b, modelReport.chos_evaluating.unit);
                }

            }

            if (modelReport.chos_proposed_for_approve != null)
            {
                ProposedForApprove_Report(section, modelReport);
                NoteTimelineSummaryReport(section, modelReport);
            }
            //FooterApproveReport(section, modelReport);
            FooterReport(section, modelReport);


            StartOfferIndex += 2;


            if (!string.IsNullOrEmpty(modelReport.explanationAttach))
            {
                document.LastSection.AddPageBreak();
                HeadingReport(section, modelReport, true);
                ExplanImageReport(section, modelReport);
            }


            //-------------Table Offer-------------

            //-------------------------------------

        }
        private void HeaderNew(Section section, ChosRootObject modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow(); int icell = 0;
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            // Header
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].AddParagraph("Chartering Out - Crude");
            row.Cells[icell].Format.Font.Size = 10;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;

            // Logo
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

            // For Vessel
            row.Cells[icell].AddParagraph("VESSEL :");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chos_evaluating.vessel_name.ToUpper()));
            row.Cells[icell].Format.LeftIndent = new Unit(1, UnitType.Millimeter);
            row.Cells[icell].MergeRight = 7;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("ON SUBJECT DATE");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(modelReport.chos_evaluating.date_purchase), "dd MMM yyyy"));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;

            // Details
            int icellDetails = 0;
            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("DETAILS :");
            row.Cells[icell].MergeRight = 2;

            icell += row.Cells[icell].MergeRight + 1;
            icellDetails = icell;

            Table tmpTable = new Table();
            MakeTableDetail(modelReport, tmpTable);
            row.Cells[icellDetails].Elements.Add(tmpTable);
            row.Cells[icellDetails].MergeRight = 15;


        }




        Setting _setting = JSONSetting.getSetting("JSON_CHARTER_OUT_VESSEL");
        HtmlRemoval htmlRemove = new HtmlRemoval();
        private void HeadingReport(Section section, ChosRootObject modelReport, bool AttachPage)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            // Create the header of the table
            Row row = table.AddRow(); int icell = 0;
            Image _img;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;

            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            // Header
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].AddParagraph("Chartering Out - Crude");
            row.Cells[icell].Format.Font.Size = 10;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;

            // Logo
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathLogo());
            _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].Borders.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            icell += row.Cells[icell].MergeRight + 1;

            row = table.AddRow();
            row.Format.Font.Bold = true;
            icell = 0;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;

            // For Vessel
            row.Cells[icell].AddParagraph("VESSEL :");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(modelReport.chos_evaluating.vessel_name.ToUpper()));
            row.Cells[icell].Format.LeftIndent = new Unit(1, UnitType.Millimeter);
            row.Cells[icell].MergeRight = 7;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("ON SUBJECT DATE");
            row.Cells[icell].MergeRight = 2;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(modelReport.chos_evaluating.date_purchase), "dd MMM yyyy"));
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 1;

            // Details
            int icellDetails = 0;
            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Font.Size = FontSize - 1;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].AddParagraph("DETAILS :");
            row.Cells[icell].MergeRight = 2;

            icell += row.Cells[icell].MergeRight + 1;
            icellDetails = icell;

            Table tmpTable = new Table();
            MakeTableDetail(modelReport, tmpTable);
            row.Cells[icellDetails].Elements.Add(tmpTable);
            row.Cells[icellDetails].MergeRight = 15;


            if (AttachPage == false)
            {
                row = table.AddRow(); icell = 0;
                row.HeadingFormat = true;
                row.Format.Font.Size = FontSize - 1;
                row.Height = 12;
                pageHight += row.Height;
                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Top;
                row.Cells[icell].AddParagraph("TYPE OF FIXING :");
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 2;

                icell += row.Cells[icell].MergeRight + 1;
                foreach (var item in modelReport.chos_chartering_method.chos_type_of_fixings)
                {
                    var typvalue = item.type_value == "Marketing Order" ? "Market Order" : item.type_value;
                    if (item.type_flag.ToUpper() == "Y")
                    {
                        row.Cells[icell].AddParagraph().AddFormattedText("\u006e", new Font("Wingdings")).AddFormattedText(" " + typvalue, new Font(FontName, FontSize - 1));
                    }
                    else
                    {
                        row.Cells[icell].AddParagraph().AddFormattedText("\u00A8", new Font("Wingdings")).AddFormattedText(" " + typvalue, new Font(FontName, FontSize - 1));
                    }
                    row.Cells[icell].Format.Font.Bold = true;
                    row.Cells[icell].Borders.Color = Colors.Transparent;
                    row.Cells[icell].Borders.Top.Color = Colors.Black;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    row.Cells[icell].MergeRight = 2;
                    icell += row.Cells[icell].MergeRight + 1;
                }
                row.Format.Font.Bold = false;
                row.Cells[icell].MergeRight = 9;
                row.Cells[icell].Borders.Left.Color = Colors.Black;
                row.Cells[icell].Borders.Bottom.Color = Colors.Black;



                row = table.AddRow(); icell = 0;
                row.HeadingFormat = true;
                row.Format.Font.Size = FontSize - 1;

                row.Format.Font.Bold = true;
                row.Format.Alignment = ParagraphAlignment.Left;
                row.VerticalAlignment = VerticalAlignment.Top;
                row.Cells[icell].AddParagraph("REASON TO\nCHARTER OUT");
                row.Cells[icell].MergeRight = 2;
                row.Cells[icell].Borders.Right.Color = Colors.Black;
                row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                icell += row.Cells[icell].MergeRight + 1;
                //row.Cells[icell].MergeRight = 15;

                Table tmpTableReason = new Table();
                ReasonForCharteringOut(tmpTableReason, modelReport.chos_chartering_method.explanation);
                row.Cells[icellDetails].Elements.Add(tmpTableReason);
                row.Cells[icellDetails].MergeRight = 15;
            }

        }

        public void ReasonForCharteringOut(Table table, string reason)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize;
            //table.Format.Alignment = ParagraphAlignment.Center;
            //table.Rows.Height = 4;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 30; c++)
            {
                column = table.AddColumn("0.5cm");
            }
            int icell = 0;
            Row row;
            row = table.AddRow(); icell = 0;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            //List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(nullToEmtry(reason), false);
            //List<htmlTagList> list = htmlRemove.RearrangeHtmlTagList(Lst);
            //if (list.Count > 0)
            //{
            //    for (int i = 0; i < list.Count; i++)
            //    {
            row = table.AddRow(); icell = 0;
            row.Format.Font.Size = FontSize - 1;
            row.Height = 30;
            pageHight += row.Height;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            Paragraph _prg = row.Cells[icell].AddParagraph();
            //_prg.AddFormattedText(HtmRemoval.ValuetoParagraph(modelReport.chos_chartering_method.explanation));
            if (!string.IsNullOrEmpty(reason))
            {
                htmlRemove.ValuetoParagraph(reason, _prg);
            }
            row.Cells[icell].MergeRight = 29;
            row.Cells[icell].Borders.Top.Color = Colors.White;

            //    }
            //}
            //else
            //{
            //    row = table.AddRow(); icell = 0;
            //    row.Format.Font.Size = FontSize - 1;
            //    row.Height = 12;
            //    pageHight += row.Height;
            //    row.Format.Font.Bold = true;
            //    row.Format.Alignment = ParagraphAlignment.Left;
            //    row.VerticalAlignment = VerticalAlignment.Top;
            //    row.Cells[icell].AddParagraph();
            //    row.Cells[icell].MergeRight = 29;
            //    row.Cells[icell].Borders.Top.Color = Colors.White;

            //}

            //row = table.AddRow();
            //row.Height = 30;
            //pageHight += row.Height;
            //if (!string.IsNullOrEmpty(reason))
            //{
            //    List<htmlTagList> Lst = htmlRemove.SplitHTMLTag(nullToEmtry(reason), false);
            //    List<htmlTagList> list = htmlRemove.RearrangeHtmlTagList(Lst);
            //    if (list.Count > 0)
            //    {
            //        for (int i = 0; i < list.Count; i++)
            //        {
            //            Paragraph _prg = row.Cells[0].AddParagraph();
            //            htmlRemove.ValuetoParagraph(list[i], _prg);
            //            //Paragraph _prg = row.Cells[icell].AddParagraph();
            //            //htmlRemove.ValuetoParagraph(list[i], _prg);
            //        }
            //    }

            //}
            //else
            //{
            //    row.Cells[0].AddParagraph("");
            //}
        }

        public void MakeTableDetail(ChosRootObject root, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Center;
            //table.Rows.Height = 10;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 30; c++)
            {
                column = table.AddColumn("0.5cm");
            }
            int icell = 0;
            //Row row = table.AddRow();
            //row.Format.Alignment = ParagraphAlignment.Center;
            //row.VerticalAlignment = VerticalAlignment.Center;

            //Row 1
            Row row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("OWNER :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Thaioil Co,Ltd");
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("OWNER'S BROKER : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.owner_broker_name));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;


            //Row 2
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("CHARTERER :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.charterer_name));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CHARTERER'S BROKER : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.charterer_broker_name));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;


            //Row 3
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("FREIGHT :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_evaluating.freight + " " + root.chos_evaluating.freight_detail));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("FLAT RATE : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_evaluating.flat_rate));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("AT YEAR : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_evaluating.year));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 3;


            //Row 4
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("VESSEL SIZE :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_evaluating.vessel_size));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("ROUTE : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.route));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;

            //Row 5
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("CHARTER PARTY :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.charter_party));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("LOADING PORTS : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.loading_ports));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;

            //Row 6
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("LAYCAN :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(root.chos_proposed_for_approve.laycan_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(root.chos_proposed_for_approve.laycan_to).SplitWord(" ")[0], "dd MMM yy"));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("DISCHARGING PORTS : ");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(root.chos_proposed_for_approve.discharging_ports));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 6;

            //Row 7
            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("VOYAGE PERIOD :");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 4;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(root.chos_evaluating.period_date_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(root.chos_evaluating.period_date_to).SplitWord(" ")[0], "dd MMM yy"));
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 5;


            row = table.AddRow();
        }

        private void NoCharterOutCaseA_Report(Section section, ChosRootObject modelReport, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = Color.Empty;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("A) NO CHARTER OUT CASE");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 18;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;

            // Details
            int icellDetails = 0;
            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            icellDetails = icell;

            Table tmpTable = new Table();
            MakeTableDetail_Expense(modelReport, tmpTable);
            row.Cells[icellDetails].Elements.Add(tmpTable);
            row.Cells[icellDetails].MergeRight = 15;
            row.Cells[icellDetails].Borders.Color = Colors.Black;
            row.Cells[icellDetails].Borders.Left.Color = Color.Empty;
        }

        public void MakeTableDetail_Expense(ChosRootObject root, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 30; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("EST. EXPENSE");
            row.Cells[icell].MergeRight = 13;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            // row.Cells[icell].Borders.Color = Colors.Black;

            row = table.AddRow();
            for (int i = 0; i < root.chos_no_charter_out_case_a.chos_est_expense.Count; i++)
            {
                Table tmpTable = new Table();
                MakeTableDetail_Expense_Sub(root.chos_no_charter_out_case_a.chos_est_expense[i], tmpTable, root.chos_evaluating.unit);
                row.Cells[icell].Elements.Add(tmpTable);
            }

            row.Cells[icell].MergeRight = 13;
            //row.Cells[icell].Borders.Left.Color = Colors.Black;
            //row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 13;
            // row.Cells[icell].Borders.Right.Color = Colors.Black;
            //row.Cells[icell].Borders.Left.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Height = 16;
            row.Cells[icell].AddParagraph("NO CHARTER OUT: TOTAL EXPENSE");
            //row.Cells[icell].Shading.Color = Colors.Gray;
            //row.Cells[icell].Format.LeftIndent = 2;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Size = FontSize - 2;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[icell].MergeRight = 10;
            //row.Cells[icell].Borders.Left.Color = Colors.Black;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;


            icell += row.Cells[icell].MergeRight + 9;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(root.chos_no_charter_out_case_a.total_expense_no_charter) + " " + root.chos_evaluating.unit);
            //row.Cells[icell].Shading.Color = Colors.Gray;
            //row.Cells[icell].Format.RightIndent = 3;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Size = FontSize - 2;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].MergeRight = 7;
            //row.Cells[icell].Borders.Right.Color = Colors.Black;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;


            //row = table.AddRow(); icell = 2;
            //row.Cells[icell].AddParagraph("BUNKER");
            //row.Cells[icell].MergeRight = 10;
            //icell += row.Cells[icell].MergeRight + 7;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.bunker_cost)) + " " + unit);
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 7;

            row = table.AddRow(); icell = 0;
        }

        public void MakeTableDetail_Expense_Sub(ChosEstExpense obj, Table table, string unit)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 3;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 26; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("PERIOD");
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 2;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_period_date_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_period_date_to).SplitWord(" ")[0], "dd MMM yy"));
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(obj.remarks));
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 17;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("TIME CHARTER COST");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph("no. days");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 4;

            row = table.AddRow(); icell = 2;
            row.Cells[icell].AddParagraph("TC HIRE");
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToZero(obj.tc_hire_no));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 4;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.tc_hire_cost)) + " " + unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 7;

            row = table.AddRow(); icell = 2;
            row.Cells[icell].AddParagraph("PORT CHANGE+AGENCY");
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 7;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.port_charge)) + " " + unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 7;

            row = table.AddRow(); icell = 2;
            row.Cells[icell].AddParagraph("BUNKER");
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 7;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.bunker_cost)) + " " + unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 7;

            row = table.AddRow(); icell = 2;
            row.Cells[icell].AddParagraph("OTHER COSTS");
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 7;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.other_costs)) + " " + unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 7;

            row = table.AddRow(); icell = 2;
            row.Cells[icell].AddParagraph("TOTAL TC COST");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 10;
            icell += row.Cells[icell].MergeRight + 7;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.total_tc_cost)) + " " + unit);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 7;

        }

        private void CharterOutCase_Report(Section section, ChosCharterOutCaseB obj, string unit, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Left.Color = Colors.Black;
            this.table.Borders.Right.Color = Colors.Black;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("7cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            column = this.table.AddColumn("9cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            Row row = table.AddRow(); int icell = 0;
            row.Cells[icell].AddParagraph("B) CHARTER OUT CASE");
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 2;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 2;

            // Detail
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            Table tmpTable = new Table();
            MakeTableCharterOutCaseBOne_Sub_Report(obj, unit, tmpTable);
            row.Cells[icell].Elements.Add(tmpTable);

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            Table tmpTable2 = new Table();
            MakeTableChosCharterOutCaseBTwo_Sub_Report(obj.chos_charter_out_case_b_two, obj.total_expense_b_two, unit, tmpTable2);
            row.Cells[icell].Elements.Add(tmpTable2);

            // Footer
            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Right.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBOne_Sub_Report(ChosCharterOutCaseB obj, string unit, Table table)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Format.Alignment = ParagraphAlignment.Center;
            table.Rows.Height = 8;
            var total = 0.0;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 28; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[icell].AddParagraph("B1) EST. INCOME");
            row.Cells[icell].MergeRight = 12;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Borders.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 12;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            for (int i = 0; i < obj.chos_charter_out_case_b_one.Count; i++)
            {
                Table tmpTable = new Table();
                total += Convert.ToDouble((nullToZero(obj.chos_charter_out_case_b_one[i].total_income)));
                MakeTableCharterOutCaseBOne_SubDetail_Report(obj.chos_charter_out_case_b_one[i], tmpTable, unit, obj.chos_charter_out_case_b_one.Count);
                row.Cells[icell].Elements.Add(tmpTable);


            }


            row = table.AddRow(); icell = 0;
            row.Cells[icell].Borders.Color = Color.Empty;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].AddParagraph("TOTAL INCOME");
            row.Cells[icell].Format.Font.Size = FontSize;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].MergeRight = 5;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;
            row.Cells[icell].Borders.Left.Color = Color.Empty;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(total.ToString()) + " " + unit);
            row.Cells[icell].Format.Font.Size = FontSize;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 6;


            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 12;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBOne_SubDetail_Report(ChosCharterOutCaseBOne obj, Table table, string unit, int chos_one)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 3;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 24; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("PERIOD");
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 4;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_period_date_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_period_date_to).SplitWord(" ")[0], "dd MMM yy"));
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 18;

            row = table.AddRow(); icell = 1;
            row.Cells[icell].AddParagraph("1");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CHARTER OUT");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].MergeRight = 7;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(obj.vessel_name + "   " + obj.voyage_no);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 12;

            row = table.AddRow(); icell = 4;
            row.Cells[icell].AddParagraph("Fixing WS");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.fixing_ws)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("%");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

            row = table.AddRow(); icell = 4;
            row.Cells[icell].AddParagraph("Est. Flat rate");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.est_flat_rate)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("$/MT");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

            row = table.AddRow(); icell = 4;
            row.Cells[icell].AddParagraph("Min loaded");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.min_loaded)));
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("MT");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;



            if (obj.chos_b_one_est_income_deduct != null)
            {
                for (int i = 0; i < obj.chos_b_one_est_income_deduct.Count; i++)
                {
                    row = table.AddRow(); icell = 5;
                    row.Cells[icell].AddParagraph("Deduct: " + nullToEmtry(obj.chos_b_one_est_income_deduct.ElementAt(i).name));
                    row.Cells[icell].MergeRight = 7;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_b_one_est_income_deduct.ElementAt(i).deduct)));
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                    row.Cells[icell].MergeRight = 8;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].AddParagraph("%");
                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;


                }
            }


            row = table.AddRow(); icell = 4;
            row.Cells[icell].AddParagraph("EST. FREIGHT COST");
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.freight)));
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(unit);
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

            if (chos_one > 1)
            {
                row = table.AddRow(); icell = 4;
                row.Cells[icell].AddParagraph("TOTAL");
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 8;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.total_income)));
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 8;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

            }



            //row.Cells[icell].MergeRight = 11;

            row = table.AddRow(); icell = 0;

            //row = table.AddRow(); icell = 1;
            //row.Cells[icell].AddParagraph("2");
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("DEMURRAGE");
            //row.Cells[icell].Format.Font.Bold = true;
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //row.Cells[icell].MergeRight = 7;
            //if (obj.other_expense != null)
            //{
            //    for (int i = 0; i < obj.other_expense.Count; i++)
            //    {
            //        row = table.AddRow(); icell = 4;
            //        row.Cells[icell].AddParagraph(nullToEmtry(obj.other_expense.ElementAt(i).other_key));
            //        row.Cells[icell].MergeRight = 6;
            //        icell += row.Cells[icell].MergeRight + 1;
            //        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.other_expense.ElementAt(i).other_value)) + " " + unit);
            //        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //        row.Cells[icell].MergeRight = 13;
            //    }
            //}

            //row = table.AddRow(); icell = 0;

            //row = table.AddRow(); icell = 0;
            //row.Cells[icell].AddParagraph("TOTAL INCOME");
            ////row.Cells[icell].Shading.Color = Colors.Gray;
            //row.Cells[icell].Format.Font.Size = FontSize;
            //row.Cells[icell].Format.Font.Bold = true;
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //row.Cells[icell].MergeRight = 10;
            ////row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.total_income)) + " " + unit);
            ////row.Cells[icell].Shading.Color = Colors.Gray;
            //row.Cells[icell].Format.Font.Size = FontSize;
            //row.Cells[icell].Format.Font.Bold = true;
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
            //row.Cells[icell].MergeRight = 13;

            row = table.AddRow(); icell = 0;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            //row.Cells[icell].MergeRight = 6;
        }

        public void MakeTableChosCharterOutCaseBTwo_Sub_Report(ChosCharterOutCaseBTwo obj, string expense_total, string unit, Table table)
        {

            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 8;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 17; c++)
            {
                column = table.AddColumn("0.5cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.VerticalAlignment = VerticalAlignment.Center;

            row.Cells[icell].AddParagraph("B2) EST. EXPENSE");
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].Format.Font.Size = FontSize - 1;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Borders.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            Table tmpTable = new Table();
            MakeTableCharterOutCaseBTwo_SubDetail_Report(obj, expense_total, tmpTable, unit);
            row.Cells[icell].Elements.Add(tmpTable);


            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 16;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        public void MakeTableCharterOutCaseBTwo_SubDetail_Report(ChosCharterOutCaseBTwo obj, string expense_total, Table table, string unit)
        {
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 3;
            table.Format.Alignment = ParagraphAlignment.Left;
            table.Rows.Height = 6;

            int hireno = Convert.ToInt32(nullToZero(obj.chos_case_cost_of_mt.tc_hire_no));
            int hirecost = Convert.ToInt32(nullToZero(obj.chos_case_cost_of_mt.tc_hire_cost_day.Replace(",", "")));
            int cost_voy = hireno * hirecost;

            Column column = table.AddColumn("0.25cm");
            for (int c = 0; c < 36; c++)
            {
                column = table.AddColumn("0.25cm");
            }

            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;

            if (obj.chos_case_cost_of_mt != null)
            {
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("PERIOD");
                row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 4;

                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_case_cost_of_mt.chos_period_date_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_case_cost_of_mt.chos_period_date_to).SplitWord(" ")[0], "dd MMM yy"));
                row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 26;

                row = table.AddRow(); icell = 2;
                row.Cells[icell].AddParagraph("1");
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("COST");
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 10;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_case_cost_of_mt.vessel_name) + " " + nullToEmtry(obj.chos_case_cost_of_mt.voyage_no));
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 16;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("TIME CHARTER COST");
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("no. days");
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("USD/Day");
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("USD/Voy");
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("TC HIRE");
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.tc_hire_no)));
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.tc_hire_cost_day)));
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(cost_voy.ToString()));
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].MergeRight = 2;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("PORT CHARGE+AGENCY");
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.port_charge)));
                row.Cells[icell].MergeRight = 14;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].MergeRight = 2;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("BUNKER");
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.bunker_cost)));
                row.Cells[icell].MergeRight = 14;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].MergeRight = 2;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("OTHER COSTS");
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.other_costs)));
                row.Cells[icell].MergeRight = 14;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].MergeRight = 2;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph("TOTAL TC COST");
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 9;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.total_tc_cost)));
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 14;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 2;



                row = table.AddRow(); int row_number = 0;
                if (obj.chos_case_cost_of_mt.chos_spot_in_vessel != null)
                {
                    for (int i = 0; i < obj.chos_case_cost_of_mt.chos_spot_in_vessel.Count; i++)
                    {
                        row_number = i + 2;


                        row = table.AddRow(); icell = 2;
                        row.Cells[icell].AddParagraph(row_number.ToString());
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("SPOT IN VESSEL: ");
                        row.Cells[icell].Format.Font.Underline = Underline.Single;
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].MergeRight = 10;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).vessel_name);
                        row.Cells[icell].MergeRight = 16;

                        row = table.AddRow(); icell = 0;
                        row.Cells[icell].AddParagraph("PERIOD");
                        row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
                        row.Cells[icell].MergeRight = 4;
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].Format.Font.Underline = Underline.Single;

                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("LOAD " + _FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).loading_date_from).SplitWord(" ")[0], "dd MMM yy") + " - " + _FN.ConvertDateFormatBackFormat(nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).loading_date_to).SplitWord(" ")[0], "dd MMM yy"));
                        row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].MergeRight = 26;



                        row = table.AddRow(); icell = 5;
                        row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).ws));
                        row.Cells[icell].MergeRight = 9;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).ws_percent)));
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].MergeRight = 14;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("%");
                        row.Cells[icell].MergeRight = 2;

                        row = table.AddRow(); icell = 5;
                        row.Cells[icell].AddParagraph("Flat rate");
                        row.Cells[icell].MergeRight = 9;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).flat_rate)));
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].MergeRight = 14;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("$/MT");
                        row.Cells[icell].MergeRight = 2;

                        row = table.AddRow(); icell = 5;
                        row.Cells[icell].AddParagraph("Min Loaded");
                        row.Cells[icell].MergeRight = 9;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).min_loaded)));
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].MergeRight = 14;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph("MT");
                        row.Cells[icell].MergeRight = 2;

                        if (obj.chos_case_cost_of_mt.chos_spot_in_vessel[i].chos_b_two_spot_in_deduct != null)
                        {
                            if (obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).deduct_action == "Deduct Before")
                            {
                                for (int j = 0; j < obj.chos_case_cost_of_mt.chos_spot_in_vessel[i].chos_b_two_spot_in_deduct.Count; j++)
                                {


                                    row = table.AddRow(); icell = 5;
                                    row.Cells[icell].AddParagraph("Deduct: " + nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).chos_b_two_spot_in_deduct.ElementAt(j).name));
                                    row.Cells[icell].MergeRight = 9;
                                    icell += row.Cells[icell].MergeRight + 1;
                                    row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).chos_b_two_spot_in_deduct.ElementAt(j).deduct)));
                                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                                    row.Cells[icell].MergeRight = 14;
                                    icell += row.Cells[icell].MergeRight + 1;
                                    row.Cells[icell].AddParagraph("%");
                                    row.Cells[icell].MergeRight = 2;
                                    // row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

                                }
                            }
                        }


                        if (obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).additional != null)
                        {

                            row = table.AddRow(); icell = 5;
                            row.Cells[icell].AddParagraph("Additional: " + nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).additional_name));
                            row.Cells[icell].MergeRight = 9;
                            icell += row.Cells[icell].MergeRight + 1;
                            row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).additional)));
                            row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                            row.Cells[icell].MergeRight = 14;
                            icell += row.Cells[icell].MergeRight + 1;
                            row.Cells[icell].AddParagraph(unit);
                            row.Cells[icell].MergeRight = 2;
                        }

                        if (obj.chos_case_cost_of_mt.chos_spot_in_vessel[i].chos_b_two_spot_in_deduct != null)
                        {
                            if (obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).deduct_action == "Deduct After")
                            {
                                for (int j = 0; j < obj.chos_case_cost_of_mt.chos_spot_in_vessel[i].chos_b_two_spot_in_deduct.Count; j++)
                                {


                                    row = table.AddRow(); icell = 5;
                                    row.Cells[icell].AddParagraph("Deduct: " + nullToEmtry(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).chos_b_two_spot_in_deduct.ElementAt(j).name));
                                    row.Cells[icell].MergeRight = 9;
                                    icell += row.Cells[icell].MergeRight + 1;
                                    row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).chos_b_two_spot_in_deduct.ElementAt(j).deduct)));
                                    row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                                    row.Cells[icell].MergeRight = 14;
                                    icell += row.Cells[icell].MergeRight + 1;
                                    row.Cells[icell].AddParagraph("%");
                                    row.Cells[icell].MergeRight = 2;
                                    // row.Cells[icell].Format.Alignment = ParagraphAlignment.Left;

                                }
                            }
                        }

                        row = table.AddRow(); icell = 5;
                        row.Cells[icell].AddParagraph("EST.FREIGHT AMOUNT");
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].MergeRight = 9;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_spot_in_vessel.ElementAt(i).freight)));
                        row.Cells[icell].Format.Font.Underline = Underline.Single;
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].MergeRight = 14;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].MergeRight = 2;

                        row = table.AddRow();
                    }
                }

                row = table.AddRow(); icell = 2;
                row.Cells[icell].AddParagraph((row_number + 1).ToString());
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("OTHER EXPENSE: ");
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 10;

                if (obj.chos_case_cost_of_mt.chos_other_expense != null)
                {
                    for (int i = 0; i < obj.chos_case_cost_of_mt.chos_other_expense.Count; i++)
                    {
                        row = table.AddRow(); icell = 7;
                        row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_case_cost_of_mt.chos_other_expense.ElementAt(i).other_expense));
                        row.Cells[icell].MergeRight = 7;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(obj.chos_case_cost_of_mt.chos_other_expense.ElementAt(i).amount)));
                        row.Cells[icell].Format.Font.Underline = Underline.Single;
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                        row.Cells[icell].MergeRight = 14;
                        icell += row.Cells[icell].MergeRight + 1;
                        row.Cells[icell].AddParagraph(unit);
                        row.Cells[icell].Format.Font.Bold = true;
                        row.Cells[icell].MergeRight = 2;
                    }
                }

                row = table.AddRow();

                row = table.AddRow(); icell = 3;
                row.Cells[icell].AddParagraph("TOTAL EXPENSE");
                //row.Cells[icell].Shading.Color = Colors.Gray;
                row.Cells[icell].Format.Font.Size = FontSize;
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 11;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(_FN.intTostrMoney(nullToZero(expense_total)));
                //row.Cells[icell].Shading.Color = Colors.Gray;
                row.Cells[icell].Format.Font.Size = FontSize;
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 14;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph(unit);
                row.Cells[icell].Format.Font.Size = FontSize;
                row.Cells[icell].Format.Font.Underline = Underline.Single;
                row.Cells[icell].Format.Font.Bold = true;
                row.Cells[icell].MergeRight = 2;
            }
        }

        private void ProposedForApprove_Report(Section section, ChosRootObject obj, bool AttachPage = false)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = Color.Empty;
            this.table.Format.Font.Size = FontSize - 2;
            this.table.Format.Font.Bold = true;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("0.5cm");
            column.Format.Alignment = ParagraphAlignment.Left;

            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }
            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row.Cells[icell].MergeRight = 36;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Right.Color = Colors.Black;


            row = table.AddRow(); icell = 0;
            row.Cells[icell].AddParagraph("BENEFIT :");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("A) NO CHARTER OUT : TOTAL EXPENSE");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph("B)=B2)-B1) CHARTER OUT : TOTAL EXPENSE");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 10;
            row.Cells[icell].Borders.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph("A) - B) NET BENEFIT");
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Shading.Color = Colors.WhiteSmoke;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_benefit.chos_total_expense_no_charter) == "" ? "" : _FN.intTostrMoney(nullToZero(obj.chos_benefit.chos_total_expense_no_charter)) + " " + obj.chos_evaluating.unit);
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_benefit.chos_total_expense) == "" ? "" : _FN.intTostrMoney(nullToZero(obj.chos_benefit.chos_total_expense)) + " " + obj.chos_evaluating.unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 10;
            row.Cells[icell].Borders.Color = Colors.Black;

            icell += row.Cells[icell].MergeRight + 2;
            row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_benefit.chos_net_benefit) == "" ? "" : _FN.intTostrMoney(nullToZero(obj.chos_benefit.chos_net_benefit)) + " " + obj.chos_evaluating.unit);
            row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[icell].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 36;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
        }

        private void NoteTimelineSummaryReport(Section section, ChosRootObject obj)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Format.Font.Bold = true;
            this.table.Format.Font.Size = FontSize - 2;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Left;
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
                column.Format.Alignment = ParagraphAlignment.Left;
            }
            Row row = table.AddRow(); int icell = 0;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("NOTE");
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].MergeRight = 2;

            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph(nullToEmtry(obj.chos_note));
            row.Cells[icell].Format.Font.Bold = false;
            row.Cells[icell].MergeRight = 15;
            //row.Cells[icell].Borders.Right.Color = Colors.Black;
            row.Cells[icell].Borders.Color = Colors.Black;
            row.Cells[icell].Borders.Top.Color = Color.Empty;
            row.Cells[icell].Borders.Bottom.Color = Color.Empty;
            row.Cells[icell].Borders.Left.Color = Color.Empty;


            row = table.AddRow(); icell = 0;
            row.Cells[icell].MergeRight = 17;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].Borders.Right.Color = Colors.Black;

        }

        private void FooterReport2(Section section, ChosRootObject modelReport)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
            //row.Cells[icell].MergeRight = 37;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

        }

        private void FooterReport(Section section, ChosRootObject model)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;

            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            var approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            // _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            approveName_1 = _FN.GetNameOffApprove(model.approve_items, "APPROVE_1");
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_2", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            // approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if (!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_3", ref height, ref width));
            //else
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(model.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_1") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = !String.IsNullOrEmpty(approveName_1) ? _FN.GetNameOffApprove(model.approve_items, "APPROVE_2") : _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            //approveName = _FN.GetNameOffApprove(model.approve_items, "APPROVE_3");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

        }

        private void FooterApproveReport(Section section, ChosRootObject modelReport)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
            //row.Cells[icell].MergeRight = 37;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            double height = 0; double width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width, true));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_3");
            height = 0; width = 0;
            if (!string.IsNullOrEmpty(approveName))
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_2");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;



            //this.table = section.AddTable();
            //this.table.Style = "Table";
            //this.table.Format.Font.Name = FontName;
            //this.table.Borders.Left.Color = Colors.Black;
            //this.table.Borders.Right.Color = Colors.Black;
            //this.table.Format.Font.Size = FontSize - 2;
            //this.table.Format.Font.Bold = true;
            //this.table.Rows.Height = defaultHeight;
            //this.table.Format.Alignment = ParagraphAlignment.Center;

            //Column column = this.table.AddColumn("5cm");
            //column = this.table.AddColumn("4.5cm");
            //column = this.table.AddColumn("5cm");
            //column = this.table.AddColumn("4.5cm");


            //Row row = table.AddRow(); int icell = 0;

            ////row = table.AddRow(); icell = 0;
            //row.Cells[icell].AddParagraph("REQUESTED BY");
            //Table tmpTable1 = new Table();
            ////RequestedBySubApproveReport(modelReport, tmpTable1, "CMCS-Anawat");
            //row.Cells[icell].Elements.Add(tmpTable1);

            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("VERIFIED BY");
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //Table tmpTable2 = new Table();
            ////Cert_ApproveBySubApproveReport(modelReport, tmpTable2, "CMCS");
            //row.Cells[icell].Elements.Add(tmpTable2);

            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("CERTIFIED BY");
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //Table tmpTable3 = new Table();
            ////RequestedBySubApproveReport(modelReport, tmpTable3, "CMVP");
            //row.Cells[icell].Elements.Add(tmpTable3);

            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].AddParagraph("APPROVED BY");
            //row.Cells[icell].Format.Font.Underline = Underline.Single;
            //Table tmpTable4 = new Table();
            ////Cert_ApproveBySubApproveReport(modelReport, tmpTable4, "EVPC");
            //row.Cells[icell].Elements.Add(tmpTable4);

            ////row = table.AddRow(); icell = 0;
            //icell = 0;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            //icell += row.Cells[icell].MergeRight + 1;
            //row.Cells[icell].Borders.Bottom.Color = Colors.Black;
        }

        private void RequestedBySubApproveReport(ChosRootObject obj, Table table, string ptBy)
        {
            Image _img;
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Font.Bold = true;
            table.Rows.Height = 8;
            double height = 0; double width = 0;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 14; c++)
            {
                column = table.AddColumn("0.5cm");
            }
            Row row = table.AddRow(); int icell = 0;

            if (ptBy.Equals("CMCS-Anawat"))
            {

                //CHARTERER
                row.Cells[icell].AddParagraph("Trader:");
                row.Cells[icell].VerticalAlignment = VerticalAlignment.Bottom;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 2;
                icell += row.Cells[icell].MergeRight + 1;

                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(obj.approve_items, "APPROVE_1", ref height, ref width));
                if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
                else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
                else _img.Width = new Unit(3, UnitType.Centimeter);
                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                row.Cells[icell].MergeRight = 5;
                row.Cells[icell].Borders.Bottom.Color = Colors.Black;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(obj.approve_items, "APPROVE_1")));
                row.Cells[icell].MergeRight = 5;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            }
            else if (ptBy.Equals("CMCS"))
            {
                //S / H
                row = table.AddRow(); icell = 0;
                row.Cells[icell].AddParagraph("S/H:");
                row.Cells[icell].VerticalAlignment = VerticalAlignment.Bottom;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Right;
                row.Cells[icell].MergeRight = 2;
                icell += row.Cells[icell].MergeRight + 1;

                height = 0; width = 0;
                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(obj.approve_items, "APPROVE_1", ref height, ref width, true));
                if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
                else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
                else _img.Width = new Unit(3, UnitType.Centimeter);
                row.Cells[icell].MergeRight = 5;
                row.Cells[icell].Borders.Bottom.Color = Colors.Black;

                row = table.AddRow(); icell = 5;
                row.Cells[icell].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(obj.approve_items, "APPROVE_1", true)));
                row.Cells[icell].MergeRight = 5;
                row.Cells[icell].Format.Alignment = ParagraphAlignment.Center;
            }

        }

        private void Cert_ApproveBySubApproveReport(ChosRootObject obj, Table table, string ptBy)
        {
            Image _img;
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 2;
            table.Format.Font.Bold = true;
            table.Rows.Height = 8;
            table.Format.Alignment = ParagraphAlignment.Center;
            double height = 0; double width = 0;

            Column column = table.AddColumn("0.5cm");
            for (int c = 0; c < 14; c++)
            {
                column = table.AddColumn("0.5cm");
            }
            Row row = table.AddRow(); int icell = 0;
            row.VerticalAlignment = VerticalAlignment.Bottom;
            row = table.AddRow(); icell = 3;

            if (ptBy.Equals("CMVP"))
            {
                //Approve2
                height = 0; width = 0;
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(obj.approve_items, "APPROVE_2", ref height, ref width));
                if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
                else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
                else _img.Width = new Unit(3, UnitType.Centimeter);
                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            }
            else if (ptBy.Equals("EVPC"))
            {
                //Approve3
                height = 0; width = 0;
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(obj.approve_items, "APPROVE_3", ref height, ref width));
                if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
                else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
                else _img.Width = new Unit(3, UnitType.Centimeter);
                _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            }
            row.Cells[icell].Borders.Bottom.Color = Colors.Black;
            row.Cells[icell].MergeRight = 5;
            row = table.AddRow(); icell = 0;
            row = table.AddRow(); icell = 3;
            row.Cells[icell].AddParagraph(ptBy);
            row.Cells[icell].MergeRight = 5;

        }

        private string nullToEmtry(string value)
        {
            return value == null ? "" : value;
        }

        private string nullToZero(string value)
        {
            return value == null ? "0" : value;
        }

        private void OfferTableZone(Section section, ChotRootObject modelReport, int itemOfferIndex)
        {

            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize - 2;
            this.table.Rows.Height = defaultHeight;

            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            #region--------HeaddingTable-------------



            Row row = table.AddRow();
            row.Cells[0].AddParagraph("Table1 : Applied for \"Fixing Counter\"");
            row.Cells[0].MergeRight = 37;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Left;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;

            int offer_count = 0;
            int offer_width = 0;
            int fixing_width = 0;
            if (itemOfferIndex + 5 <= modelReport.chot_negotiation_summary.chot_offers_items[0].chot_round_items.Count)
            {
                offer_count = 5;
            }
            else
            {
                offer_count = modelReport.chot_negotiation_summary.chot_offers_items[0].chot_round_items.Count % 5;
            }
            switch (offer_count)
            {
                case 1: offer_width = 16; fixing_width = 7; break;
                case 2: offer_width = 8; fixing_width = 7; break;
                case 3: offer_width = 6; fixing_width = 5; break;
                case 4: offer_width = 4; fixing_width = 7; break;
                case 5: offer_width = 4; fixing_width = 3; break;
                default: offer_width = 4; fixing_width = 3; break;
            }

            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            row.Cells[icell].AddParagraph("Broker");
            row.Cells[icell].MergeRight = 4;
            row.Cells[0].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;

            int icellQ = icell;
            row.Cells[icell].AddParagraph("Charterer");
            row.Cells[icell].MergeRight = 3;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;
            int icelR2 = icell;

            row.Cells[icell].AddParagraph("Offer and Counter");
            row.Cells[icell].MergeRight = 22;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("Final Deal");
            row.Cells[icell].MergeRight = 5;
            row.Cells[icell].MergeDown = 2;
            icell += row.Cells[icell].MergeRight + 1;

            // Fixing Items
            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Fixing Items");
            row.Cells[icell].MergeRight = fixing_width - 1;
            row.Cells[icell].MergeDown = 1;
            icell += row.Cells[icell].MergeRight + 1;
            icelR2 = icell;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph(string.Format("{0}", _FN.GetORDINALNUMBERS(itemOfferIndex + (i + 1))));
                row.Cells[icell].MergeRight = offer_width - 1;
                if (i != offer_count - 1)
                    icell += row.Cells[icell].MergeRight + 1;
            }

            icell = icelR2;
            row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            for (int i = 0; i < offer_count; i++)
            {
                row.Cells[icell].AddParagraph("Offer");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
                row.Cells[icell].AddParagraph("Counter");
                row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                icell += row.Cells[icell].MergeRight + 1;
            }

            #endregion

            if (modelReport.chot_negotiation_summary.chot_offers_items.Count > 0)
            {
                modelReport.chot_negotiation_summary.chot_offers_items = modelReport.chot_negotiation_summary.chot_offers_items.OrderByDescending(p => p.final_flag).ToList();
            }
            //Loop Bind Data
            for (int i = 0; i < modelReport.chot_negotiation_summary.chot_offers_items.Count; i++)
            {

                var _offerItem = modelReport.chot_negotiation_summary.chot_offers_items[i];
                int MergeDown = _offerItem.chot_round_items[itemOfferIndex].chot_round_info.Count < 3 ? 2 : _offerItem.chot_round_items[itemOfferIndex].chot_round_info.Count - 1;
                icell = 0;
                row = table.AddRow();
                row.Format.Alignment = ParagraphAlignment.Center;
                row.VerticalAlignment = VerticalAlignment.Center;
                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.broker_name, 2));
                row.Cells[icell].MergeRight = 4;
                row.Cells[icell].MergeDown = MergeDown;
                icell += row.Cells[icell].MergeRight + 1;

                row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.charterer_name, 2));
                row.Cells[icell].MergeRight = 3;
                row.Cells[icell].MergeDown = MergeDown;

                icell += row.Cells[icell].MergeRight + 1;


                int rcell = icell; bool firstrow = true; string tmpVal = "";
                int count = 0;
                foreach (var item in _offerItem.chot_round_items[itemOfferIndex].chot_round_info)
                {
                    tmpVal = "";
                    if (!firstrow)
                    {
                        row = table.AddRow();
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                    }

                    icell = rcell;
                    row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], item.type, fixing_width - 1));
                    row.Cells[icell].MergeRight = fixing_width - 1;
                    row.Cells[icell].Borders.Bottom.Color = Colors.Black;
                    icell += row.Cells[icell].MergeRight + 1;
                    for (int j = 0; j < offer_count; j++)
                    {
                        tmpVal = _offerItem.chot_round_items[itemOfferIndex + j].chot_round_info.Where(x => x.type == item.type).First().offer;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal), 3));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                        tmpVal = _offerItem.chot_round_items[itemOfferIndex + j].chot_round_info.Where(x => x.type == item.type).First().counter;
                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _FN.intTostrMoney(tmpVal), 3));
                        row.Cells[icell].MergeRight = (offer_width / 2) - 1;
                        icell += row.Cells[icell].MergeRight + 1;
                    }
                    if (firstrow)
                    {

                        row.Cells[icell].AddParagraph(_RFN.AdjustIfTooWideToFitIn(row.Cells[icell], _offerItem.final_deal, 5));
                        row.Cells[icell].MergeRight = 5;
                        row.Cells[icell].MergeDown = MergeDown;
                        firstrow = false;
                    }

                }

            }
        }

        private void BottomZoneReport(Section section, ChotRootObject modelReport)
        {
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = defaultHeight;
            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }
            int icell = 0;
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            row.Cells[icell].Format.Font.Size = 12;
            row.Cells[icell].Format.Font.Underline = Underline.Single;
            row.Cells[icell].Format.Font.Bold = true;
            row.Cells[icell].MergeRight = 37;
            row.Cells[icell].AddParagraph("Charter Out Evaluation");

            row = table.AddRow(); icell = 0;
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Top;
            Table tmpTable = new Table();
            MakeTableFinalFixDetail(modelReport.chot_proposed_for_approve, modelReport.chot_detail, tmpTable);
            row.Cells[icell].MergeRight = 37;
            row.Cells[icell].Elements.Add(tmpTable);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;


        }

        public void MakeTableFinalFixDetail(ChotProposedForApprove Detail, ChotDetail Cargo, Table table)
        {
            table.Borders.Color = Colors.Black;
            table.Format.Font.Name = FontName;
            table.Format.Font.Size = FontSize - 1;
            table.Rows.Height = 8;
            table.AddColumn("3cm");
            table.AddColumn("3cm");
            table.AddColumn("5cm");
            table.AddColumn(new Unit(7.7, UnitType.Centimeter));

            int icell = 0;
            #region-----------------Table2----------------------
            Row row = table.AddRow();
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Freight");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Lumpsum : ", TextFormat.Bold).AddFormattedText(Detail.freight_lumpsum, TextFormat.NotBold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("USD/MT : ", TextFormat.Bold).AddFormattedText(Detail.freight_rate_mt, TextFormat.NotBold);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Estimated Total Freight (USD) : ", TextFormat.Bold).AddFormattedText(Detail.freight_rate_est_total, TextFormat.NotBold);
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Others : ", TextFormat.Bold).AddFormattedText(Detail.freight_others, TextFormat.NotBold);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Estimated Total Freight (USD) : ", TextFormat.Bold).AddFormattedText(Detail.freight_others_est_total, TextFormat.NotBold);
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Format.Font.Bold = false;

            // Estimated Total Variable Cost
            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Estimated Total Variable Cost");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Bunler (USD) : ", TextFormat.Bold).AddFormattedText(Detail.est_bunker, TextFormat.NotBold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Port Charge (USD) : ", TextFormat.Bold).AddFormattedText(Detail.est_port_change, TextFormat.NotBold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Total Commission : ", TextFormat.Bold).AddFormattedText(Detail.est_total_commission, TextFormat.NotBold).AddFormattedText("% (USD)", TextFormat.Bold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            // Estimated Profit Over Variable Cost
            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Estimated Profit Over Variable Cost");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText(Detail.est_profit, TextFormat.NotBold).AddFormattedText(" (USD)", TextFormat.Bold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            // Market Freight Reference
            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("Market Freight Reference");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Lumpsum : ", TextFormat.Bold).AddFormattedText(Detail.mrk_freight_lumpsum, TextFormat.NotBold);
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("USD/MT : ", TextFormat.Bold).AddFormattedText(Detail.mrk_freight_rate_mt, TextFormat.NotBold);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Estimated Total Freight (USD) : ", TextFormat.Bold).AddFormattedText(Detail.mrk_freight_rate_est_total, TextFormat.NotBold);
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Format.Font.Bold = false;

            row = table.AddRow(); icell = 0;
            row.Format.Font.Bold = true;
            row.Format.Alignment = ParagraphAlignment.Left;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Cells[icell].AddParagraph("");
            row.Cells[icell].MergeRight = 1;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Others : ", TextFormat.Bold).AddFormattedText(Detail.mrk_freight_others, TextFormat.NotBold);
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph().AddFormattedText("Estimated Total Freight (USD) : ", TextFormat.Bold).AddFormattedText(Detail.mrk_freight_others_est_total, TextFormat.NotBold);
            row.Cells[icell].Borders.Left.Color = Colors.White;
            row.Cells[icell].Format.Font.Bold = false;


            #endregion
        }
        private void FooterReport(Section section, ChotRootObject modelReport)
        {
            Image _img;
            string approveName = "";
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("0.5cm");
            for (int i = 0; i < 37; i++)
            {
                column = this.table.AddColumn("0.5cm");
            }

            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            int icell = 0;
            //row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy.");
            //row.Cells[icell].MergeRight = 37;

            //icell = 0;
            //row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.VerticalAlignment = VerticalAlignment.Center;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Requested by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Verified by");
            row.Cells[icell].MergeRight = 9;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Endorsed by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("Approved by");
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Borders.Color = Colors.Transparent;
            //row.Borders.Bottom.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;
            row.Cells[icell].AddParagraph("Trader");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Trader
            var approveName_1 = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1");
            double height = 0; double width = 0;
            if (!String.IsNullOrEmpty(approveName_1))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].AddParagraph("S/H");
            row.Cells[icell].MergeRight = 1;
            row.Cells[icell].Borders.Left.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //S/H
            var approveName_2 = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_2");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            height = 0; width = 0;
            if (!String.IsNullOrEmpty(approveName_2))
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width));
            }
            else
            {
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            }
            //_img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            row.Cells[3].Borders.Bottom.Color = Colors.White;
            row.Cells[icell].MergeRight = 7;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            //Approve4
            height = 0; width = 0;
            _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[37].Borders.Right.Color = Colors.Black;

            //Approve3
            //approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_3");
            height = 0; width = 0;
            //if (!string.IsNullOrEmpty(approveName))
            //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
            //else
                _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_4", ref height, ref width));
            if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
            else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
            else _img.Width = new Unit(3, UnitType.Centimeter);
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Right.Color = Colors.Black;
            icell += row.Cells[icell].MergeRight + 1;

            icell = 0;
            row = table.AddRow();
            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Borders.Top.Color = Colors.White;
            row.HeadingFormat = true;
            row.Format.Font.Bold = true;

            approveName = !String.IsNullOrEmpty(approveName_1)? _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1") : _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_2");
            
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            icell += row.Cells[icell].MergeRight + 1;

            row.Cells[icell].Borders.Right.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            approveName = !String.IsNullOrEmpty(approveName_2) ? _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_2") : _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_3");
            //approveName = _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_3");
            row.Cells[icell].AddParagraph(string.Format("( CMCS{0} )", !String.IsNullOrEmpty(approveName) ? "-" + approveName : ""));
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            row.Cells[icell].Borders.Left.Color = Colors.White;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("CMVP");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;
            row.Cells[icell].AddParagraph("EVPC");
            _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
            row.Cells[icell].MergeRight = 8;
            icell += row.Cells[icell].MergeRight + 1;

        }
        ////private void FooterReport(Section section, ChotRootObject modelReport)
        //{
        //    Image _img;
        //    this.table = section.AddTable();
        //    this.table.Style = "Table";
        //    this.table.Format.Font.Name = FontName;
        //    this.table.Borders.Color = TableBorder;
        //    this.table.Format.Font.Size = FontSize;
        //    this.table.Rows.Height = 18;
        //    Column column = this.table.AddColumn("0.5cm");
        //    for (int i = 0; i < 37; i++)
        //    {
        //        column = this.table.AddColumn("0.5cm");
        //    }

        //    Row row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Center;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    int icell = 0;
        //    row.Cells[icell].AddParagraph("Requested, certified and approved can be done either via e-mail or hard copy");
        //    row.Cells[icell].MergeRight = 37;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    icell = 0;
        //    row = table.AddRow();
        //    row.HeadingFormat = true;
        //    row.Format.Alignment = ParagraphAlignment.Left;
        //    row.VerticalAlignment = VerticalAlignment.Center;
        //    row.Format.Font.Bold = true;
        //    row.Cells[icell].AddParagraph("Requested By");
        //    row.Cells[icell].MergeRight = 17;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Certified by");
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("Approved by");
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    icell = 0;
        //    row = table.AddRow();
        //    row.Borders.Bottom.Color = Colors.White;
        //    row.HeadingFormat = true;
        //    row.Format.Font.Bold = true;
        //    row.Cells[icell].AddParagraph("Charterer");
        //    row.Cells[icell].MergeRight = 2;
        //    row.Cells[icell].Borders.Right.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    //Trader
        //    double height = 0; double width = 0;
        //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width));
        //    if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
        //    else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
        //    else _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[icell].MergeRight = 5;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row.Cells[icell].AddParagraph("S/H");
        //    row.Cells[icell].MergeRight = 1;
        //    row.Cells[icell].Borders.Right.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    //S/H
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    height = 0; width = 0;
        //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_1", ref height, ref width, true));
        //    if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
        //    else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
        //    else _img.Width = new Unit(3, UnitType.Centimeter);
        //    row.Cells[3].Borders.Bottom.Color = Colors.White;
        //    row.Cells[icell].MergeRight = 6;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    //Approve2
        //    height = 0; width = 0;
        //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_2", ref height, ref width));
        //    if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
        //    else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
        //    else _img.Width = new Unit(3, UnitType.Centimeter);
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    //Approve3
        //    height = 0; width = 0;
        //    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetPathSignature(modelReport.approve_items, "APPROVE_3", ref height, ref width));
        //    if (height > 0) _img.Height = new Unit(height, UnitType.Centimeter);
        //    else if (width > 0) _img.Width = new Unit(width, UnitType.Centimeter);
        //    else _img.Width = new Unit(3, UnitType.Centimeter);
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;

        //    icell = 0;
        //    row = table.AddRow();
        //    row.Cells[icell].Borders.Right.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Borders.Top.Color = Colors.White;
        //    row.HeadingFormat = true;
        //    row.Format.Font.Bold = true;
        //    row.Cells[6].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1")));
        //    row.Cells[icell].MergeRight = 7;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    icell += row.Cells[icell].MergeRight + 1;

        //    row.Cells[icell].Borders.Right.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[6].AddParagraph(string.Format("({0}-{1})", type, _FN.GetNameOffApprove(modelReport.approve_items, "APPROVE_1", true)));
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    row.Cells[icell].MergeRight = 7;
        //    row.Cells[icell].Borders.Left.Color = Colors.White;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("CMVP");
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;
        //    row.Cells[icell].AddParagraph("EVPC");
        //    _RFN.SetCellAlignment(row.Cells[icell], ReportAlignment.CC);
        //    row.Cells[icell].MergeRight = 9;
        //    icell += row.Cells[icell].MergeRight + 1;

        //}

        private void ExplanImageReport(Section section, ChosRootObject modelReport)
        {
            Image _img;
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Format.Font.Name = FontName;
            this.table.Borders.Color = TableBorder;
            this.table.Format.Font.Size = FontSize;
            this.table.Rows.Height = 18;
            Column column = this.table.AddColumn("1cm");
            for (int i = 0; i < 18; i++)
            {
                column = this.table.AddColumn("1cm");
            }



            string[] _image = modelReport.explanationAttach.Split('|');
            foreach (var _item in _image)
            {
                if (_item != "")
                {
                    string[] itemsplit = _item.ToString().Split(':');
                    Row row = table.AddRow();
                    int icell = 0;
                    row.Format.Alignment = ParagraphAlignment.Center;
                    row.VerticalAlignment = VerticalAlignment.Center;
                    row.Format.Font.Bold = true;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    icell += row.Cells[icell].MergeRight + 1;
                    _img = row.Cells[icell].AddParagraph().AddImage(_FN.GetExplanImage(itemsplit[0], URL));
                    if (!string.IsNullOrEmpty(URL))
                    {
                        images.Add(_img.Name);
                    }
                    _img.Width = new Unit(11, UnitType.Centimeter);
                    row.Cells[icell].Borders.Right.Color = Colors.White;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].MergeRight = 12;
                    icell += row.Cells[icell].MergeRight + 1;
                    row.Cells[icell].Borders.Left.Color = Colors.White;
                    row.Cells[icell].AddParagraph("");
                    row.Cells[icell].MergeRight = 2;
                    row.Borders.Bottom.Color = Colors.White;
                    if (itemsplit.Length > 0)
                    {
                        row = table.AddRow();
                        icell = 0;
                        row.Format.Alignment = ParagraphAlignment.Center;
                        row.VerticalAlignment = VerticalAlignment.Center;
                        row.Format.Font.Bold = true;
                        row.Cells[icell].AddParagraph(itemsplit[1]);
                        row.Cells[icell].MergeRight = 18;
                    }
                }
            }
        }

        #endregion
    }
}