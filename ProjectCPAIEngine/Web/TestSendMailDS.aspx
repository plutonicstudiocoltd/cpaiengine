﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestSendMailDS.aspx.cs" Inherits="ProjectCPAIEngine.Web.TestSendMailDS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Test Send Mail"></asp:Label>
        </div>
        <div>
            <asp:Label ID="Label2" runat="server" Text="Sender : "></asp:Label>
            <asp:TextBox ID="sender1" runat="server">zariya</asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label7" runat="server" Text="Sender password : "></asp:Label>
            <asp:TextBox ID="password1" runat="server">Kungpoo8</asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label8" runat="server" Text="From : "></asp:Label>
            <asp:TextBox ID="from1" runat="server">zariya@thaioilgroup.com</asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label3" runat="server" Text="Recive : "></asp:Label>
            <asp:TextBox ID="recive" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label4" runat="server" Text="Subject : "></asp:Label>
            <asp:TextBox ID="subject" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label5" runat="server" Text="Body : "></asp:Label>
            <asp:TextBox ID="body" runat="server" TextMode="MultiLine" Height="150" Width="200"></asp:TextBox>
        </div>
        <div>
            <asp:Label ID="Label6" runat="server" Text="Attachment : "></asp:Label>
            <asp:FileUpload ID="attachment" runat="server" />
        </div>
        <div>
            <asp:Button ID="sendbtn" runat="server" Text="Send" OnClick="sendBtn_Click" />
            <asp:Button ID="sendbtn1" runat="server" Text="Send(ok)" OnClick="sendBtn_Click1" />
        </div>
        <div>
            <asp:TextBox ID="error" runat="server" Height="48px" Width="1081px"></asp:TextBox>
        </div>

        <br />
        <br />

        <table style="width: 100%">
            <tr>
                <td style="width: 50%">
                    <div>
                        <asp:Label ID="Label9" Style="font-weight: bold" runat="server" Text="::::: Test Pricing :::::"></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="Label10" runat="server" Text="Benchmark: "></asp:Label>
                        <asp:TextBox ID="txtBenchmark" runat="server">OSP</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label11" runat="server" Text="Pricing Status : "></asp:Label>
                        <asp:TextBox ID="txtPricingStatus" runat="server">P</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label12" runat="server" Text="Date : "></asp:Label>
                        <asp:TextBox ID="txtDate" runat="server" Width="200px">01/01/2017 to 31/01/2017</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label13" runat="server" Text="Material : "></asp:Label>
                        <asp:TextBox ID="txtMat" runat="server">PHET</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label14" runat="server" Text="Crude Vol : "></asp:Label>
                        <asp:TextBox ID="txtCrudeVol" runat="server"></asp:TextBox>
                    </div>

                    <div>
                        <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="ButtonPrice_Click" />
                    </div>
                    <div>
                        <asp:GridView ID="GridView1" runat="server">
                        </asp:GridView>
                    </div>
                </td>
                <td style="width: 50%">
                    <div>
                        <asp:Label ID="Label15" Style="font-weight: bold" runat="server" Text="::::: Test Exchange Rate :::::"></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="Label16" runat="server" Text="BL Date: "></asp:Label>
                        <asp:TextBox ID="txtBLDate" runat="server">30/12/2016</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label17" runat="server" Text="Complete Time : "></asp:Label>
                        <asp:TextBox ID="txtComTime" runat="server">00:00</asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="Label18" runat="server" Text="Transport Type : "></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlTransportType">
                            <asp:ListItem Value="VESSEL">VESSEL</asp:ListItem>
                            <asp:ListItem Value="TRAIN">TRAIN</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div>
                        <asp:Label ID="Label19" runat="server" Text="Exchange Rate Type : "></asp:Label>
                        <asp:DropDownList runat="server" ID="ddlExType">
                            <asp:ListItem Value="A">Agreement</asp:ListItem>
                            <asp:ListItem Value="B">BOT</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <asp:Button runat="server" ID="btnExchange" Text="Submit" OnClick="btnExchange_Click" />

                    <div>
                        <asp:Label ID="Label20" runat="server" Text="Result : "></asp:Label>
                        <asp:TextBox ID="txtResult" runat="server"></asp:TextBox>
                    </div>
                </td>
            </tr>
        </table>




        <br />
        <br />
        <br />
    </form>
</body>
</html>
