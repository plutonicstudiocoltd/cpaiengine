﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class CustomerServiceModelTests
    {
        [TestMethod()]
        public void AddTest()
        {
            try
            {
                CSEncryptDecrypt.Decrypt("C:\\Users\\นิติศักดิ์\\Desktop\\CMCS201612191159071489.pdf"
                    , "C:\\Users\\นิติศักดิ์\\Desktop\\CMCS201612191159079999.pdf"
                    , "CPAI2TOPSI20162");
                //CustomerViewModel_Detail model = new CustomerViewModel_Detail();
                //CustomerServiceModel service = new CustomerServiceModel();
                //ReturnValue rtn = new ReturnValue();

                //// Set blank value

                //model.CustName = null;
                //model.Company = null;
                //model.Nation = null;
                //model.CountryKey = null;
                //model.Control = null;
                //rtn = service.Add(model, "ADMIN");
                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.CustName = "Aekkasit (Test add)";
                //model.Company = "1400";
                //model.Nation = "I";
                //model.CountryKey = "TH";
                //model.Control = new List<CustomerViewModel_Control>();
                //model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BROKER", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "2", System = "CPAI", Type = "BRssKER", Color = "#ffffff", Status = "ACTIVE" });
                //model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "2", System = "CPaI", Type = "BroKER", Color = "#ffffff", Status = "ACTIVE" });
                //rtn = service.Add(model, "ADMIN");

                //Assert.AreEqual(false, rtn.Status, rtn.Message);

                //model.CustName = "Aekkasit (Test add)";
                //model.Company = "1400";
                //model.Nation = "I";
                //model.CountryKey = "TH";
                //model.Control = new List<CustomerViewModel_Control>();
                //model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "1", System = "CPAI", Type = "BROKER", Color = "#808080", Status = "ACTIVE" });
                //model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "2", System = "TEST", Type = "CUS", Color = "#ffffff", Status = "ACTIVE" });

                //rtn = service.Add(model, "ADMIN");

                //Assert.AreEqual(true, rtn.Status, rtn.Message);

                return;

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void EditTest()
        {
            CustomerViewModel_Detail model = new CustomerViewModel_Detail();
            CustomerServiceModel service = new CustomerServiceModel();
            ReturnValue rtn = new ReturnValue();

            // Set blank value

            model.CustName = null;
            model.Company = null;
            model.Nation = null;
            model.CountryKey = null;
            model.Control = null;

            rtn = service.Edit(model, "ADMIN");
            Assert.AreEqual(false, rtn.Status, rtn.Message);

            model.CustCode = "CPAI16112415035166";
            model.CustName = "Aekkasit (EDIT)";
            model.CountryKey = "SG";
            model.Company = "1400";
            model.Nation = "I";
            model.CountryKey = "TH";
            model.Control = new List<CustomerViewModel_Control>();
            model.Control.Add(new CustomerViewModel_Control { RowID = "5d097efe6b1c4bccb3afd2aac39d9d3d", Order = "1", System = "CPAI", Type = "BROKER", Color = "#808080", Status = "ACTIVE" });
            model.Control.Add(new CustomerViewModel_Control { RowID = "b8d63db12a8b48a2acbafa49309d6d0a", Order = "2", System = "TEST", Type = "", Color = "#ffffff", Status = "ACTIVE" });
            model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "3", System = "TEST2", Type = "", Color = "#aabbcc", Status = "ACTIVE" });

            rtn = service.Edit(model, "ADMIN");

            Assert.AreEqual(false, rtn.Status, rtn.Message);

            model.CustCode = "CPAI16112415035166";
            model.CreateType = "CPAI";
            model.CustDetailID = "0ec1123deb3f492b82d2e2432c248ccc";
            model.CustName = "Aekkasit (Test EDIT)";
            model.CountryKey = "SG";
            model.Company = "1400";
            model.Nation = "I";
            model.CountryKey = "TH";
            model.Control = new List<CustomerViewModel_Control>();
            model.Control.Add(new CustomerViewModel_Control { RowID = "30de31f956e1481fab8aac15649f4e6c", Order = "1", System = "CPAI", Type = "BROKER", Color = "#808080", Status = "ACTIVE" });
            model.Control.Add(new CustomerViewModel_Control { RowID = "5dcb0f6596fc42c8a29a91ff26bcf183", Order = "2", System = "TEST", Type = "BROKER", Color = "#ffffff", Status = "ACTIVE" });
            model.Control.Add(new CustomerViewModel_Control { RowID = "", Order = "3", System = "CPAI", Type = "CUS", Color = "#aabbcc", Status = "ACTIVE" });

            rtn = service.Edit(model, "ADMIN");

            Assert.AreEqual(true, rtn.Status, rtn.Message);

        }

        [TestMethod()]
        public void SearchTest()
        {
            CustomerViewModel_Seach model = new CustomerViewModel_Seach();
            CustomerServiceModel service = new CustomerServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                //// Search All
                //model.sCustCode = "";
                //model.sCustName = "";
                //model.sCountry = "";
                //model.sCustType = "";
                //model.sStatus = "";
                //model.sCreateType = "";

                //rtn = service.Search(ref model);
                //Assert.AreEqual(true, rtn.Status, rtn.Message);

                // Search by condition
                model.sCustCode = ""; // "0000000027";
                model.sCustName = "Aek";
                model.sCountry = "";
                model.sType = "";
                model.sStatus = "";
                model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTest()
        {
            CustomerViewModel_Detail model = new CustomerViewModel_Detail();
            CustomerServiceModel service = new CustomerServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model = service.Get("");
                Assert.AreEqual(null, model, "ไม่ใส่รหัส");

                model = service.Get("e2c9a2413fb94fc0ac0aef06d7022216");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");

                model = service.Get("1000000420");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");

                model = service.Get("0ec1123deb3f492b82d2e2432c248ccc");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");


            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void Test()
        {
            var list1 = new[] { new { CustNum = "C1" }, new { CustNum = "C2" }, new { CustNum = "C3" } };
            var list2 = new[] {
                                new {
                                    ID = 1,
                                    CustNum = "C1",
                                    Name = "Jason",
                                    Country = "TH"
                                },
                                new {
                                     ID = 2,
                                    CustNum = "C1",
                                    Name = "Jason 2",
                                    Country = "TH"
                                },
                                new {
                                     ID = 3,
                                    CustNum = "C2",
                                    Name = "Jim",
                                    Country = "TH"
                                },
                                new {
                                    ID = 4,
                                    CustNum = "C3",
                                    Name = "Mike1",
                                    Country = "TH"
                                },
                                new {
                                    ID = 5,
                                    CustNum = "C3",
                                    Name = "Mike2",
                                    Country = "TH"
                                },
                                new {
                                    ID = 6,
                                    CustNum = "C3",
                                    Name = "Mike3",
                                    Country = "TH"
                                },
                                new {
                                      ID = 7,
                                    CustNum = "C4",
                                    Name = "Philip",
                                    Country = "TH"
                                }
                            };

            var lst = list1
                // Select distinct IDs that are in both lists:
                .Where(lst1 => list2
                    .Select(lst2 => lst2.CustNum)
                    .Contains(lst1.CustNum))
                .Distinct()
                // select items in list 2 matching the IDs above:
                .Select(lst1 => list2
                    .Where(lst2 => lst2.CustNum == lst1.CustNum)
                    .First());

            foreach (var item in lst)
            {
                string str = String.Format("CustNum: {0}, Name: {1}, Country: {2}", item.CustNum, item.Name, item.Country);
                Console.WriteLine(str);
            }
        }

        public void custTest()
        {

        }

        [TestMethod()]
        public void Search2Test()
        {
            CustomerViewModel_Seach model = new CustomerViewModel_Seach();
            CustomerServiceModel service = new CustomerServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {


                // Search by condition
                model.sCustCode = "";
                model.sCustName = "PTT";
                model.sCountry = "";
                model.sType = "";
                model.sStatus = "";
                model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetSystemJSONTest()
        {
            try
            {
                string json = CustomerServiceModel.GetSystemJSON("");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetSystemForDDLTest()
        {

            List<DropdownServiceModel_Data> data = new List<DropdownServiceModel_Data>();
            try
            {
                data = CustomerServiceModel.GetSystemForDDL("ACTIVE");
                Assert.AreNotEqual(null, data, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTypeJSONTest()
        {
            try
            {
                string json = CustomerServiceModel.GetTypeJSON("");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTypeForDDLTest()
        {
            List<DropdownServiceModel_Data> data = new List<DropdownServiceModel_Data>();
            try
            {
                data = CustomerServiceModel.GetTypeForDDL("ACTIVE");
                Assert.AreNotEqual(null, data, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void getCustDDLJsonTest()
        {
            try
            {
                string json = CustomerServiceModel.getCustDDLJson("CPAI", "CUS|BROKER");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

                json = CustomerServiceModel.getCustDDLJson("CPAI", "S|BROKER");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

                json = CustomerServiceModel.getCustDDLJson("CPAI", "S");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}