﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class HedgingSteeringCommitteeServiceModelTests
    {
        [TestMethod()]
        public void AddTest()
        {
            /*HedgingSteeringCommitteeViewModel_Detail model = new HedgingSteeringCommitteeViewModel_Detail();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.Underlying = "YD150SW";
                model.CropPlan = "2017";
                model.Production = "10";
                model.UnitPrice = "$/BBL";
                model.UnitVolume = "KBBL";
                model.ActiveDate_FromMonth = "5";
                model.ActiveDate_FromYear = "2017";
                model.ActiveDate_ToMonth = "06";
                model.ActiveDate_ToYear = "2017";
                model.DetailData = new List<HedgingSteeringCommitteeViewModel_DetailData>();
                model.DetailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
                {
                    Order = "1",
                    Price = "21.5",
                    Volume = "22.5"
                });
                
                rtn = service.Add(ref model, "Test User");
            }
            catch (Exception ex)
            {
                return;
            }*/
        }

        [TestMethod()]
        public void EditTest()
        {
            /*HedgingSteeringCommitteeViewModel_Detail model = new HedgingSteeringCommitteeViewModel_Detail();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.OrderID = "8f4ea0ade1394d78953ac425dd59ce5f";
                model.Underlying = "YD150SW";
                model.CropPlan = "2017";
                model.Production = "10";
                model.UnitPrice = "$/BBL";
                model.UnitVolume = "KBBL";
                model.ActiveDate_FromMonth = "5";
                model.ActiveDate_FromYear = "2017";
                model.ActiveDate_ToMonth = "6";
                model.ActiveDate_ToYear = "2017";
                model.DetailData = new List<HedgingSteeringCommitteeViewModel_DetailData>();
                model.DetailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
                {
                    RowID = "34df6008ca25484c9aa1dbc6986e3419",
                    Order = "1",
                    Price = "21.5",
                    Volume = "22.5"
                });
                model.DetailData.Add(new HedgingSteeringCommitteeViewModel_DetailData
                {
                    RowID = "ce1e0ac6263e46719c8473011e328fc1",
                    Order = "2",
                    Price = "23.5",
                    Volume = "24.5"
                });

                rtn = service.Edit(model, "Test User");
            }
            catch (Exception ex)
            {
                return;
            }*/
        }

        [TestMethod()]
        public void SearchTest()
        {
            /*HedgingSteeringCommitteeViewModel_Search model = new HedgingSteeringCommitteeViewModel_Search();
            HedgingSteeringCommitteeServiceModel service = new HedgingSteeringCommitteeServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                model.sUnderlying = "YD150SW";
                model.sCropPlan = "2017";
                model.sActiveDate_FromMonth = "5";
                model.sActiveDate_FromYear = "2017";
                model.sActiveDate_ToMonth = "6";
                model.sActiveDate_ToYear = "2017";
                rtn = service.Search(ref model);

                model.sOrderID = "8f4ea0ade1394d78953ac425dd59ce5f";
                HedgingSteeringCommitteeViewModel_Detail detail = service.GetDetail(model.sOrderID);

                //List<SelectListItem> lst = HedgingSteeringCommitteeServiceModel.getUnderlying(false, string.Empty, "CPAI", true);
            }
            catch (Exception ex)
            {
                return;
            }*/
        }
    }
}