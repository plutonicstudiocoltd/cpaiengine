﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class HolidayServiceModelTests
    {
        [TestMethod()]
        public void CheckHolidayTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void getMaxIDTest()
        {
            try
            {
                string rtn = HolidayServiceModel.getMaxID();
                Assert.AreNotEqual("", rtn, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}