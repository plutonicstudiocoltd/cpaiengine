﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class TceMkServiceModelTests
    {
        [TestMethod()]
        public void getTransactionByIDTest()
        {
            try
            {
                string json = TceMkServiceModel.getTransactionByID("201701171053430064551");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}