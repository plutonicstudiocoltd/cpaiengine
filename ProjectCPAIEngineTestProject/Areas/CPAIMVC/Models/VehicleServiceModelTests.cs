﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectCPAIEngine.Areas.CPAIMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProjectCPAIEngine.DAL.DALMaster;
using ProjectCPAIEngine.DAL.Entity;
using ProjectCPAIEngine.Areas.CPAIMVC.ViewModels;
using ProjectCPAIEngine.Utilities;

namespace ProjectCPAIEngine.Areas.CPAIMVC.Models.Tests
{
    [TestClass()]
    public class VehicleServiceModelTests
    {
        [TestMethod()]
        public void AddTest()
        {
            try
            {

                VehicleViewModel_Detail model = new VehicleViewModel_Detail();
                VehicleServiceModel service = new VehicleServiceModel();
                ReturnValue rtn = new ReturnValue();


                model.VEH_VEHICLE = "";
                model.VEH_TYPE = "";
                model.VEH_OWNER = "";
                model.VEH_LANGUAGE = "";
                model.VEH_VEH_TEXT = "";
                model.VEH_GROUP = "";
                model.Control = null;

                rtn = service.Add(ref model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.VEH_VEHICLE = "a";
                model.VEH_TYPE = "a";
                model.VEH_OWNER = "a";
                model.VEH_LANGUAGE = "a";
                model.VEH_VEH_TEXT = "a";
                model.VEH_GROUP = "a";
                model.Control = new List<VehicleViewModel_Control>();
                // System and Type is Duplicate
                model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "CPAI", Type = "BOKER", Status = "ACTIVE" });
                model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "CPsAI", Type = "BOKERa", Status = "ACTIVE" });
                //model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "AACT", Type = "vV", Status = "ACTIVE" });
                //model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "CPaI", Type = "BOKeR", Status = "ACTIVE" });

                rtn = service.Add(ref model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);


                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void EditTest()
        {
            try
            {

                VehicleViewModel_Detail model = new VehicleViewModel_Detail();
                VehicleServiceModel service = new VehicleServiceModel();
                ReturnValue rtn = new ReturnValue();

                model.VEH_ID = "";
                model.VEH_VEHICLE = "";
                model.VEH_TYPE = "";
                model.VEH_OWNER = "";
                model.VEH_LANGUAGE = "";
                model.VEH_VEH_TEXT = "";
                model.VEH_GROUP = "";
                model.Control = null;

                rtn = service.Edit(model, "ADMIN");
                Assert.AreEqual(false, rtn.Status, rtn.Message);

                model.VHE_CREATE_TYPE = "CPAI";
                model.VEH_ID = "CPAI16121615065136";
                model.VEH_VEHICLE = "b";
                model.VEH_TYPE = "b";
                model.VEH_OWNER = "b";
                model.VEH_LANGUAGE = "EN";
                model.VEH_VEH_TEXT = "b";
                model.VEH_GROUP = "b";
                model.Control = new List<VehicleViewModel_Control>();
                // System and Type is Duplicate
                model.Control.Add(new VehicleViewModel_Control { RowID = "1cbe6d8225ab4a44afeaa4e47c8e1242", System = "CPAI", Type = "BOKER", Status = "ACTIVE" });
                model.Control.Add(new VehicleViewModel_Control { RowID = "124dccb5bfb54f578dc223d93ab31724", System = "CPsAI", Type = "BOKERa", Status = "ACTIVE" });
                model.Control.Add(new VehicleViewModel_Control { RowID = "f5ae4277fc394a768b8488a39a529328", System = "AACT", Type = "vV", Status = "ACTIVE" });
                model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "AACT", Type = "BOKeR", Status = "ACTIVE" });

                rtn = service.Edit(model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);

                model.VHE_CREATE_TYPE = "SAP";
                model.VEH_ID = "1000000307";

                model.Control = new List<VehicleViewModel_Control>();
                // System and Type is Duplicate
                model.Control.Add(new VehicleViewModel_Control { RowID = "1", System = "CPAI", Type = "SCH_C", Status = "ACTIVE" });
                model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "CPAI", Type = "SCH_P", Status = "ACTIVE" });
                //model.Control.Add(new VehicleViewModel_Control { RowID = "", System = "CPaI", Type = "BOKeR", Status = "ACTIVE" });

                rtn = service.Edit(model, "ADMIN");

                Assert.AreEqual(true, rtn.Status, rtn.Message);


                return;
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void SearchTest()
        {
            VehicleViewModel_Search model = new VehicleViewModel_Search();
            VehicleServiceModel service = new VehicleServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                // Search All
                //model.sVendorCode = "";
                //model.sVendorName = "";
                //model.sCountry = "";
                //model.sStatus = "";
                //model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);

                // Search by condition
                model.sCreateDate = "";
                model.sVehicleID = "1000000307";
                model.sVehicleName = "";
                model.sVehicleText = "";
                model.sStatus = "";
                model.sType = "";
                model.sCreateType = "";

                rtn = service.Search(ref model);
                Assert.AreEqual(true, rtn.Status, rtn.Message);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetTest()
        {
            VehicleViewModel_Detail model = new VehicleViewModel_Detail();
            VehicleServiceModel service = new VehicleServiceModel();
            ReturnValue rtn = new ReturnValue();
            try
            {
                //SAP
                model = service.Get("1000000307");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");

                // CPAI
                model = service.Get("CPAI16121615014913");
                Assert.AreNotEqual(null, model, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void GetVehTypeJSONTest()
        {
            try
            {
                string json = VehicleServiceModel.GetCtrlTypeJSON("");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }

        [TestMethod()]
        public void getVehicleDDLJsonTest()
        {
            try
            {
                string json = VehicleServiceModel.getVehicleDDLJson("CPAI","");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

                json = VehicleServiceModel.getVehicleDDLJson("CPAI", "SCH_P|BOKER");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

                json = VehicleServiceModel.getVehicleDDLJson("CPAI", "CRUDE");
                Assert.AreNotEqual("", json, "ไม่พบข้อมูล");

            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, "Error");
                return;
            }
        }
    }
}