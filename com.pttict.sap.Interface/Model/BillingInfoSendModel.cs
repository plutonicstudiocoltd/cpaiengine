﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class BillingInfoSendModel
    {
        public string VKORG { get; set; } //Sales Organization
        public string S_VTWEG { get; set; } //BAPI Selection Structure: Distribution Channel
        public string S_FKART { get; set; } //BAPI Selection Structure: Distribution Channel
        public string S_FKDAT { get; set; } //IS-H: General Range Structure for Billing Date Invoice
        public string S_PRSDT { get; set; } //Range For PRSDT
        public string S_KUNNR { get; set; } //Range Structure for Data Element KUNAG
        public string S_VSTEL { get; set; } //Range Structure for Data Element VSTEL
        public string S_VBELN { get; set; } //Range: Sales and Distribution Document Number
        public string S_FKSTO { get; set; } //Range For FKSTO
        public string S_ERDAT { get; set; } //Range For ERDAT
        public string S_ERNAM { get; set; } //Range For ERNAM
        public string S_AEDAT { get; set; } //Range For AEDAT
    }
}
