﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.sap.Interface.sap.intansit.post;


namespace com.pttict.sap.Interface.Model
{
    public class IntansitPostModel
    {
        public ZGOODSMVT_CODE ZGOODSMVT_CODE { get; set; }
        public ZGOODSMVT_HEADER ZGOODSMVT_HEADER { get; set; }
        public List<BAPIRET2> RETURN { get; set; }
        public List<ZGOODSMVT_ITEM_01> ZGOODSMVT_ITEM_01 { get; set; }
        public List<BAPIOIL2017_GM_ITM_CRTE_PARAM> ZGOODSMVT_ITEM_PARAM { get; set; }
        public List<ZGOODSMVT_ITEM_QUAN> ZGOODSMVT_ITEM_QUAN { get; set; }
        public BAPI2017_GM_HEAD_RET GM_HEAD_RET { get; set; }
    }
}
