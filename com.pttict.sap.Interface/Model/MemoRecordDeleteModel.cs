﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class MemoRecordDeleteModel
    {
        public string BUKRS { get; set; } //Company Code
        public string IDENR { get; set; } //MEMO Number
        public string TESTRUN { get; set; }
    }
}
