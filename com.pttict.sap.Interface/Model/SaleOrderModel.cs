﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.pttict.sap.Interface.Model
{
    public class SaleOrderModel
    {
        public List<BAPISDHD1> mORDER_HEADER_IN { get; set; }//ORDER_HEADER_IN 
        public List<BAPICOND> mORDER_CONDITIONS_IN { get; set; }//ORDER_CONDITIONS_IN
        public List<BAPISDITM> mORDER_ITEMS_IN { get; set; }//ORDER_ITEMS_IN 
        public List<BAPIPARNR> mORDER_PARTNERS { get; set; }//ORDER_PARTNERS
        public List<BAPISCHDL> mORDER_SCHEDULES_IN { get; set; }//ORDER_SCHEDULES_IN
        //for Change
        public string SALESDOCUMENT_IN { get; set; }//SALESDOCUMENT_IN
        public string Flag { get; set; } //Flag del


        public class BAPISDHD1  //ORDER_HEADER_IN 
        {
            public string DOC_TYPE { get; set; }               
            public string SALES_ORG { get; set; }                
            public string DISTR_CHAN { get; set; }                 
            public string DIVISION { get; set; } //Global Config                 
            public string REQ_DATE_H { get; set; }                
            public string DATE_TYPE { get; set; } //Global Config                 
            public string PURCH_NO_C { get; set; }                  
            public string PURCH_NO_S { get; set; }
            public string CURRENCY { get; set; }               
        }
        public class BAPICOND  //ORDER_CONDITIONS_IN 
        {
            public string ITM_NUMBER { get; set; }//for change
            public string COND_TYPE { get; set; }//Global Config
            public decimal COND_VALUE { get; set; }
            public string CURRENCY { get; set; }
            public decimal COND_P_UNT { get; set; }//Global Config
        }

        public class BAPISDITM //ORDER_ITEMS_IN
        {
            //change
            public string ITM_NUMBER { get; set; }
            public string PURCH_DATE { get; set; }//2017-06-01
            //
            public string MATERIAL { get; set; }
            public string PLANT { get; set; }
            public string ITEM_CATEG { get; set; } //Global Config          
            public string SALES_UNIT { get; set; }
            //del
            public string REASON_REJ { get; set; } //Global Config    
        }
        public class BAPIPARNR // ORDER_PARTNERS
        {

            public string PARTN_ROLE_SP { get; set; }//Global Config
            public string PARTN_ROLE_BP { get; set; }//Global Config
            public string PARTN_ROLE_PY { get; set; }//Global Config
            public string PARTN_ROLE_SH { get; set; }//Global Config
            public string PARTN_ROLE { get; set; }//Global Config
            public string PARTN_NUMB { get; set; }
            public string ITM_NUMBER { get; set; }//Global Config


        }

        public class BAPISCHDL //ORDER_SCHEDULES_IN
        {
            //for change
            public string ITM_NUMBER { get; set; }
            public string SCHED_LINE { get; set; }
            //
            public string REQ_DATE { get; set; }
            public string DATE_TYPE { get; set; }//Global Config
            public decimal REQ_QTY { get; set; }
        }

    }  
}
