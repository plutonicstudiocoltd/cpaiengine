﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using com.pttict.sap.Interface.sap.accrual.post;

namespace com.pttict.sap.Interface.Service
{
    public class AccrualPostServiceConnectorImpl : BasicBean
    {
        public void connect(string config, BAPIACHE09 DocumentHeader, List<BAPIACGL09> AccountGl, List<BAPIACCR09> CurrencyAmount, List<BAPIPAREX> Extension2, List<ZFI_GL_MAPPING> zfi_GL_MAPPING)
        {
            try
            {
                AccrualService service = new AccrualService();
                service.Post(config, DocumentHeader, AccountGl, CurrencyAmount, Extension2, zfi_GL_MAPPING);

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
