﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using static com.pttict.sap.Interface.Model.GoodsIssueUpdateModel;


namespace com.pttict.sap.Interface.Service
{
    public class GoodsIssueUpdateService : BasicBean
    {
        public string Update(string JsonConfig, string content, ref string msg)
        {
            try
            {

                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<GoodsIssueUpdateModel>(content);
                sap.gi.update.DeliveryQtyUpdateRequest_Sync_Out_SIService SAPService = new sap.gi.update.DeliveryQtyUpdateRequest_Sync_Out_SIService();

                string ACT_GI_DATE = jsonContent.ACT_GI_DATE;
                sap.gi.update.LIKP DELIVERY_HEADER = new sap.gi.update.LIKP();
                DELIVERY_HEADER.VBELN = jsonContent.mDELIVERY_HEADER[0].VBELN; //DELIVERY_NUMBER = "1230002881";
                //Sale Type: SWAP=Z005, Clearline=Z008, Borrow=Z009
                DELIVERY_HEADER.TRATY = jsonContent.mDELIVERY_HEADER[0].TRATY;//"Z008";

                string DELIVERY_NUMBER = DELIVERY_HEADER.VBELN;

                int index1 = 0;
                sap.gi.update.ZDO_ITEM[] DELIVERY_ITEM = new sap.gi.update.ZDO_ITEM[jsonContent.mDELIVERY_ITEM.Count];
                foreach (ZDO_ITEM jsDELIVERY_ITEM in jsonContent.mDELIVERY_ITEM)
                {
                    DELIVERY_ITEM[index1] = new sap.gi.update.ZDO_ITEM();
                    DELIVERY_ITEM[index1].VBELN_VL = DELIVERY_HEADER.VBELN; //DELIVERY_NUMBER = "1230002881";
                    DELIVERY_ITEM[index1].POSNR_VL = jsDELIVERY_ITEM.POSNR_VL;//"000010";
                    DELIVERY_ITEM[index1].MATNR = jsDELIVERY_ITEM.MATNR; //"YMURBAN";
                    DELIVERY_ITEM[index1].WERKS = jsDELIVERY_ITEM.WERKS; //"1200";
                    DELIVERY_ITEM[index1].LIANP = jsDELIVERY_ITEM.LIANP; //"X"; Global config
                    DELIVERY_ITEM[index1].LFIMG = jsDELIVERY_ITEM.LFIMG; //1600;
                    DELIVERY_ITEM[index1].LFIMGSpecified = true;
                    if (jsDELIVERY_ITEM.SALES_ORG == "1100" && jsDELIVERY_ITEM.WERKS == "1200")
                    {
                        if (jsDELIVERY_ITEM.CustomerCode == "0000400004")
                        {
                            DELIVERY_ITEM[index1].LGORT = "12D7";
                        }
                        else if (jsDELIVERY_ITEM.CustomerCode == "0000000024")
                        {
                            DELIVERY_ITEM[index1].LGORT = "12C1";
                        }
                        else if (jsDELIVERY_ITEM.CustomerCode == "0000000053")
                        {
                            DELIVERY_ITEM[index1].LGORT = "12D9";
                        }
                        else
                        {
                            DELIVERY_ITEM[index1].LGORT = null;
                        }
                    }
                    else if (jsDELIVERY_ITEM.SALES_ORG == "1400" && jsDELIVERY_ITEM.WERKS == "4200")
                    {
                        if(jsDELIVERY_ITEM.CustomerCode == "0000400001")
                        {
                            DELIVERY_ITEM[index1].LGORT = "42D1";
                        }
                        else
                        {
                            DELIVERY_ITEM[index1].LGORT = null;
                        }
                    }
                    else
                    {
                        DELIVERY_ITEM[index1].LGORT = null;
                    }
                    //DELIVERY_ITEM[index1].LGORT = jsDELIVERY_ITEM.LGORT; // "12B1";
                    DELIVERY_ITEM[index1].ZTEMP = jsDELIVERY_ITEM.ZTEMP; //40;
                    DELIVERY_ITEM[index1].ZTEMPSpecified = true;
                    DELIVERY_ITEM[index1].ZDENS_15C = jsDELIVERY_ITEM.ZDENS_15C; // 0.8832M;
                    DELIVERY_ITEM[index1].ZDENS_15CSpecified = true;
                    index1++;
                }

                index1 = 0;
                sap.gi.update.LIPSO2[] DELIVERY_ITEM_ADD_QTY = new sap.gi.update.LIPSO2[jsonContent.mDELIVERY_ITEM_ADD_QTY.Count];
                foreach (LIPSO2 jsDELIVERY_ITEM_ADD_QTY in jsonContent.mDELIVERY_ITEM_ADD_QTY)
                {
                    DELIVERY_ITEM_ADD_QTY[index1] = new sap.gi.update.LIPSO2();
                    DELIVERY_ITEM_ADD_QTY[index1].VBELN = DELIVERY_HEADER.VBELN;
                    DELIVERY_ITEM_ADD_QTY[index1].POSNR = jsDELIVERY_ITEM_ADD_QTY.POSNR;//"000010";
                    DELIVERY_ITEM_ADD_QTY[index1].MSEHI = jsDELIVERY_ITEM_ADD_QTY.MSEHI;//"BBL";
                    DELIVERY_ITEM_ADD_QTY[index1].ADQNT = jsDELIVERY_ITEM_ADD_QTY.ADQNT;// 1600;
                    DELIVERY_ITEM_ADD_QTY[index1].ADQNTSpecified = true;
                    DELIVERY_ITEM_ADD_QTY[index1].MANEN = jsDELIVERY_ITEM_ADD_QTY.MANEN;// "X";       
                    index1++;
                }

                sap.gi.update.BAPISDTEXT[] DELVIERY_TEXT = new sap.gi.update.BAPISDTEXT[0];
                sap.gi.update.BAPIRET2[] RETURN_DO = new sap.gi.update.BAPIRET2[0];
                sap.gi.update.BAPIRET2[] RETURN_GI = new sap.gi.update.BAPIRET2[0];

                //SAPService.Url = "http://ec-sap-toppid.thaioilgroup.com:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=BC_CIP&receiverParty=&receiverService=&interface=DeliveryQtyUpdateRequest_Sync_Out_SI&interfaceNamespace=http://thaioilgroup.com/_i_oil/product/deliveryqty_update/";
                //SAPService.Credentials = AuthServiceConnect("ZPI_IF_CIP", "1@zpi_if_cip");

                var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                SAPService.Url = jsonConfig.sap_url;
                SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);


                string UPD_DO_IND = "X";
                string UPD_GI_IND = "X";

                SAPService.DeliveryQtyUpdateRequest_Sync_Out_SI(
                      ACT_GI_DATE,
                      DELIVERY_HEADER,
                      DELIVERY_NUMBER,
                      UPD_DO_IND,
                      UPD_GI_IND,
                  ref DELIVERY_ITEM,
                  ref DELIVERY_ITEM_ADD_QTY,
                  ref DELVIERY_TEXT,
                  ref RETURN_DO,
                  ref RETURN_GI
                      );

                string logon_GI = "";
                string logon_DO = "";
                string msg_GI = "";
                string msg_DO = "";
                int rcount = 0;
                int chkerr = 0;
                if (RETURN_GI != null)
                {
                    rcount = RETURN_GI.Length;

                    for (int i = 0; i < rcount; i++)
                    {
                        if (RETURN_GI[i].TYPE == "E")
                        {
                            //msg += RETURN_GI[i].MESSAGE + "|" + RETURN_GI[i].MESSAGE_V1 + "|" + RETURN_GI[i].MESSAGE_V2 + "|" + RETURN_GI[i].MESSAGE_V3 + "|" + RETURN_GI[i].MESSAGE_V4;
                            msg_GI += RETURN_GI[i].MESSAGE + "|";
                            chkerr = 0;
                        }
                        else
                        {
                            chkerr++;
                        }
                    }
                }
                else
                {
                    chkerr = 0;
                } 
                logon_GI = chkerr == 0 ? (String.IsNullOrEmpty(msg_GI) ? "ERROR_GI" : "ERROR_GI" + "|" + msg_GI) : "Success_GI";
                int rcount_DO = 0;
                chkerr = 0;
                if (RETURN_DO != null)
                {
                    rcount_DO = RETURN_DO.Length;
                    for (int i = 0; i < rcount_DO; i++)
                    {
                        if (RETURN_DO[i].TYPE == "E")
                        {
                            //msg += RETURN_DO[i].MESSAGE + "|" + RETURN_DO[i].MESSAGE_V1 + "|" + RETURN_DO[i].MESSAGE_V2 + "|" + RETURN_DO[i].MESSAGE_V3 + "|" + RETURN_DO[i].MESSAGE_V4;
                            msg_DO += RETURN_DO[i].MESSAGE + "|";
                            chkerr = 0;
                        }
                        else
                        {
                            chkerr++;
                        }
                    }
                }
                else
                {
                    chkerr = 0;
                }
                logon_DO = chkerr == 0 ? (String.IsNullOrEmpty(msg_DO)? "ERROR_DO" : "ERROR_DO" + "|" + msg_DO ): "Success_DO"; 
                return msg = logon_GI + "||" + logon_DO;
            }
            catch (Exception ex)
            {                
                throw ex; 
            }

        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
