﻿using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common;
using System.Web;

namespace com.pttict.sap.Interface.Service
{
    public class GoodsIssueUpdateServiceConnectorImpl : BasicBean, DownstreamConnector<string>
    {
        public DownstreamResponse<string> connect(string Jsonconfig, string content)
        {

            DownstreamResponse<string> downResp = new DownstreamResponse<string>();
            downResp.setNameSpace(ServiceConstant.NAMESPACE);
            downResp.setResultCode(ServiceConstant.RESP_SYETEM_ERROR[0]);
            downResp.setResultDesc(ServiceConstant.RESP_SYETEM_ERROR[1]);

            GoodsIssueUpdateService service = new GoodsIssueUpdateService();
            string msg = "";
            string res = service.Update(Jsonconfig, content, ref msg);
            bool chkerr = false;
            bool chkMsgGI = false;
            bool chkMsgDO = false;
            msg = msg.Replace("||", "#");
            var msgGI = msg.Split('#')[0].Trim();
            var arrMsgGI = msgGI.Split('|');
            if (arrMsgGI.Count() > 1)
            {
                msgGI = arrMsgGI[1];
                chkerr = true;
                chkMsgGI = true;
            }
            var msgDO = msg.Split('#')[1].Trim();
            var arrMsgDO = msgDO.Split('|');
            if(arrMsgDO.Count() > 1)
            {
                msgDO = arrMsgDO[1];
                chkerr = true;
                chkMsgDO = true;
            }

            if (!chkerr)
            {
                downResp.setResultCode(ServiceConstant.RESP_SUCCESS[0]);
                downResp.setResultDesc(ServiceConstant.RESP_SUCCESS[1]);
                downResp.setResponseData(res);
            }
            else
            {
                msg = "";
                if (chkMsgGI && chkMsgDO)
                {
                    msg = msgGI +" & "+ msgDO;
                }else if(chkMsgGI)
                {
                    msg = msgGI;
                }else if(chkMsgDO)
                {
                    msg = msgDO;
                }
                downResp.setResultCode(ServiceConstant.RESP_SAP_ERROR[0]);
                downResp.setResultDesc(msg);
                downResp.setResponseData(res);
            }

            return downResp;
        }

    }
}

