﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace com.pttict.sap.Interface.Service
{
    public class IntansitPostService : BasicBean
    {
        public string Post(string JsonConfig, string JsonContent, ref string msg)
        {
            string result = "S";
            sap.intansit.post.GoodsInTransitPostRequest_Sync_Out_SIService SAPService = new sap.intansit.post.GoodsInTransitPostRequest_Sync_Out_SIService();

            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonContent = javaScriptSerializer.Deserialize<IntansitPostModel>(JsonContent);

            sap.intansit.post.ZGOODSMVT_CODE ZGOODSMVT_CODE = jsonContent.ZGOODSMVT_CODE;
            sap.intansit.post.ZGOODSMVT_HEADER ZGOODSMVT_HEADER = jsonContent.ZGOODSMVT_HEADER;
            sap.intansit.post.BAPIRET2[] RETURN = jsonContent.RETURN.ToArray();
            sap.intansit.post.ZGOODSMVT_ITEM_01[] ZGOODSMVT_ITEM_01 = jsonContent.ZGOODSMVT_ITEM_01.ToArray();
            sap.intansit.post.BAPIOIL2017_GM_ITM_CRTE_PARAM[] ZGOODSMVT_ITEM_PARAM = jsonContent.ZGOODSMVT_ITEM_PARAM.ToArray();
            sap.intansit.post.ZGOODSMVT_ITEM_QUAN[] ZGOODSMVT_ITEM_QUAN = jsonContent.ZGOODSMVT_ITEM_QUAN.ToArray();
            sap.intansit.post.BAPI2017_GM_HEAD_RET GM_HEAD_RET = jsonContent.GM_HEAD_RET;

            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            SAPService.Url = jsonConfig.sap_url;
            SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

            GM_HEAD_RET = SAPService.GoodsInTransitPostRequest_Sync_Out_SI(ZGOODSMVT_CODE, ZGOODSMVT_HEADER, ref RETURN, ref ZGOODSMVT_ITEM_01, ref ZGOODSMVT_ITEM_PARAM, ref ZGOODSMVT_ITEM_QUAN);
            if (RETURN != null) {
                int rcount = RETURN.Length;
                for (int i = 0; i < rcount; i++)
                {
                    if (RETURN[i].TYPE == "E")
                    {
                        result = "E";
                        msg += RETURN[i].MESSAGE + " " + RETURN[i].MESSAGE_V1 + " " + RETURN[i].MESSAGE_V2 + " " + RETURN[i].MESSAGE_V3 + " " + RETURN[i].MESSAGE_V4;
                    }
                }
            }
            

            if (result == "S")
            {
                if (GM_HEAD_RET != null) {
                    result = GM_HEAD_RET.MAT_DOC;
                }                
            }

            return result;

        }

        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }

        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;            

            return NC;
        }

    }
}
