﻿using com.pttict.downstream.common.utilities;
using com.pttict.downstream.common.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.pttict.downstream.common;

namespace com.pttict.sap.Interface.Service
{
    public class MemoRecordCreateServiceConnectorImpl : BasicBean
    {
        public sap.memorecord.create.BAPIRETURN1 connect(string config, string content)
        {
            try
            {
                MemoRecordService service = new MemoRecordService();

                var returnResult = service.Create(config, content);
                return returnResult;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
