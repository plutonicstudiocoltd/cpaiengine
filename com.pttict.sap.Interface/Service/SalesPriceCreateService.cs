﻿using com.pttict.downstream.common.utilities;
using com.pttict.sap.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using static com.pttict.sap.Interface.Model.SalesPriceCreateModel;


namespace com.pttict.sap.Interface.Service
{
    public class SalesPriceCreateService : BasicBean
    {

        //public void Create(string JsonConfig, string content)
        //{
        public string Create(string JsonConfig, string content, ref string msg)
        {
            try
            {
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var jsonContent = javaScriptSerializer.Deserialize<List<SalesPriceCreateModel>>(content);
                string result_status = "";
                msg = "";
                string temp_msg = "";
                foreach (var j in jsonContent)
                {
                    string DATAB = j.DATAB;//"2017-06-01";Delivery Date
                    string DATBI = j.DATBI;//"2017-06-01";//Delivery Date
                    string KBETR = j.KBETR;//"1860.5502";//Unit Price
                    string KMEIN = j.KMEIN;//"BBL";//Sales Unit
                    string KONWA = j.KONWA;//"TH4";//Currency
                    string KOTABNR = j.KOTABNR;//"901"; //Global Config
                    decimal KPEIN = j.KPEIN;//1;//Global Config
                    bool KPEINSpecified = true;
                    string KRECH = j.KRECH;//"";//Global Config
                    string KSCHL = j.KSCHL; //"ZCR1"; //Provisional Price
                    string KUNNR = j.KUNNR; //"0000000013";//Customer Code
                    string MATNR = j.MATNR; //"YMB";//Material
                    string VKORG = j.VKORG; //"1100";//Company Code
                    string VTWEG = j.VTWEG; //"31";//DC
                    string WERKS = j.WERKS; //"1200";//Plant
                    string ZZVSTEL = j.ZZVSTEL; //"12SV";//Shipping Point


                    sap.salesprice.create.BAPIRETURN1[] BAPIreturn = new sap.salesprice.create.BAPIRETURN1[0];

                    sap.salesprice.create.PriceCreateRequest_Sync_Out_SIService SAPService = new sap.salesprice.create.PriceCreateRequest_Sync_Out_SIService();
                    var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);
                    SAPService.Url = jsonConfig.sap_url;
                    SAPService.Credentials = AuthServiceConnect(jsonConfig.sap_user, jsonConfig.sap_pass);

                    string SUBRC = "";
                    string result = SAPService.PriceCreateRequest_Sync_Out_SI(
                        DATAB,
                        DATBI,
                        KBETR,
                        KMEIN,
                        KONWA,
                        KOTABNR,
                        KPEIN,
                        KPEINSpecified,
                        KRECH,
                        KSCHL,
                        KUNNR,
                        MATNR,
                        VKORG,
                        VTWEG,
                        WERKS,
                        ZZVSTEL,
                        ref BAPIreturn

                        );

                    
                    int rcount = BAPIreturn.Length;
                    for (int i = 0; i < rcount; i++)
                    {
                        if (BAPIreturn[i].TYPE == "E")
                        {
                            result_status += "E";
                            temp_msg += "|Dalivery Date: " + j.DATAB + ", " + "ZCR Index: " + j.KSCHL + ", " + "Customer Code: " + j.KUNNR + ", " + "Materila: " + j.MATNR + ", " + "Company Code: " + j.VKORG + ", " + "Plant: " + j.WERKS;
                            temp_msg += "@" + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE) ? BAPIreturn[i].MESSAGE + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V1) ? BAPIreturn[i].MESSAGE_V1 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V2) ? BAPIreturn[i].MESSAGE_V2 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V3) ? BAPIreturn[i].MESSAGE_V3 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V4) ? BAPIreturn[i].MESSAGE_V4 + "; " : "");
                            
                        }
                        else if (BAPIreturn[i].TYPE == "S")
                        {
                            result_status += "S";
                            temp_msg += "|Dalivery Date: " + j.DATAB + ", " + "ZCR Index: " + j.KSCHL + ", " + "Customer Code: " + j.KUNNR + ", " + "Materila: " + j.MATNR + ", " + "Company Code: " + j.VKORG + ", " + "Plant: " + j.WERKS;
                            temp_msg += "@" + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE) ? BAPIreturn[i].MESSAGE + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V1) ? BAPIreturn[i].MESSAGE_V1 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V2) ? BAPIreturn[i].MESSAGE_V2 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V3) ? BAPIreturn[i].MESSAGE_V3 + "; " : "")
                                           + (!string.IsNullOrEmpty(BAPIreturn[i].MESSAGE_V4) ? BAPIreturn[i].MESSAGE_V4 + "; " : "");
                        }
                    }

                    // return res;
                    msg = temp_msg ;

                }
                return result_status;

                //string DATAB = "2017-06-01";//Delivery Date
                //string DATBI = "2017-06-01";//Delivery Date
                //string KBETR = "1860.5502";//Unit Price
                //string KMEIN = "BBL";//Sales Unit
                //string KONWA = "TH4";//Currency
                //string KOTABNR = "901"; //Global Config
                //decimal KPEIN = 1;//Global Config
                //bool KPEINSpecified = true;
                //string KRECH = "";//Global Config
                //string KSCHL = "ZCR1"; //Provisional Price
                //string KUNNR = "0000000013";//Customer Code
                //string MATNR = "YMB";//Material
                //string VKORG = "1100";//Company Code
                //string VTWEG = "31";//DC
                //string WERKS = "1200";//Plant
                //string ZZVSTEL = "12SV";//Shipping Point

            }
            catch (Exception ex)
            {
                return "Error can't process :" + ex;
            }
        }



        private System.Net.NetworkCredential AuthServiceConnect(string JsonConfig)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var jsonConfig = javaScriptSerializer.Deserialize<ConfigModel>(JsonConfig);

            NC.UserName = jsonConfig.sap_user;
            NC.Password = jsonConfig.sap_pass;

            return NC;
        }
        private System.Net.NetworkCredential AuthServiceConnect(string sap_user, string sap_pass)
        {
            System.Net.NetworkCredential NC = new System.Net.NetworkCredential();
            NC.UserName = sap_user;
            NC.Password = sap_pass;

            return NC;
        }
    }
}
